<?php

class Dto_CdrInvoice extends Dto_CdrDb {

    public function getStratosInmarsatSummary ($_comandsearch, $_beginDate, $_endDate, $_whsearch){
        //search on stratos_inmarsat
        $query  = "select S.cdrdate, S.cdrtime, S.product, S.calsig, S.numberfrom, S.destinationnumber, ";
        $query .= "S.sessionclosuredate, S.sessionclosuretime, S.calltype, S.bitnum, S.numwrdup ";
        $query .= "from stratos_inmarsat S ";
        $query .= "where S.calsig='$_comandsearch' and S.cdrdate between '$_beginDate' and '$_endDate' ";
        $query .= "order by (S.cdrdate, S.cdrtime) asc";

        $rows = $this->selectRows($query);

        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total==0) {
            return false;
        }

        return $rows;
    }

    public function getCdrTemporarySummary($_accountid, $_comandsearch, $_beginDate, $_endDate, $_whsearch){
        //search on cdr
        if($_whsearch=="SCRD"){
            $query  = "select S.id, S.cdrdate, S.cdrtime, P.serial, P.voicenumber, ";
            $query .= "P.planid, S.destinationnumber, C.traffictypedesc, C.qty, C.qtyunitcode, C.chargeoutofbundle ";
            $query .= "from stratos_inmarsat S, cdr C, simcardperiod P ";
            $query .= "where S.id = C.sourceid and S.numberfrom = P.voicenumber ";
            $query .= "and P.serial = '$_comandsearch'	and P.id = C.simcardperiod ";
            $query .= "and C.provideraccountid = $_accountid	and S.cdrdate between '$_beginDate' and '$_endDate' ";
            $query .= "order by (S.cdrdate, S.cdrtime) asc";

        }else if($_whsearch=="IMSI"){
            $query  = "select S.id, S.cdrdate, S.cdrtime, P.serial, P.voicenumber, ";
            $query .= "P.planid, S.destinationnumber, C.traffictypedesc, C.qty, C.qtyunitcode, C.chargeoutofbundle ";
            $query .= "from stratos_inmarsat S, cdr C, simcardperiod P ";
            $query .= "where S.id = C.sourceid and S.numberfrom = P.voicenumber ";
            $query .= "and S.calsig = '$_comandsearch'	and P.id = C.simcardperiod ";
            $query .= "and C.provideraccountid = $_accountid	and S.cdrdate between '$_beginDate' and '$_endDate' ";
            $query .= "order by (S.cdrdate, S.cdrtime) asc";
        }else{
            return false;
        }

        $rows = $this->selectRows($query);

        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total==0) {
            return false;
        }

        return $rows;

    }

    private function createInvoiceSequence($_invoiceid, $_providerid){
        //not exist
        $query  = "select serialnumber, MAX(sequence) ";
        $query .= "from invoicesequence ";
        $query .= "where customerid='$_providerid' group by serialnumber";
        $row = $this->selectOneRow($query);
        $newCode = "";
        $nextNum = ((int)($row[1])+1);
        $newCode = $row[0]."-".$nextNum;

        $queryInsert = "INSERT into invoicesequence (serialnumber, sequence, customerid, invoiceid) ";
        $queryInsert .= " VALUES ('$row[0]', '$nextNum', '$_providerid', '$_invoiceid') ";
        $ok = $this->executeUpdate($queryInsert);

        return $newCode;
    }

    public function getInvoiceSequence($_invoiceid, $_providerid){
        //Provider 10003
        $query  = "select serialnumber, sequence ";
        $query .= "from invoicesequence ";
        $query .= "where invoiceid=$_invoiceid and customerid=$_providerid ";

        $invoiceCode="";

        $rows = $this->selectRows($query);
        if (!$rows) {
            $invoiceCode=$this->createInvoiceSequence($_invoiceid, $_providerid);
        }
        $total = count($rows);
        if ($total==0) {
            $invoiceCode=$this->createInvoiceSequence($_invoiceid, $_providerid);
        }

        foreach($rows as $row){
            //if exist
            $invoiceCode = $row['serialnumber'].'-'.$row['sequence'];
        }

        return $invoiceCode;
    }

    public function getMovementsInvoince($_invoiceid){
        $query  = "select C.cdrdate, C.cdrtime, S.voicenumber, I.destinationnumber, I.calltype, C.traffictypedesc, C.qty, C.qtyunitcode, C.chargeoutofbundle ";
        $query .= "from cdr C, simcardperiod S, stratos_inmarsat I ";
        $query .= "where S.id=C.simcardperiod and I.id=C.sourceid and S.invoiceid=$_invoiceid ";
        $query .= "order by (C.cdrdate, C.cdrtime) asc";

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total==0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            
            $e = new CdrMovementInvoice();

            $e->date = $row['cdrdate'];
            $e->time = $row['cdrtime'];
            $e->subscriber = $row['voicenumber'];
            $e->numberDestination = $row['destinationnumber'];
            $e->calltype = $row['calltype'];
            $e->plan = $row['traffictypedesc'];
            $e->qty_duration = $row['qty'];
            $e->duration = " ".$row['qtyunitcode'];
            $e->additionalCost = $row['chargeoutofbundle'];

            array_push($aRecord, $e);

        }

        return $aRecord;

    }
    public function getSimcardPeriod($_invoiceid) {

        $query = "select s.* from simcardperiod s ";
        $query .= "where s.invoiceid = $_invoiceid ";
        $query .= "order by s.serial , s.ym desc ";


        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            $e = new CdrSimcardPeriod();

            $e->id = $row["id"];
            $e->serial = $row["serial"];
            $e->voicenumber = $row["voicenumber"];
            $e->datanumber = $row["datanumber"];
            $e->code2 = $row["code2"];
            $e->year = $row["year"];
            $e->month = $row["month"];
            $e->day = $row["day"];
            $e->customeraccountid = intval($row["customeraccountid"]);
            $e->provideraccountid = intval($row["provideraccountid"]);

            $e->subscriptionprice = doubleval($row["subscriptionprice"]);
            $e->fixedchargesprice = doubleval($row["fixedchargesprice"]);
            $e->additionalchargesprice = doubleval($row["additionalchargesprice"]);

            $e->activationprice = doubleval($row["activationprice"]);
            $e->suspensionprice = doubleval($row["suspensionprice"]);
            $e->deactivationprice = doubleval($row["deactivationprice"]);

            $e->invoiceid = intval($row["invoiceid"]);
            $e->planid = intval($row["planid"]);

            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getById($_id) {

        $query = "select * from invoice where id = $_id order by invoicedate desc ";
        $row = $this->selectOneRow($query);
        $e = new CdrInvoice();
        $e->id = $row["id"];
        $e->provideraccountid = $row["provideraccountid"];
        $e->customeraccountid = $row["customeraccountid"];
        $e->invoicedate = $row["invoicedate"];
        $e->duedate = $row["duedate"];
        $e->year = $row["ym"];
        $e->month = $row["month"];
        $e->customername = $row["customername"];
        $e->total = doubleval($row["total"]);
        $e->paymentstatus = intval($row["paymentstatus"]);

        return $e;
    }

    public function getCustomerInvoices($_provideraccountid) {
        $query = "select * from invoice where provideraccountid = $_provideraccountid order by invoicedate desc ";



        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            $e = new CdrInvoice();
            $e->id = $row["id"];
            $e->provideraccountid = $row["provideraccountid"];
            $e->customeraccountid = $row["customeraccountid"];
            $e->invoicedate = $row["invoicedate"];
            $e->duedate = $row["duedate"];
            $e->year = $row["ym"];
            $e->month = $row["month"];
            $e->customername = $row["customername"];
            $e->total = number_format($row["total"], 2);
            $e->paymentstatus = intval($row["paymentstatus"]);

            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getInvoiceCdrs($_invoiceid) {
 
        
        $query = "select cdr.chargecode, cdr.qtyunitcode, servicetype, cdr.chargeoutofbundle, cdr.chargeinbundle, cdr.qty, cdr.cdrdate, cdr.cdrtime,  ";
        $query.= "cdr.traffictypedesc, cdr.simcardperiod ,  ";
        $query.= "sp.voicenumber, sp.datanumber , ";
        $query.= "si.destinationnumber ";
        $query.= "from cdr cdr ";
        $query.= "right join simcardperiod sp on cdr.simcardperiod =  sp.id ";
        $query.= "right join stratos_inmarsat si on si.id = cdr.sourceid ";
        $query.= "where sp.invoiceid = $_invoiceid ";
        $query.= "order by sp.voicenumber , cdr.qtyunitcode, cdr.cdrdate asc, cdr.cdrtime asc ";
 
	//echo $query; die();

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {

            array_push($aRecord, $this->getRowToCdr($row));
        }
        return $aRecord;
    }
    
    public function getCdrsByProvider($_provideraccountid ) {


        $query = "( ";
        $query .= "select si.numberfrom,  si.ipaddress, si.destinationnumber, si.countrycode, cdr.*  ";
        $query .= "from stratos_inmarsat  si  ";
        $query .= "right join cdr  cdr on cdr.sourceid = si.id and cdr.sourcetable = 'stratos_inmarsat' ";
        $query .= "right join simcardperiod s on s.id = cdr.simcardperiod ";
        $query .= "where s.provideraccountid = $_provideraccountid and cdr.qty  > 0  and cdr.sourcetable = 'stratos_inmarsat' ";
        
        $query .= "order by s.serial , s.year desc, s.month desc ) ";
        $query .= "UNION ";
        $query .= "( ";
        $query .= "select si.imsi as numberfrom,  '' as ipaddress , si.callednumber, si.locationareacode as countrycode, cdr.* ";
        $query .= "from tap2030  si  ";
        $query .= "right join cdr  cdr on cdr.sourceid = si.id and cdr.sourcetable = 'tap2030' ";
        $query .= "right join simcardperiod s on s.id = cdr.simcardperiod ";
        $query .= "where s.provideraccountid = $_provideraccountid and cdr.qty  > 0  and cdr.sourcetable = 'tap2030' ";
        
        $query .= "order by s.serial , s.year desc, s.month desc) ";

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {

            array_push($aRecord, $this->getRowToCdr($row));
        }
        return $aRecord;
    }
       

	    public function getCdrsByAccountidGroup($_accountid, $_simcard="", $_voice="", $_data="", $_datebegin, $_dateend ,  $_servicetype ) {

 
        $query.= "select sp.voicenumber, cdr.traffictypedesc, sum(cdr.chargeoutofbundle) as chargeoutofbundle ";
        $query.= "from cdr cdr ";
        $query.= "right join simcardperiod sp on cdr.simcardperiod =  sp.id ";
        $query.= "right join stratos_inmarsat si on si.id = cdr.sourceid ";
        $query.= "where cdr.customeraccountid = $_accountid ";
        
        if($_simcard != ""){
            $query .= " and  sp.serial like '%$_simcard%'   ";
        }
        if($_voice != ""){
            $query .= " and  sp.voicenumber like '%$_voice%'   ";
        }
        if($_data != ""){
            $query .= " and  sp.datanumber like '%$_data%'   ";
        }
        if($_datebegin != ""){
            $query .= " and  ( cdr.cdrdate  between '$_datebegin' and '$_dateend'  )   ";
        }        
        $query .= " and cdr.servicetype = $_servicetype ";
			
        $query .= " group by voicenumber, traffictypedesc  ";
		$query .= "order by sp.voicenumber , cdr.traffictypedesc  ";
         
     
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {

            array_push($aRecord, $this->getRowToCdr($row));
        }        
        
        
        return $aRecord;
    }

	   
    public function getCdrsByAccountid($_accountid, $_simcard="", $_voice="", $_data="", $_datebegin, $_dateend ,  $_servicetype ) {

 
        $query = "select cdr.chargecode, cdr.qtyunitcode, servicetype, cdr.chargeoutofbundle, cdr.chargeinbundle, cdr.qty, cdr.cdrdate, cdr.cdrtime,  ";
        $query.= "cdr.traffictypedesc, cdr.simcardperiod ,  ";
        $query.= "sp.voicenumber, sp.datanumber , ";
        $query.= "si.destinationnumber ";
        $query.= "from cdr cdr ";
        $query.= "right join simcardperiod sp on cdr.simcardperiod =  sp.id ";
        $query.= "right join stratos_inmarsat si on si.id = cdr.sourceid ";
        $query.= "where cdr.customeraccountid = $_accountid ";
        
        if($_simcard != ""){
            $query .= " and  sp.serial like '%$_simcard%'   ";
        }
        if($_voice != ""){
            $query .= " and  sp.voicenumber like '%$_voice%'   ";
        }
        if($_data != ""){
            $query .= " and  sp.datanumber like '%$_data%'   ";
        }
        if($_datebegin != ""){
            $query .= " and  ( cdr.cdrdate  between '$_datebegin' and '$_dateend'  )   ";
        }        
        $query .= " and cdr.servicetype = $_servicetype ";
        $query .= " order by cdr.cdrdate asc, cdr.cdrtime asc ";
        
 //echo $query; die();        
       
     
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {

            array_push($aRecord, $this->getRowToCdr($row));
        }        
        
        
        return $aRecord;
    }
    
    public function getRowToCdr($row) {
        $e = new CdrRecord();
        
        /*
        $query = "select cdr.chargecode, cdr.qtyunitcode, servicetype, cdr.chargeoutofbundle, cdr.chargeinbundle, cdr.qty, ";
        $query.= "cdr.traffictypedesc, cdr.simcardperiod ,  ";
        $query.= "sp.voicenumber, sp.datanumber , ";
        $query.= "si.destinationnumber ";
        $query.= "from cdr cdr ";
        $query.= "right join simcardperiod sp on cdr.simcardperiod =  sp.id ";
        $query.= "right join stratos_inmarsat si on si.id = cdr.sourceid ";
        $query.= "where sp.invoiceid = $_invoiceid ";
        $query.= "order by cdr.cdrdate asc, cdr.cdrtime asc ";        
        */
        
        $e->id = $row["id"];
        $e->serial = $row["serial"];
        
        
        //if($row["datanumber"] != NULL){
          //  $e->numberfrom = $row["datanumber"];
       // }else{
           $e->numberfrom = $row["voicenumber"];
       // }
        
        $e->destinationnumber = $row["destinationnumber"];
        $e->ipaddres = $row["ipaddres"];
        $e->countrycode = $row["countrycode"];
        $e->sourcetable = $row["sourcetable"];
        $e->sourceid = $row["sourceid"];
        $e->cdrdate = $row["cdrdate"];
        $e->cdrtime = $row["cdrtime"];
        $e->numwrdunitcode = $row["numwrdunitcode"];
        $e->bitnumunitcode = $row["bitnumunitcode"];
        $e->traffictypesourcecode = $row["traffictypesourcecode"];
        $e->traffictypedestcode = $row["traffictypedestcode"];
        $e->simcardperiod = $row["simcardperiod"];
        $e->provideraccountid = $row["provideraccountid"];
        $e->planidcrypt = $row["planidcrypt"];
        $e->traffictypecode = $row["traffictypecode"];
        $e->traffictypedesc = utf8_decode($row["traffictypedesc"]);
        $e->numwrd = $row["numwrd"];
        $e->bitnum = $row["bitnum"];
        $e->numwrdup = $row["numwrdup"];
        $e->numwrddown = $row["numwrddown"];
        $e->bitnumup = $row["bitnumup"];
        $e->bitnumdown = $row["bitnumdown"];
        $e->numwrdinbundle = $row["numwrdinbundle"];
        $e->numwrdoutofbundle = $row["numwrdoutofbundle"];
        $e->bitnuminbundle = $row["bitnuminbundle"];
        $e->bitnumoutofbundle = $row["bitnumoutofbundle"];
        $e->chargeinbundle = $row["chargeinbundle"];
        $e->chargeoutofbundle = $row["chargeoutofbundle"];
        
        $e->qtyunitcode = trim($row["qtyunitcode"]);
        $e->chargecode = trim($row["chargecode"]);
        
        $e->qty = doubleval($row["qty"]);
        
        $e->servicetype = intval($row["servicetype"]);
        
        return $e;
    }

}
