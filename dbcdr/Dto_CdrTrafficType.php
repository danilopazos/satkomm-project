<?php

class Dto_CdrTrafficType extends Dto_CdrDb {

    public function getCdrTrafficTypeByCode($_code, $_provider, $_network, $_ttgroup) {

        $query = "";

        if ($_provider == "STRATOS" && $_network == "INMARSAT") {
            $query = "select ta.id, ta.provider, ta.network, ta.code, ta.ttgroup, ";
            $query .= "  ta.servicecode, s.description as servicecode_desc, ";
            $query .= "  ta.calltypecode , ct.description as  calltypecode_desc , ta.cdkind, ck.product as cdkind_desc , ";
            $query .= " ta.oceanregioncode , ocr.description as oceanregioncode_desc , ta.destinationzone, ta.dateregistered ";
            $query .= " from traffictypeassoc ta ";
            $query .= " left join stratos_inmarsat_calltype ct ON ct.code = ta.calltypecode ";
            $query .= " left join stratos_inmarsat_cdkind ck ON ck.cdkind = ta.cdkind ";
            $query .= " left join stratos_inmarsat_service s ON s.code = ta.servicecode ";
            $query .= " left join stratos_inmarsat_oceanregion ocr ON ocr.code = ta.oceanregioncode ";
            $query .= "where ta.deleted = 0 AND ta.code = '$_code' AND ta.network = '$_network' AND ta.provider = '$_provider' AND ta.ttgroup = '$_ttgroup' order by   ta.cdkind asc ";
        }
        
        if ($_provider == "IRIDIUM" && $_network == "IRIDIUM") {
            $query = "select ta.id, ta.provider, ta.network, ta.code,  ta.ttgroup, ";
            $query .= "  ta.servicecode, ";
            $query .= "  ta.calltypecode ,   ta.cdkind,  ";
            $query .= " ta.oceanregioncode ,   ta.destinationzone, ta.dateregistered ";
            $query .= " from traffictypeassoc ta ";
            $query .= "where ta.deleted = 0 AND ta.code = '$_code' AND ta.network = '$_network' AND ta.provider = '$_provider' AND ta.ttgroup = '$_ttgroup'  order by   ta.cdkind asc ";
        } 
        
        if ($_provider == "STRATOS" && $_network == "IRIDIUM") {
            $query = "select ta.id, ta.provider, ta.network, ta.code,  ta.ttgroup, ";
            $query .= "  ta.servicecode, s.description as servicecode_desc, ";
            $query .= "  ta.calltypecode , ct.description as  calltypecode_desc , ta.cdkind, ck.product as cdkind_desc , ";
            $query .= " ta.oceanregioncode , ocr.description as oceanregioncode_desc , ta.destinationzone, ta.dateregistered ";
            $query .= " from traffictypeassoc ta ";
            $query .= " left join stratos_iridium_calltype ct ON ct.code = ta.calltypecode ";
            $query .= " left join stratos_iridium_destinationzone s ON s.code = ta.destinationzone ";
            $query .= " left join stratos_iridium_oceanregion ocr ON ocr.code = ta.oceanregioncode ";
            $query .= "where ta.deleted = 0 AND ta.code = '$_code' AND ta.network = '$_network' AND ta.provider = '$_provider' AND ta.ttgroup = '$_ttgroup'  order by   ta.cdkind asc ";
        }


        $_rows = $this->selectRows($query);
        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();

        $_tempcdking = "";


        if (
                ($_provider == "STRATOS" && $_network == "INMARSAT") ||
                ($_provider == "STRATOS" && $_network == "IRIDIUM")
        ) {

            foreach ($_rows as $_row) {
                $e = new CdrTrafficType();
                $e->id = trim($_row["id"]);
                $e->code = trim($_row["code"]);
                $e->network = trim($_row["network"]);
                $e->provider = trim($_row["provider"]);
                $e->servicecode = trim($_row["servicecode"]);
                $e->servicecode_desc = trim($_row["servicecode_desc"]);
                $e->calltypecode = trim($_row["calltypecode"]);
                $e->calltypecode_desc = trim($_row["calltypecode_desc"]);
                $e->cdkind = trim($_row["cdkind"]);
                $e->cdkind_desc = trim($_row["cdkind_desc"]);
                $e->oceanregioncode = trim($_row["oceanregioncode"]);
                $e->oceanregioncode_desc = trim($_row["oceanregioncode_desc"]);
                $e->destinationzone = trim($_row["destinationzone"]);
                $e->destinationzone_desc = trim($_row["destinationzone_desc"]);
                $e->dateregistered = trim($_row["dateregistered"]);
                $e->ttgroup = trim($_row["ttgroup"]);
                
                
                if ($e->cdkind != $_tempcdking) {
                    array_push($aRecord, $e);
                }

                $_tempcdking = $e->cdkind;
            }//foreach
        } else {

             foreach ($_rows as $_row) {
                $e = new CdrTrafficType();
                $e->id = trim($_row["id"]);
                $e->code = trim($_row["code"]);
                $e->network = trim($_row["network"]);
                $e->provider = trim($_row["provider"]);
                $e->servicecode = trim($_row["servicecode"]);
                $e->servicecode_desc = trim($_row["servicecode_desc"]);
                $e->calltypecode = trim($_row["calltypecode"]);
                $e->calltypecode_desc = trim($_row["calltypecode_desc"]);
                $e->cdkind = trim($_row["cdkind"]);
                $e->cdkind_desc = trim($_row["cdkind_desc"]);
                $e->oceanregioncode = trim($_row["oceanregioncode"]);
                $e->oceanregioncode_desc = trim($_row["oceanregioncode_desc"]);
                $e->destinationzone = trim($_row["destinationzone"]);
                $e->destinationzone_desc = trim($_row["destinationzone_desc"]);
                $e->dateregistered = trim($_row["dateregistered"]);
                $e->ttgroup = trim($_row["ttgroup"]);
                array_push($aRecord, $e);
             }//foreach
        }

        return $aRecord;
    }

    public function insertTrafficTypeCode($cdrTrafficType) {

        $_code = $cdrTrafficType->code;
        $_provider = $cdrTrafficType->provider;
        $_network = $cdrTrafficType->network;
        $_servicecode = $cdrTrafficType->servicecode;
        $_calltypecode = $cdrTrafficType->calltypecode;
        $_cdkind = $cdrTrafficType->cdkind;
        $_oceanregioncode = $cdrTrafficType->oceanregioncode;
        $_destinationzone = $cdrTrafficType->destinationzone;
        $_dateregistered = $cdrTrafficType->dateregistered;
        $_description = $cdrTrafficType->description;
        $_ttgroup = $cdrTrafficType->ttgroup;

        $query = "INSERT into traffictypeassoc(  code, provider, network, servicecode, calltypecode, cdkind, oceanregioncode, destinationzone, dateregistered, description , ttgroup )";
        $query .= " VALUES ('$_code' , '$_provider' , '$_network' , '$_servicecode' , '$_calltypecode' , '$_cdkind' , '$_oceanregioncode' , '$_destinationzone' , CURRENT_TIMESTAMP  , '$_description' , '$_ttgroup' )  ";


        return $this->executeUpdate($query);
    }

    public function deleteTrafficTypeCode($_id) {

        $query = "update  traffictypeassoc set deleted = 1 where id = $_id ";

        return $this->executeUpdate($query);
    }

    public function getStratosInmarsatService() {
        $query = "select code, description from stratos_inmarsat_service order by description asc";
        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {
            $e = new CodeDesc();
            $e->code = trim($_row["code"]);
            $e->description = trim($_row["description"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getStratosInmarsatCalltype() {
        $query = "select code, description from stratos_inmarsat_calltype order by description asc";
        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {
            $e = new CodeDesc();
            $e->code = trim($_row["code"]);
            $e->description = trim($_row["description"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getStratosInmarsatCdkind() {
        $query = "select cdkind as code, product as description from stratos_inmarsat_cdkind group by code, description order by product asc ";
        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {
            $e = new CodeDesc();
            $e->code = trim($_row["code"]);
            $e->description = trim($_row["description"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getStratosInmarsatOceanRegion() {
        $query = "select code, description from stratos_inmarsat_oceanregion order by description asc ";
        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {
            $e = new CodeDesc();
            $e->code = trim($_row["code"]);
            $e->description = trim($_row["description"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getStratosIridiumCallType() {
        $query = "select code, description from stratos_iridium_calltype order by description asc";
        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {
            $e = new CodeDesc();
            $e->code = trim($_row["code"]);
            $e->description = trim($_row["description"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getStratosIridiumDestinationZone() {
        $query = "select code, description from stratos_iridium_destinationzone order by description asc ;";
        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {
            $e = new CodeDesc();
            $e->code = trim($_row["code"]);
            $e->description = trim($_row["description"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getStratosIridiumOceanRegion() {
        $query = "select code, description from stratos_iridium_oceanregion order by description asc ";
        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {
            $e = new CodeDesc();
            $e->code = trim($_row["code"]);
            $e->description = trim($_row["description"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

}
