<?php

class Dto_CdrDb {

    public $link;

    public function connect($typeAction = "") {
        $config = new ConfigCdr();
       
        
        
        //conectarse a una base de datos llamada "mary" en el host "sheep" con el nombre de usuario y password

        $conn_string = "host=".$config->main_dbhost." port=".$config->main_dbport." dbname=".$config->main_dbname." user=".$config->main_dbuser." password=".$config->main_dbpass."";
        
        // echo $conn_string; die();

        //$this->link = pg_connect($config->main_dbhost . ":" . $config->main_dbport, $config->main_dbuser, $config->main_dbpass);
       $this->link = pg_connect(  $conn_string  );

	/*	
	if( $this->link  ){
		echo "Si conecta"; die();
	}else{
		echo "No conecta"; die();
	}         
	*/       

        //_select_db($config->main_dbname, $this->link);
    }

    public function close() {
        pg_close($this->link);
    }
 

    public function selectRows($query) {
        $this->connect();

        $data = pg_query($this->link , $query );
        
        
        
        $aList = array();

        if (!$data) {
            return false;
        } else {

            if (pg_num_rows($data) > 0) {

                while ($item = pg_fetch_array($data)) {

                    array_push($aList, $item);
                }//while
            } else {
                return false;
            }//else
        }//else

        $this->close();

        return $aList;
    }

    public function selectOneRow($query) {

        $this->connect();
        $data = pg_query($this->link , $query );
        $aList = array();

        if (!$data) {
            return false;
        } else {

            if (pg_num_rows($data) > 0) {

                $aList = pg_fetch_array($data);
            } else {
                return false;
            }//else
        }//else

        $this->close();

        return $aList;
    }

    public function selectOneColumn($column, $query) {
        $this->connect();
        $data = pg_query($this->link , $query );

        $value = null;

        if (!$data) {
            return false;
        } else {

            if (pg_num_rows($data) > 0) {

                $item = pg_fetch_array($data);

                $value = $item[$column];
            } else {
                return false;
            }//else
        }//else

        $this->close();

        return $value;
    }

    public function executeUpdate($query) {

        $this->connect("update");

        $ok = false;
        if (pg_query($this->link , $query )) {
            $ok = true;
        } else {
            $ok = false;
        }

        $this->close();

        return $ok;
    }

    
    
    public function getDateByTimeZone($_date){
        //America/New_York	
        if( $_SESSION["session_timezonename"] == ""){
            $_SESSION["session_timezonename"] = "America/New_York";
        }
        if($_date !=""){
            $UTC = new DateTimeZone("UTC");
            $newTZ = new DateTimeZone($_SESSION["session_timezonename"]);
            $date = new DateTime( $_date , $UTC );
            $date->setTimezone( $newTZ );
            return $date->format('Y-m-d H:i:s'); 
        }else{
            return "";
        }
    }

}
