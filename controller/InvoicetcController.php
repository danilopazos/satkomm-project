<?php
require_once ("../lib/bean/AccountConfig.php");
require_once ("../lib/bean/InvoiceToCustomer.php");
require_once ("../lib/bean/InventoryItemSimple.php");
require_once ("../lib/bean/InventoryItem.php");

require_once ("../lib/bean/InvoicetcDispatchService.php");
require_once ("../lib/bean/InvoicetcDispatchDevice.php");
//----------------------------------------------------------

require_once ("../lib/bean/Currency.php");
require_once ("../lib/bean/CurrencyExchange.php");
require_once ("../lib/bean/Quote.php");
require_once ("../lib/bean/Plan.php");
require_once ("../lib/bean/PlanTrafficType.php");
require_once ("../lib/bean/DocumentProduct.php");
require_once ("../lib/bean/DocumentService.php");
require_once ("../lib/bean/ShippingProvider.php");
require_once ("../lib/bean/ItemAutocomplete.php");


require_once ("../lib/bean/HeadDocumentExport.php");
require_once ("../lib/bean/DetailProductExport.php");

require_once ("../lib/db/Dto_Currency.php");
require_once ("../lib/db/Dto_Inventory.php");
require_once ("../lib/db/Dto_Plan.php");
require_once ("../lib/db/Dto_Document.php");
require_once ("../lib/db/Dto_Quote.php");
require_once ("../lib/db/Dto_ShippingProvider.php");
require_once ("../lib/db/Dto_InvoiceToCustomer.php");
require_once ("../lib/helper/DatatableHelper.php");

require_once ("../lib/controller/DocumentemailController.php");
require_once ("../lib/controller/MediaController.php");
require_once ("../lib/controller/DocumentgenerateController.php");

class InvoicetcController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function dispatchitemsAction() {
        
        $aDispatchDevice  = array();
        $aDispatchService = array();
        
        
        
        $invoiceid = trim( $this->aRequest["invoiceid"]  );
        
        $aTempServices = trim($_REQUEST["aservices"]);
        $aTempDevices = trim($_REQUEST["adevices"]);

        $aJsonServices = json_decode($aTempServices, true);
        $aJsonDevices = json_decode($aTempDevices, true);

        //echo var_dump( $aJsonDevices  );         die();

        $total_services = count($aJsonServices);
        $total_devices = count($aJsonDevices);
  
        $dto_invoice = new Dto_InvoiceToCustomer();
        $tempInvoice = $dto_invoice->getByIdCrypt($invoiceid);
        
        $aInventory = array();
        if ($total_services > 0) {
            foreach ($aJsonServices as $service) {
                
                $item = new InventoryItem();
                $item->id = intval($service["simcarditemid"]);
                $item->customerinvoiceid = $tempInvoice->id;
                $item->inventorystatus = 2;
                $item->operationdate = $service["activationdate"];
                $item->operationtime = $this->getTimeFormat($service["activationtime"]);
                $item->statusdescription = $service["operationdesc"];
                $item->actionuserid = $this->session_userid;
                 
                array_push($aInventory, $item);
                
                //-------We put the object dispatch service------------------------
                $dispatchService = new InvoicetcDispatchService();
                $dispatchService->invoicetcid = $invoiceid;
                $dispatchService->simcarditemid = intval($service["simcarditemid"]);
                $dispatchService->simcardserial = '';
                
                $dispatchService->planforaccount = $service["planforaccount"];
                $dispatchService->planforprovider = $service["planforprovider"];
                $dispatchService->description = $service["operationdesc"];
                $dispatchService->deviceitemid  = intval($service["deviceitemid"]);
                $dispatchService->deviceserial = '';
                $dispatchService->userid = $this->session_userid;
                
                $dispatchService->activationdate = $service["activationdate"];
                $dispatchService->activationtime = $this->getTimeFormat($service["activationtime"]);
                array_push($aDispatchService, $dispatchService);
                //------------------------------------------------------------------
                
                
                if(intval($service["deviceitemid"]) > 0){
                        
                    $item = new InventoryItem();
                    $item->id = intval($service["deviceitemid"]);
                    $item->customerinvoiceid = $tempInvoice->id;
                    $item->inventorystatus = 2;
                    $item->atsimcardid = intval($service["simcarditemid"]); 
                    $item->atstatus = 1;
                    $item->actionuserid = $this->session_userid;
                    array_push($aInventory, $item);
                }//if
                
                 
            }//foreach
        }//if
        
        if($total_devices > 0 ){
            foreach ($aJsonDevices as $device) {
                
                $item = new InventoryItem();
                $item->id = intval($device["productitemid"]);
                $item->customerinvoiceid = $tempInvoice->id;
                $item->inventorystatus = 2;
                $item->statusdescription = $device["productdesc"];
                 
                
                $found = false;
                $totalitems = count($aInventory);
                if($totalitems > 0 ){
                    foreach( $aInventory as $inventory  ){
                        if($inventory->id == $item->id){  $found = true;  } 
                    }
                }//if
                
                if(!$found){
                    array_push($aInventory, $item);
                }
                
                
                $dispatchDevice= new InvoicetcDispatchDevice();
                $dispatchDevice->invoicetcid = $invoiceid;
                $dispatchDevice->description = $device["operationdesc"];
                $dispatchDevice->deviceitemid  = intval($device["productitemid"]);
                $dispatchDevice->deviceserial = '';
                $dispatchDevice->userid = $this->session_userid;
                 
                array_push($aDispatchDevice, $dispatchDevice);
                 
                
            }//foreach
        }//if
        
        $_ok = false;
        
        $_ok = $dto_invoice->insertInvoiceDispatchDevices($aDispatchDevice);
        
        $_ok = $dto_invoice->insertInvoiceDispatchServices($this->session_accountid, $tempInvoice->customerid , $aDispatchService);
        
        $dto_invoice->updateInventoryItem($aInventory); 
        $dto_invoice->setInvoicetcDispatched( $tempInvoice->id  );
        
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $_ok );
        $view->load("template", "json");
         
    }

//function

    function requestonlydevicesAction() {
        $_productidcrypt = trim($this->aRequest["productidcrypt"]);

        $_invoiceid = trim($this->aRequest["invoiceid"]);


        $dto_invoice = new Dto_InvoiceToCustomer();

        $aDevices = $dto_invoice->getDevicesByProductId($this->session_accountid, $_invoiceid, $_productidcrypt);


        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("devices" => $aDevices);
        $view->load("template", "json");
    }

    function requestsimcardsanddevicesAction() {

        $_planidcrypt = trim($this->aRequest["planidcrypt"]);
        $_customerid = intval($this->aRequest["customerid"]);
        
        $_parentplanidcrypt = trim($this->aRequest["parentplanidcrypt"]);
        $_invoiceid = trim($this->aRequest["invoiceid"]);

        
        $_customeraccountid = trim($this->aRequest["customeraccountid"]);




        $dto_invoice = new Dto_InvoiceToCustomer();

        $aSimcards = $dto_invoice->getSimcardAssocToService($this->session_accountid, $_customerid, $_invoiceid, $_planidcrypt, $_parentplanidcrypt);
        $aDevices = $dto_invoice->getDevicesAssocToService($this->session_accountid, $_invoiceid, $_planidcrypt, $_parentplanidcrypt);


        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("simcards" => $aSimcards, "devices" => $aDevices);
        $view->load("template", "json");
    }

    function indexAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/invoicetc/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("invoicetc", "index");
    }

    function viewAction() {

        $docidcrypt = trim($this->aRequest["id"]);
        $dto_entity = new Dto_InvoiceToCustomer();
        $invoicetc = $dto_entity->getByIdCrypt($docidcrypt);
        $aProducts = $dto_entity->getProductsByDocumentId($docidcrypt, 'invoicetc');
        $aServices = $dto_entity->getServicesByDocumentId($docidcrypt, 'invoicetc');


        $view = new ViewController();

        //------------------------
  
        $dto_account = new Dto_Account();
        $e_account = $dto_account->getById( $this->session_accountid  );
        $view->e_subject = $e_account->name.' -  Factura # '.$invoicetc->invoiceno.' '.$this->session_firstname.' '.$this->session_lastname ;
        $view->e_attach = $invoicetc->invoiceno.'.pdf';
        $view->e_from = $this->session_username;
        $view->e_fromname = $this->session_firstname.' '.$this->session_lastname;
        $view->e_to = $invoicetc->contactemail;
         
        //---------------------------
        
        
        
        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aCss, "assets/plugin/bootstrap-timepicker.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-timepicker/css/timepicker.less");


        array_push($view->aJavascript, "assets/bower_components/json2/json2.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/invoicetc/view.js");
        
        if($invoicetc->dispatched == 0){
            array_push($view->aJavascript, "assets/js/modules/invoicetc/dispatch.js");
            $view->bodyjquery = "Invoicetc.init();  Dispatch.init(); ";
             
        }else{
            
            $view->aDispatchedServices = $dto_entity->getDispatchedServices($docidcrypt);
            $view->aDispatchedDevices  = $dto_entity->getDispatchedDevices($docidcrypt);
            
            $view->bodyjquery = "Invoicetc.init();";
        }

        $view->invoicetc = $invoicetc;
        $view->aProduct = $aProducts;
        $view->aService = $aServices;
        
        $view->load("invoicetc", "view");
    }

    function requestdeleteAction() {

        $_idcrypt = trim($_REQUEST["id"]);

        $dto_entity = new Dto_InvoiceToCustomer();
        $_result = $dto_entity->delete($_idcrypt, $this->session_userid);

        $ok = 0;
        if ($_result) {
            $ok = 1;
        }
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $ok);
        $view->load("template", "json");
    }

    function registerAction() {
        
        $dto_account = new Dto_Account();
        $dto_entity = new Dto_InvoiceToCustomer();
        $dto_user = new Dto_User();

        $entity = new InvoiceToCustomer();


        $view = new ViewController();
        
        $dto_currency = new Dto_Currency();
        $view->aCurrency = $dto_currency->getListActive($this->session_accountid);    
         
        
        $view->accountconfig = $dto_account->getAccountConfig($this->session_accountid);
        $view->aStatus = array('C', 'P', 'D');
        $view->aSalesman = $dto_user->getByAccountid($this->session_accountid);
        $view->aPaymentTerms = array("prepaid", "15", "30", "45", "60", "90", "00");
        
        $view->aDeliveryTerms = array("pickupinoffice", "sendingbycurier", "shippingnotincluded", "other" );
        
        $dto_sp = new Dto_ShippingProvider();
        $view->aCarrierCompany = $dto_sp->getAll();
        $view->aOperationType = array("S", "R");
        $view->aHasTaxes = array(1, 0);
 
        array_push($view->aCss, "assets/bower_components/jquery-autocomplete/jquery-ui.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aCss, "assets/bower_components/jquery-autocomplete/style.css");
        array_push($view->aCss, "assets/bower_components/select2/select2.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css");
 
        array_push($view->aJavascript, "assets/js/modules/saleorder/index.js");
        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");

        array_push($view->aJavascript, "assets/bower_components/jquery-autocomplete/jquery.mockjax.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-autocomplete/jquery.autocomplete.js");
        array_push($view->aJavascript, "assets/bower_components/select2/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js");

        array_push($view->aJavascript, "assets/js/modules/invoicetc/register.js");

        $view->bodyjquery = "Invoicetc.init(); Register.init();  ";

        $basedonquoteidcrypt = trim($this->aRequest["basedonquote"]);
        $docidcrypt = trim($this->aRequest["id"]);
        if ($docidcrypt != "") {
            $view->invoicetc = $dto_entity->getByIdCrypt($docidcrypt);
            $view->aProduct = $dto_entity->getProductsByDocumentId($docidcrypt, 'invoicetc');
            $view->aService = $dto_entity->getServicesByDocumentId($docidcrypt, 'invoicetc');

            $dto_user = new Dto_User();
            $view->aContact = $dto_user->getByAccountid($view->invoicetc->customerid);
        } else if ($basedonquoteidcrypt != "") {
            $dto_quotetemp = new Dto_Quote();
            $quoteEnt = $dto_quotetemp->getByIdCrypt($basedonquoteidcrypt);

            $view->invoicetc = new InvoiceToCustomer();
            $view->invoicetc = $this->parseQuoteToInvoice($quoteEnt);


            $view->aProduct = $dto_entity->getProductsByDocumentId($basedonquoteidcrypt, 'quote');
            $view->aService = $dto_entity->getServicesByDocumentId($basedonquoteidcrypt, 'quote');


            $dto_user = new Dto_User();
            $view->aContact = $dto_user->getByAccountid($view->invoicetc->customerid);

            $view->invoicetc->invoiceno = $dto_entity->generateInvoiceNo($view->accountconfig->invoiceprefix, $this->session_accountid);
            $view->invoicetc->assignedto = $this->session_userid;
        } else {
            $entity->invoiceno = $dto_entity->generateInvoiceNo($view->accountconfig->invoiceprefix, $this->session_accountid);
            $entity->assignedto = $this->session_userid;
            $entity->hastaxes = 1;
            $entity->taxespercent = 18;
             
            $entity->taxespercent = doubleval($view->accountconfig->conftaxespercent);            
            
            $view->invoicetc = $entity;
        }

        $view->load("invoicetc", "register");
    }

    function parseQuoteToInvoice($quote) {
        $invoice = new InvoiceToCustomer();
        $invoice->subject = $quote->subject;
        $invoice->invoicedate = $quote->quotedate;
        $invoice->invoiceduedate = $quote->quoteuntil;
        $invoice->quoteidcrypt = $quote->idcrypt;
        $invoice->quotedesc = stripslashes($quote->quoteno) . " - " . stripslashes($quote->subject);
        $invoice->assignedto = $quote->assignedto;
        $invoice->customer_name = $quote->customer_name;
        $invoice->customerid = $quote->customerid;
        $invoice->contactid = $quote->contactid;
        $invoice->contacttelephone = $quote->contacttelephone;
        $invoice->contactmobile = $quote->contactmobile;
        $invoice->contactemail = $quote->contactemail;
        $invoice->contactsalutation = $quote->contactsalutation;
        $invoice->paymentterms = $quote->paymentterms;
        $invoice->paymenttermsother = $quote->paymenttermsother;
        $invoice->carriercompany = $quote->carriercompany;
        $invoice->carriercompanyother = $quote->carriercompanyother;
        $invoice->carriertracking = $quote->carriertracking;
        $invoice->operationtype = $quote->operationtype;
        $invoice->shippingenabled = $quote->shippingenabled;
        $invoice->discount = $quote->discount;
        $invoice->hastaxes = $quote->hastaxes;
        $invoice->subtotal = $quote->subtotal;
        $invoice->shippingcost = $quote->shippingcost;
        $invoice->taxespercent = $quote->taxespercent;
        $invoice->total = $quote->total;
        $invoice->obs = $quote->obs;

        $invoice->deliveryterms = $quote->deliveryterms;
        $invoice->deliverytermsother = $quote->deliverytermsother;
        
        $invoice->bill_country = $quote->bill_country;
        $invoice->bill_state = $quote->bill_state;
        $invoice->bill_city = $quote->bill_city;
        $invoice->bill_street = $quote->bill_street;
        $invoice->bill_zipcode = $quote->bill_zipcode;
        $invoice->bill_pobox = $quote->bill_pobox;

        $invoice->ship_country = $quote->ship_country;
        $invoice->ship_state = $quote->ship_state;
        $invoice->ship_city = $quote->ship_city;
        $invoice->ship_street = $quote->ship_street;
        $invoice->ship_zipcode = $quote->ship_zipcode;
        $invoice->ship_pobox = $quote->ship_pobox;

        $invoice->currency_code = $quote->currency_code;
        $invoice->currency_exchange = $quote->currency_exchange; 
        $invoice->totalusd = $quote->totalusd; 
        
        return $invoice;
    }

    function requestlistAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('q.invoiceno', 'q.operationtype', 'ac.name', 'q.status', 'q.subject', 'asi.firstname', 'q.invoicedate', 'q.invoiceduedate', 'q.total');
        $_datatableSetup->_sortCol = "q.id";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_InvoiceToCustomer();
        $totalRecords = $dto_entity->getListTotal($this->session_accountid, $datatable->_search);
        $aRecords = $dto_entity->getList($this->session_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array("sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array());

        $fileLangTemplate = "../lib/view/invoicetc/lang/" . strtolower($this->session_langcode) . ".php";
        if (file_exists($fileLangTemplate)) {
            include $fileLangTemplate;
        }

        if ($aRecords) {
            foreach ($aRecords as $record) {

                $total = $record->total ;


                $output['aaData'][] = array(
                    $record->invoiceno, $_lang_aoperationtype[$record->operationtype], $record->customer_name, $_lang_quotestatus[$record->status], $record->subject,
                    $record->assigned_names, $record->invoicedate, $record->invoiceduedate, $record->currency_code.' '.$total,
                    '<a class="btn btn-xs btn-primary" href="../invoicetc/view/id/' . $record->idcrypt . '"><i class="fa fa-folder"></i></a>'
                );
            }
        }


        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestgetproductbyidAction() {
        $_idcrypt = trim($this->aRequest["idcrypt"]);

        $dto_product = new Dto_Product();
        $entity = $dto_product->getDetailById($this->session_langcode, $_idcrypt);

        $view = new ViewController();
        $view->data = $entity;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function getproductsrequestAction() {

        $_type = trim($_REQUEST["type"]);
        $_search = trim($_REQUEST["name_startsWith"]);

        $_operation = trim($_REQUEST["operationtype"]);
        if ($_operation == "") {
            $_operation = 'S';
        }
       
        $_customeraccountid = intval($_REQUEST["customeraccountid"]);
        
        $dto_entity = new Dto_InvoiceToCustomer();
        $aRecord = array();

        if ($_type == "product") {
           
            $aRecord = $dto_entity->getProducts($this->session_accountid, $_customeraccountid , $_operation, strtoupper($this->session_langcode), $_search);
        } else if ($_type == "service") {
             
            $_productlist = trim($_REQUEST["productlist"]);
         
            $aRecord = $dto_entity->getServices($this->session_accountid, $_customeraccountid ,  $_operation, strtoupper($this->session_langcode), $_search, $_productlist);
        }

        $view = new ViewController();
        $view->data = $aRecord;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function getcontactsAction() {
        $_accountid = intval($this->aRequest["accountid"]);
        $dto_user = new Dto_User();
        $aRecord = $dto_user->getByAccountid($_accountid);

        $view = new ViewController();
        $view->data = $aRecord;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function getcustomerAction() {

        $_search = trim($_REQUEST["name_startsWith"]);

        $dto_account = new Dto_Account();
        $aRecord = $dto_account->getListByName($this->session_accountid, $_search, "a.id", "asc", 0, 100);

        $view = new ViewController();
        $view->data = $aRecord;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function getquotebysubjectAction() {
        $_search = trim($_REQUEST["name_startsWith"]);

        $dto_quote = new Dto_Quote();
        $aRecord = $dto_quote->getListBySubject($this->session_accountid, $_search, "q.quoteno", "asc", 0, 100);

        $view = new ViewController();
        $view->data = $aRecord;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function requestregisterAction() {

        $_e = $this->getDocumentRequest();

        $_action = "create";
        if ($_e->idcrypt != "") {
            $_action = "update";
        } else {
            $_e->idcrypt = md5(date('YmdHis')) . '' . rand(10000, 99999) . '' . rand(10000, 99999);
        }
        $ok = 1;

        if ($_e->customerid == "" || $_e->contactid == "" || $_e->subject == "") {
            $ok = -1;
        }


        $_result = false;
        $_resultproducts = false;
        $_resultservices = false;
        if ($ok == 1) {
            $dto_entity = new Dto_InvoiceToCustomer();
            if ($_action == "create") {
                $_result = $dto_entity->register($_e);
            } else {
                $_result = $dto_entity->update($_e);
            }
            $dto_entity->changeStatusOfOtherDocuments($_e, $this->session_userid);


            if ($_result) {
                //we need to update the items
                $_resultproducts = $dto_entity->updateProducts($_e->idcrypt, $_e->aProduct);
                $_resultservices = $dto_entity->updateServices($_e->idcrypt, $_e->aService);
            }
        }


        if ($_result && $_resultproducts && $_resultservices) {
            $ok = 1;
        }


        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $ok, "id" => $_e->idcrypt);
        $view->load("template", "json");
    }

    function getDocumentRequest() {
        $_idcrypt = trim($_REQUEST["id"]);

        $_quoteidcrypt = trim($_REQUEST["quoteid"]);
        $_code = trim($_REQUEST["code"]);
        $_status = trim($_REQUEST["status"]);
        $_subject = trim($_REQUEST["subject"]);
        $_salesman = trim($_REQUEST["salesman"]);
        $_quotedate = trim($_REQUEST["invoicedate"]);
        $_quoteuntil = trim($_REQUEST["invoiceduedate"]);
        $_customerid = trim($_REQUEST["customerid"]);
        $_contactid = trim($_REQUEST["contactid"]);
        $_contacttelephone = trim($_REQUEST["contacttelephone"]);
        $_contactmobile = trim($_REQUEST["contactmobile"]);
        $_contactemail = trim($_REQUEST["contactemail"]);
        $_contactsalutation = trim($_REQUEST["contactsalutation"]);
        $_paymentterms = trim($_REQUEST["paymentterms"]);
        $_paymenttermsother = trim($_REQUEST["paymenttermsother"]);
        $_carriercompany = trim($_REQUEST["carriercompany"]);
        $_carriercompanyother = trim($_REQUEST["carriercompanyother"]);
        $_carriertracking = trim($_REQUEST["carriertracking"]);
        $_obs = trim($_REQUEST["obs"]);
        $_bill_country = trim($_REQUEST["bill_country"]);
        $_bill_state = trim($_REQUEST["bill_state"]);
        $_bill_city = trim($_REQUEST["bill_city"]);
        $_bill_street = trim($_REQUEST["bill_street"]);
        $_bill_zipcode = trim($_REQUEST["bill_zipcode"]);
        $_bill_pobox = trim($_REQUEST["bill_pobox"]);
        $_ship_country = trim($_REQUEST["ship_country"]);
        $_ship_state = trim($_REQUEST["ship_state"]);
        $_ship_city = trim($_REQUEST["ship_city"]);
        $_ship_street = trim($_REQUEST["ship_street"]);
        $_ship_zipcode = trim($_REQUEST["ship_zipcode"]);
        $_ship_pobox = trim($_REQUEST["ship_pobox"]);

        $_deliveryterms = trim($_REQUEST["deliveryterms"]);
        $_deliverytermsother = trim($_REQUEST["deliverytermsother"]);

        $_purchaseorderno = trim($_REQUEST["purchaseorderno"]);

        $discount = doubleval($_REQUEST["discount"]);
        $hastaxes = doubleval($_REQUEST["hastaxes"]);
        $subtotal = doubleval($_REQUEST["subtotal"]);
        $shippingcost = doubleval($_REQUEST["shippingcost"]);
        $taxespercent = doubleval($_REQUEST["taxespercent"]);
        $operationtype = trim($_REQUEST["operationtype"]);
        $total = doubleval($_REQUEST["total"]);

        $_deliveryterms = trim($_REQUEST["deliveryterms"]);
        $_deliverytermsother = trim($_REQUEST["deliverytermsother"]);
        
        if (trim($_REQUEST["quotesubject"]) == "") {
            $_quoteidcrypt = "";
        }

        if($_status ==""){
            $_status = "C";
        }        
        
        
        $_acurrency_code = trim($_REQUEST["currency_code"]);
        $_currency_exchange = doubleval($_REQUEST["currency_exchange"]);
        
        $SplitCurrencyCode = split("-_-", $_acurrency_code);
        $_currency_code = trim($SplitCurrencyCode[0]);
                
        
        $e = new InvoiceToCustomer();
        $e->idcrypt = $_idcrypt;

        $e->quoteidcrypt = $_quoteidcrypt;
        $e->code = $_code;
        $e->status = $_status;
        $e->subject = $_subject;
        $e->assignedto = $_salesman;
        $e->invoicedate = $_quotedate;
        $e->invoiceduedate = $_quoteuntil;
        $e->purchaseorderno = $_purchaseorderno;
        $e->customerid = $_customerid;
        $e->contactid = $_contactid;
        $e->contacttelephone = $_contacttelephone;
        $e->contactmobile = $_contactmobile;
        $e->contactemail = $_contactemail;
        $e->contactsalutation = $_contactsalutation;
        $e->paymentterms = $_paymentterms;
        $e->paymenttermsother = $_paymenttermsother;
        $e->carriercompany = $_carriercompany;
        $e->carriercompanyother = $_carriercompanyother;
        $e->carriertracking = $_carriertracking;
        $e->obs = $_obs;
        $e->operationtype = $operationtype;
        $e->accountid = $this->session_accountid;
        $e->shippingenabled = $_shippingenabled;
        
        $e->deliveryterms = $_deliveryterms;
        $e->deliverytermsother = $_deliverytermsother;
        
        $e->bill_country = $_bill_country;
        $e->bill_state = $_bill_state;
        $e->bill_city = $_bill_city;
        $e->bill_street = $_bill_street;
        $e->bill_zipcode = $_bill_zipcode;
        $e->bill_pobox = $_bill_pobox;

        $e->ship_country = $_ship_country;
        $e->ship_state = $_ship_state;
        $e->ship_city = $_ship_city;
        $e->ship_street = $_ship_street;
        $e->ship_zipcode = $_ship_zipcode;
        $e->ship_pobox = $_ship_pobox;

        $e->discount = $discount;
        $e->hastaxes = $hastaxes;
        $e->subtotal = $subtotal;
        $e->shippingcost = $shippingcost;
        $e->taxespercent = $taxespercent;
        $e->total = $total;

        $e->createdby = $this->session_userid;
        $e->modifiedby = $this->session_userid;
        $e->deletedby = $this->session_userid;

        $e->deliveryterms = $_deliveryterms;
        $e->deliverytermsother = $_deliverytermsother;
        
        $e->currency_code = $_currency_code;
        $e->currency_exchange = $_currency_exchange;        
        
        $_shippingenabled = "off";
        if (trim($_REQUEST["controlControlShipping"]) == "on") {
            $_shippingenabled = "on";
        }
        $e->shippingenabled = $_shippingenabled;

        //we need to setup de the products
        $aProduct = array();
        $itemindex = 1;
        if ($_REQUEST["productrowid"] != null) {
            $totalProduct = count($_REQUEST["productrowid"]);
            if ($totalProduct > 0) {
                foreach ($_REQUEST["productrowid"] as $_rowid) {
                    $_pq = new DocumentProduct();
                    $_pq->idcrypt = trim($_REQUEST["productid_" . $_rowid]);
                    $_pq->name = trim($_REQUEST["productname_" . $_rowid]);
                    $_pq->description = trim($_REQUEST["productdesc_" . $_rowid]);
                    $_pq->price = doubleval($_REQUEST["productprice_" . $_rowid]);
                    $_pq->qty = doubleval($_REQUEST["productqty_" . $_rowid]);
                    $_pq->months = doubleval($_REQUEST["productmonths_" . $_rowid]);
                    $_pq->subtotal = $_pq->price * $_pq->months * $_pq->qty;
                    $_pq->operationtype = trim($_REQUEST["operationtype_" . $_rowid]);
                    $_pq->doctype = "invoicetc";
                    $_pq->itemindex = $itemindex;
                    array_push($aProduct, $_pq);
                    $itemindex ++;
                }
            }
        }

        $aService = array();
        $itemindex = 1;
        if ($_REQUEST["servicerowid"] != null) {
            $totalService = count($_REQUEST["servicerowid"]);
            if ($totalService > 0) {
                foreach ($_REQUEST["servicerowid"] as $_rowid) {
                    $_sq = new DocumentService();
                    $_sq->idcrypt = trim($_REQUEST["serviceid_" . $_rowid]);
                    $_sq->name = trim($_REQUEST["servicename_" . $_rowid]);
                    $_sq->description = trim($_REQUEST["servicedesc_" . $_rowid]);
                    $_sq->price = doubleval($_REQUEST["serviceprice_" . $_rowid]);
                    $_sq->qty = doubleval($_REQUEST["serviceqty_" . $_rowid]);
                    $_sq->activationprice = doubleval($_REQUEST["serviceactivation_" . $_rowid]);
                    $_sq->subtotal = ( $_sq->price + $_sq->activationprice ) * $_sq->qty;
                    $_sq->doctype = "invoicetc";
                    $_sq->itemindex = $itemindex;
                    array_push($aService, $_sq);
                    $itemindex ++;
                }
            }
        }

        $e->aProduct = $aProduct;
        $e->aService = $aService;

        return $e;
    }

    function downloadAction() {
        
        $_d = new DocumentemailController();
        $_d->downloadinvoicetcAction($this->aRequest["id"], strtolower($this->session_langcode));
         
    }

    
        function getTimeFormat($_time){
        //7:5 PM
        $aTime1 = split(" ", $_time);
        $pm = $aTime1[1];
        $ispm = false;
        $aTime2 = split(":",$aTime1[0] );
        if($pm =="PM"){
            $ispm = true;
        }
        $hour   = intval($aTime2[0]);
        $minute = intval($aTime2[1]);
        
        $str_hour = "";
        $str_minute = "";
        
        if($ispm){   
          $hour =   $hour + 12;  
          $str_hour = $hour."";
        }else{
            if($hour <10){
                $str_hour = "0".$hour;
            }else{
                $str_hour = $hour."";
            }
        }
        

        if($minute <10){
           $str_minute = "0".$minute;
        }else{
            $str_minute = $minute."";
        }
        

        return $str_hour.":".$str_minute.":00";

        
    }
    
    
}
