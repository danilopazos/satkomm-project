<?php

require_once ("../lib/dbcdr/Dto_CdrInvoice.php");


require_once ("../lib/bean/AirtimePlan.php");
require_once ("../lib/db/Dto_AirtimePlan.php");

require_once ("../lib/bean/DetailCdr.php");
require_once ("../lib/bean/CdrInvoice.php");
require_once ("../lib/bean/CdrSimcardPeriod.php");
require_once ("../lib/bean/Address.php");
require_once ("../lib/bean/Account.php");
require_once ("../lib/bean/CdrRecord.php");
require_once ("../lib/bean/CdrMovementInvoice.php");

require_once("../lib/bean/InfoToConsumptionPdf.php");


/*
  require_once ("../lib/bean/Address.php");
  require_once ("../lib/bean/SaleOrder.php");
  require_once ("../lib/bean/SaleOrderDetail.php");
  require_once ("../lib/bean/Invoice.php");
  require_once ("../lib/bean/ItemDispatch.php");
  require_once ("../lib/bean/InvoiceDetail.php");
  require_once ("../lib/bean/InventoryItem.php");
  require_once ("../lib/bean/ShippingProvider.php");

  require_once ("../lib/db/Dto_Saleorder.php");
  require_once ("../lib/db/Dto_Invoice.php");
  require_once ("../lib/db/Dto_Inventory.php");
  require_once ("../lib/db/Dto_ShippingProvider.php");
 */


require_once ("../lib/bean/HeadDocumentExport.php");
require_once ("../lib/bean/DetailProductExport.php");

require_once ("../lib/helper/DatatableHelper.php");

require_once ("../lib/controller/MediaController.php");
require_once ("../lib/controller/DocumentgenerateController.php");

class CustomerconsumptionController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {

        $dto_cc = new Dto_CdrInvoice();
        $aRecord = $dto_cc->getCustomerInvoices($this->session_accountid);


        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");

        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/customerconsumption/index.js");


        $view->aRecord = $aRecord;

        $view->bodyjquery = "TableData.init();  ";
        $view->load("customerconsumption", "index");
    }

    function invoicepdfAction() {

        $dto_account = new Dto_Account();
        $dto_airtime = new Dto_AirtimePlan();
        $_aPlans = $dto_airtime->getListAll($this->session_accountid  );
        
        $view = new ViewController();

        $fileLangTemplate = "../lib/view/invoicetc/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate;

        $docid = trim($this->aRequest["id"]);

        $dto_invoice = new Dto_CdrInvoice();
        $invoice = $dto_invoice->getById($docid);

        $accountid = $this->session_accountid;
        $infoprovider = $dto_account->infoToGeneratePdf($accountid, $invoice->customeraccountid);

        $aSimcardPeriod = $dto_invoice->getSimcardPeriod($docid);

        $aDetailProduct = array();
        $subtotal = 0;

        $period_invoice="";

        foreach ($aSimcardPeriod as $detail) {

            $entity = new DetailProductExport();

            //Periodo
            $month_period=$detail->month;
            if(strlen($month_period)==1) $month_period ="0".$month_period;
                
            $entity->codeProduct = $_lang_invoiceperiod_lblperiod . ' ( ' . $detail->year . ' - ' . $month_period . ' ) ';

            
            $new_period_invoice=$month_period."-".$detail->year;
            $period_invoice = $new_period_invoice;
            $itemDescription = "";

            /*
              $_lang_invoiceperiod_lblperiod = 'Period';
              $_lang_invoiceperiod_lblsubscriptionprice = 'Plan';
              $_lang_invoiceperiod_lblfixedchargesprice = 'Fixed charges';
              $_lang_invoiceperiod_lbladditionalchargesprice = 'Additional charges';
              $_lang_invoiceperiod_lblactivationprice= 'Charges per activation';
              $_lang_invoiceperiod_lblsuspensionprice = 'Charges per suspension';
              $_lang_invoiceperiod_lbldeactivationprice = 'Charges per deactivation';
             */
                foreach($_aPlans as $e){
                    if($e->id == $detail->planid){
                        // $report .="<td>".$e->name."</td>";
                        //Plan
                        $itemDescription .= "Plan: " .trim($e->name)." <br />";
                    }
                }


            $itemDescription .= $_lang_invoiceperiod_lblsubscriptionprice . ": ".$infoprovider->currencyIso." " . number_format(doubleval($detail->subscriptionprice*$infoprovider->currencyExchange),2)." <br />";

            if (doubleval($detail->fixedchargesprice) > 0) {
                $itemDescription .= $_lang_invoiceperiod_lblfixedchargesprice . ": ".$infoprovider->currencyIso." " . number_format(doubleval($detail->fixedchargesprice*$infoprovider->currencyExchange),2)." <br />";
            }
            if (doubleval($detail->additionalchargesprice) > 0) {
                $itemDescription .= $_lang_invoiceperiod_lbladditionalchargesprice . ": ".$infoprovider->currencyIso." " . number_format(doubleval($detail->additionalchargesprice*$infoprovider->currencyExchange),2)." <br />";
            }
            /*
            if (doubleval($detail->activationprice) > 0) {
                $itemDescription .= $_lang_invoiceperiod_lblactivationprice . ": USD " . doubleval($detail->activationprice)."   \n";
            }
            if (doubleval($detail->suspensionprice) > 0) {
                $itemDescription .= $_lang_invoiceperiod_lblsuspensionprice . ": USD " . doubleval($detail->suspensionprice)."   \n";
            }
            if (doubleval($detail->deactivationprice) > 0) {
                $itemDescription .= $_lang_invoiceperiod_lbldeactivationprice . ": USD " . doubleval($detail->deactivationprice)."   \n";
            }
            */
            
            $entity->description = $itemDescription;

            //Nro    abonado
            $entity->nameProduct = $detail->serial . ' (' . $detail->voicenumber . ') ';
            $entity->quantity = 1;
            $entity->price = $detail->price;



            //$productTotalprice = doubleval($detail->subscriptionprice) + doubleval($detail->fixedchargesprice) + doubleval($detail->additionalchargesprice) + doubleval($detail->activationprice) + doubleval($detail->suspensionprice) + doubleval($detail->deactivationprice);
            
            $productTotalprice = number_format(doubleval($detail->subscriptionprice),2) + number_format(doubleval($detail->fixedchargesprice),2) + number_format(doubleval($detail->additionalchargesprice),2)  ;

            $entity->totalPrice = $productTotalprice;
            $subtotal = $subtotal + $productTotalprice;


            array_push($aDetailProduct, $entity);
        }

        //info de la empresa
        $_account = $dto_account->getById($invoice->customeraccountid);

        $headDocument = new HeadDocumentExport();

        //get invoice movements
        $headDocument->movementsInvoice = $dto_invoice->getMovementsInvoince($docid);
                

        $headDocument->accountName = $_account->name;
        $headDocument->numberDocument = $_account->taxid;
        $headDocument->contactPhone = $_account->phonenumber;
        $headDocument->contactFirstName = $_account->contact_names;
        $headDocument->contactPhone = $_account->contacttelephone;
        $headDocument->contactEmail = $_account->contactemail;
     
        $headDocument->dateDocument = $invoice->invoicedate;
        $headDocument->dateDueDocument = $invoice->duedate;
        $headDocument->datePeriod = $period_invoice;

        $headDocument->billStreet = $_account->billaddress->street;
        $headDocument->billCity = $_account->billaddress->city;
        $headDocument->billState = $_account->billaddress->state;
        $headDocument->billCode = $_account->billaddress->zipcode;
        $headDocument->billCountry = $_account->billaddress->country;

        $headDocument->webSiteAccount = $invoice->webSite;
        $headDocument->phoneContact = $invoice->phonenumber;

        $headDocument->shippingAddress = $_account->shipaddress->street;
        $headDocument->shippingCountry = $_account->shipaddress->country;
        $headDocument->shippingState = $_account->shipaddress->state;
        $headDocument->shippingCity = $_account->shipaddress->city;
        $headDocument->shippingZipCode = $_account->shipaddress->zipcode;

        $headDocument->colorBanner = "#F71907";
        $headDocument->textBanner = ""; //Here we put the banner id - like stamp
        $headDocument->fontBanner = 100;
        
        //about taxes
        $headDocument->parcialtotal = $subtotal;
        $headDocument->taxes = $infoprovider->taxammount;
        $headDocument->total = $subtotal + ($subtotal * ($headDocument->taxes / 100) );

        $voiceconsumption = 0;
        $dataconsumption = 0;
        if($infoprovider->countrycode=="CO"){
            //Taxes only for Colombia
            foreach($headDocument->movementsInvoice as $item){
                if($item->calltype == "001"){ $voiceconsumption += $item->additionalCost; }
                elseif($item->calltype == "035"){ $dataconsumption += $item->additionalCost; }
            }
            $view->voiceconsumption = ($voiceconsumption*0.04); //voice taxes 4%
            $view->dataconsumption = ($dataconsumption*0.04); //data taxes 4%
        }
        $view->detailDocument = $aDetailProduct;

        if( $infoprovider->providerid == "10003"){
            $concatInvoiceSequence  = $dto_invoice->getInvoiceSequence($docid, $infoprovider->providerid);
            //invoice->year : 201801
            $concatInvoiceSequence .= substr($invoice->year , -2).substr($invoice->year , 2,2);
            $view->code = $concatInvoiceSequence;

        }else{
            $invoiceno = $invoice->provideraccountid.''.$invoice->customeraccountid.''.$invoice->year.''.$invoice->month;  
            $view->code = $invoiceno;

        }

        $view->lang_index_table = $_lang_quote_title;
        $view->lang_invoice_number = $_lang_register_label_quoteno;
        $view->lang_invoice_date = $_lang_register_label_quotedate;
        $view->lang_invoice_due_date = $_lang_register_label_quoteuntil;
        $view->typeDocument = $_lang_index_label_module;
        $view->lang_document_intro = $_lang_quote_intro;


        //enviando a generar el pdf
        $document = new DocumentGenerateController();

            //headDocument = datos de la empresa. fechas y sello
            //view = textos, montos finales y array de detalle
            //accountid = id de la cuenta en sesion
        $document->generateInvoicePeriod($view, $this, $headDocument, $infoprovider);
    }
    
    function invoicecdrpdfAction() {

        $view = new ViewController();


        $fileLangTemplate = "../lib/view/invoicetc/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate;

        $docid = trim($this->aRequest["id"]);

        $dto_invoice = new Dto_CdrInvoice();
        $invoice = $dto_invoice->getById($docid);


        $aCdrs= $dto_invoice->getInvoiceCdrs($docid);
        
    
        $aDetailProduct = array();
        $subtotal = 0;
        foreach ($aCdrs as $detail) {
 
            array_push($aDetailProduct, $detail);
        }


        $dto_account = new Dto_Account();
        $_account = $dto_account->getById($invoice->customeraccountid);

        $headDocument = new HeadDocumentExport();
        $headDocument->accountName = $_account->name;
        $headDocument->numberDocument = "xxx";
        $headDocument->contactPhone = $_account->phonenumber;
        $headDocument->contactFirstName = $_account->contact_names;
        $headDocument->contactPhone = $_account->contacttelephone;
        $headDocument->contactEmail = $_account->contactemail;

     
        $headDocument->dateDocument = $invoice->invoicedate;
        $headDocument->dateDueDocument = $invoice->duedate;

        $headDocument->billStreet = $_account->billaddress->street;
        $headDocument->billCity = $_account->billaddress->city;
        $headDocument->billState = $_account->billaddress->state;
        $headDocument->billCode = $_account->billaddress->zipcode;
        $headDocument->billCountry = $_account->billaddress->country;

        $headDocument->webSiteAccount = $invoice->webSite;
        $headDocument->phoneContact = $invoice->phonenumber;

        $headDocument->shippingAddress = $_account->shipaddress->street;
        $headDocument->shippingCountry = $_account->shipaddress->country;
        $headDocument->shippingState = $_account->shipaddress->state;
        $headDocument->shippingCity = $_account->shipaddress->city;
        $headDocument->shippingZipCode = $_account->shipaddress->zipcode;


        $headDocument->parcialtotal = $subtotal;
        
        if($invoice->provideraccountid > 10001){
            $headDocument->taxes = 18;
        }

        $headDocument->total = $subtotal + ($subtotal * ($headDocument->taxes / 100) );

        $headDocument->colorBanner = "#F71907";
        $headDocument->textBanner = ""; //Here we put the banner id
        $headDocument->fontBanner = 100;


        $accountid = $this->session_accountid;

        $view->detailDocument = $aDetailProduct;

        $invoiceno = $invoice->provideraccountid.''.$invoice->customeraccountid.''.$invoice->year.''.$invoice->month;  
        $view->code = $invoiceno;

        $view->lang_index_table = $_lang_quote_title;
        $view->lang_invoice_number = $_lang_register_label_quoteno;
        $view->lang_invoice_date = $_lang_register_label_quotedate;
        $view->lang_invoice_due_date = $_lang_register_label_quoteuntil;
        $view->typeDocument = $_lang_index_label_module;
        $view->lang_document_intro = $_lang_quote_intro;







        $document = new DocumentGenerateController();
        $document->generateInvoiceCdr($view, $this, $accountid, $headDocument);
    }
    
//function
}
