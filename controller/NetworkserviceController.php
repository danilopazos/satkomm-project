<?php

require_once ("../lib/bean/NetworkService.php");
require_once ("../lib/db/Dto_NetworkService.php");

require_once ("../lib/helper/DatatableHelper.php");

class NetworkserviceController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/networkservice/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("networkservice", "index");
    }

    function requestgetlistindexAction() {
        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('id', 'name');
        $_datatableSetup->_sortCol = "name";
        $_datatableSetup->_sortDir = "asc";

        //$datatableHelper = new DatatableHelper();
        //$datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_networkservice = new Dto_NetworkService();

        $aRecords = $dto_networkservice->getList();

        $totalRecords = count($aRecords);
        $totalFiltered = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {
            $id = $record->id;
            $name = $record->name;

            $link_edit = '<a href="#"  onclick="fnc_viewmodal(\'' . $id . '\')"    class="btn btn-xs btn-default  user-viewmodal  " title="' . $id . '"  ><i class="fa fa-pencil"></i></a>';
            $link_delete = '<a href="#"  onclick="fnc_delete(\'' . $id . '\'  )"    class="btn btn-xs btn-default  user-viewmodal  "  ><i class="fa fa-trash-o"></i></a>';


            $output['aaData'][] = array($id, $name, $link_edit . ' &nbsp; ' . $link_delete);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestupdateAction() {
        $_id = trim($_REQUEST["id"]);
        $_name = trim($_REQUEST["name"]);
        $ok = 1;

        $_networkservice = new NetworkService();
        $_networkservice->id = $_id;
        $_networkservice->name = $_name;

        $dto_networkservice = new Dto_NetworkService();
        $result = $dto_networkservice->update($_networkservice);
        if (!($result)) {
            $ok = -1;
        }

        $view = new ViewController();
        $view->template = 'blank';

        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = array("result" => 1);
        }

        $view->load("template", 'json');
    }

    function requestdeleteAction() {
        $_id = trim($_REQUEST["id"]);

        $ok = 1;

        $_networkservice = new NetworkService();
        $_networkservice->id = $_id; 

        $dto_networkservice = new Dto_NetworkService();
        $result = $dto_networkservice->delete($_id);
        if (!($result)) {
            $ok = -1;
        }

        $view = new ViewController();
        $view->template = 'blank';

        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = array("result" => 1);
        }

        $view->load("template", 'json');
    }

}
