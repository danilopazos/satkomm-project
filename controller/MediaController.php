<?php

require_once ("../lib/bean/Media.php");
require_once ("../lib/db/Dto_Media.php");

class MediaController extends BaseController {
    
    function __construct() {
       // isAuth();
       // $this->setSession();
    }
    
    
    function imagebyidAction($media_idcrypt = "", $media_type = ""){
        $idcrypt = trim($this->aRequest["id"]);
        $type = trim($this->aRequest["type"]);
        
        if($media_idcrypt != ""){
            $idcrypt = $media_idcrypt;
            $type = $media_type;
        }
        
       
        $dto_media = new Dto_Media();
        $entity = $dto_media->getByIdCrypt($idcrypt);
       
        $metadata = $dto_media->getMetadataFromMediaBlob($entity->id);
      
        $prename = "";
        $blobcontent = "";
        if($type =="" || $type =="common"){
            $blobcontent = $dto_media->getCommonBlobById($entity->id);
            $prename = "common";
        }else  if($type =="thumb"){
            $blobcontent = $dto_media->getThumbBlobById($entity->id);
            $prename = "thumb";
        }else  if($type =="original"){
            $blobcontent = $dto_media->getOriginalBlobById($entity->id);
            $prename = "original";
        }
    
        $view = new ViewController ();
        $view->template = "blank";
        $fileext = $metadata->fileext;
        $filename = $prename."_".$metadata->filename;
        
        
        $view->fileext           = $fileext;
        $view->filename       = $filename;
        $view->blobcontent =   $blobcontent;
     
        $view->load("media", "image");
    }//function
     
    function imagebyproductidAction(){
     
        $idcrypt = trim($this->aRequest["productid"]);
        $type = trim($this->aRequest["type"]);
        $dto_media = new Dto_Media();
        $media_idcrypt = $dto_media->getIdCryptByProductId($idcrypt);
      
         $this->imagebyidAction($media_idcrypt , $type);
    }//function    
    
    function mylogoAction(){
        
        isAuth();
        $this->setSession();
        
        $accountid = $this->session_accountid;
        $dto_account = new Dto_Account();
        $media_idcrypt = $dto_account->getLogoIdByAccountid($accountid);
       
        if($media_idcrypt != ""){
            $this->imagebyidAction($media_idcrypt , "original");
        }else{
           
            $view = new ViewController ();
            $view->template = "blank";
          
            $view->fileext           = "png";
            $view->filename       = "logo";           
            $view->blobcontent = file_get_contents( getCurrentUrlPath()."/assets/images/favicon.png");;

            $view->load("media", "image");
            
        }
    }//function   
    
    function getlogoAction(){
     
        $accountid = trim($this->aRequest["accountid"]);
        $dto_account = new Dto_Account();
        $media_idcrypt = $dto_account->getLogoIdByAccountid($accountid);
       
        if($media_idcrypt != ""){
            $this->imagebyidAction($media_idcrypt , "original");
        }else{
           
            $view = new ViewController ();
            $view->template = "blank";
          
            $view->fileext           = "png";
            $view->filename       = "logo";           
            $view->blobcontent = file_get_contents( getCurrentUrlPath()."/assets/images/favicon.png");;

            $view->load("media", "image");
            
        }
    }//function  
    
    
    function imagebyaccountidAction(){
     
        $accountid = trim($this->aRequest["accountid"]);
        $dto_account = new Dto_Account();
        $media_idcrypt = $dto_account->getLogoIdByAccountid($accountid);
       
        if($media_idcrypt != ""){
            $this->imagebyidAction($media_idcrypt , "original");
        }else{
           
            $view = new ViewController ();
            $view->template = "blank";
          
            $view->fileext           = "png";
            $view->filename       = "logo";           
            $view->blobcontent = file_get_contents( getCurrentUrlPath()."/assets/images/favicon.png");;

            $view->load("media", "image");
            
        }
    }//function    
    
} 