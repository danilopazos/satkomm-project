<?php

require_once ("../lib/bean/Address.php");
require_once ("../lib/bean/SaleOrder.php");
require_once ("../lib/bean/SaleOrderDetail.php");

require_once ("../lib/bean/Fees.php");
require_once ("../lib/bean/Taxes.php");

require_once ("../lib/util/mailservice/template.php");
require_once ("../lib/controller/EmailController.php");

require_once ("../lib/db/Dto_Saleorder.php");
require_once ("../lib/db/Dto_Cart.php");
require_once ("../lib/db/Dto_Invoice.php");

require_once ("../lib/db/Dto_Taxes.php");
require_once ("../lib/db/Dto_Fees.php");

require_once ("../lib/helper/DatatableHelper.php");

class SaleorderController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {
        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/saleorder/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("saleorder", "index");
    }

    function viewAction() {

        $_code = trim($this->aRequest["code"]);

        $dto_saleorder = new Dto_Saleorder();
        $saleorder = $dto_saleorder->getByCode($_code);
        $saleorderDetail = $dto_saleorder->getDetailByCode($_code);

        $view = new ViewController();
        array_push($view->aJavascript, "assets/js/modules/saleorder/index.js");

        if ($this->canIProcessOrder($this->session_accountid, $_code) > 0) {
            $view->processOrder = true;
        } else {
            $view->processOrder = false;
        }
        
        $view->session_accountid = intval($this->session_accountid);
        $view->saleorder = $saleorder;
        $view->detail = $saleorderDetail;
        $view->load("saleorder", "view");
    }
    
    function viewrAction() {
        $_idcrypt = trim($this->aRequest["id"]);
        $dto_saleorder = new Dto_Saleorder();
        $_code = $dto_saleorder->getCodeByIdcrypt($_idcrypt) ;
        redirect("saleorder", "view", array("code"=>$_code));
    }
        
    
    
    
    function cancelorderAction(){
        $_code = trim($this->aRequest["code"]);
        $dto_saleorder = new Dto_Saleorder();
        $dto_saleorder->cancelOrder($_code, $this->session_userid);
        
        redirect("saleorder", "view", array("code"=>$_code));
    }
    
    
    function processAction() {
        $_code = trim($this->aRequest["code"]);

        $dto_saleorder = new Dto_Saleorder();
        $saleorder = $dto_saleorder->getByCode($_code);
        $saleorderDetail = $dto_saleorder->getDetailByCode($_code);

        $view = new ViewController();

        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aJavascript, "assets/js/modules/saleorder/index.js");
        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/saleorder/process.js");

        if ($this->canIProcessOrder($this->session_accountid, $_code) > 0) {
            $view->processOrder = true;
        } else {
            $view->processOrder = false;
            return;
        }

        //we need to generate the new invoice code
        $dto_user = new Dto_User();
        $dto_invoice = new Dto_Invoice();
        $view->invoicecode = $dto_invoice->generateInvoiceCode();

        $view->saleorder = $saleorder;
        $view->detail = $saleorderDetail;
        $view->user = $dto_user->getById($saleorder->userid);
        
        
        $dto_fees = new Dto_Fees();
        $dto_taxes = new Dto_Taxes();
        
        $view->fees = $dto_fees->getFeesByAccountId($this->session_accountid);
        $view->taxes = $dto_taxes->getTaxes($this->session_accountid);

        $view->bodyjquery = "FormElements.init();  Register.init(); ";

        $view->load("saleorder", "process");
    }

    function canIProcessOrder($_parentaccountid, $_code) {
        $result = false;

        $dto_saleorder = new Dto_Saleorder();
        $saleorderid = $dto_saleorder->getCodeByChildOrder($_parentaccountid, $_code);

        if ($saleorderid > 0) {
            $result = true;
        }

        return $result;
    }

    function myordersAction() {

        $view = new ViewController();
        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/saleorder/myorders.js");

        $view->bodyjquery = "TableData.init(); TableDataReadyToDispatch.init(); TableDataDispatched.init(); TableDataDelivered.init();  TableDataCancelled.init();  ";
        $view->load("saleorder", "myorders");
    }

    function requestplaceproductorderAction() {
        $dto_saleorder = new Dto_Saleorder();
        $_code = $dto_saleorder->generateOrderCode();

        $totalAmmount = 0;
        $_saleorderdetail = $this->getRequestSaleOrderDetail();
        foreach ($_saleorderdetail as $item) {
            $totalAmmount = $totalAmmount + doubleval($item->subtotal);
        }


        $_saleorder = $this->getRequestSaleOrder($totalAmmount, $_code);

        $_billaddress = $this->getRequestBillAddress();
        $_shipaddress = $this->getRequestShipAddress();

        $result = $dto_saleorder->placeOrder($_saleorder, $_saleorderdetail, $_billaddress, $_shipaddress);
        
        $_e = $dto_saleorder->getByCode($_code);
        
        $data = array("result" => $result);
        if ($result) {

            
            $fileLang = "../lib/view/saleorder/lang/".trim($this->session_langcode). ".php";
            if (file_exists($fileLang)) {
                require_once($fileLang);
            }
            
            
            $dto_cart = new Dto_Cart();
            $dto_cart->deleteCart($this->session_accountid);
            
            //we need to send the cancell order email
            $emailController = new EmailController();
            
            //sending the order to the customer
            $_title = $_lang_email_tocustomer_subject_titlepart.' #'.$_code; 
            $_message1 = $_lang_email_tocustomer_message1;
            if($_saleorder->dispatchnow == 1){
                $_message2 = $_lang_email_tocustomer_dispatchnow;
            }else{
                $_message2 = $_lang_email_tocustomer_message2;
            }
            
            $content1 = getCancelOrderEmail(  $_code , $_e->idcrypt, $_title , $_message1, $_message2 , $_lang_code , $_lang_url   );
            $emailController->send($this->session_username, "", $_title , $content1);
            //------------------------------------------------------
            //------------------------------------------------------
            //------------------------------------------------------
            //we need to send the order to the provider
            $_title = $_lang_email_toprovider_subject_titlepart.' #'.$_code; 
            $_message1 = $_lang_email_toprovider_message1;
            if($_saleorder->dispatchnow == 1){
                $_message2 = $_lang_email_toprovider_dispatchnow;
            }else{
                $_message2 = "";
            }
            
            $config = new Config();
            $aEmails = $config->aMaxtrixHouseProvidersEmail;
            $content2 = getSentOrderEmail($_code ,  $_e->idcrypt, $_title , $_message1, $_message2 , $_lang_code, $_lang_url );
            $emailController->sendArray(  $aEmails , "", $_title , $content2);
            

            $data = array("result" => $_code);
        }


        $view = new ViewController();
        $view->data = $data;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function requestgetmyordersAction() {
        
        
        $_orderstate = trim($this->aRequest["orderstate"]);
        
        $fileLangTemplate = "../lib/view/saleorder/lang/" . strtolower($this->session_langcode) . ".php";
        if(file_exists($fileLangTemplate)){
            include $fileLangTemplate;
        }
        
        
        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('so.code', 'so.orderdate', 'i.dispatchdate', 'i.deliverydate', 'i.shippingtrackingid', 'i.code', 'so.totalitems', 'so.x_ammount',  'so.orderstatus');
        $_datatableSetup->_sortCol = "a.orderdate";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Saleorder();
        $totalRecords = $dto_entity->getMySaleOrdersTotal($_orderstate, $this->session_accountid, $datatable->_search);
        $aRecords = $dto_entity->getMySaleOrders($_orderstate, $this->session_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {

            $status = "created";
            $classBtn = "btn-bricky";
            if ($record->invoice_code != "") {
                $status = "ready to dispacth";
                $classBtn = "btn-yellow";
            }
            if ($record->invoice_dispatchdate != "") {
                $classBtn = "btn-yellow";
            }
            if ($record->invoice_deliverydate != "") {
                $status = "delivered";
                $classBtn = "btn-primary";
            }
            if($record->orderstatus == 0){
                $status = 'cancelled';
                $classBtn = "btn-dark-grey";
            }

            //$detailView = '<a href="/saleorder/view/code/' . $record->code . '" class="btn btn-xs ' . $classBtn . '" >' . $record->code . '</a>';
            $detailView = '<a href="/saleorder/view/code/' . $record->code . '" class="btn btn-xs btn-default" >' . $record->code . '</a>';
            $btnStatus = '<a href="/saleorder/view/code/' . $record->code . '" class="btn btn-xs ' . $classBtn . '" >' . $status. '</a>';
            
            $invoiceLink = '';
            if ($record->invoice_code != "") {
                $invoiceLink = '<a href="/invoice/view/code/' . $record->invoice_code . '" class="btn btn-xs btn-default" >' . $record->invoice_code . '</a>';
            }

            $_shippingtrackinginfo = '';
            $showtrackinginfo = false;
            if ($record->shippingprovidercode != "") {
               $_shippingtrackinginfo = '<strong>' . $record->shippingprovider_name . '</strong>';
               
            }
            if ($record->shippingtrackingid != "") {
                $_shippingtrackinginfo .=  '&nbsp;';
                if ($record->shippingprovidercode == "FEDEX") {

                    $_shippingtrackinginfo .=  '<a  target="_blank" href="https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=' . $record->shippingtrackingid . '&clienttype=ivother">' . $record->shippingtrackingid . '</a>';
                } else if ($record->shippingprovidercode == "UPS") {

                    $_shippingtrackinginfo .=  '<a target="_blank" href="https://www.ups.com/WebTracking/track?loc=en_US">' . $record->shippingtrackingid . '</a>';
                }  else if ($record->shippingprovidercode == "DISTRIBUTOR") {

                    $_shippingtrackinginfo .=  $_lang_view_label_distributor;
                }  else if ($record->shippingprovidercode == "ND") {

                    $_shippingtrackinginfo .=  $_lang_view_label_shippingnotdefined;
                } else {

                    $_shippingtrackinginfo .=  $record->shippingtrackingid;
                    
                }
                
            }else{
                $_shippingtrackinginfo = 'N/D';
                
            }




            $output['aaData'][] = array($detailView, substr($record->orderdate, 0, 10), $record->invoice_dispatchdate, $record->invoice_deliverydate, $_shippingtrackinginfo, $invoiceLink, $record->totalitems, moneyFormat($record->x_ammount),   $btnStatus);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestgetcustomerordersAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('so.code', 'a.name', 'so.orderdate', 'i.dispatchdate', 'i.deliverydate', 'i.code', 'so.totalitems', 'so.x_ammount',  'so.orderstatus');
        $_datatableSetup->_sortCol = "a.orderdate";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Saleorder();
        $totalRecords = $dto_entity->getMyCustomerOrdersTotal($this->session_accountid, $datatable->_search);
        $aRecords = $dto_entity->getMyCustomerOrders($this->session_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );


        foreach ($aRecords as $record) {

            $status = "created";
            $classBtn = "btn-bricky";
            if ($record->invoice_code != "") {
                $status = "ready to dispacth";
                $classBtn = "btn-yellow";
            }
            if ($record->invoice_dispatchdate != "") {
                $classBtn = "btn-yellow";
            }
            if ($record->invoice_deliverydate != "") {
                $status = "delivered";
                $classBtn = "btn-primary";
            }
            if($record->orderstatus == 0){
                $status = 'cancelled';
                $classBtn = "btn-dark-grey";
            }
            $btnStatus = '<a href="/saleorder/view/code/' . $record->code . '" class="btn btn-xs ' . $classBtn . '" >' . $status. '</a>';
            $detailView = '<a href="/saleorder/view/code/' . $record->code . '" class="btn btn-xs btn-default" >' . $record->code . '</a>';

            $invoiceLink = '';
            if ($record->invoice_code != "") {
                $invoiceLink = '<a href="/invoice/view/code/' . $record->invoice_code . '" class="btn btn-xs btn-default" >' . $record->invoice_code . '</a>';
            }

            $output['aaData'][] = array($detailView, $record->accountname, substr($record->orderdate, 0, 10), $record->invoice_dispatchdate, $record->invoice_deliverydate, $invoiceLink, $record->totalitems, moneyFormat($record->x_ammount),   $btnStatus);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function getRequestSaleOrder($totalAmmount, $code) {

        $dto_saleorder = new Dto_Saleorder();
        $saleorder = new SaleOrder();
        $saleorder->accountid = trim($_REQUEST["accountid"]);
        $saleorder->ordercomments = trim($_REQUEST["ordercomments"]);
        $saleorder->code = $code;
        $saleorder->orderdate = date('Y-m-d');
        $saleorder->orderpaymentrand = time() . '' . rand(10000, 99999);
        $saleorder->userid = $this->session_userid;
        $saleorder->userip = get_ip();
        $saleorder->sessionid = session_id();
        $saleorder->username = $this->session_user;
        $saleorder->totalitems = intval($_REQUEST["totalitem"]);
        $saleorder->taxes = 0;
        $saleorder->shipping_cost = 0;
        $saleorder->payment_method = trim($_REQUEST["payment_method"]);
        $saleorder->x_ammount = $totalAmmount;
        $saleorder->subtotal = $totalAmmount;
        
        $_dispatchnow = intval($_REQUEST["dispatchnow"]);
        $saleorder->dispatchnow = $_dispatchnow;
         
        
        return $saleorder;
    }

    function getRequestSaleOrderDetail() {

        $aSaleOrderDetail = array();
        
        $_totalitem = intval($_REQUEST["totalitem"]);

        for ($index = 1; $index <= $_totalitem; $index ++) {

            $_itemid = $_REQUEST[$index . "_itemid"];
            $_deleted = $_REQUEST[$index . "_deleted"];
            $_productid = $_REQUEST[$index . "_productid"];
            $_productname = $_REQUEST[$index . "_productname"];
            $_price = $_REQUEST[$index . "_price"];
            $_sku = $_REQUEST[$index . "_sku"];
            $_qty = $_REQUEST[$index . "_qty"];
            $_subtotal = $_REQUEST[$index . "_subtotal"];
            $_operation = $_REQUEST[$index . "_operation"];

            if (intval($_deleted) == 0) {

                $saleOrderDetail = new SaleOrderDetail();
                $saleOrderDetail->productid = $_productid;
                $saleOrderDetail->productprice = $_price;
                $saleOrderDetail->qty = $_qty;
                $saleOrderDetail->itemid = $_itemid;
                $saleOrderDetail->subtotal = $_subtotal;
                $saleOrderDetail->sku = $_sku;
                $saleOrderDetail->operation = $_operation;
                $saleOrderDetail->productname = $_productname;

                array_push($aSaleOrderDetail, $saleOrderDetail);
            }
        }

        return $aSaleOrderDetail;
    }

    function getRequestBillAddress() {

        $_bill_country = trim($_REQUEST["bill_country"]);
        $_bill_state = trim($_REQUEST["bill_state"]);
        $_bill_city = trim($_REQUEST["bill_city"]);
        $_bill_street = trim($_REQUEST["bill_street"]);
        $_bill_zipcode = trim($_REQUEST["bill_zipcode"]);
        $_bill_pobox = trim($_REQUEST["bill_pobox"]);

        $billAddress = new Address();
        $billAddress->country = $_bill_country;
        $billAddress->state = $_bill_state;
        $billAddress->city = $_bill_city;
        $billAddress->street = $_bill_street;
        $billAddress->zipcode = $_bill_zipcode;
        $billAddress->pobox = $_bill_pobox;
        $billAddress->type = 'BILLING';
        return $billAddress;
    }

    function getRequestShipAddress() {

        $_ship_country = trim($_REQUEST["ship_country"]);
        $_ship_state = trim($_REQUEST["ship_state"]);
        $_ship_city = trim($_REQUEST["ship_city"]);
        $_ship_street = trim($_REQUEST["ship_street"]);
        $_ship_zipcode = trim($_REQUEST["ship_zipcode"]);
        $_ship_pobox = trim($_REQUEST["ship_pobox"]);

        $shipAddress = new Address();
        $shipAddress->country = $_ship_country;
        $shipAddress->state = $_ship_state;
        $shipAddress->city = $_ship_city;
        $shipAddress->street = $_ship_street;
        $shipAddress->zipcode = $_ship_zipcode;
        $shipAddress->pobox = $_ship_pobox;
        $shipAddress->type = 'SHIPPING';
        return $shipAddress;
    }

}
