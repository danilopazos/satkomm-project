<?php

require_once ("../lib/bean/Simcard.php");
require_once ("../lib/bean/Product.php");
require_once ("../lib/bean/AirtimePlan.php");
require_once ("../lib/bean/Network.php");
require_once ("../lib/bean/PeriodInfo.php");
require_once ("../lib/bean/ServiceType.php");
require_once ("../lib/bean/TrafficIncrement.php");
require_once ("../lib/bean/TrafficSetup.php");
require_once ("../lib/bean/Traffictype.php");
require_once ("../lib/bean/TrafficUnit.php");
require_once ("../lib/bean/ChargeType.php");
require_once ("../lib/bean/TrafficAllowance.php");
require_once ("../lib/bean/TrafficAllowanceDetail.php");
require_once ("../lib/bean/TrafficService.php");

require_once ("../lib/db/Dto_Account.php");
require_once ("../lib/db/Dto_Product.php");
require_once ("../lib/db/Dto_AirtimePlan.php");
require_once ("../lib/db/Dto_Network.php");
require_once ("../lib/db/Dto_PeriodInfo.php");
require_once ("../lib/db/Dto_ServiceType.php");
require_once ("../lib/db/Dto_TrafficIncrement.php");
require_once ("../lib/db/Dto_TrafficSetup.php");
require_once ("../lib/db/Dto_Traffictype.php");
require_once ("../lib/db/Dto_TrafficUnit.php");
require_once ("../lib/db/Dto_ChargeType.php");
require_once ("../lib/db/Dto_TrafficAllowance.php");
require_once ("../lib/db/Dto_TrafficService.php");

require_once ("../lib/util/ViewUtil.php");

require_once ("../lib/helper/DatatableHelper.php");

class MyairtimeplanController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {

        $dto_airtimeplan = new Dto_AirtimePlan();
        $dto_airtimeplan->setUpList($this->session_accountid, $this->session_parentaccountid);

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/myairtimeplan/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("myairtimeplan", "index");
    }

    function registerAction() {
        $dto_account = new Dto_Account();
        $dto_entity = new Dto_AirtimePlan();



        $view = new ViewController();

        $view->aCustomer = $dto_account->getListByName($this->session_accountid, "", "a.name", "asc", 0, 100);


        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");


        $view->aPlan = $dto_entity->getListOnlyPostPaid("", "pd.name", "desc", 0, 30, "ENG", $this->session_parentaccountid, $this->session_accountid);

        array_push($view->aJavascript, "assets/js/modules/myairtimeplan/register.js");
        $view->bodyjquery = "Register.init();   ";
        $view->load("myairtimeplan", "register");
    }

    function loadparentbaseAction() {

        $_parentplan = trim($this->aRequest["parentplan"]);
        $_customeraccountid = intval($this->aRequest["customeraccountid"]);

        $dto_entity = new Dto_AirtimePlan();
        $entity = $dto_entity->getByIdCrypt($_parentplan);

        $dto_account = new Dto_Account();
        $_account = $dto_account->getById($_customeraccountid);


        $view = new ViewController();
        $view->parentplan = $_parentplan;
        $view->parent = $entity;
        $view->trafficAllowance = $dto_entity->getTrafficAllowanceByPlanId($entity->id);
        $view->trafficSetup = $dto_entity->getTrafficSetupeByPlanId($entity->id);
        $view->trafficIncrement = $dto_entity->getTrafficIncrementByPlanId($entity->id);
        $view->totalsku = $dto_entity->getTotalSku($entity->sku);

        $view->customeraccountid = $_customeraccountid;
        $view->customername = $_account->name;

        $view->newsku = $entity->sku . '' . $view->totalsku;

        array_push($view->aJavascript, "assets/js/modules/myairtimeplan/loadparentbase.js");
        $view->bodyjquery = "Register.init();   ";
        $view->load("myairtimeplan", "loadparentbase");
    }

    function saveplanfrombaseAction() {

        $_parentplan = $_REQUEST["parentplan"];

        $dto_entity = new Dto_AirtimePlan();

        $entity = $dto_entity->getByIdCrypt($_parentplan);
        $trafficAllowance = $dto_entity->getTrafficAllowanceByPlanId($entity->id);
        $trafficSetup = $dto_entity->getTrafficSetupeByPlanId($entity->id);

        $_totalallowance = count($trafficAllowance);
        if ($_totalallowance > 0) {

            for ($i = 0; $i < $_totalallowance; $i++) {
                $e = $trafficAllowance[$i];
                if ($_REQUEST["allowancename_" . $e->id] != null) {
                    $e->tname = $_REQUEST["allowancename_" . $e->id];
                }
                if ($_REQUEST["allowanceqty_" . $e->id] != null) {
                    $e->qty = $_REQUEST["allowanceqty_" . $e->id];
                }
                $trafficAllowance[$i] = $e;
            }
        }

        $_totaladditional = count($trafficSetup);
        if ($_totaladditional > 0) {

            for ($i = 0; $i < $_totaladditional; $i++) {
                $e = $trafficSetup[$i];
                if ($_REQUEST["ts_" . $e->ttcode_source] != null) {
                    $e->chargeammount = $_REQUEST["ts_" . $e->ttcode_source];
                }

                $trafficSetup[$i] = $e;
            }
        }

        $newIdcrypt = $dto_entity->createSonPlan($this->session_accountid, $this->session_parentaccountid, $_parentplan);

        $newplan = $dto_entity->getByIdCrypt($newIdcrypt);

        $newplan->accountid = $this->session_accountid;
        $newplan->customeraccountid = intval($_REQUEST["customeraccountid"]);
        $newplan->parentplan = trim($_REQUEST["parentplan"]);
        $newplan->sku = $_REQUEST["sku"];
        $newplan->name = $_REQUEST["name"];
        $newplan->subscriptionprice = $_REQUEST["subscriptionprice"];
        $newplan->fixedpayment = $_REQUEST["fixedpayment"];
        $newplan->activationpayment = $_REQUEST["activationpayment"];
        $newplan->suspensionpayment = $_REQUEST["suspensionpayment"];
        $newplan->deactivationpayment = $_REQUEST["deactivationpayment"];
        $newplan->description = $_REQUEST["description"];

        if ($newplan->customeraccountid > 0) {
            $dto_entity->updateCustomerAccountid($newIdcrypt, $newplan->customeraccountid);
        }

        $dto_entity->updatePlanInfo($newplan);
        $dto_entity->updateAllowance($newplan, $trafficAllowance);
        $dto_entity->updateAdditional($newplan, $trafficSetup);

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => true, "idcrypt" => $newIdcrypt);
        $view->load("template", "json");
    }

    function saveplanAction() {

        $_parentplan = $_REQUEST["parentplan"];
        $newIdcrypt = $_REQUEST["idcrypt"];

        $dto_entity = new Dto_AirtimePlan();

        $entity = $dto_entity->getByIdCrypt($newIdcrypt);
        $trafficAllowance = $dto_entity->getTrafficAllowanceByPlanId($entity->id);
        $trafficSetup = $dto_entity->getTrafficSetupeByPlanId($entity->id);

        $_totalallowance = count($trafficAllowance);
        if ($_totalallowance > 0) {

            // echo var_dump($_REQUEST); die();

            for ($i = 0; $i < $_totalallowance; $i++) {
                $e = $trafficAllowance[$i];
                if ($_REQUEST["allowancename_" . $e->seedcode] != null) {
                    $e->tname = $_REQUEST["allowancename_" . $e->seedcode];
                }
                if ($_REQUEST["allowanceqty_" . $e->seedcode] != null) {
                    $e->qty = $_REQUEST["allowanceqty_" . $e->seedcode];
                }
                $trafficAllowance[$i] = $e;
            }

            //  echo var_dump($trafficAllowance); die();
        }

        //echo var_dump($trafficAllowance); die();

        $_totaladditional = count($trafficSetup);
        
       // echo var_dump($trafficSetup); 
        
        
        if ($_totaladditional > 0) {

            for ($i = 0; $i < $_totaladditional; $i++) {
                $e = $trafficSetup[$i];
                if ($_REQUEST["ts_" . $e->ttcode_source] != "") {
                    $e->chargeammount = $_REQUEST["ts_" . $e->ttcode_source];
                }

                $trafficSetup[$i] = $e;
            }
        }
        //echo var_dump($trafficSetup); die();
        //$newIdcrypt = $dto_entity->createSonPlan($this->session_accountid, $this->session_parentaccountid,  $_parentplan   );

        $newplan = $dto_entity->getByIdCrypt($newIdcrypt);

        $newplan->accountid = $this->session_accountid;
        $newplan->parentplan = trim($_REQUEST["parentplan"]);
        $newplan->sku = $_REQUEST["sku"];
        $newplan->name = $_REQUEST["name"];

        $newplan->version = trim($_REQUEST["version"]);
        $newplan->aviable = intval($_REQUEST["aviable"]);

        $newplan->subscriptionprice = $_REQUEST["subscriptionprice"];
        $newplan->fixedpayment = $_REQUEST["fixedpayment"];
        $newplan->activationpayment = $_REQUEST["activationpayment"];
        $newplan->suspensionpayment = $_REQUEST["suspensionpayment"];
        $newplan->deactivationpayment = $_REQUEST["deactivationpayment"];
        $newplan->description = $_REQUEST["description"];

        $dto_entity->updatePlanInfo($newplan);
        $dto_entity->updateAllowance($newplan, $trafficAllowance);
        $dto_entity->updateAdditional($newplan, $trafficSetup);

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => true, "idcrypt" => $newIdcrypt);
        $view->load("template", "json");
    }

    function updateAction() {
        $dto_entity = new Dto_AirtimePlan();


        $_idcrypt = trim($this->aRequest["idcrypt"]);
        $entity = $dto_entity->getByIdCrypt($_idcrypt);

        $_parentplan = $entity->parentplan;

        $parentplanObject = $dto_entity->getByIdCrypt($_parentplan);


        $view = new ViewController();
        $view->parentplan = $_parentplan;
        $view->parent = $parentplanObject;
        $view->entity = $entity;

        $dto_account = new Dto_Account();
        $_account = $dto_account->getById($entity->customeraccountid);
        $view->customeraccountid = $entity->customeraccountid;
        $view->customername = $_account->name;


        $view->e_trafficAllowance = $dto_entity->getTrafficAllowanceByPlanId($entity->id);
        $view->e_trafficSetup = $dto_entity->getTrafficSetupeByPlanId($entity->id);

        $view->trafficAllowance = $dto_entity->getTrafficAllowanceByPlanId($parentplanObject->id);
        $view->trafficSetup = $dto_entity->getTrafficSetupeByPlanId($parentplanObject->id);
        $view->trafficIncrement = $dto_entity->getTrafficIncrementByPlanId($parentplanObject->id);


        array_push($view->aJavascript, "assets/js/modules/myairtimeplan/update.js");


        $view->bodyjquery = "Register.init();   ";
        $view->load("myairtimeplan", "update");
    }

    function requestdetailfordocumentAction() {

        $dto_entity = new Dto_AirtimePlan();

        $_idcrypt = trim($this->aRequest["idcrypt"]);
        $entity = $dto_entity->getByIdCrypt($_idcrypt);

        $view = new ViewController();

        $view->entity = $entity;

        $view->trafficAllowance = $dto_entity->getTrafficAllowanceByPlanId($entity->id);
        $view->trafficSetup = $dto_entity->getTrafficSetupeByPlanId($entity->id);
        $view->trafficIncrement = $dto_entity->getTrafficIncrementByPlanId($entity->id);

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");

        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/myairtimeplan/view.js");

        $view->template = "blank";
        $view->load("myairtimeplan", "requestdetailfordocument");
    }

    function viewAction() {

        $dto_entity = new Dto_AirtimePlan();

        $_idcrypt = trim($this->aRequest["idcrypt"]);
        $entity = $dto_entity->getByIdCrypt($_idcrypt);

        $_parentplan = $entity->parentplan;

        $parentplanObject = $dto_entity->getByIdCrypt($_parentplan);

        
        $view = new ViewController();
        
        $totalSimcards = $dto_entity->getSimcardsByPlanTotal($_idcrypt, $datatable->_search);
        $view->totalsimcards = $totalSimcards;
        

        $dto_account = new Dto_Account();
        $_account = $dto_account->getById($entity->customeraccountid);
        $view->customeraccountid = $entity->customeraccountid;
        $view->customername = $_account->name;

        $view->parentplan = $_parentplan;
        $view->parent = $parentplanObject;
        $view->entity = $entity;

        $view->e_trafficAllowance = $dto_entity->getTrafficAllowanceByPlanId($entity->id);
        $view->e_trafficSetup = $dto_entity->getTrafficSetupeByPlanId($entity->id);

        $view->trafficAllowance = $dto_entity->getTrafficAllowanceByPlanId($parentplanObject->id);
        $view->trafficSetup = $dto_entity->getTrafficSetupeByPlanId($parentplanObject->id);
        $view->trafficIncrement = $dto_entity->getTrafficIncrementByPlanId($parentplanObject->id);
        
        $view->aDevices = $dto_entity->getProductAssoc($parentplanObject->id);

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");

        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/myairtimeplan/view.js");


        $view->bodyjquery = "TableData.init();   ";
        $view->load("myairtimeplan", "view");
    }

    function requestgetindexAction() {

        $fileLangTemplate = "../lib/view/template/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate;

        $fileLangTemplate2 = "../lib/view/myairtimeplan/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate2;

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('p.sku', 'p.network_code', 'p.servicetype_code',
            'pd.name', 'a.name', 'p.contract_period_code', 'p.payment_period_code', 'p.price',
            'p.activationpayment',
            ' (  select DISTINCT count(ao.iserial) from airtimeoperation ao  where ao.planidcryptforcustomer = p.idcrypt  ) '
            , 'p.vers', 'p.aviable');


        $_datatableSetup->_sortCol = "p.sku";
        $_datatableSetup->_sortDir = "asc";

         
        
        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_AirtimePlan();
        $totalRecords = $dto_entity->getListTotal($datatable->_search, 'ENG', $this->session_accountid);
        $aRecords = $dto_entity->getlist($datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2, 'ENG', $this->session_accountid);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {

            $_version = $record->version;
            if (trim($_version) == "") {
                $_version = '<a href="#" class="btn-xs btn-default btn">N/D</a>';
            } else {
                $_version = '<a href="#" class="btn-xs btn-default btn">' . $_version . '</a>';
            }

            $_avaliable = '<a href="#" class="btn-xs btn-red btn">' . $_lang_register_label_noaviable . '</a>';
            if (intval($record->aviable) == 1) {
                $_avaliable = '<a href="#" class="btn-xs btn-green btn">' . $_lang_register_label_aviable . '</a>';
            }


            $_customername = $record->customername;
            if (strlen(trim($_customername)) == 0 || $_customername == NULL) {
                $_customername = '---';
            }

            $detailView = '<a href="/myairtimeplan/view/idcrypt/' . $record->idcrypt . '" class="btn btn-xs btn-default" >' . $record->sku . '</a>';
            $output['aaData'][] = array($detailView, $record->networkcode, $record->servicetypecode, $record->name, $_customername, $_lang_period_option[$record->contractperiodcode], $_lang_period_option[$record->paymentperiodcode]
                , 'USD ' . moneyFormat($record->subscriptionprice), 'USD ' . moneyFormat($record->activationpayment),
                $record->totalsimcards,
                $_version,
                $_avaliable
            );
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestgetsimcardsAction() {

        $_idcrypt = trim($this->aRequest["planid"]);
        $dto_entity = new Dto_AirtimePlan();
        $_planid = $dto_entity->getIdByIdCrypt($_idcrypt);



        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('i.iserial', 'i.voicenumber', 'i.datanumber', 'i.sku', 'i.productname', 'i.operationregistered', 'acc.name', 'ao.datetoexecute');
        $_datatableSetup->_sortCol = "i.iserial";
        $_datatableSetup->_sortDir = "asc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);


        $totalRecords = $dto_entity->getSimcardsByPlanTotal($_idcrypt, $datatable->_search);
        $aRecords = $dto_entity->getSimcardsByPlan($this->session_accountid,  $_idcrypt, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {

            $_btnoperation = "";
            if ($record->operationregistered == "activate") {
                $_btnoperation = '<a href="#" class="btn btn-xs btn-green">' . $record->operationregistered . '</a>';
            } else if ($record->operationregistered == "suspend") {
                $_btnoperation = '<a href="#" class="btn btn-xs btn-orange">' . $record->operationregistered . '</a>';
            } else if ($record->operationregistered == "deactivate") {
                $_btnoperation = '<a href="#" class="btn btn-xs btn-dark-grey">' . $record->operationregistered . '</a>';
            }

            $detailView = '<a target="_blank" href="/inventory/view/id/' . $record->inventoryitemid . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';
            $output['aaData'][] = array($detailView, $record->voicenumber, $record->datanumber, $record->sku, $record->productname, $_btnoperation, $record->customername, $record->datetoexecute);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function deleteAction() {

        $dto_entity = new Dto_AirtimePlan();

        $_idcrypt = trim($this->aRequest["idcrypt"]);

        $dto_entity->deleteAirtimePlan($_idcrypt);

        redirect("myairtimeplan", "index");
    }

  
}

//class