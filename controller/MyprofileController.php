<?php
require_once ("../lib/bean/Lang.php");

class MyprofileController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {

        $dto_user = new Dto_User();
        $user = $dto_user->getById(  $this->session_userid );

        $view = new ViewController();
        
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/myprofile/index.js");        
        $view->bodyjquery = "UpdateInfo.init(); UpdatePassword.init();  ";
        $view->entity = $user;
        
        $lang1 = new Lang();
        $lang1->code = 'eng';
        $lang1->name = 'English';
        
        $lang2 = new Lang();
        $lang2->code = 'esp';
        $lang2->name = 'Espa&ntilde;ol';
        
        $view->aLang = array();
        array_push($view->aLang, $lang1);
        array_push($view->aLang, $lang2);
        
        $view->load("myprofile", "index");
    }
    
    
    function requestupdateuserinfoAction(){
       $_userid = trim($_REQUEST["userid"]);
       $_firstname = trim($_REQUEST["firstname"]);
       $_lastname = trim($_REQUEST["lastname"]);
       $_mobile = trim($_REQUEST["mobile"]);
       $_phone = trim($_REQUEST["phone"]);
       $_phoneext = trim($_REQUEST["phoneext"]);
       $_lang = trim($_REQUEST["lang"]);
       
       $dto_user = new Dto_User();
       $user = new User();
       $user->id = $_userid;
       $user->firstname = $_firstname;
       $user->lastname = $_lastname;
       $user->mobile = $_mobile;
       $user->phone = $_phone;
       $user->phoneext = $_phoneext;
       $user->lang = $_lang;
       $result = false;
       
       if($_firstname !="" && $_lastname!="" && $_mobile !=""){
           $result = $dto_user->updatemyprofile($user);
       }
       
       $view = new ViewController();
       $view->template = "blank";
       $view->data = array("result"=> $result);
       $view->load("template","json");
       
    }
    
    function requestupdatepasswordAction(){
       $_userid = trim($_REQUEST["userid"]);
       $_actualpassword = trim($_REQUEST["actualpassword"]);
       $_newpassword = trim($_REQUEST["newpassword"]);
       $_confirmpassword = trim($_REQUEST["confirmpassword"]);
        
       $result = false;
       
       //we need to verify the actual password
       $dto_user = new Dto_User();
       $user = $dto_user->checkPassword($_userid, $_actualpassword);
       
       if($user->id != ""  &&  (  $_newpassword ==  $_confirmpassword ) ){
           $result =  $dto_user->updatePassword($_userid, $_newpassword);
       }
       
       $view = new ViewController();
       $view->template = "blank";
       $view->data = array("result"=> $result);
       $view->load("template","json");
       
    }
}
