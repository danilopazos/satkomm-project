<?php

require_once ("../lib/controller/EmailController.php");
require_once ("../lib/util/mailservice/template.php");

class UserController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function requestregisteruserAction() {
        $operation = "create";
        $user = $this->getRequestUser();
        $user->status = 1;
        $ok = 1;

        $dto_user = new Dto_User();
        $usernameId = $dto_user->getIdByUsername($user->username);
        if ($user->id != "") {
            $operation = "update";
            if ($usernameId != $user->id && $usernameId != "") {
                $ok = -4;
            }
        } else {
            $user->id = $dto_user->generateId($user->accountid);
            if ($usernameId != "") {
                $ok = -4;
            }
        }

        
        $isCustomer = intval($_REQUEST["iscustomer"]);
         
        if($isCustomer == 1){
            
            if ($operation == "create" && ( $user->accountid == "" || $user->username == ""  || $user->firstname == "" || $user->lastname == ""
                    || $user->position == "" || $user->country == "" || $user->state == "" 
                    || $user->city == ""  || $user->street == ""  
                    
                    )) {
                $ok = -2;
            } else if ($operation == "update" && ( $user->accountid == "" || $user->firstname == "" 
                    || $user->lastname == "" 
                    || $user->position == "" || $user->country == "" || $user->state == "" 
                    || $user->city == ""  || $user->street == ""    )) {
                $ok = -2;
            }
            
        }else{
            
            if ($operation == "create" && ( $user->accountid == "" || $user->username == "" || $user->pass == "" || $user->firstname == "" || $user->lastname == "")) {
                $ok = -2;
            } else if ($operation == "update" && ( $user->accountid == "" || $user->firstname == "" || $user->lastname == "")) {
                
                $ok = -2;
            }            
            
        }
        
        

        if ($ok == 1) {
            if ($operation == "create") {
                $result = $dto_user->create($user);
                if (!($result)) {
                    $ok = -3;
                }
            } else {
                $result = $dto_user->update($user);
                if (!($result)) {
                    $ok = -3;
                }
            }
        }

        $view = new ViewController();
        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = $user;
        }
        $view->template = "blank";
        $view->load("template", "json");
    }

    function requestgetuserbyidAction() {
        $_id = trim($this->aRequest["id"]);
        $dto_user = new Dto_User();
        $user = $dto_user->getById($_id);

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $user;
        $view->load("template", "json");
    }

    function requestchangepasswordAction() {
        $_accountid = trim($_REQUEST["accountid"]);
        $_id = trim($_REQUEST["id"]);
        $_password = trim($_REQUEST["password"]);


        if (!($this->canIcheckTheAccount($_accountid))) {
            return;
        }

        $ok = 1;
        $dto_user = new Dto_User();
        $result = $dto_user->updatePassword($_id, $_password);
        if (!($result)) {
            $ok = -1;
        }


        $view = new ViewController();
        $view->template = 'blank';

        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = array("result" => 1);
        }

        $view->load("template", 'json');
    }

    function requestchangeusernameAction() {
        $_accountid = trim($_REQUEST["accountid"]);
        $_id = trim($_REQUEST["id"]);
        $_username = trim($_REQUEST["username"]);

        if (!($this->canIcheckTheAccount($_accountid))) {
            return;
        }

        $ok = 1;
        $dto_user = new Dto_User();

        $usernameId = $dto_user->getIdByUsername($_username);

        if ($usernameId != $_id && $usernameId != "") {
            $ok = -2;
        }


        if ($ok == 1) {
            $result = $dto_user->updateUsername($_id, $_username);
            if (!($result)) {
                $ok = -1;
            }
        }

        $view = new ViewController();
        $view->template = 'blank';

        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = array("result" => 1);
        }

        $view->load("template", 'json');
    }

    function requestchangestatusAction() {
        $_accountid = trim($_REQUEST["accountid"]);
        $_id = trim($_REQUEST["id"]);
        $_status = intval($_REQUEST["status"]);
        $ok = 1;
        if (!($this->canIcheckTheAccount($_accountid))) {
            return;
        }

        $dto_user = new Dto_User();
        $result = $dto_user->updateStatus($_id, $_status);
 
        if (!($result)) {
            $ok = -1;
        }

        $view = new ViewController();
        $view->template = 'blank';

        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = array("result" => 1);
        }

        $view->load("template", 'json');
    }

    function getRequestUser() {

        $_id = trim($_REQUEST["id"]);
        $_accountid = trim($_REQUEST["accountid"]);
        $_profileid = trim($_REQUEST["profile"]);
        $_password = trim($_REQUEST["password"]);
        $_username = trim($_REQUEST["username"]);
        $_firstname = trim($_REQUEST["firstname"]);
        $_lastname = trim($_REQUEST["lastname"]);
        $_phone = trim($_REQUEST["phone"]);
        $_phoneext = trim($_REQUEST["phoneext"]);
        $_mobile = trim($_REQUEST["mobile"]);
        
        $_position = trim($_REQUEST["position"]);
        $_country = trim($_REQUEST["user_country"]);
        $_state = trim($_REQUEST["user_state"]);
        $_city = trim($_REQUEST["user_city"]);
        $_street = trim($_REQUEST["user_street"]);
        $_zipcode = trim($_REQUEST["user_zipcode"]);
        $_pobox = trim($_REQUEST["user_pobox"]);
        
        $user = new User();
        $user->id = $_id;
        $user->profileid = $_profileid;
        $user->accountid = $_accountid;
        $user->username = $_username;
        $user->pass = $_password;
        $user->firstname = $_firstname;
        $user->lastname = $_lastname;
        $user->phone = $_phone;
        $user->phoneext = $_phoneext;
        $user->mobile = $_mobile;
        
        $user->position = $_position;
        $user->country = $_country;
        $user->state = $_state;
        $user->city = $_city;
        $user->street = $_street;
        $user->zipcode = $_zipcode;
        $user->pobox = $_pobox;
        
        return $user;
    }

}
