<?php

require_once ("../lib/helper/DatatableHelper.php");
require_once ("../lib/Config.php");
require_once ("../lib/controller/MediaController.php");

class DocumentGenerateController extends BaseController {

    function __construct() {
        $this->setSession();
    }

    function imageAction() {
        $view = new ViewController ();

        $sizeFont = intval($this->aRequest["sizeFont"]);
        $defaultText = "DEFAULT";
        $view->sizeFont = 3;
        if ($sizeFont > 0) {
            $view->sizeFont = $sizeFont;
        }

        if (trim($this->aRequest["text"]) != "") {
            $defaultText = strtoupper(trim($this->aRequest["text"]));
        }

        $backR = 255;
        $backG = 255;
        $backB = 255;
        $backgroundColor = "#FFFFFF";

        if (trim($this->aRequest["background"]) != "") {
            $backgroundColor = "#" . strtoupper(trim($this->aRequest["background"]));
        }

        list($backR, $backG, $backB) = sscanf($backgroundColor, "#%02x%02x%02x");

        $fontR = 0;
        $fontG = 0;
        $fontB = 0;
        $fontColor = "#000000";

        if (trim($this->aRequest["font"]) != "") {
            $fontColor = "#" . strtoupper(trim($this->aRequest["font"]));
        }

        list($fontR, $fontG, $fontB) = sscanf($fontColor, "#%02x%02x%02x");

        $view->fontR = $fontR;
        $view->fontG = $fontG;
        $view->fontB = $fontB;

        $view->backR = $backR;
        $view->backG = $backG;
        $view->backB = $backB;

        $config = new Config();

        $view->defaultText = $defaultText;
        $view->template = "blank";


        $view->load("generator", "image2");

        $view->template = "blank";
        $view->load("generator", "image2");
    }

    public function generateDocument($view, $session, $accountId, $headDocument) {

        include "../lib/util/TCPDF-master/config/tcpdf_config.php";
        include "../lib/util/TCPDF-master/tcpdf.php";
        include "../lib/util/TCPDF-master/CustomPdf.php";
        include "../lib/util/BarCode/barcode.php";
        include "../lib/util/BarCode/TextToImage.php";
        include "../lib/helper/DocumentLang/" . strtolower($session->session_langcode) . ".php";


        $view->langWords = new LangWords();
        $dto_account = new Dto_Account();

        $media_idcrypt = $dto_account->getLogoIdByAccountid($accountId);

        $dto_media = new Dto_Media();
        $entity = $dto_media->getByIdCrypt($media_idcrypt);

        $metadata = $dto_media->getMetadataFromMediaBlob($entity->id);

        $prename = "";
        $blobcontent = "";
        $blobcontent = $dto_media->getOriginalBlobById($entity->id);

        $account = $dto_account->getById($accountId);


        $view->blobcontent = $blobcontent;
        $view->account = $account;

        $headDocument->creadtedFirstName = $session->session_firstname;
        $headDocument->creadtedLastName = $session->session_lastname;

        $view->headDocument = $headDocument;

        $view->template = "blank";
        $view->load("generator", "document");

        return $view;
    }

    public function generatePackingSlip($view, $session, $accountId, $headDocument) {

        include "../lib/util/TCPDF-master/config/tcpdf_config.php";
        include "../lib/util/TCPDF-master/tcpdf.php";
        include "../lib/util/TCPDF-master/CustomPdf.php";
        include "../lib/util/BarCode/barcode.php";
        include "../lib/util/BarCode/TextToImage.php";
        include "../lib/helper/DocumentLang/" . strtolower($session->session_langcode) . ".php";


        $view->langWords = new LangWords();
        $dto_account = new Dto_Account();

        $media_idcrypt = $dto_account->getLogoIdByAccountid($accountId);

        $dto_media = new Dto_Media();
        $entity = $dto_media->getByIdCrypt($media_idcrypt);


        $metadata = $dto_media->getMetadataFromMediaBlob($entity->id);

        $prename = "";
        $blobcontent = "";
        $blobcontent = $dto_media->getOriginalBlobById($entity->id);

        $account = $dto_account->getById($accountId);


        $view->blobcontent = $blobcontent;
        $view->account = $account;

        $headDocument->creadtedFirstName = $session->session_firstname;
        $headDocument->creadtedLastName = $session->session_lastname;

        $view->headDocument = $headDocument;

        $view->template = "blank";
        $view->load("generator", "documentpackingslip");

        return $view;
    }

    public function generateQuote($view, $session, $accountId, $headDocument, $_langcode = "eng") {

        include "../lib/util/TCPDF-master/config/tcpdf_config.php";
        include "../lib/util/TCPDF-master/tcpdf.php";
        include "../lib/util/TCPDF-master/CustomPdf.php";
        include "../lib/util/BarCode/barcode.php";
        include "../lib/util/BarCode/TextToImage.php";
        include "../lib/helper/DocumentLang/" . strtolower($_langcode) . ".php";


        $view->langWords = new LangWords();
        $dto_account = new Dto_Account();

        $media_idcrypt = $dto_account->getLogoIdByAccountid($accountId);

        $dto_media = new Dto_Media();
        $entity = $dto_media->getByIdCrypt($media_idcrypt);


        $metadata = $dto_media->getMetadataFromMediaBlob($entity->id);

        $prename = "";
        $blobcontent = "";
        $blobcontent = $dto_media->getOriginalBlobById($entity->id);

        $account = $dto_account->getById($accountId);


        $view->blobcontent = $blobcontent;
        $view->account = $account;

        $headDocument->creadtedFirstName = $session->session_firstname;
        $headDocument->creadtedLastName = $session->session_lastname;

        $view->headDocument = $headDocument;

        $view->template = "blank";
        $view->load("generator", "quote");

        return $view;
    }

    
    public function generateInvoice($view, $session, $accountId, $headDocument, $infoprovider) {

        include "../lib/util/TCPDF-master/config/tcpdf_config.php";
        include "../lib/util/TCPDF-master/tcpdf.php";
        include "../lib/util/BarCode/barcode.php";
        include "../lib/util/BarCode/TextToImage.php";
        include "../lib/helper/DocumentLang/" . strtolower($session->session_langcode) . ".php";

        //Define header to pdf
        $headerpdf="CustomPdf.php";
        if($infoprovider->headerinvoice){
            $headerpdf=$infoprovider->headerinvoice;
        }
        include "../lib/util/TCPDF-master/".$headerpdf;
        

        //Include ivoice-text for countries
        $country_code=$infoprovider->countrycode;
        if($country_code=="PE" or $country_code=="AR" or $country_code=="CO"){
            include "../lib/view/generator/nomenclature/".$country_code."/". strtolower($session->session_langcode) .".php";
        }else{
            include "../lib/view/generator/nomenclature/default/". strtolower($session->session_langcode) .".php";
        }
        $view->nomenclature = new NomenclatureCountry();

        //Add logo header pdf
        if($infoprovider->logoimg){
            $view->logo_header=$infoprovider->logoimg;
        }else{
            $view->logo_header="logo_satkomm.png";
        }

        $view->countryprovider = $infoprovider->countrycode;
        
        $view->currencyDescription = $infoprovider->currencyDescription;
        $view->currencyIso = $infoprovider->currencyIso;
        $view->currencySymbol = $infoprovider->currencySymbol;
        $view->currencyExchange = $infoprovider->currencyExchange;
        
        $view->langWords = new LangWords();
        $dto_account = new Dto_Account();

        $media_idcrypt = $dto_account->getLogoIdByAccountid($accountId);

        $dto_media = new Dto_Media();
        $entity = $dto_media->getByIdCrypt($media_idcrypt);


        $metadata = $dto_media->getMetadataFromMediaBlob($entity->id);

        $prename = "";
        $blobcontent = "";
        $blobcontent = $dto_media->getOriginalBlobById($entity->id);

        $account = $dto_account->getById($accountId);


        $view->blobcontent = $blobcontent;
        $view->account = $account;

        $headDocument->creadtedFirstName = $session->session_firstname;
        $headDocument->creadtedLastName = $session->session_lastname;

        $view->headDocument = $headDocument;

        $view->template = "blank";
        $view->load("generator", "invoice");

        return $view;
    }
    
    public function generateProviderInvoiceService($view, $session, $accountId, $headDocument) {

        include "../lib/util/TCPDF-master/config/tcpdf_config.php";
        include "../lib/util/TCPDF-master/tcpdf.php";
        include "../lib/util/TCPDF-master/CustomPdf.php";
        include "../lib/util/BarCode/barcode.php";
        include "../lib/util/BarCode/TextToImage.php";
        include "../lib/helper/DocumentLang/" . strtolower($session->session_langcode) . ".php";


        $view->langWords = new LangWords();
        $dto_account = new Dto_Account();

        $media_idcrypt = $dto_account->getLogoIdByAccountid($accountId);

        $dto_media = new Dto_Media();
        $entity = $dto_media->getByIdCrypt($media_idcrypt);


        $metadata = $dto_media->getMetadataFromMediaBlob($entity->id);

        $prename = "";
        $blobcontent = "";
        $blobcontent = $dto_media->getOriginalBlobById($entity->id);

        $account = $dto_account->getById($accountId);


        $view->blobcontent = $blobcontent;
        $view->account = $account;

        $headDocument->creadtedFirstName = $session->session_firstname;
        $headDocument->creadtedLastName = $session->session_lastname;

        $view->headDocument = $headDocument;

        $view->template = "blank";
        $view->load("generator", "providerinvoiceservice");

        return $view;
    }
    
    

    public function generateInvoicePeriod($view, $session, $headDocument, $infoprovider) {
        
        include "../lib/util/TCPDF-master/config/tcpdf_config.php";
        include "../lib/util/TCPDF-master/tcpdf.php";
        include "../lib/util/BarCode/barcode.php";
        include "../lib/util/BarCode/TextToImage.php";
        include "../lib/helper/DocumentLang/" . strtolower($session->session_langcode) . ".php";

        //Define header to pdf
        $headerpdf="CustomPdf.php";
        if($infoprovider->headerinvoice){
            $headerpdf=$infoprovider->headerinvoice;
        }
        include "../lib/util/TCPDF-master/".$headerpdf;

        //Define format design to Pdf
        $view_name_pdf="invoiceperiod";
        if($infoprovider->formatinvoice){
            $view_name_pdf=$infoprovider->formatinvoice;
        }

        //Include ivoice-text for countries
        $country_code=$infoprovider->countrycode;
        if($country_code=="PE" or $country_code=="AR" or $country_code=="CO"){
            include "../lib/view/generator/nomenclature/".$country_code."/". strtolower($session->session_langcode) .".php";
        }else{
            include "../lib/view/generator/nomenclature/default/". strtolower($session->session_langcode) .".php";
        }
        $view->nomenclature = new NomenclatureCountry();

        //Add logo header pdf
        if($infoprovider->logoimg){
            $view->logo_header=$infoprovider->logoimg;
        }else{
            $view->logo_header="logo_satkomm.png";
        }

        $view->countryprovider = $infoprovider->countrycode;
        
        $view->currencyDescription = $infoprovider->currencyDescription;
        $view->currencyIso = $infoprovider->currencyIso;
        $view->currencySymbol = $infoprovider->currencySymbol;
        $view->currencyExchange = $infoprovider->currencyExchange;

        $view->langWords = new LangWords();
        $dto_account = new Dto_Account();

        $media_idcrypt = $dto_account->getLogoIdByAccountid($infoprovider->providerid);

        $dto_media = new Dto_Media();
        $entity = $dto_media->getByIdCrypt($media_idcrypt);


        $metadata = $dto_media->getMetadataFromMediaBlob($entity->id);

        $prename = "";
        $blobcontent = "";
        $blobcontent = $dto_media->getOriginalBlobById($entity->id);

        $account = $dto_account->getById($infoprovider->providerid);   

        //Aqui esta la relación de usuario con empresa -  10003 AS TELECOM
        $view->blobcontent = $blobcontent;

        $view->account = $account;

        $headDocument->creadtedFirstName = $session->session_firstname;
        $headDocument->creadtedLastName = $session->session_lastname;

        $view->headDocument = $headDocument;

        $view->template = "blank";
        $view->load("generator", $view_name_pdf);

        return $view;
    }

    public function generateInvoiceCdr($view, $session, $accountId, $headDocument) {

        include "../lib/util/TCPDF-master/config/tcpdf_config.php";
        include "../lib/util/TCPDF-master/tcpdf.php";
        include "../lib/util/TCPDF-master/CustomPdf.php";
        include "../lib/util/BarCode/barcode.php";
        include "../lib/util/BarCode/TextToImage.php";
        include "../lib/helper/DocumentLang/" . strtolower($session->session_langcode) . ".php";


        $view->langWords = new LangWords();
        $dto_account = new Dto_Account();

        $media_idcrypt = $dto_account->getLogoIdByAccountid($accountId);

        $dto_media = new Dto_Media();
        $entity = $dto_media->getByIdCrypt($media_idcrypt);


        $metadata = $dto_media->getMetadataFromMediaBlob($entity->id);

        $prename = "";
        $blobcontent = "";
        $blobcontent = $dto_media->getOriginalBlobById($entity->id);

        $account = $dto_account->getById($accountId);


        $view->blobcontent = $blobcontent;
        $view->account = $account;

        $headDocument->creadtedFirstName = $session->session_firstname;
        $headDocument->creadtedLastName = $session->session_lastname;

        $view->headDocument = $headDocument;

        $view->template = "blank";
        $view->load("generator", "invoicecdr");

        return $view;
    }

}
