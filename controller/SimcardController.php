<?php
require_once ("../lib/bean/AirtimeOperation.php");
require_once ("../lib/bean/AirtimePlan.php");
require_once ("../lib/bean/Plan.php");
require_once ("../lib/bean/InventoryItem.php");
require_once("../lib/bean/PlanTrafficType.php");
require_once ("../lib/db/Dto_Plan.php"); 
require_once ("../lib/db/Dto_AirtimePlan.php"); 
require_once ("../lib/db/Dto_Inventory.php"); 
require_once ("../lib/db/Dto_AirtimeOperation.php"); 

class SimcardController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }
    
    function requestoperationdeactivateAction(){
        $dto_airtimeoperation = new Dto_AirtimeOperation();
        $dto_plan = new Dto_AirtimePlan();
        
        $_airtimeoperationid = trim($_REQUEST["airtimeoperationid"]);
        
        $tempAirtimeOperaion = $dto_airtimeoperation->getById($_airtimeoperationid);
         
        $entity = new AirtimeOperation();
        $entity->requestedbyuserid = $this->session_userid;
        $entity->id = $dto_airtimeoperation->generateid();
        $entity->accountassignedid = $tempAirtimeOperaion->accountassignedid;
        $entity->datetoexecute = $_REQUEST["operationdate"];
        $timetoexecute = $_REQUEST["operationtime"];
        $entity->planid = $tempAirtimeOperaion->planid;
        $entity->inventoryitemid = $tempAirtimeOperaion->inventoryitemid;
        $entity->accountid = $tempAirtimeOperaion->accountid;
        $entity->subtotal = doubleval($plan->deactivationpayment);
        $entity->paymentmethod = '';
        $entity->paymentmethod_price = 0;
        $entity->taxes = 0;      
        $entity->totalpayment = 0;
        
        $entity->timetoexecute = $this->getTimeFormat($timetoexecute);
        
        $plan = $dto_plan->getByIdCrypt($tempAirtimeOperaion->planid);
        $entity->deactivation_price = doubleval($plan->deactivationpayment);
        
        $entity->operation = "deactivate";      
        $_result = $dto_airtimeoperation->create($entity, $this->session_langcode) ;
       
       
        //we need to set the inventory in the cart
     
        $dto_inventory = new Dto_Inventory(); 
        $dto_inventory->setInventoryOperationRequested($entity->inventoryitemid, "airtime_cart");
        
      
        $view = new ViewController();
        $view->template = "blank";
        
        if($_result){ $view->data = array("id"=>$entity->id);     }
        else{   $view->data = false;  }
        
        $view->load("template", "json");        
    }
    
    function requestoperationsuspendAction(){
        $dto_airtimeoperation = new Dto_AirtimeOperation();
        $dto_plan = new Dto_AirtimePlan();
        
        $_airtimeoperationid = trim($_REQUEST["airtimeoperationid"]);
        
        $tempAirtimeOperaion = $dto_airtimeoperation->getById($_airtimeoperationid);
         
        $entity = new AirtimeOperation();
        $entity->requestedbyuserid = $this->session_userid;
        $entity->id = $dto_airtimeoperation->generateid();
        $entity->accountassignedid = $tempAirtimeOperaion->accountassignedid;
        
        $entity->planidcryptforcustomer = $tempAirtimeOperaion->planidcryptforcustomer;
        $entity->plannameforcustomer = $tempAirtimeOperaion->plannameforcustomer;
        $entity->voicenumber = $tempAirtimeOperaion->voicenumber;
        $entity->datanumber = $tempAirtimeOperaion->datanumber;
        
        $entity->datetoexecute = $_REQUEST["operationdate"];
        $timetoexecute = $_REQUEST["operationtime"];
        $entity->planid = $tempAirtimeOperaion->planid;
        $entity->inventoryitemid = $tempAirtimeOperaion->inventoryitemid;
        $entity->accountid = $tempAirtimeOperaion->accountid;
        $entity->subtotal = doubleval($plan->suspensionpayment);
        $entity->paymentmethod = '';
        $entity->paymentmethod_price = 0;
        $entity->taxes = 0;      
        $entity->totalpayment = 0;
        
        $entity->timetoexecute = $this->getTimeFormat($timetoexecute);
        
        $plan = $dto_plan->getByIdCrypt($tempAirtimeOperaion->planid);
        $entity->suspension_price = doubleval($plan->suspensionpayment);
        
        $entity->operation = "suspend";      
        $_result = $dto_airtimeoperation->create($entity, $this->session_langcode) ;
       
       
        //we need to set the inventory in the cart
     
        $dto_inventory = new Dto_Inventory(); 
        $dto_inventory->setInventoryOperationRequested($entity->inventoryitemid, "airtime_cart");
        
      
        $view = new ViewController();
        $view->template = "blank";
        
        if($_result){ $view->data = array("id"=>$entity->id);     }
        else{   $view->data = false;  }
        
        $view->load("template", "json");        
    }
    
    function requestoperationactiveAction(){

        $dto_airtimeoperation = new Dto_AirtimeOperation();
        $entity = new AirtimeOperation();
        $entity->requestedbyuserid = $this->session_userid;
        $entity->id = $dto_airtimeoperation->generateid();
        $entity->accountassignedid = $_REQUEST["accountassigned"];
        $entity->datetoexecute = $_REQUEST["operationdate"];
        $timetoexecute = $_REQUEST["operationtime"];
        $entity->planid = $_REQUEST["planid"];
        $entity->inventoryitemid = $_REQUEST["inventoryid"];
        $entity->accountid = $_REQUEST["accountid"];
        $entity->subtotal = $_REQUEST["subtotal"];
        $entity->paymentmethod = $_REQUEST["paymentmethod"];
        $entity->paymentmethod_price = $_REQUEST["paymentmethod_price"];
        $entity->taxes = $_REQUEST["taxes"];      $entity->totalpayment = $_REQUEST["totaltopay"];
        $entity->timetoexecute = $this->getTimeFormat($timetoexecute);
        
        //planforaccount
        $entity->planidcryptforcustomer = trim($_REQUEST["planforaccount"]);
        if($entity->planidcryptforcustomer != ""){
            $dto_plan = new Dto_Plan();
            $tempPlanForCustomer = $dto_plan->getPlanById($entity->planidcryptforcustomer, $this->session_langcode);
            $entity->plannameforcustomer = $tempPlanForCustomer->name;
        }
        
        $entity->operation = "activate"; 
        $_result = $dto_airtimeoperation->create($entity, $this->session_langcode) ;
        
        //we need to set the inventory in the cart
        $dto_inventory = new Dto_Inventory();
        $dto_inventory->setInventoryOperationRequested($entity->inventoryitemid, "airtime_cart");
        
        
        $view = new ViewController();
        $view->template = "blank";
        
        if($_result){ $view->data = array("id"=>$entity->id);     }
        else{   $view->data = false;  }
        
        $view->load("template", "json");
        
    }
    
    function getTimeFormat($_time){
        //7:5 PM
        $aTime1 = split(" ", $_time);
        $pm = $aTime1[1];
        $ispm = false;
        $aTime2 = split(":",$aTime1[0] );
        if($pm =="PM"){
            $ispm = true;
        }
        $hour   = intval($aTime2[0]);
        $minute = intval($aTime2[1]);
        
        $str_hour = "";
        $str_minute = "";
        
        if($ispm){   
          $hour =   $hour + 12;  
          $str_hour = $hour."";
        }else{
            if($hour <10){
                $str_hour = "0".$hour;
            }else{
                $str_hour = $hour."";
            }
        }
        

        if($minute <10){
           $str_minute = "0".$minute;
        }else{
            $str_minute = $minute."";
        }
        

        return $str_hour.":".$str_minute.":00";

        
    }
    
    function requestgetplandetailAction(){
        $_planid = trim($this->aRequest["planid"]);
        $dto_plan = new Dto_Plan();
        $plan = $dto_plan->getPlanById($_planid, $this->session_langcode);
        
        $aPlanSons = $dto_plan->getPlanForCustomer($this->session_accountid, $_planid, $this->session_langcode);
        
        $plan->aPlanSons = $aPlanSons;
        
        $view = new ViewController();
        $view->template = "blank";
        $view->data = $plan;
        
        $view->load("template", "json");
        
    }
    
    
    
}
