<?php

require_once ("../lib/bean/TrafficUnit.php");
require_once ("../lib/bean/TrafficUnitEq.php");
require_once ("../lib/db/Dto_TrafficUnit.php");
require_once ("../lib/db/Dto_TrafficUnitEq.php");

require_once ("../lib/helper/DatatableHelper.php");

class TrafficunitController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {


        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/responsive.css");

        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/trafficunit/index.js");

        $dto_entity = new Dto_TrafficUnit();
        $view->aList = $dto_entity->getAllUnits();

        $view->accountid = $this->session_accountid;
        $view->bodyjquery = "TableData.init(); Register.init();  ";
        $view->load("trafficunit", "index");
    }

    function requestgetindexAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('code', 'description', 'code');
        $_datatableSetup->_sortCol = " code ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_TrafficUnit();

        $totalRecords = $dto_entity->getListTotal($datatable->_search);
        $aRecords = $dto_entity->getList($datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {
            $linkDeleted = '<a href="#" onclick="fnc_deleteRecord( \'' . $record->id . '\' );" class="btn btn-xs btn-bricky"  ><i class="fa fa-trash-o"></i></a>';
            $linkUpdated = '<a href="#" onclick="fnc_updateRecord( \'' . $record->id . '\' );" class="btn btn-xs btn-primary"  ><i class="fa fa-pencil"></i></a>';
            $linkEq = '<a href="#" onclick="fnc_updateEquivalence( \'' . trim($record->id) . '\' ,  \'' . trim($record->code) . '\' ,  \'' . trim($record->description) . '\' );" class="btn btn-xs btn-yellow"  ><i class="fa fa-balance-scale"></i></a>';
            $output['aaData'][] = array($record->code, $record->description, $linkDeleted . ' &nbsp; ' . $linkUpdated . ' &nbsp; ' . $linkEq);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestgetequivalenceAction() {
        $_codea = trim($this->aRequest["codea"]);
        $dto_eq = new Dto_TrafficUnitEq();
        
        $view = new ViewController();
        $view->template = "blank";
        $view->data = $dto_eq->getByCodeA($_codea);
        
        $view->load("template", "json");
    }

    function requestregistereqAction() {
        $_codea = trim($_REQUEST["codea"]);
        $aRecord = $this->getRequestEq();
         
        $dto_entity = new Dto_TrafficUnitEq();
        $result = $dto_entity->registerBatch($aRecord, $_codea);
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $result);
        $view->load("template", "json");
    }

    function requestregisterAction() {
        $e = $this->getRequestRegister();
        $dto_entity = new Dto_TrafficUnit();
        $_op = "create";
        if ($e->id != "") {
            $_op = "update";
        } else {
            $e->id = $dto_entity->generateId();
        }


        $ok = 1;

        if ($e->code != "" && $e->description != "") {
            $tempentity = $dto_entity->getByCode($e);
            if ($_op == "create") {
                if ($tempentity) {
                    $ok = -1;
                }
            } else {
                if ($tempentity && $tempentity->id != $e->id) {
                    $ok = -1;
                }
            }

            $tempentity = $dto_entity->getByDescription($e);
            if ($_op == "create") {
                if ($tempentity) {
                    $ok = -4;
                }
            } else {
                if ($tempentity && $tempentity->id != $e->id) {
                    $ok = -4;
                }
            }
        } else {
            $ok = -2;
        }

        $result = true;
        if ($ok == 1) {
            if ($_op == "create") {
                $result = $dto_entity->register($e);
            } else {
                $result = $dto_entity->update($e);
            }
        }

        if (!$result) {
            $ok = -3;
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $ok);
        $view->load("template", "json");
    }

    function requestdeleteAction() {
        $result = false;
        $dto_entity = new Dto_TrafficUnit();
        $e = new TrafficUnit();
        $e->id = trim($_REQUEST["id"]);
        $result = $dto_entity->delete($e);
        $ok = 1;
        if (!$result) {
            $ok = -1;
        }
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $ok);
        $view->load("template", "json");
    }

    function requestgetbyidAction() {
        $dto_entity = new Dto_TrafficUnit();
        $e = new TrafficUnit();
        $e->id = trim($this->aRequest["id"]);
        $e = $dto_entity->getById($e);
        $view = new ViewController();
        $view->template = "blank";
        $view->data = $e;
        $view->load("template", "json");
    }

    function getRequestRegister() {
        $_id = trim($_REQUEST["id"]);
        $_code = trim($_REQUEST["code"]);
        $_description = trim($_REQUEST["description"]);
        $e = new TrafficUnit();
        $e->id = $_id;
        $e->description = $_description;
        $e->code = $_code;
        return $e;
    }

    function getRequestEq() {
        $_total = intval($_REQUEST["total"]);
        $_codea = trim($_REQUEST["codea"]);
        $aRecord = array();
        for ($i = 0; $i < $_total; $i++) {
            $e = new TrafficUnitEq();
            $_codeb = null;
            if (isset($_REQUEST["eqindex_" . $i])) {
                $_codeb = trim($_REQUEST["eqindex_" . $i]);
            }
            $qtya = null;  
            if (isset($_REQUEST["eq_" . $_codeb])) {
                $qtya = doubleval($_REQUEST["eq_" . $_codeb]);
            }
           
                $e->codea = $_codea;
                $e->codeb = $_codeb;
                $e->qtya = $qtya;
                array_push($aRecord, $e);
             
        }
        return $aRecord;
    }

}
