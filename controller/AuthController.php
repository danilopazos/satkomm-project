<?php
require_once ("../lib/controller/EmailController.php");
require_once ("../lib/controller/SmsController.php");
require_once ("../lib/util/mailservice/template.php");

require_once ("../lib/bean/UsersystemByIp.php");
require_once ("../lib/bean/UsersystemLoginLog.php");
require_once ("../lib/bean/UsersystemLoginToken.php");
require_once ("../lib/db/Dto_Login.php");

class AuthController extends BaseController {
    
    function verificationcodeAction(){
        $_username = trim($this->aRequest["username"]);
        $dto_user = new Dto_User();
        $user = $dto_user->getByUsername($_username);
       
        $email = trim($user->username);
        $mobile = trim($user->mobile);
        
        $totalEmailCharacters = strlen($email);
        $tempemail_part01 = substr($email, 0, 2);
        $postAt = strpos($email, "@");
        $tempemail_part02 = substr($email, $postAt , strlen($email)  );
        $pendingEmailCharacters =  intval($totalEmailCharacters - (strlen($tempemail_part01) + strlen($tempemail_part02)));
        $dotspending = '';
        while(strlen($dotspending) < $pendingEmailCharacters ){
            $dotspending .= '.';
        }
        
        $lastemail = $tempemail_part01.''.$dotspending.''.$tempemail_part02;
        
        $temp2mobile = substr($mobile, strlen($mobile) - 4 , strlen($mobile)  ) ;
        $pendingMobileCharacters = intval(strlen($mobile)  - 5);
        $dotspending = '';
        while(strlen($dotspending) < $pendingMobileCharacters ){
            $dotspending .= 'x';
        }
        $lastmobile = $dotspending.''.$temp2mobile;
        
     
        
        $view = new ViewController();
        
        array_push($view->aJavascript, "assets/js/modules/auth/verificationcode.js");

        $view->bodyjquery = "VerificationCode.init();  ";        
        
        //we need to set the email and mobile phone
        $view->username = $user->username;
        $view->email = $lastemail;
        $view->mobile = $lastmobile;
        $view->template = "blank";
        $view->global_accountid = $this->global_accountid;
        $view->load("auth", "verificationcode");
    }
    
   function requestvalidateidentificationcodeAction(){
        $_identificationcode = trim($_REQUEST["identificationcode"]);
        $_username = trim($_REQUEST["username"]);
        
        $dto_user = new Dto_User();
        $user = $dto_user->getByUsername($_username);
       
        $_tokencode = new UsersystemLoginToken();
        $_tokencode->iptovalidate = get_ip();
        $_tokencode->userid = $user->id;
        $_tokencode->validationcode = $_identificationcode;
        
        $dto_login = new Dto_Login();
        $tokenid = $dto_login->validateTokenCode($_tokencode);
        
        $ok = false;
        if(trim($tokenid) !=""){
            if(strlen($tokenid) > 10){
                $result = $dto_login->setTokenValidated($tokenid);
                if($result){
                    $ok = true;
                    
                }
            }
        }
        
        if($ok){
            $entity = new UsersystemByIp();
            $entity->id = $dto_login->generateId();
            $entity->ip = get_ip();
            $entity->userid = $user->id;
            $ok = $dto_login->registerip($entity);
        }
        
        $view = new ViewController();
        $view->data = array("result"=>$ok);  
        $view->template = "blank";
        $view->load("template", "json");
   }
    
    function requestsentidentificationcodeAction(){
        $_destination = trim($_REQUEST["destination"]);
        $_username = trim($_REQUEST["username"]);
        $dto_user = new Dto_User();
        $user = $dto_user->getByUsername($_username);
        $dto_login = new Dto_Login();
        
        
        $finaldestination = '';
        if($_destination == 'email'){
            $finaldestination = $user->username;
        }else{
            $finaldestination = $user->mobile;
        }
        
        $usersystemLoginToken = new UsersystemLoginToken();
        $usersystemLoginToken->id = $dto_login->generateId();
        $usersystemLoginToken->userid = $user->id;
        $usersystemLoginToken->iptovalidate = get_ip();
        $identificationcode = rand(100000, 999999 );
        $usersystemLoginToken->validationcode = $identificationcode;
        $usersystemLoginToken->validationtype = $_destination;
        $usersystemLoginToken->validationdest = $finaldestination;
        
        $result = $dto_login->registertokencode($usersystemLoginToken);
        $sendStatus = false;
        if($result){
            if($_destination == "email"){
                $sendStatus = $this->sendIdentificationCodeEmail($finaldestination, $identificationcode);
            }else if($_destination =="mobile"){
                $sendStatus = $this->sendIdentificationCodeSms($finaldestination, $identificationcode);
            }
        }
        
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result"=>$result);
        $view->load("template", "json");
        
    }
    
    function sendIdentificationCodeSms($_number, $_identificationcode){
        $controller = new SmsController();
        $controller->send($_number, 'The verification code of your Satkomm Dashboard is: '.$_identificationcode);
        return true;
    }
    
    function sendIdentificationCodeEmail($_email, $identificationcode){
        $controller = new EmailController();
        $result = $controller->send($_email, "", "Satkomm Dashboard - Verification code", 'The verification code of your Satkomm Dashboard is: '.$identificationcode );
        if($result == 1){
            return true;
        }
        return false;
    }
    
    
    
    function indexAction() {
        
        /*
        if (trim($_SESSION ["session_userid"]) != "") {
            redirect("dashboard", "index");
        }
        */
        
    //existencia de la sesion
    //if(isset($_SESSION ["session_userid"])){
        if ( !(  trim($_SESSION ["session_userid"]) == ""  ||  
                $_SESSION ["session_userid"] == null  ||  
                !(isset($_SESSION ["session_userid"])) ||  
                
                !(intval($_SESSION ["session_userid"]) > 0)
                
            )  
                ) {
            redirect("dashboard", "index");
        }        
    //}
        
        $view = new ViewController ();
        $view->template = "blank";
        $view->global_accountid = $this->global_accountid;
        $view->load("auth", "index");
    }

    function signoutAction() {

        $_SESSION ["session_userid"] = "";
        $_SESSION ["session_username"] = "";
        $_SESSION ["session_profileid"] = "";
        redirect("auth", "index");
    }

    function forgotAction() {

        if (intval($_SESSION ["session_userid"]) > 0) {
            redirect("dashboard", "index");
        }

        $view = new ViewController ();
        $view->template = "blank";
        $view->global_accountid = $this->global_accountid;
        $view->load("auth", "forgot");
    }
    
    function validateforgotAction(){
        
        $_token = trim($this->aRequest["token"]);
        
        if($_token != ""){
            $dto_auth = new Dto_Auth();
            $forgotid = $dto_auth->validateForgot($_token);
        
            if($forgotid   == 0){
                $_token = "";
            }
        }        
        
        $view = new ViewController ();
        $view->template = "blank";
        $view->global_accountid = $this->global_accountid;
        $view->token = $_token;
        $view->load("auth", "validateforgot");
        
    }
    
    
    function requestforgotAction() {
        $_username = trim($_REQUEST["username"]);
        $dto_auth = new Dto_Auth();
        $token = $dto_auth->authForgot($_username);

        if ($token != "") {

            $activationlinkTxt = getCurrentUrlPath() . "/auth/validateforgot/token/".$token;
            $activationlink = '<a href="' . $activationlinkTxt . '">' . $activationlinkTxt . "</a>";

            $emailcontent = getPasswordReset($activationlink);

            $emailController = new EmailController();

            $name = "";
            $email = $_username;
            $emailsubject = "Password Reset";
            $emailsent = $emailController->send($email, $name, $emailsubject, $emailcontent);
            if($emailsent == 0 ){
                    $token = "";
            }
        }


        $view = new ViewController();
        $view->template = "blank";
        $view->data = $token;
        $view->load("template", "json");
    }

    function requestauthAction() {
        $_username = trim($_REQUEST["username"]);
        $_password = $_REQUEST["password"];
        $_timezonecustomer = $_REQUEST["timezonecustomer"];
        

        $dto_auth = new Dto_Auth();
        $user = $dto_auth->authIndex($_username, $_password);
        
        if ( $user->id != "") {
            
            //We need to check if the ip was validated
            $dto_login  = new Dto_Login();
            
            
//            if(!$dto_login->getIfIpExistsByUserid($user->id, get_ip())){
//            
//                $user->id = "verificationcode";
//                
//            }else{
                
                //$dto_login = new Dto_Login();
                $usersystemLoginLog = new UsersystemLoginLog();
                $usersystemLoginLog->id = $dto_login->generateId();
                $usersystemLoginLog->userid = $user->id;
                $usersystemLoginLog->userip = get_ip();
                $dto_login->registerlog($usersystemLoginLog);
                
                $_SESSION ["session_userid"] = $user->id;
                $_SESSION ["session_username"] = $user->username;
                $_SESSION ["session_profileid"] = $user->profileid;
                
                /*
                $timezone_offset_minutes = $_timezonecustomer;  // $_GET['timezone_offset_minutes']
                $timezone_name = timezone_name_from_abbr("", $timezone_offset_minutes*60, false);
                
                echo "timezone_offset_minutes: ".$timezone_offset_minutes."<br />";
                echo "El timezone es: ".$timezone_name; die();
                */
                $timezone_name = timezone_name_from_abbr($_timezonecustomer);
                $_SESSION ["session_timezonename"] = $timezone_name;
                
                
//            }
        } else {
            $_SESSION ["session_userid"] = "";
            $_SESSION ["session_username"] = "";
            $_SESSION ["session_profileid"] = "";
            $_SESSION ["session_timezonename"] = "";
        }


        $view = new ViewController();
        $view->template = "blank";
        $view->data = $user;
        $view->load("template", "json");
    }

    function requestpasswordchangeAction(){
        $_password = trim($_REQUEST["password"]);
        $_passwordconfirmation = trim($_REQUEST["confirmpassword"]);
        $_token = trim($_REQUEST["token"]);
        $output = 0;
        if($_password != "" && ($_password == $_passwordconfirmation)  ){
           $dto_auth = new Dto_Auth();
           if($dto_auth->changePasswordByForgot($_token, $_password)){
               $output = 1;
           }
        }
        
        $view = new ViewController ();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
        
        
    }
    
}
