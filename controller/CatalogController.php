<?php
require_once ("../lib/bean/Product.php");
require_once ("../lib/bean/ProductDetail.php");
require_once ("../lib/db/Dto_Product.php");

require_once ("../lib/helper/DatatableHelper.php");

class CatalogController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/catalog/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("catalog", "index");
    }

    function requestgetdatatablelistAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('p.id',    'p.sku', 'pd.name', 'p.network_code' , 'pb.buy' );
        $_datatableSetup->_sortCol = "pd.name";
        $_datatableSetup->_sortDir = "asc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_product = new Dto_Product();
        $totalRecords = $dto_product->getListByCatalogTotal($this->session_langcode  ,  $datatable->_search );
        $aRecords = $dto_product->getListByCatalog($this->session_accountid, $this->session_langcode  , $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array(   "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()   );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {
            
            $distprice =   $record->distprice ;
            
            $link_productdetail1 = '<a href="#"  onclick="fnc_productdetail(\'' . $record->idcrypt . '\')"    class="btn btn-xs btn-default  user-viewmodal  "  >'.$record->sku.'</a>';
            $link_productdetail2 = '<a href="#"  onclick="fnc_productdetail(\'' . $record->idcrypt . '\')"    class="btn btn-xs btn-default  user-viewmodal  "  >'.$record->name.'</a>';
            
            $link_distprice = '<a href="#"  onclick="fnc_addcart(\'' . $record->idcrypt . '\' ,  \'' . $record->name . '\'   ,  \'' . $record->distprice . '\'  )"    class="btn btn-xs btn-default  user-viewmodal  ">USD '.moneyFormat($distprice).'  </a>';
            $link_addcart = '<a href="#"  onclick="fnc_addcart(\'' . $record->idcrypt . '\' ,  \'' . $record->name . '\'   ,  \'' . $record->distprice . '\'  )"    class="btn btn-default  user-viewmodal  ">   <i class="fa fa-cart-plus"></i>  </a>';
      
            $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/'.$record->mediaid.'/type/thumb"  >';
            
            $output['aaData'][] = array( $image_product, $link_productdetail1   , $link_productdetail2  , $record->network_code   ,   $link_distprice  , $link_addcart      );
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }
    
    function requestgetproductbyidAction(){
        $_idcrypt = trim($this->aRequest["idcrypt"]);
        
        $dto_product = new Dto_Product();
        $entity = $dto_product->getDetailById($this->session_langcode, $_idcrypt);
        
        $view = new ViewController();
        $view->data = $entity;
        $view->template = "blank";
        $view->load("template", "json");
        
    }
    
    
}