<?php

require_once ("../lib/bean/InvoicePayment.php");
require_once ("../lib/db/Dto_InvoicePayment.php");
require_once ("../lib/db/Dto_Account.php");
require_once ("../lib/helper/DatatableHelper.php");

class InvoicepaymentController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {

        $view = new ViewController ();


        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/invoicepayment/index.js");
        $view->bodyjquery = "TableData.init();  ";

        $dto_ip = new Dto_InvoicePayment();

        if (intval($this->session_accountid) == 10001) {
            $view->aInvoices = $dto_ip->getProviderInvoicesFromCustomersSummary($this->session_accountid);
        } else {
            $view->aInvoices = $dto_ip->getInvoicesFromCustomersSummary($this->session_accountid);
        }


        $view->load("invoicepayment", "index");
    }

    function mystatementAction() {

        $customerid = $this->session_accountid;

        $dto_account = new Dto_Account();
        $tempaccount = $dto_account->getById($customerid);

        $view = new ViewController ();

        $view->customerid = $tempaccount->id;


       // array_push($view->aJavascript, "assets/js/modules/invoicepayment/customerinvoices.js");


        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aCss, "assets/plugin/bootstrap-timepicker.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-timepicker/css/timepicker.less");

        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");


        $view->accountname = $tempaccount->name;
        $view->creditlimit = $tempaccount->creditlimit;
        $dto_ip = new Dto_InvoicePayment();


        $view->aInvoices = $dto_ip->getProviderInvoicesFromCustomers(10001, $customerid);


        $view->session_accountid = intval($this->session_accountid);


       // $view->bodyjquery = "ModalPayment.init();";

        $view->load("invoicepayment", "mystatement");
    }

    function customerinvoicesAction() {

        $customerid = intval($this->aRequest["customerid"]);

        $dto_account = new Dto_Account();
        $tempaccount = $dto_account->getById($customerid);

        $view = new ViewController ();

        $view->customerid = $tempaccount->id;


        array_push($view->aJavascript, "assets/js/modules/invoicepayment/customerinvoices.js");


        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aCss, "assets/plugin/bootstrap-timepicker.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-timepicker/css/timepicker.less");

        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");


        $view->accountname = $tempaccount->name;
        $view->creditlimit = $tempaccount->creditlimit;
        $dto_ip = new Dto_InvoicePayment();

        if (intval($this->session_accountid) == 10001) {
            $view->aInvoices = $dto_ip->getProviderInvoicesFromCustomers($this->session_accountid, $customerid);
        } else {
            $view->aInvoices = $dto_ip->getInvoicesFromCustomers($this->session_accountid, $customerid);
        }

        $view->session_accountid = intval($this->session_accountid);


        $view->bodyjquery = "ModalPayment.init();";

        $view->load("invoicepayment", "customerinvoices");
    }

    function savepaymentAction() {
        //array(5) { ["paymentammount"]=> string(0) "" ["paymentdate"]=> string(10) "2017-11-13" ["customerid"]=> string(5) "10006" ["invoiceid"]=> string(1) "4" ["paymentref"]=> string(0) "" } 
        $_paymentammount = doubleval($_REQUEST["paymentammount"]);
        $_tableinvoice = trim($_REQUEST["tableinvoice"]);
        $_paymentdate = trim($_REQUEST["paymentdate"]);
        $_customerid = intval($_REQUEST["customerid"]);
        $_invoiceid = intval($_REQUEST["invoiceid"]);
        $_paymentref = trim($_REQUEST["paymentref"]);

        if (intval($this->session_accountid) == 10001) {
            $_tableinvoice = "invoice";
        }


        $dto_ip = new Dto_InvoicePayment();
        $dto_ip->savePayment($this->session_accountid, $_customerid, $_tableinvoice, $_invoiceid, $_paymentammount, $_paymentdate, $_paymentref, $this->session_userid);

        redirect("invoicepayment", "customerinvoices", array("customerid" => $_customerid));
    }

    function myinvoicesAction() {

        $view = new ViewController ();

        $dto_ip = new Dto_InvoicePayment();
        $view->aInvoices = $dto_ip->getMyInvoices($this->session_accountid);

        $view->load("invoicepayment", "myinvoices");
    }

}
