<?php

require_once ("../lib/bean/Account.php");
require_once ("../lib/bean/Quote.php");
require_once ("../lib/bean/DocumentProduct.php");
require_once ("../lib/bean/DocumentService.php");

require_once ("../lib/bean/ItemAutocomplete.php");
require_once ("../lib/bean/ShippingProvider.php");

require_once ("../lib/bean/HeadDocumentExport.php");
require_once ("../lib/bean/DetailProductExport.php");


require_once ("../lib/bean/InvoiceToCustomer.php");
require_once ("../lib/bean/InventoryItemSimple.php");
require_once ("../lib/bean/InventoryItem.php");

require_once ("../lib/bean/InvoicetcDispatchService.php");
require_once ("../lib/bean/InvoicetcDispatchDevice.php");
//----------------------------------------------------------

require_once ("../lib/db/Dto_Account.php");
require_once ("../lib/db/Dto_Document.php");
require_once ("../lib/db/Dto_Quote.php");
require_once ("../lib/db/Dto_ShippingProvider.php");
require_once ("../lib/db/Dto_InvoiceToCustomer.php");

require_once ("../lib/controller/MediaController.php");
require_once ("../lib/controller/DocumentgenerateController.php");


require_once ("../lib/util/mailservice/template.php");
require_once ("../lib/controller/EmailController.php");

require_once("../lib/bean/InfoToConsumptionPdf.php");


class DocumentemailController extends BaseController {

    function __construct() {
        $this->setSession();
    }

    public function sendattachAction() {
        $_langcode = $this->session_langcode;
        $fileLang = "../lib/view/documentemail/lang/" . $_langcode . ".php";

        if (file_exists($fileLang)) {
            require_once($fileLang);
        }

        $_doctype = trim($this->aRequest["doctype"]);
        $_id = trim($this->aRequest["id"]);
        $_subject = utf8_decode(trim($_REQUEST["e_subject"]));
        $_to = trim($_REQUEST["e_to"]);
        $_description = nl2br(utf8_decode(trim($_REQUEST["e_description"])));
        $_from = trim($_REQUEST["e_from"]);
        $_fromname = trim($_REQUEST["e_fromname"]);
        $_documentid = trim($_REQUEST["documentid"]);
        $_attachname = trim($_REQUEST["e_attachname"]);

        $_fileurl = "";

        if ($_doctype == "quote") {
            $_fileurl = getCurrentUrlPath() . "/documentemail/downloadquote/id/$_documentid/langcode/" . $_langcode;
        } else {
            $_fileurl = getCurrentUrlPath() . "/documentemail/downloadinvoicetc/id/$_documentid/langcode/" . $_langcode;
        }

        $_content = getDocumentEmail($_subject, $_description, $_lang_download_info, $_attachname, $_fileurl);

        $aTo = split(",", $_to);


        $emailController = new EmailController();
        $_ok = $emailController->sendArray($aTo, "", $_subject, $_content, $_from, $_fromname);

        $_result = true;
        if ($_ok == 0) {
            $_result = false;
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $_result);
        $view->load("template", "json");
    }

    public function downloadquoteAction($docidcrypt = "", $_langcode = "") {


        if ($docidcrypt == "") {
            $docidcrypt = trim($this->aRequest["id"]);
        }
        if ($_langcode == "") {
            $_langcode = trim($this->aRequest["langcode"]);
        }
        if ($_langcode == "") {
            $_langcode = "eng";
        }


        $view = new ViewController();


        $fileLangTemplate = "../lib/view/quote/lang/" . $_langcode . ".php";
        include $fileLangTemplate;



        $dto_quote = new Dto_Quote();
        $invoice = $dto_quote->getByIdCrypt($docidcrypt);

        $aProducts = $dto_quote->getProductsByDocumentId($docidcrypt, 'quote');
        $aServices = $dto_quote->getServicesByDocumentId($docidcrypt, 'quote');

        $aDetailProduct = array();
        $totalProducts = count($aProducts);
        $totalServices = count($aServices);

        if ($totalProducts > 0) {
            foreach ($aProducts as $detail) {
                $entity = new DetailProductExport();

                $entity->codeProduct = $detail->sku;

                $itemDescription = "";
                if ($detail->months > 0 && $invoice->operationtype == "rent") {
                    $itemDescription .= "<b>" . $_lang_product_months . "</b>  " . $detail->months . "\n";
                }
                $itemDescription .= $detail->description;
                $entity->description = $itemDescription;

                $entity->nameProduct = $detail->name;
                $entity->quantity = $detail->qty;
                $entity->price = $detail->price;

                $productTotalprice = doubleval($detail->price) * doubleval($detail->qty);
                if ($detail->months > 0 && $invoice->operationtype == "rent") {
                    $productTotalprice = doubleval($detail->price) * doubleval($detail->qty) * doubleval($detail->months);
                }


                $entity->totalPrice = $productTotalprice;

                array_push($aDetailProduct, $entity);
            }
        }

        if ($totalServices > 0) {
            foreach ($aServices as $detail) {
                $entity = new DetailProductExport();

                $entity->codeProduct = $detail->sku;

                $itemDescription = "";
                if ($detail->activationprice > 0) {
                    $itemDescription .= "<b>" . $_lang_service_activation . "</b>  " . moneyFormat($detail->activationprice) . "\n";
                }
                $itemDescription .= "<b>" . $_lang_service_name . "</b>  " . moneyFormat($detail->price) . "\n";
                $itemDescription .= $detail->description;
                $entity->description = $itemDescription;


                $entity->nameProduct = $detail->name;
                $entity->quantity = $detail->qty;
                $entity->price = $detail->price + $detail->activationprice;
                $entity->totalPrice = doubleval($detail->price + $detail->activationprice) * doubleval($detail->qty);

                array_push($aDetailProduct, $entity);
            }
        }

        $headDocument = new HeadDocumentExport();
        $headDocument->accountName = $invoice->customer_name;
        $headDocument->numberDocument = $invoice->ordercode;
        $headDocument->contactPhone = $invoice->phonenumber;
        $headDocument->contactFirstName = $invoice->contact_names;
        $headDocument->contactPhone = $invoice->contacttelephone;
        $headDocument->contactEmail = $invoice->contactemail;

        //$headDocument->contactLastName = $invoice->userLastName;

        $headDocument->salesManFisrtName = $invoice->assigned_names;
        //$headDocument->salesManLastName = $invoice->userLastName;

        $headDocument->comments = $invoice->obs;
        $headDocument->dateDocument = $invoice->quotedate;
        $headDocument->dateDueDocument = $invoice->quoteuntil;

        $headDocument->billStreet = $invoice->bill_street;
        $headDocument->billCity = $invoice->bill_city;
        $headDocument->billState = $invoice->bill_state;
        $headDocument->billCode = $invoice->bill_zipcode;
        $headDocument->billCountry = $invoice->bill_country;

        $headDocument->webSiteAccount = $invoice->webSite;
        $headDocument->phoneContact = $invoice->phonenumber;

        $headDocument->shippingAddress = $invoice->ship_street;
        $headDocument->shippingCountry = $invoice->ship_country;
        $headDocument->shippingState = $invoice->ship_state;
        $headDocument->shippingCity = $invoice->ship_city;
        $headDocument->shippingZipCode = $invoice->ship_zipcode;

        $headDocument->paymentFee = $invoice->payment_fee;
        $headDocument->parcialtotal = $invoice->subtotal;
        $headDocument->taxes = $invoice->taxespercent;
        $headDocument->shippingCost = $invoice->shippingcost;
        $headDocument->total = $invoice->total;

        $headDocument->colorBanner = "#F71907";
        $headDocument->textBanner = ""; //Here we put the banner id
        $headDocument->fontBanner = 100;


        $accountid = $invoice->accountid;

        $view->detailDocument = $aDetailProduct;

        $view->code = $invoice->quoteno;

        $view->lang_index_table = $_lang_quote_title;
        $view->lang_invoice_number = $_lang_register_label_quoteno;
        $view->lang_invoice_date = $_lang_register_label_quotedate;
        $view->lang_invoice_due_date = $_lang_register_label_quoteuntil;
        $view->typeDocument = $_lang_index_label_module;
        $view->lang_document_intro = $_lang_quote_intro;

        $view->contact_salutation = $invoice->contactsalutation;


        $view->credit_terms = $_lang_label_paymentterms;
        $view->credit_terms_info = "";
        if ($invoice->paymentterms != "") {
            $view->credit_terms_info = $_lang_paymentterms[$invoice->paymentterms];
        }
        if ($invoice->paymenttermsother != "") {
            $view->credit_terms_info = $invoice->paymenttermsother;
        }



        $view->shipping_company = $_lang_label_shippingcompany;
        $view->shipping_company_info = "";
        if ($invoice->carriercompany != "") {
            $view->shipping_company_info = $invoice->carriercompany;
        }
        if ($invoice->paymenttermsother != "") {
            $view->shipping_company_info = $invoice->carriercompanyother;
        }

        $view->currency_code = $invoice->currency_code;


        $document = new DocumentGenerateController();
        $document->generateQuote($view, $this, $accountid, $headDocument, $_langcode);
    }

//function

    public function downloadinvoicetcAction($docidcrypt = "", $_langcode = "") {



        if ($docidcrypt == "") {
            $docidcrypt = trim($this->aRequest["id"]);
        }

        if ($_langcode == "") {
            $_langcode = trim($this->aRequest["langcode"]);
        }
        if ($_langcode == "") {
            $_langcode = "eng";
        }

        $view = new ViewController();
        $dto_account = new Dto_Account();

        $fileLangTemplate = "../lib/view/invoicetc/lang/" . strtolower($_langcode) . ".php";
        include $fileLangTemplate;

        $dto_quote = new Dto_InvoiceToCustomer();
        $invoice = $dto_quote->getByIdCrypt($docidcrypt);

        $infoprovider = $dto_account->infoToGeneratePdf($invoice->accountid, $invoice->customerid);
        
        $aProducts = $dto_quote->getProductsByDocumentId($docidcrypt, 'invoicetc');
        $aServices = $dto_quote->getServicesByDocumentId($docidcrypt, 'invoicetc');

        $totalProducts = count($aProducts);
        $totalServices = count($aServices);

        $aDetailProduct = array();
        
        if ($totalProducts > 0) {
            foreach ($aProducts as $detail) {
                $entity = new DetailProductExport();

                $entity->codeProduct = $detail->sku;

                $itemDescription = "";
                if ($detail->months > 0 && $invoice->operationtype == "rent") {
                    $itemDescription .= "<b>" . $_lang_product_months . "</b>  " . $detail->months . "\n";
                }
                $itemDescription .= $detail->description;
                $entity->description = $itemDescription;

                $entity->nameProduct = $detail->name;
                $entity->quantity = $detail->qty;
                $entity->price = $detail->price;

                $productTotalprice = doubleval($detail->price) * doubleval($detail->qty);
                if ($detail->months > 0 && $invoice->operationtype == "rent") {
                    $productTotalprice = doubleval($detail->price) * doubleval($detail->qty) * doubleval($detail->months);
                }


                $entity->totalPrice = $productTotalprice;

                array_push($aDetailProduct, $entity);
            }
        }

        if ($totalServices > 0) {
            foreach ($aServices as $detail) {
                $entity = new DetailProductExport();

                $entity->codeProduct = $detail->sku;

                $itemDescription = "";
                if ($detail->activationprice > 0) {
                    $itemDescription .= "<b>" . $_lang_service_activation . "</b>  " . moneyFormat($detail->activationprice) . "\n";
                }
                $itemDescription .= "<b>" . $_lang_service_name . "</b>  " . moneyFormat($detail->price) . "\n";
                $itemDescription .= $detail->description;
                $entity->description = $itemDescription;

                $entity->nameProduct = $detail->name;
                $entity->quantity = $detail->qty;
                $entity->price = $detail->price + $detail->activationprice;
                $entity->totalPrice = doubleval($detail->price + $detail->activationprice) * doubleval($detail->qty);

                array_push($aDetailProduct, $entity);
            }
        }

        $headDocument = new HeadDocumentExport();
        $headDocument->accountName = $invoice->customer_name;
        $headDocument->numberDocument = $invoice->ordercode;
        $headDocument->contactPhone = $invoice->phonenumber;
        $headDocument->contactFirstName = $invoice->contact_names;
        $headDocument->contactPhone = $invoice->contacttelephone;
        $headDocument->contactEmail = $invoice->contactemail;

        //$headDocument->contactLastName = $invoice->userLastName;

        $headDocument->salesManFisrtName = $invoice->assigned_names;
        //$headDocument->salesManLastName = $invoice->userLastName;

        $headDocument->comments = $invoice->obs;
        $headDocument->dateDocument = $invoice->invoicedate;
        $headDocument->dateDueDocument = $invoice->invoicedate;

        $headDocument->billStreet = $invoice->bill_street;
        $headDocument->billCity = $invoice->bill_city;
        $headDocument->billState = $invoice->bill_state;
        $headDocument->billCode = $invoice->bill_zipcode;
        $headDocument->billCountry = $invoice->bill_country;

        $headDocument->webSiteAccount = $invoice->webSite;
        $headDocument->phoneContact = $invoice->phonenumber;

        $headDocument->shippingAddress = $invoice->ship_street;
        $headDocument->shippingCountry = $invoice->ship_country;
        $headDocument->shippingState = $invoice->ship_state;
        $headDocument->shippingCity = $invoice->ship_city;
        $headDocument->shippingZipCode = $invoice->ship_zipcode;

        $headDocument->paymentFee = $invoice->payment_fee;
        $headDocument->parcialtotal = $invoice->subtotal;
        $headDocument->taxes = $invoice->taxespercent;
        $headDocument->shippingCost = $invoice->shippingcost;
        $headDocument->total = $invoice->total;

        $headDocument->colorBanner = "#F71907";
        $headDocument->textBanner = ""; //Here we put the banner id
        $headDocument->fontBanner = 100;


        $accountid = $invoice->accountid;

        $view->detailDocument = $aDetailProduct;

        $view->code = $invoice->invoiceno;

        $view->lang_index_table = $_lang_quote_title;
        $view->lang_invoice_number = $_lang_register_label_quoteno;
        $view->lang_invoice_date = $_lang_register_label_quotedate;
        $view->lang_invoice_due_date = $_lang_register_label_quoteuntil;
        $view->typeDocument = $_lang_index_label_module;
        $view->lang_document_intro = $_lang_quote_intro;

        $view->contact_salutation = $invoice->contactsalutation;


        $view->credit_terms = $_lang_label_paymentterms;
        $view->credit_terms_info = "";
        if ($invoice->paymentterms != "") {
            $view->credit_terms_info = $_lang_paymentterms[$invoice->paymentterms];
        }
        if ($invoice->paymenttermsother != "") {
            $view->credit_terms_info = $invoice->paymenttermsother;
        }



        $view->shipping_company = $_lang_label_shippingcompany;
        $view->shipping_company_info = "";
        if ($invoice->carriercompany != "") {
            $view->shipping_company_info = $invoice->carriercompany;
        }
        if ($invoice->paymenttermsother != "") {
            $view->shipping_company_info = $invoice->carriercompanyother;
        }


        $view->purchaseorder = $_lang_register_label_purchaseorder;
        $view->purchaseorder_info = $invoice->purchaseorderno;
        $view->shipping_tracking = $_lang_register_label_carriertracking;
        $view->shipping_tracking_info = $invoice->carriertracking;

        $view->currency_code = $invoice->currency_code;
        //die();
        $document = new DocumentGenerateController();
        $document->generateInvoice($view, $this, $accountid, $headDocument, $infoprovider);
    }

//function
}

//class