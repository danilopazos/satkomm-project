<?php
require_once ("../lib/bean/AccountConfig.php");
require_once ("../lib/bean/Account.php");
require_once ("../lib/bean/Quote.php");
require_once ("../lib/bean/DocumentProduct.php");
require_once ("../lib/bean/DocumentService.php");

require_once ("../lib/bean/Currency.php");
require_once ("../lib/bean/CurrencyExchange.php");
require_once ("../lib/bean/ItemAutocomplete.php");
require_once ("../lib/bean/ShippingProvider.php");

require_once ("../lib/bean/HeadDocumentExport.php");
require_once ("../lib/bean/DetailProductExport.php");

require_once ("../lib/db/Dto_Currency.php");
require_once ("../lib/db/Dto_Account.php");
require_once ("../lib/db/Dto_Document.php");
require_once ("../lib/db/Dto_Quote.php");
require_once ("../lib/db/Dto_ShippingProvider.php");

require_once ("../lib/helper/DatatableHelper.php");

require_once ("../lib/controller/DocumentemailController.php");
require_once ("../lib/controller/MediaController.php");
require_once ("../lib/controller/DocumentgenerateController.php");

class QuoteController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/quote/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("quote", "index");
    }

    function viewAction() {
        
        $docidcrypt = trim($this->aRequest["id"]);
        $dto_quote = new Dto_Quote();
        $quote = $dto_quote->getByIdCrypt( $docidcrypt  );
        $aProducts = $dto_quote->getProductsByDocumentId($docidcrypt, 'quote');
        $aServices = $dto_quote->getServicesByDocumentId($docidcrypt, 'quote');
        
        $view = new ViewController();
        
        
        //------------------------
  
        $dto_account = new Dto_Account();
        $e_account = $dto_account->getById( $this->session_accountid  );
        $view->e_subject = $e_account->name.' -  Cotizaci&oacute;n # '.$quote->quoteno.' '.$this->session_firstname.' '.$this->session_lastname ;
        $view->e_attach = $quote->quoteno.'.pdf';
        $view->e_from = $this->session_username;
        $view->e_fromname = $this->session_firstname.' '.$this->session_lastname;
        $view->e_to = $quote->contactemail;
         
        //---------------------------
        
        
        array_push($view->aJavascript, "assets/js/modules/quote/view.js");
        $view->quote = $quote;
        $view->aProduct = $aProducts;
        $view->aService = $aServices;
        $view->bodyjquery = "Quote.init(); ";
        $view->load("quote", "view");
    }
    
    function requestdeleteAction(){
        
        $_idcrypt = trim($_REQUEST["id"]);
        
        $dto_quote = new Dto_Quote();
        $_result = $dto_quote->delete($_idcrypt, $this->session_userid);
        
        $ok = 0;
        if($_result){
            $ok = 1;
        }
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result"=>$ok);
        $view->load("template", "json");
    }
    
    
    function registerAction() {
        
        $dto_account = new Dto_Account();
        
        $view = new ViewController();
        $view->accountconfig = $dto_account->getAccountConfig($this->session_accountid);
        $dto_quote = new Dto_Quote();
        $dto_user = new Dto_User();

        $quote = new Quote();

        $dto_currency = new Dto_Currency();
        $view->aCurrency = $dto_currency->getListActive($this->session_accountid);
        
        
        
        $view->aStatus = array('C', 'S', 'A', 'R');
        $view->aSalesman = $dto_user->getByAccountid($this->session_accountid);
        $view->aPaymentTerms = array("prepaid", "15", "30", "45", "60", "90", "00");
        
        $view->aDeliveryTerms = array("pickupinoffice", "sendingbycurier", "shippingnotincluded", "other" );
        
        $dto_sp = new Dto_ShippingProvider();
        $view->aCarrierCompany = $dto_sp->getAll();
        $view->aOperationType = array("S", "R");
        $view->aHasTaxes = array(1, 0);


        $quote->quoteno = $dto_quote->generateQuoteNo($view->accountconfig->quoteprefix, $this->session_accountid);
        $quote->assignedto = $this->session_userid;
        $quote->hastaxes = 1;
        
        
        
        $view->quote = $quote;
        

        $quote->taxespercent = doubleval($view->accountconfig->conftaxespercent);
        
        array_push($view->aCss, "assets/bower_components/jquery-autocomplete/jquery-ui.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aCss, "assets/bower_components/jquery-autocomplete/style.css");
        array_push($view->aCss, "assets/bower_components/select2/select2.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css");


        array_push($view->aJavascript, "assets/js/modules/saleorder/index.js");
        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");

        array_push($view->aJavascript, "assets/bower_components/jquery-autocomplete/jquery.mockjax.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-autocomplete/jquery.autocomplete.js");
        array_push($view->aJavascript, "assets/bower_components/select2/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js");

        array_push($view->aJavascript, "assets/js/modules/quote/register.js");

        $view->bodyjquery = "Quote.init(); Register.init();  ";

        
        $docidcrypt = trim($this->aRequest["id"]);
        if($docidcrypt != ""){
            $view->quote = $dto_quote->getByIdCrypt( $docidcrypt  );
            $view->aProduct = $dto_quote->getProductsByDocumentId($docidcrypt, 'quote');
            $view->aService = $dto_quote->getServicesByDocumentId($docidcrypt, 'quote');
            
            $dto_user = new Dto_User();
            $view->aContact = $dto_user->getByAccountid($view->quote->customerid)   ;
         } 
        
        
        $view->load("quote", "register");
    }

    function requestlistAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('q.quoteno', 'q.operationtype', 'ac.name', 'q.status', 'q.subject', 'asi.firstname', 'q.quotedate', 'q.quoteuntil', 'q.total' );
        $_datatableSetup->_sortCol = "q.quotedate";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_quote = new Dto_Quote();
        $totalRecords = $dto_quote->getListTotal($this->session_accountid, $datatable->_search);
        $aRecords = $dto_quote->getList($this->session_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array("sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array());

        $fileLangTemplate = "../lib/view/quote/lang/" . strtolower($this->session_langcode) . ".php";
        if(file_exists($fileLangTemplate)){
            include $fileLangTemplate;
        }
        
        
        foreach ($aRecords as $record) {

            $quotetotal =    $record->total ;

        
            $output['aaData'][] = array(
                $record->quoteno, $_lang_aoperationtype[$record->operationtype], $record->customer_name, $_lang_quotestatus[$record->status], $record->subject, 
                $record->assigned_names, $record->quotedate, $record->quoteuntil, $record->currency_code.' '.$quotetotal   ,
                '<a class="btn btn-xs btn-primary" href="../quote/view/id/'.$record->idcrypt.'"><i class="fa fa-folder"></i></a>'
            );
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestgetproductbyidAction() {
        $_idcrypt = trim($this->aRequest["idcrypt"]);

        $dto_product = new Dto_Product();
        $entity = $dto_product->getDetailById($this->session_langcode, $_idcrypt);

        $view = new ViewController();
        $view->data = $entity;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function getproductsrequestAction() {

        $_type = trim($_REQUEST["type"]);
        $_search = trim($_REQUEST["name_startsWith"]);

        $_operation = trim($_REQUEST["operationtype"]);
        if ($_operation == "") {
            $_operation = 'S';
        }
        
        $_customeraccountid = intval($_REQUEST["customeraccountid"]);

        $dto_quote = new Dto_Quote();
        $aRecord = array();

        if ($_type == "product") {
            $aRecord = $dto_quote->getProducts($this->session_accountid, $_customeraccountid , $_operation, strtoupper($this->session_langcode), $_search);
        } else if ($_type == "service") {
            $_productlist = trim($_REQUEST["productlist"]);
            $aRecord = $dto_quote->getServices($this->session_accountid, $_customeraccountid  , $_operation, strtoupper($this->session_langcode), $_search, $_productlist);
        }

        $view = new ViewController();
        $view->data = $aRecord;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function getcontactsAction() {
        $_accountid = intval($this->aRequest["accountid"]);
        $dto_user = new Dto_User();
        $aRecord = $dto_user->getByAccountid($_accountid);

        $view = new ViewController();
        $view->data = $aRecord;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function getcustomerAction() {

        $_search = trim($_REQUEST["name_startsWith"]);

        $dto_account = new Dto_Account();
        $aRecord = $dto_account->getListByName($this->session_accountid, $_search, "a.id", "asc", 0, 100);

        $view = new ViewController();
        $view->data = $aRecord;
        $view->template = "blank";
        $view->load("template", "json");
    }

    function requestregisterAction() {
 
        $_e = $this->getQuoteRequest();

        $_action = "create";
        if ($_e->idcrypt != "") {
            $_action = "update";
        } else {
            $_e->idcrypt = md5(date('YmdHis')) . '' . rand(10000, 99999) . '' . rand(10000, 99999);
        }
        $ok = 1;

        if ($_e->customerid == "" || $_e->contactid == "" || $_e->subject == "") {
            $ok = -1;
        }


        $_result = false;
        $_resultproducts = false;
        $_resultservices = false;
        if ($ok == 1) {
            $dto_quote = new Dto_Quote();
            if ($_action == "create") {
                $_result = $dto_quote->register($_e);
            } else {
                $_result = $dto_quote->update($_e);
            }

            if ($_result) {
                //we need to update the items
                $_resultproducts = $dto_quote->updateProducts($_e->idcrypt, $_e->aProduct);
                $_resultservices = $dto_quote->updateServices($_e->idcrypt, $_e->aService);
            }
        }


        if ($_result && $_resultproducts && $_resultservices) {
            $ok = 1;
        }


        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $ok, "id" => $_e->idcrypt);
        $view->load("template", "json");
    }

    function getQuoteRequest() {
        $_idcrypt = trim($_REQUEST["id"]);

        $_code = trim($_REQUEST["code"]);
        $_status = trim($_REQUEST["status"]);
        $_subject = trim($_REQUEST["subject"]);
        $_salesman = trim($_REQUEST["salesman"]);
        $_quotedate = trim($_REQUEST["quotedate"]);
        $_quoteuntil = trim($_REQUEST["quoteuntil"]);
        $_customerid = trim($_REQUEST["customerid"]);
        $_contactid = trim($_REQUEST["contactid"]);
        $_contacttelephone = trim($_REQUEST["contacttelephone"]);
        $_contactmobile = trim($_REQUEST["contactmobile"]);
        $_contactemail = trim($_REQUEST["contactemail"]);
        $_contactsalutation = trim($_REQUEST["contactsalutation"]);
        $_paymentterms = trim($_REQUEST["paymentterms"]);
        $_paymenttermsother = trim($_REQUEST["paymenttermsother"]);
        $_carriercompany = trim($_REQUEST["carriercompany"]);
        $_carriercompanyother = trim($_REQUEST["carriercompanyother"]);
        $_obs = trim($_REQUEST["obs"]);
        $_bill_country = trim($_REQUEST["bill_country"]);
        $_bill_state = trim($_REQUEST["bill_state"]);
        $_bill_city = trim($_REQUEST["bill_city"]);
        $_bill_street = trim($_REQUEST["bill_street"]);
        $_bill_zipcode = trim($_REQUEST["bill_zipcode"]);
        $_bill_pobox = trim($_REQUEST["bill_pobox"]);
        $_ship_country = trim($_REQUEST["ship_country"]);
        $_ship_state = trim($_REQUEST["ship_state"]);
        $_ship_city = trim($_REQUEST["ship_city"]);
        $_ship_street = trim($_REQUEST["ship_street"]);
        $_ship_zipcode = trim($_REQUEST["ship_zipcode"]);
        $_ship_pobox = trim($_REQUEST["ship_pobox"]);

        $_deliveryterms = trim($_REQUEST["deliveryterms"]);
        $_deliverytermsother = trim($_REQUEST["deliverytermsother"]);
        
        $_acurrency_code = trim($_REQUEST["currency_code"]);
        $_currency_exchange = doubleval($_REQUEST["currency_exchange"]);
        
        $SplitCurrencyCode = split("-_-", $_acurrency_code);
        $_currency_code = trim($SplitCurrencyCode[0]);
        
        
        //$_shippingenabled = trim($_REQUEST["shippingenabled"]);

        $discount = doubleval($_REQUEST["discount"]);
        $hastaxes = doubleval($_REQUEST["hastaxes"]);
        $subtotal = doubleval($_REQUEST["subtotal"]);
        $shippingcost = doubleval($_REQUEST["shippingcost"]);
        $taxespercent = doubleval($_REQUEST["taxespercent"]);
        $operationtype = trim($_REQUEST["operationtype"]);
        $total = doubleval($_REQUEST["total"]);

        if($_status ==""){
            $_status = "C";
        }
        
        

        $e = new Quote();
        $e->idcrypt = $_idcrypt;
        $e->code = $_code;
        $e->status = $_status;
        $e->subject = $_subject;
        $e->assignedto = $_salesman;
        $e->quotedate = $_quotedate;
        $e->quoteuntil = $_quoteuntil;
        $e->customerid = $_customerid;
        $e->contactid = $_contactid;
        $e->contacttelephone = $_contacttelephone;
        $e->contactmobile = $_contactmobile;
        $e->contactemail = $_contactemail;
        $e->contactsalutation = $_contactsalutation;
        $e->paymentterms = $_paymentterms;
        $e->paymenttermsother = $_paymenttermsother;
        $e->carriercompany = $_carriercompany;
        $e->carriercompanyother = $_carriercompanyother;
        $e->obs = $_obs;
        $e->operationtype = $operationtype;
        $e->accountid = $this->session_accountid;
        $e->shippingenabled = $_shippingenabled;
            
        $e->currency_code = $_currency_code;
        $e->currency_exchange = $_currency_exchange;
        
        
        $e->deliveryterms = $_deliveryterms;
        $e->deliverytermsother = $_deliverytermsother;
        
        $e->bill_country = $_bill_country;
        $e->bill_state = $_bill_state;
        $e->bill_city = $_bill_city;
        $e->bill_street = $_bill_street;
        $e->bill_zipcode = $_bill_zipcode;
        $e->bill_pobox = $_bill_pobox;

        $e->ship_country = $_ship_country;
        $e->ship_state = $_ship_state;
        $e->ship_city = $_ship_city;
        $e->ship_street = $_ship_street;
        $e->ship_zipcode = $_ship_zipcode;
        $e->ship_pobox = $_ship_pobox;

        $e->discount = $discount;
        $e->hastaxes = $hastaxes;
        $e->subtotal = $subtotal;
        $e->shippingcost = $shippingcost;
        $e->taxespercent = $taxespercent;
        $e->total = $total;

        $e->createdby = $this->session_userid;
        $e->modifiedby = $this->session_userid;
        $e->deletedby = $this->session_userid;

        $_shippingenabled = "off";
        if (trim($_REQUEST["controlControlShipping"]) == "on") {
            $_shippingenabled = "on";
        }
        $e->shippingenabled = $_shippingenabled;

        //we need to setup de the products
        $aProduct = array();
        $itemindex = 1;
        if ($_REQUEST["productrowid"] != null) {
            $totalProduct = count($_REQUEST["productrowid"]);
            if ($totalProduct > 0) {
                foreach ($_REQUEST["productrowid"] as $_rowid) {
                    $_pq = new DocumentProduct();
                    $_pq->idcrypt = trim($_REQUEST["productid_" . $_rowid]);
                    $_pq->name = trim($_REQUEST["productname_" . $_rowid]);
                    $_pq->description = trim($_REQUEST["productdesc_" . $_rowid]);
                    $_pq->price = doubleval($_REQUEST["productprice_" . $_rowid]);
                    $_pq->qty = doubleval($_REQUEST["productqty_" . $_rowid]);
                    $_pq->months = doubleval($_REQUEST["productmonths_" . $_rowid]);
                    $_pq->subtotal = $_pq->price * $_pq->months * $_pq->qty;
                    $_pq->operationtype = trim($_REQUEST["operationtype_" . $_rowid]);
                    $_pq->doctype = "quote";
                    $_pq->itemindex = $itemindex;
                    array_push($aProduct, $_pq);
                    $itemindex ++;
                }
            }
        }

        $aService = array();
        $itemindex = 1;
        if ($_REQUEST["servicerowid"] != null) {
            $totalService = count($_REQUEST["servicerowid"]);
            if ($totalService > 0) {
                foreach ($_REQUEST["servicerowid"] as $_rowid) {
                    $_sq = new DocumentService();
                    $_sq->idcrypt = trim($_REQUEST["serviceid_" . $_rowid]);
                    $_sq->name = trim($_REQUEST["servicename_" . $_rowid]);
                    $_sq->description = trim($_REQUEST["servicedesc_" . $_rowid]);
                    $_sq->price = doubleval($_REQUEST["serviceprice_" . $_rowid]);
                    $_sq->qty = doubleval($_REQUEST["serviceqty_" . $_rowid]);
                    $_sq->activationprice = doubleval($_REQUEST["serviceactivation_" . $_rowid]);
                    $_sq->subtotal = ( $_sq->price + $_sq->activationprice ) * $_sq->qty;
                    $_sq->doctype = "quote";
                    $_sq->itemindex = $itemindex;
                    array_push($aService, $_sq);
                    $itemindex ++;
                }
            }
        }

        $e->aProduct = $aProduct;
        $e->aService = $aService;

        return $e;
    }
    
    
    function downloadAction(){ 
        
        $_d = new DocumentemailController();
        $_d->downloadquoteAction($this->aRequest["id"], strtolower($this->session_langcode));
        
       
    }
    
    

}
