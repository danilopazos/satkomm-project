<?php
require_once ("../lib/bean/Product.php");
require_once ("../lib/bean/ProductDetail.php");
require_once ("../lib/bean/Account.php");
require_once ("../lib/bean/SpecialProductPrice.php");

require_once ("../lib/db/Dto_MyProduct.php");
require_once ("../lib/db/Dto_Account.php");

require_once ("../lib/helper/DatatableHelper.php");

class MyproductsController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {
        
        if($this->session_accountid > 10001){
            $dto_product = new Dto_MyProduct();
            $dto_product->generatemyproducts($this->session_parentaccountid, $this->session_accountid, $this->session_userid);
        }
        
        $dto_account = new Dto_Account();
        
        $view = new ViewController ();

        $view->aCustomer = $dto_account->getListByName($this->session_accountid, "", "a.name", "asc", 0, 100);
        
        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js");
        array_push($view->aJavascript, "assets/js/modules/myproducts/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("myproducts", "index");
    }
    
    function requestgetdatatablelistAction() {
     
        $fileLangTemplate = "../lib/view/myproducts/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate;        
        
        $_datatableSetup = new Datatable();
        $_accountid = intval($this->session_accountid);
        
        if( $_accountid == 10001){
            $_datatableSetup->_aColumns = array('p.id',    'p.sku', 'pd.name', 'p.network_code' , 'pb.price', 'pl.buy',  'pl.rentpermonth',    '  ( (  ( pl.buy - pb.price  )   *100)/pb.price  )      '  
             , " ( select count(*) as total from productlistprice where  accountid = $_accountid  and customeraccountid > 0 and mainidcrypt = p.idcrypt ) "   );
        }else{
            $_datatableSetup->_aColumns = array('p.id',    'p.sku', 'pd.name', 'p.network_code' , 'pb.buy' , 'pl.buy',   'pl.rentpermonth',   ' ( ( ( pl.buy - pb.buy  )   *100)/pb.buy  )   '
             , " ( select count(*) as total from productlistprice where  accountid = $_accountid  and customeraccountid > 0 and mainidcrypt = p.idcrypt ) "   );
        }
        
        $_datatableSetup->_sortCol = "pd.name";
        $_datatableSetup->_sortDir = "asc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_product = new Dto_MyProduct();
        $totalRecords = $dto_product->getListByCatalogTotal( $this->session_accountid,   $this->session_langcode  ,  $datatable->_search );
        $aRecords = $dto_product->getListByCatalog($this->session_accountid,  $this->session_userid ,   $this->session_langcode  , $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        
         
        $totalFiltered = count($aRecords);
        $output = array(   "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()   );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {

            $link_productdetail1 = '<a href="#"  onclick="fnc_productdetail(\'' . $record->idcrypt . '\')"    class="btn btn-xs btn-default  user-viewmodal  "  >'.$record->sku.'</a>';
            $link_productdetail2 = '<a href="#"  onclick="fnc_productdetail(\'' . $record->idcrypt . '\')"    class="btn btn-xs btn-default  user-viewmodal  "  >'.$record->name.'</a>';
            
            $distprice =   $record->distprice ;
            $margin = moneyFormat(  $record->buymargin )  ; 
            
            $classBtnMargin = "";
            if(doubleval($margin) == 0){
                $btnMargin = '<a href="#" class="btn btn-xs btn-default ">'.$margin.'%</a>';
            }else if(doubleval($margin) > 0 ){
                $btnMargin = '<a href="#" class="btn btn-xs btn-green ">'.$margin.'%</a>';
            }else if(doubleval($margin) < 0 ){
                $btnMargin = '<a href="#" class="btn btn-xs btn-red ">'.$margin.'%</a>';
            }
            
            $buy =   $record->buy  ;
            $buyisenabled  = intval($record->buyisenabled);
            $rentpermonth =          $record->rentpermonth   ;
            $rentpermonthisenabled = intval($record->rentpermonthisenabled);            
           
            if($buyisenabled == 0){
                $buy = '<strike>'.$buy.'</strike>';
            }
            
            if($rentpermonthisenabled == 0){
                $rentpermonth = '<strike>'.$rentpermonth.'</strike>';
            }
            
            
            $productbuy         = '<a href="#"  onclick="fnc_loadprices(\'' . $record->idcrypt . '\'   ,  \''.$record->name.'\'  )"    class="btn btn-default  user-viewmodal  ">  '.'USD '.moneyFormat($buy).'  </a>';
            $productrentbymonth = '<a href="#"  onclick="fnc_loadprices(\'' . $record->idcrypt . '\'  ,  \''.$record->name.'\'   )"    class="btn btn-default  user-viewmodal  ">  '.'USD '.moneyFormat($rentpermonth).'  </a>';
            
             
            $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/'.$record->mediaid.'/type/thumb"  >';
            
            $_totalcustom = $record->totalcustom;
            
            $_linkcustom = "";
            if($_totalcustom > 0){
                $_linkcustom = '<a href="#" onclick="fnc_loadspecialprices(\'' . $record->idcrypt . '\' ,  \''.$record->name.'\'    )" class="btn btn-xs btn-primary"> '.$_lang_view_label_specialprices.' ( '.$_totalcustom.' )</a>';
            }else{
                $_linkcustom = '<a href="#"  class="btn btn-xs btn-default"> '.$_lang_view_label_specialprices.' ( '.$_totalcustom.' )</a>';
            }
            
            $output['aaData'][] = array( $image_product, $link_productdetail1   , $link_productdetail2  , $record->network_code  
                    , 'USD '.moneyFormat($distprice)  ,    $productbuy ,  $productrentbymonth  , $btnMargin , $_linkcustom );
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }
    
    function requestgetproductbyidAction(){
        $_idcrypt = trim($this->aRequest["idcrypt"]);
        
        $dto_product = new Dto_MyProduct();
        $entity = $dto_product->getDetailById($this->session_langcode, $_idcrypt);
        
        $view = new ViewController();
        $view->data = $entity;
        $view->template = "blank";
        $view->load("template", "json");
        
    }
    
    function requestgetpricesbyidAction(){
        $_idcrypt = trim($this->aRequest["idcrypt"]);
        $_customeraccountid = intval($this->aRequest["customeraccountid"]);
        $dto_product = new Dto_MyProduct();
        $entity = $dto_product->getPricesById($this->session_accountid, $_idcrypt , $_customeraccountid);
        
        $view = new ViewController();
        $view->data = $entity;
        $view->template = "blank";
        $view->load("template", "json");
        
    }
    
    function requestupdatepriceAction(){
        
        $_idcrypt = trim($_REQUEST["idcrypt"]);
        $_buy = doubleval($_REQUEST["buy"]);
        $_rentpermonth = doubleval($_REQUEST["rentpermonth"]);
        $_customeraccountid = intval($_REQUEST["customeraccountid"]);
        
        $_buyisenabled = 0;
        $_rentpermonthisenabled = 0;
        
        if($_REQUEST["buyisenabled"] == "on"){
            $_buyisenabled = 1;
        }
        if($_REQUEST["rentpermonthisenabled"] == "on"){
            $_rentpermonthisenabled = 1;
        }
        
        
        $dto_product = new Dto_MyProduct();
        $_result = false;
        
        $productprice = $dto_product->getPricesById($this->session_accountid, $_idcrypt , $_customeraccountid); 
        
        if($productprice->idcrypt !=""){
            
            $_distprice =  $productprice->distprice;
            if($_distprice ==0 ){$_distprice = 1; }
            $tempMargin = $_buy - $_distprice; 
            $buymargin  = ($tempMargin*100) /$_distprice;

            $_result = $dto_product->updateProductListPrice($this->session_accountid, $this->session_userid, $_idcrypt, $_buy, $_rentpermonth, $_buyisenabled, $_rentpermonthisenabled, $buymargin , $_customeraccountid);
        }else{
            //echo 'VAMOS A INSERTAR EL PRECIO '; die();
            //$_result = $dto_product->updateProductListPrice($this->session_accountid, $this->session_userid, $_idcrypt, $_buy, $_rentpermonth, $_buyisenabled, $_rentpermonthisenabled, $buymargin , $_customeraccountid);
            $buymargin = 0;
            $_result = $dto_product->insertProductListPrice($this->session_accountid, $this->session_userid, $_idcrypt, $_buy, $_rentpermonth, $_buyisenabled, $_rentpermonthisenabled, $buymargin ,  $_customeraccountid);
        }
        
        $view = new ViewController();
        $view->data = array("result", $_result );
        $view->template = "blank";
        $view->load("template", "json");
        
        
    }
    
    function requestspecialpricesAction(){
        
        $dto_product = new Dto_MyProduct();
        $aRecord = $dto_product->getSpecialPrices($this->session_accountid, trim($this->aRequest["idcrypt"]));
            
        $view = new ViewController();
        $view->data = $aRecord  ;
        $view->template = "blank";
        $view->load("template", "json");       
    }
    
    
    
}