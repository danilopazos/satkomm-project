<?php

require_once ("../lib/bean/InventoryItemHistory.php");
require_once ("../lib/bean/InventoryItem.php");
require_once ("../lib/bean/SimcardActivated.php");
require_once ("../lib/bean/DeviceOnSale.php");
require_once ("../lib/bean/Plan.php");
require_once ("../lib/bean/Invoice.php");
require_once ("../lib/bean/PlanTrafficType.php");
require_once ("../lib/bean/AirtimeOperation.php");
require_once ("../lib/bean/InventoryAirtimeOperation.php");

require_once ("../lib/db/Dto_Inventory.php");
require_once ("../lib/db/Dto_Plan.php");
require_once ("../lib/db/Dto_Invoice.php");
require_once ("../lib/db/Dto_AirtimeOperation.php");

require_once ("../lib/helper/DatatableHelper.php");

class InventoryController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function viewAction() {

        $_id = $this->aRequest["id"];
        $dto_inventory = new Dto_Inventory();
        $dto_plan = new Dto_Plan();
        $dto_account = new Dto_Account();
        $dto_invoice = new Dto_Invoice();
         
        $view = new ViewController();
         
        $view->id = $_id;
        $view->accountid = $this->session_accountid;
        $view->entity = $dto_inventory->getItemByid($_id);

        $view->aviableplans = $dto_plan->getPlansByProduct($view->entity->productid, $this->session_langcode, $this->session_accountid, $this->session_parentaccountid);
        $view->invoice = $dto_invoice->getByCode($view->entity->documentcode);

        $view->customerAccounts = $dto_account->getMyCustomersAndMe($this->session_accountid);
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");

        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aCss, "assets/plugin/bootstrap-timepicker.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-timepicker/css/timepicker.less");

        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js");

        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");

        $view->inventoryHistory = $dto_inventory->getInventoryHistoryById($_id);

        
        if ($view->entity->issimcard == 1 || $view->entity->gawosc == 1) {
            array_push($view->aJavascript, "assets/js/modules/inventory/viewsimcard.js");
       
      
            $dto_airtimeoperation = new Dto_AirtimeOperation();
            $airtimeoperation = $dto_airtimeoperation->getByInventoryItemId($_id, 1);
            $plan = $dto_plan->getPlanById($airtimeoperation->planid, 'ENG');

            $view->airtimeoperation = $airtimeoperation;
            $view->plan = $plan;

            $view->aAirtimeOperation = $dto_airtimeoperation->getListByInventoryItemId($_id);
            
          
            $view->bodyjquery = "ViewSimcard.init();  ";
            $view->load("inventory", "view");
        } else {
            $view->load("inventory", "view");
        }
    }

    function mydevicesAction() {


        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/responsive.css");

        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aCss, "assets/plugin/bootstrap-timepicker.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-timepicker/css/timepicker.less");

        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");



        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/inventory/mydevices.js");

        $view->accountid = $this->session_accountid;
        $view->bodyjquery = "TableNotInventory.init(); TableOnInventory.init();  TableOnCustomer.init(); TableOnOther.init(); ";
        $view->load("inventory", "mydevices");
    }

    function mysimcardsAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/responsive.css");

        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aCss, "assets/plugin/bootstrap-timepicker.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-timepicker/css/timepicker.less");

        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/inventory/mysimcards.js");

        $view->accountid = $this->session_accountid;
        $view->bodyjquery = "LoadControls.init(); TableNotInventory.init(); TableOnInventory.init(); TableActivated.init(); TableDataSuspend.init(); TableDataDeactivated.init();  TablePendingToActivate.init();    ";
        $view->load("inventory", "mysimcards");
    }

    function requestgetmyinventoryAction() {

        $fileLang = "../lib/view/inventory/lang/" . trim($this->session_langcode) . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        }


        $_accountid = intval($this->aRequest["accountid"]);
        $_issimcard = intval($this->aRequest["issimcard"]);
        $_inventorystatus = intval($this->aRequest["inventorystatus"]);

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('id', 'iserial', 'documentcode', 'sku', 'productname', 'productprice', 'datereceived', 'voicenumber', 'operationrequested', 'operationregistered');
        $_datatableSetup->_sortCol = " datereceived ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Inventory();

        $totalRecords = $dto_entity->getMyInventoryTotal("", $_accountid, $_issimcard, $datatable->_search, $_inventorystatus);
        $aRecords = $dto_entity->getMyInventory("", $_accountid, $_issimcard, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2, $_inventorystatus);


        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {
            $image_product = '<img width="45" height="45" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $record->mediaid . '/type/thumb"  >';

            if ($record->issimcard == 1) {
                $detailView = '<a href="/inventory/view/id/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';
            } else {
                $detailView = '<a href="/inventory/view/id/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';
            }

            $status = "";
            if ($record->operationregistered == "activate") {
                $status = '<a href="#" class="btn btn-xs btn-green">' . $record->operationregistered . '</a>';
            } else if ($record->operationregistered == "suspend") {
                $status = '<a href="#" class="btn btn-xs btn-orange">' . $record->operationregistered . '</a>';
            } else if ($record->operationregistered == "deactivate") {
                $status = '<a href="#" class="btn btn-xs btn-black">' . $record->operationregistered . '</a>';
            }

            $operationrequested = "";

            if ($record->operationrequested == "airtime_cart") {
                $operationrequested = '<a href="/airtimeoperation/mycart/" class="btn btn-xs btn-primary"><i class="fa fa-shopping-cart"> &nbsp; On cart</i></a>';
            } else if ($record->operationrequested == "airtime_order") {
                $operationrequested = '<a href="/airtimeoperation/searchairtimeorder/iserial/' . $record->iserial . '" class="btn btn-xs btn-primary"><i class="fa fa-plane"></i>  &nbsp; On order</a>';
            }

            $airtimedetails = '';
            if ($record->planid != "") {
                $airtimedetails .= "<strong>" . $record->planname . "</strong><br />";
            }
            if ($record->voicenumber != "") {
                $airtimedetails .= "<strong>Voice #:</strong>&nbsp; " . $record->voicenumber . "<br />";
            }
            if ($record->datanumber != "") {
                $airtimedetails .= "<strong>Data #:</strong>&nbsp; " . $record->datanumber . "<br />";
            }


            if (intval($record->inventorystatus) > 0) {

                $_inventorycheck = '<a href="javascript: modal_setdispatch(\'' . $record->id . '\' );"   class="btn btn-xs btn-blue"><i class="fa fa-dropbox"></i> &nbsp; ' . $_lang_inventory_registermovement . '</a>';
            } else {
                $_inventorycheck = '<a href="javascript: modal_setoninventory(\'' . $record->id . '\' );"   class="btn btn-xs btn-blue"><i class="fa fa-check"></i> &nbsp; ' . $_lang_inventory_registerentrance . '</a>';
            }



            $output['aaData'][] = array($image_product, $detailView, $record->documentcode, $record->sku, $record->productname, moneyFormat($record->productprice), $record->statusdate, $airtimedetails, $_inventorycheck, $status . '<br />' . $operationrequested);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestgetinventorysimcardsnotarrivedAction() {

        $fileLang = "../lib/view/inventory/lang/" . trim($this->session_langcode) . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        }

        $_accountid = intval($this->aRequest["accountid"]);
        $_issimcard = intval($this->aRequest["issimcard"]);
        $_status = trim($this->aRequest["status"]);
        $_inventorystatus = intval($this->aRequest["inventorystatus"]);


        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('id', 'iserial', 'documentcode', 'sku', 'productname', 'productprice', 'inv.invoicedate', 'inv.dispatchdate');
        $_datatableSetup->_sortCol = " inv.dispatchdate ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Inventory();

        $totalRecords = 0;
        $aRecords = array();


        $totalRecords = $dto_entity->getMyInventorySimcardTotal($_status, $_accountid, $_issimcard, $datatable->_search, $_inventorystatus);
        $aRecords = $dto_entity->getMyInventorySimcard($_status, $_accountid, $_issimcard, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2, $_inventorystatus);


        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {

            $image_product = '<img width="45" height="45" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $record->mediaid . '/type/thumb"  >';

            $detailView = '<a href="/inventory/view/id/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';

            $invoicelink = '<a target="_blank" class="btn btn-xs btn-default" href="/invoice/view/code/' . $record->documentcode . '">' . $record->documentcode . '</a>';

            $_inventorycheck = '<a href="javascript: modal_setoninventory(\'' . $record->id . '\' );"   class="btn btn-xs btn-blue"><i class="fa fa-check"></i> &nbsp; ' . $_lang_inventory_registerentrance . '</a>';

            $output['aaData'][] = array($image_product, $detailView, $invoicelink, $record->sku, $record->productname, 'USD ' . moneyFormat($record->productprice), substr($record->invoicedate, 0, 10), $record->dispatchdate, $_inventorycheck);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestgetinventorysimcardsoninventoryAction() {

        $fileLang = "../lib/view/inventory/lang/" . trim($this->session_langcode) . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        }

        $_accountid = intval($this->aRequest["accountid"]);
        $_issimcard = intval($this->aRequest["issimcard"]);
        $_status = trim($this->aRequest["status"]);
        $_inventorystatus = intval($this->aRequest["inventorystatus"]);


        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('id', 'iserial', 'documentcode', 'sku', 'productname', 'productprice', 'inv.invoicedate', 'inv.dispatchdate', 'i.datereceived');
        $_datatableSetup->_sortCol = " i.datereceived ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Inventory();

        $totalRecords = 0;
        $aRecords = array();


        $totalRecords = $dto_entity->getMyInventorySimcardTotal($_status, $_accountid, $_issimcard, $datatable->_search, $_inventorystatus);
        $aRecords = $dto_entity->getMyInventorySimcard($_status, $_accountid, $_issimcard, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2, $_inventorystatus);


        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {

            $image_product = '<img width="45" height="45" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $record->mediaid . '/type/thumb"  >';

            $detailView = '<a href="/inventory/view/id/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';

            $invoicelink = '<a target="_blank" class="btn btn-xs btn-default" href="/invoice/view/code/' . $record->documentcode . '">' . $record->documentcode . '</a>';

            $output['aaData'][] = array($image_product, $detailView, $invoicelink, $record->sku, $record->productname, 'USD ' . moneyFormat($record->productprice), substr($record->invoicedate, 0, 10), $record->dispatchdate, $record->datereceived);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestgetsimcardspendingtoexecuteAction() {

    
        
        $fileLang = "../lib/view/inventory/lang/" . trim($this->session_langcode) . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        }

        $_accountid = intval($this->aRequest["accountid"]);
        $_issimcard = intval($this->aRequest["issimcard"]);
        $_status = trim($this->aRequest["status"]);
        $_inventorystatus = intval($this->aRequest["inventorystatus"]);
        
        $_requeststatus  = 'ordered';
        

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('ao.id', 'ao.iserial', 'ao.voicenumber', 'ao.datanumber', 'ao.productname', 'ao.servicetype_code', 'ao.planname', 'ao.plannameforcustomer', ' acc.name' , 'ao.processeddatetime');
        $_datatableSetup->_sortCol = " ao.processeddatetime ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Inventory();

        $totalRecords = 0;
        $aRecords = array();


        $totalRecords = $dto_entity->getSimcardsActivatedTotal( $this->session_accountid, $_status,  $datatable->_search , $_requeststatus );
        $aRecords = $dto_entity->getSimcardsActivated($this->session_accountid,   $_status, $datatable->_search , $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2 , $_requeststatus );
        

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {

            $image_product = '<img width="45" height="45" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/' . $record->productid . '/type/thumb"  >';

            $detailView = '<a href="/inventory/view/id/' . $record->inventoryitemid . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';
            
            $_customerplan = '<a target="_blank" href="/myairtimeplan/view/idcrypt/' . $record->planidcryptforcustomer . '" class="btn btn-xs btn-default" >'.$record->customerplanname.'</a>';
            
            $output['aaData'][] = array($image_product, $detailView,  $record->voicenumber, $record->datanumber, $record->productname,  $record->servicetype_code , $record->providerplanname , $_customerplan , $record->customername , $record->processeddatetime);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function getactivatedsimAction(){
        //route
        
        $dto = new Dto_Inventory();
        $table = $dto->getSimInformationActivated();
        $tableprint = "<table border='1'>";
        $tableprint .= "<tr>";
            $tableprint .= "<td>SIM CARD</td>";
            $tableprint .= "<td>NUMERO DE VOZ</td>";
            $tableprint .= "<td>NUMERO DE DATOS</td>";
            $tableprint .= "<td>PRODUCTO</td>";
            $tableprint .= "<td>TIPO PLAN</td>";
            $tableprint .= "<td>PROVEEDOR PLAN</td>";
            $tableprint .= "<td>CLIENTE PLAN</td>";
            $tableprint .= "<td>PRECIO PLAN</td>";
            $tableprint .= "<td>CARGO FIJO</td>";
            $tableprint .= "<td>CLIENTE</td>";
            $tableprint .= "<td>FECHA PROCESAMIENTO</td>";
        $tableprint .= "</tr>";
        foreach ($table as $item) {
            $tableprint .= "<tr>";
            $tableprint .= "<td>".$item['iserial']."</td>";
            $tableprint .= "<td>".$item['voicenumber']."</td>";
            $tableprint .= "<td>".$item['datanumber']."</td>";
            $tableprint .= "<td>".$item['productname']."</td>";
            $tableprint .= "<td>".$item['servicetype_code']."</td>";
            $tableprint .= "<td>".$item['providerplanname']."</td>";
            $tableprint .= "<td>".$item['customerplanname']."</td>";
            $tableprint .= "<td>".$item['price']."</td>";
            $tableprint .= "<td>".$item['fixedpayment']."</td>";
            $tableprint .= "<td>".$item['customername']."</td>";
            $tableprint .= "<td>".$item['processeddatetime']."</td>";
            $tableprint .= "</tr>";
        }
        $tableprint .= "</table>";
        echo $tableprint;
    }
     
    
    
    
    function requestgetsimcardsactivatedAction() {

    
        
        $fileLang = "../lib/view/inventory/lang/" . trim($this->session_langcode) . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        }

        $_accountid = intval($this->aRequest["accountid"]);
        $_issimcard = intval($this->aRequest["issimcard"]);
        $_status = trim($this->aRequest["status"]);
        $_inventorystatus = intval($this->aRequest["inventorystatus"]);
        
        /*
        $_requeststatus  = trim($this->aRequest["requeststatus"]);
        
        if($_requeststatus == ""){
            $_requeststatus  = 'executed';
        }
        */

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('ao.id', 'ao.iserial', 'ao.voicenumber', 'ao.datanumber', 'ao.productname', 'ao.servicetype_code', 'ao.planname', 'ao.plannameforcustomer', ' acc.name' , 'ao.processeddatetime');
        $_datatableSetup->_sortCol = " ao.processeddatetime ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Inventory();

        $totalRecords = 0;
        $aRecords = array();


        $totalRecords = $dto_entity->getSimcardsActivatedTotal( $this->session_accountid, $_status,  $datatable->_search  );
        $aRecords = $dto_entity->getSimcardsActivated($this->session_accountid,   $_status, $datatable->_search , $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2 );


        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {

            $image_product = '<img width="45" height="45" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/' . $record->productid . '/type/thumb"  >';

            $detailView = '<a href="/inventory/view/id/' . $record->inventoryitemid . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';
            
            $_customerplan = '<a target="_blank" href="/myairtimeplan/view/idcrypt/' . $record->planidcryptforcustomer . '" class="btn btn-xs btn-default" >'.$record->customerplanname.'</a>';
            
            $output['aaData'][] = array($image_product, $detailView,  $record->voicenumber, $record->datanumber, $record->productname,  $record->servicetype_code , $record->providerplanname , $_customerplan , $record->customername , $record->processeddatetime);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }
 
    
    
    
    function requestdevicesoncustomerAction() {
        
        $fileLang = "../lib/view/inventory/lang/" . trim($this->session_langcode) . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        }

        $_accountid = intval($this->aRequest["accountid"]);
        $_issimcard = intval($this->aRequest["issimcard"]);
        $_status = trim($this->aRequest["status"]);
        $_inventorystatus = intval($this->aRequest["inventorystatus"]);


        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('invdet.deviceitemid', 'item.iserial', 'item.sku', 'item.productname', 'acc.name', 'item.documentcode', 'inv.invoiceno', 'inv.invoicedate', ' inv.invoiceduedate' , 'invdet.operationdatetime', 'invdet.operationdatetime');
        $_datatableSetup->_sortCol = " invdet.operationdatetime ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Inventory();

        $totalRecords = 0;
        $aRecords = array();

        $totalRecords = $dto_entity->getInventoryDevicesOnCustomerTotal( $this->session_accountid, $_status,  $datatable->_search );
        $aRecords = $dto_entity->getInventoryDevicesOnCustomer($this->session_accountid,   $_status, $datatable->_search , $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2 );

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {

            $image_product = '<img width="45" height="45" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/' . $record->productid . '/type/thumb"  >';

            $detailView = '<a href="/inventory/view/id/' . $record->inventoryitemid . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';
            
             $_inventorycheck = '<a href="javascript: modal_setdispatch(\'' . $record->inventoryitemid . '\' );"   class="btn btn-xs btn-blue"><i class="fa fa-dropbox"></i> &nbsp; ' . $_lang_inventory_registermovement . '</a>';

             
            $output['aaData'][] = array($image_product, $detailView,  $record->sku, $record->productname, $record->customername,  $record->providerinvoice , $record->customerinvoice , $record->invoicedate , $record->invoiceduedate , $record->operationdatetime , $_inventorycheck);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }
 
           
    
    function requestsetoninventoryAction() {

        $_inventoryitemid = intval($_REQUEST["inventoryitemid"]);
        $_statusdate = trim($_REQUEST["statusdate"]);
        $_inventoryaction = intval($_REQUEST["inventoryaction"]);

        $dto_inventory = new Dto_Inventory();
        $_result = false;

        $_statusdescription = $_REQUEST["statusdescription"];

        //if (intval($_inventoryaction) == 1) {
        $_result = $dto_inventory->updateInventoryStatus(
                $_inventoryitemid, $_inventoryaction, $_statusdate, $_statusdescription, 0, 0, 0, $this->session_userid);
        //}

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $_result);
        $view->load("template", "json");
    }
    
    
    
    
    

}
