<?php
require_once ("../lib/bean/Simcard.php");
require_once ("../lib/bean/Product.php");
require_once ("../lib/bean/AirtimePlan.php");
require_once ("../lib/bean/Network.php");
require_once ("../lib/bean/PeriodInfo.php");
require_once ("../lib/bean/ServiceType.php");
require_once ("../lib/bean/TrafficIncrement.php");
require_once ("../lib/bean/TrafficSetup.php");
require_once ("../lib/bean/Traffictype.php");
require_once ("../lib/bean/TrafficUnit.php");
require_once ("../lib/bean/ChargeType.php");
require_once ("../lib/bean/TrafficAllowance.php");
require_once ("../lib/bean/TrafficAllowanceDetail.php");
require_once ("../lib/bean/TrafficService.php");
require_once ("../lib/bean/AirtimePlanSetupChange.php");


require_once ("../lib/db/Dto_Account.php");
require_once ("../lib/db/Dto_Product.php");
require_once ("../lib/db/Dto_AirtimePlan.php");
require_once ("../lib/db/Dto_Network.php");
require_once ("../lib/db/Dto_PeriodInfo.php");
require_once ("../lib/db/Dto_ServiceType.php");
require_once ("../lib/db/Dto_TrafficIncrement.php");
require_once ("../lib/db/Dto_TrafficSetup.php");
require_once ("../lib/db/Dto_Traffictype.php");
require_once ("../lib/db/Dto_TrafficUnit.php");
require_once ("../lib/db/Dto_ChargeType.php");
require_once ("../lib/db/Dto_TrafficAllowance.php");
require_once ("../lib/db/Dto_TrafficService.php");
require_once ("../lib/db/Dto_AirtimePlanSetupChange.php");

require_once ("../lib/util/ViewUtil.php");

require_once ("../lib/helper/DatatableHelper.php");

class AirtimeplanController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {
        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/airtimeplan/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("airtimeplan", "index");
    }

    function requestgetindexAction() {
        
        $fileLangTemplate = "../lib/view/template/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate;
        
        $fileLangTemplate2 = "../lib/view/airtimeplan/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate2;        
        
        $_datatableSetup = new Datatable();

        $_datatableSetup->_aColumns = array('p.sku', 'p.network_code', 'p.servicetype_code', 
              'pd.name', 'a.name', 'p.contract_period_code' , 'p.payment_period_code', 'p.price' ,   
              'p.activationpayment', 
            
               ' (  select DISTINCT count(ao.iserial) from airtimeoperation ao  where ao.planid = p.idcrypt  ) ' ,
            
               'p.vers', 'p.aviable');
        
        $_datatableSetup->_sortCol = "p.sku";
        $_datatableSetup->_sortDir = "asc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_AirtimePlan();
        $totalRecords = $dto_entity->getListTotal($datatable->_search);
        $aRecords = $dto_entity->getlist($datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        
        
        $view = new ViewController();
         
        
        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );
      
        foreach ($aRecords as $record) {
            
            $_version = $record->version;
            if(  trim($_version) == "" ){
                $_version = '<a href="#" class="btn-xs btn-default btn">N/D</a>';
            }else{
                $_version = '<a href="#" class="btn-xs btn-default btn">'.$_version.'</a>';
            }
            
            $_avaliable = '<a href="#" class="btn-xs btn-red btn">'.$_lang_register_label_noaviable.'</a>';
            if(intval($record->aviable) == 1){
                $_avaliable = '<a href="#" class="btn-xs btn-green btn">'.$_lang_register_label_aviable.'</a>';
            }
            
            
            
            $_customername = $record->customername;
            if(strlen(trim($_customername)) == 0  || $_customername == NULL ){ $_customername = '---'; }
            
            $detailView = '<a href="/airtimeplan/register/idcrypt/' . $record->idcrypt . '" class="btn btn-xs btn-default" >' . $record->sku . '</a>';
            $output['aaData'][] = array($detailView, $record->networkcode, $record->servicetypecode, $record->name, $_customername ,  
                $_lang_period_option[$record->contractperiodcode], $_lang_period_option[$record->paymentperiodcode], 
                'USD '.moneyFormat($record->subscriptionprice) , 'USD '.moneyFormat($record->activationpayment) , 
                $record->totalsimcards ,
                $_version, 
                $_avaliable 
                    );
        }

        
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }    
    
    function viewAction() {

        $_idcrypt = trim($this->aRequest["idcrypt"]);

        $dto_entity = new Dto_AirtimePlan();
        $entity = $dto_entity->getByIdCrypt($_idcrypt);

        
        $view = new ViewController();
        $totalSimcards = $dto_entity->getSimcardsByPlanTotal($_idcrypt, "");
        $view->totalsimcards = $totalSimcards;        
        
        
        
        $view->entity = $entity;
        $view->load("airtimeplan", "view");
    }

    function requestgetsimcardsAction() {
       
        $_idcrypt = trim($this->aRequest["planid"]);
        $dto_entity = new Dto_AirtimePlan();
        $_planid = $dto_entity->getIdByIdCrypt($_idcrypt);

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('i.iserial', 'i.voicenumber', 'i.datanumber', 'i.sku', 'i.productname', 'i.operationregistered', 'acc.name', 'ao.datetoexecute');
        $_datatableSetup->_sortCol = "i.iserial";
        $_datatableSetup->_sortDir = "asc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $totalRecords = $dto_entity->getSimcardsByPlanTotal($_idcrypt, $datatable->_search);
        $aRecords = $dto_entity->getSimcardsByPlan( $this->session_accountid,    $_idcrypt, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {

            $_btnoperation = "";
            if ($record->operationregistered == "activate") {
                $_btnoperation = '<a href="#" class="btn btn-xs btn-green">' . $record->operationregistered . '</a>';
            } else if ($record->operationregistered == "suspend") {
                $_btnoperation = '<a href="#" class="btn btn-xs btn-orange">' . $record->operationregistered . '</a>';
            } else if ($record->operationregistered == "deactivate") {
                $_btnoperation = '<a href="#" class="btn btn-xs btn-dark-grey">' . $record->operationregistered . '</a>';
            }

            $detailView = '<a target="_blank" href="/inventory/view/id/' . $record->inventoryitemid . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';
            $output['aaData'][] = array($detailView, $record->voicenumber, $record->datanumber, $record->sku, $record->productname, $_btnoperation, $record->customername, $record->datetoexecute);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }
        
    function requestgetproductassocAction() {
        $_planidcrypt = trim($this->aRequest["planid"]);
        $dto_entity = new Dto_AirtimePlan();
        $entity = $dto_entity->getByIdCrypt($_planidcrypt);

        $view = new ViewController();
        $aRecords = $dto_entity->getProductAssoc($entity->id);


        //pd.name , p.idcrypt, p.sku
        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('p.sku', 'pd.name' ,  'pd.name');
        $_datatableSetup->_sortCol = "pd.name";
        $_datatableSetup->_sortDir = "asc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);
        
        $totalRecords = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );
 
        foreach ($aRecords as $record) {
            $deleteRecord = '<a data-toggle="modal" onclick=" fnc_setproductassoc_productid(\''.$record->idcrypt.'\') ;  "   href="#product-delete"   class="btn btn-xs btn-bricky" ><i class="fa fa-trash-o" /></a>';
            $output['aaData'][] = array( $record->sku , $record->name , $deleteRecord);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
 
    }

    function addproductAction() {
        $_productidcrypt = trim($_REQUEST["productid"]);
        $_planidcrypt = trim($_REQUEST["planid"]);
        $_operation = trim($_REQUEST["operation"]);

        $dto_product = new Dto_Product();
        $dto_plan = new Dto_AirtimePlan();

        $productid = $dto_product->getIdByIdCrypt($_productidcrypt);
        $planid = $dto_plan->getIdByIdCrypt($_planidcrypt);

        $result = $dto_plan->assocProduct($planid, $productid, $_operation, $this->session_userid);

        $view = new ViewController();
        $view->data = array("result"=>$result);
        $view->load("template", "json");
    }

    function deleteassocproductAction() {
        $_productidcrypt = trim($this->aRequest["productid"]);
        $_planidcrypt = trim($this->aRequest["planid"]);

        $dto_product = new Dto_Product();
        $dto_plan = new Dto_AirtimePlan();

        $productid = $dto_product->getIdByIdCrypt($_productidcrypt);
        $planid = $dto_plan->getIdByIdCrypt($_planidcrypt);

        $result = $dto_plan->deleteassocProduct($planid, $productid,  $this->session_userid);
        
        $view = new ViewController();
        $view->data = array("result"=>$result);
        $view->load("template", "json");
    }

    function registerAction() {
        $_idcrypt = trim($this->aRequest["idcrypt"]);
        $dto_entity = new Dto_AirtimePlan();
        $entity = $dto_entity->getByIdCrypt($_idcrypt);
        $entity_esp = $dto_entity->getByIdCrypt($_idcrypt, 'ESP');


        $dto_serviceType = new Dto_ServiceType();
        $dto_network = new Dto_Network();
        $dto_period = new Dto_PeriodInfo();
        $dto_traffictype = new Dto_Traffictype();
        $dto_trafficUnit = new Dto_TrafficUnit();
        $dto_charge = new Dto_ChargeType();
         
        $view = new ViewController();
        
        $dto_account = new Dto_Account();
        $view->aCustomer = $dto_account->getListByName($this->session_accountid, "", "a.name", "asc", 0, 100);
        
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");

        $view->aNetwork = $dto_network->getAll();
        $view->aPeriod = $dto_period->getAll();
        $view->aServiceType = $dto_serviceType->getAll();
        $view->aTrafficUnit = $dto_trafficUnit->getAllUnits();
        $view->aCharge = $dto_charge->getAll();
 
        $view->aTrafficTypeGroup = $dto_traffictype->getTtgroupList();
        
        if ($entity->networkcode != "") {
            $view->aTrafficType = $dto_traffictype->getByNetworkcode($entity->networkcode, $entity->ttgroup  );
        }        
        
        
        $view->entity = $entity;
        
        //-----------------
        //echo var_dump($view->entity); die();        
        //-----------------
        
        $view->entity_esp = $entity_esp;

        if ($entity->idcrypt != "") {

            $totalSimcards = $dto_entity->getSimcardsByPlanTotal($_idcrypt, "");
            $view->totalsimcards = $totalSimcards;                   
             
            //Total traffic Increment
            $totalTrafficIncrement = 0;
            $dto_trafficIncrement = new Dto_TrafficIncrement();
            $view->aTrafficIncrement = $dto_trafficIncrement->getByPlanId($entity->id);
            if ($view->aTrafficIncrement) {
                $totalTrafficIncrement = count($view->aTrafficIncrement);
            }
            $view->trafficincrement_total = $totalTrafficIncrement;


            //Traffic Element
            $dto_trafficElement = new Dto_TrafficSetup();
            $totalTrafficElement = 0;
            $view->aTrafficElement = $dto_trafficElement->getByPlanId($entity->id);
            if ($view->aTrafficElement) {
                $totalTrafficElement = count($view->aTrafficElement);
            }
            $view->trafficelement_total = $totalTrafficElement;


            //Total traffic allowance
            $dto_trafficAllowance = new Dto_TrafficAllowance();
            $totalTrafficAllowance = 0;
            $view->aTrafficAllowance = $dto_trafficAllowance->getByPlanId($entity->id);
            if ($view->aTrafficAllowance) {
                $totalTrafficAllowance = count($view->aTrafficAllowance);
            }
            $view->trafficallowance_total = $totalTrafficAllowance;



            //products from catalog
            $dto_product = new Dto_Product();
            $view->aCatalog = $dto_product->getListByCatalogDiffPlan($entity->id, 'ENG', '', 'pd.name', 'asc');

            array_push($view->aJavascript, "assets/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js");
            array_push($view->aCss, "assets/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css");
            array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");

            array_push($view->aJavascript, "assets/js/modules/airtimeplan/update.js");
            array_push($view->aJavascript, "assets/js/modules/airtimeplan/trafficsetup.js");
            array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
            
            
            array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
            array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


            array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
            array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
            array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
            array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
            array_push($view->aJavascript, "assets/js/modules/airtimeplan/productsetup.js");            
            
            $view->bodyjquery = "TableData.init(); Register.init(); RegisterEng.init(); RegisterEsp.init(); TrafficSetup.trafficIncrement(); TrafficSetup.trafficElement(); TrafficSetup.trafficAllowance(); TableDataProduct.init(); ";
            
            
            $view->load("airtimeplan", "update");
        } else {
            array_push($view->aJavascript, "assets/js/modules/airtimeplan/register.js");
            $view->bodyjquery = "Register.init();   ";
            $view->load("airtimeplan", "register");
        }
    }

    function requestregisterAction() {
        $e = $this->getRequestedEntity();
        $_operation = 'create';
        if ($e->idcrypt != "") {
            $_operation = 'update';
        }

        $dto_entity = new Dto_AirtimePlan();
        $idBySku = $dto_entity->getIdBySku($e->sku);
        $tempE = $dto_entity->getByIdCrypt($e->idcrypt);
 
        $ok = 1;
        
        if($_operation == "create" && $idBySku > 0){            
            $ok = -1;
        } 
        if($_operation == "update" &&   $idBySku != $tempE->id && $idBySku > 0  ){   
            $ok = -1;
        }        
        
        if(! $dto_entity->getBySkuAndVersionIsUnique(trim($e->idcrypt), intval($this->session_accountid)  , $e->sku, $e->version) ){
            $ok = -4; //sku and version is repeated
        } 
        
        $result = false;
        if ($ok == 1) {
               
            
            if ($_operation == "update" && $e->sku  != ""  && $e->networkcode != ""    && $e->servicetypecode != "") {
                
                $result = $dto_entity->update($e);
                
                
                
            } else if($_operation == "create" &&  $e->sku != "" &&  $e->name != "" && $e->networkcode != ""  && $e->servicetypecode != "") {
                 
                $e->idcrypt = $dto_entity->register($e);
                 
                if ($e->idcrypt == "") {
                    $result = false;
                } else {
                    $result = true;
                }
                
                
            } else {
                $ok = -3;
            } 
        }
        if (!$result) {
            
            if($ok != -4){
                $ok = -2;
            }
            
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $ok, "idcrypt" => $e->idcrypt);
        $view->load("template", "json");
    }

    function requestregisterdescAction() {
        $e = $this->getRequestedEntity();

        $result = false;

        if ($e->name != "" && $e->idcrypt != "") {
            $dto_entity = new Dto_AirtimePlan();
            $tempE = $dto_entity->getByIdCrypt($e->idcrypt);
            $e->id = $tempE->id;
            $result = $dto_entity->updateDesc($e);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $result);
        $view->load("template", "json");
    }

    function getRequestedEntity() {
        
        $_idcrypt = trim($_REQUEST["idcrypt"]);
        $_sku = trim($_REQUEST["sku"]);
        
        $_ttgroup = trim($_REQUEST["ttgroup"]);
        
        $_version = trim($_REQUEST["version"]);
        $_aviable = intval($_REQUEST["aviable"]);
        
        $_customeraccountid = intval($_REQUEST["customeraccountid"]);
        $_networkcode = trim($_REQUEST["networkcode"]);
        $_paymentperiodcode = trim($_REQUEST["paymentperiodcode"]);
        $_contractperiodcode = trim($_REQUEST["contractperiodcode"]);
        $_servicetypecode = trim($_REQUEST["servicetypecode"]);
        $_langcode = trim($_REQUEST["langcode"]);
        $_subscriptionprice = doubleval($_REQUEST["subscriptionprice"]);
        $_fixedpayment = doubleval($_REQUEST["fixedpayment"]);
        $_activationpayment = doubleval($_REQUEST["activationpayment"]);
        $_suspensionpayment = doubleval($_REQUEST["suspensionpayment"]);
        $_deactivationpayment = doubleval($_REQUEST["deactivationpayment"]);
        $_extensionpayment = doubleval($_REQUEST["extensionpayment"]);

        $_name = trim($_REQUEST["name"]);
        $_intro = trim($_REQUEST["intro"]);
        $_description = trim($_REQUEST["description"]);

        $e = new AirtimePlan();
        if ($_idcrypt != "") {
            $e->modifiedby = $this->session_userid;
        } else {
            $e->createdby = $this->session_userid;
        }
        $e->accountid = $this->session_accountid;
        $e->idcrypt = $_idcrypt;
        $e->ttgroup = $_ttgroup;
        $e->sku = $_sku;
        $e->customeraccountid = $_customeraccountid;
        $e->networkcode = $_networkcode;
        $e->paymentperiodcode = $_paymentperiodcode;
        $e->contractperiodcode = $_contractperiodcode;
        $e->servicetypecode = $_servicetypecode;
        $e->langcode = $_langcode;
        $e->subscriptionprice = $_subscriptionprice;
        $e->fixedpayment = $_fixedpayment;
        $e->activationpayment = $_activationpayment;
        $e->suspensionpayment = $_suspensionpayment;
        $e->deactivationpayment = $_deactivationpayment;
        $e->extensionpayment = $_extensionpayment;
        
        $e->name = $_name;
        $e->description = $_description;
        $e->intro = $_intro;

        $e->version = $_version;
        $e->aviable = $_aviable;
        
        return $e;
    }

    function requestregistertrafficincrementAction() {
        $total = intval($_REQUEST["ttincrement_total"]);
        $result = false;
        $planidcrypt = trim($_REQUEST["planidcrypt"]);
        
          
        $dto_entity = new Dto_AirtimePlan();
        $plan = $dto_entity->getByIdCrypt($planidcrypt);

        //we need to create the array
        $aRecord = array();
        if ($total > 0) {
            for ($i = 0; $i < $total; $i++) {
                $deleted = intval($_REQUEST["ti_deleted_" . $i]);
                if ($deleted == 0) {
                    
                    
                    
                    $traffictypecode = trim($_REQUEST["ti_traffictypecode_" . $i]);

                    $min_unitcode = trim($_REQUEST["ti_min_unitcode_" . $i]);
                    $min_unitqty = doubleval($_REQUEST["ti_min_unitqty_" . $i]);

                    $inc_unitcode = trim($_REQUEST["ti_inc_unitcode_" . $i]);
                    $inc_unitqty = doubleval($_REQUEST["ti_inc_unitqty_" . $i]);

                    $e = new TrafficIncrement();
                    $e->planidcrypt = trim($_REQUEST["planidcrypt"]);
                    $e->traffictypecode = $traffictypecode;
                    $e->min_unitcode = $min_unitcode;
                    $e->min_unitqty = $min_unitqty;
                    $e->inc_unitcode = $inc_unitcode;
                    $e->inc_unitqty = $inc_unitqty;
                    $e->planid = $plan->id;
                    $e->createdby = $this->session_userid;

                    array_push($aRecord, $e);
                }//if
            }//for
        }//if

        

        $total = count($aRecord);
        $result = false;
        if ($total > 0) {
            $dto_trafficincrement = new Dto_TrafficIncrement();
            $dto_trafficincrement->deleteBatch($planidcrypt);
            $result = $dto_trafficincrement->registerBatch($aRecord);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $result);
        $view->load("template", "json");
    }

    function requestregistertrafficallowanceAction() {
        $dto_plan = new Dto_AirtimePlan();
        $planidcrypt = trim($_REQUEST["planidcrypt"]);
        $plan = $dto_plan->getByIdCrypt($planidcrypt);

        $dto_ta = new Dto_TrafficAllowance();
        $result = false;
        $total = intval($_REQUEST["tsallowance_total"]);
        $aRecord = array();
        for ($i = 0; $i < $total; $i++) {
            $entity = new TrafficAllowance();
            $qty = doubleval($_REQUEST["qty_" . $i]);
            $name = trim($_REQUEST["ts_allowancename_" . $i]);
            $deleted = intval($_REQUEST["ts_deleted_" . $i]);
            $_trafficsetup = $_REQUEST["trafficsetupid_" . $i];
            $aInt = array();
            $totalSetup = count($_trafficsetup);
            $ok = true;
            if ($totalSetup > 0) {
                foreach ($_trafficsetup as $ts) {
                    array_push($aInt, $ts);
                }
            } else {
                $ok = false;
            }
             if ($qty < 0) {
                $ok = false;
             }
            if ($ok && $deleted == 0) {
                $entity->qty = $qty;
                $entity->tname = $name;
                $entity->aTrafficSetupId = $aInt;
                $entity->planid = intval($plan->id);
                $entity->createdby = $this->session_userid;
                array_push($aRecord, $entity);
            }
        }

        $dto_ta->deleteBatch($plan->id);
        $result = $dto_ta->registerBatch($aRecord);
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $result);
        $view->load("template", "json");
    }

    function requestregistertrafficelementAction() {
        $total = intval($_REQUEST["ttelement_total"]);
        $result = false;
        $planidcrypt = trim($_REQUEST["planidcrypt"]);

        $dto_entity = new Dto_AirtimePlan();
        $plan = $dto_entity->getByIdCrypt($planidcrypt);
        //we need to create the array
        $aRecord = array();
        if ($total > 0) {
            for ($i = 0; $i < $total; $i++) {
                $deleted = intval($_REQUEST["te_deleted_" . $i]);
                if ($deleted == 0) {
                    $ttcode_source = trim($_REQUEST["te_ttcode_source_" . $i]);
                    $ttcode_dest = trim($_REQUEST["te_ttcode_dest_" . $i]);
                    $tunitcode = trim($_REQUEST["te_tunitcode_" . $i]);
                    $chargecode = trim($_REQUEST["te_chargecode_" . $i]);

                    $chargeammount = doubleval($_REQUEST["te_chargeammount_" . $i]);

                    $e = new TrafficSetup();
                    $e->planidcrypt = trim($_REQUEST["planidcrypt"]);
                    $e->ttcode_source = $ttcode_source;
                    $e->ttcode_dest = $ttcode_dest;
                    $e->tunitcode = $tunitcode;
                    $e->chargecode = $chargecode;
                    $e->chargeammount = $chargeammount;
                    $e->planid = $plan->id;
                    $e->createdby = $this->session_userid;

                    array_push($aRecord, $e);
                }//if
            }//for
        }//if


        $total = count($aRecord);
        $result = false;
        //echo "El total es: ".$total; die();
        if ($total > 0) {
            $dto_trafficsetup = new Dto_TrafficSetup();
            $dto_trafficsetup->deleteBatch($planidcrypt);
            $result = $dto_trafficsetup->registerBatch($aRecord);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $result);
        $view->load("template", "json");
    }
    
    
    function recalculatechildplansAction(){
        
        $_planidcrypt   = trim($this->aRequest["planidcrypt"]);
        $_notifydealers = trim($this->aRequest["notifydealers"]);
        
        $_tonotify = 0;
        if(trim($_notifydealers)=="true"){
            $_tonotify = 1;
        }
        
        
        $dto_entity = new Dto_AirtimePlan();
        $_result = $dto_entity->recalculateChildPlans($_planidcrypt,  $this->session_accountid , $this->session_userid , $_tonotify );
             
     
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $_result);
        $view->load("template", "json");        
        
        
    }//function

}
