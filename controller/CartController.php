<?php

require_once ("../lib/bean/Address.php");
require_once ("../lib/bean/Account.php");
require_once ("../lib/bean/Cart.php");
require_once ("../lib/db/Dto_Cart.php");
require_once ("../lib/db/Dto_Account.php");

class CartController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    public function mycartAction() {

        $dto_cart = new Dto_Cart();
        $aCart = $dto_cart->getCartBySessionId($this->session_accountid);

        $total = count($aCart);
        if ($total == 0) {
            redirect("cart", "emptycart");
        }

        $dto_account = new Dto_Account();
        $view = new ViewController ();
        $view->aCart = $aCart;
        $view->account = $dto_account->getById($this->session_accountid);
        $view->accountid = $this->session_accountid;
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/cart/index.js");

        //Register
        $view->bodyjquery = "Register.init();  ";
        $view->load("cart", "index");
    }

    public function emptycartAction() {
        $view = new ViewController();
        $view->load("cart", "emptycart");
    }

    public function requestadditemAction() {

        $_idcrypt = trim($_REQUEST["idcrypt"]);
        $_price = doubleval($_REQUEST["price"]);
        $_quantity = trim($_REQUEST["quantity"]);

        $entity = new Cart();
        $entity->productid = $_idcrypt;
        $entity->accountid = $this->session_accountid;
        $entity->buyprice = doubleval($_price);
        $entity->subtotal = intval($_quantity) * $_price;
        $entity->qty = $_quantity;
        $entity->operation = 'buy';

        $dto_cart = new Dto_Cart();
        $result = $dto_cart->additem($entity);

        $view = new ViewController();
        $view->data = array("result" => $result);
        $view->template = "blank";
        $view->load("template", "json");
    }

    public function requestupdateitemAction() {

        $_idcrypt = trim($_REQUEST["idcrypt"]);
        $_price = doubleval($_REQUEST["price"]);
        $_quantity = trim($_REQUEST["quantity"]);
        $_itemid = trim($_REQUEST["itemid"]);

        $entity = new Cart();
        $entity->itemid = $_itemid;
        $entity->productid = $_idcrypt;
        $entity->buyprice = doubleval($_price);
        $entity->subtotal = intval($_quantity) * $_price;
        $entity->qty = $_quantity;
        $entity->operation = 'buy';

        $dto_cart = new Dto_Cart();
        $result = $dto_cart->updateitem($entity);

        $view = new ViewController();
        $view->data = array("result" => $result);
        $view->template = "blank";
        $view->load("template", "json");
    }

    public function requestdeleteitemAction() {

         $_itemid = trim($this->aRequest["itemid"]);

        
        $dto_cart = new Dto_Cart();
         $result = $dto_cart->deletebyitemid($_itemid);

        $view = new ViewController();
        $view->data = array("result" => $result);
        $view->template = "blank";
        $view->load("template", "json");
    }

}
