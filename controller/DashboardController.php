<?php

require_once ("../lib/bean/InvoicePayment.php");
require_once ("../lib/db/Dto_InvoicePayment.php");
require_once ("../lib/db/Dto_Account.php");

require_once ("../lib/bean/Cart.php");
require_once ("../lib/bean/InventoryItem.php");
require_once ("../lib/bean/Plan.php");
require_once ("../lib/bean/PlanTrafficType.php");
require_once ("../lib/bean/AllowanceAlert.php");

require_once ("../lib/db/Dto_Cart.php");
require_once ("../lib/db/Dto_Inventory.php");
require_once ("../lib/db/Dto_Plan.php");
require_once ("../lib/db/Dto_AllowanceAlert.php");


require_once ("../lib/db/Dto_Dashboard.php");

class DashboardController extends BaseController {
    function __construct() {
        isAuth();
        $this->setSession();
    }

    
    
    function indexAction() {
        
        $dto_cart  = new Dto_Cart();
        $aCart = $dto_cart->getCartBySessionId(  $this->session_accountid   );
        
        $dto_inventory = new Dto_Inventory();
        $totaldevices = $dto_inventory->getMyInventoryTotal( "",$this->session_accountid, 0, "");
        $totalsimcards = $dto_inventory->getMyInventoryTotal( "", $this->session_accountid, 1, "");
        
        $dto_allowancealert = new Dto_AllowanceAlert();
        $totalallowancealert = $dto_allowancealert->getListByAccountidTotal($this->session_accountid,  $datatable->_search); 
        
        $view = new ViewController ();
        
        
        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-daterangepicker/daterangepicker.css"); 
        
  
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js"); 
        
        array_push($view->aJavascript, "assets/bower_components/bootstrap-daterangepicker/moment.js"); 
   	array_push($view->aJavascript, "assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"); 
        
        
        
        
        $view->totalcart = count($aCart);
        $view->totaldevices = $totaldevices;
        $view->totalallowancealert = $totalallowancealert;
        $view->totalsimcards = $totalsimcards;
        
        
        $_accountid = $this->session_accountid;
      
        $_daysago = intval($_REQUEST["daysago"]);
        if($_daysago==0){ $_daysago = 30;   }
        
        $dto_dashboard = new Dto_Dashboard();
        $view->aCustomersActivity = $dto_dashboard->getCustomerswithmoreactivity($this->session_accountid);
       
        $_dateend = date('Y-m-d');
        $_datestart = date('Y-m-d', strtotime('-'.$_daysago.' days'));
        
        $view->daysago = $_daysago;
       
        
        if($this->session_accountid == 10001){
        
            $view->aServicesMoreActivated = $dto_dashboard->getServicesMoreActivatedMatrix( $_datestart, $_dateend);
            $view->aActivationsPerDealer = $dto_dashboard->getActivationsPerDistributor($_accountid, $_datestart, $_dateend);
            $view->aDevicesBestSell = $dto_dashboard->getDevicesBestSellMatrix( $_datestart, $_dateend);
            $view->getDistributorPerSales = $dto_dashboard->getDistributorPerSales($this->session_accountid, $_datestart, $_dateend);
        
        }else{
            $view->aServicesMoreActivated = $dto_dashboard->getServicesMoreActivated($_accountid, $_datestart, $_dateend);
            $view->aActivationsPerDealer = $dto_dashboard->getActivationsPerDistributor($_accountid, $_datestart, $_dateend);
            $view->aDevicesBestSell = $dto_dashboard->getDevicesBestSell($_accountid, $_datestart, $_dateend);
            $view->getDistributorPerSales = $dto_dashboard->getDistributorPerSales($_accountid, $_datestart, $_dateend);
            
        }
        
        array_push($view->aJavascript, "assets/js/modules/dashboard/index.js");
        
        
        $view->bodyjquery = "Dashboard.init();  ";
        $view->load("dashboard", "index");
    }

    
}
