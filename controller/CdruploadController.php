<?php

require_once ("../lib/dbcdr/Dto_Cdrfile.php");

class CdruploadController extends BaseController {

    function __construct() {
        //isAuth();
        //$this->setSession();
    }
     
    public function testAction() {

        $view = new ViewController ();
        
        
        $view->template = "blank";
        $view->load("cdrupload", "test");
    }

    public function existsfileAction() {
        
        $_result = false;
        //se busca el archivo con su md5, si no existe devuelve 0
        $_filemd5  = trim($_REQUEST["filemd5"]);
        $_filename = trim($_REQUEST["filename"]);
        $_filetype = trim($_REQUEST["filetype"]);
        
        
       
        
        if ($_filemd5 != "" && $_filename != "" && $_filetype != "") {
            $dto_cdrfile = new Dto_Cdrfile();
            $_total = $dto_cdrfile->existsfile($_filemd5, $_filename, $_filetype);
            if($_total>0){
                $_result = true;
            }
        }
        
        $view = new ViewController();
        $view->data = $_result;
        $view->template = "blank";
        $view->load("template", "json");
        
    }

    public function uploadwsAction() {

        $_result = false;
        if (isset($_REQUEST["submit"])) {

            $config = new Config();

            $_filetype = $_REQUEST["filetype"];

		//echo "Hasta aca llegamos"; die();

            if ($_filetype != "") {

                $uploads_dir = $config->main_cdrfiles . '/temp';
                
                $_filename = basename($_FILES["fileToUpload"]["name"]);
                $_filenametemp = date('ymdhis') . '_' . rand(0, 9999) . '_' . $_filename;

                $file_path = "$uploads_dir/$_filenametemp";

                move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $file_path);

                $fopen_media = fopen($file_path, 'rb');
                $fileContent = fread($fopen_media, filesize($file_path));
                $fileContent = pg_escape_bytea($fileContent);

		//echo "se movio el archivo"; die();

                $md5_file = md5_file($file_path, false);

		//echo "El file md5 es ".$md5_file;

                if ($md5_file != "") {

                    $localIP = get_ip();
		//	echo "La ip es:".$localIP;
                    $dto_cdrfile = new Dto_Cdrfile();
                    $_result = $dto_cdrfile->uploadfile($md5_file, $_filename, $_filetype, $localIP, $fileContent);
                }
            }
        }


        $view = new ViewController();
        $view->data = $_result;
        $view->template = "blank";
        $view->load("template", "json");
    }

}
