<?php

require_once ("../lib/bean/Network.php");
require_once ("../lib/bean/Product.php");

require_once ("../lib/bean/AirtimePlan.php");
require_once ("../lib/db/Dto_Network.php");
require_once ("../lib/db/Dto_AirtimePlan.php");
require_once ("../lib/db/Dto_Product.php");


require_once ("../lib/db/Dto_ReportService.php");

//require_once '/../lib/controller/PhpExcel/PHPExcel.php';

class ReportsController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {
        $view = new ViewController ();

        //$view->bodyjquery = "TableData.init();  ";
        $view->load("reports", "index");
    }
    

    function productsconsolidatedAction() {


        $dto_users = new Dto_User();
        $aUsers = $dto_users->getByAccountid($this->session_accountid);


        $aPeriod = array();

        $year = intval(date("Y"));

        for ($i = $year; $i > ($year - 2); $i--) {
            for ($j = 12; $j >= 1; $j--) {
                array_push($aPeriod, array("value" => $i . "-" . $j, "year" => $i, "month" => $j));
            }
        }

        $dto_network = new Dto_Network();

        $dto_airtimeplan = new Dto_AirtimePlan();


        $view = new ViewController();



        array_push($view->aJavascript, "assets/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js");
        array_push($view->aCss, "assets/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css");
        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");

        array_push($view->aJavascript, "assets/js/modules/reports/servicesconsolidated.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");


        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");


        $view->bodyjquery = "Form.init();  ";

        $view->aNetwork = $dto_network->getAll();
        $view->aUsers = $aUsers;
        $view->aPeriod = $aPeriod;
        $dto_product = new Dto_Product();
        $view->aPlan = $dto_product->getListByCatalog("eng", "", "p.id", "asc");

        $view->load("reports", "productsconsolidated");
    }
    

    function servicesconsolidatedAction() {


        $dto_users = new Dto_User();
        $aUsers = $dto_users->getByAccountid($this->session_accountid);


        $aPeriod = array();

        $year = intval(date("Y"));

        for ($i = $year; $i > ($year - 2); $i--) {
            for ($j = 12; $j >= 1; $j--) {
                array_push($aPeriod, array("value" => $i . "-" . $j, "year" => $i, "month" => $j));
            }
        }

        $dto_network = new Dto_Network();

        $dto_airtimeplan = new Dto_AirtimePlan();


        $view = new ViewController();



        array_push($view->aJavascript, "assets/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js");
        array_push($view->aCss, "assets/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css");
        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");

        array_push($view->aJavascript, "assets/js/modules/reports/servicesconsolidated.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");


        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");


        $view->bodyjquery = "Form.init();  ";

        $view->aNetwork = $dto_network->getAll();
        $view->aUsers = $aUsers;
        $view->aPeriod = $aPeriod;
        $view->aPlan = $dto_airtimeplan->getList("", "p.id", "desc", 0, 100);

        $view->load("reports", "servicesconsolidated");
    }

    function servicesconsolidated_xlsAction() {

        $aOperation = array();
        $_total_operation = count($_REQUEST["operation"]);

        $aPeriod = array();
        $_total_period = count($_REQUEST["period"]);
        
        $aNetwork = array();
        $_total_network = count($_REQUEST["networkcode"]);

        $aUser = array();
        $_total_user = count($_REQUEST["user"]);
        
        
        if (intval($_total_period) == 0) {
            array_push($aPeriod, date("Y") . "-" . date("m"));
        } else {
            foreach ($_REQUEST["period"] as $period) {
                array_push($aPeriod, $period);
            }
        }


        if (intval($_total_operation) == 0) {
            $_total_operation = 3;
            $aOperation = array("activate", "suspend", "deactivate");
        } else {
            foreach ($_REQUEST["operation"] as $operation) {
                array_push($aOperation, $operation);
            }
        }

        if (intval($_total_network) == 0) {
           $dto_network = new Dto_Network();
           $listNetwork = $dto_network->getAll();
           
           foreach($listNetwork as $tempn){
               array_push($aNetwork, $tempn->code);
           }
           
        } else {
            foreach ($_REQUEST["networkcode"] as $network) {
                array_push($aNetwork, $network);
            }
        }

        
        if (intval($_total_user) == 0) {
           $dto_user = new Dto_User();
           $listUser = $dto_user->getByAccountid($this->session_accountid);
           
           foreach($listUser as $tempn){
               array_push($aUser, $tempn->firstname." ".$tempn->lastname);
           }
           
        } else {
            foreach ($_REQUEST["user"] as $user) {
                array_push($aUser, $user);
            }
        }
        
        
        
        
        $fileLangTemplate = "../lib/view/reports/lang/" . strtolower($this->session_langcode) . ".php";
        if (file_exists($fileLangTemplate)) {
            include $fileLangTemplate;
        }

        $dto_report = new Dto_ReportService();
        $aConsolidatedByPlan = $dto_report->getConsolidatedByPlan($aUsers, $aPeriod, $aNetwork, $aOperation, $aPlans);
        $aConsolidatedByNetwork = $dto_report->getConsolidatedByNetwork($aUsers, $aPeriod, $aNetwork, $aOperation, $aPlans);
        $aConsolidatedByUser = $dto_report->getConsolidatedByUser($aUsers, $aPeriod, $aNetwork, $aOperation, $aPlans);

        
        $aFoundPlansName = array();
        foreach ($aConsolidatedByPlan as $record) {
            array_push($aFoundPlansName, $record["planname"]);
        }
        $aFoundPlansName = array_unique($aFoundPlansName);

        $report = "";
        $report .= "<table border='1'>";
        $report .="<tr><th align='left'>" . $_lang_generation_reportname . "</th><td align='left'>" . $_lang_index_label_services_consolidated . "</td></tr>";
        $report .="<tr><th align='left'>" . $_lang_generation_user . "</th><td align='left'>" . $this->session_firstname . " " . $this->session_lastname . "</td></tr>";
        $report .="<tr><th align='left'>" . $_lang_generation_datetime . "</th><td align='left'>" . date("Y-m-d H:i:s") . "</td></tr>";


        //By Plans Report
        //======================================
        for ($i = 0; $i < 3; $i++) {
            $report .="<tr><td></td> </tr>";
        }
        //echo var_dump($aPeriod); die();
        $report .="<tr  ><td>by Plans</td>";
        foreach ($aPeriod as $period) {
            $aTxt = split("-", $period);
            $report .="<td cellborder=1 align='center' colspan='" . $_total_operation . "'>" . $aTxt[0] . "-" . $_lang_month[intval($aTxt[1]) - 1] . "</td>";
        }
        $report .="</tr>";
        $report .="<tr><td></td>";
        foreach ($aPeriod as $period) {
            foreach ($aOperation as $operation) {
                $textOperation = "";
                if ($operation == "activate") {
                    $textOperation = $_lang_operation_activate;
                }
                if ($operation == "deactivate") {
                    $textOperation = $_lang_operation_deactivate;
                }
                if ($operation == "suspend") {
                    $textOperation = $_lang_operation_suspend;
                }
                $report .="<td cellborder=1  align='center' >" . $textOperation . "</td>";
            }
        }
        $report .="</tr>";
        
        foreach ($aFoundPlansName as $planname) {
            $report .="<tr><td>" . $planname . "</td>";
            foreach ($aPeriod as $period) {
                foreach ($aOperation as $operation) {
                    
                    $found = false;
                    foreach ($aConsolidatedByPlan as $record) {
                        
                        //echo "Periodo:".$period." :::  ----> ".$record["year"]."-".$record["month"]; die();
                       
                        if (trim($record["planname"]) == trim($planname) && trim($record["operation"]) == $operation && trim($period) == trim($record["year"] . '-' . $record["month"]) ) {
                            $report .="<td cellborder=1  align='center' >" .  $record["qty"] . "</td>";
                            $found = true;
                        }
                    }
                    if(!$found){
                        $report .="<td cellborder=1  align='center' > - </td>";
                    }
                    
                    
                }
            }
            $report .="</tr>";
        }
        //=============================
        

      //By Network Report
        //======================================
        for ($i = 0; $i < 3; $i++) {
            $report .="<tr><td></td> </tr>";
        }
        //echo var_dump($aPeriod); die();
        $report .="<tr  ><td>by Network</td>";
        foreach ($aPeriod as $period) {
            $aTxt = split("-", $period);
            $report .="<td cellborder=1 align='center' colspan='" . $_total_operation . "'>" . $aTxt[0] . "-" . $_lang_month[intval($aTxt[1]) - 1] . "</td>";
        }
        $report .="</tr>";
        $report .="<tr><td></td>";
        foreach ($aPeriod as $period) {
            foreach ($aOperation as $operation) {
                $textOperation = "";
                if ($operation == "activate") {
                    $textOperation = $_lang_operation_activate;
                }
                if ($operation == "deactivate") {
                    $textOperation = $_lang_operation_deactivate;
                }
                if ($operation == "suspend") {
                    $textOperation = $_lang_operation_suspend;
                }
                $report .="<td cellborder=1  align='center' >" . $textOperation . "</td>";
            }
        }
        $report .="</tr>";
        
        foreach ($aNetwork as $networkcode) {
            $report .="<tr><td>" . $networkcode . "</td>";
            foreach ($aPeriod as $period) {
                foreach ($aOperation as $operation) {
                    
                    $found = false;
                    foreach ($aConsolidatedByNetwork as $record) {
                        
                        //echo "Periodo:".$period." :::  ----> ".$record["year"]."-".$record["month"]; die();
                       
                        if (trim($record["networkcode"]) == trim($networkcode) && trim($record["operation"]) == $operation && trim($period) == trim($record["year"] . '-' . $record["month"]) ) {
                            $report .="<td cellborder=1  align='center' >" .  $record["qty"] . "</td>";
                            $found = true;
                        }
                    }
                    if(!$found){
                        $report .="<td cellborder=1  align='center' > - </td>";
                    }
                    
                    
                }
            }
            $report .="</tr>";
        }
        //============================= 
        
        
        
   //By User Report
        //======================================
        for ($i = 0; $i < 3; $i++) {
            $report .="<tr><td></td> </tr>";
        }
        //echo var_dump($aPeriod); die();
        $report .="<tr  ><td>by Executive</td>";
        foreach ($aPeriod as $period) {
            $aTxt = split("-", $period);
            $report .="<td cellborder=1 align='center' colspan='" . $_total_operation . "'>" . $aTxt[0] . "-" . $_lang_month[intval($aTxt[1]) - 1] . "</td>";
        }
        $report .="</tr>";
        $report .="<tr><td></td>";
        foreach ($aPeriod as $period) {
            foreach ($aOperation as $operation) {
                $textOperation = "";
                if ($operation == "activate") {
                    $textOperation = $_lang_operation_activate;
                }
                if ($operation == "deactivate") {
                    $textOperation = $_lang_operation_deactivate;
                }
                if ($operation == "suspend") {
                    $textOperation = $_lang_operation_suspend;
                }
                $report .="<td cellborder=1  align='center' >" . $textOperation . "</td>";
            }
        }
        $report .="</tr>";
        
        foreach ($aUser as $user) {
            $report .="<tr><td>" . $user . "</td>";
            foreach ($aPeriod as $period) {
                foreach ($aOperation as $operation) {
                    
                    $found = false;
                    foreach ($aConsolidatedByUser as $record) {
                        
                        //echo "Periodo:".$user." :::  ----> ".$record["username"]; die();
                       
                       // if (trim($record["username"]) == trim($user) && trim($record["operation"]) == $operation && trim($period) == trim($record["year"] . '-' . $record["month"]) ) {
                         
                         if (trim($record["username"]) == trim($user) && trim($record["operation"]) == $operation && trim($period) == trim($record["YEAR"] . '-' . $record["MONTH"])  ) {
                         
                        $report .="<td cellborder=1  align='center' >" .  $record["qty"] . "</td>";
                            $found = true;
                        }
                    }
                    if(!$found){
                        $report .="<td cellborder=1  align='center' > - </td>";
                    }
                    
                    
                }
            }
            $report .="</tr>";
        }
        //============================= 
        
        
        

        $report .= "</table>";


        $file = "servicesconsolidated_" . date('Y-m-d H:i:s') . ".xls";
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=$file");
        echo $report;
    }

}
