<?php

require_once ("../lib/bean/Address.php");
require_once ("../lib/bean/AirtimeOperation.php");
require_once ("../lib/bean/SaleOrder.php");
require_once ("../lib/bean/SaleOrderDetail.php");
require_once ("../lib/bean/Invoice.php");
require_once ("../lib/bean/ItemDispatch.php");
require_once ("../lib/bean/InvoiceDetail.php");
require_once ("../lib/bean/InventoryItem.php");
require_once ("../lib/bean/ShippingProvider.php");

require_once ("../lib/bean/HeadDocumentExport.php");
require_once ("../lib/bean/DetailProductExport.php");

require_once ("../lib/db/Dto_AirtimeOperation.php");
require_once ("../lib/db/Dto_Saleorder.php");
require_once ("../lib/db/Dto_Invoice.php");
require_once ("../lib/db/Dto_Inventory.php");
require_once ("../lib/db/Dto_ShippingProvider.php");

require_once ("../lib/helper/DatatableHelper.php");

require_once ("../lib/controller/MediaController.php");
require_once ("../lib/controller/DocumentgenerateController.php");

class InvoiceController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/invoice/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("invoice", "index");
    }

    function viewAction() {

        $_code = trim($this->aRequest["code"]);

        $_idcrypt = trim($this->aRequest["idcrypt"]);

        $dto_invoice = new Dto_Invoice();

        if ($_code != "") {
            $invoice = $dto_invoice->getByCode($_code);
        }

        if ($_idcrypt != "") {
            $invoice = $dto_invoice->getByIdcrypt($_idcrypt);
        }

        if ($invoice->code == "") {
            die();
        }


        $view = new ViewController();

        if ($invoice->invoicetype == "GOODS") {

            $invoiceDetail = $dto_invoice->getDetailByCode($invoice->code);
            $allItemsRegistered = true;

            if (count($invoiceDetail) > 0) {
                foreach ($invoiceDetail as $record) {
                    if ($record->totaldispatched != $record->qty) {
                        $allItemsRegistered = false;
                    }
                }
            }

            $dto_shippingprovider = new Dto_ShippingProvider();
            $view->aShippingProvider = $dto_shippingprovider->getAll();
            $view->canDispatchInvoice = $allItemsRegistered;
        } else if ($invoice->invoicetype == "SERVICES") {
            $dto_ao = new Dto_AirtimeOperation();
            $invoiceDetail = $dto_ao->getRequestOperationsByStatus($invoice->ordercode, 1);
        }


        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aJavascript, "assets/js/modules/saleorder/index.js");
        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");



        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/invoice/view.js");

        if ($this->canIProcessInvoice($this->session_accountid, $_code) > 0) {
            $view->processInvoice = true;
        } else {
            $view->processInvoice = false;
        }
        $view->bodyjquery = "DispatchItems.init();  DeliveryDate.init();  PaymentDate.init();  ";
        $view->invoice = $invoice;
        $view->detail = $invoiceDetail;
        $view->load("invoice", "view");
    }

    function dispatchAction() {
        $_code = trim($this->aRequest["code"]);
        $dto_invoice = new Dto_Invoice();
        $invoice = $dto_invoice->getByCode($_code);
        $inventoryItems = $dto_invoice->getDetailDispatchedByCode($_code);

        $view = new ViewController();
        $view->invoice = $invoice;
        $view->inventoryItems = $inventoryItems;
        $view->load("invoice", "dispatch");
    }

    function confirmdispatchAction() {
        $_code = trim($this->aRequest["code"]);
        $dto_invoice = new Dto_Invoice();
        $dto_invoice->setDispatchDate($_code, $this->session_userid, date('Y-m-d'));
        redirect("invoice", "view", array("code" => $_code));
    }

    function setdeliverydateAction() {
        $_code = trim($this->aRequest["code"]);
        $_deliverydate = trim($_REQUEST["deliverydate"]);

        $_shippingprovidercode = trim($_REQUEST["shippingprovidercode"]);
        $_shippingtrackingid = trim($_REQUEST["shippingtrackingid"]);

        $dto_invoice = new Dto_Invoice();
        $dto_invoice->setDeliveryDate($_code, $this->session_userid, $_deliverydate);

        $dto_invoice->setShippingTrackingInfo($_code, $_shippingprovidercode, $_shippingtrackingid);

        $dto_inventory = new Dto_Inventory();
        $dto_inventory->transferInventoryFromInvoice($_code, $_deliverydate);

        redirect("invoice", "view", array("code" => $_code));
    }

    function requestgenerateinvoiceAction() {

        $dto_saleorder = new Dto_Saleorder();
        $dto_invoice = new Dto_Invoice();
        $invoice = $this->getRequestGenerateInvoice();
        $invoice->code = $dto_invoice->generateInvoiceCode();


        $aDetail = $dto_saleorder->getDetailByCode($invoice->ordercode);

        $invoice->totalitems = count($aDetail);

        $result = $dto_invoice->generateInvoice($invoice, $aDetail);

        $view = new ViewController();
        if ($result) {
            $view->data = array("code" => $invoice->code);
        } else {
            $view->data = array("result" => $result);
        }

        $view->template = "blank";
        $view->load("template", "json");
    }

    function requestgetcustomerinvoicesAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('i.code', 'a.name', 'i.invoicedate', 'i.dispatchdate', 'i.deliverydate', 'i.totalitems', 'i.payment_method', 'i.x_ammount');
        $_datatableSetup->_sortCol = "a.orderdate";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Invoice();

        $totalRecords = $dto_entity->getMyCustomerInvoicesTotal($this->session_accountid, $datatable->_search);
        $aRecords = $dto_entity->getMyCustomerInvoices($this->session_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        if ($totalFiltered > 0 && $aRecords) {
            foreach ($aRecords as $record) {

                $status = "created";
                $classBtn = "btn-bricky";

                if ($record->dispatchdate != "") {
                    $classBtn = "btn-yellow";
                }
                if ($record->deliverydate != "") {
                    $status = "delivered";
                    $classBtn = "btn-primary";
                }
 
                $icon_payment = '';
                if ($record->payment_method == "creditcard") {
                    $icon_payment = '<i class="fa fa-credit-card"></i>';
                } else if ($record->payment_method == "banktransfer") {
                    $icon_payment = '<i class="fa fa-bank"></i>';
                } else {
                    $icon_payment = '<i class="fa fa-money"></i>';
                }

                $detailView = '<a href="/invoice/view/code/' . $record->code . '" class="btn btn-xs ' . $classBtn . '" >' . $record->code . '</a>';

                $output['aaData'][] = array($detailView, $record->accountname, substr($record->invoicedate, 0, 10), $record->dispatchdate, $record->deliverydate, $record->totalitems, $icon_payment . "&nbsp;" . $record->payment_method, moneyFormat($record->x_ammount));
            }
        }
        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestinsertitemsbybatchAction() {
        $dto_invoice = new Dto_Invoice();
        $aRecord = $this->getRequestItemsByBatch();

        $ok = true;
        $totalRepeated = 0;


        $aSerialsRepeated = $dto_invoice->getSerialsRepeated($aRecord);
        if ($aSerialsRepeated) {
            $totalRepeated = count($aSerialsRepeated);
            $ok = false;
        }


        $_documentcode = $_REQUEST["documentcode"];
        $_itemid = $_REQUEST["itemid"];
        $total = count($aRecord);
        $_result = false;
        if ($total > 0 && $ok) {

            $_deleteok = $dto_invoice->deleteItemDispatchedByBatch($_documentcode, $_itemid);
            if ($_deleteok) {
                $_result = $dto_invoice->insertItemDispatchByBatch($aRecord);
            }
            if (!($_deleteok && $_result)) {
                $ok = false;
            }
        }

        $view = new ViewController();
        $view->template = "blank";
        if ($ok) {
            $view->data = array("result" => 1);
        } else {
            $view->data = array("result" => 0, "repeated" => $aSerialsRepeated);
        }
        $view->load("template", "json");
    }

    function requestitemsbybatchAction() {

        $_documentcode = trim($this->aRequest["documentcode"]);
        $_itemid = trim($this->aRequest["itemid"]);

        $dto_invoice = new Dto_Invoice();
        $aRecords = $dto_invoice->getItemsDispatched($_documentcode, $_itemid);


        $view = new ViewController();
        $view->template = "blank";
        $view->data = $aRecords;
        $view->load("template", "json");
    }

    function requestsetinvoicepaymentAction() {
        $_code = trim($_REQUEST["code"]);
        $_obs = trim($_REQUEST["paymentobs"]);
        $_paymentdate = trim($_REQUEST["paymentdate"]);

        $dto_invoice = new Dto_Invoice();
        $result = $dto_invoice->setPaymentDate($_code, $_paymentdate, $_obs, $this->session_userid);

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $result);
        $view->load("template", "json");
    }

    function getRequestItemsByBatch() {
        $aRecord = array();

        $_documentcode = $_REQUEST["documentcode"];
        foreach ($_REQUEST as $key => $value) {

            if (strpos($key, "serial") > -1) {

                $aItemRequest = split("_", $key);
                $_index = $aItemRequest[1];
                $_itemid = $aItemRequest[2];

                $_serialvalue = $value;
                $_code2value = $_REQUEST["code2_" . $_index . "_" . $_itemid];

                $itemDispatch = new ItemDispatch();
                $itemDispatch->documentcode = $_documentcode;
                $itemDispatch->index = $_index;
                $itemDispatch->itemid = $_itemid;
                $itemDispatch->serial = $_serialvalue;
                $itemDispatch->code2 = $_code2value;
                array_push($aRecord, $itemDispatch);
            }
        }

        return $aRecord;
    }

    function getRequestGenerateInvoice() {

        $_subtotal = trim($_REQUEST["invoice_subtotal"]);
        $_invoiceduedate = trim($_REQUEST["invoiceduedate"]);
        $_shipping_cost = trim($_REQUEST["shipping_cost"]);
        $_payment_method = trim($_REQUEST["payment_method"]);
        $_payment_fee = trim($_REQUEST["payment_fee"]);
        $_invoicecomments = trim($_REQUEST["invoicecomments"]);
        $_ordercode = trim($_REQUEST["ordercode"]);
        $_totalinvoice = doubleval($_REQUEST["totalinvoice"]);

        $invoice = $this->setInvoiceFromSaleOrder($_ordercode);
        $invoice->ordercode = $_ordercode;
        $invoice->subtotal = $_subtotal;
        $invoice->invoiceduedate = $_invoiceduedate;
        $invoice->payment_method = $_payment_method;
        $invoice->invoicecomments = $_invoicecomments;
        $invoice->shipping_cost = $_shipping_cost;
        $invoice->payment_fee = $_payment_fee;
        $invoice->taxes = 0;
        $invoice->x_ammount = $_totalinvoice;
        $invoice->username = $this->session_user;
        return $invoice;
    }

    public function setInvoiceFromSaleOrder($_ordercode) {

        $dto_saleorder = new Dto_Saleorder();
        $saleorder = $dto_saleorder->getByCode($_ordercode);

        $invoice = new Invoice();
        $invoice->accountid = $saleorder->accountid;
        $invoice->username = $this->session_user;
        $invoice->invoicedate = date('Y-m-d');
        $invoice->userid = $this->session_userid;
        $invoice->userip = get_ip();
        $invoice->sessionid = session_id();

        $invoice->billing_country = $saleorder->billing_country;
        $invoice->billing_state = $saleorder->billing_state;
        $invoice->billing_city = $saleorder->billing_city;
        $invoice->billing_pobox = $saleorder->billing_pobox;
        $invoice->billing_zipcode = $saleorder->billing_zipcode;
        $invoice->billing_address = $saleorder->billing_address;

        $invoice->shipping_country = $saleorder->shipping_country;
        $invoice->shipping_state = $saleorder->shipping_state;
        $invoice->shipping_city = $saleorder->shipping_city;
        $invoice->shipping_pobox = $saleorder->shipping_pobox;
        $invoice->shipping_zipcode = $saleorder->shipping_zipcode;
        $invoice->shipping_address = $saleorder->shipping_address;


        return $invoice;
    }

    function canIProcessInvoice($_parentaccountid, $_code) {
        $result = false;

        $dto_invoice = new Dto_Invoice();
        $saleorderid = $dto_invoice->getCodeByChildInvoice($_parentaccountid, $_code);

        if ($saleorderid > 0) {
            $result = true;
        }

        return $result;
    }

    function downloadinvoiceAction() {
        $view = new ViewController();


        $fileLangTemplate = "../lib/view/invoice/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate;

        $_code = trim($this->aRequest["code"]);

        $dto_invoice = new Dto_Invoice();
        $invoice = $dto_invoice->getByCode($_code);
        $invoiceDetail = $dto_invoice->getDetailByCode($_code);
        $aDetailProduct = array();

        foreach ($invoiceDetail as $detail) {
            $entity = new DetailProductExport();

            $entity->codeProduct = $detail->sku;
            $entity->nameProduct = $detail->productname;
            $entity->quantity = $detail->qty;
            $entity->price = $detail->productprice;
            $entity->totalPrice = doubleval($detail->productprice) * doubleval($detail->qty);

            array_push($aDetailProduct, $entity);
        }

        $headDocument = new HeadDocumentExport();
        $headDocument->accountName = $invoice->accountname;
        $headDocument->numberDocument = $invoice->ordercode;
        $headDocument->contactPhone = $invoice->phonenumber;
        $headDocument->contactFirstName = $invoice->userfirstName;
        $headDocument->contactLastName = $invoice->userLastName;

        $headDocument->salesManFisrtName = $invoice->userfirstName;
        $headDocument->salesManLastName = $invoice->userLastName;

        $headDocument->comments = $invoice->invoicecomments;
        $headDocument->dateDocument = $invoice->invoicedate;
        $headDocument->dateDueDocument = $invoice->invoiceduedate;

        $headDocument->billStreet = $invoice->billing_address;
        $headDocument->billCity = $invoice->billing_city;
        $headDocument->billState = $invoice->billing_state;
        $headDocument->billCode = $invoice->billing_zipcode;

        $headDocument->billCountry = $invoice->billing_country;
        $headDocument->webSiteAccount = $invoice->webSite;
        $headDocument->phoneContact = $invoice->phonenumber;

        $headDocument->shippingAddress = $invoice->shipping_address;
        $headDocument->shippingCountry = $invoice->shipping_country;
        $headDocument->shippingState = $invoice->shipping_state;
        $headDocument->shippingCity = $invoice->shipping_city;
        $headDocument->shippingZipCode = $invoice->shipping_zipcode;

        $headDocument->paymentFee = $invoice->payment_fee;
        $headDocument->parcialtotal = $invoice->subtotal;
        $headDocument->taxes = $invoice->taxes;
        $headDocument->shippingCost = $invoice->shipping_cost;
        $headDocument->total = $invoice->x_ammount;

        $headDocument->colorBanner = "#F71907";
        $headDocument->textBanner = ""; //Here we put the banner id
        $headDocument->fontBanner = 100;


        $headDocument->paymentMethod = $invoice->payment_method ;
        
        $headDocument->shippingtrackingid = $invoice->shippingtrackingid ;
        $headDocument->shippingprovider_name = $invoice->shippingprovider_name; 
        $headDocument->ordercode = $invoice->ordercode; 
        
        $accountid = $invoice->provideraccountid;

        $view->detailDocument = $aDetailProduct;
        $view->code = $_code;

        $view->lang_index_table = $_lang_index_table_title;
        $view->lang_invoice_number = $_lang_view_label_invoiceno;
        $view->lang_invoice_date = $_lang_view_label_invoicedate;
        $view->lang_invoice_due_date = $_lang_view_label_invoiceduedate;
        $view->typeDocument = $_lang_index_label_module;

        $document = new DocumentGenerateController();
        $document->generateDocument($view, $this, $accountid, $headDocument);
    }

    function downloadinvoiceservicesAction() {
        $view = new ViewController();


        $fileLangTemplate = "../lib/view/template/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate;

        $fileLangTemplate = "../lib/view/invoice/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate;

        $_code = trim($this->aRequest["code"]);

        $dto_invoice = new Dto_Invoice();
        $invoice = $dto_invoice->getByCode($_code);


        $dto_ao = new Dto_AirtimeOperation();
        $invoiceDetail = $dto_ao->getRequestOperationsByStatus($invoice->ordercode, 1);

        //$invoiceDetail = $dto_invoice->getDetailByCode($_code);
        $aDetailProduct = array();

        foreach ($invoiceDetail as $detail) {
            $entity = new DetailProductExport();

            $_currency = "USD ";
            $entity->nameProduct = $detail->productname;

            $_description = "";
            $_description .= '<strong>' . $_lang_plan . '</strong>: ' . $detail->planname . " \n ";

            $_lbl_contractperiod = $_lang_contractperiodcode;
            if ($detail->subscription_period == "p0") {
                $_lbl_contractperiod = $_lang_validityperiodcode;
            }


            $_description .= '<strong>' . $_lang_paymentperiodcode . '</strong>: ' . $_lang_period_option[$detail->subscription_period] . " \n ";

            $_description .= '<strong>' . $_lbl_contractperiod . '</strong>: ' . $_lang_period_option[$detail->contract_period] . " \n ";

            if ($detail->subscription_price > 0) {
                $_description .= '<strong>' . $_lang_subscriptionprice . '</strong>: ' . $_currency . ' ' . $detail->subscription_price . " \n ";
            }

            if ($detail->activation_price > 0) {
                $_description .= '<strong>' . $_lang_activationpayment . '</strong>: ' . $_currency . ' ' . $detail->activation_price . " \n ";
            }

            if ($detail->fixed_price > 0) {
                $_description .= '<strong>' . $_lang_fixedpayment . '</strong>: ' . $_currency . ' ' . $detail->fixed_price . " \n ";
            }

            if ($detail->suspension_price > 0) {
                $_description .= '<strong>' . $_lang_suspensionpayment . '</strong>: ' . $_currency . ' ' . $detail->suspension_price . " \n ";
            }

            if ($detail->deactivation_price > 0) {
                $_description .= '<strong>' . $_lang_deactivationpayment . '</strong>: ' . $_currency . ' ' . $detail->deactivation_price . " \n ";
            }

            if ($detail->iserial != "") {
                $_description .= '<strong>' . $_lang_view_dispatch_head_serial . '</strong>: ' . $detail->iserial . '<br />';
            }

            $entity->description = $_description;

            $entity->totalPrice = $detail->subtotal;
            $entity->quantity = 1;


            array_push($aDetailProduct, $entity);
        }

        $headDocument = new HeadDocumentExport();
        $headDocument->accountName = $invoice->accountname;
        $headDocument->numberDocument = $invoice->ordercode;
        $headDocument->contactPhone = $invoice->phonenumber;
        $headDocument->contactFirstName = $invoice->userfirstName;
        $headDocument->contactLastName = $invoice->userLastName;

        $headDocument->salesManFisrtName = $invoice->userfirstName;
        $headDocument->salesManLastName = $invoice->userLastName;

        $headDocument->comments = $invoice->invoicecomments;
        $headDocument->dateDocument = $invoice->invoicedate;
        $headDocument->dateDueDocument = $invoice->invoiceduedate;

        $headDocument->billStreet = $invoice->billing_address;
        $headDocument->billCity = $invoice->billing_city;
        $headDocument->billState = $invoice->billing_state;
        $headDocument->billCode = $invoice->billing_zipcode;

        $headDocument->billCountry = $invoice->billing_country;
        $headDocument->webSiteAccount = $invoice->webSite;
        $headDocument->phoneContact = $invoice->phonenumber;

        $headDocument->shippingAddress = $invoice->shipping_address;
        $headDocument->shippingCountry = $invoice->shipping_country;
        $headDocument->shippingState = $invoice->shipping_state;
        $headDocument->shippingCity = $invoice->shipping_city;
        $headDocument->shippingZipCode = $invoice->shipping_zipcode;

        $headDocument->paymentFee = $invoice->payment_fee;
        $headDocument->parcialtotal = $invoice->subtotal;
        $headDocument->taxes = $invoice->taxes;

        $headDocument->shippingCost = $invoice->shipping_cost;
        $headDocument->total = $invoice->x_ammount;

        $headDocument->colorBanner = "#F71907";
        $headDocument->textBanner = ""; //Here we put the banner id
        $headDocument->fontBanner = 100;

        $headDocument->paymentMethod = $invoice->payment_method;
        
        $headDocument->ordercode = $invoice->ordercode; 
        
        
        
        $accountid = $invoice->provideraccountid;

        $view->detailDocument = $aDetailProduct;
        $view->code = $_code;

        $view->lang_index_table = $_lang_index_table_title;
        $view->lang_invoice_number = $_lang_view_label_invoiceno;
        $view->lang_invoice_date = $_lang_view_label_invoicedate;
        $view->lang_invoice_due_date = $_lang_view_label_invoiceduedate;
        $view->typeDocument = $_lang_index_label_module;

        $document = new DocumentGenerateController();
        $document->generateProviderInvoiceService($view, $this, $accountid, $headDocument);
    }

    function downloadpackingslipAction() {
        $view = new ViewController();

        $fileLangTemplate = "../lib/view/invoice/lang/" . strtolower($this->session_langcode) . ".php";
        include $fileLangTemplate;

        $_code = trim($this->aRequest["code"]);

        $dto_invoice = new Dto_Invoice();
        $invoice = $dto_invoice->getByCode($_code);
        $inventoryItems = $dto_invoice->getDetailDispatchedByCode($_code);

        $aDetailProduct = array();

        foreach ($inventoryItems as $detail) {
            $entity = new DetailProductExport();

            $entity->codeProduct = $detail->sku;
            $entity->nameProduct = $detail->productname;
            $entity->quantity = $detail->qty;
            $entity->price = $detail->productprice;
            $entity->description = $detail->iserial;
            $entity->totalPrice = doubleval($detail->productprice) * doubleval($detail->qty);

            array_push($aDetailProduct, $entity);
        }

        $headDocument = new HeadDocumentExport();
        $headDocument->accountName = $invoice->accountname;
        $headDocument->numberDocument = $invoice->ordercode;
        $headDocument->contactPhone = $invoice->phonenumber;
        $headDocument->contactFirstName = $invoice->firstname;
        $headDocument->contactLastName = $invoice->lastname;



        $headDocument->comments = $invoice->invoicecomments;
        $headDocument->dateDocument = $invoice->invoicedate;
        $headDocument->dateDueDocument = $invoice->invoiceduedate;

        $headDocument->billStreet = $invoice->billing_address;
        $headDocument->billCity = $invoice->billing_city;
        $headDocument->billState = $invoice->billing_state;
        $headDocument->billCode = $invoice->billing_zipcode;

        $headDocument->billCountry = $invoice->billing_country;
        $headDocument->webSiteAccount = $invoice->webSite;
        $headDocument->phoneContact = $invoice->phonenumber;

        $headDocument->shippingAddress = $invoice->shipping_address;
        $headDocument->shippingCountry = $invoice->shipping_country;
        $headDocument->shippingState = $invoice->shipping_state;
        $headDocument->shippingCity = $invoice->shipping_city;
        $headDocument->shippingZipCode = $invoice->shipping_zipcode;

        $headDocument->paymentFee = $invoice->payment_fee;
        $headDocument->parcialtotal = $invoice->subtotal;
        $headDocument->taxes = $invoice->taxes;
        $headDocument->shippingCost = $invoice->shipping_cost;
        $headDocument->total = $invoice->x_ammount;

        $accountid = $invoice->provideraccountid;

        $view->detailDocument = $aDetailProduct;
        $view->code = $_code;

        $view->lang_index_table = $_lang_index_table_title;
        $view->lang_invoice_number = $_lang_view_label_invoiceno;
        $view->lang_invoice_date = $_lang_view_label_invoicedate;
        $view->lang_invoice_due_date = $_lang_view_label_invoiceduedate;
        $view->typeDocument = $_lang_index_label_module;

        $document = new DocumentGenerateController();
        $document->generatePackingSlip($view, $this, $accountid, $headDocument);
    }

}
