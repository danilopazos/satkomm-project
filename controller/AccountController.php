<?php

require_once ("../lib/bean/Address.php");
require_once ("../lib/bean/CreditCard.php");
require_once ("../lib/bean/CountryTaxType.php");
require_once ("../lib/bean/Currency.php");


require_once ("../lib/db/Dto_Address.php");
require_once ("../lib/db/Dto_CreditCard.php");
require_once ("../lib/db/Dto_Currency.php");

require_once ("../lib/helper/DatatableHelper.php");

class AccountController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function requestupdatestyleAction() {
        $_accountid = $this->session_accountid;
        $_stylecolor = trim($_REQUEST["stylecolor"]);
        $dto_account = new Dto_Account();
        $dto_account->updateStyleColor($_accountid, $_stylecolor);
        redirect("account", "myaccount");
    }

    function requestupdatemylogoAction() {

        $mediapath = $_FILES["mediaimage_file"]["tmp_name"];
        $filename = $_FILES["mediaimage_file"]["name"];
        $fileext = pathinfo($filename, PATHINFO_EXTENSION);
        $accountid = $this->session_accountid;

        $fopen_media = fopen($mediapath, 'rb');
        $mediaContent = fread($fopen_media, filesize($mediapath));
        $mediaContent = addslashes($mediaContent);

        $dto = new Dto_Account();
        $result = $dto->setLogoByAccountid($accountid, $mediaContent, $fileext, $filename);

        //echo var_dump($result); die();

        redirect("account", "myaccount");
    }
    
    
    
    function indexAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/account/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("account", "index");
    }

    function requestgetlistindexAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('a.name', 'a.code', 'ba.country', 'ba.city', 'a.taxid');
        $_datatableSetup->_sortCol = "a.name";
        $_datatableSetup->_sortDir = "asc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_account = new Dto_Account();
        $totalRecords = $dto_account->getListTotal($this->session_accountid, $datatable->_search);
        $aAccounts = $dto_account->getList($this->session_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aAccounts);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aAccounts as $account) {
            $billaddress = $account->billaddress;
            $country = $billaddress->country;
            $city = $billaddress->city;

            $detailView = '<a href="/account/view/id/' . $account->id . '" class="btn btn-xs btn-default" >' . $account->name . '</a>';

            $output['aaData'][] = array($detailView, $account->code, $country, $city, $account->taxid);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function registerAction() {
        //we define the taxtype per country
        $aCountryTaxType = array();

        $obj1 = new CountryTaxType( );
        $obj1->code = 'PE';
        $obj1->name = 'PERU';
        $obj1->taxtype = 'RUC';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'PY';
        $obj1->name = 'PARAGUAY';
        $obj1->taxtype = 'RUC';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'EC';
        $obj1->name = 'ECUADOR';
        $obj1->taxtype = 'RUC';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'CL';
        $obj1->name = 'CHILE';
        $obj1->taxtype = 'RUT';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'UY';
        $obj1->name = 'URUGUAY';
        $obj1->taxtype = 'RUT';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'BO';
        $obj1->name = 'BOLIVIA';
        $obj1->taxtype = 'NIT';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'CO';
        $obj1->name = 'COLOMBIA';
        $obj1->taxtype = 'NIT';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'BR';
        $obj1->name = 'BRAZIL';
        $obj1->taxtype = 'CPF';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'VE';
        $obj1->name = 'VENEZUELA';
        $obj1->taxtype = 'RIF';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'PA';
        $obj1->name = 'PANAMA';
        $obj1->taxtype = 'RUC';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);

        $obj1 = new CountryTaxType( );
        $obj1->code = 'US';
        $obj1->name = 'USA';
        $obj1->taxtype = 'RESALE CERTIFICATE FOR SALE TAX';
        $obj1->charlimit = 11;
        array_push($aCountryTaxType, $obj1);
 
        $dto_account = new Dto_Account();

        $view = new ViewController();
        $view->id = trim($this->aRequest["id"]);
        $view->session_accountid = $this->session_accountid;
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/account/register.js");

        $view->bodyjquery = "Register.init();  ";
        $view->account = $dto_account->getById($view->id);
        
        //currency relation
        $currencyrelation=$dto_account->getCurrencyRelation($view->session_accountid, $view->id);
        if($currencyrelation==false){
            $view->currencyRelation = "USD";
        } else{
            $view->currencyRelation = $currencyrelation;
        }
        $dto_currency = new Dto_Currency();
        $view->listCurrecies = $dto_currency->getListActive($view->session_accountid);
        
        
        $view->aCountryTaxType = $aCountryTaxType;
        $dto_user = new Dto_User();
        $view->aUsers = $dto_user->getByAccountid($this->session_accountid);

        $view->load("account", "register");
    }

    function viewAction() {

        $_accountid = trim($this->aRequest["id"]);
        if (!($this->canIcheckTheAccount($_accountid))) {
            return;
        }
        $dto_account = new Dto_Account();

        $view = new ViewController();
        $view->id = $_accountid;
        $view->account = $dto_account->getById($_accountid);

        $dto_profile = new Dto_Profile();



        //we detect if is client or distributor
        $iscustomer = false;
        if (intval($view->account->parentaccountid) > 10001) {
            //isclient
            $view->aProfile = $dto_profile->getProfileClient();
            $iscustomer = true;
        } else {
            $view->aProfile = $dto_profile->getProfiles();
        }
        
       
        $view->session_profileid = $this->session_profileid;
        $view->session_accountid = $this->session_accountid;
        $view->iscustomer = $iscustomer;

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");

        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");

        if ($iscustomer) {
            array_push($view->aJavascript, "assets/js/modules/account/view_customer.js");
        } else {
            array_push($view->aJavascript, "assets/js/modules/account/view.js");
        }

        $view->bodyjquery = "TableUsersData.init(); RegisterUser.init();  RegisterUserUpdate.init();   UserChangePassword.init();  UserChangeUsername.init();   UserChangeStatus.init();  ";
        $view->load("account", "view");
    }

    function myaccountAction() {
        $_accountid = trim($this->session_accountid);
        if (!($this->canIcheckTheAccount($_accountid))) {
            return;
        }
        $dto_account = new Dto_Account();

        $view = new ViewController();
        $view->id = $_accountid;
        $view->account = $dto_account->getById($_accountid);
        $view->session_accountid = $this->session_accountid;

        $dto_profile = new Dto_Profile();
        $view->aProfile = $dto_profile->getProfiles();
        $view->aStyles = $this->aStyles;

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aCss, "assets/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css");
        array_push($view->aCss, "assets/bower_components/summernote/dist/summernote.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-fileinput/css/fileinput.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/account/view.js");

        $view->bodyjquery = "TableUsersData.init(); RegisterUser.init();  RegisterUserUpdate.init();   UserChangePassword.init();  UserChangeUsername.init();   UserChangeStatus.init();  ";
        $view->load("account", "view");
    }

    //--- we get the account info
    function requestgetaccountbyidAction() {

        $_accountid = trim($_REQUEST["accountid"]);

        if (!($this->canIcheckTheAccount($_accountid))) {
            return;
        }


        $dto_account = new Dto_Account();
        $account = $dto_account->getById($_accountid);

        $view = new ViewController();
        $view->data = $account;
        $view->load("template", "json");
    }

    //-- we get users by account ----
    function requestgetusersbyaccountAction() {
        $dto_account = new Dto_Account();
        $_accountid = trim($this->aRequest["accountid"]);
        $account = $dto_account->getById($_accountid);

        $iscustomer = false;
        if (intval($account->parentaccountid) > 10001) {
            $iscustomer = true;
        }
        
        
        $fileLangTemplate = "../lib/view/account/lang/" . strtolower($this->session_langcode) . ".php";
        if(file_exists($fileLangTemplate)){
            include $fileLangTemplate;
        }


        if (!($this->canIcheckTheAccount($_accountid))) {
            return;
        }

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('id', 'username', 'profileid', 'firstname', 'lastname', 'mobile', 'phone');
        $_datatableSetup->_sortCol = "a.name";
        $_datatableSetup->_sortDir = "asc";

        //$datatableHelper = new DatatableHelper();
        //$datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_user = new Dto_User();
        $aUsers = $dto_user->getByAccountid($_accountid);

        $totalRecords = count($aUsers);
        $totalFiltered = count($aUsers);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        $dto_profile = new Dto_Profile();
        foreach ($aUsers as $user) {

            $detailView = '<a href="#"  onclick="fnc_user_viewmodal(\'' . $user->id . '\')"    class="btn btn-xs btn-default  user-viewmodal  " title="' . $user->id . '"  >' . $user->username . '</a>';
            $phone = $user->phone;
            if ($user->phone != "" && $user->phoneext != "") {
                $phone .=" Ext: " . $user->phoneext;
            }
            if ($phone == "") {
                $phone = "---";
            }

            $link_changepassword = '<a href="#"  onclick="fnc_user_changepassword(\'' . $user->id . '\')"    class="btn btn-xs btn-default  user-viewmodal  " title="' . $user->id . '"  ><i class="fa fa-lock"></i></a>';

            if ($iscustomer) {
                $link_changepassword = "";
            }

            $link_envelop = '<a href="#"  onclick="fnc_user_changeemail(\'' . $user->id . '\')"    class="btn btn-xs btn-default  user-viewmodal  " title="' . $user->id . '"  ><i class="fa fa-envelope-o"></i></a>';
            $link_edit = '<a href="#"  onclick="fnc_user_viewmodal(\'' . $user->id . '\')"    class="btn btn-xs btn-default  user-viewmodal  " title="' . $user->id . '"  ><i class="fa fa-pencil"></i></a>';
            $link_status = '<a href="#"  onclick="fnc_user_changestatus(\'' . $user->id . '\',   \'' . $user->status . '\' )"    class="btn btn-xs btn-default  user-viewmodal  " title="' . $user->id . '"  ><i class="fa fa-cog"></i></a>';

            $lbl_status = '<a href="#"  class="btn btn-xs btn-green  user-viewmodal  "  ><i class="fa fa-circle"></i></a>';
            if ($user->status == 0) {
                $lbl_status = '<a href="#"  class="btn btn-xs btn-red  user-viewmodal  "  ><i class="fa fa-circle"></i></a>';
            }


            $profileValue = "";
            if ($iscustomer) {
                $profile = $dto_profile->getProfileClientById($user->profileid);
                 
                if ($profile->id == 'SKIUCUAR') {
                    $profileValue = $_lang_profile_SKIUCUAR;
                }
                if ($profile->id == 'SKIUCURL') {
                    $profileValue = $_lang_profile_SKIUCURL;
                }
                if ($profile->id == 'SKIUCUAL') {
                    $profileValue = $_lang_profile_SKIUCUAL;
                }
            } else {
                $profile = $dto_profile->getProfileById($user->profileid);
                $profileValue = $profile->name;
            }

            $output['aaData'][] = array($lbl_status, $detailView, $profileValue, $user->firstname, $user->lastname, $user->mobile, $phone, $link_changepassword . ' &nbsp; ' . $link_envelop . ' &nbsp; ' . $link_edit . '  &nbsp;   ' . $link_status);
        }


        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    //-- we get credit cards by account
    function requestgetcreditcardsbyaccountAction() {

        $_accountid = trim($_REQUEST["accountid"]);

        if (!($this->canIcheckTheAccount($_accountid))) {
            return;
        }
 
        $dto_creditCard = new Dto_CreditCard();
        $aCreditCard = $dto_creditCard->getByAccountid($_accountid);

        $view = new ViewController();
        $view->data = $aCreditCard;
        $view->load("template", "json");
    }

    //-- we create a new account
    function requestregisteraccountAction() {

        $ok = 1; //todo ok
        $operation = "update";
        $_parentaccountid = $this->session_accountid;

        $account = $this->getRequestAccount();
        $account->parentaccountid = $_parentaccountid;
        $billAddress = $this->getRequestBillAddress();
        $shipAddress = $this->getRequestShipAddress();

        $dto_account = new Dto_Account();

        $account->modifiedbyuser = $this->session_userid;
        if ($account->id == "") {
            $account->id = $dto_account->generateNewId();
            $operation = "create";
            $account->createdbyuser = $this->session_userid;
        }

        if ($account->subdomain != "") {
            // we need to check if the subdomain is unique        
            $tempaccountid = $dto_account->getAccountBySubdomain($account->subdomain);

            if ($operation == "create" && intval($tempaccountid) > 1000) {
                $ok = -4;
            }

            if ($operation == "update" && intval($tempaccountid) > 1000 && intval($tempaccountid) != intval($account->id)) {
                $ok = -4;
            }
        }

        if ($account->code != "") {
  
            $idbycode = $dto_account->getIdByCode($account->code);
            if ($operation == "create" && $idbycode > 0) {
                $ok = -1;
            } else if ($operation == "update" && $idbycode > 0 && $idbycode != $account->id) {
                $ok = -1;
            }
        } else {
            $account->code = $_parentaccountid . '-' . $dto_account->generateNewCode($_parentaccountid);
            
            /*
            $totalchilds = $dto_account->gettotalchidls( $this->session_accountid  );
            
            $account->code = $this->session_accountid.'-'.$totalchilds;
           */
        }

        if ($account->name == "" || ( $billAddress->country == "" || $billAddress->state == "" || $billAddress->city == "" || $billAddress->street == "" || $billAddress->zipcode == "" ) || ( $shipAddress->country == "" || $shipAddress->state == "" || $shipAddress->city == "" || $shipAddress->street == "" || $shipAddress->zipcode == "" )
        ) {
            $ok = -2;
        }



        if ($ok == 1) {
            $dto_address = new Dto_Address();
            if ($operation == "create") {

                $billaddressid = $dto_address->create($billAddress->type, $billAddress->country, $billAddress->state, $billAddress->city, $billAddress->street, $billAddress->zipcode, $billAddress->pobox);
                $shipaddressid = $dto_address->create($shipAddress->type, $shipAddress->country, $shipAddress->state, $shipAddress->city, $shipAddress->street, $shipAddress->zipcode, $shipAddress->pobox);

                $account->billaddressid = $billaddressid;
                $account->shipaddressid = $shipaddressid;

                $result = $dto_account->create($account);
                if (!($result)) {
                    $ok = -3;
                }
            } else if ($operation == "update") {

                $originalAccount = $dto_account->getById($account->id);

                //echo var_dump($originalAccount); die();

                $dto_address->update($originalAccount->billaddressid, $billAddress->country, $billAddress->state, $billAddress->city, $billAddress->street, $billAddress->zipcode, $billAddress->pobox);
                $dto_address->update($originalAccount->shipaddressid, $shipAddress->country, $shipAddress->state, $shipAddress->city, $shipAddress->street, $shipAddress->zipcode, $shipAddress->pobox);

                $account->billaddressid = $originalAccount->billaddressid;
                $account->shipaddressid = $originalAccount->shipaddressid;

                $result = $dto_account->update($account);
                if (!($result)) {
                    $ok = -3;
                }
            }
        }

        $view = new ViewController();
        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = $account;
        }
        $view->template = "blank";
        $view->load("template", "json");
    }

    function requestupdatestatusAction() {

        $ok = 1;
        $_accountid = trim($_REQUEST["accountid"]);
        $_status = trim($_REQUEST["status"]);

        if (!($this->canIcheckTheAccount($_accountid))) {
            $ok = -1;
        }

        $dto_account = new Dto_Account();
        $result = $dto_account->updateStatus($_accountid, $_status);

        if (!$result) {
            $ok = -2;
        }


        $view = new ViewController();
        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = array("success" => 1);
        }
        $view->load("template", "json");
    }

    function requestdeleteAction() {

        $ok = 1;
        $_accountid = trim($_REQUEST["accountid"]);

        if (!($this->canIcheckTheAccount($_accountid))) {
            $ok = -1;
        }

        $dto_account = new Dto_Account();
        $result = $dto_account->delete($_accountid);

        if (!$result) {
            $ok = -2;
        }


        $view = new ViewController();
        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = array("success" => 1);
        }
        $view->load("template", "json");
    }

    //-- Account functions --- 
    function getRequestAccount() {

        $_id = intval(trim($_REQUEST["id"]));
        $_code = trim($_REQUEST["code"]);
        $_name = trim($_REQUEST["name"]);
        $_email = trim($_REQUEST["email"]);
        $_website = trim($_REQUEST["website"]);
        $_phone = trim($_REQUEST["phone"]);
        $_phoneext = trim($_REQUEST["phoneext"]);

        $_taxid = trim($_REQUEST["taxid"]);


        $_aTaxtypeid = trim($_REQUEST["taxtypeid"]);
        $aElement = split("_", $_aTaxtypeid);
        $_countrycode = trim($aElement[0]);
        $_taxtypeid = trim($aElement[1]);

        
        $_creditlimit = doubleval($_REQUEST["creditlimit"]);
        $_invoiceduedays = intval($_REQUEST["invoiceduedays"]);
        $_currencyrelation = $_REQUEST["currencyrelation"];
        $_confquotevaliddays = intval($_REQUEST["confquotevaliddays"]);
        $_confinvoicedue = intval($_REQUEST["confinvoicedue"]);
        $_conftaxespercent = doubleval($_REQUEST["conftaxespercent"]); 
        
        $_invoiceprefix = trim($_REQUEST["invoiceprefix"]);
        $_quoteprefix = trim($_REQUEST["quoteprefix"]);
        
        
        $_subdomain = trim($_REQUEST["subdomain"]);

        $_ownerid = trim($_REQUEST["ownerid"]);
        
        
        $_customplansactive = intval($_REQUEST["customplansactive"]);
        

        $account = new Account();
        $account->id = $_id;
        $account->code = $_code;
        $account->countrycode = $_countrycode;
        $account->name = $_name;
        $account->email = $_email;
        $account->website = $_website;
        $account->phone = $_phone;
        $account->phoneext = $_phoneext;
        $account->taxid = $_taxid;
        $account->taxtypeid = $_taxtypeid;
        $account->subdomain = $_subdomain;
        $account->ownerid = $_ownerid;
        
        $account->creditlimit = $_creditlimit;
        $account->invoiceduedays = $_invoiceduedays;
        $account->currencyrelation = $_currencyrelation;
        $account->confquotevaliddays = $_confquotevaliddays;
        $account->confinvoicedue = $_confinvoicedue;
        $account->conftaxespercent = $_conftaxespercent;
        
        $account->invoiceprefix = $_invoiceprefix;
        $account->quoteprefix = $_quoteprefix;
        
        $account->customplansactive = $_customplansactive;
        
        return $account;
    }

    function getRequestBillAddress() {

        $_bill_country = trim($_REQUEST["bill_country"]);
        $_bill_state = trim($_REQUEST["bill_state"]);
        $_bill_city = trim($_REQUEST["bill_city"]);
        $_bill_street = trim($_REQUEST["bill_street"]);
        $_bill_zipcode = trim($_REQUEST["bill_zipcode"]);
        $_bill_pobox = trim($_REQUEST["bill_pobox"]);

        $billAddress = new Address();
        $billAddress->country = $_bill_country;
        $billAddress->state = $_bill_state;
        $billAddress->city = $_bill_city;
        $billAddress->street = $_bill_street;
        $billAddress->zipcode = $_bill_zipcode;
        $billAddress->pobox = $_bill_pobox;
        $billAddress->type = 'BILLING';
        return $billAddress;
    }

    function getRequestShipAddress() {

        $_ship_country = trim($_REQUEST["ship_country"]);
        $_ship_state = trim($_REQUEST["ship_state"]);
        $_ship_city = trim($_REQUEST["ship_city"]);
        $_ship_street = trim($_REQUEST["ship_street"]);
        $_ship_zipcode = trim($_REQUEST["ship_zipcode"]);
        $_ship_pobox = trim($_REQUEST["ship_pobox"]);

        $shipAddress = new Address();
        $shipAddress->country = $_ship_country;
        $shipAddress->state = $_ship_state;
        $shipAddress->city = $_ship_city;
        $shipAddress->street = $_ship_street;
        $shipAddress->zipcode = $_ship_zipcode;
        $shipAddress->pobox = $_ship_pobox;
        $shipAddress->type = 'SHIPPING';
        return $shipAddress;
    }

}
