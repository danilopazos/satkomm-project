<?php
require_once ("../lib/bean/Fees.php");
require_once ("../lib/bean/Taxes.php");
require_once ("../lib/db/Dto_Taxes.php");
require_once ("../lib/db/Dto_Fees.php");

require_once ("../lib/helper/DatatableHelper.php");

class SystemController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }
    
    
    function feesAction(){
        $dto = new Dto_Fees();
        $entity = $dto->getFeesByAccountId($this->session_accountid);
        $view = new ViewController();
        $view->entity = $entity;
        $view->load("system", "fees");
    }
    
    function feeseditAction(){
        $dto = new Dto_Fees();
        $entity = $dto->getFeesByAccountId($this->session_accountid);
        $view = new ViewController();
        
        
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/system/feesedit.js");

        $view->bodyjquery = "Register.init();  ";  
        
        $view->entity = $entity;
        $view->load("system", "feesedit");        
    }
    
    function requestfeeseditAction(){
        $_accountid = $this->session_accountid;
        $_creditcard = doubleval($_REQUEST["creditcard"]);
        $_wiretransfer = doubleval($_REQUEST["wiretransfer"]);
        $result = false;
      
        $dto = new Dto_Fees();
        $result = $dto->setFeesByAccountId($_accountid, $_creditcard, $_wiretransfer);
        
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result"=>$result);
        $view->load("template", "json");        
        
    }
    
    
    
    function taxesAction() {
        $dto = new Dto_Taxes();
        $entity = $dto->getTaxes($this->session_accountid);
        $view = new ViewController();
        $view->entity = $entity;
        $view->load("system", "taxes");
    }

    function taxeseditAction() {
        $dto = new Dto_Taxes();
        $entity = $dto->getTaxes($this->session_accountid);
        $view = new ViewController();
        $view->entity = $entity;
        
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/system/taxesedit.js");

        $view->bodyjquery = "Register.init();  ";        
        
        $view->load("system", "taxesedit");
    }

    function requesttaxeseditAction() {
        $_accountid = $this->session_accountid;
        $result = false;
        if (isset($_REQUEST["saletaxes"]) && isset($_REQUEST["communicationtaxes"])) {

            $_saletaxes = doubleval($_REQUEST["saletaxes"]);
            $_communicationtaxes = doubleval($_REQUEST["communicationtaxes"]);
            $dto = new Dto_Taxes();
            $entity = new Taxes();
            $entity->accountid = $_accountid;
            $entity->saletaxes = $_saletaxes;
            $entity->communicationtaxes = $_communicationtaxes;

            $result = $dto->setTaxes($_accountid, $entity);
        }
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result"=>$result);
        $view->load("template", "json");
    }
    
}
