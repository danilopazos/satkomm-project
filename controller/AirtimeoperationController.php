<?php

require_once ("../lib/bean/AirtimeOperation.php");
require_once ("../lib/bean/Plan.php");
require_once ("../lib/bean/AirtimeOrder.php");
require_once ("../lib/bean/InventoryItem.php");
require_once ("../lib/bean/PlanTrafficType.php");
require_once ("../lib/bean/Fees.php");
require_once ("../lib/bean/Taxes.php");
require_once ("../lib/bean/Invoice.php");

require_once ("../lib/db/Dto_Plan.php");
require_once ("../lib/db/Dto_Invoice.php");
require_once ("../lib/db/Dto_Inventory.php");
require_once ("../lib/db/Dto_AirtimeOperation.php");
require_once ("../lib/db/Dto_Fees.php");
require_once ("../lib/db/Dto_Taxes.php");

require_once ("../lib/helper/DatatableHelper.php");

class AirtimeoperationController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    public function searchairtimeorderAction() {
        $_iserial = trim($this->aRequest["iserial"]);
        $dto_airtimeoperation = new Dto_AirtimeOperation();
        $airtimeordercode = $dto_airtimeoperation->getOrdercodeByIserial($_iserial);
        redirect("airtimeoperation", "vieworder", array("orderid" => $airtimeordercode));
    }

    public function myordersAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");

        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/airtimeoperation/myorders.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("airtimeoperation", "myorders");
    }

    function requestgetmyordersAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('ao.code', 'ao.orderdate', 'ao.totalitems', 'ao.totalpayment', 'ao.orderstatus');
        $_datatableSetup->_sortCol = "ao.orderdate";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_AirtimeOperation();
        $totalRecords = $dto_entity->getOrdersByAccountCustomerTotal($this->session_accountid, $datatable->_search);
        $aRecords = $dto_entity->getOrdersByAccountCustomer($this->session_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
         
        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );


        foreach ($aRecords as $record) {

            $detailView = '<a href="/airtimeoperation/vieworder/orderid/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->code . '</a>';

            $orderstatus = '<a href="#" class="btn btn-xs btn-orange" >' . $record->orderstatus . '</a>';
            if ($record->orderstatus == "EXECUTED") {
                $orderstatus = '<a href="#" class="btn btn-xs btn-green" >' . $record->orderstatus . '</a>';
            }


            $output['aaData'][] = array($detailView, substr($record->orderdate, 0, 10), $record->totalitems, moneyFormat($record->totalpayment), $orderstatus);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    public function customerordersAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");

        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/airtimeoperation/customerorders.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("airtimeoperation", "customeroders");
    }

    function requestgetcustomerordersAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('ao.code', ' ac.name', 'ao.orderdate', 'ao.totalitems', 'ao.totalpayment', 'ao.orderstatus');
        $_datatableSetup->_sortCol = "ao.orderdate";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_AirtimeOperation();
        $totalRecords = $dto_entity->getOrdersByAccountProviderTotal($this->session_accountid, $datatable->_search);
        $aRecords = $dto_entity->getOrdersByAccountProvider($this->session_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {

            $detailView = '<a href="/airtimeoperation/vieworder/orderid/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->code . '</a>';

            $orderstatus = '<a href="#" class="btn btn-xs btn-orange" >' . $record->orderstatus . '</a>';
            if ($record->orderstatus == "EXECUTED") {
                $orderstatus = '<a href="#" class="btn btn-xs btn-green" >' . $record->orderstatus . '</a>';
            }


            $output['aaData'][] = array($detailView, $record->accountcustomer->name, substr($record->orderdate, 0, 10), $record->totalitems, moneyFormat($record->totalpayment), $orderstatus);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    public function mycartAction() {

        $dto_entity = new Dto_AirtimeOperation();
        $view = new ViewController();
        $view->accountid = $this->session_accountid;
        $view->aCart = $dto_entity->getRequestOperationsByCart($this->session_accountid, "requested");

        if (!$view->aCart) {
            redirect("airtimeoperation", "emptycart");
        }

        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/airtimeoperation/mycart.js");

        $view->bodyjquery = "MyCart.init();  ";


        $dto_fees = new Dto_Fees();
        $dto_taxes = new Dto_Taxes();

        $view->fees = $dto_fees->getFeesByAccountId($this->session_parentaccountid);
        $view->taxes = $dto_taxes->getTaxes($this->session_parentaccountid);

        if ($this->session_account_shippingcountry != "USA") {
            $view->taxes->saletaxes = 0;
            $view->taxes->communicationtaxes = 0;
        }


        $view->load("airtimeoperation", "mycart");
    }

    public function emptycartAction() {
        $view = new ViewController();
        $view->load("airtimeoperation", "emptycart");
    }

    public function myairtimerequestedAction() {
        $view = new ViewController();
        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/airtimeoperation/myairtimerequested.js");

        $view->bodyjquery = "TableData.init();  ";

        $view->load("airtimeoperation", "myairtimerequested");
    }

    public function viewAction() {
        $_id = trim($this->aRequest["id"]);
        $dto_airtimeoperation = new Dto_AirtimeOperation();
        $entity = $dto_airtimeoperation->getById($_id);


        $view = new ViewController();
        $view->entity = $entity;
        $view->accountid = $this->session_accountid;
        $view->load("airtimeoperation", "view");
    }

    function requestdeleterequestedoperationAction() {
        $dto_airtimeoperation = new Dto_AirtimeOperation();
        $dto_inventory = new Dto_Inventory();
        $_id = trim($this->aRequest["id"]);
        $ao = $dto_airtimeoperation->getById($_id);
        $result = false;
        if ($ao->requeststatus == "requested") {
            $result = $dto_airtimeoperation->deleteById($_id);
            $result2 = $dto_inventory->setInventoryOperationRequested($ao->inventoryitemid, '');
        }

        $output = array();
        if ($result) {
            $output = array("result", $result);
        }


        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestgetmyartimerequestedAction() {

        $_accountid = intval($this->session_accountid);

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('a.id', 'a.operation', 'as.name', 'a.requesteddatetime', 'a.datetoexecute', 'a.datetoexecute', 'a.productname', 'a.planname', 'a.totalpayment');
        $_datatableSetup->_sortCol = " a.datetoexecute ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_AirtimeOperation();

        $totalRecords = $dto_entity->getMyRequestedOperationTotal($_accountid, $datatable->_search);


        $aRecords = $dto_entity->getMyRequestedOperation($_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {
            $image_product = '<img width="45" height="45" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/' . $record->productid . '/type/thumb"  >';

            $detailView = '<a href="/airtimeoperation/view/id/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->operation . '</a>';

            $output['aaData'][] = array($image_product, $detailView, $record->accountassigned->name, substr($record->requesteddatetime, 0, 10), $record->datetoexecute, $record->productname, $record->planname, moneyFormat($record->totalpayment), $record->requeststatus);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestplaceorderAction() {

        $dto_airtime = new Dto_AirtimeOperation();
        $items = $this->getCartItemsByRequest();


        $e = $this->getAirtimeorderByRequest();
        $e->code = $dto_airtime->generatecode($this->session_parentaccountid);
        $e->id = $dto_airtime->generateId();

        $result = $dto_airtime->placeOrder($e);

        if ($result) {
            $result = $dto_airtime->setOrderCodeToAirtimeOperations($e->code, $items);
        }

        //set inventory item in order
        $dto_inventory = new Dto_Inventory();
        if ($result) {
            $result_changestate = $dto_inventory->setInventoryOperationRequestedByBatch($items, 'airtime_order');
        }

        $view = new ViewController();
        $view->template = "blank";

        $output = array();
        if ($result) {
            $output = array("id" => $e->id);
        } else {
            $output = array("id" => false);
        }


        $view->data = $output;
        $view->load("template", "json");
    }

    function getAirtimeorderByRequest() {
        $e = new AirtimeOrder();
        $e->totalitems = intval($_REQUEST["totalitems"]);
        $e->accountcustomerid = $this->session_accountid;
        $e->accountproviderid = $this->session_parentaccountid;
        $e->sessionid = session_id();
        $e->userid = $this->session_userid;
        $e->orderdate = date("Y-m-d");
        $e->userip = get_ip();
        $e->subtotal = doubleval($_REQUEST["subtotal"]);
        $e->taxes = doubleval($_REQUEST["taxes"]);
        $e->payment_method = trim($_REQUEST["payment_method"]);
        $e->convenience_fee = doubleval($_REQUEST["convenience_fee"]);
        $e->totalpayment = doubleval($_REQUEST["totalpayment"]);
        $e->ordercomments = trim($_REQUEST["ordercomments"]);

        return $e;
    }

    function getCartItemsByRequest() {
        $aIds = array();
        for ($i = 1; $i <= intval($_REQUEST["totalitems"]); $i++) {
            $cartid = trim($_REQUEST["cartid_" . $i]);
            $inventoryitemid = trim($_REQUEST["inventoryitemid_" . $i]);

            $aRecord = array("cartid" => $cartid, "inventoryitemid" => $inventoryitemid);

            $isdeleted = trim($_REQUEST["aodeleted_" . $cartid]);
            if ($isdeleted == 0) {
                array_push($aIds, $aRecord);
            }
        }
        return $aIds;
    }

    function vieworderAction() {
        $_orderid = trim($this->aRequest["orderid"]);
        $dto_entity = new Dto_AirtimeOperation();

        $order = $dto_entity->getOrderById($_orderid);

        $_items = $dto_entity->getRequestOperationsByStatus($order->code);

        $operationsexecuted = true;
        $total = count($_items);
        if ($total > 0) {
           
            foreach ($_items as $item) {
                if ($item->requeststatus != "executed") {
                    $operationsexecuted = false;
                }
            }
        }

        if ($operationsexecuted) {
            $dto_entity->setAirtimeOrderStatus($_orderid, "EXECUTED");
            $order->orderstatus = 'EXECUTED';
        }
        
        
        
        $view = new ViewController();
        
        
        
        if($order->orderstatus == 'EXECUTED'){
            $dto_invoice = new Dto_Invoice();
            $_invoice = $dto_invoice->getInvoiceByOrderCode($order->code, "SERVICES");
            
           
            if(!$_invoice){
                
               $view->invoice   = $dto_invoice->generateInvoiceByAirtime( $order->code , $this->session_userid );
              
            }else{
                $view->invoice = $_invoice;
            }
        } 
          
        if ($this->canIProcessOrder($this->session_accountid, $order->code)) {

            $view->processOrder = true;
        } else {
            $view->processOrder = false;
        }
        
        //---------Archivos ----------------
        /*
        array_push($view->aCss, "assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css");
        array_push($view->aCss, "assets/plugin/bootstrap-timepicker.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-timepicker/css/timepicker.less");

        array_push($view->aJavascript, "assets/bower_components/moment/min/moment.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js");
        array_push($view->aJavascript, "assets/bower_components/autosize/dist/autosize.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        */
        //--------- ----------------
        
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.mask.js");
        
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/airtimeoperation/vieworder.js");
        $view->bodyjquery = "FormElements.init(); Register.init();  ";

        $view->entity = $order;
        $view->aCart = $_items;
        $view->load("airtimeoperation", "vieworder");
    }

    function canIProcessOrder($_parentaccountid, $_code) {
        $dto_order = new Dto_AirtimeOperation();
        $orderid = $dto_order->getCodeByChildOrder($_parentaccountid, $_code);

        if ($orderid == false) {
            return false;
        }

        return true;
    }

    function requestexecuteAction() {

        $operationid = trim($_REQUEST["operationid"]);
        $_voicenumber = trim($_REQUEST["voicenumber"]);
        $_datanumber = trim($_REQUEST["datanumber"]);
        $_faxnumber = trim($_REQUEST["faxnumber"]); 
        $_imsi = trim($_REQUEST["imsi"]);
        $_puk1 = trim($_REQUEST["puk1"]);
        $_puk2 = trim($_REQUEST["puk2"]);
        $_pin = trim($_REQUEST["pin"]);  
        
        $_expirydate = trim($_REQUEST["expirydate"]); 
        
        $_operation = trim($_REQUEST["operation"]);

        $processedbyuserid = $this->session_userid;

        $ok = true;
        $view = new ViewController();
        $view->template = "blank";

        //we need to validate if this numbers are ok
        $dto_entity = new Dto_AirtimeOperation();
        $ao = $dto_entity->getById($operationid);

        if ($_operation == "activate") {

            $errorVoicenumberRepeated = false;
            $errorDatanumberRepeated = false;
            $errorFaxnumberRepeated = false;
            $errorImsiRepeated = false;


            $aVoicenumber = $dto_entity->getVoicenumber($_voicenumber);
            $aDatanumber = $dto_entity->getDatanumber($_datanumber);
            $aFaxnumber = $dto_entity->getFaxnumber($_faxnumber);
            $aImsi = $dto_entity->getImsi($_imsi);
            
            if (!$aVoicenumber) {
                $total = count($aVoicenumber);
                if ($total > 0) {
                    foreach ($aVoicenumber as $tempserial) {
                        if ($tempserial != $ao->iserial) {
                            $ok = false;
                            $errorVoicenumberRepeated = true;
                        }
                    }
                }
            }

            if (!$aDatanumber) {
                $total = count($aDatanumber);
                if ($total > 0) {
                    foreach ($aDatanumber as $tempserial) {
                        if ($tempserial != $ao->iserial) {
                            $ok = false;
                            $errorDatanumberRepeated = true;
                        }
                    }
                }
            }

            if (!$aFaxnumber) {
                $total = count($aFaxnumber);
                if ($total > 0) {
                    foreach ($aFaxnumber as $tempserial) {
                        if ($tempserial != $ao->iserial) {
                            $ok = false;
                            $errorFaxnumberRepeated = true;
                        }
                    }
                }
            }

            if (!$aImsi) {
                $total = count($aImsi);
                if ($total > 0) {
                    foreach ($aImsi as $tempserial) {
                        if ($tempserial != $ao->iserial) {
                            $ok = false;
                            $errorImsiRepeated = true;
                        }
                    }
                }
            }
            
            
            

            if ($ok) {

                $result1 = false;
                $result2 = false;

                $result1 = $dto_entity->executeActivation($operationid, $processedbyuserid, $_voicenumber, $_datanumber, $_faxnumber,  $_imsi,  $_expirydate ,  $_puk1, $_puk2, $_pin);

                if ($result1) {
                    $result2 = $dto_entity->executeActivationToDevice($ao->accountid, $ao->iserial, $ao->operation, $_voicenumber, $_datanumber, $_faxnumber, $_imsi, $_expirydate,   $_puk1, $_puk2, $_pin);
                }

                if (!($result1 && $result2)) {
                    $ok = false;
                }
            }

            if ($ok) {
                $view->data = array("result" => 1);
            } else {
                $view->data = array("result" => 0, "repeatedvoicenumber" => $errorVoicenumberRepeated, 
                     "repeateddatanumber" => $errorDatanumberRepeated ,
                     "repeatedfaxnumber" => $errorFaxnumberRepeated ,
                     "repeatedimsi" => $errorImsiRepeated
                        );
            }
        } else if ($_operation == "suspend") {

            $result1 = false;
            $result2 = false;

            $result1 = $dto_entity->executeSuspend($operationid, $processedbyuserid, $ao->iserial);

            if ($result1) {
                $result2 = $dto_entity->executeSuspendToDevice($ao->accountid, $ao->iserial, $ao->operation);
            }

            if (!($result1 && $result2)) {
                $ok = false;
            }

            if ($ok) {
                $view->data = array("result" => 1);
            } else {
                $view->data = array("result" => 0);
            }
        } else if ($_operation == "deactivate") {

            $result1 = false;
            $result2 = false;

            $result1 = $dto_entity->executeDeactivation($operationid, $processedbyuserid, $ao->iserial);

            if ($result1) {
                $result2 = $dto_entity->executeDeactivationToDevice($ao->accountid, $ao->iserial, $ao->operation);
            }

            if (!($result1 && $result2)) {
                $ok = false;
            }

            if ($ok) {
                $view->data = array("result" => 1);
            } else {
                $view->data = array("result" => 0);
            }
        }

        $view->load("template", "json");
    }

}
