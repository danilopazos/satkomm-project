<?php

require_once("../lib/util/smsservice/ConfigSms.php");
require __DIR__ . '/Twilio/autoload.php';

use Twilio\Rest\Client;

class SmsController extends BaseController {

    public function send($to, $content) {
        $config = new ConfigSms();
        
        $client = new Client( $config->account_sid , $config->auth_token );
        $client->messages->create($to, array('from' => $config->from , 'body' => $content));
    }

}
