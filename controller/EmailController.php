<?php
require_once("../lib/util/mailservice/ConfigEmail.php");
require_once("../lib/util/mailservice/class.phpmaileroauthgoogle.php");
require_once("../lib/util/mailservice/class.smtp.php");
require_once("../lib/util/mailservice/class.phpmailer.php");


class EmailController extends BaseController {
    
 
    public function send($email, $name, $emailsubject, $emailcontent, $_fromname  = "") {
        
        
        if($_fromname ==""){  $_fromname = $configEmail->fromname; }
        
        $configEmail = new ConfigEmail();

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $configEmail->smtphost;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $configEmail->username;                 // SMTP username
        $mail->Password = $configEmail->password;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $configEmail->smtpport;                                    // TCP port to connect to
        $mail->SMTPAuth = true;
        $mail->setFrom($configEmail->fromemail, $_fromname);

        $mail->addAddress($email, $name);     // Add a recipient

        $mail->addReplyTo($configEmail->fromemail, $_fromname);

        //---CAMBIAR LOS DESTINATARIOS -----------------/
        /*
          $mail->addBCC('vishal_sodani@yahoo.com');
          $mail->addBCC('franco@intiways.travel');
          $mail->addBCC('marketing@intiways.travel');
          $mail->addBCC('info@intiways.travel');
         */
        $mail->isHTML(true);

        $mail->Subject = $emailsubject;
        $mail->Body = $emailcontent;
        //$mail->AltBody = $emailcontent;
        if (!$mail->send()) {
         return 0;
        } else {
          return 1  ;
        }
    } 
    
    public function sendArray($aEmail, $name, $emailsubject, $emailcontent, $fromeemail="", $fromname="") {
        
        if($fromname ==""){
            $fromname = $configEmail->fromname;
        }
        if($fromeemail ==""){
            $fromeemail = $configEmail->fromemail;
        }
        
        
        $_totalEmails = count($aEmail);
        
        if($_totalEmails == 0){return 0;}
        
        $configEmail = new ConfigEmail();
        
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $configEmail->smtphost;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $configEmail->username;                 // SMTP username
        $mail->Password = $configEmail->password;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $configEmail->smtpport;                                    // TCP port to connect to
        $mail->SMTPAuth = true;
        $mail->setFrom($fromeemail, $fromname);

        $mail->addAddress($aEmail[0], $name);     // Add a recipient


        
        $mail->addReplyTo($fromeemail, $fromname );

        //---CAMBIAR LOS DESTINATARIOS -----------------/
        if($_totalEmails > 1){
            for( $i=1; $i<$_totalEmails; $i++  ){
                $mail->addCC($aEmail[$i]);
            }
        }

        
        /*
          $mail->addBCC('vishal_sodani@yahoo.com');
          $mail->addBCC('franco@intiways.travel');
          $mail->addBCC('marketing@intiways.travel');
          $mail->addBCC('info@intiways.travel');
         */
        $mail->isHTML(true);

        $mail->Subject = $emailsubject;
        $mail->Body = $emailcontent;
        //$mail->AltBody = $emailcontent;
        if (!$mail->send()) {
         return 0;
        } else {
          return 1  ;
        }
    } 
    
    
} 