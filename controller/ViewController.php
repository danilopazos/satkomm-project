<?php

class ViewController {

    public $template = "";
    public $aJavascript = array();
    public $aCss = array();
    public $aLangCodes = array();
    public $http_host = "";
    public $server_protocol = "";
    public $lang_code = "eng";
    public $session_user = array();
    public $session_firstname = "";
    public $session_lastname = "";
    public $session_profileid = "";
    public $stylecolor = "";
    public $session_timezonename = "Europe/London";
    public function __construct() {

        if ($_SERVER['SERVER_PROTOCOL'] == 'HTTP/1.1') {
            $this->server_protocol = 'http://';
        } else {
            $this->server_protocol = 'https://';
        }
        $this->server_protocol = 'http://';
        //$this->http_host = $this->server_protocol . "" . $_SERVER['HTTP_HOST'];
        $this->template = "default";

        //if(isset($_SESSION ["session_userid"])){
            if ($_SESSION["session_userid"] != "") {

                $dto_user = new Dto_User();
                $user = $dto_user->getById(trim($_SESSION["session_userid"]));
                $this->session_timezonename = trim($_SESSION["session_timezonename"]);
                $this->session_firstname = $user->firstname;
                $this->session_lastname = $user->lastname;
                $this->session_profileid = $user->profileid;
                $this->lang_code = $user->lang;
                
                $dto_account = new Dto_Account();
                $account = $dto_account->getById($user->accountid);
                $this->stylecolor = trim($account->stylecolor);
                if( $this->stylecolor == ""){
                    $this->stylecolor = 'light';
                }
            }
        //}
    }

    public function setTemplate($var) {
        $this->template = $var;
    }

    public function load($module, $action) {
        $lang_code = 'eng';
        $fileLangTemplate = "../lib/view/template/lang/" . $this->lang_code . ".php";
        if (file_exists($fileLangTemplate)) {
            include($fileLangTemplate);
        } else {
            $fileLangTemplate = "../lib/view/template/lang/" . $lang_code . ".php";
            include($fileLangTemplate);
        }

        $filewithOutLangCode = "../lib/view/template/" . $this->template . "/index.php";
        if (file_exists($filewithOutLangCode)) {
            require_once($filewithOutLangCode);
        }
    }

    public function loadContent($module, $action) {
        $lang_code = 'eng';
        $filewithOutLangCode = "../lib/view/" . $module . "/" . $action . ".php";

        $fileLangTemplate = "../lib/view/template/lang/" . $this->lang_code . ".php";

        if (file_exists($fileLangTemplate)) {
            include($fileLangTemplate);
        } else {
            $fileLangTemplate = "../lib/view/template/lang/" . $lang_code . ".php";
            include($fileLangTemplate);
        }


        $fileLang = "../lib/view/" . $module . "/lang/" . $this->lang_code . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        } else {
            
            $fileLang = "../lib/view/" . $module . "/lang/" . $lang_code . ".php";
            if (file_exists($fileLang)) {
                require_once($fileLang);
            }
            
        }

        require_once($filewithOutLangCode);
    }

    public function loadWidget($widget = "", $params = array()) {
        $lang_code = 'eng';
        $filewithOutLangCode = "../lib/view/widget/" . $widget . "/index.php";


        $fileLang = "../lib/view/widget/" . $widget . "/lang/" . $this->lang_code . ".php";
       
        if (file_exists($fileLang)) {
            require_once($fileLang);
           
        } else {
            
            $fileLang = "../lib/view/widget/" . $widget . "/lang/" . $lang_code . ".php";
            if(file_exists($fileLang)){
                require_once($fileLang);
            }
            
        }

        require_once($filewithOutLangCode);
    }

//function
}
