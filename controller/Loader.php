<?php
setlocale(LC_MONETARY,"en_US");  

require_once ("../lib/Config.php");
require_once ("../lib/ConfigReader.php");
require_once ("../lib/ConfigUpdate.php");


require_once ("../lib/ConfigCdr.php");


require_once ("../lib/util/HttpUtil.php");
require_once ("../lib/util/CacheUtil.php");
require_once ("../lib/util/ConsoleUtil.php");


require_once ("../lib/controller/BaseController.php");
require_once ("../lib/controller/ViewController.php");

require_once ("../lib/bean/User.php");
require_once ("../lib/bean/Account.php");
require_once ("../lib/bean/Profile.php");
require_once ("../lib/bean/Taxes.php");
require_once ("../lib/bean/Address.php");

require_once ("../lib/dbcdr/Dto_CdrDb.php");
require_once ("../lib/db/Dto_Db.php");
require_once ("../lib/db/Dto_Auth.php");
require_once ("../lib/db/Dto_User.php");
require_once ("../lib/db/Dto_Account.php");
require_once ("../lib/db/Dto_Profile.php");
require_once ("../lib/db/Dto_Taxes.php");
 
//We create the cache and image folder
//--------------------------------------
$config = new Config();
$config->loadTempFolder();
//--------------------------------------
$module = "auth";
$action = "index";

$requestURI = explode('/', $_SERVER['REQUEST_URI']);
$scriptName = explode('/', $_SERVER['SCRIPT_NAME']);


for ($i = 0; $i < sizeof($scriptName); $i++) {
    if ($requestURI[$i] == $scriptName[$i]) {
        unset($requestURI[$i]);
    }
}

$command = array_values($requestURI);

if (count($command) > 0) {
    $language = $command[0];
    if (count($command) > 1) {
        $module = $command[1];
    }
    if (count($command) > 2) {
        $action = $command[2];
    }
    $paramsIndex = 3;
    if ($language == "") {
        $language = 'eng';
    } else if ($language != "eng" && $language != "esp") {
        $language = "eng";
        
        if (count($command) > 0) {
            $module = $command[0];
        }
        if (count($command) > 1) {
            $action = $command[1];
        }
        $paramsIndex = 2;
    }//else
}//if

$global_userid = "";
$global_profileid = "";
$global_customerid = "";

$session_userid = 0;
$session_profileid = 0;
$session_customerid = 0;


if (isset($_SESSION ["session_userid"])) {
    $session_userid = $_SESSION ["session_userid"];
}


$user = null;

if ($module == "") {
    $module = "auth";
    $action = "index";
} // if



$controllerName = ucwords($module) . "Controller";
$actionName = $action . "Action";
$classFilePath = "../lib/controller/" . $controllerName . ".php";
 
if(!(file_exists($classFilePath))){ 
    //echo 'El archivo no existe: '.$classFilePath; die();
    die();
} 

require_once $classFilePath;

$controllerObject = new $controllerName ();
$controllerObject->lang_code = $language;
$controllerObject->session_user = $user; 

$_globalsubdomain = getSubdomainName();
$temp_accountid = 0;
if($_globalsubdomain != false && $_globalsubdomain !="www"){
    //we need to check what account has that subdomain registered
     $dto_ac = new Dto_Account();
     $temp_accountid = intval($dto_ac->getAccountBySubdomain($_globalsubdomain));
     $controllerObject->session_subdomain = $_globalsubdomain;
     $controllerObject->global_accountid = $temp_accountid;
}

if($temp_accountid == 0){
     $controllerObject->session_subdomain = "www";
     $controllerObject->global_accountid =  10001;
}

//we need to check the params index
$totalParams = count($command);
$aRequest = array();
if ($totalParams > 0) {
    $isKey = true;
    for ($i = $paramsIndex; $i < $totalParams; $i = $i + 2) {
        $value = "";
        $key = $command[$i];
        if (isset($command[$i + 1])) {
            $value = $command[$i + 1];
        }
        //array_push($aRequest, array($key => $value) );
        $aRequest[$key] = $value;
    }//for
}//if
$controllerObject->aRequest = $aRequest;

$controllerObject->$actionName();