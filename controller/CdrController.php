<?php

require_once ("../lib/bean/Cdr.php");
require_once ("../lib/db/Dto_Cdr.php");

require_once ("../lib/bean/AirtimePlan.php");
require_once ("../lib/db/Dto_AirtimePlan.php");

require_once ("../lib/dbcdr/Dto_CdrInvoice.php");
require_once ("../lib/bean/CdrRecord.php");
require_once ("../lib/bean/Account.php");

require_once ("../lib/db/Dto_Account.php");
require_once ("../lib/db/Dto_Inventory.php");


class CdrController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function temporaryfilterAction(){

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-daterangepicker/daterangepicker.css");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-daterangepicker/moment.js");
   	    array_push($view->aJavascript, "assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"); 
        array_push($view->aJavascript, "assets/js/modules/cdr/index.js");

        $view->accountid = $this->session_accountid;
        $view->bodyjquery = "Cdr.init();  ";

        $view->load("cdr", "temporaryfilter");
    }

    function indexAction() {


        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-daterangepicker/daterangepicker.css");


        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");

        array_push($view->aJavascript, "assets/bower_components/bootstrap-daterangepicker/moment.js");
   	    array_push($view->aJavascript, "assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"); 

        array_push($view->aJavascript, "assets/js/modules/cdr/index.js");

        $dto_entity = new Dto_CdrInvoice();
        $view->aRecord = $dto_entity->getCdrsByProvider($this->session_accountid);

        $dto_account = new Dto_Account();
        $view->aAccount = $dto_account->getMyCustomersAndMe($this->session_accountid);
        
        

        $view->accountid = $this->session_accountid;
        $view->bodyjquery = "Cdr.init();  ";
        $view->load("cdr", "index");
    }

    function requestgetindexAction() {

        $_accountid = intval($this->aRequest["accountid"]);
        $_issimcard = intval($this->aRequest["issimcard"]);


        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('start_date', 'start_time', 'mobilenumber', 'callednumber', 'countrydialedname', 'ratingcalltype',  'units', 'totalcharge' );
        $_datatableSetup->_sortCol = " start_date ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Cdr();

        $totalRecords = $dto_entity->getListByAccountidTotal($this->session_accountid,  $datatable->_search);
        $aRecords = $dto_entity->getListByAccountid($this->session_accountid,   $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {
            $image_product = '<img width="45" height="45" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $record->mediaid . '/type/thumb"  >';

            if ($record->issimcard == 1) {
                $detailView = '<a href="/inventory/view/id/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';
            } else {
                $detailView = '<a href="/inventory/view/id/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';
            }
            $output['aaData'][] = array($record->start_date, $record->start_time, $record->mobilenumber, $record->callednumber, $record->countrydialedname, $record->ratingcalltype,  $record->units, $record->totalcharge);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function generateCdrTemporaryAction(){
        $view = new ViewController ();

        $whsearch = $_REQUEST['wheresearch'];
        $wtsearch = $_REQUEST['whatsearch'];

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/bootstrap-daterangepicker/daterangepicker.css");
        
        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/customerconsumption/index.js");


        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/bootstrap-daterangepicker/moment.js");
   	    array_push($view->aJavascript, "assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"); 
        array_push($view->aJavascript, "assets/js/modules/cdr/index.js");

        $view->accountid = $this->session_accountid;
        
        $view->bodyjquery = "Cdr.init(); TableData.init();";
        
        $dto_cdr = new Dto_CdrInvoice();

        $_commandSearch = trim($_REQUEST['simcard']);
        $_daterange = trim($_REQUEST["daterange"]);
        
        if(strlen($_commandSearch)==0){
            $view->load("cdr", "temporaryfilter");
        }

        // 11/07/2017 - 12/20/2017
        $aDate = explode(" - ", $_daterange);
        $Tempbegindate = $aDate[0];
        $Tempenddate = $aDate[1];

        $beginDate = $this->getDateToYYMMDDFormat($Tempbegindate);
        $endDate = $this->getDateToYYMMDDFormat($Tempenddate);

        $view->wtSearch = $wtsearch;
        $view->whSearch = $whsearch;
        $view->commandSearch = $_commandSearch;
        $view->daterange = $_daterange;
        $view->startdate = $beginDate;
        $view->enddate = $endDate;
        
        if($wtsearch=="CDR"){
            //search on cdr
            $consInformation=$dto_cdr->getCdrTemporarySummary($this->session_accountid, $_commandSearch, $beginDate, $endDate, $whsearch);
            
            if($consInformation==false){
                $view->errorMessage = "No se ha encontrado información de consumos";
                $view->load("cdr", "temporaryfilter");
            }
            
            $planId="";
            foreach ($consInformation as $item) {
                $planId = $item['planid'];
                break;
            }

            $dto_airtime = new Dto_AirtimePlan();
            $planDetail = $dto_airtime->getPlanDescTemporary($planId);
            $view->planDetail = $planDetail;
            $view->information=$consInformation;

        }else if ($wtsearch=="NOF"){
            //search on stratos_inmarsat

            if($whsearch=="SCRD"){
                $dto_inventory = new Dto_Inventory();
                $imsi = $dto_inventory->getImsiBySimCard($_commandSearch);
                if($imsi==false){
                    $view->errorMessage = "Sim Card no ha sido encontrado";
                    $view->load("cdr", "temporaryfilter");
                }
                //save imsi
                $_commandSearch = $imsi;
            }
            $consStratosInformation = $dto_cdr->getStratosInmarsatSummary($_commandSearch, $beginDate, $endDate, $whsearch);
            if($consStratosInformation==false){
                $view->errorMessage = "No se ha encontrado información de consumos";
                $view->load("cdr", "temporaryfilter");
            }
            $view->consStratosInformation=$consStratosInformation;
        }

        $view->load("cdr", "temporaryfilter");
        
    }

    function generateExcelReportAction(){
        $_whSearch = trim($_REQUEST['whSearch']);
        $_commandSearch = trim($_REQUEST['commandSearch']);
        $_dateRange = trim($_REQUEST['dateRange']);
        $_wtSearch = trim($_REQUEST['wtSearch']);

        $aDate = explode(" - ", $_dateRange);
        $Tempbegindate = $aDate[0];
        $Tempenddate = $aDate[1];

        $beginDate = $this->getDateToYYMMDDFormat($Tempbegindate);
        $endDate = $this->getDateToYYMMDDFormat($Tempenddate);
        
        $dto_cdr = new Dto_CdrInvoice();
        $report = "<html xmlns:o='urn:schemas-microsoft-com:office:office'
                        xmlns:x='urn:schemas-microsoft-com:office:excel'
                         xmlns='http://www.w3.org/TR/REC-html40'>";

        if($_wtSearch=="CDR"){
            //search on cdr
            $consInformation=$dto_cdr->getCdrTemporarySummary($this->session_accountid, $_commandSearch, $beginDate, $endDate, $_whSearch);
            
            $planId="";
            foreach ($consInformation as $item) {
                $planId = $item['planid'];
                break;
            }

            $dto_airtime = new Dto_AirtimePlan();
            $planDetail = $dto_airtime->getPlanDescTemporary($planId);
            
            $report .= "<table border='1'>";
                $report .= "<thead>";
                    $report .= "<tr>";
                        $report .= "<th>Fecha y hora</th>";
                        $report .= "<th>Sim Card</th>";
                        $report .= "<th>Número</th>";
                        $report .= "<th>Destino</th>";
                        $report .= "<th>Tipo</th>";
                        $report .= "<th>Duración</th>";
                        $report .= "<th>Consumo USD</th>";
                    $report .= "</tr>";
                $report .= "</thead>";
                $report .= "<tbody>";
                    $summary_details = array();
                    foreach($consInformation as $item){
                        $i=0;
                        $index = 0;
                        foreach($summary_details as $detail){
                            $i ++;
                            if($detail == utf8_decode($item['traffictypedesc'])){
                                $index = $i;
                            }
                        }
                            
                        if($index > 0){
                            $sd_time = ($summary_details[$index][0])+$item['qty'];
                            $sd_ammount = ($summary_details[$index][1])+$item['chargeoutofbundle'];
                            $array_replace = array($index => array(0 => $sd_time,  1 => $sd_ammount));
                            $summary_details = array_replace($summary_details, $array_replace);
                        }else{
                            array_push($summary_details, utf8_decode($item['traffictypedesc']));
                            array_push($summary_details, array(0 => $item['qty'], 1 => $item['chargeoutofbundle']));
                        }

                        $report .= "<tr align='center'>";
                            $report .= "<td>".$item['cdrdate']." ".$item['cdrtime']."</td>";
                            $report .= "<td style='mso-number-format:".'"\@"'.";'>".$item['serial']."</td>";
                            $report .= "<td style='mso-number-format:".'"\@"'.";'>".$item['voicenumber']."</td>";
                            $report .= "<td style='mso-number-format:".'"\@"'.";'>".$item['destinationnumber']."</td>";
                            $report .= "<td>".utf8_decode($item['traffictypedesc'])."</td>";
                            $report .= "<td>".$item['qty']." ".$item['qtyunitcode']."</td>";
                            $report .= "<td>".number_format($item['chargeoutofbundle'], 2)."</td>";
                        $report .= "</tr>";
                    }
                $report .= "</tbody>";

                $report .= "<tr>";
                    $report .= "<th colspan='7'><hr /></th>";
                $report .= "</tr>";
                $report .= "<tr>";
                    $report .= "<th colspan='7'>Resumen del consumo</th>";
                $report .= "</tr>";
                $report .= "<tr>";
                    $report .= "<th colspan='3' align='right'>Intervalo de consulta</th>";
                    $report .= "<td colspan='4'>Del ".$beginDate." al ".$endDate."</td>";
                $report .= "</tr>";
                $report .= "<tr>";
                    $report .= "<th colspan='3' align='right'>Plan: ".$planDetail['name']."</th>";
                    $report .= "<td colspan='4'>USD ".number_format($planDetail['price'], 2)."</td>";
                $report .= "</tr>";

                $cant_details = count($summary_details);
                $index=0;
                while($index<$cant_details){
                    $total_ammoumnt += $summary_details[$index+1][1];
                    
                        $report .= "<tr>";
                            $report .= "<th colspan='3' align='right'>Adicional: ".$summary_details[$index]."</th>";
                            $report .= "<td colspan='4'>USD ".number_format($summary_details[$index+1][1], 2)."</td>";
                        $report .= "</tr>";
                    
                    $index = $index +2;
                }

                $report .= "<tr>";
                    $report .= "<th colspan='3' align='right'>Total (sin impuestos)</th>";
                    $report .= "<td colspan='4'>USD ".number_format(($total_ammoumnt+$planDetail['price']) ,2)."</td>";
                $report .= "</tr>";

            $report .= "</table>";

        }else if ($_wtSearch=="NOF"){
            //search on stratos_inmarsat
            if($_whSearch=="SCRD"){
                $dto_inventory = new Dto_Inventory();
                $_commandSearch = $dto_inventory->getImsiBySimCard($_commandSearch); //return imsi     
            }
            
            $consStratosInformation = $dto_cdr->getStratosInmarsatSummary($_commandSearch, $beginDate, $endDate, $_whSearch);
            
            $report .= "<table border='1'>";
                $report .= "<thead>";
                    $report .= "<tr>";
                        $report .= "<th>Fecha y hora</th>";
                        $report .= "<th>IMSI</th>";
                        $report .= "<th>".utf8_decode("Número")."</th>";
                        $report .= "<th>Destino</th>";
                        $report .= "<th>Producto</th>";
                        $report .= "<th>Tipo</th>";
                        $report .= "<th>".utf8_decode("Duración de sesión")."</th>";
                        $report .= "<th>Consumo</th>";
                    $report .= "</tr>";
                $report .= "</thead>";
                $report .= "<tbody>";
                    $consTotal = array();
                    foreach($consStratosInformation as $item){
                        if($item['sessionclosuredate']!="1980-01-01"){
                            $durationText = "";
                            $durationHou = 0;
                            $durationMin = 0;
                            $durationSeg = (strtotime($item['sessionclosuredate']." ".$item['sessionclosuretime']))-(strtotime($item['cdrdate']." ".$item['cdrtime']));
                            while($durationSeg>59){
                                $durationMin ++;
                                $durationSeg = $durationSeg - 60;
                            }
                            while ($durationMin>59){
                                $durationHou ++;
                                $durationMin = $durationMin - 60;
                            }
                            if($durationHou>0){
                                $durationText = $durationHou."h ";
                            }
                            if($durationHou>0 or $durationMin>0){
                                $durationText .= $durationMin."m ";
                            }
                            $durationText .= $durationSeg."s";
                        }else{
                            $durationText = "-";
                        }   
                        $report .= "<tr align='center'>";
                            $report .= "<td>".$item['cdrdate']." ".$item['cdrtime']."</td>";
                            $report .= "<td style='mso-number-format:".'"\@"'.";'>".$item['calsig']."</td>";
                            $report .= "<td style='mso-number-format:".'"\@"'.";'>".$item['numberfrom']."</td>";
                            $report .= "<td style='mso-number-format:".'"\@"'.";'>".$item['destinationnumber']."</td>";
                            $report .= "<td>".$item['product']."</td>";
                            $report .= "<td>".$item['calltype']."</td>";
                            $report .= "<td>".$durationText."</td>";
                            if($item['calltype']=='001') {
                                $increment = $item['numwrdup'];
                                $secCall = $item['numwrdup'];
                                $minCall = 0;
                                $durCall = "";
                                while($secCall > 59){
                                    $minCall ++;
                                    $secCall = $secCall - 60;
                                }
                                if($minCall>0) $durCall = $minCall."m ";
                                $durCall .= $secCall."s";
                                $report .= "<td>".$durCall."</td>";
                            }
                            else if($item['calltype']=='014') {
                                $increment = $item['numwrdup'];
                                $report .= "<td>".$item['numwrdup']." sms</td>";
                            }
                            else if($item['calltype']=='035') {
                                $increment = $item['bitnum'];                                        
                                $temp_bytes = ($item['bitnum'])/8;
                                $mbytes = $temp_bytes / 125000;
                                $report .= "<td>".number_format($mbytes,2)." MB</td>";
                            }
                        $report .= "</tr>";
                        
                        $index = 0;
                        $i = 0;
                        foreach($consTotal as $ceil){
                            $i ++;
                            if($ceil === $item['calltype']){
                                $index = $i;
                            }
                        }

                        if($index>0){
                            $newTotal = $consTotal[$index] + $increment;
                            $arr_replace = array($index => $newTotal);
                            $consTotal = array_replace($consTotal, $arr_replace);
                        }else{
                            array_push($consTotal, $item['calltype']);
                            array_push($consTotal, $increment);
                        }

                    }
                $report .= "</tbody>";
                

                
                $report .= "<tr>";
                    $report .= "<th colspan='8'><hr /></th>";
                $report .= "</tr>";
                $report .= "<tr>";
                    $report .= "<th colspan='8'>Resumen del consumo</th>";
                $report .= "</tr>";
                $report .= "<tr>";
                    $report .= "<th colspan='4' align='right'>Intervalo de consulta</th>";
                    $report .= "<td colspan='4'>Del ".$beginDate." al ".$endDate."</td>";
                $report .= "</tr>";

                $index = 0;
                $cant = count($consTotal);
                while($index<$cant){
                
                $report .= "<tr>";
                    $report .= "<td colspan='4' align='right'>Consumo tipo: (".$consTotal[$index].")</td>";
                    $report .= "<td colspan='4'>";

                            if($consTotal[$index]=="001"){
                                $totSeg = $consTotal[$index+1];
                                $totMin = 0;
                                $totHor = 0;
                                $totTxt = "";
                                while($totSeg>59){
                                    $totMin ++;
                                    $totSeg = $totSeg - 60;
                                }
                                while ($totMin>59){
                                    $totHor ++;
                                    $totMin = $totMin - 60;
                                }
                                if($totHor > 0) $totTxt = $totHor."h ";
                                if($totMin > 0 or $totHor > 0) $totTxt .= $totMin."m ";
                                $totTxt .= $totSeg."s ";
                                $report .= $totTxt;
                            }
                            
                            else if($consTotal[$index]=="014"){ 
                                $report .= $consTotal[$index+1]." sms"; 
                            }

                            else if($consTotal[$index]=="035"){
                                $totbyte = ($consTotal[$index+1] / 8); 
                                $totmb = $totbyte / 125000;
                                $report .= number_format($totmb, 2)." MB"; 
                            }
                        
                    $report .= "</td>";
                $report .= "</td>";

                $index = $index +2;

            }//end while

            $report .= "</table>";

        }
        $report .= "</html>";
        
        $file = "crdreport_".date('y-m-d His').".xls";
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file");
        echo $report;
        exit;
    }

    function generateAction(){

       $dto_airtime = new Dto_AirtimePlan();
       $_aPlans = $dto_airtime->getListAll($this->session_accountid  );
      // echo var_dump($_aPlans); die();

       $_servicetype = trim($_REQUEST["servicetype"]);

       $_daterange = trim($_REQUEST["daterange"]);

       // 11/07/2017 - 12/20/2017
       $aDate = explode(" - ", $_daterange);
       $Tempbegindate = $aDate[0];
       $Tempenddate = $aDate[1];

       $beginDate = $this->getDateToYYMMDDFormat($Tempbegindate).' 00:00:00';
       $endDate = $this->getDateToYYMMDDFormat($Tempenddate).' 23:59:59';
       $accountid = $_REQUEST["accountid"];
       $simcard = $_REQUEST["simcard"];
       $voice = $_REQUEST["voice"];
       $data = $_REQUEST["data"];

       $dto_entity = new Dto_CdrInvoice();
       $aRecord = $dto_entity->getCdrsByAccountid(intval($accountid),$simcard , $voice, $data , $beginDate , $endDate , $_servicetype );
		$aGroup = $dto_entity->getCdrsByAccountidGroup(intval($accountid),$simcard , $voice, $data , $beginDate , $endDate , $_servicetype );
		
		 
		
        $fileLang = "../lib/view/cdr/lang/" . trim($this->session_langcode) . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        }

        $report="";

        $report .="<table border=1>";
        $report .="<thead>";

        $report .="<tr>";
        $report .="<th>$_lang_view_label_startdate</th>";
        $report .="<th>$_lang_view_label_starttime</th>";
        //$report .="<th>$_lang_view_source</th>";
        $report .="<th>$_lang_view_label_mobilenumber</th>";
        $report .="<th>$_lang_view_label_callednumber</th>";
        $report .="<th>$_lang_view_plan</th>";
        $report .="<th>$_lang_view_type</th>";
        $report .="<th>$_lang_view_duration</th>";
        $report .="<th>$_lang_view_additionalcharges</th>";

        $report .="</tr>";

        $report .="</thead>";

        $report .="<tbody>";

        $total = count($aRecord);
        if($total> 0){

            foreach( $aRecord as $record   ){

                $report .="<tr>";
                $report .="<td>".$record->cdrdate."</td>";
                $report .="<td>".$record->cdrtime."</td>";
                //$report .="<td>".$record->serial."</td>";
                $report .="<td>".$record->numberfrom."</td>";
                $report .="<td>".$record->destinationnumber."</td>";

                foreach($_aPlans as $e){
                    if($e->idcrypt == $record->planidcrypt){
                        $report .="<td>".$e->name."</td>";
                    }
                }
                
                if($record->servicetype){
                    $report .="<td>POSTPAID</td>";                    
                }else{
                    $report .="<td>PREPAID</td>";
                }
                
                //$report .="<td>".str_replace(chr(194)," ",$record->traffictypedesc)."</td>";
                $report .="<td>".  $record->traffictypedesc  ."</td>";
                $report .="<td>".$record->qty."   ".$record->qtyunitcode."</td>";
                
                if($record->chargecode=="MONEY"){
                    $report .="<td>".$record->chargeoutofbundle." USD</td>";
                }else{
                    $report .="<td>".$record->chargeoutofbundle." ".$record->chargecode."</td>";
                }
                
                $report .="</tr>";

            }

        }
		
		$totalgroup = count($aGroup);
		
		if($totalgroup > 0 ){
			 $report .="<tr>";
			 $report .="<th> </th>";
			 $report .="<th> </th>";
			 $report .="<th colspan='6'> </th>";
			 $report .="</tr>";
			
			
			 $report .="<tr>";
			 $report .="<th>$_lang_view_label_mobilenumber</th>";
			 $report .="<th>$_lang_view_type</th>";
			 $report .="<th colspan='6'>$_lang_view_additionalcharges</th>";
			 $report .="</tr>";
			$_tempNumber = "";
			for($i=0; $i<$totalgroup; $i++){
				
			 $_temp = 	$aGroup[$i];	
			 
			 if(			$_tempNumber != $_temp->numberfrom ){
				 
				 			 $report .="<tr>";
			 $report .="<th> </th>";
			 $report .="<th> </th>";
			 $report .="<th colspan='6'> </th>";
			 $report .="</tr>";
			
			 }
			 
			 $report .="<tr>";
			 $report .="<td>".$_temp->numberfrom."</td>";
			 $report .="<td>".str_replace(chr(194),"", $_temp->traffictypedesc )."</td>";  
			 $report .="<td colspan='6'>".number_format((float)$_temp->chargeoutofbundle, 2, '.', '')." USD </td>";
			 $report .="</tr>";
				
				$_tempNumber  = $_temp->numberfrom;
				
			}//for
		}
		
		
        $report .="</tbody>";
	
        $report .="</table>";


        $file = "crdreport_".date('y-m-d His').".xls";
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=$file");
        echo $report;



    }

    function getDateToYYMMDDFormat($_oldformat){
        $newformat = "";

        $aDate = explode("/",$_oldformat);
		//07/01/208
		//	2018-07-01
        $newformat = $aDate[2].'-'.$aDate[0].'-'.$aDate[1];

        return $newformat;
    }

}
