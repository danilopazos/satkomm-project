<?php

require_once ("../lib/bean/Currency.php");
require_once ("../lib/bean/CurrencyExchange.php");

require_once ("../lib/db/Dto_Currency.php");

class CurrencyController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }
    
    public function saveexchangeAction(){
        
        $_currencycode = trim($_REQUEST["currencycode"]);
        $_xrate = doubleval($_REQUEST["xrate"]);
        
        $_dto_currency = new Dto_Currency();
        $result = $_dto_currency->addExchange( $this->session_accountid , $_currencycode , $_xrate, $this->session_userid  );
        
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result"=>$result);
        $view->load("template", "json");
        
    }
    
    public function listhistoryAction(){
        
        $_currencycode = trim($this->aRequest["currencycode"]);
 
        
        $_dto_currency = new Dto_Currency();
        $aRecord = $_dto_currency->getListHistory( $this->session_accountid, $_currencycode   );
        
        $view = new ViewController();
        $view->template = "blank";
        $view->data = $aRecord;
        $view->load("template", "json");
        
    }    
    
    
    public function indexAction() {

        $dto_currency = new Dto_Currency();
        $aCurrency = $dto_currency->getList($this->session_accountid);
 

     
        $view = new ViewController ();
       
        $view->aCurrency = $aCurrency;
        $view->accountid = $this->session_accountid;
        
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/bower_components/jquery.moneyformat/index.js");
        array_push($view->aJavascript, "assets/js/modules/currency/index.js");

        
        $view->bodyjquery = "Index.init();  ";
        $view->load("currency", "index");
    }
 
}
