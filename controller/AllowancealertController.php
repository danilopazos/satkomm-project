<?php

require_once ("../lib/bean/TrafficAllowance.php");
require_once ("../lib/bean/AllowanceAlert.php");
require_once ("../lib/bean/InventoryItem.php");
require_once ("../lib/bean/ChargeType.php");
require_once ("../lib/bean/Traffictype.php");
require_once ("../lib/bean/TrafficUnit.php");
require_once ("../lib/bean/NotificationSmsPrice.php");

require_once ("../lib/db/Dto_TrafficAllowance.php");
require_once ("../lib/db/Dto_AllowanceAlert.php");
require_once ("../lib/db/Dto_Inventory.php");
require_once ("../lib/db/Dto_ChargeType.php");
require_once ("../lib/db/Dto_Traffictype.php");
require_once ("../lib/db/Dto_TrafficUnit.php");
require_once ("../lib/db/Dto_NotificationSmsPrice.php");


require_once ("../lib/controller/EmailController.php");
require_once ("../lib/controller/SmsController.php");

require_once ("../lib/util/mailservice/template.php");
require_once ("../lib/helper/DatatableHelper.php");

class AllowancealertController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function indexAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/allowancealert/index.js");

        $view->bodyjquery = "TableData.init();  ";
        $view->load("allowancealert", "index");
    }

    function requestgetlistindexAction() {

        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('alert.registerdatetime', 'a.name', ' alert.iserial', 'alert.subject ', 'alert.emails ', 'alert.mobilenumbers ');
        $_datatableSetup->_sortCol = "a.registerdatetime";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_AllowanceAlert();
        $totalRecords = $dto_entity->getListByAccountidTotal($this->session_accountid, $datatable->_search);
        $aRecords = $dto_entity->getListByAccountid($this->session_accountid, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);

        $totalFiltered = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {
            $_delete = '<a href="#" onclick="fnc_delete(\'' . $record->id . '\')" class="btn btn-xs btn-bricky"  ><i class="fa fa-trash"></i></a>';
            $detailView = '<a href="/allowancealert/register/id/' . $record->id . '/inventoryitemid/' . $record->inventoryitemid . '" class="btn btn-xs btn-default" >' . $record->registerdatetime . '</a>';
            $output['aaData'][] = array($detailView, $record->accountname, $record->plan_name, $record->trafficallowance_name, $record->iserial, $record->subject, $record->emails, $record->mobilenumbers, $_delete);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function deletealertAction() {

        $_deleteid = trim($_REQUEST["delete_id"]);
        $dto = new Dto_AllowanceAlert();
        $result = $dto->deleteById($_deleteid);

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $result);
        $view->load("template", "json");
    }

    function registerAction() {
        $_selectinventoryitem = "";
        $_id = "";

        $_selectinventoryitem = trim($this->aRequest["inventoryitemid"]);


        $dto_entity = new Dto_AllowanceAlert();

        $view = new ViewController();
        $view->id = trim($this->aRequest["id"]);
        $view->session_accountid = $this->session_accountid;
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/allowancealert/register.js");

        $view->bodyjquery = "Register.init();  ";
        $view->entity = $dto_entity->getById($view->id);

        if ($_selectinventoryitem == "" && $_id == "") {
            redirect("allowancealert", "mysimcards");
        } else {

            $dto_inventory = new Dto_Inventory();
            $view->inventoryitemid = $_selectinventoryitem;
            $view->inventoryitem = $dto_inventory->getMyInventoryActivatedByItemId($_selectinventoryitem, $this->session_accountid);


            $dto_trafficAllowance = new Dto_TrafficAllowance();
            $aTrafficAllowance = $dto_trafficAllowance->getTrafficAllowanceByInventoryItemId($_selectinventoryitem);

            $dto_chargeType = new Dto_ChargeType();
            $dto_traffictype = new Dto_Traffictype();
            $dto_trafficunit = new Dto_TrafficUnit();

            $view->aTrafficAllowance = $aTrafficAllowance;

            $view->aChargeType = $dto_chargeType->getAll();
            $view->aTrafficType = $dto_traffictype->getAll();
            $view->aTrafficUnit = $dto_trafficunit->getAll();
            $view->load("allowancealert", "register");
        }
    }

    function smsratesAction() {

        $view = new ViewController();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");


        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/allowancealert/smsrates.js");
        $view->bodyjquery = "TableData.init();  ";
        $dto_notificationsmsprice = new Dto_NotificationSmsPrice();
        $view->aPrices = $dto_notificationsmsprice->getAll();
        $view->load("allowancealert", "smsrates");
    }

    function requestsaveAction() {

        $entity = $this->getRequestRegister();

        $_id = $entity->id;

        $dto_entity = new Dto_AllowanceAlert();
        $entity->registeruserid = $this->session_userid;
        $entity->customeraccountid = $this->session_accountid;

        $result = false;
        if ($_id == "") {

            $entity->id = $dto_entity->generateid();

            $result = $dto_entity->register($entity);
        } else {

            $result = $dto_entity->update($entity);
        }


        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $result);
        $view->load("template", "json");
    }

    function requesttestAction() {

        $dto_allowancealert = new Dto_AllowanceAlert();
        $e = $this->getRequestRegister();

        $result = 0;
        $_resultsms = true;
        $_resultemail = true;

        if ($e->id != "") {

            $alert = $dto_allowancealert->getById($e->id);

            if ($alert->id != "") {


                if ($alert->mobilenumbers != "") {
                    //$_resultsms = $this->sendSmsNow($alert);
                }
                if ($alert->emails != "") {
                    $_resultemail = $this->sendEmail($alert);
                }
            } else {
                $result = -1;
            }
        } else {
            $result = -1;
        }

        if ($_resultsms && $_resultemail) {
            $result = 1;
        }
        

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $result);
        $view->load("template", "json");
    }

    function sendSmsNow($_entity) {

        $fileLang = "../lib/view/allowancealert/lang/" . trim($this->session_langcode) . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        }

        $result = false;

        $_content1 = "";
        $_content2 = "";

        $aNumbers = split(",", $_entity->mobilenumbers);

        $_test_ta = false;
        $_test_ac = false;

        if ($_entity->taexceed01 > 0) {
            $_content1 .= $this->session_accountname . ': ' . $_lang_smsalert_ta_1 .' '.$_entity->subject. '  #' . $_entity->source . ' ' . $_entity->trafficallowance_name . ' ' . $_lang_smsalert_ta_2 . ' ' . $_entity->taexceed01 . '% ';
            $_test_ta = true;
        }

        if ($_entity->acexceed01 > 0) {
            $_content2 .= $this->session_accountname . ': ' . $_lang_smsalert_ac_1 .' '.$_entity->subject. ' #'. $_entity->source . '  ' . $_lang_smsalert_ac_2 . ' USD ' . $_entity->acexceed01 . ' ';
            $_test_ac = true;
        }


        $smsController = new SmsController();

        foreach ($aNumbers as $number) {

            if ($_test_ta) {
                $smsController->send($number, $_content1);
            }

            if ($_test_ac) {
                $smsController->send($number, $_content2);
            }

            $result = true;
            sleep(3);
        }

        return $result;
    }

    function sendEmail($_entity) {

        
        $fileLang = "../lib/view/allowancealert/lang/" . trim($this->session_langcode) . ".php";
        if (file_exists($fileLang)) {
            require_once($fileLang);
        }        
        
        $result = false;
  
        $emailController = new EmailController();

        $aEmails = split(",", $_entity->emails);

        if ($_entity->taexceed01 > 0) {
            $_content1 .=  $_lang_smsalert_ta_1 .' '.$_entity->subject. '  #' . $_entity->source . ' ' . $_entity->trafficallowance_name . ' ' . $_lang_smsalert_ta_2 . ' ' . $_entity->taexceed01 . '% ';
            $_test_ta = true;
        }

        if ($_entity->acexceed01 > 0) {
            $_content2 .=  $_lang_smsalert_ac_1 .' '.$_entity->subject. ' #'. $_entity->source . '  ' . $_lang_smsalert_ac_2 . ' USD ' . $_entity->acexceed01 . ' ';
            $_test_ac = true;
        }
      
        
        foreach ($aEmails as $email) {
            
            $emailsubject = $this->session_accountname . ': ' .$_entity->subject. '  #'. $_entity->source;
            
            
            if($_test_ta){
                $sent = $emailController->send($email, $email, $emailsubject, $_content1, $this->session_accountname);
                if ($sent == 0) {
                    $result = false;
                } else {
                    $result = true;
                }
            }
            
            if($_test_ac){
                $sent = $emailController->send($email, $email, $emailsubject, $_content2, $this->session_accountname);
                if ($sent == 0) {
                    $result = false;
                } else {
                    $result = true;
                }
            }
            
            sleep(3);
        }//foreach


        return $result;
    }

    function getRequestRegister() {
        $dto_inventory = new Dto_Inventory();

        $_id = trim($_REQUEST["id"]);
        $_inventoryitemid = trim($_REQUEST["inventoryitemid"]);

        $inventoryitem = $dto_inventory->getMyInventoryActivatedByItemId($_inventoryitemid, $this->session_accountid);


        $_emails = trim($_REQUEST["emails"]);
        $_subject = trim($_REQUEST["subject"]);
        $_mobilenumbers = trim($_REQUEST["mobilenumbers"]);

        $_taidcrypt = trim($_REQUEST["taidcrypt"]);

        $dto_ta = new Dto_AllowanceAlert();
        if ($_taidcrypt == '') {

            $_planidcrypt = $inventoryitem->planid;
        } else {

            $_planidcrypt = $dto_ta->getPlanIdcryptByTallowance($_taidcrypt);
        }


        $_alertallowance01 = doubleval($_REQUEST["alertallowance01"]);
        $_alertallowance02 = doubleval($_REQUEST["alertallowance02"]);
        $_alertallowance03 = doubleval($_REQUEST["alertallowance03"]);

        $_alertadditionalcharges01 = doubleval($_REQUEST["alertadditionalcharges01"]);
        $_alertadditionalcharges02 = doubleval($_REQUEST["alertadditionalcharges02"]);
        $_alertadditionalcharges03 = doubleval($_REQUEST["alertadditionalcharges03"]);

        $iserial = $inventoryitem->iserial;
        $sku = $inventoryitem->sku;
        $voicenumber = $inventoryitem->voicenumber;
        $datanumber = $inventoryitem->datanumber;

        //first we set up if we need to set allowance

        $entity = new AllowanceAlert();
        $entity->id = $_id;
        $entity->planidcrypt = $_planidcrypt;
        $entity->iserial = $iserial;
        $entity->source = $voicenumber;
        $entity->datanumber = $datanumber;
        $entity->inventoryitemid = $_inventoryitemid;
        $entity->emails = $_emails;
        $entity->subject = $_subject;
        $entity->mobilenumbers = $_mobilenumbers;

        $entity->taidcrypt = $_taidcrypt;

        $entity->taexceed01 = $_alertallowance01;
        $entity->taexceed02 = $_alertallowance02;
        $entity->taexceed03 = $_alertallowance03;

        $entity->acexceed01 = $_alertadditionalcharges01;
        $entity->acexceed02 = $_alertadditionalcharges02;
        $entity->acexceed03 = $_alertadditionalcharges03;


        return $entity;
    }

    function mysimcardsAction() {


        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/responsive.css");

        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/allowancealert/mysimcards.js");

        $view->accountid = $this->session_accountid;
        $view->bodyjquery = "TableData.init();  ";
        $view->accountid = $this->session_accountid;
        $view->load("allowancealert", "mysimcards");
    }

    function requestgetmyinventorysimcardsAction() {

        $_accountid = intval($this->aRequest["accountid"]);
        $_issimcard = intval($this->aRequest["issimcard"]);


        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('id', 'iserial', 'productname', 'pl.servicetype_code', 'pldesc.name', 'voicenumber', 'datanumber', 'operationregistered');
        $_datatableSetup->_sortCol = " datereceived ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Inventory();

        $totalRecords = $dto_entity->getMyInventoryActivatedTotal($_accountid, $_issimcard, $datatable->_search);
        $aRecords = $dto_entity->getMyInventoryActivated($_accountid, $_issimcard, $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);
        $totalFiltered = count($aRecords);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        foreach ($aRecords as $record) {
            $image_product = '<img width="45" height="45" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $record->mediaid . '/type/thumb"  >';


            $detailView = '<a href="/allowancealert/register/inventoryitemid/' . $record->id . '" class="btn btn-xs btn-default" >' . $record->iserial . '</a>';


            $status = "";
            if ($record->operationregistered == "activate") {
                $status = '<a href="/allowancealert/register/inventoryitemid/' . $record->id . '" class="btn btn-xs btn-green">' . $record->operationregistered . '</a>';
            } else if ($record->operationregistered == "suspend") {
                $status = '<a href="/allowancealert/register/inventoryitemid/' . $record->id . '" class="btn btn-xs btn-orange">' . $record->operationregistered . '</a>';
            } else if ($record->operationregistered == "deactivate") {
                $status = '<a href="/allowancealert/register/inventoryitemid/' . $record->id . '" class="btn btn-xs btn-black">' . $record->operationregistered . '</a>';
            }



            $output['aaData'][] = array($image_product, $detailView, $record->productname, $record->servicetype_code, $record->planname, $record->voicenumber, $record->datanumber, $status);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function viewAction() {


        $view = new ViewController();

        array_push($view->aJavascript, "assets/js/modules/allowancealert/view.js");
        $_id = trim($this->aRequest["id"]);

        $dto_allowancealert = new Dto_AllowanceAlert();
        $dto_inventory = new Dto_Inventory();

        $view->entity = $dto_allowancealert->getById($_id);

        $view->inventoryitem = $dto_inventory->getItemByid($view->entity->inventoryitemid);

        //$view->bodyjquery = " View.init();  ";
        $view->load("allowancealert", "view");
    }

}
