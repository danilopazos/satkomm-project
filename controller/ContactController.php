<?php

require_once ("../lib/db/Dto_Contact.php");

class ContactController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }

    function requestregisteruserAction() {
        $operation = "create";
        $contact = $this->getRequestContact();
        $ok = 1;

        $dto_contact = new Dto_Contact();
        
        if ($contact->id != "") {
            $operation = "update";
            $contact->modifiedbyuser = $this->session_userid;
        }else{
            $contact->createdbyuser = $this->session_userid;
        }

        if ($operation == "create" && ( $contact->accountid == "" || $contact->firstname == "" || $contact->lastname == "" || $contact->email == ""  )) {
            $ok = -2;
        } else if ($operation == "update" && ( $contact->accountid == "" || $contact->firstname == "" || $contact->lastname == "" || $contact->email == "" )) {
            $ok = -2;
        }

        if ($ok == 1) {
            if ($operation == "create") {
                $result = $dto_contact->create($contact);
                if (!($result)) {
                    $ok = -3;
                }
            } else {
                $result = $dto_contact->update($contact);
                if (!($result)) {
                    $ok = -3;
                }
            }
        }

        $view = new ViewController();
        if ($ok < 1) {
            $view->data = array("error" => $ok);
        } else {
            $view->data = $user;
        }
        $view->template = "blank";
        $view->load("template", "json");
    }

    function requestgetcontactbyidAction() {
        $_id = trim($this->aRequest["id"]);
        $dto_contact = new Dto_Contact();
        $contact = $dto_contact->getById($_id);

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $contact;
        $view->load("template", "json");
    }

    function getRequestContact() {
 
        $_id = trim($_REQUEST["id"]);
        $_firstname = trim($_REQUEST["firstname"]);
        $_lastname = trim($_REQUEST["lastname"]);
        $_email = trim($_REQUEST["email"]);
        $_accountid = intval($_REQUEST["accountid"]);
        $_leadsource = trim($_REQUEST["leadsource"]);
        $_mobileec = trim($_REQUEST["mobileec"]);
        $_mobile = trim($_REQUEST["mobile"]);
        $_phoneec = trim($_REQUEST["phoneec"]);
        $_phone = trim($_REQUEST["phone"]);
        $_phoneext = trim($_REQUEST["phoneext"]);
        $_fax = trim($_REQUEST["fax"]);
        $_title = trim($_REQUEST["title"]);
        $_salutation = trim($_REQUEST["salutation"]);
        $_department = trim($_REQUEST["department"]);
        $_dateofbirth = trim($_REQUEST["dateofbirth"]);
        $_others = trim($_REQUEST["others"]);
        $_description = trim($_REQUEST["description"]);
        $_ownerid = trim($_REQUEST["ownerid"]);
        
        $_e = new Contact;
        $_e->id = $_id;
        $_e->email = $_email;
        $_e->firstname = $_firstname;
        $_e->lastname = $_lastname;
        $_e->accountid = $_accountid;
        $_e->leadsource = $_leadsource;
        $_e->mobilecc = $_mobileec;
        $_e->mobile = $_mobile;
        $_e->phoneext = $_phoneec;
        $_e->phone = $_phone;
        $_e->phoneext = $_phoneext;
        $_e->fax = $_fax;
        $_e->title = $_title;
        $_e->salutation = $_salutation;
        $_e->department = $_department;
        $_e->dateofbirth =$_dateofbirth;
        $_e->others = $_others;
        $_e->description = $_description;
        $_e->ownerid = $_ownerid;
        
        return $_e;
    }

}
