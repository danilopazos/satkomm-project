<?php

require_once ("../lib/bean/Network.php");
require_once ("../lib/bean/Traffictype.php");
require_once ("../lib/bean/AirtimeProvider.php");
require_once ("../lib/bean/CodeDesc.php");
require_once ("../lib/bean/CdrTrafficType.php");

require_once ("../lib/db/Dto_Traffictype.php");
require_once ("../lib/db/Dto_Network.php");
require_once ("../lib/db/Dto_AirtimeProvider.php");


require_once ("../lib/dbcdr/Dto_CdrTrafficType.php");


require_once ("../lib/helper/DatatableHelper.php");

class TraffictypeController extends BaseController {

    function __construct() {
        isAuth();
        $this->setSession();
    }
 
    
    function assocAction() {
        $view = new ViewController();
        
        
        array_push($view->aJavascript, "assets/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js");
        array_push($view->aCss, "assets/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css");
        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");

        array_push($view->aJavascript, "assets/js/modules/airtimeplan/update.js");
        array_push($view->aJavascript, "assets/js/modules/airtimeplan/trafficsetup.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
 
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/traffictype/assoc.js");        
        
        
        $_code = trim($this->aRequest["code"]);
        $_codename = trim(urldecode($this->aRequest["codename"]));
        $_network = trim($this->aRequest["network"]);
        $_provider = trim($this->aRequest["provider"]);
        $_ttgroup = trim($this->aRequest["ttgroup"]);

        $aService = array();
        $aCalltype = array();
        $aCdkind = array();
        $aOceanRegion = array();
        
        $cdrTrafficType = new Dto_CdrTrafficType();
        
        if ($_network == "INMARSAT" && $_provider == "STRATOS") {

            
            $aService = $cdrTrafficType->getStratosInmarsatService();
            $aCalltype = $cdrTrafficType->getStratosInmarsatCallType();
            $aCdkind = $cdrTrafficType->getStratosInmarsatCdkind();
            $aOceanRegion = $cdrTrafficType->getStratosInmarsatOceanRegion();
            array_push($view->aJavascript, "assets/js/modules/traffictype/assoc_stratos_inmarsat.js");
            
        } else if ($_network == "IRIDIUM" && $_provider == "STRATOS") {
 

            $aCalltype = $cdrTrafficType->getStratosIridiumCallType();
            $aCdkind = $cdrTrafficType->getStratosIridiumDestinationZone();
            $aOceanRegion = $cdrTrafficType->getStratosIridiumOceanRegion();
            array_push($view->aJavascript, "assets/js/modules/traffictype/assoc_stratos_iridium.js");
            
        }else if ($_network == "IRIDIUM" && $_provider == "IRIDIUM") {
            
            /*
            $aCalltype = $cdrTrafficType->getStratosIridiumCallType();
            $aCdkind = $cdrTrafficType->getStratosIridiumDestinationZone();
            $aOceanRegion = $cdrTrafficType->getStratosIridiumOceanRegion();
             */
            $aCalltype  = false;
            $aCdkind = false;
            $aOceanRegion = false;
            
            array_push($view->aJavascript, "assets/js/modules/traffictype/assoc_iridium_iridium.js");
        }
         
        
        $view->aList = $cdrTrafficType->getCdrTrafficTypeByCode($_code, $_provider, $_network, $_ttgroup )  ;
 

        $view->_code = $_code;
        $view->_codename = $_codename;
        $view->_network = $_network;
        $view->_provider = $_provider;

        $view->_aService = $aService;
        $view->_aCalltype = $aCalltype;
        $view->_aCdkind = $aCdkind;
        $view->_aOceanregion = $aOceanRegion;
        $view->_ttgroup = $_ttgroup;
        $view->accountid = $this->session_accountid;
        $view->bodyjquery = "  Register.init();  ";

        $view->load("traffictype", "assoc");
    }

    function assocdeleteAction() {

        $_code = trim($this->aRequest["code"]);
        $_network = trim($this->aRequest["network"]);
        $_provider = trim($this->aRequest["provider"]);
        $_id  = trim($this->aRequest["id"]);
        
        $dto_e = new Dto_CdrTrafficType();
        $_result = $dto_e->deleteTrafficTypeCode($_id);

        $_ok = 1;
        if (!$_result) {
            $_ok = 2;
        }
        
        redirect("traffictype", "assoc", array("code"=>$_code, "network"=>$_network, "provider"=>$_provider) );
        
        
    }

    function assocregisterAction() {

        $_code = trim($_REQUEST["code"]);
        $_provider = trim($_REQUEST["provider"]);
        $_network = trim($_REQUEST["network"]);
        $_servicecode = trim($_REQUEST["servicecode"]);
        $_calltypecode = trim($_REQUEST["calltypecode"]);
        $_cdkind = $_REQUEST["cdkind"];
        $_oceanregioncode = trim($_REQUEST["oceanregioncode"]);
        $_destinationzone = trim($_REQUEST["destinationzone"]);
        $_description = trim($_REQUEST["description"]);
        $_ttgroup = trim($_REQUEST["ttgroup"]);

        $_result = false;
        if ($_code != "" && $_provider != "" && $_network != "") {
            
            //echo var_dump($_cdkind); die();
            if($_provider == "STRATOS" && $_network =="INMARSAT"  ){
                 
                $_totalcdkind = count($_cdkind);
                
                for($i=0; $i<$_totalcdkind; $i++){
                    
                    $_tempcdkind = $_cdkind[$i];
                    
                    $e = new CdrTrafficType();
                    $e->code = $_code;
                    $e->provider = $_provider;
                    $e->network = $_network;
                    $e->servicecode = $_servicecode;
                    $e->calltypecode = $_calltypecode;
                    $e->cdkind = $_tempcdkind;
                    $e->oceanregioncode = $_oceanregioncode;
                    $e->destinationzone = $_destinationzone;
                    $e->description = $_description;
                    $e->ttgroup = $_ttgroup;

                    $dto_e = new Dto_CdrTrafficType();
                    $_result = $dto_e->insertTrafficTypeCode($e);

                }
            }else if($_provider == "STRATOS" && $_network =="IRIDIUM"  ){
                
                    $e = new CdrTrafficType();
                    $e->code = $_code;
                    $e->provider = $_provider;
                    $e->network = $_network;
                    $e->servicecode = $_servicecode;
                    $e->calltypecode = $_calltypecode;
                    $e->cdkind = $_cdkind;
                    $e->oceanregioncode = $_oceanregioncode;
                    $e->destinationzone = $_destinationzone;
                    $e->description = $_description;
                    $e->ttgroup = $_ttgroup;
                    
                    $dto_e = new Dto_CdrTrafficType();
                    $_result = $dto_e->insertTrafficTypeCode($e);
                
                
            }else if($_provider == "IRIDIUM" && $_network =="IRIDIUM"  ){
                
                    $e = new CdrTrafficType();
                    $e->code = $_code;
                    $e->provider = $_provider;
                    $e->network = $_network;
                    $e->servicecode = $_servicecode;
                    $e->calltypecode = "";
                    $e->cdkind = "";
                    $e->oceanregioncode = "";
                    $e->destinationzone = "";
                    $e->description = $_description;
                    $e->ttgroup = $_ttgroup;
                    
                    $dto_e = new Dto_CdrTrafficType();
                    $_result = $dto_e->insertTrafficTypeCode($e);
                
            }

        }

        $_ok = 1;
        if (!$_result) {
            $_ok = 2;
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $_ok);
        $view->load("template", "json");
    }

    function indexAction() {

        $view = new ViewController ();

        array_push($view->aCss, "assets/bower_components/select2/dist/css/select2.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css");
        array_push($view->aCss, "assets/bower_components/datatables/media/css/responsive.css");

        array_push($view->aJavascript, "assets/bower_components/bootbox.js/bootbox.js");
        array_push($view->aJavascript, "assets/bower_components/jquery-mockjax/dist/jquery.mockjax.min.js");
        array_push($view->aJavascript, "assets/bower_components/select2/dist/js/select2.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/jquery.dataTables.min.js");
        array_push($view->aJavascript, "assets/bower_components/datatables/media/js/dataTables.bootstrap.js");
        array_push($view->aJavascript, "assets/js/modules/templates/jquery.validate.js");
        array_push($view->aJavascript, "assets/js/modules/traffictype/index.js");
        
        $dto_traffictype = new Dto_TrafficType();
        $dto_network = new Dto_Network();
        $view->aNetwork = $dto_network->getAll();
        
        $view->aTrafficTypeGroup = $dto_traffictype->getTtgroupList();
        
        $dto_airtimeProvider = new Dto_AirtimeProvider();

        $view->aAirtimeProvider = $dto_airtimeProvider->getAll();

        $view->accountid = $this->session_accountid;
        $view->bodyjquery = "TableData.init(); Register.init();  ";
        $view->load("traffictype", "index");
    }

    function requestgetindexAction() {

        $_accountid = intval($this->aRequest["accountid"]);
        $_issimcard = intval($this->aRequest["issimcard"]);

        $_ttgroup = trim($_REQUEST["ttgroup"]);
        
        
        
        $_datatableSetup = new Datatable();
        $_datatableSetup->_aColumns = array('code', 'networkcode', 'airtimeprovider', 'description', 'code');
        $_datatableSetup->_sortCol = " networkcode ";
        $_datatableSetup->_sortDir = "desc";

        $datatableHelper = new DatatableHelper();
        $datatable = $datatableHelper->processRequest($_datatableSetup);

        $dto_entity = new Dto_Traffictype();

        $totalRecords = $dto_entity->getListTotal(  $_ttgroup , $datatable->_search);
        $aRecords = $dto_entity->getList($_ttgroup , $datatable->_search, $datatable->_sortCol, $datatable->_sortDir, $datatable->_limit1, $datatable->_limit2);

        $output = array(
            "sEcho" => intval($_REQUEST['sEcho']), "iTotalRecords" => $totalRecords, "iTotalDisplayRecords" => $totalRecords, "aaData" => array()
        );

        //$account->billaddress->country,  $account->billaddress->city
        foreach ($aRecords as $record) {
            $linkDeleted = '<a href="#" onclick="fnc_deleteRecord( \'' . $record->id . '\' );" class="btn btn-xs btn-bricky"  ><i class="fa fa-trash-o"></i></a>';
            $linkUpdated = '<a href="#" onclick="fnc_updateRecord( \'' . $record->id . '\' );" class="btn btn-xs btn-primary"  ><i class="fa fa-pencil"></i></a>';

            
            $linkAssoc = '<a target="_blank" href="assoc/code/' . $record->code . '/codename/'. urlencode($record->description).'/network/' . $record->networkcode . '/provider/' . $record->airtimeprovider . '/ttgroup/'.$record->ttgroup.'">' . $record->code . '</a>';

            $output['aaData'][] = array($linkAssoc, $record->ttgroup ,  $record->networkcode,    $record->airtimeprovider, $record->description, $linkDeleted . ' &nbsp; ' . $linkUpdated);
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = $output;
        $view->load("template", "json");
    }

    function requestregisterAction() {
        $e = $this->getRequestRegister();
        $dto_entity = new Dto_Traffictype();
        $_op = "create";
        if ($e->id != "") {
            $_op = "update";
        } else {
            $e->id = $dto_entity->generateId();
        }



        $ok = 1;

        if ($e->code != "" && $e->airtimeprovider != "" && $e->description != "") {
            $tempentity = $dto_entity->getByCode($e);
            if ($_op == "create") {
                if ($tempentity) {
                    $ok = -1;
                }
            } else {
                if ($tempentity && $tempentity->id != $e->id) {
                    $ok = -1;
                }
            }

            $tempentity = $dto_entity->getByDescription($e);
            if ($_op == "create") {
                if ($tempentity) {
                    $ok = -4;
                }
            } else {
                if ($tempentity && $tempentity->id != $e->id) {
                    $ok = -4;
                }
            }
        } else {
            $ok = -2;
        }

        $result = true;
        if ($ok == 1) {
            if ($_op == "create") {
                $result = $dto_entity->register($e);
            } else {
                $result = $dto_entity->update($e);
            }
        }

        if (!$result) {
            $ok = -3;
        }

        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $ok);
        $view->load("template", "json");
    }

    function requestdeleteAction() {
        $result = false;
        $dto_entity = new Dto_Traffictype();
        $e = new TrafficType();
        $e->id = trim($_REQUEST["id"]);
        $result = $dto_entity->delete($e);
        $ok = 1;
        if (!$result) {
            $ok = -1;
        }
        $view = new ViewController();
        $view->template = "blank";
        $view->data = array("result" => $ok);
        $view->load("template", "json");
    }

    function requestgetbyidAction() {
        $dto_entity = new Dto_Traffictype();
        $e = new TrafficType();
        $e->id = trim($this->aRequest["id"]);
        $e = $dto_entity->getById($e);
        $view = new ViewController();
        $view->template = "blank";
        $view->data = $e;
        $view->load("template", "json");
    }

    function getRequestRegister() {
        $_id = trim($_REQUEST["id"]);
        $_code = trim($_REQUEST["code"]);
        $_ttgroup = trim($_REQUEST["ttgroup"]);
        $_description = trim($_REQUEST["description"]);
        $_networkcode = trim($_REQUEST["networkcode"]);
        $_airtimeprovider = trim($_REQUEST["airtimeprovider"]);
        $e = new Traffictype();
        $e->id = $_id;
        $e->description = $_description;
        $e->code = $_code;
        $e->ttgroup = $_ttgroup;
        $e->networkcode = $_networkcode;
        $e->airtimeprovider = $_airtimeprovider;
        return $e;
    }

}