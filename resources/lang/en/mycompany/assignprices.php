<?php

return [
    'title_page' => 'List of assigned prices',
    'subtitle_page' => 'Prices assigned to :',

    'table_typeplan' => 'Type of plan',
    'table_plan' => 'Plan',
    'table_band' => 'Band',
    'table_notes' => 'Anotations',
    'table_costo' => 'Cost',
    'table_assigned' => 'Price list',
    'table_review' => 'Check',
    'btn_review' => 'Check prices',

    'text_found' => ':number prices found',
    'text_notfound' => '0 coincidences...',
    'btn_back' => 'Return',

    'modal_btn_savebutton' => 'Save price list',
    'modal_btn_cancel' => 'Cancel',
    'modal_title' => 'Price list : ',
    'modal_subtitle_cost' => 'Service cost : ',
    'modal_table_description' => 'Description',
    'modal_table_price' => 'USD sales value',
    'modal_addprice' => 'Add new price',
    'modal_moreconnections' => 'more',
    'modal_from' => 'From ',
    'modal_to' => ' to ',
    'modal_connections' => ' connections',
    'modal_months' => ' months',
    'modal_withoutassign' => 'Not assigned',
    'modal_typenote' => 'Type a description...',
    'modal_typeprice' => 'Type a sale value...',
    'modal_saved' => 'Prices saved!',
];