<?php

return [

    'title_page' => 'Bank accounts',

    'btn_addaccount' => 'Register a new bank account',

    'subtitle_accountslist' => 'List of registered bank accounts',

    'title_calltoaction' => 'No bank accounts have been registered',
    'text_calltoaction' => 'Comienza ahora y registra una cuenta',
    'btn_calltoaction' => 'Start now and register an account',

    'table_image' => 'Logo',
    'table_bank' => 'Bank',
    'table_currency' => 'Currency',
    'table_account' => 'Account number',
    'table_cci' => 'CCI account number',
    'table_edit' => 'Edit',

    'modal_title' => 'Register a bank account',
    'modal_subtitle_bank' => 'Select a bank',
    'modal_subtitle_currency' => 'Account currency',
    'modal_subtitle_number' => 'Account number',
    'modal_subtitle_cci' => 'CCI account number',

    'modal_btnsave' => 'Register a bank account',
    'modal_btncancel' => 'Cancel',

    'modal_confirmalert' => 'The account has been successfully registered!',

];
