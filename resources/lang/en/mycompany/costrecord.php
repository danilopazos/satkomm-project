<?php

return [
    'title_page' => 'Cost Detail Record',
    'subtitle_page' => 'List of registered details',

    'text_noresults' => 'No costs have been registered!',
    'text_calltoaction' => 'Start now and record a cost',
    'btn_newrecord' => 'Register a new cost',

    'table_detail' => 'Cost detail',
    'table_date' => 'Registration date',

    'modal_save' => 'Save detail',
    'modal_cancel' => 'Cancel',
    'modal_title' => 'Detail Record',
    'modal_text' => 'Enter a detail',
    'modal_saved' => 'Registered detail!',

];