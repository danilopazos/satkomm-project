<?php

return [

    'title_page' => 'Document Renewal',

    'subtitle_page' => 'Information on the deadline for documents',

    'text_term' => 'Term in months for renewal',
    'text_yes' => 'Yes',
    'text_required' => 'Required',
    'text_months' => 'months',

    'btn_update' => 'Update due date',

    'table_description' => 'Document Description',
    'table_forcustomer' => 'Document attached to the Client\'s record?',
    'table_fororder' => 'Document attached to the Service Order?',
    'table_due' => 'Does expiration apply?',

    'modal_saveok' => 'Record saved!',
    'modal_minterm' => 'The minimum period is 1 month',
    'modal_needtype' => 'You must enter a minimum term',

];
