<?php

return [
    'title_page' => 'Register a new customer',
    'title_pageedit' => 'Edit a customer record',

    'subtitle_information' => 'Company information',

    'text_typedocument' => 'Type document',
    'text_numberdocument' => 'Number document',
    'text_companyname' => 'Company name',
    'text_brandname' => 'Brand name',
    'text_contactmail' => 'Email contact',
    'text_contactphone' => 'Telephone contact',
    'text_website' => 'Website',
    'text_currency' => 'Billing currency',

    'subtitle_billing' => 'Billing address',
    'text_fastaddress' => 'Fast description',
    'text_address' => 'Complete address',
    'text_country' => 'Country',
    'text_department' => 'Department',
    'text_province' => 'Province',
    'text_district' => 'District',
    'text_postalcode' => 'Postal code',

    'btn_savecustomer' => 'Save customer',
    'btn_updatecustomer' => 'Update record',

    'select_textselect' => 'Choose - -',

    'modal_saved' => 'Information saved!',
    'modal_updated' => 'Information updated!',
    'modal_complete' => 'Complete all the information',

    'text_creditdatys' => 'Credit for issuing receipts (in days)',

];
