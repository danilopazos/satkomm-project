<?php

return [
    'title_page' => 'Type exchange',

    'btn_newcurrency' => 'Register a new currency',
    'tabletitle_list' => 'List currencies',

    'table_country' => 'Country',
    'table_currency' => 'Currency',
    'table_exchange' => 'Exchange',
    'table_history' => 'History',
    'table_modify' => 'Edit',

    'text_noresults' => 'No currencies have been assigned to the company!',
    'text_calltoaction' => 'Start now and register a currency!',

    'modal_title' => 'Register a new currency',
    'modal_selectCur' => 'Select a currency',
    'modal_exchange' => 'Type your amount exchange',

    'modal_btnsave' => 'Register a currency',
    'modal_btncancel' => 'Cancel',

    'modal_success' => 'The currency has been created!',

];
