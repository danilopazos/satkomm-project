<?php

return [
    'title_page' => 'Register a new address',
    'title_page_edit' => 'Edit address',

    'subtitle_information' => 'Company information',

    'text_typedocument' => 'Type documents',
    'text_numberdocument' => 'Document number',
    'text_companyname' => 'Company name',
    'text_brandname' => 'Brand name',
    'text_contactmail' => 'Email contact',
    'text_contactphone' => 'Telephone contact',
    'text_website' => 'Website',
    'text_currency' => 'Billing currency',

    'subtitle_billing' => 'Billing address',
    'subtitle_infoaddress' => 'Address information',
    'text_company' => 'Select a company',
    'text_companyName' => 'Company name',
    'text_fastaddress' => 'Fast description',
    'text_address' => 'Complete address',
    'text_country' => 'Country',
    'text_department' => 'Department',
    'text_province' => 'Province',
    'text_district' => 'District',
    'text_typeaddress' => 'Type address',
    'text_postalcode' => 'Postal code',
    'text_coordinates' => 'Coordinates',

    'btn_updateaddress' => 'Update address',
    'btn_saveaddress' => 'Register a new address',
    'btn_savecustomer' => 'Save customer',
    'btn_updatecustomer' => 'Update customer record',

    'select_textselect' => 'Choose - -',

    'modal_complete' => 'Complete all the information!',
    'modal_saved' => 'Address saved!',
    'modal_updated' => 'Address updated!',

];
