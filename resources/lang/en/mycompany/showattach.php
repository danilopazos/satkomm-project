<?php

return [
    'title_page' => 'Attach documents',

    'subtitle_page' => 'Choose a document to attach',

    'save_ok' => 'Document saved!',

    'text_typeattach' => 'Type document',
    'text_notes' => 'Anotations',
    'text_attach' => 'Select the document to attach',
    'text_expedition' => 'Date expedition',
    'text_expiration' => 'Date expiration',

    'option_choose' => 'Choose - -',

    'btn_attach' => 'Save document',
    'btn_return' => 'View record',
    'btn_returnorder' => 'Return to the order service',

    'modal_message' => 'Complete the information!!',

];
