<?php

return [
    'title_page' => 'Register a new provider',

    'subtitle_information' => 'Provider information',

    'text_typedocument' => 'Type document',
    'text_numberdocument' => 'Numer document',
    'text_companyname' => 'Company name',
    'text_brandname' => 'Brand name',
    'text_contactmail' => 'Email contact',
    'text_contactphone' => 'Telephone contact',
    'text_website' => 'Website',
    'text_currency' => 'Default currency',

    'subtitle_billing' => 'Provider address',
    'text_fastaddress' => 'Fast description',
    'text_address' => 'Complete address',
    'text_country' => 'Country',
    'text_department' => 'Department',
    'text_province' => 'Province',
    'text_district' => 'District',
    'text_postalcode' => 'Postal code',

    'btn_savecustomer' => 'Save provider',
    'btn_updatecustomer' => 'Update record',

    'select_textselect' => 'Choose - -',

];
