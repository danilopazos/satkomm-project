<?php

return [
    'title_page' => 'Company profile',

    'subtitle_information' => 'Company information',
    'text_companyname' => 'Company name',
    'text_brandname' => 'Brand name',
    'text_contactmail' => 'Email contact',
    'text_contactphone' => 'Telephone contact',
    'text_website' => 'Web page',
    'text_currency' => 'Default currency',

    'subtitle_billing' => 'Bill address',
    'text_fastaddress' => 'Fast description',
    'text_address' => 'Complete address',
    'text_country' => 'Country',
    'text_department' => 'Department',
    'text_province' => 'Province',
    'text_district' => 'District',
    'text_postalcode' => 'Postal code',

    'table_typeaddress' => 'Type address',
    'table_description' => 'Description',
    'table_status' => 'Status',
    'table_address' => 'Address',
    'table_location' => 'Location',
    'table_postal' => 'Postal code',
    'table_edit' => 'Edit',
    'href_editcustomer' => 'Edit record',

    'btn_addaddress' => 'Register a new address',
    'btn_editcompany' => 'Edit record',

    'subtitle_address' => 'Registered Addresses',

    'subtitle_contracts' => 'Assigned contracts',
    'href_download' => 'Download',
    'modal_nofiles' => 'Choose a document!',
    'text_nocontracts' => 'There are no documents attached to this customer!',
    'file_choose' => 'Attach a contract here',
    'href_upload' => 'Upload contract signed',
    'saved_attach' => 'Contract saved!',

    'href_uploadhere' => 'Attach documents',

    'btn_moreoptions' => 'More options',
        'option_upload_companylogo' => 'Change company logo',
        'option_upload_explainimageinvoice' => 'Change the explanatory image for invoice',
        'option_edit_information' => 'Update company information',
        'option_edit_documentsexpiration' => 'Time for document renewal',
        'option_aditionaltasks' => 'Aditional tasks',
        'option_viewinvoices' => 'View list invoices',
        'option_recordcost' => 'Costs records',
        'option_listprices' => 'Plans and prices assigned',

    'text_typerecord' => 'Type record',

    'text_creditdays' => 'Credit for issuing receipts',
    'text_days' => ':number days',

];
