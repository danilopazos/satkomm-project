<?php

return [
    'title_page' => 'My customers',

    'btn_newcustomer' => 'Register a new customer',
    'btn_assigncustomertoprovider' => 'Assign a provider as a customer',

    'tabletitle_list' => 'Customers list',

    'table_code' => 'Code',
    'table_customer' => 'Customer',
    'table_location' => 'Location',
    'table_currency' => 'Billing currency',
    'table_edit' => 'Edit',
    'table_information' => 'View',
    'table_assign' => 'Assign as a provider',

    'message_saveok' => 'Record saved!',

    'text_noresults' => 'No customers have registered!',
    'text_calltoaction' => 'Register a customer now',

];
