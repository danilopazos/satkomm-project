<?php

return [
    'title_page' => 'My providers',

    'btn_newcustomer' => 'Register a new provider',
    'btn_assigncustomertoprovider' => 'Assign a customer as a provider',

    'tabletitle_list' => 'Providers list',

    'table_code' => 'Code',
    'table_customer' => 'Customer',
    'table_provider' => 'Provider',
    'table_location' => 'Location',
    'table_currency' => 'Currency',
    'table_edit' => 'Edit',
    'table_information' => 'View',

    'message_saveok' => 'Record saved!',

    'table_assign' => 'Assign as a customer',

    'text_noresults' => 'No suppliers have registered!',
    'text_calltoaction' => 'Register a provider now',

];
