<?php

return [

    'title_page' => 'My notes',
    'title_page_new' => 'Create a note',

    'btn_create' => 'Create a note',
    'btn_save' => 'Save note',
    'btn_delete' => 'Delete note',
    'btn_returnnotes' => 'Return to my notes',

    'text_title' => 'Type your title here',

    'text_body' => 'Type your notes here',
    'text_saved' => 'Note saved!',
    'text_updated' => 'Saved : ',

    'text_remember' => 'Remember : All the notes notes you create here are personal. And for security, they are encrypted.',

    'title_hello' => 'Hello ',
    'title_calltoaction' => ', you haven\'t notes saved',
    'text_calltoaction' => 'Start now!',
    'btn_calltoaction' => 'Create a note here',

    'modal_noblank' => 'It\'s not possible to create a blank note!',
    'modal_saved' => 'Note saved!',
    'modal_behere' => 'Stay here',
    'modal_checkall' => 'See all my notes',
    'modal_askquit' => 'Are you sure     to delete this note?',
    'modal_yesdelete' => 'Yes, delete',
    'modal_cancel' => 'Cancel',

];
