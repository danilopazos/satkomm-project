<?php

return [
    'title_page' => 'Astelekomm',
    'title_login' => 'Login to your account',
    'input_user' => 'User',
    'input_pass' => 'Password',
    'check_remember' => 'Remember my account',
    'btn_login' => 'Login now',
    'link_forgotpass' => 'Forgot my password',
    'fail_login' => '( ! ) Incorrect username and/or password!',
];
