<?php

return [
    'title_page' => 'Users with access',

    'btn_newcustomer' => 'Register a new user',

    'tabletitle_list' => 'Users list',

    'table_email' => 'Email',
    'table_name' => 'Names',
    'table_company' => 'Company',
    'table_jobtitle' => 'Jobtitle',
    'table_usertype' => 'Type user',
    'table_disbled' => 'Block',
    'table_changepass' => 'New password',

    'message_saveok' => 'User registered!',

];
