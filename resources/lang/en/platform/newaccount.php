<?php

return [

    'title' => 'Create an account',

    'text_name' => 'Names',
    'text_lastname' => 'Lastnames',
    'text_telephone' => 'Telephone',
    'text_cellphone' => 'Cellphone',
    'text_company' => 'Company',
    'text_jobtitle' => 'Jobtitle',
    'text_user' => 'Email',
    'text_password' => 'Password',
    'text_usertype' => 'Type account',
    'text_language' => 'Account language',

    'btn_create' => 'Create an account',

    'option_select' => 'Choose - -',
];
