<?php

return [

    'text_hello' => 'Hello ',
    
    'menu_myprofile' => 'Profile',
    'menu_mypassword' => 'New password',
    'menu_logout' => 'Logout',

    'alert_title' => 'Alert center',
    'alert_showmore' => 'View all alerts',

    'leftmenu_godashboard' => 'Go Dashboard',
    'leftmenu_titlemenu' => 'Menu',


    'leftmenu_platform' => 'Platform',
        'leftmenu_platformaccounts' => 'Users with access',
        'leftmenu_platformcompanies' => 'Companies with access',
        'leftmenu_sendalerts' => 'Send alerts',

    'leftmenu_productivity' => 'Personal space',
        'leftmenu_notes' => 'Notes',
        'leftmenu_calendar' => 'Calendar', //not yet


    'leftmenu_mycompany' => 'My company',
        'leftmenu_companyprofile' => 'Company profile',
        'leftmenu_bankaccount' => 'Banks accounts',
        'leftmenu_exchange' => 'Type exchange',
        'leftmenu_customers' => 'My customers',
        'leftmenu_providers' => 'My providers',



    'leftmenu_vsat' => 'VSAT',
        'leftmenu_inventory' => 'Inventory',
        'leftmenu_inventoryvsat' => 'Vsat inventory',
        'leftmenu_inventoryview' => 'View inventory',
        'leftmenu_inventorynew' => 'Record inventory',
        'leftmenu_plans' => 'Plans',
        'leftmenu_consultvsat' => 'Order services',
        'leftmenu_periodsvsat' => 'Invoice periods',
        
        'leftmenu_viewinvoices' => 'View all payments',
        'leftmenu_viewconsumptions' => 'Filter consumptions',

    'title_dashboard' => 'Dashboard',
        'subtitle_earnings' => 'Total earnings on ',
        'subtitle_orderscomplete' => 'Billed services on ',
        'subtitle_totalactive' => 'Activations on ',
            'text_totalactive' => ' commercials ',
        'subtitle_ordersdue' => 'Orders to expire ',
        'subtitle_historial' => 'Revenue history up to ',
        'subtitle_inventory' => 'Vsat antennas inventory summary',


    'title_myprofile' => 'My profile',
        'text_name' => 'Names',
        'text_lastname' => 'Lastnames',
        'text_email' => 'Email',
        'text_jobtitle' => 'Job title',
        'text_cellphone' => 'Cellphone',
        'text_telephone' => 'Telephone',
        'text_language' => 'Account language',
        'btn_saveprofile' => 'Update my profile',


    'title_changepassword' => 'Change my password',
        'text_username' => 'Username',
        'text_password' => 'Actual password',
        'text_newpass' => 'New password',
        'text_reppass' => 'Confirm your new password',
        'btn_savepass' => 'Update my password',

    'btn_report' => 'Revenue for month : ',

    'text_search' => 'Search',

    'modal_ups' => 'Oops! Something went wrong!',
    'modal_btnok' => 'Understood!',
    'modal_complete' => 'Complete all the information',
    'modal_saved' => 'The information has been updated',
    'modal_passsaved' => 'Password updated!',
    'modal_passincorrect' => 'Password is incorrect!',
    'modal_passdifferent' => 'Passwords entered are different!',

    'text_listorders' => 'Orders pending verification',
    'text_moreorders' => 'Show list complete',

];
