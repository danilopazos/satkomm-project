<?php

return [
    'title_page' => 'Restricted access',
    'title' => 'You do not have permission for this operation',
    'btn_gohome' => 'Back home',
];