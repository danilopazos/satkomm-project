<?php

return [
    'title_404' => 'Error 404 | Astel',
    'text_404' => 'Looks like you are lost in space...',
    'btn' => 'Go Home'
];