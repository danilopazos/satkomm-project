<?php

return [
    'title_page' => 'Edit consumer receipt',
    'title' => 'Editing form',

    'text_customer' => 'Customer',
    'text_period' => 'Period',
    'text_serial' => 'Serial number',
    'text_correlative' => 'Correlative',
    'text_datecreate' => 'Date create',
    'text_datedue' => 'Date due',
    'text_currency' => 'Invoice currency',
    'text_exchange' => 'Type exchange (form USD to new currency)',
    'text_totalpay' => 'Total to pay',
    'text_cost' => 'Service cost',
    'text_notes' => 'Anotations',
        'btn_save' => 'Update information',

    'title_updates' => 'History updates',
    'text_updates' => 'Information that was modified in the operation',
    'table_by' => 'updated by :name :lastname',

    'modal_updated' => 'Information saved!',
    'modal_complete' => 'Complete all the information!',
];