<?php

return [
    'title_page' => 'Register a new item',
    'title_page_edit' => 'Edit items',
    'title_pagestart' => 'Insert items',

    'subtitle_information' => 'Entry group',
        'text_entrygroup' => 'Entry group',
            'option_no' => 'No assign',
            'option_yes' => 'Assign existing group',
            'option_create' => 'Creat group and assign',

    'subtitle_productinfo' => 'Product information',
        'text_typeinventory' => 'Inventory',
        'text_typeproduct' => 'Type product',
        'text_brandproduct' => 'Product brand',
        'text_modelproduct' => 'Product model',
        'text_quantity' => 'Products quantity',

    'select_default' => 'Choose - -',

    'texto_assignedgroup' => 'Grup assign',
    'text_createGroupEntry' => 'Group name',
    'select_createGroupEntry' => 'Select a group',

    'text_anotations' => 'Anotations',
    'text_status' => 'Inventory status',

    'btn_start' => 'Start with registration',
    'btn_saveitems' => 'Register Product Entry',
    'btn_update' => 'Update register',

    'subtitle_itemdetails' => 'Products detail',

    'table_number' => 'Number',
    'table_description' => 'Description',
    'table_anotation' => 'Anotations',
    'table_status' => 'Status',
    'table_group' => 'Entry group',
    'table_info' => 'Aditional information',

    'modal_save' => 'Register updated!',

];
