<?php

return [
    'title_page' => 'VSAT plans',

    'btn_newplan' => 'Register a new vsat plan',

    'tabletitle_list' => 'Vsat plans list',

    'table_plan' => 'Plan details',
    'table_type' => 'Type',
    'table_check' => 'View',
    'bandtype' => 'Band',
    'table_edit' => 'Edit',

    'message_saveok' => 'Plan saved!',

    'text_noresults' => 'No vsat plans have been created',
    'text_calltoaction' => 'Start now and register a new plan',

    'table_actives' => 'Active services',
    'text_actives' => ':number active services',
    'text_nopostpaid' => 'It\'s not postpaid',

];
