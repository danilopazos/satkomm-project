<?php

return [
    'title_page' => 'Vsat inventory',

    'btn_newplan' => 'Register a new item',

    'tabletitle_list' => 'Vsat inventory list',

    'table_plan' => 'Plan details',
    'table_type' => 'Type',
    'table_check' => 'View',
    'bandtype' => 'Band',

    'table_code' => 'Code',
    'table_product' => 'Product',
    'table_brand' => 'Brand',
    'table_model' => 'Model',
    'table_group' => 'Group',
    'table_series' => 'Serials',
    'table_anotation' => 'Anotations',
    'table_status' => 'Status',
    'table_edit' => 'Edit',

    'message_saveok' => 'Plan saved!',

    'text_noresults' => 'No items have been registered to the inventory!',
    'text_calltoaction' => 'Start now and register an item',


];
