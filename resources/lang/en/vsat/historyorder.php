<?php

return [
    
    'title_page' => 'Movement History',
    'text_orderservice' => 'Order service : ',

    'table_dates' => 'Processed',
    'table_description' => 'Description',
    'table_notes' => 'Notes',
    'table_user' => 'User',

    'table_by' => 'by :name :lastname',

    'text_noresults' => 'No results!',
    'text_calltoaction' => 'The service order has no movement history.',
    'btn_newplan' => 'Return to service order',

    'text_notes' => 'Notes : :notes',
    'href_download' => 'Download attachment',

];
