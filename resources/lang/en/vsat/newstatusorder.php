<?php

return [
    'title_page' => 'Modify the order status',

    'subtitle_page' => 'Register a new status',

    'text_order' => 'Order code',
    'text_date' => 'Date register',
    'text_status' => 'Choose a status',
        'select_option' => 'Choose - -',

    'text_anotations' => 'Anotations',

    'btn_save' => 'Save new status',

    'modal_complete' => 'Complete the information!',
    'modal_saved' => 'Status saved!',

    'modal_complete_anotation' => 'To proceed with this action you must enter an entry!',
    'modal_sure' => 'Yes, I\'m sure',
    'modal_cancel' => 'Cancel',
    'modal_title_sure' => 'Are you sure to perform this action?',
    'modal_text_1' => 'This service order has',
    'modal_text_2' => ' current contract',
    'modal_left' => '',
    'modal_days' => ' day(s)',
    'to_due' => ' for the expiration of the contract.',

    'alert_fail' => 'Could not perform this action!',
    'text_statusnow' => 'Actual state : ',
    'text_attach' => 'Select an attachment',

];
