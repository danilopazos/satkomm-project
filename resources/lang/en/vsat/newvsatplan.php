<?php

return [
    'title_page' => 'Register a new VSAT plan',

    'subtitle_information' => 'Plan information',
    'subtitle_prices' => 'Prices information',

    'text_typeband' => 'Band type',
    'text_typeplan' => 'Plan type',
    'text_planname' => 'Plan name',
    'text_plannotes' => 'Anotations',
    'text_donwload' => 'Download',
    'text_upload' => 'Upload',
    'text_anytime' => 'All time',
    'text_early' => 'From 8am to 6pm',
    'text_total' => 'Total',
    'text_cir' => 'CIR',
    'text_cost' => 'USD service cost',
    'text_commode' => 'USD commode cost',

    'text_typespeed' => 'Speed',
    'text_typeunits' => 'Units',

    'table_activations' => 'Activations',
    'table_permanence' => 'Permanence',
    'table_notes' => 'Anotations',
    'table_code' => 'Correlative',
    'table_usdprice' => 'USD price',

    'table_to' => 'to',
    'table_months' => 'months',
    'table_more' => 'more',

    'link_addprice' => 'Add price',

    'btn_saveplan' => 'Save VSAT plan',

    'subtitle_aboutcosts' => 'Cost information',
    'modal_update' => 'Record updated!',
    'modal_saved' => 'Plan saved!',
    'modal_complete' => 'Complete all the information!',

];
