<?php

return [
    'title_page' => 'Add discount',
    'btn_returnorder' => 'Return to order service',
    
    'text_ordservice' => 'Order service',
    'text_anotations' => 'Anotations',
    'text_discounts' => 'USD discount',
    'text_dateStart' => 'Date start',
    'text_dateEnd' => 'Date end',
    'text_attach' => 'File attach',

    'btn_save' => 'Save discount',
    'btn_add' => 'Add discount',

    'alert_saved' => 'The discount information has been saved!',

    'modal_complete' => 'Complete all the information!',

    'text_historial' => 'Discounts list',
    'table_description' => 'Description',
    'table_period' => 'Period',
    'table_amount' => 'Amount',
    'href_download' => 'Download attach',
    'table_total' => 'Total discounts',
];