<?php

return [
    'title_page' => 'Orders service',

    'btn_neworden' => 'Register a new order service',

    'tabletitle_list' => 'Orders service list',

    'table_ordenservice' => 'Order service',
    'table_typeservice' => 'Service',
    'table_sancode' => 'SAN code',
    'table_customer' => 'Customer',
    'table_activedate' => 'Activation',
    'table_plan' => 'Plan',
    'table_price' => 'Total',
    'table_status' => 'Status',
    'table_edit' => 'Edit',
    'table_check' => 'View',
    'table_cancel' => 'Date cancel',
    'table_created' => 'Created',

    'message_saveok' => 'Plan saved!',

    'text_noresults' => 'No service orders have been created!',
    'text_calltoaction' => 'Start now and register an order!',


];
