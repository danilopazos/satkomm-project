<?php

return [
    'title_page' => 'Invoice periods',

    'btn_newinvoice' => 'Check consumptions',
    'btn_download' => 'Download',

    'tabletitle_list' => 'Invoices list',

    'table_code' => 'Invoice',
    'table_period' => 'Period',
    'table_customer' => 'Customer',
    'table_sancode' => 'SAN code',
    'table_orderattach' => 'Orders attach',
    'table_ammount' => 'Total',
    'table_document' => 'Document',
    'table_numdocument' => 'Document number',
    'table_dateissue' => 'Date issue',
    'table_review' => 'Download',
    'btn_edit' => 'Edit',

    'message_saveok' => 'Plan saved!',

    'text_noresults' => 'No consumption invoices have been generated!',
    'text_calltoaction' => 'Check consumption before generating invoices!',


];
