<?php

return [

    'title_page' => 'Operating costs of the service order:',
    'title_pagerecord' => 'Assign an operating cost',
    'btn_back' => 'Return to service order',

    'subtitle_page' => 'List of operating costs assigned to the service order',
    'text_noresults' => 'No cost was assigned to this service order!',
    'text_calltoaction' => 'Start now and assign a cost',
    
    'btn_viewoptions' => 'See options',
    'btn_startnow' => 'Assign an operating cost',
    'alert_saved' => 'The operating cost has just been registered!',

    'table_date' => 'Date of issue',
    'table_provider' => 'Provider',
    'table_typedoc' => 'Type document',
    'table_numberdoc' => 'Number document',
    'table_description' => 'Descriptions',
    'table_entry' => 'Entries',
    'table_exit' => 'Exists',
    'table_document' => 'Document',

    'text_createdby' => 'Created by : ',
    'href_download' => 'Download',
    'text_totals' => 'Totlas : ',
    'text_profit' => 'Profitability of the service : ',
    
    'text_aboutservice' => 'About order service',
    'text_orderservice' => 'Order service',
    'text_typerecord' => 'Type record',
    'text_serviceprovider' => 'Service provider',
    'text_typecost' => 'Type cost',
    'text_anotations' => 'Anotations',
    'text_aboutdocument' => 'About document',
    'text_currency' => 'Document currency',
    'text_costwithout' => 'Cost without taxes',
    'file_select' => 'Select a support document',

    'select_choose' => 'Choose - -',

    'modal_complete' => 'Complete the fields marked in red!',

];
