<?php

return [
    'page_header' => 'Plan information',
    'page_title' => 'Plan : ',

    'btn_options' => 'View options',
        'options_edit' => 'Edit plan',
        'option_newplan' => 'Create a new plan',
        'option_neworder' => 'Create a new order',
        'option_back' => 'Return to list plans',

    'subtitle_aboutplan' => 'Plan information',
        'text_codeplan' => 'Plan code',
        'text_band' => 'Band type',
        'text_plan' => 'Plan type',
        'text_nameplan' => 'Plan name',
        'text_notes' => 'Anotations',

    'subtitle_aboutcosts' => 'Costs associated with the plan',
        'text_cost' => 'Plan cost',
        'text_commodate' => 'Commode cost',
        'text_totals' => 'Totla cost',

    'subtitle_founds' => ':number service orders associated with this plan were found',
        'table_customer' => 'Customer',
        'table_order' => 'Order service',
        'table_datestart' => 'Activation date',
        'table_sellvalue' => 'Monthly sell value',
        'table_check' => 'Check order',

        'text_noresults' => 'No orders associated with this plan were found',
    'text_information' => 'When there are orders with this same plan, they will be displayed here',

];