<?php

return [

    'title' => ' | Consumption invoice',

    'text_number' => 'Customer support : ',
    'text_mail' => 'E-mail : ',
    'text_by' => 'by ',

    'text_customerinfo' => 'CUSTOMER INFORMATION',

    //from Peru - Si la empresa es de Peru, esto no se puede cambiar
    'customer_taxnumber' => 'RUC',
    'customer_name' => 'Razón social',
    'text_taxes' => 'I.G.V. ',

    'customer_address' => 'Dirección',
    'customer_location' => 'Ubicación',
    'customer_currency' => 'Moneda',
    'customer_exchange' => ' - Exch. : ',

    'text_invoiceinfo' => 'INVOICE',
    'invoice_number' => 'Invoice Nº',
    'invoice_datecreate' => 'Issue',
    'invoice_datedue' => 'Expiration',
    'invoice_period' => 'Period',

    'text_invoicesummary' => 'INVOICE SUMMARY',
    'text_description' => 'Description',
    'text_import' => 'Amount in ',

    'text_for' => ' for ',
    'text_day' => 'Prorated for :number day',
    'text_days' => 'Prorated for :number days',
    'text_period' => 'Period : from ',
    'text_periodto' => ' to ',
    'text_totalcharge' => 'Subtotal amount',
    'text_finaltotal' => 'Total to pay',
    'text_create' => 'Date issue',
    'text_due' => 'Date expire',

    'title_concepts' => 'CONCEPTOS FACTURABLES',

    'subtitle_concepts' => 'Activación del servicio',
    'detail_concepts' => 'Cargo facturado al cliente por única vez cuando se activa el servicio del teléfono.',

    'subtitle_charges' => 'Activación del servicio',
    'detail_charges' => 'Cargo mensual facturado al cliente por mantenimiento de cada teléfono. Cargo promocional calculado desde la fecha de inicio del servicio hasta el siguiente cierre de facturación.',

    'subtitle_plan' => 'Plan Mensual',
    'detail_plan' => 'Cargo mensual factura al cliente por plan contratado de cada teléfono. Cargo promocional calculado desde la fecha de inicio del servicio hasta el siguiente cierre de facturación.',

    'subtitle_callsad' => 'Cargo por Llamadas Adicionales',
    'detail_callsad' => 'Cargo  por  tráfico  de  voz,  datos  y  mensaje  de  texto  que  no  se encuentren comprendidos dentro del plan.',

    'subtitle_otherad' => 'Otros Cargos',

    'litle_titleotherad' => 'Cargo  de  reconexión:  ',
    'detail_otherad' => 'Cargo  facturado  si  el  cliente  cancela  un  recibo después de habérsele cortado por deuda.',

    'litle_titlerent' => 'Renta Fraccionaria por Cambio de Plan: ',
    'detail_rent' => 'Cargo facturando si durante el  ciclo  de  facturación  el  cliente  cambió  de  plan  tarifario  mensual distinto al que ya tenía.',

    'litle_titleservices' => 'Servicio  de  Telefonía:  ',
    'detail_servicescall' => 'Cargo  factura  correspondiente  al  tráfico  y  a otros  conceptos  mensuales  generados  por  un  determinado  contrato suscrito al cliente.',

    'title_notes' => 'NOTAS AL CLIENTE',
    'detail_notes' => 'AS  TELECOM  le  informa  que  en  caso  de  uso  indebido  o  fraudulento  del  servicio  contratado,  ASTELECOM  podrá  resolver  el contrato,  suspender  y/o  cortar  definitivamente  el  servicio  de  una  o  de  todas  las  líneas  contratadas,  así  como  rechazar  contratar con  usted  en  el  futuro.  Documento  válido  para  efectos  tributarios  según  Resolución  SUNAT  N°  007-99/SUNAT.  Documento  no afecto al Régimen de Retención del I.G.V. según Resolución N° 037-2002/SUNAT.',

    'title_pays' => 'LUGARES DE PAGO',
    'detail_pays' => 'Av. 2 de Mayo 516 Dpto. 201 Miraflores, Lima - Perú',

    'title_address' => 'MEDIOS DE PAGO',

    'title_cards' => 'TARJETAS DE CRÉDITO ACEPTADAS',

    'text_bank' => 'CUENTA EN : ',
    'text_bankcci' => 'CCI : ',

    'text_discount' => 'Discount applied to the service',

];
