<?php

return [
    'title_page' => 'Acceso restringido',
    'title' => 'No tienes permiso para esta operación',
    'btn_gohome' => 'Volver al home',
];