<?php

return [
    'title_404' => 'Error 404 | Astel',
    'text_404' => 'Parece que estás perdido en el espacio...',
    'btn' => 'Volver'
];