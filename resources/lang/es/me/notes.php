<?php

return [

    'title_page' => 'Mis Notas',
    'title_page_new' => 'Crear una nota',

    'btn_create' => 'Crear una nota',
    'btn_save' => 'Guardar nota',
    'btn_delete' => 'Eliminar nota',
    'btn_returnnotes' => 'Regresar a mis notas',

    'text_title' => 'Ingresa un título',

    'text_body' => 'Ingresa tus apuntes',
    'text_saved' => 'La nota ha sido guardada!',
    'text_updated' => 'Guardado : ',

    'text_remember' => 'Recuerda : Todas las notas que crees aquí, son personales. Y por seguridad, están encriptadas.',

    'title_hello' => 'Hola ',
    'title_calltoaction' => ', no tienes notas guardadas',
    'text_calltoaction' => '¡Comienza ahora!',
    'btn_calltoaction' => 'Crea una nota aquí',

    'modal_noblank' => 'No es posible crear una nota en blanco!',
    'modal_saved' => 'La nota ha sido guardada!',
    'modal_behere' => 'Quedarme aquí',
    'modal_checkall' => 'Ver todas mis notas',
    'modal_askquit' => '¿Estás seguro que deseas eliminar esta nota?',
    'modal_yesdelete' => 'Si, eliminar',
    'modal_cancel' => 'Cancelar',

];
