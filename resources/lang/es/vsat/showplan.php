<?php

return [
    'page_header' => 'Información de un plan',
    'page_title' => 'Plan : ',

    'btn_options' => 'Ver opciones',
        'options_edit' => 'Editar este plan',
        'option_newplan' => 'Crear un nuevo plan',
        'option_neworder' => 'Crear orden de servicio',
        'option_back' => 'Volver a la lista de planes',

    'subtitle_aboutplan' => 'Información del plan',
        'text_codeplan' => 'Código del plan',
        'text_band' => 'Tipo de banda',
        'text_plan' => 'Tipo del plan',
        'text_nameplan' => 'Nombre del plan',
        'text_notes' => 'Anotaciones',

    'subtitle_aboutcosts' => 'Costos asociados al plan',
        'text_cost' => 'Costo del plan',
        'text_commodate' => 'Costo por comodato',
        'text_totals' => 'Costo total',

    'subtitle_founds' => 'Se encontró :number órdenes de servicio asociados a este plan',
        'table_customer' => 'Cliente',
        'table_order' => 'Orden de servicio',
        'table_datestart' => 'Fecha de activación',
        'table_sellvalue' => 'Valor mensual de venta',
        'table_check' => 'Revisar orden',

    'text_noresults' => 'No se encontró órdenes asociadas a este plan',
    'text_information' => 'Cuando existan órdenes con este mismo plan, se mostrarán aquí',

];