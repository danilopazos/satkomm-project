<?php

return [
    'title_page' => 'Registra un nuevo item',
    'title_page_edit' => 'Editar item',
    'title_pagestart' => 'Ingresa los items',

    'subtitle_information' => 'Grupo de ingreso',
        'text_entrygroup' => 'Grupo de ingreso',
            'option_no' => 'No asignar',
            'option_yes' => 'Asignar grupo existente',
            'option_create' => 'Crear grupo y asignar',

    'subtitle_productinfo' => 'Información del producto',
        'text_typeinventory' => 'Inventario de registro',
        'text_typeproduct' => 'Tipo de producto',
        'text_brandproduct' => 'Marca del producto',
        'text_modelproduct' => 'Model de producto',
        'text_quantity' => 'Cantidad de productos',

    'select_default' => 'Seleccione - -',

    'texto_assignedgroup' => 'Grupo asignado',
    'text_createGroupEntry' => 'Nombre del grupo',
    'select_createGroupEntry' => 'Elige grupo',

    'text_anotations' => 'Anotaciones',
    'text_status' => 'Estado en inventario',

    'btn_start' => 'Comenzar con el registro',
    'btn_saveitems' => 'Registrar ingreso de productos',
    'btn_update' => 'Actualizar el registro',

    'subtitle_itemdetails' => 'Detalle de los productos',

    'table_number' => 'Nro',
    'table_description' => 'Descripción',
    'table_anotation' => 'Anotaciones',
    'table_status' => 'Estado',
    'table_group' => 'Grupo de ingreso',
    'table_info' => 'Información adicional',

    'modal_save' => 'Registro actualizado!',

];
