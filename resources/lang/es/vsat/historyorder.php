<?php

return [
    
    'title_page' => 'Historico de movimientos',
    'text_orderservice' => 'Orden de servicio : ',

    'table_dates' => 'Procesado',
    'table_description' => 'Descripción',
    'table_notes' => 'Anotaciones de la operacón',
    'table_user' => 'Realizado',

    'table_by' => 'por :name :lastname',

    'text_noresults' => 'No hay resultados!',
    'text_calltoaction' => 'La orden de servicio no tiene historial de movimientos.',
    'btn_newplan' => 'Volver a la orden de servicio',

    'text_notes' => 'Anotaciones : :notes',
    'href_download' => 'Descargar archivo adjunto',

];
