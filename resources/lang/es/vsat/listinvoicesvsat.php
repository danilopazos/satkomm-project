<?php

return [
    'title_page' => 'Recibos de consumo',

    'btn_newinvoice' => 'Consultar consumos',
    'btn_download' => 'Descargar',

    'tabletitle_list' => 'Lista de recibos emitidos',

    'table_code' => 'Recibo',
    'table_period' => 'Periodo',
    'table_customer' => 'Cliente',
    'table_sancode' => 'Código SAN',
    'table_orderattach' => 'Ordenes adjuntas',
    'table_ammount' => 'Total',
    'table_document' => 'Documento fiscal',
    'table_numdocument' => 'Nro. Documento',
    'table_dateissue' => 'Fecha de emisión',
    'table_review' => 'Decargar recibo',
    'btn_edit' => 'Editar',

    'message_saveok' => 'Plan registrado correctamente!',

    'text_noresults' => 'No se han generado recibos de consumo!',
    'text_calltoaction' => 'Consulta los consumos antes de generar recibos!',


];
