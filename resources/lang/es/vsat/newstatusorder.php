<?php

return [
    'title_page' => 'Modificar el estado de la orden',

    'subtitle_page' => 'Registra el estado',

    'text_order' => 'Código de orden',
    'text_date' => 'Fecha de registro',
    'text_status' => 'Elige un estado',
        'select_option' => 'Seleccione - -',

    'text_anotations' => 'Anotaciones',

    'btn_save' => 'Registrar el estado',

    'modal_complete' => 'Completa los datos faltantes!',
    'modal_saved' => 'El estado ha sido registrado!',

    'modal_complete_anotation' => 'Para proceder con esta acción debes ingresar una anotación!',
    'modal_sure' => 'Si, estoy seguro',
    'modal_cancel' => 'Cancelar',
    'modal_title_sure' => '¿Estás seguro de realizar esta acción?',
    'modal_text_1' => 'Esta orden de servicio tiene ',
    'modal_text_2' => ' contrato vigente',
    'modal_left' => 'Falta',
    'modal_days' => ' día(s)',
    'to_due' => ' for the expiration of the contract.',

    'alert_fail' => 'No se pudo realizar esta acción!',
    'text_statusnow' => 'Estado actual : ',
    'text_attach' => 'Selecciona un adjunto',

];
