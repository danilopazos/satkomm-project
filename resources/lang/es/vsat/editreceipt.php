<?php

return [
    'title_page' => 'Editar recibo de consumo',
    'title' => 'Formulario de edición',

    'text_customer' => 'Cliente',
    'text_period' => 'Periodo',
    'text_serial' => 'Número de serie',
    'text_correlative' => 'Correlativo',
    'text_datecreate' => 'Fecha de emisión',
    'text_datedue' => 'Fecha de vencimiento',
    'text_currency' => 'Moneda para el recibo',
    'text_exchange' => 'Tipo de cambio (de USD a la nueva Moneda)',
    'text_totalpay' => 'Total a pagar',
    'text_cost' => 'Costo del servicio',
    'text_notes' => 'Anotaciones',
        'btn_save' => 'Actualizar recibo de consumo',

    'title_updates' => 'Actualizaciones en el tiempo',
    'text_updates' => 'Información que se modificó en la operación',
    'table_by' => 'actualizado por :name :lastname',

    'modal_updated' => 'La información del recibo ha sido actualizada!',
    'modal_complete' => 'Completa la información marcada en rojo!',
];