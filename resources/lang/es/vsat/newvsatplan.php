<?php

return [
    'title_page' => 'Registro de plan VSAT',

    'subtitle_information' => 'Información del plan',
    'subtitle_prices' => 'Información de precios',

    'text_typeband' => 'Tipo de banda',
    'text_typeplan' => 'Tipo de plan',
    'text_planname' => 'Nombre del plan',
    'text_plannotes' => 'Anotación del plan',
    'text_donwload' => 'Descarga',
    'text_upload' => 'Subida',
    'text_anytime' => 'Todo el tiempo',
    'text_early' => 'De 8am a 6pm',
    'text_total' => 'Total',
    'text_cir' => 'CIR',
    'text_cost' => 'Costo por el servicio USD',
    'text_commode' => 'Costo por comodato USD',

    'text_typespeed' => 'Veloc.',
    'text_typeunits' => 'Und.',

    'table_activations' => 'Activaciones',
    'table_permanence' => 'Permanencia',
    'table_notes' => 'Anotación',
    'table_code' => 'Correlativo',
    'table_usdprice' => 'Precio USD',

    'table_to' => 'a',
    'table_months' => 'meses',
    'table_more' => 'más',

    'link_addprice' => 'Nuevo precio',

    'btn_saveplan' => 'Registrar plan VSAT',

    'subtitle_aboutcosts' => 'Información sobre los costos',
    'modal_update' => 'Registro actualizado!',
    'modal_saved' => 'El plan acaba de ser registrado!',
    'modal_complete' => 'Completa los campos marcados en rojo!',


];
