<?php

return [
    'title_page' => 'Planes vsat',

    'btn_newplan' => 'Registrar un plan vsat',

    'tabletitle_list' => 'Lista de planes registrados',

    'table_plan' => 'Detalle del Plan',
    'table_type' => 'Tipo',
    'table_check' => 'Revisar',
    'bandtype' => 'Banda',
    'table_edit' => 'Editar',

    'message_saveok' => 'Plan registrado correctamente!',

    'text_noresults' => 'No se han creado planes vsat',
    'text_calltoaction' => 'Comienza ahora y registra un nuevo plan',

    'table_actives' => 'Ordenes asociadas',
    'text_actives' => ':number orden(es) asociada(s)',
    'text_nopostpaid' => 'No es postpago',

];
