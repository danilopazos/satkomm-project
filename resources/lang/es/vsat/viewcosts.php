<?php

return [

    'title_page' => 'Costos operativos de la orden de servicio : ',
    'title_pagerecord' => 'Asignar un costo operativo',
    'btn_back' => 'Volver a la orden de servicio',

    'subtitle_page' => 'Lista de costos operativos asignados a la orden de servicio',
    'text_noresults' => 'No se asignó ningún costo a esta orden de servicio!',
    'text_calltoaction' => 'Comienza ahora y asigna un costo',
    
    'btn_viewoptions' => 'Ver opciones',
    'btn_startnow' => 'Asignar un costo operativo',
    'alert_saved' => 'El costo operativo acaba de ser registrado!',

    'table_date' => 'Fecha de emisión',
    'table_provider' => 'Proveedor',
    'table_typedoc' => 'Tipo de documento',
    'table_numberdoc' => 'Número de documento',
    'table_description' => 'Descripción',
    'table_entry' => 'Ingresos',
    'table_exit' => 'Salidas',
    'table_document' => 'Documento',

    'text_createdby' => 'Creado por : ',
    'href_download' => 'Descargar',
    'text_totals' => 'Totales : ',
    'text_profit' => 'Rentabilidad del servicio : ',
    
    'text_aboutservice' => 'Sobre el servicio',
    'text_orderservice' => 'Orden de servicio',
    'text_typerecord' => 'Tipo de registro',
    'text_serviceprovider' => 'Proveedor de servicio',
    'text_typecost' => 'Tipo de costo',
    'text_anotations' => 'Anotaciones',
    'text_aboutdocument' => 'Sobre el documento',
    'text_currency' => 'Moneda del documento',
    'text_costwithout' => 'Costo sin impuestos',
    'file_select' => 'Selecciona un documento de sustento',

    'select_choose' => 'Seleccione - -',

    'modal_complete' => 'Completa los campos marcados en rojo!',

];
