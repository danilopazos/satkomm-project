<?php

return [
    'title_page' => 'Agregar descuento',
    'btn_returnorder' => 'Volver a la orden de servicio',
    
    'text_ordservice' => 'Orden de servicio',
    'text_anotations' => 'Anotaciones',
    'text_discounts' => 'Descuento en USD',
    'text_dateStart' => 'Fecha de inicio',
    'text_dateEnd' => 'Fecha de finalización',
    'text_attach' => 'Adjunta un sustento',

    'btn_save' => 'Registrar descuento',
    'btn_add' => 'Agregar descuento',

    'alert_saved' => 'Los datos han sido guardados correctamente!',

    'modal_complete' => 'Completa la información marcada en rojo!',

    'text_historial' => 'Historial de descuentos',
    'table_description' => 'Descripción',
    'table_period' => 'Periodo de validez',
    'table_amount' => 'Monto',
    'href_download' => 'Descargar sustento',
    'table_total' => 'Descuentos totales',
];