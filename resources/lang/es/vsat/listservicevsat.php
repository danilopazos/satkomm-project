<?php

return [
    'title_page' => 'Ordenes de servicio',

    'btn_neworden' => 'Registrar orden de servicio',

    'tabletitle_list' => 'Lista de ordenes de servicio',

    'table_ordenservice' => 'Orden de servicio',
    'table_typeservice' => 'Servicio',
    'table_sancode' => 'Código SAN',
    'table_customer' => 'Cliente',
    'table_activedate' => 'Activación',
    'table_plan' => 'Plan',
    'table_price' => 'Total',
    'table_status' => 'Estado',
    'table_edit' => 'Editar',
    'table_check' => 'Revisar',
    'table_cancel' => 'Cancelado',
    'table_created' => 'Creado',

    'message_saveok' => 'Plan registrado correctamente!',

    'text_noresults' => 'No se han creado ordenes de servicio!',
    'text_calltoaction' => 'Comienza ahora y registra una orden!',

];
