<?php

return [
    'title_page' => 'Inventario Vsat',

    'btn_newplan' => 'Registrar nuevo item',

    'tabletitle_list' => 'Lista de inventario Vsat',

    'table_plan' => 'Detalle del Plan',
    'table_type' => 'Tipo',
    'table_check' => 'Revisar',
    'bandtype' => 'Banda',

    'table_code' => 'Código',
    'table_product' => 'Producto',
    'table_brand' => 'Marca',
    'table_model' => 'Modelo',
    'table_group' => 'Grupo',
    'table_series' => 'Series',
    'table_anotation' => 'Anotación',
    'table_status' => 'Estado',
    'table_edit' => 'Editar',

    'message_saveok' => 'Plan registrado correctamente!',

    'text_noresults' => 'No se han registrado items al inventario!',
    'text_calltoaction' => 'Comienza ahora y registra un item',


];
