<?php

return [
    'title_page' => 'Astelekomm',
    'title_login' => 'Ingresa a tu cuenta',
    'input_user' => 'Usuario',
    'input_pass' => 'Contraseña',
    'check_remember' => 'Recordar mi cuenta',
    'btn_login' => 'Ingresar ahora',
    'link_forgotpass' => 'Olvidé mi contraseña',
    'fail_login' => '( ! ) Usuario y/o contraseña incorrecta!',
];
