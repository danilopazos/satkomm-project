<?php

return [
    'title_page' => 'Usuarios con acceso',

    'btn_newcustomer' => 'Registrar un nuevo usuario',

    'tabletitle_list' => 'Lista usuarios',

    'table_email' => 'Correo',
    'table_name' => 'Nombres',
    'table_company' => 'Empresa',
    'table_jobtitle' => 'Cargo',
    'table_usertype' => 'Tipo usuario',
    'table_disbled' => 'Bloquear',
    'table_changepass' => 'Resetear clave',

    'message_saveok' => 'Usuario creado correctamente!',

];
