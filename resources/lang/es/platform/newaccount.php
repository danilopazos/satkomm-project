<?php

return [

    'title' => 'Crear una cuenta',

    'text_name' => 'Nombres',
    'text_lastname' => 'Apellidos',
    'text_telephone' => 'Telefono',
    'text_cellphone' => 'Celular',
    'text_company' => 'Empresa',
    'text_jobtitle' => 'Puesto',
    'text_user' => 'Correo electrónico',
    'text_password' => 'Contraseña',
    'text_usertype' => 'Tipo de cuenta',
    'text_language' => 'Idioma de la cuenta',

    'btn_create' => 'Crear cuenta',

    'option_select' => 'Seleccione - -',
];
