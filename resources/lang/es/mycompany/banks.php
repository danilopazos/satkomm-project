<?php

return [

    'title_page' => 'Cuentas bancarias',

    'btn_addaccount' => 'Registrar una nueva cuenta',

    'subtitle_accountslist' => 'Lista de cuentas bancarias registradas',

    'title_calltoaction' => 'No se han registrado cuentas bancarias',
    'text_calltoaction' => 'Comienza ahora y registra una cuenta',
    'btn_calltoaction' => 'Registrar una cuenta bancaria',

    'table_image' => 'Logo',
    'table_bank' => 'Banco',
    'table_currency' => 'Moneda',
    'table_account' => 'Número de cuenta',
    'table_cci' => 'Número interbancario',
    'table_edit' => 'Editar',

    'modal_title' => 'Registro de cuenta',
    'modal_subtitle_bank' => 'Elige un banco',
    'modal_subtitle_currency' => 'Moneda de la cuenta',
    'modal_subtitle_number' => 'Número de cuenta',
    'modal_subtitle_cci' => 'Número interbancario',

    'modal_btnsave' => 'Registrar cuenta',
    'modal_btncancel' => 'Cancelar',

    'modal_confirmalert' => 'La cuenta ha sido registrada exitosamente!',

];
