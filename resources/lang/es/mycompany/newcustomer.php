<?php

return [
    'title_page' => 'Registro de cliente',
    'title_pageedit' => 'Editar regitro del cliente',

    'subtitle_information' => 'Información de la empresa',

    'text_typedocument' => 'Tipo de documento',
    'text_numberdocument' => 'Número de documento',
    'text_companyname' => 'Nombre / Razón Social',
    'text_brandname' => 'Nombre Comercial',
    'text_contactmail' => 'Email de Contacto',
    'text_contactphone' => 'Teléfono de Contacto',
    'text_website' => 'Página Web',
    'text_currency' => 'Moneda de facturación',

    'subtitle_billing' => 'Dirección de facturación',
    'text_fastaddress' => 'Descripción Rápida',
    'text_address' => 'Dirección Completa',
    'text_country' => 'País',
    'text_department' => 'Departamento',
    'text_province' => 'Provincia',
    'text_district' => 'Distrito',
    'text_postalcode' => 'Código Postal',

    'btn_savecustomer' => 'Registrar cliente',
    'btn_updatecustomer' => 'Actualizar registro del cliente',

    'select_textselect' => 'Seleccione - -',

    'modal_saved' => 'Registro guardado',
    'modal_updated' => 'Registro actualizado',
    'modal_complete' => 'Completa la información faltante',

    'text_creditdatys' => 'Crédito para la emisión de recibos (en días)',

];
