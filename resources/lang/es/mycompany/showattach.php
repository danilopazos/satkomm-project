<?php

return [
    'title_page' => 'Adjuntar documentos',

    'subtitle_page' => 'Elige el documento que deseas adjuntar',

    'save_ok' => 'El documento ha sido guardado correctamente!',

    'text_typeattach' => 'Tipo de documento',
    'text_notes' => 'Anotaciones',
    'text_attach' => 'Selecciona el documento a adjuntar',
    'text_expedition' => 'Fecha de emisión',
    'text_expiration' => 'Fecha de vencimiento',

    'option_choose' => 'Seleccione - -',

    'btn_attach' => 'Guardar documento',
    'btn_return' => 'Ver registro',
    'btn_returnorder' => 'Volver a la orden de servicio',

    'modal_message' => 'Completa los campos marcados en rojo!',

];
