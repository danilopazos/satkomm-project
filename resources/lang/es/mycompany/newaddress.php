<?php

return [
    'title_page' => 'Agrega una dirección',
    'title_page_edit' => 'Editar una dirección',

    'subtitle_information' => 'Información de la empresa',

    'text_typedocument' => 'Tipo de documento',
    'text_numberdocument' => 'Número de documento',
    'text_companyname' => 'Nombre / Razón Social',
    'text_brandname' => 'Nombre Comercial',
    'text_contactmail' => 'Email de Contacto',
    'text_contactphone' => 'Teléfono de Contacto',
    'text_website' => 'Página Web',
    'text_currency' => 'Moneda de facturación',

    'subtitle_billing' => 'Dirección de facturación',
    'subtitle_infoaddress' => 'Información de la dirección',
    'text_company' => 'Elige una empresa',
    'text_companyName' => 'Nombre de la empresa',
    'text_fastaddress' => 'Descripción Rápida',
    'text_address' => 'Dirección Completa',
    'text_country' => 'País',
    'text_department' => 'Departamento',
    'text_province' => 'Provincia',
    'text_district' => 'Distrito',
    'text_typeaddress' => 'Tipo de dirección',
    'text_postalcode' => 'Código Postal',
    'text_coordinates' => 'Coordenadas',

    'btn_updateaddress' => 'Actualizar dirección',
    'btn_saveaddress' => 'Registrar dirección',
    'btn_savecustomer' => 'Registrar cliente',
    'btn_updatecustomer' => 'Actualizar registro del cliente',

    'select_textselect' => 'Seleccione - -',

    'modal_complete' => 'Completa la información faltante!',
    'modal_saved' => 'Dirección registrada!',
    'modal_updated' => 'Dirección actualizada!',

];
