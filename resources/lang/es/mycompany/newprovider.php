<?php

return [
    'title_page' => 'Registro de proveedores',

    'subtitle_information' => 'Información de proveedores',

    'text_typedocument' => 'Tipo de documento',
    'text_numberdocument' => 'Número de documento',
    'text_companyname' => 'Nombre / Razón Social',
    'text_brandname' => 'Nombre Comercial',
    'text_contactmail' => 'Email de Contacto',
    'text_contactphone' => 'Teléfono de Contacto',
    'text_website' => 'Página Web',
    'text_currency' => 'Moneda por defecto',

    'subtitle_billing' => 'Dirección del proveedor',
    'text_fastaddress' => 'Descripción Rápida',
    'text_address' => 'Dirección Completa',
    'text_country' => 'País',
    'text_department' => 'Departamento',
    'text_province' => 'Provincia',
    'text_district' => 'Distrito',
    'text_postalcode' => 'Código Postal',

    'btn_savecustomer' => 'Registrar proveedor',
    'btn_updatecustomer' => 'Actualizar registro del proveedor',

    'select_textselect' => 'Seleccione - -',

];
