<?php

return [
    'title_page' => 'Tipo de cambio',

    'btn_newcurrency' => 'Registrar nueva moneda',
    'tabletitle_list' => 'Monedas disponibles de la empresa',

    'table_country' => 'País',
    'table_currency' => 'Moneda',
    'table_exchange' => 'Cambio',
    'table_history' => 'Historial',
    'table_modify' => 'Modificar',

    'text_noresults' => 'No se han asignado monedas a la empresa!',
    'text_calltoaction' => 'Comienza ahora y registra una moneda!',

    'modal_title' => 'Registrar nueva moneda',
    'modal_selectCur' => 'Elige una moneda',
    'modal_exchange' => 'Ingresa el tipo de cambio',

    'modal_btnsave' => 'Registrar moneda',
    'modal_btncancel' => 'Cancelar',

    'modal_success' => 'Los datos han sido guardados exitosamente!',

];
