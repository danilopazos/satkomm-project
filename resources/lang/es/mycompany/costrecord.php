<?php

return [
    'title_page' => 'Registro de detalle de costos',
    'subtitle_page' => 'Lista de detalles registrados',

    'text_noresults' => 'No se han registrado costos!',
    'text_calltoaction' => 'Comienza ahora y registra un costo',
    'btn_newrecord' => 'Registrar un nuevo costo',

    'table_detail' => 'Detalle del costo',
    'table_date' => 'Fecha de registro',

    'modal_save' => 'Guardar detalle',
    'modal_cancel' => 'Cancelar',
    'modal_title' => 'Registro de detalles',
    'modal_text' => 'Ingresa un detalle',
    'modal_saved' => 'Detalle registrado!',

];