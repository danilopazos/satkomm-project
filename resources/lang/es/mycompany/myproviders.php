<?php

return [
    'title_page' => 'Mis Proveedores',

    'btn_newcustomer' => 'Registrar un nuevo proveedor',
    'btn_assigncustomertoprovider' => 'Asignar un cliente como proveedor',

    'tabletitle_list' => 'Lista de proveedores registrados',

    'table_code' => 'Código',
    'table_customer' => 'Cliente',
    'table_provider' => 'Proveedor',
    'table_location' => 'Ubicación',
    'table_currency' => 'Moneda',
    'table_edit' => 'Editar',
    'table_information' => 'Revisar',

    'message_saveok' => 'Registro guardado!',

    'table_assign' => 'Asignar como cliente',

    'text_noresults' => 'No se han registrado proveedores!',
    'text_calltoaction' => 'Registra uno ahora',

];
