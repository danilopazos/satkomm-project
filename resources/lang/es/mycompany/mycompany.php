<?php

return [
    'title_page' => 'Perfil de empresa',

    'subtitle_information' => 'Información de la empresa',
    'text_companyname' => 'Razón Social',
    'text_brandname' => 'Nombre Comercial',
    'text_contactmail' => 'Email de Contacto',
    'text_contactphone' => 'Teléfono de Contacto',
    'text_website' => 'Página Web',
    'text_currency' => 'Moneda por defecto',

    'subtitle_billing' => 'Dirección de facturación',
    'text_fastaddress' => 'Dirección Rápida',
    'text_address' => 'Dirección Completa',
    'text_country' => 'País',
    'text_department' => 'Departamento',
    'text_province' => 'Provincia',
    'text_district' => 'Distrito',
    'text_postalcode' => 'Código Postal',

    'table_typeaddress' => 'Tipo dirección',
    'table_description' => 'Descripción',
    'table_status' => 'Estado',
    'table_address' => 'Dirección',
    'table_location' => 'Ubicación',
    'table_postal' => 'Cod. Postal',
    'table_edit' => 'Editar',
    'href_editcustomer' => 'Editar registro',

    'btn_addaddress' => 'Registra una dirección',
    'btn_editcompany' => 'Editar registro',

    'subtitle_address' => 'Direcciones registradas',

    'subtitle_contracts' => 'Contratos asignados',
    'href_download' => 'Descargar',
    'modal_nofiles' => 'Selecciona un documento!',
    'text_nocontracts' => 'No existen documentos adjuntos a este cliente!',
    'file_choose' => 'Adjunta un contrato aquí',
    'href_upload' => 'Cargar contrato firmado',
    'saved_attach' => 'Contrato guardado!',

    'href_uploadhere' => 'Adjuntar documentos',

    'btn_moreoptions' => 'Ver opciones',
        'option_upload_companylogo' => 'Editar logo de la empresa',
        'option_upload_explainimageinvoice' => 'Editar imagen explicativa del recibo',
        'option_edit_information' => 'Editar información de la empresa',
        'option_edit_documentsexpiration' => 'Renovación de documentos',
        'option_aditionaltasks' => 'Opciones adicionales',
        'option_viewinvoices' => 'Ver recibos emitidos',
        'option_recordcost' => 'Ver registro de costos',
        'option_listprices' => 'Planes y precios asignados',

    'text_typerecord' => 'Tipo de registro',

    'text_creditdays' => 'Crédito para la emisión de recibos',
    'text_days' => ':number días',

];
