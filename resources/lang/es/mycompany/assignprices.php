<?php

return [
    'title_page' => 'Lista de precios asignados',
    'subtitle_page' => 'Precios asignados a : ',

    'table_typeplan' => 'Tipo de plan',
    'table_plan' => 'Plan',
    'table_band' => 'Banda',
    'table_notes' => 'Anotaciones',
    'table_costo' => 'Costo',
    'table_assigned' => 'Lista de precios',
    'table_review' => 'Revisar',
    'btn_review' => 'Ver precios',

    'text_found' => ':number precios encontrados',
    'text_notfound' => '0 coincidencias...',
    'btn_back' => 'Volver',

    'modal_btn_savebutton' => 'Guardar lista de precios',
    'modal_btn_cancel' => 'Cancelar',
    'modal_title' => 'Lista de precios : ',
    'modal_subtitle_cost' => 'Costo del servicio : ',
    'modal_table_description' => 'Descripción',
    'modal_table_price' => 'Valor de venta en USD',
    'modal_addprice' => 'Agregar un nuevo precio',
    'modal_moreconnections' => 'más',
    'modal_from' => 'De ',
    'modal_to' => ' a ',
    'modal_connections' => ' cnxs',
    'modal_months' => ' meses',
    'modal_withoutassign' => 'Sin asignar',
    'modal_typenote' => 'Ingresa una anotación...',
    'modal_typeprice' => 'Ingresa un valor de venta...',
    'modal_saved' => 'Los precios han sido guardados correctamente!',
];