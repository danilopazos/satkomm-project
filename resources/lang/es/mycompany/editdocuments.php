<?php

return [

    'title_page' => 'Renovación de documentos',

    'subtitle_page' => 'Información sobre el plazo de vencimiento para documentos',

    'text_term' => 'Plazo en meses para la renovación',
    'text_yes' => 'Si',
    'text_required' => 'obligatorio',
    'text_months' => 'meses',

    'btn_update' => 'Actualizar plazo de vencimiento',

    'table_description' => 'Descripción del documento',
    'table_forcustomer' => '¿Documento adjunto al registro del Cliente?',
    'table_fororder' => '¿Documento adjunto a la Orden de servicio?',
    'table_due' => '¿Aplica vencimiento?',

    'modal_saveok' => 'Guardado correctamente!',
    'modal_minterm' => 'El periodo mínimo es 1 mes',
    'modal_needtype' => 'Debes ingresar un plazo mínimo',

];
