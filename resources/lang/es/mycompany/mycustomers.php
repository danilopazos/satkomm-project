<?php

return [
    'title_page' => 'Mis Clientes',

    'btn_newcustomer' => 'Registrar un nuevo cliente',
    'btn_assigncustomertoprovider' => 'Asignar a un proveedor como cliente',

    'tabletitle_list' => 'Lista de clientes registrados',

    'table_code' => 'Código',
    'table_customer' => 'Cliente',
    'table_location' => 'Ubicación',
    'table_currency' => 'Facturación',
    'table_edit' => 'Editar',
    'table_information' => 'Revisar',
    'table_assign' => 'Asignar como proveedor',

    'message_saveok' => 'Registro guardado!',

    'text_noresults' => 'No se han registrado clientes!',
    'text_calltoaction' => 'Registra uno ahora',

];
