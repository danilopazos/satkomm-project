<?php

return [

    'text_hello' => 'Hola ',

    'menu_myprofile' => 'Mi Perfil',
    'menu_mypassword' => 'Nueva Clave',
    'menu_logout' => 'Salir',

    'alert_title' => 'Centro de alertas',
    'alert_showmore' => 'Ver todas las alertas',

    'leftmenu_godashboard' => 'Ir al Dashboard',
    'leftmenu_titlemenu' => 'Menú',


    'leftmenu_platform' => 'Plataforma',
        'leftmenu_platformaccounts' => 'Usuarios con acceso',
        'leftmenu_platformcompanies' => 'Empresas con cuenta',
        'leftmenu_sendalerts' => 'Enviar alertas',

    'leftmenu_productivity' => 'Espacio personal',
        'leftmenu_notes' => 'Mis notas',
        'leftmenu_calendar' => 'Calendario', //not yet


    'leftmenu_mycompany' => 'Mi empresa',
        'leftmenu_companyprofile' => 'Perfil de empresa',
        'leftmenu_bankaccount' => 'Cuentas bancarias',
        'leftmenu_exchange' => 'Tipo de cambio',
        'leftmenu_customers' => 'Mis clientes',
        'leftmenu_providers' => 'Mis proveedores',



    'leftmenu_vsat' => 'VSAT',
        'leftmenu_inventory' => 'Inventario',
        'leftmenu_inventoryvsat' => 'Inventario Vsat',
        'leftmenu_inventoryview' => 'Ver el inventario',
        'leftmenu_inventorynew' => 'Registrar ingreso',
        'leftmenu_plans' => 'Planes',

    'leftmenu_consultvsat' => 'Ordenes de servicio',
        'leftmenu_periodsvsat' => 'Periodos facturados',
        'leftmenu_viewinvoices' => 'Ver pagos registrados',
        'leftmenu_viewconsumptions' => 'Consultar consumos',

    'title_dashboard' => 'Dashboard',
        'subtitle_earnings' => 'Ingresos totales en ',
        'subtitle_orderscomplete' => 'Servicios facturados en ',
        'subtitle_totalactive' => 'Activaciones en ',
            'text_totalactive' => ' comerciales ',
        'subtitle_ordersdue' => 'Ordenes por vencer en ',
        'subtitle_historial' => 'Histórico de ingresos hasta ',
        'subtitle_inventory' => 'Resumen del inventario de antenas Vsat',


    'title_myprofile' => 'Mi Perfil',
        'text_name' => 'Nombres',
        'text_lastname' => 'Apellidos',
        'text_email' => 'Email',
        'text_jobtitle' => 'Cargo',
        'text_cellphone' => 'Celular',
        'text_telephone' => 'Teléfono',
        'text_language' => 'Idioma de la cuenta',
        'btn_saveprofile' => 'Actualizar mi perfil',


    'title_changepassword' => 'Cambiar clave',
        'text_username' => 'Usuario',
        'text_password' => 'Clave actual',
        'text_newpass' => 'Nueva clave',
        'text_reppass' => 'Confirma clave',
        'btn_savepass' => 'Actualizar mi clave',

    'btn_report' => 'Ingresos del mes de : ',

    'text_search' => 'Buscar',
    
    'modal_ups' => 'Ups! Algo salió mal!',
    'modal_btnok' => 'Entendido!',
    'modal_complete' => 'Completa la información faltante',
    'modal_saved' => 'Información actualizada correctamente!',
    'modal_passsaved' => 'La clave ha sido actualizada!',
    'modal_passincorrect' => 'La clave es incorrecta!',
    'modal_passdifferent' => 'Las claves ingresadas son diferentes!',

    'text_listorders' => 'Ordenes pendientes de aprobación',
    'text_moreorders' => 'Ver lista completa',

];
