<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('page_title')</title>

    <!-- Custom fonts for this template-->
    <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin-2.css" rel="stylesheet">
    <link href="/css/timeline.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <link rel="icon" href="/img/icon.jpeg">
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/home">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="sidebar-brand-text mx-3">{{ config('app.name') }}<sup>1.0</sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/home">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>{{ __('dashboard/texts.leftmenu_godashboard') }}</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            {{ __('dashboard/texts.leftmenu_titlemenu') }}
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        @if (auth()->user()->usertype_id == "ALLS")
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdmin" aria-expanded="false" aria-controls="collapseTwo">
                    <i class="fas fa-tools"></i>
                    <span>{{ __('dashboard/texts.leftmenu_platform') }}</span>
                </a>
                <div id="collapseAdmin" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="/admin/showusers">{{ __('dashboard/texts.leftmenu_platformaccounts') }}</a>
                        <a class="collapse-item" href="#">{{ __('dashboard/texts.leftmenu_platformcompanies') }}</a>
                        <a class="collapse-item" href="#">{{ __('dashboard/texts.leftmenu_sendalerts') }}</a>
                    </div>
                </div>
            </li>
        @endif

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseHelpers" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-comment-alt"></i>
                <span>{{ __('dashboard/texts.leftmenu_productivity') }}</span>
            </a>
            <div id="collapseHelpers" class="collapse @if(request()->routeIs('me.*')) show @endif" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item @if(request()->routeIs('me.notes.*')) active @endif" href="/me/notes">{{ __('dashboard/texts.leftmenu_notes') }}</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-building"></i>
                <span>{{ __('dashboard/texts.leftmenu_mycompany') }}</span>
            </a>
            <div id="collapseOne" class="collapse @if(request()->routeIs('mycompany.*')) show @endif" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item @if(request()->routeIs('mycompany.profile.*')) active @endif" href="/mycompany">{{ __('dashboard/texts.leftmenu_companyprofile') }}</a>
                    <a class="collapse-item @if(request()->routeIs('mycompany.banks.*')) active @endif" href="/bank/listbanks">{{ __('dashboard/texts.leftmenu_bankaccount') }}</a>
                    <a class="collapse-item @if(request()->routeIs('mycompany.exchange.*')) active @endif" href="/typeexchange">{{ __('dashboard/texts.leftmenu_exchange') }}</a>
                    <a class="collapse-item @if(request()->routeIs('mycompany.mycustomer.*')) active @endif" href="/mycustomers">{{ __('dashboard/texts.leftmenu_customers') }}</a>
                    <a class="collapse-item @if(request()->routeIs('mycompany.myprovider.*')) active @endif" href="/myproviders">{{ __('dashboard/texts.leftmenu_providers') }}</a>
                </div>
            </div>
        </li>


        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-satellite-dish"></i>
                <span>{{ __('dashboard/texts.leftmenu_vsat') }}</span>
            </a>
            <div id="collapseThree" class="collapse @if(request()->routeIs('vsat.*')) show @endif" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item @if(request()->routeIs('vsat.inventory.*')) active @endif" href="/listinventorysvsat">{{ __('dashboard/texts.leftmenu_inventory') }}</a>
                    <a class="collapse-item @if(request()->routeIs('vsat.plan.*')) active @endif" href="/listplansvsat">{{ __('dashboard/texts.leftmenu_plans') }}</a>
                    <a class="collapse-item @if(request()->routeIs('vsat.order.*')) active @endif" href="/listservicesvsat">{{ __('dashboard/texts.leftmenu_consultvsat') }}</a>
                    <a class="collapse-item @if(request()->routeIs('vsat.invoices.*')) active @endif" href="/listinvoicesvsat">{{ __('dashboard/texts.leftmenu_periodsvsat') }}</a>
                </div>
            </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="{{ __('dashboard/texts.text_search') }}" aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>

                    @if(auth()->user()->usertype_id == 'ADSY' or auth()->user()->usertype_id == 'ALLS')
                        <!-- DIV PENDINGS -->
                        <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="openPendings();">
                                <i class="fas fa-tasks"></i>
                                <!-- Counter - Alerts -->
                                <span class="badge badge-danger badge-counter" id="countPendings" style="display: none;">0</span>
                            </a>

                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                                <h6 class="dropdown-header">
                                    <i class="fas fa-tasks"></i> {{ __('dashboard/texts.text_listorders') }}
                                </h6>
                                <div id="pendingsAlerts"></div>
                                <a class="dropdown-item text-center small text-gray-500" href="#">{{ __('dashboard/texts.text_moreorders') }}</a>
                            </div>
                        </li>
                    @endif

                    <!-- DIV ALERTS -->
                    <li class="nav-item dropdown no-arrow mx-1">
                        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="openMyAlerts();">
                            <i class="fas fa-bell fa-fw"></i>
                            <!-- Counter - Alerts -->
                            <span class="badge badge-danger badge-counter" id="countAlerts" style="display: none;">0</span>
                        </a>

                        <!-- Dropdown - Alerts -->
                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown" id="divMyAlerts" style="max-height: 50em; overflow-y: auto;">
                            <h6 class="dropdown-header" style="position: static;">
                                <i class="fas fa-bell fa-fw"></i> {{ __('dashboard/texts.alert_title') }}
                            </h6>
                            <div id="messagesAlerts"></div>
                            <a class="dropdown-item text-center small text-gray-600" href="#"> {{ __('dashboard/texts.alert_showmore') }}</a>
                        </div>
                    </li>
                    

                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ __('dashboard/texts.text_hello').auth()->user()->name }}!</span>
                            <img class="img-profile rounded-circle" src="{{ auth()->user()->imgprofile }}">
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="/myprofile">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                {{ __('dashboard/texts.menu_myprofile') }}
                            </a>
                            <a class="dropdown-item" href="/changepassword">
                                <i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>
                                {{ __('dashboard/texts.menu_mypassword') }}
                            </a>
                            <div class="dropdown-divider"></div>

                            <form method="post" action="/logout">
                                @csrf
                                <button type="submit" class="dropdown-item">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    {{ __('dashboard/texts.menu_logout') }}
                                </button>

                            </form>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">@yield('info_title')</h1>
                    @yield('putButton')
                </div>

                @yield('content')

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span style="margin-bottom: 4px;">Copyright &copy; Astelekomm {{ date("Y") }}</span><br />
                    <span style="font-size: 10px; color: #a6acaf;">Powered by <a href="https://linkedin.com/in/danilopazos" target="_blank">Danilo Pazos</a></span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<script src="/js/dashboard/general-functions.js"></script>
<script src="/js/dashboard/loadalerts.js"></script>
@if(auth()->user()->usertype_id == 'ADSY' or auth()->user()->usertype_id == 'ALLS')
    <script src="/js/dashboard/loadorderspending.js"></script>
@endif
<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="/js/sb-admin-2.min.js"></script>

<!-- Page level custom scripts -->
<script src="/js/demo/datatables-demo.js"></script>

<!-- Script for alerts -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script src="/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script language="javascript">
    let modal_ups   = "{{ __('dashboard/texts.modal_ups') }}";
    let modal_btnok = "{{ __('dashboard/texts.modal_btnok') }}";
    
    firstTimeQtyAlerts();

    @if(auth()->user()->usertype_id == 'ADSY' or auth()->user()->usertype_id == 'ALLS')
        firstTimeGetQtyPendings();
    @endif
</script>

@yield('scripts')

</body>

</html>
