@extends('layouts.dashboard')
@section('page_title', __('me/notes.title_page'))
@section('info_title', __('me/notes.title_page'))

@section('putButton')
    <a href="/me/notes" class="btn btn-primary">
        <i class="fas fa-chevron-left"></i>
        {{ __('me/notes.btn_returnnotes')  }}
    </a>
@endsection

@section('content')
    <!-- Content Row -->
    <div class="row" style="padding-bottom:15px;">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body" style="text-align:right;">
                    <a href="#" class="btn btn-danger" onclick="return deleteNote();">
                        <i class="fas fa-trash"></i>
                        {{ __('me/notes.btn_delete')  }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-8" id="messageIsSave" style="display:none; " onclick="closeAlertSaved();">
            <div class="alert alert-success" role="alert">
                <i class="fas fa-check-circle"></i>
                {{ __('me/notes.text_saved') }}
            </div>
        </div>

        <div class="col-md-8 mb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body" >
                    <form id="formNote">
                    @csrf
                    <input type="hidden" id="hiddenCode" name="hiddenCode" value="{{ $note['code'] }}">
                    <input type="hidden" id="timeToSave" name="timeToSave" value="0">
                    <input type="hidden" id="isSaved" name="isSaved" value="yes">

                    <div class="row">

                            <div class="col-sm-12 py-2">
                                <input class="form-control" name="textTitle" id="textTitle" placeholder="{{ __('me/notes.text_title') }}" autocomplete="off" value="{{ $note['title'] }}">
                            </div>

                            <div class="col-sm-12 py-2">
                                <textarea class="form-control" id="textBody" name="textBody" style="height: 30em;" placeholder="{{ __('me/notes.text_body') }}" autofocus>{{ $note['body'] }}</textarea>
                            </div>

                            <div class="col-sm-12 py-2">
                                <br />
                                <button type="button" class="btn btn-primary btn-user btn-block" onclick="saveNote();">
                                    {{ __('me/notes.btn_save') }}
                                </button>
                            </div>

                            <div class="col-sm-12 py-2" style="text-align: center;">
                                <hr/>
                                <span class="text-primary">
                                <i class="far fa-lightbulb"></i> {{ __('me/notes.text_remember') }}
                                </span>
                            </div>

                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection

@section ('scripts')
    <script language="javascript">
        let modal_noblank = "{{ __('me/notes.modal_noblank') }}";
        let modal_saved = "{{ __('me/notes.modal_saved') }}";
        let modal_behere = "{{ __('me/notes.modal_behere') }}";
        let modal_checkall = "{{ __('me/notes.modal_checkall') }}";
        let modal_askquit = "{{ __('me/notes.modal_askquit') }}";
        let modal_yesdelete = "{{ __('me/notes.modal_yesdelete') }}";
        let modal_cancel = "{{ __('me/notes.modal_cancel') }}";

    </script>
    <script src="/js/me/newnote.js"></script>
@endsection
