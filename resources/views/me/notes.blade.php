@extends('layouts.dashboard')
@section('page_title', __('me/notes.title_page'))
@section('info_title', __('me/notes.title_page'))

@section('putButton')
    <a href="/me/newnote" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
        </span>
        <span class="text">{{ __('me/notes.btn_create')  }}</span>
    </a>
@endsection

@section('content')
    <!-- Content Row -->
    <div class="row">
        @if (count($arrayNotes) > 0)
            @foreach ($arrayNotes as $note)
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body" style="cursor: pointer;" onclick="viewNote('{{ $note['code'] }}');">
                            <h5 class="card-title">
                                @if( strlen($note['title']) > 40 )
                                    {{ trim(substr($note['title'], 0, 40))."..." }}
                                @else
                                    {{ $note['title'] }}
                                @endif
                            </h5>
                            <p class="card-text">
                                @if( strlen($note['body']) > 120 )
                                    {{ trim(substr($note['body'], 0, 120))."..." }}
                                @else
                                    {{ $note['body'] }}
                                @endif
                                <br />
                                <span style="color: #aeb6bf;">{{ __('me/notes.text_updated').$note['day']." ".__('vsat/orderconsumptions.month_'.$note['month'])." ".$note['year'] }}</span>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach

        @else
            
            <div class="col-md-12 mb-4">
                <div class="card py-4">
                    @include('partials._noresults',[
                        'title' => __('me/notes.title_hello').auth()->user()->name.__('me/notes.title_calltoaction'),
                        'text' => __('me/notes.text_calltoaction'),
                        'textBtn' => __('me/notes.btn_calltoaction'),
                        'urlBtn' => '/me/newnote'
                    ])
                </div>
            </div>

        @endif

    </div>

@endsection

@section ('scripts')
    <script src="/js/me/newnote.js"></script>
@endsection
