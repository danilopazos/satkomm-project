<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@lang('error/error.title_404')</title>
        <meta name="description" content="Saleh Riaz - UI/UX Engineer. Designer. Computer Scientist">
        <meta name="keywords" content="ui engineer, ux, saleh, riaz, qureshi, website, softwares, salehriaz, salehriazq, computer scientist, design, visual design, saleh riaz qureshi"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/css/error.css">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="icon" href="/img/icon.jpeg">        
    </head>
    <body class="bg-purple">
        <div class="stars">
            <div class="central-body">
                <!-- <img class="image-404" src="/img/error/404.svg" width="300px"> -->
                <span style="color: #FFF; font-size: 120px; font-weight: bold;">404</span>
                <br />
                <span style="color: #FFF; font-size: 20px;">@lang('error/error.text_404')</span>
                <a href="/" class="btn-go-home"><i class="fas fa-home"></i> @lang('error/error.btn')</a>
            </div>
            <div class="objects">
                <img class="object_rocket" src="/img/error/rocket.svg" width="40px">
                <div class="earth-moon">
                    <img class="object_earth" src="/img/error/earth.svg" width="100px">
                    <img class="object_moon" src="/img/error/moon.svg" width="80px">
                </div>
                <div class="box_astronaut">
                    <img class="object_astronaut" src="/img/error/astronaut.svg" width="140px">
                </div>
            </div>
            <div class="glowing_stars">
                <div class="star"></div>
                <div class="star"></div>
                <div class="star"></div>
                <div class="star"></div>
                <div class="star"></div>

            </div>

        </div>
    </body>
</html>