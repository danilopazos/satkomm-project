@extends('layouts.dashboard')
@section('page_title', __('error/nopermission.title_page'))

@section('content')

<div class="row mb-4">
    <div class="col-xl-8 col-sm-12 py-2 mx-auto">
        <div class="row">
            <div class="col-sm-12 py-2">
                <h2 class="card-title text-dark font-weight-bold text-center">
                    @lang('error/nopermission.title')
                </h2>
            </div>
            <div class="col-xl-8 col-sm-10 py-2 mx-auto">
                <div class="card-body">
                    <img src="/img/error/security.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-xl-8 col-sm-10 py-4 mx-auto">
                <a href='/home' class="btn btn-primary btn-block btn-lg">
                    @lang('error/nopermission.btn_gohome')
                </a>
            </div>
        </div>
    </div>
</div>

@endsection