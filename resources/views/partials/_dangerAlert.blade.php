<div class="row">
    <div class="col-xl-9 col-md-12 mx-auto">
        <div class="alert alert-danger" role="alert">
            <i class="fas fa-times-circle"></i> {{ $message }}
        </div>
    </div>
</div>