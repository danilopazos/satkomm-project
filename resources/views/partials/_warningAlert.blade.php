<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning" role="alert">
            <i class="fas fa-clock"></i> {{ $message }}
        </div>
    </div>
</div>