<div class="row">
    <div class="col-xl-9 col-md-12 mx-auto">
        <div class="alert alert-success" role="alert">
            <i class="far fa-thumbs-up"></i> {{ $message }}
        </div>
    </div>
</div>