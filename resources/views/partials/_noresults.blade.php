<div class="col-cm-12 py-4">
    <h4 align="center" class="card-title">
            <span class="text-danger">
                <i class="fas fa-heart-broken"></i>
                {{ $title }}
            </span>
    </h4>
    <p align="center" class="card-text">
            <span class="text-gray-800">{{ $text }}</span>
        <br /><br />
        <a href="{{ $urlBtn }}" class="btn btn-danger btn-icon-split" @isset($onclick){!! $onclick !!} @endisset>
            <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
            </span>
                <span class="text">{{ $textBtn }}</span>
        </a>
    </p>
</div>