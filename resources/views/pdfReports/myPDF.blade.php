<!DOCTYPE html>
<html>
<head>
    <title>{{ $customer_name.__('pdfReports/vsat.title') }}</title>
    <style>
        #header {
            position: fixed;
            top: 0px;
            left: -50px;
            right: -50px;
            border-bottom: 1px solid #eaeded;
            height: 100px;
            padding-left: 40px;
            padding-top: -20px;
            padding-right: 50px;
            font-size: 12px;
            font-family: "Sans-serif";
        }

        #footer {
            border-top: 1px solid #eaeded;
            left: -50px;
            right: -50px;
            position: fixed;
            bottom: -20px;
            height: 40px;
            text-align: right;
            line-height: 35px;
            font-size: 10px;
            font-style: italic;
            padding-right: 50px;
            font-family: "Sans-serif";
        }

        #bodyPdf {
            position: fixed;
            top: 90px;
            left: -5px;
            font-family: "Sans-serif";
            font-size: 11px;
            padding-right: 10px;
        }

        table {
            border-collapse: collapse;
        }

        td {
            padding: 5px;
            padding-left: 10px;
        }

        p {
            margin-top: 4px;
        }

        .title {
            font-size: 12px;
            margin-bottom: 8px;
            font-weight: bold;
        }

        .sub-title {
            font-size: 11px;
            margin-bottom: 4px;
            font-weight: bold;
        }

        .terms-title {
            font-size: 14px;
            margin-bottom: 8px;
            font-weight: bold;
        }

        .terms-sub-title {
            font-size: 12px;
            margin-bottom: 4px;
            font-weight: bold;
        }
        
        .terms-text {
            font-size: 12px;
            margin-bottom: 4px;
        }

        .little-title {
            font-size: 10px;
            font-weight: bold;
        }

        .little-text {
            font-size: 10px;
        }

        .highlighted-text{
            background-color: #fcf3cf;
            padding: 4px;
        }

        .table-head{
            margin-bottom:2px; 
            background-color: #f2f3f4; 
            padding-top: 6px; 
            padding-bottom: 6px;
        }

        .table-footer{
            border: 1px solid #b2babb;
            margin-bottom:2px; 
            background-color: #f2f3f4; 
            padding-top: 6px; 
            padding-bottom: 6px;
        }

        .table-qrcode{
            border: 1px solid #b2babb;
            margin-top:50px;
            padding-top: 6px; 
            padding-bottom: 6px;
            width: 75%;
        }

        .table-body{
            border: 1px solid #b2babb;
            margin-bottom:2px;
            padding-top: 6px;
            padding-bottom: 6px;
        }

        .page {
            page-break-after: always;
        }
        .page_last {
            page-break-after: unset;
        }
        
    </style>
</head>
<body>

    <div id='header'>
        <table width="100%">
            <tr>
                <td><img src="{{ $company_logo }}" height="80px"></td>
                <td align="right">
                    <b>{{ $company_name }}</b><br />
                    <b>{{ $company_typedocument.$company_taxnumber }}</b><br />
                    {{ $company_address }}<br />
                    {{ __('pdfReports/vsat.text_number').$company_number }}<br />
                    {{ __('pdfReports/vsat.text_mail').$company_email }}<br />
                </td>
            </tr>
        </table>
    </div>

    <div id='footer'>
        {{ $company_name }}
    </div>

    @php ($page = 0)
    @php ($qtyLines = 0)
    @php ($maxLines = $document_lines)
    @php ($firstTaxes = number_format($invoice_total * $company_taxamount / 100, 2) )
    @php ($allTotal = $invoice_total +  $firstTaxes )
    <div class="page">
        @php ($page ++)
        <div id='bodyPdf'>
            <table width="100%">
                <tr>
                    <td width="60%" valign="top">
                        <p class='title'>{{ __('pdfReports/vsat.text_customerinfo') }}</p>
                        <table style="border: 1px solid #b2babb; width: 95%;">
                            <tr>
                                <td width='25%'><span class='sub-title'>{{ $customer_typedocument }}</span></td>
                                <td>{{ $customer_taxnumber }}</td>
                            </tr>
                            <tr>
                                <td><span class='sub-title'>{{ __('pdfReports/vsat.customer_name') }}</span></td>
                                <td>{{ $customer_name }}</td>
                            </tr>
                            <tr>
                                <td><span class='sub-title'>{{ __('pdfReports/vsat.customer_address') }}</span></td>
                                <td>{{ $customer_address }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>{{ $customer_location }}</td>
                            </tr>
                            <tr>
                                <td><span class='sub-title'>{{ __('pdfReports/vsat.customer_currency') }}</span></td>
                                <td>
                                    {{ $customer_currencyName }}
                                    @if ($customer_currencyIso != 'USD')
                                        {{ __('pdfReports/vsat.customer_exchange').$customer_currencySymbol." ".number_format($customer_currencyExchange, 2)." ".$customer_currencyIso }}
                                    @endif
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td valign="top">

                        <p class='title' align="right">{{ __('pdfReports/vsat.text_invoiceinfo') }}</p>
                        <table style="border: 1px solid #b2babb; width: 80%;" align="right">
                            <tr>
                                <td width="38%">
                                    <span class='sub-title'>{{ __('pdfReports/vsat.invoice_number') }}</span>
                                </td>
                                <td>
                                    {{ $invoice_number }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class='sub-title'>{{ __('pdfReports/vsat.invoice_datecreate') }}</span>
                                </td>
                                <td>
                                    {{ date('d', strtotime($invoice_create)).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($invoice_create))).' '.date('Y', strtotime($invoice_create)) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class='sub-title'>{{ __('pdfReports/vsat.invoice_datedue') }}</span>
                                </td>
                                <td>
                                    {{ date('d', strtotime($invoice_due)).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($invoice_due))).' '.date('Y', strtotime($invoice_due)) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class='sub-title'>{{ __('pdfReports/vsat.invoice_period') }}</span>
                                </td>
                                <td>
                                    {{ __('vsat/orderconsumptions.month_'.$invoice_monthperiod)." ".$invoice_yearperiod }}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>

            <p class='title'>{{ __('pdfReports/vsat.text_invoicesummary') }}</p>
            <div class="table-head">
                <table width="100%">
                    <tr>
                        <td><span class='sub-title'>{{ __('pdfReports/vsat.text_description') }}</span></td>
                        <td width="20%" align="center"><span class='sub-title'>{{ __('pdfReports/vsat.text_import').$customer_currencyIso }}</span></td>
                    </tr>
                </table>
            </div>
            
                        @php ($maxLines = 14)
                        @php ($subTotal = 0)
                        @foreach ($listSan as $san)

                            @if($qtyLines >= $maxLines)
                                @php ($maxLines = 22)
                                @php ($document_lines = $document_lines - $qtyLines)
                                @php ($qtyLines = 0)
                                                    <div class="table-qrcode">
                                                        <table width="99%">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td width="34%"><span class='little-title'>{{ __('pdfReports/vsat.customer_taxnumber') }}</span></td>
                                                                            <td><span class="little-text">{{ $customer_taxnumber }}</span></span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><span class='little-title'>{{ __('pdfReports/vsat.customer_name') }}</span></td>
                                                                            <td><span class="little-text">{{ $customer_name }}</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><span class='little-title'>{{ __('pdfReports/vsat.invoice_number') }}</span></td>
                                                                            <td><span class="little-text">{{ $invoice_number }}</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><span class='little-title'>{{ __('pdfReports/vsat.text_finaltotal') }}</span></td>
                                                                            <td><span class="little-text">{{ $customer_currencySymbol." ".number_format($allTotal, 2)." ".$customer_currencyIso }}</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><span class='little-title'>{{ __('pdfReports/vsat.text_create') }}</span></td>
                                                                            <td><span class="little-text">{{ date('d', strtotime($invoice_create)).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($invoice_create))).' '.date('Y', strtotime($invoice_create)) }}</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><span class='little-title'>{{ __('pdfReports/vsat.text_due') }}</span></td>
                                                                            <td><span class="little-text">{{ date('d', strtotime($invoice_due)).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($invoice_due))).' '.date('Y', strtotime($invoice_due)) }}</span></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td width="120px">
                                                                    <img src='https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{ $qr_code.str_replace('.', '', $allTotal) }}&choe=UTF-8&chld=L|0' width="120" height="120" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Intentional close -->
                                            <!-- Intentional open -->
                                            <div class="page">
                                                <div id='bodyPdf'>
                                                
                                                <p class='title'>{{ __('pdfReports/vsat.text_invoicesummary') }}</p>
                                                <div class="table-head">
                                                    <table width="100%">
                                                        <tr>
                                                            <td><span class='sub-title'>{{ __('pdfReports/vsat.text_description') }}</span></td>
                                                            <td width="20%" align="center"><span class='sub-title'>{{ __('pdfReports/vsat.text_import').$customer_currencyIso }}</span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                    
                            @endif

                            <div class="table-body">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            @php ($totalPay = 0)
                                            @php ($countPS = 1)
                                            @php($pincipalPlan = 0)
                                            @php ($is_second = false)
                                            @php ($is_header = false)
                                            @php ($ordersId = [])

                                            @foreach($arrayDetails as $detail)
                                                @if($san['sanCode'] == $detail['sanCode'])
                                                        @php (array_push($ordersId, $detail['orderId']))
                                                        @php ($dateStart = $detail['dateStart'])
                                                        @php ($dateEnd = $detail['dateEnd'])
                                                    
                                                        @if(!$is_header)
                                                            <b>{{ $san['addressName'] }}</b><br />

                                                            {{ __('pdfReports/vsat.text_period') }}
                                                            {{ date('d', strtotime($dateStart)).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($dateStart))).' '.date('Y', strtotime($dateStart)) }}
                                                            {{ __('pdfReports/vsat.text_periodto') }}
                                                            {{ date('d', strtotime($dateEnd)).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($dateEnd))).' '.date('Y', strtotime($dateEnd)) }}
                                                            <br /><br />
                                                            @php($is_header = true)
                                                            @php ($qtyLines = $qtyLines + 2)
                                                        @endif

                                                        @if($is_second)
                                                            <br />
                                                        @endif

                                                        &nbsp;&nbsp;&nbsp;&nbsp;{{ $detail['planName'] }}
                                                        @if(strlen($detail['proration'])>0)
                                                            <span class="highlighted-text">
                                                            @if($detail['payDays'] == 1)
                                                                {{ __('pdfReports/vsat.text_day', ['number' => $detail['payDays']]).$detail['proration'] }}
                                                            @else
                                                                {{ __('pdfReports/vsat.text_days', ['number' => $detail['payDays']]).$detail['proration'] }}                                                            
                                                            @endif
                                                            </span>
                                                        
                                                        @endif

                                                        @php ($totalPay = $totalPay + $detail['ammountToSum'])
                                                        @php($pincipalPlan++)
                                                        @php ($qtyLines = $qtyLines + 1)
                                                        @php($is_second = true)
                                                @endif
                                            @endforeach

                                            @foreach($ordersId as $orderId)
                                                @foreach($arrayDiscounts as $discount)
                                                    @if($discount['order_id'] == $orderId)
                                                        <br />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;{{ __('pdfReports/vsat.text_discount') }}
                                                        <span class="highlighted-text">
                                                            {{ $customer_currencySymbol." -".number_format($discount['usd_discount'], 2)." ".$customer_currencyIso }}
                                                        </span>

                                                        @php ($totalPay = $totalPay - $discount['usd_discount'])
                                                        @php($pincipalPlan++)
                                                        @php ($qtyLines = $qtyLines + 1)
                                                        @php($is_second = true)
                                                    @endif
                                                @endforeach
                                            @endforeach

                                            @php ($totalPay = number_format($totalPay, 2))
                                        </td>
                                        <td width="20%" align="center">{{ $customer_currencySymbol." ".$totalPay." ".$customer_currencyIso }}</td>
                                    </tr>
                                </table>
                            </div>
                            @php ($subTotal = $subTotal + $totalPay)
                        @endforeach
                        @php ($taxes = number_format($subTotal * $company_taxamount / 100, 2) )
                        @php ($allTotal = $subTotal + $taxes)

                        <div class="table-footer">
                            <table width="100%">
                                <tr>
                                    <td width="50%"></td>
                                    <td>{{ __('pdfReports/vsat.text_totalcharge') }}</td>
                                    <td width="20%" align="center">{{ $customer_currencySymbol." ".number_format($subTotal, 2)." ".$customer_currencyIso }}</td>
                                </tr>
                                <tr>
                                    <td width="45%"></td>
                                    <td>{{ __('pdfReports/vsat.text_taxes').$company_taxamount."%" }}</td>
                                    <td width="20%" align="center">{{ $customer_currencySymbol." ".number_format($taxes, 2)." ".$customer_currencyIso }}</td>
                                </tr>
                                <tr>
                                    <td width="45%"></td>
                                    <td><span class='sub-title'>{{ __('pdfReports/vsat.text_finaltotal') }}</span></td>
                                    <td width="20%" align="center"><span class='sub-title'>{{ $customer_currencySymbol." ".number_format($allTotal, 2)." ".$customer_currencyIso }}</span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="table-qrcode">
                            <table width="99%">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td width="34%"><span class='little-title'>{{ __('pdfReports/vsat.customer_taxnumber') }}</span></td>
                                                <td><span class="little-text">{{ $customer_taxnumber }}</span></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class='little-title'>{{ __('pdfReports/vsat.customer_name') }}</span></td>
                                                <td><span class="little-text">{{ $customer_name }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class='little-title'>{{ __('pdfReports/vsat.invoice_number') }}</span></td>
                                                <td><span class="little-text">{{ $invoice_number }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class='little-title'>{{ __('pdfReports/vsat.text_finaltotal') }}</span></td>
                                                <td><span class="little-text">{{ $customer_currencySymbol." ".number_format($allTotal, 2)." ".$customer_currencyIso }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class='little-title'>{{ __('pdfReports/vsat.text_create') }}</span></td>
                                                <td><span class="little-text">{{ date('d', strtotime($invoice_create)).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($invoice_create))).' '.date('Y', strtotime($invoice_create)) }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class='little-title'>{{ __('pdfReports/vsat.text_due') }}</span></td>
                                                <td><span class="little-text">{{ date('d', strtotime($invoice_due)).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($invoice_due))).' '.date('Y', strtotime($invoice_due)) }}</span></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="120px">
                                        <img src='https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{ $qr_code.str_replace('.', '', $allTotal) }}&choe=UTF-8&chld=L|0' width="120" height="120" />
                                    </td>
                                </tr>
                            </table>
                            
                        </div>
                    </td>
                </tr>
                
            </table>

        </div>
    </div>

    <div class="page_last">
        <div id='bodyPdf'>
            <table width="100%">
                <tr>
                    <td width='36%' valign="top">
                        <p align='justify'>
                            <span class='terms-title'>{{ __('pdfReports/vsat.title_concepts') }}</span><br /><br />
                            <span class='terms-sub-title'>{{ __('pdfReports/vsat.subtitle_concepts') }}</span><br />
                            <span class='terms-text'>{{ __('pdfReports/vsat.detail_concepts') }}</span><br /><br />
            
                            <span class='terms-sub-title'>{{ __('pdfReports/vsat.subtitle_plan') }}</span><br />
                            <span class='terms-text'>{{ __('pdfReports/vsat.detail_plan') }}</span><br /><br />
            
                            <span class='terms-sub-title'>{{ __('pdfReports/vsat.subtitle_callsad') }}</span><br />
                            <span class='terms-text'>{{ __('pdfReports/vsat.detail_callsad') }}</span><br /><br />
            
                            <span class='terms-sub-title'>{{ __('pdfReports/vsat.subtitle_otherad') }}</span><br />
                            <span class="terms-sub-title">{{ __('pdfReports/vsat.litle_titleotherad') }}</span>
                            <span class='terms-text'>{{ __('pdfReports/vsat.detail_otherad') }}</span><br />
                            <span class="terms-sub-title">{{ __('pdfReports/vsat.litle_titlerent') }}</span>
                            <span class='terms-text'>{{ __('pdfReports/vsat.detail_rent') }}</span><br />
                            <span class="terms-sub-title">{{ __('pdfReports/vsat.litle_titleservices') }}</span>
                            <span class='terms-text'>{{ __('pdfReports/vsat.detail_servicescall') }}</span><br />
                        </p>
                    </td>
                    <td align="right">
                            <img src="./img/recibo.png" width="400px">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p align='justify'>
                            <span class='terms-title'>{{ __('pdfReports/vsat.title_notes') }}</span><br /><br />
                            <span class='terms-text'>{{ __('pdfReports/vsat.detail_notes') }}</span><br /><br />
            
                            <span class='terms-title'>{{ __('pdfReports/vsat.title_pays') }}</span><br /><br />
                            <span class='terms-text'>{{ __('pdfReports/vsat.detail_pays') }}</span><br /><br />
            
                            <span class='terms-title'>{{ __('pdfReports/vsat.title_address') }}</span><br /><br />
                            <span class='terms-text'>
                            <table width=90%>
                                @php ($countBank = 1)
                                @foreach($arrayBanks as $bank)
                                        @if($countBank == 1)
                                            <tr>
                                                <td>
                                                    <img src=".{{ $bank['image'] }}" width="50px">
                                                </td>
                                        @endif
                                                <td width="40%">
                                                    {{ __('pdfReports/vsat.text_bank').strtoupper($bank['currency']) }}<br />
                                                    {{ __('pdfReports/vsat.text_banknro').$bank['number'] }}<br />
                                                    {{ __('pdfReports/vsat.text_bankcci').$bank['cci'] }}
                                                </td width="40%">
                                        @if($countBank == 2)
                                            </tr>
                                        @php ($countBank = 0)
                                        @endif
                                    @php ($countBank ++)
                                @endforeach
                            </table>
                            </span>
                            <br />
                            <span class='terms-title'>{{ __('pdfReports/vsat.title_cards') }}</span><br /><br /><br />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <img src="./img/visa.png" width="50px">&nbsp;&nbsp;
                            <img src="./img/master.png" width="50px">&nbsp;&nbsp;
                            <img src="./img/american.png" width="50px">&nbsp;&nbsp;
                            <img src="./img/diners.png" width="50px">
                        </p>
                    </td>
                </tr>
            </table>

        </div>
    </div>

</body>
</html>
