@extends('layouts.dashboard')
@section('page_title', __('vsat/newinventoryvsat.title_page_edit'))
@section('info_title', __('vsat/newinventoryvsat.title_page_edit'))

@section('content')

    <div class="row mb-4">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">

                    <form id="updateForm" method="post" action="{{ route('vsat.showInsertItems') }}">
                    @csrf
                    <input type="hidden" name="codeInventory" id="codeInventory" value="{{ $infoInventory->code }}">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">@lang('vsat/newinventoryvsat.subtitle_productinfo')</h5>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectInventory">@lang('vsat/newinventoryvsat.text_typeinventory')</label>
                            <select name="selectInventory" id="selectInventory" class="form-control" onchange="changeinventory();" @if ( $infoInventory->available_id == 'S4' ) disabled="disabled" @endif>
                                @foreach ($sellCategories as $sellCategory)
                                    @if ($infoInventory->category == $sellCategory->code)
                                        <option value="{{ $sellCategory->code }}" selected="selected">{{ $sellCategory->description }}</option>
                                    @else
                                        <option value="{{ $sellCategory->code }}">{{ $sellCategory->description }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypeProduct">@lang('vsat/newinventoryvsat.text_typeproduct')</label>
                            <select name="selectTypeProduct" id="selectTypeProduct" class="form-control" onchange="changeproduct();" @if ( $infoInventory->available_id == 'S4' ) disabled="disabled" @endif>
                                @foreach ($listTypeProducts as $TypeProduct)
                                    @if ($infoInventory->type == $TypeProduct->code)
                                        <option value="{{ $TypeProduct->code }}" selected="selected">{{ $TypeProduct->description }}</option>
                                    @else
                                        <option value="{{ $TypeProduct->code }}">{{ $TypeProduct->description }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectBrandProduct">@lang('vsat/newinventoryvsat.text_brandproduct')</label>
                            <select name="selectBrandProduct" id="selectBrandProduct" class="form-control" onchange="changebrand();" @if ( $infoInventory->available_id == 'S4' ) disabled="disabled" @endif>
                                @foreach ($listBrands as $brand)
                                    @if ($infoInventory->brand == $brand->code)
                                        <option value="{{ $brand->code }}" selected="selected">{{ $brand->description }}</option>
                                    @else
                                        <option value="{{ $brand->code }}">{{ $brand->description }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectModelProduct">@lang('vsat/newinventoryvsat.text_modelproduct')</label>
                            <select name="selectModelProduct" id="selectModelProduct" class="form-control" @if ( $infoInventory->available_id == 'S4' ) disabled="disabled" @endif>
                                @foreach ($listModels as $model)
                                    @if ($infoInventory->model == $model->code)
                                        <option value="{{ $model->code }}" selected="selected">{{ $model->description }}</option>
                                    @else
                                        <option value="{{ $model->code }}">{{ $model->description }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        @if (strlen($infoInventory->group_id) == 4)
                            <div class="col-sm-12 py-2" id="divGroupSelection">
                                <label for="selectGroupSelection">@lang('vsat/newinventoryvsat.texto_assignedgroup')</label>
                                <select name="selectGroupSelection" id="selectGroupSelection" class="form-control" onchange="changeentrygroup();" @if ( $infoInventory->available_id == 'S4' ) disabled="disabled" @endif>
                                    @foreach ($listGroups as $listGroup)
                                        @if ($infoInventory->group_id == $listGroup->code)
                                            <option value="{{ $listGroup->code }}" selected="selected">{{ $listGroup->description }}</option>
                                        @else
                                            <option value="{{ $listGroup->code }}">{{ $listGroup->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        @endif

                        <div class="col-sm-12 py-2">
                            <hr />
                        </div>
                        <div class="col-sm-12 py-2">
                            @lang('vsat/newinventoryvsat.text_anotations')
                            <input type="text" class="form-control" id="textAnotation" name="textAnotation" autocomplete="off" value="{{ $infoInventory->anotations }}" @if ( $infoInventory->available_id == 'S4' ) disabled="disabled" @endif>
                        </div>
                        <div class="col-sm-12 py-2">
                            @lang('vsat/newinventoryvsat.text_status')
                            <select class="form-control" id="selectStatus" name="selectStatus" @if ( $infoInventory->available_id == 'S4' ) disabled="disabled" @endif>
                                @foreach ($listAvailables as $available)
                                    @if ($infoInventory->available_id == $available->code)
                                        <option value="{{ $available->code }}" selected="selected">{{ $available->description }}</option>
                                    @else
                                        <option value="{{ $available->code }}">{{ $available->description }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        @foreach ($campusList as $campus)
                            <div class="col-sm-12 py-2">
                                {{ $campus->title }}
                                <input type="text" class="form-control" id="aditional_{{ $campus->code }}" name="aditional_{{ $campus->code }}" autocomplete="off" value="{{ $campus->description }}" @if ( $infoInventory->available_id == 'S4' ) disabled="disabled" @endif>
                            </div>
                        @endforeach

                        @if ( $infoInventory->available_id != 'S4' )
                            <div class="col-sm-12 py-4">
                                <button type="button" class="btn btn-primary btn-user btn-block" onclick="updateInventoryVsat();">
                                    @lang('vsat/newinventoryvsat.btn_update')
                                </button>
                            </div>
                        @endif
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script language="javascript">
        let modal_save = "{{ __('vsat/newinventoryvsat.modal_save')  }}";
    </script>
    <script src="/js/vsat/editinventoryvsat.js"></script>
@endsection
