@extends('layouts.dashboard')
@section('page_title', __('vsat/newstatusorder.title_page'))
@section('info_title', __('vsat/newstatusorder.title_page'))

@section('putButton')
    <a href="/vieworderservicevsat/{{ $orderCode }}" type="submit" class="btn btn-primary">
        <i class="fas fa-chevron-left"></i>&nbsp;&nbsp;
        {{ __('mycompany/showattach.btn_returnorder') }}
    </a>
@endsection

@section('content')
    <!-- modals -->
    <div class="dinamycSelector" id="customerSelector"></div>
    <div class="dinamycSelector" id="addressSelector"></div>
    <div class="dinamycSelector" id="planSelector"></div>
    <div class="dinamycSelector" id="itemSelector"></div>
    <div class="dinamycSelector" id="priceSelector"></div>

    @if(session('statusUpdate') == 'no')
        @include('partials._successDanger', ['message' => __('vsat/newstatusorder.alert_fail')])
    @endif

    <div class="row mb-4">
        <form id="formNewStatus" method="post" action="/savenewstatusfororder" enctype="multipart/form-data" onsubmit="return saveStatus();">
            <div class="col-xl-6 col-sm-12 mx-auto">
                <div class="card">
                    <div class="card-body">
                        @csrf
                        <input type="hidden" name="problems" id="problems" value='false'>
                        <div class="row">
                            <div class="col-sm-12 py-2">
                                <h4 class="card-title">{{ __('vsat/newstatusorder.subtitle_page') }}</h4>
                                {{ __('vsat/newstatusorder.text_statusnow') }}<span class="{{ $statusOrder->stylealert }}"><i class="fas fa-{{ $statusOrder->iconalert }}"></i> {{ $statusOrder->description }}</span>
                                <hr />
                            </div>

                            <div class="col-sm-6 py-2">
                                <label for="textOrdenCode">{{ __('vsat/newstatusorder.text_order') }}</label>
                                <input type="text" class="form-control" id="textOrdenCode" name="textOrdenCode" value="{{ $orderCode }}" readonly>
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="dateCreate">{{ __('vsat/newstatusorder.text_date') }}</label>
                                <input class="form-control" type="date" value="{{ date("Y-m-d") }}" id="dateCreate" name="dateCreate">
                            </div>
                            <div class="col-sm-12 py-2">
                                <label for="selectStatus">{{ __('vsat/newstatusorder.text_status')  }}</label>
                                <select class="form-control" id="selectStatus" name="selectStatus">
                                    <option value="---">{{ __('vsat/newstatusorder.select_option')  }}</option>
                                    @foreach ($listStatus as $item)
                                        <option value="{{ $item->code }}">{{ $item->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-12 py-2">
                                <label for="textAnotations">{{ __('vsat/newstatusorder.text_anotations')  }}</label>
                                <textarea class="form-control" id="textAnotations" name="textAnotations" maxlength="320"></textarea>
                            </div>
                            <div class="col-sm-12 py-12">
                                <label for="inputFile">{{ __('vsat/newstatusorder.text_attach') }}</label>
                                <input type="file" name="inputFile" id="inputFile">
                            </div>

                            <div class="col-sm-12 py-4">
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('vsat/newstatusorder.btn_save')  }}
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script language="javascript">
        let modalComplete = "{{ __('vsat/newstatusorder.modal_complete')  }}";
        let modalSaved = "{{ __('vsat/newstatusorder.modal_saved')  }}";
        let modal_complete_anotation = "{{ __('vsat/newstatusorder.modal_complete_anotation')  }}";
        let modal_sure = "{{ __('vsat/newstatusorder.modal_sure')  }}";
        let modal_cancel = "{{ __('vsat/newstatusorder.modal_cancel')  }}";
        let modal_title_sure = "{{ __('vsat/newstatusorder.modal_title_sure')  }}";
        let modal_text_1 = "{{ __('vsat/newstatusorder.modal_text_1')  }}";
        let modal_text_2 = "{{ __('vsat/newstatusorder.modal_text_2')  }}";
        let modal_left = "{{ __('vsat/newstatusorder.modal_left')  }}";
        let modal_days = "{{ __('vsat/newstatusorder.modal_days')  }}";
        let to_due = "{{ __('vsat/newstatusorder.to_due')  }}";
        
    </script>
    <script src="/js/vsat/newstatusorder.js"></script>
@endsection
