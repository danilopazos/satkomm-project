@extends('layouts.dashboard')
@section('page_title', __('vsat/editvsatplan.title_page'))
@section('info_title', __('vsat/editvsatplan.title_page'))

@section('content')

    @if (session('status'))
        @include('partials._successAlert', ['message' => __('listplanvsat.message_saveok')])
    @endif

    @php
        $isPostpaid = false;
        $isToken = false;
        $isAditional = false;

        if ($infoPlan->typeplan_id == 'PS'){
            $isPostpaid = true;

        }elseif ($infoPlan->typeplan_id == 'TK'){
            $isToken = true;

        }elseif ($infoPlan->typeplan_id == 'AD'){
            $isAditional = true;

        }
    @endphp

    <div class="row mb-4">
        <div class="col-xl-8 col-sm-12">
            <div class="card">
                <div class="card-body">

                    <form id="formSavePlan">
                    @csrf
                    <input type="hidden" name="codeVsatPlan" id="codeVsatPlan" value="{{ $infoPlan->code }}">
                    <input type="hidden" name="hiddenCodeTypePlan" id="hiddenCodeTypePlan" value="{{ $infoPlan->typeplan_id }}">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">@lang('vsat/editvsatplan.subtitle_information')</h5>
                        </div>

                    <!-- RECURRING INFORMATION -->
                        <div class="col-sm-6 py-2">
                            <label for="selectBand">@lang('vsat/editvsatplan.text_typeband')</label>
                            <select name="selectBand" id="selectBand" class="form-control" disabled>
                                @foreach ($vsatRecords as $vsatRecord)
                                    @if ($infoPlan->vsatrecord_id == $vsatRecord->code)
                                        <option value="{{ $vsatRecord->code }}" selected="selected">{{ $vsatRecord->description }}</option>
                                    @else
                                        <option value="{{ $vsatRecord->code }}">{{ $vsatRecord->description }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypePlan">@lang('vsat/editvsatplan.text_typeplan')</label>
                            <select name="selectTypePlan" id="selectTypePlan" class="form-control" onchange="changeTypePlan();" disabled>
                                @foreach ($typePlans as $typePlan)
                                    @if ($infoPlan->typeplan_id == $typePlan->code)
                                        <option value="{{ $typePlan->code }}" selected="selected">{{ $typePlan->description }}</option>
                                        @php $codePlan = $typePlan->code @endphp
                                    @else
                                        <option value="{{ $typePlan->code }}">{{ $typePlan->description }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <input type='hidden' name='hiddenCodePlan' id='hiddenCodePlan' value='{{ $codePlan }}'>
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="textPlanName">@lang('vsat/editvsatplan.text_planname')</label>
                            <input type="text" class="form-control" id="textPlanName" name="textPlanName" placeholder="" autocomplete="off" value="{{ $infoPlan->description }}" maxlength="38">
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="textAnotationPlan">@lang('vsat/editvsatplan.text_plannotes')</label>
                            <input type="text" class="form-control" id="textAnotationPlan" name="textAnotationPlan" placeholder="" autocomplete="off" value="{{ $infoPlan->anotations }}" maxlength="198">
                        </div>
                    <!-- END RECURRING INFORMATION -->


                    <!-- FORM FOR TOKEN INFORMATION -->
                        @if($isToken)
                            <div class="col-sm-4 py-2">
                                <label for="textAnytime">@lang('vsat/editvsatplan.text_anytime')</label>
                                <input type="text" class="form-control" id="textAnytime" name="textAnytime" placeholder="" autocomplete="off" value="{{ $infoPlan->anytime }}">
                            </div>
                            <div class="col-sm-2 py-2">
                                <label for="selectAnytime">@lang('vsat/editvsatplan.text_typeunits')</label>
                                <select name="selectAnytime" id="selectAnytime" class="form-control">
                                    @foreach ($techUnits as $techUnit)
                                        @if ($infoPlan->typeanytime == $techUnit->code)
                                            <option value="{{ $techUnit->code }}" selected="selected">{{ $techUnit->description }}</option>
                                        @else
                                            <option value="{{ $techUnit->code }}">{{ $techUnit->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                        @elseif($isPostpaid)
                            <div class="col-sm-4 py-2" id="div_download_1">
                                <label for="textDownload">@lang('vsat/editvsatplan.text_donwload')</label>
                                <input type="text" class="form-control" id="textDownload" name="textDownload" placeholder="" autocomplete="off" value="{{ $infoPlan->download }}">
                            </div>
                            <div class="col-sm-2 py-2" id="div_download_2">
                                <label for="selectDownload">@lang('vsat/editvsatplan.text_typespeed')</label>
                                <select name="selectDownload" id="selectDownload" class="form-control">
                                    @foreach ($techSpeeds as $techSpeed)
                                        @if ($infoPlan->typedownload == $techSpeed->code)
                                            <option value="{{ $techSpeed->code }}" selected="selected">{{ $techSpeed->description }}</option>
                                        @else
                                            <option value="{{ $techSpeed->code }}">{{ $techSpeed->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-4 py-2" id="div_upload_1">
                                <label for="textUpload">@lang('vsat/editvsatplan.text_upload')</label>
                                <input type="text" class="form-control" id="textUpload" name="textUpload" placeholder="" autocomplete="off" value="{{ $infoPlan->upload }}">
                            </div>
                            <div class="col-sm-2 py-2" id="div_upload_2">
                                <label for="selectUpload">@lang('vsat/editvsatplan.text_typespeed')</label>
                                <select name="selectUpload" id="selectUpload" class="form-control">
                                    @foreach ($techSpeeds as $techSpeed)
                                        @if ($infoPlan->typeupload == $techSpeed->code)
                                            <option value="{{ $techSpeed->code }}" selected="selected">{{ $techSpeed->description }}</option>
                                        @else
                                            <option value="{{ $techSpeed->code }}">{{ $techSpeed->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4 py-2">
                                <label for="textAnytime">@lang('vsat/editvsatplan.text_anytime')</label>
                                <input type="text" class="form-control" id="textAnytime" name="textAnytime" placeholder="" autocomplete="off" value="{{ $infoPlan->anytime }}">
                            </div>
                            <div class="col-sm-2 py-2">
                                <label for="selectAnytime">@lang('vsat/editvsatplan.text_typeunits')</label>
                                <select name="selectAnytime" id="selectAnytime" class="form-control">
                                    @foreach ($techUnits as $techUnit)
                                        @if ($infoPlan->typeanytime == $techUnit->code)
                                            <option value="{{ $techUnit->code }}" selected="selected">{{ $techUnit->description }}</option>
                                        @else
                                            <option value="{{ $techUnit->code }}">{{ $techUnit->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4 py-2" id="div_early_1">
                                <label for="textEarly">@lang('vsat/editvsatplan.text_early')</label>
                                <input type="text" class="form-control" id="textEarly" name="textEarly" placeholder="" autocomplete="off" value="{{ $infoPlan->earlymorning }}">
                            </div>
                            <div class="col-sm-2 py-2" id="div_early_2">
                                <label for="selectEarly">@lang('vsat/editvsatplan.text_typeunits')</label>
                                <select name="selectEarly" id="selectEarly" class="form-control">
                                    @foreach ($techUnits as $techUnit)
                                        @if ($infoPlan->typeearlymorning == $techUnit->code)
                                            <option value="{{ $techUnit->code }}" selected="selected">{{ $techUnit->description }}</option>
                                        @else
                                            <option value="{{ $techUnit->code }}">{{ $techUnit->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4 py-2" id="div_total_1">
                                <label for="textTotal">@lang('vsat/editvsatplan.text_total')</label>
                                <input type="text" class="form-control" id="textTotal" name="textTotal" placeholder="" autocomplete="off" value="{{ $infoPlan->total }}">
                            </div>
                            <div class="col-sm-2 py-2" id="div_total_2">
                                <label for="selectTotal">@lang('vsat/editvsatplan.text_typeunits')</label>
                                <select name="selectTotal" id="selectTotal" class="form-control">
                                    @foreach ($techUnits as $techUnit)
                                        @if ($infoPlan->typetotal == $techUnit->code)
                                            <option value="{{ $techUnit->code }}" selected="selected">{{ $techUnit->description }}</option>
                                        @else
                                            <option value="{{ $techUnit->code }}">{{ $techUnit->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4 py-2" id="div_cir_1">
                                <label for="textCir">@lang('vsat/editvsatplan.text_cir')</label>
                                <input type="text" class="form-control" id="textCir" name="textCir" placeholder="" autocomplete="off" value="{{ $infoPlan->cir }}">
                            </div>
                            <div class="col-sm-2 py-2" id="div_cir_2">
                                <label for="selectCir">@lang('vsat/editvsatplan.text_typespeed')</label>
                                <select name="selectCir" id="selectCir" class="form-control">
                                    @foreach ($techSpeeds as $techSpeed)
                                        @if ($infoPlan->typecir == $techSpeed->code)
                                            <option value="{{ $techSpeed->code }}" selected="selected">{{ $techSpeed->description }}</option>
                                        @else
                                            <option value="{{ $techSpeed->code }}">{{ $techSpeed->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                        @endif
                        
                        <div class="col-sm-12 py-2">
                            <hr />
                            <h5 class="card-title">@lang('vsat/newvsatplan.subtitle_aboutcosts')</h5>
                        </div>

                        <div class="col-sm-6 py-2" id="div_cir_2">
                            <label for="selectCir">@lang('vsat/newvsatplan.text_cost')</label>
                            <input class="form-control" id="planCost" name="planCost" autocomplete="off" placeholder="$" value="{{ $infoPlan->usdcost }}">
                        </div>
                        <div class="col-sm-6 py-2" id="div_cir_2">
                            <label for="commodeCost">@lang('vsat/newvsatplan.text_commode')</label>
                            <input class="form-control" id="commodeCost" name="commodeCost" autocomplete="off" placeholder="$" value="{{ $infoPlan->commodecost }}">
                        </div>


                        <div class="col-sm-12 py-4">
                            <button type="button" class="btn btn-primary btn-user btn-block" onclick="updatePlan();">
                                @lang('vsat/editvsatplan.btn_saveplan')
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let modal_update = "{{ __('vsat/newvsatplan.modal_update')  }}";
        let modal_complete = "{{ __('vsat/newvsatplan.modal_complete')  }}";
        
    </script>
    <script src="/js/vsat/newvsatplan.js"></script>
@endsection
