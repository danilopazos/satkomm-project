@extends('layouts.dashboard')
@section('page_title', __('vsat/newinventoryvsat.title_pagestart'))
@section('info_title', __('vsat/newinventoryvsat.title_pagestart'))

@section('content')

    <div class="row mb-4">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">

                    <form method="post" action="/saveinventoryvsat">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">{{ __('vsat/newinventoryvsat.subtitle_itemdetails')  }}</h5>
                        </div>

                        <div class="col-sm-12 py-2">
                            <table width="100%" border="1" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td width="5%">{{ __('vsat/newinventoryvsat.table_number')  }}</td>
                                        <td width="15%">{{ __('vsat/newinventoryvsat.table_description')  }}</td>
                                        <td width="15%">{{ __('vsat/newinventoryvsat.table_anotation')  }}</td>
                                        <td width="15%">{{ __('vsat/newinventoryvsat.table_status')  }}</td>
                                        <td width="20%">{{ __('vsat/newinventoryvsat.table_group')  }}</td>
                                        <td width="30%">{{ __('vsat/newinventoryvsat.table_info')  }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i = 1; $i <= $info->textQtyProducts; $i++)
                                        <tr>
                                            <td>#{{ $i }}</td>
                                            <td>{{ $brand->brand." - ".$brand->model }}</td>
                                            <td><input type="text" class="form-control" id="textNotes_{{ $i }}" name="textNotes_{{ $i }}" autocomplete="off" placeholder="{{ __('vsat/newinventoryvsat.table_anotation')  }}"></td>
                                            <td>
                                                <select class="form-control" id="selectStatus_{{ $i }}" name="selectStatus_{{ $i }}">
                                                    @foreach ($listStatus as $item)
                                                        <option value="{{ $item->code }}">{{ $item->description }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>{{ $nameGroup }}</td>
                                            <td>
                                                @foreach ($campusTexts as $campusText)
                                                    <input type="text" class="form-control" id="textNotes_{{ $i }}" name="textAditional_{{ $i }}_{{ $campusText->code }}" autocomplete="off" placeholder="{{ $campusText->title }}"><br />
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>


                        <div class="col-sm-12 py-4">
                            <input type="hidden" name="codeGroup" id="codeGroup" value="{{ $code_group }}">
                            <input type="hidden" name="codeBrand" id="codeBrand" value="{{ $codeBrand }}">
                            <input type="hidden" name="codeModel" id="codeModel" value="{{ $codeModel }}">
                            <input type="hidden" name="qtyElements" id="qtyElements" value="{{ $info->textQtyProducts }}">
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                {{ __('vsat/newinventoryvsat.btn_saveitems')  }}
                            </button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="js/vsat/newinventoryvsat.js"></script>
@endsection
