@extends('layouts.dashboard')
@section('page_title', __('vsat/viewcosts.title_page'))
@section('info_title', __('vsat/viewcosts.title_page').$orderid)

@section('putButton')
    <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-th"></i>&nbsp;&nbsp;
            <span class="text">{{ __('vsat/viewcosts.btn_viewoptions') }}</span>
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="/vsat/showassigncost/{{ $orderid }}"><i class="fas fa-plus"></i> {{ __('vsat/viewcosts.btn_startnow') }}</a>
            
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/vieworderservicevsat/{{ $orderid }}"><i class="fas fa-angle-left"></i> {{ __('vsat/viewcosts.btn_back') }}</a>
        </div>
    </div>
@endsection

@section('content')

    @if(session('status'))
        @include('partials._successAlert', ['message' => __('vsat/viewcosts.alert_saved')])
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ __('vsat/viewcosts.subtitle_page') }}</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                @if($costsList->count() == 0)
                    @include('partials._noresults',[
                        'title' => __('vsat/viewcosts.text_noresults'),
                        'text' => __('vsat/viewcosts.text_calltoaction'),
                        'textBtn' => __('vsat/viewcosts.btn_startnow'),
                        'urlBtn' => '/vsat/showassigncost/{{ $orderid }}'
                    ])
                @else
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr style="background-color: #f8f9f9;">
                            <th width="10%">{{ __('vsat/viewcosts.table_date') }}</th>
                            <th width="10%">{{ __('vsat/viewcosts.table_provider') }}</th>
                            <th width="10%">{{ __('vsat/viewcosts.table_typedoc') }}</th>
                            <th width="10%">{{ __('vsat/viewcosts.table_numberdoc') }}</th>
                            <th>{{ __('vsat/viewcosts.table_description') }}</th>
                            <th width="10%">{{ __('vsat/viewcosts.table_entry') }}</th>
                            <th width="10%">{{ __('vsat/viewcosts.table_exit') }}</th>
                            <th width="10%">{{ __('vsat/viewcosts.table_document') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                            @php ($totalEntry = 0)
                            @php ($totalCost = 0)
                            @foreach ($costsList as $cost)
                                <tr>
                                    <td>{{ $cost->created_at->format("d")." ".__('vsat/orderconsumptions.month_'.$cost->created_at->format("m"))." ".$cost->created_at->format("Y") }}</td>
                                    <td>{{ $cost->company }}</td>
                                    <td>{{ $cost->document }}</td>
                                    <td>{{ $cost->numberdocument }}</td>
                                    <td>
                                        {{ $cost->typeCost.' : '.$cost->anotations }}<br />
                                        <label class="text-gray-500">{{ __('vsat/viewcosts.text_createdby').$cost->username.' '.$cost->userlastname }}</label>
                                    </td>
                                    @if($cost->typerecord == "EN")
                                        <td align="right">
                                            @if($cost->iso4217 == 'USD')
                                                @php ($subtotal = $cost->amount)
                                                {{ $cost->symbol.' '.$subtotal.' '.$cost->iso4217 }}
                                            @else
                                                @php ($subtotal = round($cost->amount / $cost->exchange, 2))
                                                {{ '$ '.' '.number_format($subtotal, 2).' '.' USD' }}
                                            @endif
                                        </td>
                                        <td align="right">-</td>
                                        @php ($totalEntry += $subtotal)

                                    @else
                                        <td align="right">-</td>
                                        <td align="right">
                                            @if($cost->iso4217 == 'USD')
                                                @php ($subtotal = $cost->amount)
                                                {{ $cost->symbol.' -'.number_format($subtotal, 2).' '.$cost->iso4217 }}
                                            @else
                                                @php ($subtotal = round($cost->amount / $cost->exchange, 2))
                                                {{ '$ -'.$subtotal.' USD' }}
                                            @endif
                                        </td>
                                        @php ($totalCost += $subtotal)
                                    @endif

                                    <td><a href="/vsat/downloadcostdocument/{{ $orderid }}/{{ $cost->attach }}" target="_blank"><i class="fas fa-download"></i> {{ __('vsat/viewcosts.href_download') }}</a></td>
                                </tr>
                            @endforeach

                        </tbody>
                        @php ($totalProfit = round($totalEntry-$totalCost, 2))
                        <tfooter>
                            <tr style="font-weight: bold; background-color: #f8f9f9;">
                                <td colspan="5">{{ __('vsat/viewcosts.text_totals') }}</td>
                                <td align="right"><label class="text-primary">{{ '$ '.number_format($totalEntry, 2).' USD' }}</label></td>
                                <td align="right"><label class="text-danger">{{ '$ -'.number_format($totalCost, 2).' USD' }}</label></td>
                                <td></td>
                            </tr>
                            <tr style="font-weight: bold; background-color: #f8f9f9;">
                                <td colspan="5">{{ __('vsat/viewcosts.text_profit') }}</td>
                                <td colspan="2" align="right">
                                    @if($totalProfit > 0)
                                        <span class="text-primary">{{ '$ '.$totalProfit.' USD' }}</span>
                                    @else
                                        <span class="text-danger">{{ '$ '.$totalProfit.' USD' }}</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        </tfooter>
                    </table>
                @endif
            </div>
        </div>
    </div>

@endsection