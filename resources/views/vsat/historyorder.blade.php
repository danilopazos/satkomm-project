@extends('layouts.dashboard')
@section('page_title', __('vsat/historyorder.title_page'))
@section('info_title', __('vsat/historyorder.title_page'))

@section('putButton')
    <a href="{{ route('vsat.order.ViewOrderService', $orderId) }}" type="submit" class="btn btn-primary">
        <i class="fas fa-chevron-left"></i>&nbsp;&nbsp;
        @lang('mycompany/showattach.btn_returnorder')
    </a>
@endsection

@section('content')

    <div class="row mb-4">
        <div class="col-xl-6 col-sm-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            @if($listHistory->count() > 0)
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul class="timeline">
                                                @foreach($listHistory as $item)
                                                    <li>
                                                        <span class="text-{{ $item->colortext }}" @if($item->weighttext) style="font-weight: bold;" @endif>
                                                            <i class="fas fa-{{ $item->icon }}"></i> {{ ucwords($item->description) }}
                                                        </span>
                                                        
                                                        <label class="float-right">
                                                            {{ $item->created_at->diffForHumans() }}
                                                        </label>
                
                                                        <p style="margin-left: 15px; margin-top:10px;" >
                                                            @if(strlen($item->anotations) > 0)
                                                                @lang('vsat/historyorder.text_notes', ['notes' => $item->anotations])
                                                            @endif
                
                                                            @if(strlen($item->attach))
                                                                <br />
                                                                <a href="{{ route('vsat.order.downloadAttach', [$orderId, $item->code]) }}">
                                                                <i class="fas fa-download"></i> @lang('vsat/historyorder.href_download')</a>
                                                                <br />
                                                            @endif
                                                        </p>
                                                        <p align="right">
                                                            @lang('vsat/historyorder.table_by', ['name' => $item->name, 'lastname' => $item->lastname]) <img class="img-profile rounded-circle" src="{{ $item->imgprofile }}" width="20px">
                                                            <hr />
                                                        </p>
                                                    </li>
                                                @endforeach
                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @else
                                @include('partials._noresults',[
                                    'title' => __('vsat/historyorder.text_noresults'),
                                    'text' => __('vsat/historyorder.text_calltoaction'),
                                    'textBtn' => __('vsat/historyorder.btn_newplan'),
                                    'urlBtn' => route('vsat.order.ViewOrderService', $orderId)
                                ])
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
