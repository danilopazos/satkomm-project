@extends('layouts.dashboard')
@section('page_title', __('vsat/discounts.title_page'))
@section('info_title', __('vsat/discounts.title_page'))

@section('putButton')
<a href="" class="btn btn-primary">
    <i class="fas fa-chevron-left"></i>&nbsp;&nbsp;
    {{ __('vsat/discounts.btn_returnorder') }}
</a>
@endsection

@section('content')

    @if(session('status'))
        @include('partials._successAlert', [
            'message' => __('vsat/discounts.alert_saved')
        ])
    @endif

    <div class="row">
        <div class="col-xl-6 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('vsat.order.attachDiscount') }}" method="post" onsubmit="return validateForm();" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6 py-2">
                            <label for="textOrderCode">@lang('vsat/discounts.text_ordservice')</label>
                            <input type="text" class="form-control" name="textOrderCode" id="textOrderCode" value="{{ $order->id }}" readonly>
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="textAnotations">@lang('vsat/discounts.text_anotations')</label>
                            <textarea class="form-control" name="textAnotations" id="textAnotations"></textarea>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textDiscount">@lang('vsat/discounts.text_discounts')</label>
                            <input type="text" class="form-control" name="textDiscount" id="textDiscount" autocomplete="off">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="dateStart">@lang('vsat/discounts.text_dateStart')</label>
                            <input type="date" class="form-control" name="dateStart" id="dateStart" autocomplete="off">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="dateEnd">@lang('vsat/discounts.text_dateEnd')</label>
                            <input type="date" class="form-control" name="dateEnd" id="dateEnd" autocomplete="off">
                        </div>
                        <div class="col-sm-6 py-2">
                            <div class="form-group">
                                <label for="fileAttach">@lang('vsat/discounts.text_attach')</label>
                                <input type="file" class="form-control-file" id="fileAttach" name="fileAttach">
                            </div>
                        </div>
                        <div class="col-sm-12 py-2">
                            <button type="submit" class="btn btn-block btn-primary">
                                @lang('vsat/discounts.btn_save')
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script language="javascript">
    let modalComplete = "{{ __('vsat/discounts.modal_complete') }}";
</script>
<script src="/js/vsat/attachdiscount.js"></script>
@endsection