@extends('layouts.dashboard')
@section('page_title', __('mycompany/showattach.title_page'))
@section('info_title', __('mycompany/showattach.title_page'))

@section('putButton')
<a href="/vieworderservicevsat/{{ $oderCode }}" type="submit" class="btn btn-primary">
    <i class="fas fa-chevron-left"></i>&nbsp;&nbsp;
    {{ __('mycompany/showattach.btn_returnorder') }}
</a>
@endsection

@section('content')
    
    @if (session('status'))
        @include('partials._successAlert', ['message' => __('mycompany/showattach.save_ok')])
    @endif

    <div class="row mb-4">
        <div class="col-xl-7 col-sm-12 mx-auto py-2">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="/orderservice/uploaddocument" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="orderCode" id="orderCode" value="{{ $oderCode }}">
                        <div class="row">
                            <div class="col-sm-12 py-2">
                                <h5 class="card-title">{{ __('mycompany/showattach.subtitle_page')  }}</h5>
                            </div>

                            <div class="col-sm-12    py-2">
                                <label for="typeAttach">{{ __('mycompany/showattach.text_typeattach')  }}</label>
                                <select class="form-control" name="typeAttach" id="typeAttach">
                                    <option value="---">{{ __('mycompany/showattach.option_choose')  }}</option>
                                    @foreach($listType as $type)
                                        <option value="{{ $type->code }}">{{ $type->description }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-12 py-2">
                                <label for="textAnotations">{{ __('mycompany/showattach.text_notes')  }}</label>
                                <input type="text" class="form-control" id="textAnotations" name="textAnotations" autocomplete="off" maxlength="240">
                            </div>

                            <div class="col-sm-12 py-2">
                                <div class="form-group">
                                    <label for="fileAttach">{{ __('mycompany/showattach.text_attach')  }}</label>
                                    <input type="file" class="form-control-file" id="fileAttach" name="fileAttach">
                                </div>
                            </div>

                            <div class="col-sm-12 py-2">
                                <button type="submit" class="btn btn-primary btn-block" onclick="return uploadAttach();">
                                    <i class="fas fa-upload"></i>&nbsp;&nbsp;
                                    {{ __('mycompany/showattach.btn_attach') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script language='javascript'>
        let notFiles = "{{ __('mycompany/showattach.modal_message') }}";
    </script>
    <script src='/js/vsat/attachcontracts.js'></script>
@endsection