@extends('layouts.dashboard')
@section('page_title', __('vsat/neworderservicevsat.title_page_view'))
@section('info_title', __('vsat/neworderservicevsat.title_page_view'))

@section('putButton')
    <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-th"></i>&nbsp;&nbsp;
                @lang('vsat/neworderservicevsat.btn_moreoptions')
        </button>
        <div class="dropdown-menu">
            <h6 class="dropdown-header">
                @lang('vsat/neworderservicevsat.option_aboutorderservice')
            </h6>
            <a class="dropdown-item" href="{{ route('vsat.order.showEditOrderService', [$infoOrder->code]) }}">
                <i class="fas fa-pen-square"></i> @lang('vsat/neworderservicevsat.btn_editorder')
            </a>

            @if($infoOrder->adminValidation != 'PV' && $infoOrder->adminValidation != 'DS')
                <a class="dropdown-item" href="{{ route('vsat.order.newStatus', [$infoOrder->code]) }}">
                    <i class="fas fa-flag"></i> @lang('vsat/neworderservicevsat.btn_updatestatus')
                </a>
                <a class="dropdown-item" href="{{ route('vsat.order.addDiscount', [$infoOrder->code]) }}">
                    <i class="fas fa-tags"></i> @lang('vsat/neworderservicevsat.btn_adddiscount')
                </a>
                <a class="dropdown-item" href="{{ route('vsat.order.makeUpgrade', [$infoOrder->code]) }}">
                    <i class="fas fa-exchange-alt"></i> @lang('vsat/neworderservicevsat.btn_migrateservice')
                </a>
            @endif
            
            <a class="dropdown-item" href="{{ route('vsat.order.showCosts', [$infoOrder->code]) }}">
                <i class="fas fa-chart-line"></i> @lang('vsat/neworderservicevsat.text_checkcosts')
            </a>
            <a class="dropdown-item" href="{{ route('vsat.order.showHistory', [$infoOrder->code]) }}">
                <i class="fas fa-list-ol"></i> @lang('vsat/neworderservicevsat.option_historyorder')
            </a>

            @if($listDocumentsAttached->count() > 0)
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">
                    @lang('vsat/neworderservicevsat.option_documentsattachedcustomer')
                </h6>

                @foreach($listDocumentsAttached as $documentCustomer)
                    <a class="dropdown-item" href="{{ route('mycompany.download', [$documentCustomer->document_attach]) }}" target="_blank">
                        <i class="fas fa-download"></i> {{ $documentCustomer->description }}
                        @if(strlen($documentCustomer->anotations) > 0)
                            : {{ $documentCustomer->anotations }}
                        @endif
                </a>
                @endforeach

            @endif
            <a class="dropdown-item" href="{{ route('mycompany.reviewCompany', [$infoOrder->companyCode]) }}">
                <i class="fas fa-building"></i> @lang('vsat/neworderservicevsat.href_viewcompany')
            </a>

            @if($listDocumentsForOrder->count() > 0)
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">
                    @lang('vsat/neworderservicevsat.option_documentsattachedorder')
                </h6>

                @foreach($listDocumentsForOrder as $documentOrder)
                    <a class="dropdown-item" href="{{ route('vsat.downloadDocument', [$infoOrder->code, $documentOrder->attach]) }}" target="_blank">
                        <i class="fas fa-download"></i> {{ $documentOrder->description }}
                        @if(strlen($documentOrder->anotations) > 0)
                            : {{ $documentOrder->anotations }}
                        @endif
                    </a>
                @endforeach

            @endif

            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header">
                @lang('vsat/neworderservicevsat.option_attachdocuments')
            </h6>
            <a class="dropdown-item" href="{{ route('mycompany.mycustomer.showPage', [$infoOrder->companyCode]) }}">
                <i class="fas fa-upload"></i> @lang('vsat/neworderservicevsat.option_attachforcustomer')
            </a>
            <a class="dropdown-item" href="{{ route('vsat.order.showAttach', [$infoOrder->code]) }}">
                <i class="fas fa-upload"></i> @lang('vsat/neworderservicevsat.option_attachfororder')
            </a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('vsat.order.newOrdenServicevsat') }}">
                <i class="fas fa-plus"></i> @lang('vsat/neworderservicevsat.btn_neworder')
            </a>
        </div>
    </div>
@endsection

@section('content')

    <input type="hidden" name="orderServiceCode" id="orderServiceCode" value="{{ $infoOrder->code }}">

    @if(session('statusUpdate'))
        @include('partials._successAlert', ['message' => __('vsat/neworderservicevsat.text_status_updated')])
    @endif
    @if (session('status'))
        @include('partials._successAlert', ['message' => __('vsat/neworderservicevsat.saved_attach')])
    @endif
    @if ($infoOrder->adminValidation == "DS")
        @include('partials._dangerAlert', ['message' => __('vsat/neworderservicevsat.text_orderdisapproved')])
    @endif

    <div class="row">
        <!-- About documents -->
        <div class="col-xl-9 col-md-12 mx-auto">

            @if(auth()->user()->usertype_id == 'ADSY' and $infoOrder->adminValidation == 'PV' or auth()->user()->usertype_id == 'ALLS' and $infoOrder->adminValidation == 'PV')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-danger py-4" role="alert">
                            <h5 class="card-title text-gray-900 font-weight-bold">
                                <i class="fas fa-tasks"></i> @lang('vsat/neworderservicevsat.title_validation')
                            </h5>
                            
                            <p style="margin-left: 20px;">
                                <?php
                                    $failDocuments = false;
                                    foreach($listDocumentsImportants as $documents){
                                        $found = false; 
                                        foreach($listDocumentsAttached as $attached){
                                            
                                            if($documents->code == $attached->typeattach_id){
                                                $found = true;
                                                if($documents->validate_end){
                                                    //valida vencimiento
                                                    if($attached->date_expedition == null){
                                                        $expirationDocument = null;
                                                        $days_due = null;
                                                        
                                                    }else{
                                                        $expirationDocument = date("Y-m-d", strtotime($attached->date_expedition."+ ".$timeToDue." months"));
                                                        $now = new DateTime(date('Y-m-d'));
                                                        $due = new DateTime($expirationDocument);
                                                        $days_due = $due->diff($now)->format("%a");

                                                    }
                                                    if($expirationDocument > date('Y-m-d')){
                                                        //todo bien
                                                        ?>
                                                            <label class="text-success">
                                                                <i class="fas fa-check-square"></i>
                                                            </label>
                                                            <label class="text-gray-800">
                                                                <b style="text-decoration: underline;">{{ $documents->description }}</b>@lang('vsat/neworderservicevsat.text_according')
                                                            </label>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <a href="{{ route('mycompany.download', [$attached->document_attach]) }}">
                                                                <i class="fas fa-download"></i> @lang('vsat/neworderservicevsat.href_downloadattach')
                                                            </a>
                                                            <br />
                                                        <?php
                                                    }else{
                                                        //documento vencido
                                                        if($days_due != null){
                                                            $failDocuments = true;
                                                            ?>
                                                                <label class="text-warning">
                                                                    <i class="fas fa-exclamation-triangle"></i>
                                                                </label>
                                                                @lang('vsat/neworderservicevsat.text_thedocument')<b style="text-decoration: underline;">{{ $documents->description }}</b> @lang('vsat/neworderservicevsat.text_due') {{ $days_due }} @lang('vsat/neworderservicevsat.text_daysdue')
                                                                &nbsp;&nbsp;&nbsp;
                                                                <a href="{{ route('mycompany.mycustomer.showPage', [$infoOrder->companyCode]) }}">
                                                                    <i class="fas fa-upload"></i> @lang('vsat/neworderservicevsat.href_attach')
                                                                </a>
                                                                &nbsp;&nbsp;&nbsp; <a href="{{ route('mycompany.download', [$attached->document_attach]) }}">
                                                                    <i class="fas fa-download"></i> @lang('vsat/neworderservicevsat.href_downloadattach')
                                                                </a>
                                                                <br />
                                                            <?php
                                                        }else{
                                                            //sin fecha de expedicion
                                                            $failDocuments = true;
                                                            ?>
                                                                <label class="text-warning">
                                                                    <i class="fas fa-exclamation-triangle"></i>
                                                                </label>
                                                                {{ __('vsat/neworderservicevsat.text_thedocument') }}<b style="text-decoration: underline;">{{ $documents->description }}</b>@lang('vsat/neworderservicevsat.text_notime')
                                                                &nbsp;&nbsp;&nbsp;
                                                                <a href="{{ route('mycompany.mycustomer.showPage', [$infoOrder->companyCode]) }}">
                                                                    <i class="fas fa-upload"></i> @lang('vsat/neworderservicevsat.href_attach')
                                                                </a>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <a href="{{ route('mycompany.download', [$attached->document_attach]) }}">
                                                                    <i class="fas fa-download"></i> @lang('vsat/neworderservicevsat.href_downloadattach')
                                                                </a>
                                                                <br />
                                                            <?php
                                                        }

                                                    }
                                                }else{
                                                    //NUNCA VENCE
                                                    ?>
                                                        <label class="text-success">
                                                            <i class="fas fa-check-square"></i>
                                                        </label>
                                                        <label class="text-gray-800"><b style="text-decoration: underline;">{{ $documents->description }}</b>@lang('vsat/neworderservicevsat.text_according')</label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <a href="{{ route('mycompany.download', [$attached->document_attach]) }}">
                                                            <i class="fas fa-download"></i> @lang('vsat/neworderservicevsat.href_downloadattach')
                                                        </a>
                                                        <br />
                                                    <?php
                                                }

                                            }
                                        }
                                        if(!$found){
                                            //NO SE ENCONTRÓ ningún documento
                                            $failDocuments = true;
                                            ?>
                                                <label class="text-danger">
                                                    <i class="fas fa-window-close"></i>
                                                </label>
                                                <label class="text-danger">@lang('vsat/neworderservicevsat.text_notfound') <b style="text-decoration: underline;">{{ $documents->description }}</b>.</label>
                                                &nbsp;&nbsp;&nbsp; 
                                                <a href="{{ route('mycompany.mycustomer.showPage', [$infoOrder->companyCode]) }}">
                                                    <i class="fas fa-upload"></i> @lang('vsat/neworderservicevsat.href_attach')
                                                </a>
                                                <br />
                                            <?php

                                        }

                                    }
                                ?>
                            </p>

                            <!-- Indicador a true, solo si es PS -->
                            @php 
                                $validatePS = false;
                                foreach ($listPlans as $item) {
                                    if ($item->typeplan_id == "PS") {
                                        $validatePS = true;
                                        break;
                                    }
                                } 
                            @endphp
                            
                            @if ($validatePS and $infoOrder->contractMonths != 12 and $infoOrder->contractMonths != 6)
                                @php 
                                    $failDocuments = true; 
                                @endphp
                                <h5 class="card-title text-gray-900">
                                    <b><i class="fas fa-exclamation-triangle"></i> @lang('vsat/neworderservicevsat.text_aditionalanotations')</b>
                                </h5>
                                <p style="margin-left: 20px;">
                                    <label class="text-danger"><i class="fas fa-window-close"></i>
                                        @lang('vsat/neworderservicevsat.text_optionaditional_1') <b style="text-decoration: underline;">@lang('vsat/neworderservicevsat.text_optionaditional_2')</b>
                                    </label>
                                    <mark class="text-danger">
                                        @lang('vsat/neworderservicevsat.text_termtyped', ['month' => $infoOrder->contractMonths])
                                    </mark>
                                    <br />
                                </p>
                            @endif

                            <hr />
                            @if($failDocuments)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="checkWithProblems" name="checkWithProblems">
                                    <label class="form-check-label text-gray-800" for="checkWithProblems">
                                        <b style="text-decoration: underline;">@lang('vsat/neworderservicevsat.checkbox_agree_1')</b> @lang('vsat/neworderservicevsat.checkbox_agree_2') <b style="text-decoration: underline;">@lang('vsat/neworderservicevsat.checkbox_agree_3')</b> @lang('vsat/neworderservicevsat.checkbox_agree_4')
                                    </label>
                                </div>
                            @endif

                            <div class="form-check mb-4">
                                <input class="form-check-input" type="checkbox" id="checkReview" name="checkReview">
                                <label class="form-check-label text-gray-800" for="checkReview">
                                    <b style="text-decoration: underline;">@lang('vsat/neworderservicevsat.checkbox_agree_5')</b> @lang('vsat/neworderservicevsat.checkbox_agree_6')
                                </label>
                            </div>
                            <button class="btn btn-primary" @if($failDocuments) onclick="approveOrder('withErrors');" @else onclick="approveOrder('withoutErrors');" @endif>
                                <i class="fas fa-check"></i>&nbsp;&nbsp;@lang('vsat/neworderservicevsat.btn_approve')
                            </button>
                            <button class="btn btn-danger" onclick="disapproveOrder();">
                                <i class="fas fa-times"></i>&nbsp;&nbsp;@lang('vsat/neworderservicevsat.btn_disapprove')
                            </button>
                            <a href="{{ route('mycompany.reviewCompany', [$infoOrder->companyCode]) }}" class="btn btn-secondary">
                                <i class="fas fa-building"></i>&nbsp;&nbsp;@lang('vsat/neworderservicevsat.btn_checkcompany')
                            </a>
                        </div>
                    </div>
                </div>
            @elseif($infoOrder->adminValidation == 'PV')
                @include('partials._warningAlert', ['message' => __('vsat/neworderservicevsat.message_waitvalidation')])
            @endif

            @if($docsImportantsForOrder->count() > $listDocumentsForOrder->count() )
                <div class="alert alert-danger py-4" role="alert">
                    <h5 class="card-title text-gray-900">
                        <b><i class="fas fa-tasks"></i> @lang('vsat/neworderservicevsat.text_alert_notfound')</b>
                    </h5>
                    <p style="margin-left: 20px;">
                        <?php
                        foreach($docsImportantsForOrder as $documentImportant){
                            $found = false;
                            foreach($listDocumentsForOrder as $documentOrder){
                                if($documentImportant->code == $documentOrder->code){
                                    $found = true;
                                }
                            }

                            if(!$found){
                                ?>
                                    <label class="text-danger">
                                        <i class="fas fa-window-close"></i>
                                        @lang('vsat/neworderservicevsat.text_documentnotfound')<b style="text-decoration: underline;">{{ $documentImportant->description }}</b>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="{{ route('vsat.order.showAttach', [$infoOrder->code]) }}">
                                        <i class="fas fa-upload"></i> @lang('vsat/neworderservicevsat.href_attach')
                                    </a>
                                    <br />
                                <?php
                            }
                        }
                        ?>
                    </p>
                    <hr />
                    <label class="mb-0"><i class="fas fa-exclamation-circle"></i> @lang('vsat/neworderservicevsat.text_rememberattach')</label>
                </div>
            @endif

        </div>
        <!-- End about documentos -->

        <div class="col-xl-9 col-md-12 mb-5 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 py-4">
                                <div class="d-flex justify-content-between">
                                    <div class="justify-content-start">
                                        <h4 class="card-title text-dark mb-0">@lang('vsat/neworderservicevsat.subtitle_information')</h4>
                                    </div>
                                    <div class="justify-content-end">
                                        <a href="{{ route('vsat.order.showEditOrderService', [$infoOrder->code]) }}" class="btn btn-outline-primary btn-sm">
                                            <i class="fas fa-edit"></i> @lang('vsat/neworderservicevsat.btn_editorder')
                                        </a>
                                    </div>
                                </div>

                            @lang('vsat/neworderservicevsat.text_statusorder')<span class="{{ $infoOrder->statusStyle }}"><i class="fas fa-{{ $infoOrder->statusIcon }}"></i> {{ $infoOrder->statusText }}</span>
                        </div>
                        <div class="col-sm-12 py-2 mb-0">
                            <div class="row px-4">
                                <div class="col-sm-6">
                                    <p class="mb-0">@lang('vsat/neworderservicevsat.text_ordenservicecode')</p>
                                    <p class="spanTextShow">{{ $infoOrder->code }}</p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="mb-0">@lang('vsat/neworderservicevsat.text_creationdate')</p>
                                    <p class="spanTextShow mb-0">
                                        @if (strlen($infoOrder->create) > 0)
                                            {{ $infoOrder->create->format("d")." ".__('vsat/orderconsumptions.month_'.$infoOrder->create->format("m"))." ".$infoOrder->create->format("Y") }}
                                        @endif
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="mb-0">@lang('vsat/neworderservicevsat.text_taxidcustomer')</p>
                                    <p class="spanTextShow">{{ $infoOrder->companyDoc }}</p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="mb-0">@lang('vsat/neworderservicevsat.text_namecustomer')</p>
                                    <p class="spanTextShow">{{ $infoOrder->companyName }}</p>
                                </div>
                                <div class="col-sm-12">
                                    <p class="mb-0">@lang('vsat/neworderservicevsat.text_placedetail')</p>
                                    <p class="spanTextShow">{{ $infoOrder->addressInfo }}</p>
                                </div>
                                @if(strlen($infoOrder->coordinates) > 0)
                                    <div class="col-sm-6">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_coordinates')</p>
                                        <p class="spanTextShow">{{ $infoOrder->coordinates }}</p>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <p class="mb-0">@lang('vsat/neworderservicevsat.text_credit')</p>
                                    <p class="spanTextShow">@lang('vsat/neworderservicevsat.text_creditdetails', ['num' => $infoOrder->credit])</p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="mb-0">@lang('vsat/neworderservicevsat.text_typeofbilling')</p>
                                    <p class="spanTextShow">{{ $infoOrder->typeOfBilling }}</p>
                                </div>
                                @if (strlen($infoOrder->orderAnotations)>0)
                                    <div class="col-sm-12">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.subtitle_anotationsdetail')</p>
                                        <p class="spanTextShow">
                                            <span class="boxAnotationsYellow">{{ $infoOrder->orderAnotations }}</span>
                                        </p>
                                    </div>
                                @endif

                                @if (strlen($infoOrder->lastOrderCorde)>0)
                                    <div class="col-sm-6">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_lastorder')</p>
                                        <p class="spanTextShow">
                                            <a href="{{ route('vsat.order.ViewOrderService', [$infoOrder->lastOrderCorde]) }}">{{ $infoOrder->lastOrderCorde }}</a>
                                        </p>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 mb-0">
                            <hr />
                        </div>
                        <div class="col-sm-12 py-4">
                            <div class="d-flex justify-content-between">
                                <div class="justify-content-start">
                                    <h4 class="card-title text-dark mb-0">@lang('vsat/neworderservicevsat.subtitle_planinformation')</h4>
                                </div>
                                <div class="justify-content-end">
                                    <a href="{{ route('vsat.order.showAssignCost', [$infoOrder->code]) }}" class="btn btn-outline-primary btn-sm">
                                        <i class="fas fa-plus"></i> @lang('vsat/neworderservicevsat.btn_addcost')
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12 py-2">
                            <div class="row px-4 mb-0">
                                <div class="col-sm-6">
                                    <p class="mb-0">@lang('vsat/neworderservicevsat.text_typeactive')</p>
                                    <p class="spanTextShow">{{ $infoOrder->typeService }}</p>
                                </div>
                                @if (strlen($infoOrder->dateActive) > 0)
                                    <div class="col-sm-6">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_activationdate')</p>
                                        <p class="spanTextShow">
                                            <span class="boxAlertGreen">
                                                <i class="fas fa-check"></i> {{ $infoOrder->dateActive->format("d")." ".__('vsat/orderconsumptions.month_'.$infoOrder->dateActive->format("m"))." ".$infoOrder->dateActive->format("Y") }}
                                            </span>
                                        </p>
                                    </div>
                                @endif

                                @if ( strlen($infoOrder->contractMonths) > 0)
                                    <div class="col-sm-6">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_contracttitle')</p>
                                        <p class="spanTextShow">
                                            @lang('vsat/neworderservicevsat.text_contractdetails', ['num' => $infoOrder->contractMonths])
                                        </p>
                                    </div>
                                @endif

                                <div class="col-sm-6">
                                    <p class="mb-0">@lang('vsat/neworderservicevsat.text_sancode')</p>
                                    <p class="spanTextShow">{{ $infoOrder->sanCode }}</p>
                                </div>
                                @if (strlen($infoOrder->dateDue) > 0)
                                    <div class="col-sm-6">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_duedate')</p>
                                        <p class="spanTextShow">
                                            <span class="boxAlertYellow">
                                                <i class="fas fa-exclamation"></i> {{ $infoOrder->dateDue->format("d")." ".__('vsat/orderconsumptions.month_'.$infoOrder->dateDue->format("m"))." ".$infoOrder->dateDue->format("Y") }}
                                            </span>
                                        </p>
                                    </div>
                                @endif

                                @if (strlen($infoOrder->dateEnd) > 0)
                                    <div class="col-sm-6">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_enddate')</p>
                                        <p class="spanTextShow">
                                            <span class="boxAlertRed">
                                                <i class="fas fa-times"></i> {{ $infoOrder->dateEnd->format("d")." ".__('vsat/orderconsumptions.month_'.$infoOrder->dateEnd->format("m"))." ".$infoOrder->dateEnd->format("Y") }}
                                            </span>  
                                        </p>
                                    </div>
                                @endif

                                <!-- Table prices -->
                                <div class="col-sm-12 pt-4">
                                    <table width="100%" class="table border" cellpadding="0" style="max-width:80em;">
                                        <thead>
                                            <tr class="text-dark bg-light">
                                                <th class="align-middle">@lang('vsat/neworderservicevsat.text_planname')</td>
                                                <th class="align-middle" width="15%">@lang('vsat/neworderservicevsat.table_monthlycost')</th>
                                                <th class="align-middle" width="15%">@lang('vsat/neworderservicevsat.table_monthlyprice')</th>
                                                <th class="align-middle" width="20%">@lang('vsat/neworderservicevsat.text_plancost')</th>
                                                <th class="align-middle" width="20%">@lang('vsat/neworderservicevsat.text_planprice')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $totalCost = $invoiceExit;
                                                $totalPrice = $invoiceEntry;
                                            @endphp

                                            @foreach ( $listPlans as $plan )
                                                @php
                                                    if ($plan->commodecost != null){
                                                        $commodeCost = $plan->commodecost;
                                                    }else{
                                                        $commodeCost = 0;
                                                    }
                                                @endphp
                                                <tr>
                                                    <td class="align-middle">
                                                        <span>
                                                            <a href="{{ route('vsat.plan.showPlan', [$plan->codePlan]) }}" target="_blank">{{ $plan->typePlan ." - " .$plan->namePlan }}</a><br />

                                                            @isset($daysWithService)
                                                                <span>
                                                                    &nbsp;&nbsp;&nbsp;@lang('vsat/neworderservicevsat.text_servicesby')
                                                                </span>

                                                                <span class="boxAlertYellow">
                                                                    @lang('vsat/neworderservicevsat.text_days', ['day' => $daysWithService])
                                                                </span>

                                                            @else
                                                                @if($infoOrder->contractMonths != null)
                                                                    <span>
                                                                        &nbsp;&nbsp;&nbsp;@lang('vsat/neworderservicevsat.text_valueestimate')
                                                                    </span>
                                                                    <span class="boxAlertYellow">
                                                                        @lang('vsat/neworderservicevsat.text_months', ['month' => $infoOrder->contractMonths])
                                                                    </span>
                                                                @endif
                                                            @endisset
                                                        </span>
                                                    </td>
                                                    <td class="align-middle">$ -{{ number_format($plan->usdcost + $commodeCost, 2) }}</td>
                                                    <td class="align-middle">$ {{ number_format($plan->pricePlan, 2) }}</td>
                                                    <td class="align-middle text-danger">$ -{{ number_format($invoiceExit, 2) }}</td>
                                                    <td class="align-middle text-primary">$ {{ number_format($invoiceEntry, 2) }}</td>
                                                </tr>
                                            @endforeach

                                            <tr>
                                                <td class="align-middle" colspan="3">
                                                    <span>
                                                        @lang('vsat/neworderservicevsat.text_sumarryoperatings')<br />
                                                        <a href="{{ route('vsat.order.showCosts', [$infoOrder->code]) }}" class="px-4 text-secondary">
                                                            <i class="fas fa-search"></i> @lang('vsat/neworderservicevsat.text_checkcosts')
                                                        </a>
                                                    </span>
                                                </td>
                                                @php
                                                    $totalCost += $exitAmount;
                                                    $totalPrice += $entryAmount;
                                                @endphp
                                                <td class="text-danger align-middle">$ -{{ number_format($exitAmount, 2) }}</td>
                                                <td class="text-primary align-middle">$ {{ number_format($entryAmount, 2) }}</td>
                                            </tr>

                                        </tbody>
                                        <tfooter>
                                            <tr class="font-weight-bold bg-light">
                                                <td colspan="3">@lang('vsat/neworderservicevsat.text_total')</td>
                                                <td class="text-danger">$ -{{ number_format($totalCost, 2) }}</td>
                                                <td class="text-primary">$ {{ number_format($totalPrice, 2) }}</td>
                                            </tr>
                                            <tr class="font-weight-bold bg-light">
                                                <td colspan="4">@lang('vsat/neworderservicevsat.text_profit')</td>
                                                <td>
                                                    <span>
                                                        @php ($profit = $totalPrice - $totalCost)
                                                        @if ($profit > 0)
                                                            <span class="text-primary">$ {{ number_format($profit, 2) }}</span>
                                                        @else
                                                            <span class="text-danger">$ {{ number_format($profit, 2) }}</span>
                                                        @endif
                                                    </span>
                                                </td>
                                            </tr>
                                        </tfooter>
                                    </table>
                                </div>
                                <!-- End table prices -->

                            </div>
                        </div>

                        <!-- Discount information -->
                        @if($listDiscounts->count() > 0)
                            <div class="col-sm-12 py-4">
                                <div class="d-flex justify-content-between">
                                    <div class="justify-content-start">
                                        <h4 class="card-title text-dark mb-0">@lang('vsat/discounts.text_historial')</h4>
                                    </div>
                                    <div class="justify-content-end">
                                        <a href="{{ route('vsat.order.addDiscount', [$infoOrder->code]) }}" class="btn btn-outline-primary btn-sm">
                                            <i class="fas fa-plus"></i> @lang('vsat/discounts.btn_add')
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-12 py-2">
                                <div class="row px-4 mb-0">
                                    <div class="col-sm-12">
                                        <table class="table border">
                                            <thead>
                                                <tr class="bg-light text-dark">
                                                    <th>@lang('vsat/discounts.table_description')</th>
                                                    <th width="35%">@lang('vsat/discounts.table_period')</th>
                                                    <th width="15%">@lang('vsat/discounts.table_amount')</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php($totalDiscount = 0)
                                                @foreach($listDiscounts as $discount)
                                                    @php($totalDiscount += $discount->discount_amount)
                                                    <tr>
                                                        <td>
                                                            {{ $discount->description }}<br />
                                                            <a href="{{ route('vsat.order.downloadDiscount', [$infoOrder->code, $discount->code]) }}" class="px-4 text-secondary">
                                                                <i class="fas fa-download"></i> @lang('vsat/discounts.href_download')
                                                            </a>
                                                        </td>
                                                        <td class="align-middle">
                                                            @php($discount_start = date('Y-m', strtotime($discount->date_start)))
                                                            @php($discount_end = date('Y-m', strtotime($discount->date_end)))
                                                            @if($discount_start == $discount_end)
                                                                {{ __('vsat/orderconsumptions.month_'.substr($discount_start, 5, 2)).' '.substr($discount_start, 0, 4) }}
                                                            @else
                                                                {{ __('vsat/orderconsumptions.month_'.substr($discount_start, 5, 2)).' '.substr($discount_start, 0, 4) }}
                                                                <i class="fas fa-arrow-circle-right"></i>
                                                                {{ __('vsat/orderconsumptions.month_'.substr($discount_end, 5, 2)).' '.substr($discount_end, 0, 4) }}
                                                            @endif
                                                        </td>
                                                        <td class="align-middle text-danger">$ {{ number_format($discount->discount_amount, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            <tfooter>
                                                <tr class="border-bottom bg-light font-weight-bold">
                                                    <td colspan="2">@lang('vsat/discounts.table_total')</td>
                                                    <td class="text-danger">$ {{ number_format($totalDiscount, 2) }}</td>
                                                </tr>
                                            </tfooter>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <!-- End discount information -->

                        <!-- Item attach information -->
                        @if ( strlen($infoOrder->itemCode) > 0 )
                            <div class="col-sm-12 mb-0">
                                <hr />
                            </div>
                            <div class="col-sm-12 py-4">
                                <h4 class="card-title text-dark mb-0">@lang('vsat/neworderservicevsat.subtitle_antennainfo')</h4>
                            </div>

                            <div class="col-sm-12 py-2">
                                <div class="row px-4 mb-0">
                                    <div class="col-sm-6">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_itemcode')</p>
                                        <p class="spanTextShow">{{ $infoOrder->itemCode }}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_itemstatus')</p>
                                        <p class="spanTextShow">
                                            <span class="{{ $infoOrder->itemStyleAlert }}">
                                                <i class="fas fa-{{ $infoOrder->itemIconAlert }}"></i> {{ $infoOrder->itemStatus }}
                                            </span>
                                        </p>
                                    </div>
                                    <div class="col-sm-12">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_itemdetail')</p>
                                        <p class="spanTextShow">{{ $infoOrder->itemDescription }}</p>
                                    </div>
                                    <div class="col-sm-12">
                                        <p class="mb-0">@lang('vsat/neworderservicevsat.text_itemanotations')</p>
                                        <p class="spanTextShow">
                                            <span class="boxAnotationsYellow">{{ $infoOrder->itemAnotations }}</span>
                                        </p>
                                    </div>
                                    @foreach($infoOrder->itemTextDetail as $itemText)
                                        <div class="col-sm-4">
                                            <p class="mb-0">{{ $itemText->title }}</p>
                                            <p class="spanTextShow">{{ $itemText->description }}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <!-- End item attach information -->

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- List suspension -->
    @if($listSuspension->count() > 0)
        <div class="row">
            <div class="col-sm-12 py-4">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 py-2">
                                <h4 class="card-title text-dark mb-0">@lang('vsat/neworderservicevsat.title_suspensions')</h4>
                            </div>
                            <div class="col-md-12">
                                <br />
                                <table class="table" cellpadding="0" style="max-width:70em;">
                                    <thead>
                                        <tr style="font-weight: bold; background-color: #f8f9f9;">
                                            <td width="15%">@lang('vsat/neworderservicevsat.table_history_cut')</td>
                                            <td width="15%">@lang('vsat/neworderservicevsat.table_history_active')</td>
                                            <td width="15%">@lang('vsat/neworderservicevsat.table_history_days')</td>
                                            <td>@lang('vsat/neworderservicevsat.table_history_type') }}</td>
                                            <td width="20%">@lang('vsat/neworderservicevsat.table_history_by')</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($listSuspension as $suspension)
                                            <tr>
                                                <td>
                                                    <span class="text-danger">
                                                    {{ $suspension->start_suspension->format("d")." ".__('vsat/orderconsumptions.month_'.$suspension->start_suspension->format("m"))." ".$suspension->start_suspension->format("Y") }}
                                                    </span>
                                                </td>
                                                <td>
                                                    @if($suspension->end_suspension != null)
                                                        <span class="text-primary">
                                                            {{ $suspension->end_suspension->format("d")." ".__('vsat/orderconsumptions.month_'.$suspension->end_suspension->format("m"))." ".$suspension->end_suspension->format("Y") }}
                                                        </span>
                                                    @else
                                                        <span class="text-danger">
                                                                @lang('vsat/neworderservicevsat.table_history_noactive')
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($suspension->end_suspension != null)
                                                        @lang('vsat/neworderservicevsat.text_days', ['day' => $suspension->start_suspension->diffInDays($suspension->end_suspension)])
                                                    @else
                                                        @lang('vsat/neworderservicevsat.text_days', ['day' => $suspension->start_suspension->diffInDays(Carbon\Carbon::now())])
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($suspension->stop_billing)
                                                        <span class="text-danger">
                                                            @lang('vsat/neworderservicevsat.table_history_suspendedbill')
                                                        </span>
                                                    @else
                                                        @lang('vsat/neworderservicevsat.table_history_suspended')
                                                    @endif
                                                </td>
                                                <td>{{ $suspension->name.' '.$suspension->lastname }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- End list suspension -->

@endsection

@section('scripts')
    <script language="javascript">
        let userName = "{{ auth()->user()->name }}";
        let notFiles = "{{ __('vsat/neworderservicevsat.notFiles') }}";
        let modal_asksure_1 = "{{ __('vsat/neworderservicevsat.modal_asksure_1') }}";
        let modal_asksure_2 = "{{ __('vsat/neworderservicevsat.modal_asksure_2') }}";
        let modal_text_describemotive = "{{ __('vsat/neworderservicevsat.modal_text_describemotive') }}";
        let modal_text_warningaccept = "{{ __('vsat/neworderservicevsat.modal_text_warningaccept') }}";
        let modal_option_sure = "{{ __('vsat/neworderservicevsat.modal_option_sure') }}";
        let modal_option_notsure = "{{ __('vsat/neworderservicevsat.modal_option_notsure') }}";
        let modal_error_describemotive = "{{ __('vsat/neworderservicevsat.modal_error_describemotive') }}";
        let modal_error_warningaccept = "{{ __('vsat/neworderservicevsat.modal_error_warningaccept') }}";
        let modal_approved_witherrors = "{{ __('vsat/neworderservicevsat.modal_approved_witherrors') }}";
        let modal_approved_ok = "{{ __('vsat/neworderservicevsat.modal_approved_ok') }}";
        let modal_disapproved = "{{ __('vsat/neworderservicevsat.modal_disapproved') }}";
        let modal_alert_acceptterms = "{{ __('vsat/neworderservicevsat.modal_alert_acceptterms') }}";
        let modal_nearto_approve = "{{ __('vsat/neworderservicevsat.modal_nearto_approve') }}";
        let modal_nearto_disapprove = "{{ __('vsat/neworderservicevsat.modal_nearto_disapprove') }}";
        let modal_btnsure = "{{ __('vsat/neworderservicevsat.modal_btnsure') }}";
        let modal_btnproceed = "{{ __('vsat/neworderservicevsat.modal_btnproceed') }}";
        let modal_btncancel = "{{ __('vsat/neworderservicevsat.modal_btncancel') }}";
        let modal_btncontinue = "{{ __('vsat/neworderservicevsat.modal_btncontinue') }}";
    </script>
    <script src='/js/vsat/vieworderservice.js'></script>
@endsection