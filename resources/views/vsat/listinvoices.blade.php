@extends('layouts.dashboard')
@section('page_title', __('vsat/listinvoicesvsat.title_page'))
@section('info_title', __('vsat/listinvoicesvsat.title_page'))

@section('putButton')
    <a href="/viewordersconsumptions" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
        </span>
        <span class="text">@lang('vsat/listinvoicesvsat.btn_newinvoice')</span>
    </a>
@endsection

@section('content')

    @if (session('status'))
        @include('partials._successAlert', ['message' => __('vsat/listinvoicesvsat.message_saveok')])
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">@lang('vsat/listinvoicesvsat.tabletitle_list')</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                @if (count($arrayReceipts) == 0)
                    @include('partials._noresults',[
                        'title' => __('vsat/listinvoicesvsat.text_noresults'),
                        'text' => __('vsat/listinvoicesvsat.text_calltoaction'),
                        'textBtn' => __('vsat/listinvoicesvsat.btn_newinvoice'),
                        'urlBtn' => '/viewordersconsumptions'
                    ])
                @else
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th width="10%">@lang('vsat/listinvoicesvsat.table_code')</th>
                            <th width="20%">@lang('vsat/listinvoicesvsat.table_customer')</th>
                            <th>@lang('vsat/listinvoicesvsat.table_orderattach')</th>
                            <th width="10%">@lang('vsat/listinvoicesvsat.table_period')</th>
                            <th width="10%">@lang('vsat/listinvoicesvsat.table_ammount')</th>
                            <th width="10%">@lang('vsat/listinvoicesvsat.btn_edit')</th>
                            <th width="10%">@lang('vsat/listinvoicesvsat.table_review')</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php ($serialId = 0)
                            @foreach ($arrayReceipts as $receipt)
                            <tr>

                                <td>{{ $receipt['receiptNumber'] }}</td>
                                <td>{{ $receipt['customerName'] }}</td>
                                <td>
                                    @foreach ($receipt['listSan'] as $san)

                                        @php ($serialId ++)
                                        <button class="btn btn-light btn-icon btn-icon-split btn-sm" type="button" data-toggle="collapse" data-target="#collapseExample_{{ $serialId }}" aria-expanded="false" aria-controls="collapseExample_{{ $serialId }}" style="margin-bottom: 5px;">
                                            <span class="text">
                                                @lang('vsat/orderconsumptions.table_codesan', ['code' => $san['sanCode']])
                                            </span>
                                            <span class="icon text-white-50">
                                                <i class="fas fa-caret-down"></i>
                                            </span>
                                        </button>

                                                    <div class="collapse" id="collapseExample_{{ $serialId }}">
                                                        <div class="card card-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    @lang('vsat/orderconsumptions.table_place', ['place' => $san['addressName']])</a><br />
                                                                    {{ $san['location'] }}
                                                                </div>

                                                                @php($OnlyOnFirstAditional = true)

                                                                @foreach ($receipt['ordersAttach'] as $order)
                                                                    @if($san['sanCode'] == $order['sanCode'])

                                                                        @if($order['typePlan'] == 'PS')
                                                                            <div class="col-sm-12">
                                                                                <hr />
                                                                                @lang('vsat/orderconsumptions.table_orders')<br />
                                                                                <a href="/vieworderservicevsat/{{ $order['orderId'] }}" target="_blank">
                                                                                    {{ $order['orderId'] }}
                                                                                </a>
                                                                                {{ " - ".$order['planName']." - ".$order['totalAmount'] }}
                                                                                @if ($order['payDays'] == 1)
                                                                                    <span class="boxAlertYellow">
                                                                                        {{ __('vsat/orderconsumptions.table_proration').$order['payDays'] }} {{ __('vsat/orderconsumptions.text_day')." : ".$order['proration'] }}
                                                                                    </span>
                                                                                @elseif($order['payDays'] > 1)
                                                                                    <span class="boxAlertYellow">
                                                                                        {{ __('vsat/orderconsumptions.table_proration').$order['payDays'] }} {{ __('vsat/orderconsumptions.text_days')." : ".$order['proration'] }}
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                            
                                                                            <div class="col-sm-12">
                                                                                @php ($dateStart = $order['dateStart'])
                                                                                {{ __('vsat/orderconsumptions.table_active')." : " }}
                                                                                {{ date("d", strtotime($dateStart))." ".__('vsat/orderconsumptions.month_'.date("m", strtotime($dateStart)))." ".date("Y", strtotime($dateStart)) }}
                                                                                
                                                                                @if( strlen($order['dateEnd']) > 9 )
                                                                                    @php ($dateEnd = $order['dateEnd'])
                                                                                    {{ " -> ".__('vsat/orderconsumptions.table_cancel')." : " }}
                                                                                    {{ date("d", strtotime($dateEnd))." ".__('vsat/orderconsumptions.month_'.date("m", strtotime($dateEnd)))." ".date("Y", strtotime($dateEnd)) }}
                                                                                @endif
                                                                            </div>
                                                                        @elseif($order['typePlan'] == 'AD')
                                                                            <div class="col-sm-12" style="margin-top: -20px;">
                                                                                <br />
                                                                                @if($OnlyOnFirstAditional)
                                                                                <br />
                                                                                    @php($OnlyOnFirstAditional = false)
                                                                                    {{ __('vsat/orderconsumptions.title_aditionals') }}<br />
                                                                                @endif
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <a href="/vieworderservicevsat/{{ $order['orderId'] }}" target="_blank">
                                                                                    {{ $order['orderId'] }}
                                                                                </a>
                                                                                {{ " - ".$order['planName']." - ".$order['totalAmount'] }}
                                                                                
                                                                            </div>
                                                                        @else
                                                                            <div class="col-sm-12" style="margin-top: -20px;">
                                                                                <br />
                                                                                @if($OnlyOnFirstAditional)
                                                                                <br />
                                                                                    @php($OnlyOnFirstAditional = false)
                                                                                    {{ __('vsat/orderconsumptions.table_aditionals') }}<br />
                                                                                @endif
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <a href="/vieworderservicevsat/{{ $order['orderId'] }}" target="_blank">
                                                                                    {{ $order['orderId'] }}
                                                                                </a>
                                                                                {{ " - ".$order['planName']." - ".$order['totalAmount'] }}
                                                                                
                                                                                @php ($dateStart = $order['dateStart'])<br />
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;{{ __('vsat/orderconsumptions.table_active')." : " }}
                                                                                {{ date("d", strtotime($dateStart))." ".__('vsat/orderconsumptions.month_'.date("m", strtotime($dateStart)))." ".date("Y", strtotime($dateStart)) }}
                                                                            </div>

                                                                        @endif
                                                                    
                                                                    @endif
                                                                @endforeach
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                            
                                            <br />
                                    
                                    @endforeach
                                </td>
                                <td>{{ __('vsat/orderconsumptions.month_'.$receipt['month'])." ".$receipt['year'] }}</td>
                                <td>{{ $receipt['total'] }}</td>
                                <td>
                                    <a href="{{ route('vsat.invoices.editReceipt', [$receipt['receiptCode']]) }}" class="btn btn-secondary btn-icon-split btn-sm">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-edit"></i>
                                    </span>
                                        <span class="text">@lang('vsat/listinvoicesvsat.btn_edit')</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('vsat.vsatConsumption', [$receipt['receiptCode']]) }}" class="btn btn-success btn-icon-split btn-sm" target="_blank">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-download"></i>
                                    </span>
                                        <span class="text">@lang('vsat/listinvoicesvsat.btn_download')</span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>

@endsection
