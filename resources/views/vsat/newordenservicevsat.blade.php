@extends('layouts.dashboard')
@section('page_title', __('vsat/neworderservicevsat.title_page'))
@section('info_title', __('vsat/neworderservicevsat.title_page'))

@section('content')
    <!-- modals -->
    <div class="dinamycSelector" id="customerSelector"></div>
    <div class="dinamycSelector" id="addressSelector"></div>
    <div class="dinamycSelector" id="planSelector"></div>
    <div class="dinamycSelector" id="itemSelector"></div>
    <div class="dinamycSelector" id="priceSelector"></div>
    <!-- Content Row -->
    <div class="row">
    <form id="formNewOrder">
        <div class="col-xl-9 col-sm-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <input type="hidden" name="hiddenCodeCustomer" id="hiddenCodeCustomer" value="">
                    <input type="hidden" name="hiddenCodeAddress" id="hiddenCodeAddress" value="">
                    <input type="hidden" name="hiddenCodePlan" id="hiddenCodePlan" value="">
                    <input type="hidden" name="hiddenCodeItem" id="hiddenCodeItem" value="">
                    <input type="hidden" name="hiddenCodePrice" id="hiddenCodePrice" value="">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">{{ __('vsat/neworderservicevsat.subtitle_information')  }}</h5>
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="textOrdenCode">{{ __('vsat/neworderservicevsat.text_ordenservicecode')  }}</label>
                            <input type="text" class="form-control" id="textOrdenCode" name="textOrdenCode" placeholder="" autocomplete="off" value="" maxlength="8" onkeyup="searchCodeExist(this);">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="dateCreate">{{ __('vsat/neworderservicevsat.text_creationdate')  }}</label>
                            <input class="form-control" type="date" value="{{ date("Y-m-d") }}" id="dateCreate" name="dateCreate">
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="textCustomer">{{ __('vsat/neworderservicevsat.text_customer')  }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="textCustomer" name="textCustomer" autocomplete="off" onkeyup="searchDisplayElements(this, 'customerSelector', event);" >
                            </div>

                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="selectAddress">{{ __('vsat/neworderservicevsat.text_address')  }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="selectAddress" name="selectAddress" autocomplete="off" onkeyup="searchDisplayElements(this, 'addressSelector', event);">
                            </div>

                            <a href="/newaddress">{{ __('vsat/neworderservicevsat.text_addadress')  }}</a>

                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypeBilling">{{ __('vsat/neworderservicevsat.text_typeofbilling')  }}</label>
                            <select class="form-control" id="selectTypeBilling" name="selectTypeBilling" onchange="getListPlans();">
                                <option value="---">{{ __('vsat/neworderservicevsat.text_select')  }}</option>
                                @foreach ($typesBilling as $typeBilling)
                                    <option value="{{ $typeBilling->code }}">{{ $typeBilling->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if (isset ($lastOrder))
                            <div class="col-sm-6 py-2">
                                <label for="textLastOrder">{{ __('vsat/neworderservicevsat.text_lastorder')  }}</label>
                                <input type="text" class="form-control" id="textLastOrder" name="textLastOrder" value="{{ $lastOrder }}" autocomplete="off" readonly>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9 col-sm-12 mx-auto py-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">{{ __('vsat/neworderservicevsat.subtitle_planinformation')  }}</h5>
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="selectTypeActive">{{ __('vsat/neworderservicevsat.text_typeactive')  }}</label>
                            <select class="form-control" id="selectTypeActive" name="selectTypeActive" onchange="changeTypeActivation();">
                                <option value="---">{{ __('vsat/neworderservicevsat.text_select')  }}</option>
                                @foreach ($typeServices as $typeService)
                                    <option value="{{ $typeService->code }}">{{ $typeService->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="dateActivation">{{ __('vsat/neworderservicevsat.text_activationdate')  }}</label>
                            <input class="form-control" type="date" value="" id="dateActivation" name="dateActivation" onchange="calculateDue();" readonly>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textSanCode">{{ __('vsat/neworderservicevsat.text_sancode')  }}</label>
                            <input type="text" class="form-control" id="textSanCode" name="textSanCode" autocomplete="off">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textTimeContract">{{ __('vsat/neworderservicevsat.text_servicetime')  }}</label>
                            <input type="text" class="form-control" id="textTimeContract" name="textTimeContract" autocomplete="off" onkeyup="calculateDue();">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="dateDue">{{ __('vsat/neworderservicevsat.text_duedate')  }}</label>
                            <input class="form-control" type="date" value="" id="dateDue" name="dateDue" readonly>
                        </div>
                        <div class="col-sm-12 py-2">
                            <hr />
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="selectBand">{{ __('vsat/neworderservicevsat.text_band')  }}</label>
                            <select class="form-control" id="selectBand" name="selectBand" onchange="getListPlans();">
                                <option value="---">{{ __('vsat/neworderservicevsat.text_select')  }}</option>
                                @foreach ($vsatRecords as $vsatRecord)
                                    <option value="{{ $vsatRecord->code }}">{{ $vsatRecord->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypePlan">{{ __('vsat/neworderservicevsat.text_typePlan')  }}</label>
                            <select class="form-control" id="selectTypePlan" name="selectTypePlan" onchange="getListPlans();">
                                <option value="---">{{ __('vsat/neworderservicevsat.text_select')  }}</option>
                                @foreach ($typePlans as $typePlan)
                                    <option value="{{ $typePlan->code }}">{{ $typePlan->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="selectPlan">{{ __('vsat/neworderservicevsat.text_plan')  }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="selectPlan" name="selectPlan" onkeyup="searchDisplayElements(this, 'planSelector', event);" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-sm-12 py-2">
                            <label for="selectPlanPrice">{{ __('vsat/neworderservicevsat.text_planprice')  }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="selectPlanPrice" name="selectPlanPrice" onkeyup="searchDisplayElements(this, 'priceSelector', event);" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-12 py-4">
                            <button type="button" class="btn btn-secondary btn-user btn-block" id="addButton" onclick="addPlans();">
                                {{ __('vsat/neworderservicevsat.btn_addplans')  }}
                            </button>
                        </div>

                        <div class="col-sm-12 py-4">
                            <table width="100%" class="table table-bordered" cellpadding="0" id="tablePlans">
                                <thead>
                                    <tr>
                                        <td width="15%">{{ __('vsat/neworderservicevsat.table_delete')  }}</td>
                                        <td width="15%">{{ __('vsat/neworderservicevsat.table_code')  }}</td>
                                        <td>{{ __('vsat/neworderservicevsat.table_plan')  }}</td>
                                        <td width="20%">{{ __('vsat/neworderservicevsat.table_price')  }}</td>
                                    </tr>
                                </thead>

                            </table>
                            <input type="hidden" value="0" id="qtyPlansTable" name="qtyPlansTable">
                        </div>

                    </div>
                </div>
            </div>
        </div>

        @if (! isset ($lastOrder))
        <div class="col-xl-9 col-sm-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">{{ __('vsat/neworderservicevsat.subtitle_antenna')  }}</h5>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectSellCategories">{{ __('vsat/neworderservicevsat.text_sellcategories')  }}</label>
                            <select class="form-control" id="selectSellCategories" name="selectSellCategories" onchange="changeSellCategory(); getListInventoryItems();">
                                <option value="---">{{ __('vsat/neworderservicevsat.text_select')  }}</option>
                                @foreach ($sellCategories as $sellCategory)
                                    <option value="{{ $sellCategory->code }}">{{ $sellCategory->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypeProduct">{{ __('vsat/neworderservicevsat.text_typeproduct')  }}</label>
                            <select class="form-control" id="selectTypeProduct" name="selectTypeProduct" onchange="getListInventoryItems();">
                                <option value="---">{{ __('vsat/neworderservicevsat.text_select')  }}</option>
                            </select>
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="selectAntenna">{{ __('vsat/neworderservicevsat.text_antenna')  }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="selectAntenna" name="selectAntenna" autocomplete="off" onkeyup="searchDisplayElements(this, 'itemSelector', event);">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="col-xl-9 col-sm-12 mx-auto py-4 mb-5">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">{{ __('vsat/neworderservicevsat.subtitle_anotations')  }}</h5>
                        </div>

                        <div class="col-sm-12 py-2">
                            <label for="textareaComents">{{ __('vsat/neworderservicevsat.text_anotations')  }}</label>
                            <textarea class="form-control" name="textareaComents" id="textareaComents"></textarea>
                        </div>

                        <div class="col-sm-12 py-4">
                            <button type="button" class="btn btn-primary btn-user btn-block" id="sendButtom" onclick="saveOrder();">
                                {{ __('vsat/neworderservicevsat.btn_saveorder')  }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
    </div>

@endsection

@section('scripts')
    <script src="/js/vsat/createorderservice.js"></script>
    <script>
        let modalComplete = "{{ __('vsat/neworderservicevsat.modal_complete')  }}";
        let modalSaved = "{{ __('vsat/neworderservicevsat.modal_saved')  }}";
        let modelCodeUsed = "{{ __('vsat/neworderservicevsat.modal_orderused')  }}";

        let table_delete = "{{ __('vsat/neworderservicevsat.table_delete')  }}";
        let text_demoservice = "{{ __('vsat/neworderservicevsat.text_demoservice')  }}";
        let text_select = "{{ __('vsat/neworderservicevsat.text_select')  }}";
        let text_noresults = "{{ __('vsat/neworderservicevsat.text_noresults')  }}";
        let text_anotationjs = "{{ __('vsat/neworderservicevsat.text_anotationjs')  }}";
        let text_codejs = "{{ __('vsat/neworderservicevsat.text_codejs')  }}";
        let text_contractfor = "{{ __('vsat/neworderservicevsat.text_contractfor')  }}";
        let text_months = "{{ __('vsat/neworderservicevsat.text_months')  }}";
        let text_from = "{{ __('vsat/neworderservicevsat.text_from')  }}";
        let text_to = "{{ __('vsat/neworderservicevsat.text_to')  }}";
        let text_connections = "{{ __('vsat/neworderservicevsat.text_connections')  }}";

        $(document).ready(function(){
            getListCompanies();
        });
        $('#selectAddress').selectpicker();
        $('#selectPlan').selectpicker();
        $('#selectAntenna').selectpicker();
    </script>
@endsection
