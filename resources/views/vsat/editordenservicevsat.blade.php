@extends('layouts.dashboard')
@section('page_title', __('vsat/neworderservicevsat.title_page_edit'))
@section('info_title', __('vsat/neworderservicevsat.title_page_edit'))

@section('content')
    <!-- modals -->
    <div class="dinamycSelector" id="customerSelector"></div>
    <div class="dinamycSelector" id="addressSelector"></div>
    <div class="dinamycSelector" id="planSelector"></div>
    <div class="dinamycSelector" id="itemSelector"></div>
    <div class="dinamycSelector" id="priceSelector"></div>
    
    <div class="row">
    <form id="formNewOrder">
        <div class="col-xl-9 col-sm-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <input type="hidden" name="hiddenCodeCustomer" id="hiddenCodeCustomer" value="{{ $infoOrder->customer_code }}">
                    <input type="hidden" name="hiddenCodeAddress" id="hiddenCodeAddress" value="{{ $infoOrder->address_code }}">
                    <input type="hidden" name="hiddenCodePlan" id="hiddenCodePlan" value="">
                    <input type="hidden" name="hiddenCodeItem" id="hiddenCodeItem" value="{{ $infoOrder->product_id }}">
                    <input type="hidden" name="hiddenLastCodeItem" id="hiddenLastCodeItem" value="{{ $infoOrder->product_id }}">
                    <input type="hidden" name="hiddenCodePrice" id="hiddenCodePrice" value="{{ $infoOrder->price_plan }}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">@lang('vsat/neworderservicevsat.subtitle_information')</h5>
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="textOrdenCode">@lang('vsat/neworderservicevsat.text_ordenservicecode')</label>
                            <input type="text" class="form-control" id="textOrdenCode" name="textOrdenCode" autocomplete="off" value="{{ $infoOrder->code }}" readonly>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="dateCreate">@lang('vsat/neworderservicevsat.text_creationdate')</label>
                            <input class="form-control" type="date" value="{{ $infoOrder->create_date }}" id="dateCreate" name="dateCreate">
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="textCustomer">@lang('vsat/neworderservicevsat.text_customer')</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="textCustomer" name="textCustomer" autocomplete="off" onkeyup="searchDisplayElements(this, 'customerSelector', event);">
                            </div>

                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="selectAddress">@lang('vsat/neworderservicevsat.text_address')</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="selectAddress" name="selectAddress" autocomplete="off" onkeyup="searchDisplayElements(this, 'addressSelector', event);" >
                            </div>

                            <a href="/newaddress">@lang('vsat/neworderservicevsat.text_addadress')</a>

                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypeBilling">@lang('vsat/neworderservicevsat.text_typeofbilling')</label>
                            <select class="form-control" id="selectTypeBilling" name="selectTypeBilling" onchange="getListPlans();">
                                <option value="---">@lang('vsat/neworderservicevsat.text_select')</option>
                                @foreach ($typesBilling as $typeBilling)
                                        @if ($infoOrder->typebilling_id == $typeBilling->code)
                                            <option value="{{ $typeBilling->code }}" selected="selected">{{ $typeBilling->description }}</option>
                                        @else
                                            <option value="{{ $typeBilling->code }}">{{ $typeBilling->description }}</option>
                                        @endif

                                @endforeach
                            </select>
                        </div>


                        @if (strlen ($infoOrder->lastOrderCode) > 0)
                            <div class="col-sm-6 py-2">
                                <label for="textLastOrder">@lang('vsat/neworderservicevsat.text_lastorder')</label>
                                <input type="text" class="form-control" id="textLastOrder" name="textLastOrder" value="{{ $infoOrder->lastOrderCode }}" autocomplete="off" readonly>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9 col-sm-12 mx-auto py-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">@lang('vsat/neworderservicevsat.subtitle_planinformation')</h5>
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="selectTypeActive">@lang('vsat/neworderservicevsat.text_typeactive')</label>
                            <select class="form-control" id="selectTypeActive" name="selectTypeActive" onchange="changeTypeActivation();">
                                <option value="---">@lang('vsat/neworderservicevsat.text_select')</option>
                                @foreach ($typeServices as $typeService)
                                    @if ($infoOrder->typeservice_id == $typeService->code)
                                        <option value="{{ $typeService->code }}" selected="selected">{{ $typeService->description }}</option>
                                    @else
                                        <option value="{{ $typeService->code }}">{{ $typeService->description }}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="dateActivation">@lang('vsat/neworderservicevsat.text_activationdate')</label>
                            <input class="form-control" type="date" value="{{ $infoOrder->date_start }}" id="dateActivation" name="dateActivation" onchange="calculateDue();" readonly>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textSanCode">@lang('vsat/neworderservicevsat.text_sancode')</label>
                            <input type="text" class="form-control" value="{{ $infoOrder->sancode }}" id="textSanCode" name="textSanCode" autocomplete="off">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textTimeContract">@lang('vsat/neworderservicevsat.text_servicetime')</label>
                            <input type="text" class="form-control" value="{{ $infoOrder->contracttime }}" id="textTimeContract" name="textTimeContract" autocomplete="off" onkeyup="calculateDue();">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="dateDue">@lang('vsat/neworderservicevsat.text_duedate')</label>
                            <input class="form-control" type="date" value="{{ $infoOrder->date_due }}" id="dateDue" name="dateDue" readonly>
                        </div>
                        <div class="col-sm-12 py-2">
                            <hr />
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectBand">@lang('vsat/neworderservicevsat.text_band')</label>
                            <select class="form-control" id="selectBand" name="selectBand" onchange="getListPlans();">
                                <option value="---">@lang('vsat/neworderservicevsat.text_select')</option>
                                @foreach ($vsatRecords as $vsatRecord)
                                    <option value="{{ $vsatRecord->code }}">{{ $vsatRecord->description }}</option>

                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypePlan">@lang('vsat/neworderservicevsat.text_typePlan')</label>
                            <select class="form-control" id="selectTypePlan" name="selectTypePlan" onchange="getListPlans();">
                                <option value="---">@lang('vsat/neworderservicevsat.text_select')</option>
                                @foreach ($typePlans as $typePlan)
                                    <option value="{{ $typePlan->code }}">{{ $typePlan->description }}</option>

                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="selectPlan">@lang('vsat/neworderservicevsat.text_plan')</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="selectPlan" name="selectPlan" onkeyup="searchDisplayElements(this, 'planSelector', event);" value="" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-sm-12 py-2">
                            <label for="selectPlanPrice">@lang('vsat/neworderservicevsat.text_planprice')</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="selectPlanPrice" name="selectPlanPrice" onkeyup="searchDisplayElements(this, 'priceSelector', event);" value="" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-12 py-4">
                            <button type="button" class="btn btn-secondary btn-user btn-block" id="addButton" onclick="addPlans();">
                                @lang('vsat/neworderservicevsat.btn_addplans')
                            </button>
                        </div>

                        <div class="col-sm-12 py-4">
                            <table width="100%" class="table table-bordered" cellpadding="0" id="tablePlans">
                                <thead>
                                <tr>
                                    <td width="15%">@lang('vsat/neworderservicevsat.table_delete')</td>
                                    <td width="15%">@lang('vsat/neworderservicevsat.table_code')</td>
                                    <td>@lang('vsat/neworderservicevsat.table_plan')</td>
                                    <td width="20%">@lang('vsat/neworderservicevsat.table_price')</td>
                                </tr>
                                </thead>
                                <tbody>
                                @php ($count = 0)
                                @foreach ($listPlans as $plan)
                                    @php ($count ++)
                                    <tr>
                                        <td><a href="#" onclick="return deleteItem(this);">@lang('vsat/neworderservicevsat.table_delete')</a><input type="hidden" name="idExistPlan_{{ $count }}" id="idExistPlan_{{ $count }}" value="{{ $plan->code }}"></td>
                                        <td>{{ $plan->codePlan }}<input type="hidden" name="codePlanTable_{{ $count }}" id="codePlanTable_{{ $count }}" value="{{ $plan->codePlan }}"></td>
                                        <td>{{ $plan->typePlan." - ".$plan->namePlan }}</td>
                                        <td>$ {{ $plan->pricePlan }}<input type="hidden" name="codePriceTable_{{ $count }}" id="codePriceTable_{{ $count }}" value="{{ $plan->pricePlan }}"></td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                            <input type="hidden" value="{{ $count }}" id="qtyPlansTable" name="qtyPlansTable">
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9 col-sm-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">@lang('vsat/neworderservicevsat.subtitle_antenna')</h5>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectSellCategories">@lang('vsat/neworderservicevsat.text_sellcategories')</label>
                            <select class="form-control" id="selectSellCategories" name="selectSellCategories" onchange="changeSellCategory(); getListInventoryItems();">
                                <option value="---">@lang('vsat/neworderservicevsat.text_select')</option>
                                @foreach ($sellCategories as $sellCategory)
                                    @if ($infoOrder->sellcategodory_id == $sellCategory->code)
                                        <option value="{{ $sellCategory->code }}" selected="selected">{{ $sellCategory->description }}</option>
                                    @else
                                        <option value="{{ $sellCategory->code }}">{{ $sellCategory->description }}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypeProduct">@lang('vsat/neworderservicevsat.text_typeproduct')</label>
                            <select class="form-control" id="selectTypeProduct" name="selectTypeProduct" onchange="getListInventoryItems();">
                                <option value="---">@lang('vsat/neworderservicevsat.text_select')</option>
                                @foreach ($typeProducts as $typeProduct)
                                    @if ($infoOrder->typeproduct_id == $typeProduct->code)
                                        <option value="{{ $typeProduct->code }}" selected="selected">{{ $typeProduct->description }}</option>
                                    @else
                                        <option value="{{ $typeProduct->code }}">{{ $typeProduct->description }}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="selectAntenna">@lang('vsat/neworderservicevsat.text_antenna')</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="selectAntenna" name="selectAntenna" autocomplete="off" onkeyup="searchDisplayElements(this, 'itemSelector', event);">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9 col-sm-12 mx-auto mb-5">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">@lang('vsat/neworderservicevsat.subtitle_anotations')</h5>
                        </div>

                        <div class="col-sm-12 py-2">
                            <label for="textareaComents">@lang('vsat/neworderservicevsat.text_anotations')</label>
                            <textarea class="form-control" name="textareaComents" id="textareaComents">{{ $infoOrder->notes }}</textarea>
                        </div>

                        <div class="col-sm-12 py-4">
                            <button type="button" class="btn btn-primary btn-user btn-block" id="sendButtom" onclick="updateOrder();">
                                @lang('vsat/neworderservicevsat.btn_updateorder')
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
    </div>

@endsection

@section('scripts')
    <script src="/js/vsat/createorderservice.js"></script>
    <script>
        let modalComplete = "{{ __('vsat/neworderservicevsat.modal_complete')  }}";
        let modalUpdated = "{{ __('vsat/neworderservicevsat.modal_updated')  }}";
        let modelCodeUsed = "{{ __('vsat/neworderservicevsat.modal_orderused')  }}";

        let table_delete = "{{ __('vsat/neworderservicevsat.table_delete')  }}";
        let text_demoservice = "{{ __('vsat/neworderservicevsat.text_demoservice')  }}";
        let text_select = "{{ __('vsat/neworderservicevsat.text_select')  }}";
        let text_noresults = "{{ __('vsat/neworderservicevsat.text_noresults')  }}";
        let text_anotationjs = "{{ __('vsat/neworderservicevsat.text_anotationjs')  }}";
        let text_codejs = "{{ __('vsat/neworderservicevsat.text_codejs')  }}";
        let text_contractfor = "{{ __('vsat/neworderservicevsat.text_contractfor')  }}";
        let text_months = "{{ __('vsat/neworderservicevsat.text_months')  }}";
        let text_from = "{{ __('vsat/neworderservicevsat.text_from')  }}";
        let text_to = "{{ __('vsat/neworderservicevsat.text_to')  }}";
        let text_connections = "{{ __('vsat/neworderservicevsat.text_connections')  }}";

        $(document).ready(function(){
            getListCompanies();
            selectCustomer("{{ $infoOrder->customer_name }}", "{{ $infoOrder->customer_code }}");
            selectAddress("{{ $infoOrder->address_name }}", "{{ $infoOrder->address_code }}");
            changeTypeActivation();
            getListInventoryItems();
            selectItem("{{ $infoOrder->product_name }}", "{{ $infoOrder->product_id }}");
        });

    </script>
@endsection
