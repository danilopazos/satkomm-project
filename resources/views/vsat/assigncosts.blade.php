@extends('layouts.dashboard')
@section('page_title', __('vsat/viewcosts.title_pagerecord'))
@section('info_title', __('vsat/viewcosts.title_pagerecord'))

@section('putButton')
    <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-th"></i>&nbsp;&nbsp;
            <span class="text">@lang('vsat/viewcosts.btn_viewoptions')</span>
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('vsat.order.showCosts', [$orderid]) }}"><i class="fas fa-chart-line"></i> @lang('vsat/neworderservicevsat.text_checkcosts')</a>
            
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('vsat.order.ViewOrderService', [$orderid]) }}"><i class="fas fa-angle-left"></i> @lang('vsat/viewcosts.btn_back')</a>
        </div>
    </div>
@endsection

@section('content')

    <div class="row mb-4">
        
        <div class="col-sm-8 py-2">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('vsat.saveAssignCost') }}" onsubmit="return saveCost();" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h4>@lang('vsat/viewcosts.text_aboutservice')</h4>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="orderCode">@lang('vsat/viewcosts.text_orderservice')</label>
                            <input type="text" class="form-control" name="orderCode" id="orderCode" value="{{ $orderid }}" readonly>
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="selectTypeRecord">@lang('vsat/viewcosts.text_typerecord')</label>
                            <select class="form-control" name="selectTypeRecord" id="selectTypeRecord" onchange="changeTypeRecord();">
                                <option value="---">@lang('vsat/viewcosts.select_choose')</option>
                                @foreach($listRecord as $record)
                                    <option value="{{ $record->code }}">{{ $record->description }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="selectProvider">@lang('vsat/viewcosts.text_serviceprovider')</label>
                            <select class="form-control" name="selectProvider" id="selectProvider">
                                <option value="---">@lang('vsat/viewcosts.select_choose')</option>
                                @foreach($listProvider as $list)
                                    <option value="{{ $list->code }}">{{ $list->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="selectCost">@lang('vsat/viewcosts.text_typecost')</label>
                            <select class="form-control" name="selectCost" id="selectCost">
                                    <option value="---">@lang('vsat/viewcosts.select_choose')</option>
                                    @foreach($listTypeCost as $cost)
                                    <option value="{{ $cost->code }}">{{ $cost->description }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-12 py-2">
                            <label for="textAnotation">@lang('vsat/viewcosts.text_anotations')</label>
                            <input type="text" class="form-control" name="textAnotation" id="textAnotation">
                        </div>

                        <div class="col-sm-12 py-2">
                            <hr />
                            <h4>@lang('vsat/viewcosts.text_aboutdocument')</h4>
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="selectDocument">@lang('vsat/viewcosts.table_typedoc')</label>
                            <select class="form-control" name="selectDocument" id="selectDocument">
                                <option value="---">@lang('vsat/viewcosts.select_choose')</option>
                                @foreach($listInvoice as $invoice)
                                    <option value="{{ $invoice->code }}">{{ $invoice->description }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="numberDocument">@lang('vsat/viewcosts.table_numberdoc')</label>
                            <input type="text" class="form-control" name="numberDocument" id="numberDocument">
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="dateCreate">@lang('vsat/viewcosts.table_date')</label>
                            <input type="date" class="form-control" name="dateCreate" id="dateCreate">
                        </div>

                        <div class="col-sm-6 py-2">
                            <label for="selectCurrency">@lang('vsat/viewcosts.text_currency')</label>
                            <select class="form-control" name="selectCurrency" id="selectCurrency">
                                <option value="---">@lang('vsat/viewcosts.select_choose')</option>
                                @foreach($currencies as $currency)
                                    <option value="{{ $currency->code }}">{{ $currency->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <div class="col-sm-6 py-2">
                            <label for="costAmount">@lang('vsat/viewcosts.text_costwithout')</label>
                            <input type="text" class="form-control" name="costAmount" id="costAmount">
                        </div>

                        <div class="col-sm-12 py-2">
                            <label for="fileAttach">@lang('vsat/viewcosts.file_select')</label><br />
                            <input type="file" id="fileAttach" name="fileAttach">
                        </div>

                        <div class="col-sm-12 py-4">
                            <input type="submit" class="btn btn-primary btn-user btn-block" value="Registrar costo operativo">
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let modal_complete = "{{ __('vsat/viewcosts.modal_complete') }}";

    </script>
    <script src="/js/vsat/saveassigncost.js"></script>
@endsection