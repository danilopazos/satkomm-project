@extends('layouts.dashboard')
@section('page_title', __('vsat/showplan.page_header'))
@section('info_title', __('vsat/showplan.page_title').$infoPlan->planDescription)

@section('putButton')
    <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-th"></i>&nbsp;&nbsp;
                {{ __('vsat/showplan.btn_options') }}
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="/editvsatplan/{{ $planId }}"><i class="fas fa-edit"></i> {{ __('vsat/showplan.options_edit') }}</a>
            <a class="dropdown-item" href="/newvsatplan"><i class="fas fa-plus"></i> {{ __('vsat/showplan.option_newplan') }}</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/newordenservicevsat"><i class="fas fa-file-invoice"></i> {{ __('vsat/showplan.option_neworder') }}</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/listplansvsat"><i class="fas fa-angle-left"></i> {{ __('vsat/showplan.option_back') }}</a>

        </div>
    </div>
@endsection

@section('content')

    <div class="row mb-4">
        <div class="col-sm-12 py-2">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">{{ __('vsat/showplan.subtitle_aboutplan') }}</h5>
                        </div>
                        <div class="col-sm-12 py-2">
                            <table width="80%" style="margin-left: 15px;">
                                <tr>
                                    <td width="28%">{{ __('vsat/showplan.text_codeplan') }}</td>
                                    <td>{{ $infoPlan->code }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('vsat/showplan.text_band') }}</td>
                                    <td>{{ $infoPlan->bandDescription }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('vsat/showplan.text_plan') }}</td>
                                    <td>{{ $infoPlan->typeDescription }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('vsat/showplan.text_nameplan') }}</td>
                                    <td>{{ $infoPlan->planDescription }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('vsat/showplan.text_notes') }}</td>
                                    <td>{{ $infoPlan->planNote }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                
                                @isset($infoPlan->download)
                                    <tr>
                                        <td>{{ __('vsat/editvsatplan.text_donwload')  }}</td>
                                        <td>
                                            {{ $infoPlan->download }}
                                            @foreach ($listSeppeds as $speed)
                                                {{ $speed->code == $infoPlan->typedownload ? $speed->description : '' }}
                                            @endforeach
                                        </td>
                                    </tr>
                                @endisset

                                @isset($infoPlan->upload)
                                    <tr>
                                        <td>{{ __('vsat/editvsatplan.text_upload')  }}</td>
                                        <td>
                                            {{ $infoPlan->upload }}
                                            @foreach ($listSeppeds as $speed)
                                                {{ $speed->code == $infoPlan->typeupload ? $speed->description : '' }}
                                            @endforeach
                                        </td>
                                    </tr>
                                @endisset

                                @isset($infoPlan->anytime)
                                    <tr>
                                        <td>{{ __('vsat/editvsatplan.text_anytime')  }}</td>
                                        <td>
                                            {{ $infoPlan->anytime }}
                                            @foreach ($listUnits as $units)
                                                {{ $units->code == $infoPlan->typeanytime ? $units->description : '' }}
                                            @endforeach
                                        </td>
                                    </tr>
                                @endisset

                                @isset($infoPlan->earlymorning)
                                    <tr>
                                        <td>{{ __('vsat/editvsatplan.text_early')  }}</td>
                                        <td>
                                            {{ $infoPlan->earlymorning }}
                                            @foreach ($listUnits as $units)
                                                {{ $units->code == $infoPlan->typeearlymorning ? $units->description : '' }}
                                            @endforeach
                                        </td>
                                    </tr>
                                @endisset

                                @isset($infoPlan->total)
                                    <tr>
                                        <td>{{ __('vsat/editvsatplan.text_total')  }}</td>
                                        <td>
                                            {{ $infoPlan->total }}
                                            @foreach ($listUnits as $units)
                                                {{ $units->code == $infoPlan->typetotal ? $units->description : '' }}
                                            @endforeach
                                        </td>
                                    </tr>
                                @endisset

                                @isset($infoPlan->cir)
                                    <tr>
                                        <td>{{ __('vsat/editvsatplan.text_cir')  }}</td>
                                        <td>
                                            {{ $infoPlan->cir }}
                                            @foreach ($listSeppeds as $speed)
                                                {{ $speed->code == $infoPlan->typecir ? $speed->description : '' }}
                                            @endforeach
                                        </td>
                                    </tr>
                                @endisset

                            </table>
                        </div>

                        <div class="col-sm-12 py-2">
                            <hr />
                            <h5 class="card-title">{{ __('vsat/showplan.subtitle_aboutcosts') }}</h5>
                        </div>
                        <div class="col-sm-12 py-2">
                            <table width="80%" style="margin-left: 15px;">
                                @php( $total = 0 )
                                @if( $infoPlan->usdcost > 0 )
                                    @php( $total += $infoPlan->usdcost )
                                    <tr>
                                        <td width="28%">{{ __('vsat/showplan.text_cost') }}</td>
                                        <td>$ {{ $infoPlan->usdcost }} USD</td>
                                    </tr>
                                @endif

                                @if( $infoPlan->commodecost > 0 )
                                    @php( $total += $infoPlan->commodecost )
                                    <tr>
                                        <td width="25%">{{ __('vsat/showplan.text_commodate') }}</td>
                                        <td>$ {{ $infoPlan->commodecost }} USD</td>
                                    </tr>
                                @endif

                                <tr style="font-weight: bold;">
                                    <td>{{ __('vsat/showplan.text_totals') }}</td>
                                    <td>$ {{ number_format($total, 2) }} USD</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($infoPlan->typeId == 'PS')
            <div class="col-sm-12 py-2">
                <div class="card">
                    <div class="card-body">

                        @if($ordersAssoc->count() > 0)
                            <div class="row">
                                <div class="col-sm-12 py-2">
                                    <h5 class="card-title">{{ __('vsat/showplan.subtitle_founds', ['number' => $ordersAssoc->count()]) }}</h5>
                                    <hr />
                                    <br />
                                </div>

                                <div class="col-sm-12 py-2">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" style="min-width: 70em;" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <td>{{ __('vsat/showplan.table_customer') }}</td>
                                                    <td>{{ __('vsat/showplan.table_order') }}</td>
                                                    <td>{{ __('vsat/showplan.table_datestart') }}</td>
                                                    <td>{{ __('vsat/showplan.table_sellvalue') }}</td>
                                                    <td>{{ __('vsat/showplan.table_check') }}</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($ordersAssoc as $order)
                                                    <tr>
                                                        <td>{{ $order->companyName }}</td>
                                                        <td>{{ $order->orderCode }}</td>
                                                        <td>{{ $order->date_start }}</td>
                                                        <td>$ {{ $order->usdPrice }} USD</td>
                                                        <td>
                                                            <a href="/vieworderservicevsat/{{ $order->orderCode }}" target="_blank" class="btn btn-success btn-icon-split btn-sm">
                                                                <span class="icon text-white-50">
                                                                <i class="fas fa-search"></i>
                                                                </span>
                                                                <span class="text">{{ __('vsat/showplan.table_check') }}</span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        @else
                            <h4 align="center" class="card-title">
                                    <span class="text-danger">
                                        <i class="fas fa-heart-broken"></i>
                                        {{ __('vsat/showplan.text_noresults') }}
                                    </span>
                            </h4>
                            <p align="center" class="card-text">
                                    <span class="text-gray-800">{{ __('vsat/showplan.text_information') }}</span>
                            </p>

                        @endif
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection
