@extends('layouts.dashboard')
@section('page_title', __('vsat/listinventoryvsat.title_page'))
@section('info_title', __('vsat/listinventoryvsat.title_page'))

@section('putButton')
<a href="/newinventoryvsat" class="btn btn-primary btn-icon-split">
    <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
    </span>
    <span class="text">@lang('vsat/listinventoryvsat.btn_newplan')</span>
</a>
@endsection

@section('content')

    @if (session('status'))
        @include('partials._successAlert', ['message' => __('vsat/listinventoryvsat.message_saveok')])
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">@lang('vsat/listinventoryvsat.tabletitle_list')</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                @if($listInventories->count() == 0)
                    @include('partials._noresults',[
                        'title' => __('vsat/listinventoryvsat.text_noresults'),
                        'text' => __('vsat/vsat/listinventoryvsat.text_calltoaction'),
                        'textBtn' => __('vsat/listinventoryvsat.btn_newplan'),
                        'urlBtn' => '/newinventoryvsat'
                    ])
                @else
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>@lang('vsat/listinventoryvsat.table_code')</th>
                            <th>@lang('vsat/listinventoryvsat.table_product')</th>
                            <th>@lang('vsat/listinventoryvsat.table_brand')</th>
                            <th>@lang('vsat/listinventoryvsat.table_model')</th>
                            <th>@lang('vsat/listinventoryvsat.table_group')</th>
                            <th>@lang('vsat/listinventoryvsat.table_series')</th>
                            <th>@lang('vsat/listinventoryvsat.table_anotation')</th>
                            <th>@lang('vsat/listinventoryvsat.table_status')</th>
                            <th>@lang('vsat/listinventoryvsat.table_edit')</th>
                            <th>@lang('vsat/listinventoryvsat.table_check')</th>
                        </tr>
                        </thead>
                        <tbody>

                            @foreach ($listInventories as $listInventory)
                            <tr>
                                <td>{{ $listInventory->code }}</td>
                                <td>{{ $listInventory->typeproduct }}</td>
                                <td>{{ $listInventory->brand }}</td>
                                <td>{{ $listInventory->model }}</td>
                                <td>
                                @foreach ($listGroups as $listGroup)
                                    @if ($listGroup->code == $listInventory->group_id)
                                        {{ $listGroup->description }}
                                    @else
                                        {{ '-' }}
                                    @endif
                                @endforeach
                                </td>
                                <td>
                                    @php
                                    $series = \App\TextDetailInventory::InfoRelations($listInventory->code);
                                    foreach($series as $serie){
                                        echo $serie->title.": ".$serie->description."<br />";
                                    }
                                    @endphp
                                </td>
                                <td>
                                    @if(strlen($listInventory->anotations)>0)
                                        {{ $listInventory->anotations }}
                                    @else
                                        {{ '-' }}
                                    @endif
                                </td>
                                <td>
                                    <div class="{{ $listInventory->stylealert }}">
                                        <i class="fas fa-{{ $listInventory->iconalert }}"></i> {{ $listInventory->available }}
                                    </div>

                                </td>
                                <td>
                                    <a href="{{ route('vsat.inventory.showEdit', [$listInventory->code]) }}" class="btn btn-light btn-icon-split btn-sm">
                                        <span class="icon text-white-50">
                                            <i class="far fa-edit"></i>
                                        </span>
                                        <span class="text">@lang('vsat/listinventoryvsat.table_edit')</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="/#/{{ $listInventory->code }}" class="btn btn-success btn-icon-split btn-sm">
                                        <span class="icon text-white-50">
                                        <i class="fas fa-search"></i>
                                        </span>
                                        <span class="text">@lang('vsat/listinventoryvsat.table_check')</span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>

@endsection
