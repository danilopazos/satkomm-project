@extends('layouts.dashboard')
@section('page_title', __('vsat/editreceipt.title_page'))
@section('info_title', __('vsat/editreceipt.title_page'))

@section('content')

    <div class="row mb-4">
        <div class="col-xl-8 col-sm-12">
            <div class="card">
                <div class="card-body">

                    <form id="formEditReceipt" method="post" onsubmit="return false;">
                        @csrf
                        <input type="hidden" name="receiptCode" id="receiptCode" value="{{ $receipt->id }}">

                        <div class="row">
                            <div class="col-sm-12 py-2">
                                <h4 class="card-title text-dark">@lang('vsat/editreceipt.title')</h4>
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="customerName">@lang('vsat/editreceipt.text_customer')</label>
                                <input type="text" name="customerName" id="customerName" class="form-control" value="{{ $companyName }}" autocomplete="off" readonly>
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="period">@lang('vsat/editreceipt.text_period')</label>
                                <input type="text" name="period" id="period" class="form-control" value="{{ __('vsat/orderconsumptions.month_'.date("m", strtotime($receipt->period)))." ".date("Y", strtotime($receipt->period)) }}" autocomplete="off" readonly>
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="serialNumber">@lang('vsat/editreceipt.text_serial')</label>
                                <input type="text" name="serialNumber" id="serialNumber" class="form-control" value="{{ $receipt->serialnumber }}" autocomplete="off" maxlength="4">
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="correlativeNumber">@lang('vsat/editreceipt.text_correlative')</label>
                                <input type="text" name="correlativeNumber" id="correlativeNumber" class="form-control" value="{{ $receipt->correlative }}" autocomplete="off" maxlength="5">
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="dateCreate">@lang('vsat/editreceipt.text_datecreate')</label>
                                <input type="date" name="dateCreate" id="dateCreate" class="form-control" value="{{ $receipt->created_at->format('Y-m-d') }}" autocomplete="off">
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="dateDue">@lang('vsat/editreceipt.text_datedue')</label>
                                <input type="date" name="dateDue" id="dateDue" class="form-control" value="{{ $receipt->date_due }}" autocomplete="off">
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="currency">@lang('vsat/editreceipt.text_currency')</label>
                                <select name="currency" id="currency" class="form-control">
                                    @foreach($currencies as $currency)
                                        <option value="{{ $currency->code }}" @if($currency->code == $receipt->currency_id) selected='selected' @endif>{{ $currency->name.' - '.$currency->iso4217 }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="exchange">@lang('vsat/editreceipt.text_exchange')</label>
                                <input type="text" name="exchange" id="exchange" class="form-control" value="{{ $receipt->exchange }}" autocomplete="off">
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="totalPay">@lang('vsat/editreceipt.text_totalpay')</label>
                                <input type="text" name="totalPay" id="totalPay" class="form-control" value="{{ $receipt->usdtotal }}" autocomplete="off" readonly>
                            </div>
                            <div class="col-sm-6 py-2">
                                <label for="totalCost">@lang('vsat/editreceipt.text_cost')</label>
                                <input type="text" name="totalCost" id="totalCost" class="form-control" value="{{ $receipt->usdcost }}" autocomplete="off" readonly>
                            </div>
                            <div class="col-sm-12 py-2">
                                <label for="anotations">@lang('vsat/editreceipt.text_notes')</label>
                                <textarea name="anotations" id="anotations" class="form-control">{{ $receipt->anotations }}</textarea>
                            </div>
                            <div class="col-sm-12 py-4">
                                <button class="btn btn-primary btn-block" onclick="updateForm();">
                                        @lang('vsat/editreceipt.btn_save')
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    @if($historyUpdates->count() > 0)
        <div class="row mb-4">
                <div class="col-xl-8 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 py-2">
                                    <h4 class="card-title text-dark">@lang('vsat/editreceipt.title_updates')</h4>
                                </div>
                                <div class="col-sm-12 py-2">

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="timeline">
                                                    @foreach($historyUpdates as $item)
                                                        <li>
                                                            <span class="text-dark font-weight-bold">
                                                                <i class="fas fa-angle-double-right"></i> {{ ucwords($item->anotations) }}
                                                            </span>
                                                            
                                                            <label class="float-right">
                                                                {{ $item->created_at->diffForHumans() }}
                                                            </label>
                    
                                                            <p style="margin-left: 15px; margin-top:10px;" >
                                                                <span class="font-weight-bold">@lang('vsat/editreceipt.text_updates')</span><br />
                                                                @if(strlen($item->updates) > 0)
                                                                    {!! $item->updates !!}
                                                                @endif
                    
                                                            </p>
                                                            <p align="right">
                                                                @lang('vsat/editreceipt.table_by', ['name' => $item->name, 'lastname' => $item->lastname]) <img class="img-profile rounded-circle" src="{{ $item->imgprofile }}" width="20px">
                                                                <hr />
                                                            </p>
                                                        </li>
                                                    @endforeach
                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
@endsection

@section('scripts')
<script language="javascript">
    let modal_updated = "{{ __('vsat/editreceipt.modal_updated') }}";
    let modal_complete = "{{ __('vsat/editreceipt.modal_complete') }}";

</script>
<script src="/js/vsat/editreceipt.js"></script>
@endsection
