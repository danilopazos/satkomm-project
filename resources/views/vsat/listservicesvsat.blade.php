@extends('layouts.dashboard')
@section('page_title', __('vsat/listservicevsat.title_page'))
@section('info_title', __('vsat/listservicevsat.title_page'))

@section('putButton')
    <a href="/newordenservicevsat" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
        </span>
            <span class="text">{{ __('vsat/listservicevsat.btn_neworden') }}</span>
    </a>
@endsection

@section('content')

    @if (session('status'))
        @include('partials._successAlert', ['message' => __('vsat/listservicevsat.message_saveok')])
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ __('vsat/listservicevsat.tabletitle_list') }}</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                @if( $listOrders->count() == 0 )
                    @include('partials._noresults',[
                        'title' => __('vsat/listservicevsat.text_noresults'),
                        'text' => __('vsat/listservicevsat.text_calltoaction'),
                        'textBtn' => __('vsat/listservicevsat.btn_neworden'),
                        'urlBtn' => '/newordenservicevsat'
                    ])
                @else
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="min-width: 120em;">
                        <thead>
                        <tr>
                            <th>{{ __('vsat/listservicevsat.table_check') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_ordenservice') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_status') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_sancode') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_customer') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_typeservice') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_activedate') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_cancel') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_plan') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_price') }}</th>
                            <th>{{ __('vsat/listservicevsat.table_created') }}</th>
                        </tr>
                        </thead>    
                        <tbody>

                            @foreach ($listOrders as $listOrder)
                                <tr>
                                    <td>
                                        <a href="/vieworderservicevsat/{{ $listOrder->code }}" class="btn btn-success btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                            <i class="fas fa-search"></i>
                                            </span>
                                            <span class="text">{{ __('vsat/listservicevsat.table_check') }}</span>
                                        </a>
                                    </td>
                                    <td>{{ $listOrder->code }}</td>
                                    <td>
                                        <span class="{{ $listOrder->stylealert }}">
                                            <i class="fas fa-{{ $listOrder->iconalert }}"></i> {{ $listOrder->status }}
                                        </span>
                                    </td>
                                    <td>{{ $listOrder->sancode }}</td>
                                    <td>
                                        {{ $listOrder->customer }}<br />
                                        {{ $listOrder->addressname }}<br />
                                        {{ $listOrder->district.", ".$listOrder->province.", ".$listOrder->department }}</td>
                                    <td>
                                        @if ($listOrder->typeservice_id == "AC")
                                            <span class="boxAlertGreen">
                                                <i class="fas fa-check"></i> {{ $listOrder->typeservice }}
                                            </span>

                                        @elseif ($listOrder->typeservice_id == "DM")
                                            <span class="boxAlertYellow">
                                                <i class="fas fa-exclamation"></i> {{ $listOrder->typeservice }}
                                            </span>

                                        @endif

                                    </td>
                                    <td>
                                        @if (strlen($listOrder->date_start) > 0)
                                            {{ ($listOrder->date_start)->format('d')." ".__('vsat/orderconsumptions.month_'.($listOrder->date_start)->format('m'))." ".($listOrder->date_start)->format('Y') }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @if (strlen($listOrder->date_end) > 0)
                                            {{ ($listOrder->date_end)->format('d')." ".__('vsat/orderconsumptions.month_'.($listOrder->date_end)->format('m'))." ".($listOrder->date_end)->format('Y') }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @php ( $total = 0 )
                                        @foreach($listPlans as $plan)
                                            @if($plan->orderId == $listOrder->code)
                                                {{ $plan->namePlan }} <br />
                                                @php ( $total = $total + $plan->pricePlan )
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>$ {{ number_format($total, 2) }}</td>
                                    <td>
                                        {{ ($listOrder->created_at)->format('d')." ".__('vsat/orderconsumptions.month_'.($listOrder->created_at)->format('m'))." ".($listOrder->created_at)->format('Y') }}
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                @endif
            </div>
        </div>
    </div>

@endsection
