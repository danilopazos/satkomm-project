@extends('layouts.dashboard')
@section('page_title', __('vsat/orderconsumptions.title_page'))
@section('info_title')
    {{ __('vsat/orderconsumptions.title_page')  }}
    @if (isset($period))
        {{ " : ".__('vsat/orderconsumptions.month_'.date('m', strtotime($period)))." ".date('Y', strtotime($period)) }}
    @endif
@endsection

@if (isset($consumptionList))
    @section('putButton')
        <div class="btn-group">
            <a href="#" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-th"></i>&nbsp;&nbsp;
                {{ __('vsat/neworderservicevsat.btn_moreoptions') }}
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="/viewordersconsumptions"><i class="fas fa-search"></i> {{ __('vsat/orderconsumptions.btn_consultagain') }}</a>
                <a class="dropdown-item" href="/listinvoicesvsat"><i class="fas fa-file-invoice-dollar"></i> {{ __('vsat/orderconsumptions.btn_viewinvoices') }}</a>
                @if (count($consumptionList) > 0)
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="return createReceipts();"><i class="fas fa-tasks"></i> {{ __('vsat/orderconsumptions.table_createreceipt') }}</a>
                @endif
            </div>
        </div>
    @endsection
@endif

@section('content')

    <div class="row mb-4">
        @if (isset($arrayPeriods))
        <div class="col-xl-6 col-sm-12">
            <div class="card">
                <div class="card-body">

                    <form method="post" action="/viewordersconsumptions">
                    @csrf
                    <div class="row">
                        @if(count($arrayPeriods) > 0)
                            <div class="col-sm-12 py-2">
                                <label for="selectTypeProduct">{{ __('vsat/orderconsumptions.text_periods')  }}</label>
                                <select class="form-control" name="selectPeriod" id="selectPeriod">
                                    @foreach($arrayPeriods as $period)
                                        <option value="{{ $period }}">{{ __('vsat/orderconsumptions.month_'.substr($period, 5, 2)) ." " . substr($period, 0, 4) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-12 py-4">
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('vsat/orderconsumptions.btn_view')  }}
                                </button>
                            </div>
                        @else
                            <div class="col-sm-12 py-4">
                                <div class="col-cm-12 py-4">
                                    @include('partials._noresults',[
                                        'title' => __('vsat/orderconsumptions.text_noperiods'),
                                        'text' => __('vsat/orderconsumptions.text_calltoaction'),
                                        'textBtn' => __('vsat/orderconsumptions.btn_calltoaction'),
                                        'urlBtn' => '/listservicesvsat'
                                    ])
                                </div>
                            </div>
                        @endif
                    </div>
                    </form>

                </div>
            </div>
        </div>
        @endif

        @if (isset($consumptionList))

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="formOrderConsumption">
                            @csrf
                            <input type="hidden" id="hiddenPeriod" name="hiddenPeriod" value="{{ $period }}">
                            <div class="table-responsive">
                                @if (count($consumptionList) > 0)
                                    <table class="table table-bordered" width="100%" cellspacing="0" style="min-width: 70em;">
                                        <thead>
                                            <tr class="bg-gray-200 text-dark">
                                                <th width="20%">{{ __('vsat/orderconsumptions.table_company') }}</th>
                                                <th>{{ __('vsat/orderconsumptions.table_codessan') }}</th>
                                                <th align="center" width="15%">{{ __('vsat/orderconsumptions.table_amountpay') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($consumptionList as $consumption)
                                            @php($totalPerCustomer = 0)
                                                <tr>
                                                    <td class="text-dark">{{ $consumption['companyName'] }}</td>
                                                    <td>
                                                        @foreach($consumption['ordersList'] as $order)
                                                            @php($totalDiscount = round($order['totalDiscount'] * $consumption['currencyExchange'], 2))
                                                            <table width="100%">
                                                                <tr class="bg-gray-200 font-weight-bold text-dark">
                                                                    <td width="12%">{{ __('vsat/orderconsumptions.table_order') }}</td>
                                                                    <td>{{ __('vsat/orderconsumptions.table_location') }}</td>
                                                                    <td width="15%">{{ __('vsat/orderconsumptions.table_sancode') }}</td>
                                                                    <td width="15%">{{ __('vsat/orderconsumptions.table_status') }}</td>
                                                                    <td width="15%">{{ __('vsat/orderconsumptions.table_dateactive') }}</td>
                                                                    <td width="15%">{{ __('vsat/orderconsumptions.table_datecancel') }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td rowspan="{{ count($order['listPlans']) + 2 }}">
                                                                        <a href="/vieworderservicevsat/{{ $order['orderCode'] }}" target="_blank">{{ $order['orderCode'] }}</a>
                                                                    </td>
                                                                    <td>{{ $order['addressName'] }} <br /> {{ $order['addressDescription'] }}</td>
                                                                    <td>{{ $order['sanCode'] }}</td>
                                                                    <td><span class="{{ $order['statusStyle'] }}"><i class="fas fa-{{ $order['statusIcon'] }}"></i> {{ $order['statusName'] }}</span></td>
                                                                    <td>
                                                                        {{ date('d', strtotime($order['dateActive'])).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($order['dateActive']))).' '.date('Y', strtotime($order['dateActive'])) }}
                                                                    </td>
                                                                    <td>
                                                                        @if($order['dateEnd'] != null)
                                                                            {{ date('d', strtotime($order['dateEnd'])).' '.__('vsat/orderconsumptions.month_'.date('m', strtotime($order['dateEnd']))).' '.date('Y', strtotime($order['dateEnd'])) }}
                                                                        @else
                                                                            -
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <tr class="bg-gray-200 text-dark">
                                                                    <td>{{ __('vsat/orderconsumptions.table_typeplan') }}</td>
                                                                    <td>{{ __('vsat/orderconsumptions.table_plan') }}</td>
                                                                    <td>{{ __('vsat/orderconsumptions.table_priceplan') }}</td>
                                                                    <td>{{ __('vsat/orderconsumptions.table_apportionment') }}</td>
                                                                    <td>{{ __('vsat/orderconsumptions.table_total') }}</td>
                                                                </tr>
                                                                @foreach($order['listPlans'] as $plan)
                                                                    <tr>
                                                                        <td>{{ $plan['descriptionTypePlan'] }}</td>
                                                                        <td>{{ $plan['namePlan'] }}</td>
                                                                        <td>{{ $consumption['currencySymbol'].' '.number_format($plan['pricePlan'] * $consumption['currencyExchange'], 2).' '.$consumption['currencyIso'] }}</td>
                                                                        <td>
                                                                            @if($plan['typePlan'] == 'PS')
                                                                                {{ __('vsat/orderconsumptions.table_days', ['days' => $order['daysToPay']]) }}
                                                                            @else
                                                                                {{ __('vsat/orderconsumptions.table_noapply') }}
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            @if($plan['typePlan'] == 'PS')
                                                                                @php($totalConsumption = round(($plan['pricePlan'] * $consumption['currencyExchange'] / $order['maxDay']) * $order['daysToPay'], 2))
                                                                                {{ $consumption['currencySymbol'].' '.number_format($totalConsumption, 2).' '.$consumption['currencyIso'] }}
                                                                            @else
                                                                                @php( $totalConsumption = round($plan['pricePlan'] * $consumption['currencyExchange'], 2) )
                                                                                {{ $consumption['currencySymbol'].' '.number_format($totalConsumption, 2).' '.$consumption['currencyIso'] }}
                                                                            @endif
                                                                            @php($totalPerCustomer += $totalConsumption)
                                                                        </td>
                                                                    </tr>
                                                                @endforeach

                                                                @if($totalDiscount > 0)
                                                                    @php($totalPerCustomer -= $totalDiscount)
                                                                    <tr class="text-danger">
                                                                        <td colspan='5'>Descuentos totales</td>
                                                                        <td>{{ $consumption['currencySymbol'].' -'.number_format($totalDiscount, 2).' '.$consumption['currencyIso'] }}</td>
                                                                    </tr>
                                                                @endif
                                                            </table>
                                                            <br />
                                                        @endforeach
                                                    </td>
                                                    <td class="text-dark">
                                                        {{ $consumption['currencySymbol'].' '.number_format($totalPerCustomer, 2).' '.$consumption['currencyIso'] }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    @include('partials._noresults',[
                                        'title' => __('vsat/orderconsumptions.text_allcomplete'),
                                        'text' => __('vsat/orderconsumptions.text_calltoactioncomplete'),
                                        'textBtn' => __('vsat/orderconsumptions.btn_consultagain'),
                                        'urlBtn' => '/viewordersconsumptions'
                                    ])
                                @endif
                            </div>

                            </form>
                        </div>
                    </div>
                </div>
        @endif

    </div>

@endsection

@section('scripts')
    <script language="javascript">
        let modal_created = "{{ __('vsat/orderconsumptions.modal_created') }}";
    </script>
    <script src="/js/vsat/createinvoice.js"></script>
@endsection
