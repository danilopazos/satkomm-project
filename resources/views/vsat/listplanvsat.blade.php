@extends('layouts.dashboard')
@section('page_title', __('vsat/listplanvsat.title_page'))
@section('info_title', __('vsat/listplanvsat.title_page'))

@section('putButton')
    <a href="/newvsatplan" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
        </span>
            <span class="text">{{ __('vsat/listplanvsat.btn_newplan') }}</span>
    </a>
@endsection

@section('content')

    @if (session('status'))
        @include('partials._successAlert', ['message' => __('vsat/listplanvsat.message_saveok')])
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ __('vsat/listplanvsat.tabletitle_list') }}</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                @if (count($arrayPlans) == 0)
                    @include('partials._noresults',[
                        'title' => __('vsat/listplanvsat.text_noresults'),
                        'text' => __('vsat/listplanvsat.text_calltoaction'),
                        'textBtn' => __('vsat/listplanvsat.btn_newplan'),
                        'urlBtn' => '/newvsatplan'
                    ])
                @else
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>{{ __('vsat/listplanvsat.table_plan') }}</th>
                            <th>{{ __('vsat/listplanvsat.table_type') }}</th>
                            <th>{{ __('vsat/listplanvsat.bandtype') }}</th>
                            <th>{{ __('vsat/listplanvsat.table_actives') }}</th>
                            <th>{{ __('vsat/listplanvsat.table_check') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                            @foreach ($arrayPlans as $plan)
                            <tr>
                                <td>{{ $plan['description'] }}</td>
                                <td>{{ $plan['plan'] }}</td>
                                <td>{{ $plan['band'] }}</td>
                                <td>
                                    @if($plan['codePlan'] == 'PS')
                                        <span class="text-primary">{{ __('vsat/listplanvsat.text_actives', ['number' => $plan['qtyActives']]) }}</span>
                                    @else
                                        {{ __('vsat/listplanvsat.text_nopostpaid') }}
                                    @endif
                                </td>
                                <td>
                                    <a href="/showplan/{{ $plan['code'] }}" class="btn btn-success btn-icon-split btn-sm">
                                        <span class="icon text-white-50">
                                        <i class="fas fa-search"></i>
                                        </span>
                                        <span class="text">{{ __('vsat/listplanvsat.table_check') }}</span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>

@endsection
