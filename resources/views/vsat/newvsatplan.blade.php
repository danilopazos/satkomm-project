@extends('layouts.dashboard')
@section('page_title', __('vsat/newvsatplan.title_page'))
@section('info_title', __('vsat/newvsatplan.title_page'))

@section('content')

    <div class="row mb-4">
        <div class="col-xl-8 col-sm-12">
            <div class="card">
                <div class="card-body">

                    <form id="formSavePlan" method="post" onsubmit="return savePlan();">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h4 class="card-title text-dark">{{ __('vsat/newvsatplan.subtitle_information')  }}</h4>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectBand">{{ __('vsat/newvsatplan.text_typeband')  }}</label>
                            <select name="selectBand" id="selectBand" class="form-control">
                                @foreach ($vsatRecords as $vsatRecord)
                                    <option value="{{ $vsatRecord->code }}">{{ $vsatRecord->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypePlan">{{ __('vsat/newvsatplan.text_typeplan')  }}</label>
                            <select name="selectTypePlan" id="selectTypePlan" class="form-control" onchange="changeTypePlan();">
                                @foreach ($typePlans as $typePlan)
                                    <option value="{{ $typePlan->code }}">{{ $typePlan->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="textPlanName">{{ __('vsat/newvsatplan.text_planname')  }}</label>
                            <input type="text" class="form-control" id="textPlanName" name="textPlanName" placeholder="" autocomplete="off" value="" maxlength="38">
                        </div>
                        <div class="col-sm-12 py-2">
                            <label for="textAnotationPlan">{{ __('vsat/newvsatplan.text_plannotes')  }}</label>
                            <input type="text" class="form-control" id="textAnotationPlan" name="textAnotationPlan" placeholder="" autocomplete="off" value="" maxlength="198">
                        </div>
                        <div class="col-sm-4 py-2" id="div_download_1">
                            <label for="textDownload">{{ __('vsat/newvsatplan.text_donwload')  }}</label>
                            <input type="text" class="form-control" id="textDownload" name="textDownload" placeholder="" autocomplete="off" value="">
                        </div>
                        <div class="col-sm-2 py-2" id="div_download_2">
                            <label for="selectDownload">{{ __('vsat/newvsatplan.text_typespeed')  }}</label>
                            <select name="selectDownload" id="selectDownload" class="form-control">
                                @foreach ($techSpeeds as $techSpeed)
                                    <option value="{{ $techSpeed->code }}">{{ $techSpeed->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4 py-2" id="div_upload_1">
                            <label for="textUpload">{{ __('vsat/newvsatplan.text_upload')  }}</label>
                            <input type="text" class="form-control" id="textUpload" name="textUpload" placeholder="" autocomplete="off" value="">
                        </div>
                        <div class="col-sm-2 py-2" id="div_upload_2">
                            <label for="selectUpload">{{ __('vsat/newvsatplan.text_typespeed')  }}</label>
                            <select name="selectUpload" id="selectUpload" class="form-control">
                                @foreach ($techSpeeds as $techSpeed)
                                    <option value="{{ $techSpeed->code }}">{{ $techSpeed->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4 py-2" id="div_alltime_1">
                            <label for="textAnytime">{{ __('vsat/newvsatplan.text_anytime')  }}</label>
                            <input type="text" class="form-control" id="textAnytime" name="textAnytime" placeholder="" autocomplete="off" value="">
                        </div>
                        <div class="col-sm-2 py-2" id="div_alltime_2">
                            <label for="selectAnytime">{{ __('vsat/newvsatplan.text_typeunits')  }}</label>
                            <select name="selectAnytime" id="selectAnytime" class="form-control">
                                @foreach ($techUnits as $techUnit)
                                    <option value="{{ $techUnit->code }}">{{ $techUnit->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4 py-2" id="div_early_1">
                            <label for="textEarly">{{ __('vsat/newvsatplan.text_early')  }}</label>
                            <input type="text" class="form-control" id="textEarly" name="textEarly" placeholder="" autocomplete="off" value="">
                        </div>
                        <div class="col-sm-2 py-2" id="div_early_2">
                            <label for="selectEarly">{{ __('vsat/newvsatplan.text_typeunits')  }}</label>
                            <select name="selectEarly" id="selectEarly" class="form-control">
                                @foreach ($techUnits as $techUnit)
                                    <option value="{{ $techUnit->code }}">{{ $techUnit->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4 py-2" id="div_total_1">
                            <label for="textTotal">{{ __('vsat/newvsatplan.text_total')  }}</label>
                            <input type="text" class="form-control" id="textTotal" name="textTotal" placeholder="" autocomplete="off" value="">
                        </div>
                        <div class="col-sm-2 py-2" id="div_total_2">
                            <label for="selectTotal">{{ __('vsat/newvsatplan.text_typeunits')  }}</label>
                            <select name="selectTotal" id="selectTotal" class="form-control">
                                @foreach ($techUnits as $techUnit)
                                    <option value="{{ $techUnit->code }}">{{ $techUnit->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4 py-2" id="div_cir_1">
                            <label for="textCir">{{ __('vsat/newvsatplan.text_cir')  }}</label>
                            <input type="text" class="form-control" id="textCir" name="textCir" placeholder="" autocomplete="off" value="">
                        </div>
                        <div class="col-sm-2 py-2" id="div_cir_2">
                            <label for="selectCir">{{ __('vsat/newvsatplan.text_typespeed')  }}</label>
                            <select name="selectCir" id="selectCir" class="form-control">
                                @foreach ($techSpeeds as $techSpeed)
                                    <option value="{{ $techSpeed->code }}">{{ $techSpeed->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-12 py-2">
                            <hr />
                            <h5 class="card-title">{{ __('vsat/newvsatplan.subtitle_aboutcosts')  }}</h5>
                        </div>
                        <div class="col-sm-6 py-2" id="div_cir_2">
                            <label for="planCost">{{ __('vsat/newvsatplan.text_cost')  }}</label>
                            <input class="form-control" id="planCost" name="planCost" autocomplete="off" placeholder="$">
                        </div>
                        <div class="col-sm-6 py-2" id="div_cir_2">
                            <label for="commodeCost">{{ __('vsat/newvsatplan.text_commode')  }}</label>
                            <input class="form-control" id="commodeCost" name="commodeCost" autocomplete="off" placeholder="$">
                        </div>
                        

                        <div class="col-sm-12 py-4">
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                {{ __('vsat/newvsatplan.btn_saveplan')  }}
                            </button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let modal_saved = "{{ __('vsat/newvsatplan.modal_saved')  }}";
        let modal_complete = "{{ __('vsat/newvsatplan.modal_complete')  }}";
        
    </script>
    <script src="js/vsat/newvsatplan.js"></script>
    <script language="javascript">
        changeTypePlan();
    </script>
@endsection
