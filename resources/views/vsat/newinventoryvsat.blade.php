@extends('layouts.dashboard')
@section('page_title', __('vsat/newinventoryvsat.title_page'))
@section('info_title', __('vsat/newinventoryvsat.title_page'))

@section('content')

    <div class="row mb-4">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">

                    <form method="post" action="/startinsertitemsvsat">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">{{ __('vsat/newinventoryvsat.subtitle_productinfo')  }}</h5>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectInventory">{{ __('vsat/newinventoryvsat.text_typeinventory')  }}</label>
                            <select name="selectInventory" id="selectInventory" class="form-control" onchange="changeinventory();">
                                <option value="---">{{ __('vsat/newinventoryvsat.select_default') }}</option>
                                @foreach ($sellCategories as $sellCategory)
                                    <option value="{{ $sellCategory->code }}">{{ $sellCategory->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectTypeProduct">{{ __('vsat/newinventoryvsat.text_typeproduct')  }}</label>
                            <select name="selectTypeProduct" id="selectTypeProduct" class="form-control" onchange="changeproduct();">
                                <option value="---">{{ __('vsat/newinventoryvsat.select_default') }}</option>
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectBrandProduct">{{ __('vsat/newinventoryvsat.text_brandproduct')  }}</label>
                            <select name="selectBrandProduct" id="selectBrandProduct" class="form-control" onchange="changebrand();">
                                <option value="---">{{ __('vsat/newinventoryvsat.select_default') }}</option>
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectModelProduct">{{ __('vsat/newinventoryvsat.text_modelproduct')  }}</label>
                            <select name="selectModelProduct" id="selectModelProduct" class="form-control">
                                <option value="---">{{ __('vsat/newinventoryvsat.select_default') }}</option>
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textQtyProducts">{{ __('vsat/newinventoryvsat.text_quantity')  }}</label>
                            <input type="text" class="form-control" id="textQtyProducts" name="textQtyProducts" placeholder="" autocomplete="off" value="1">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectEntryGroup">{{ __('vsat/newinventoryvsat.text_entrygroup')  }}</label>
                            <select name="selectEntryGroup" id="selectEntryGroup" class="form-control" onchange="changeentrygroup();">
                                <option value="EGE">{{ __('vsat/newinventoryvsat.option_yes')  }}</option>
                                <option value="ENC">{{ __('vsat/newinventoryvsat.option_create')  }}</option>
                            </select>
                        </div>

                        <div class="col-sm-12 py-2" id="divGroupSelection" style="display: none;">
                            <label for="selectGroupSelection">{{ __('vsat/newinventoryvsat.select_createGroupEntry')  }}</label>
                            <select name="selectGroupSelection" id="selectGroupSelection" class="form-control" onchange="changeentrygroup();">
                                @foreach ($listgroups as $listgroup)
                                    <option value="{{ $listgroup->code }}">{{ $listgroup->description }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-12 py-2" id="divCreateGroup" style="display: none;">
                            <label for="textCreateGroup">{{ __('vsat/newinventoryvsat.text_createGroupEntry')  }}</label>
                            <input type="text" class="form-control" id="textCreateGroup" name="textCreateGroup" placeholder="" autocomplete="off" value="">
                        </div>

                        <div class="col-sm-12 py-4">
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                {{ __('vsat/newinventoryvsat.btn_start')  }}
                            </button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="js/vsat/newinventoryvsat.js"></script>
    <script>
        let optionSelect = "{{ __('vsat/newinventoryvsat.select_default') }}";
        changeentrygroup();
    </script>
@endsection
