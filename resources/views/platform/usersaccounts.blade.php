@extends('layouts.dashboard')
@section('page_title', __('platform/showusers.title_page'))
@section('info_title', __('platform/showusers.title_page'))

@section('putButton')
    <a href="/admin/newaccount" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
        </span>
        <span class="text">{{ __('platform/showusers.btn_newcustomer') }}</span>
    </a>
@endsection

@section('content')

    @if (session('status'))
        @include('partials._successAlert', ['message' => __('platform/showusers.message_saveok')])
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ __('platform/showusers.tabletitle_list') }}</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="min-width:90em;">
                    <thead>
                    <tr>
                        <th>{{ __('platform/showusers.table_name') }}</th>
                        <th>{{ __('platform/showusers.table_email') }}</th>
                        <th>{{ __('platform/showusers.table_company') }}</th>
                        <th>{{ __('platform/showusers.table_jobtitle') }}</th>
                        <th>{{ __('platform/showusers.table_usertype') }}</th>
                        <th>{{ __('platform/showusers.table_disbled') }}</th>
                        <th>{{ __('platform/showusers.table_changepass') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach ($listUsers as $item)
                            <tr @if(strlen($item->password) == 0) style="text-decoration:line-through;" @endif>
                                <td>{{ $item->name." ".$item->lastname }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->company }}</td>
                                <td>{{ $item->jobtitle }}</td>
                                <td>{{ $item->usertype }}</td>
                                    <td>
                                        <button class="btn btn-light btn-icon-split btn-sm" onclick="blockAccount('{{ $item->code }}');" @if(strlen($item->password) == 0) disabled="disabled" @endif>
                                            <span class="icon text-white-50">
                                                <i class="fas fa-ban"></i>
                                            </span>
                                            <span class="text">{{ __('platform/showusers.table_disbled') }}</span>
                                        </button>
                                    </td>
                                <td>
                                    <button class="btn btn-success btn-icon-split btn-sm" onclick="resetPassword('{{ $item->code }}');">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-lock"></i>
                                        </span>
                                        <span class="text">{{ __('platform/showusers.table_changepass') }}</span>
                                    </button>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script src="/js/platform/accounts.js"></script>
@endsection
