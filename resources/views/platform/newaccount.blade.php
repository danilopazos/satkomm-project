@extends('layouts.dashboard')
@section('page_title', __('platform/newaccount.title'))
@section('info_title', __('platform/newaccount.title'))

@section('content')
    <!-- Content Row -->
    <div class="row">

        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <!-- <h5 class="card-title">Información de la cuenta</h5> -->

                    <form method="post" action="/admin/createaccount">
                        @csrf
                    <div class="row">
                        <div class="col-sm-6 py-2">
                            <label for="textName">{{ __('platform/newaccount.text_name') }}</label>
                            <input type="text" class="form-control" id="textName" name="textName" autocomplete="off" autofocus>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textLastName">{{ __('platform/newaccount.text_lastname') }}</label>
                            <input type="text" class="form-control" id="textLastName" name="textLastName" autocomplete="off">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textTelepohne">{{ __('platform/newaccount.text_telephone') }}</label>
                            <input type="text" class="form-control" id="textTelepohne" name="textTelepohne" autocomplete="off">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textCellphone">{{ __('platform/newaccount.text_cellphone') }}</label>
                            <input type="text" class="form-control" id="textCellphone" name="textCellphone" autocomplete="off">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectCompany">{{ __('platform/newaccount.text_company') }}</label>
                            <select class="form-control" name="selectCompany" id="selectCompany">
                                <option value="---">{{ __('platform/newaccount.option_select') }}</option>
                                @foreach ($listCompanies as $company)
                                    <option value="{{ $company->code }}">{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectJobtitle">{{ __('platform/newaccount.text_jobtitle') }}</label>
                            <select class="form-control" name="selectJobtitle" id="selectJobtitle">
                                <option value="---">{{ __('platform/newaccount.option_select') }}</option>
                                @foreach ($listJobtitles as $jobtitle)
                                    <option value="{{ $jobtitle->code }}">{{ $jobtitle->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectUserType">{{ __('platform/newaccount.text_usertype') }}</label>
                            <select class="form-control" name="selectUserType" id="selectUserType">
                                <option value="---">{{ __('platform/newaccount.option_select') }}</option>
                                @foreach ($listTypes as $type)
                                    <option value="{{ $type->code }}">{{ $type->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="selectLanguage">{{ __('platform/newaccount.text_language') }}</label>
                            <select class="form-control" name="selectLanguage" id="selectLanguage">
                                <option value="---">{{ __('platform/newaccount.option_select') }}</option>
                                <option value="es">Español</option>
                                <option value="en">Inglés</option>
                            </select>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textEmail">{{ __('platform/newaccount.text_user') }}</label>
                            <input type="text" class="form-control" id="textEmail" name="textEmail" autocomplete="off">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="textPassword">{{ __('platform/newaccount.text_password') }}</label>
                            <input type="password" class="form-control" id="textPassword" name="textPassword" autocomplete="off">
                        </div>


                        <div class="col-sm-12 py-4">
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                {{ __('platform/newaccount.btn_create') }}
                            </button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>

    </div>

@endsection
