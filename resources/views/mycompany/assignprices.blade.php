@extends('layouts.dashboard')
@section('page_title', __('mycompany/assignprices.title_page'))
@section('info_title', __('mycompany/assignprices.title_page'))

@section('putButton')
    <a href="/reviewcompany/{{ $customerId }}" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
        <i class="fas fa-angle-left"></i>
        </span>
        <span class="text">{{ __('mycompany/assignprices.btn_back') }}</span>
    </a> 
@endsection

@section('content')

        <div class="row">
            <div class="col-sm-12 py-2">
                <div class="card">
                    <div class="card-body">
                        <div class="row">

                                <div class="col-sm-12 py-2 mb-4">
                                    <h4 class="card-title text-dark">{{ __('mycompany/assignprices.subtitle_page').$companyName }}</h4>
                                </div>

                                <div class="col-sm-12 py-2">
                                    <div class="table-responsive">
                                        
                                        <table id="dataTable" width="100%" class="table table-bordered" style="text-align: center; min-width: 70em;" cellspacing="0">
                                            <thead>

                                                <tr style="font-weight: bold; background-color: #f8f9f9;">
                                                    <td width="10%">{{ __('mycompany/assignprices.table_typeplan') }}</td>
                                                    <td width="15%">{{ __('mycompany/assignprices.table_plan') }}</td>
                                                    <td width="10%">{{ __('mycompany/assignprices.table_band') }}</td>
                                                    <td>{{ __('mycompany/assignprices.table_notes') }}</td>
                                                    <td width="15%">{{ __('mycompany/assignprices.table_costo') }}</td>
                                                    <td width="15%">{{ __('mycompany/assignprices.table_assigned') }}</td>
                                                    <td width="15%">{{ __('mycompany/assignprices.table_review') }}</td>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($listPlans as $plan)

                                                    <tr>
                                                        <td>{{ $plan->typePlan }}</td>
                                                        <td><a href="/editvsatplan/{{ $plan->code }}" target="_blank">{{ $plan->planName }}</a></td>
                                                        <td>{{ $plan->band }}</td>
                                                        <td>{{ $plan->planAnotation }}</td>
                                                        <td>
                                                            $ {{ number_format($plan->usdcost + $plan->commodecost, 2) }} USD
                                                        </td>
                                                        <td>
                                                            @php ($validatePrice = false)
                                                            @php ($qtyPrices = 0)

                                                            @if($plan->typePlanId == 'PS')
                                                                @foreach($postpaidPrices as $postpaid)
                                                                    @if($postpaid->vsatplan_id == $plan->code)
                                                                        @php ($validatePrice = true)
                                                                        @php ($qtyPrices = $postpaid->qtyPrices)
                                                                    @endif

                                                                @endforeach
                                                            
                                                            @else
                                                                @foreach($prepaidPrices as $prepaid)
                                                                    @if($prepaid->vsatplan_id == $plan->code)
                                                                        @php ($validatePrice = true)
                                                                        @php ($qtyPrices = $prepaid->qtyPrices)
                                                                    @endif
                                                                @endforeach

                                                            @endif

                                                            @if($validatePrice)
                                                                <span class="text-primary"><i class="fas fa-check"></i> {{ __('mycompany/assignprices.text_found', ['number' => $qtyPrices]) }}</span>
                                                                
                                                            @else
                                                                <span class="text-danger"><i class="fas fa-times"></i> {{ __('mycompany/assignprices.text_notfound') }}</span>
                                                            @endif

                                                        </td>
                                                        <td>
                                                            <button class="btn btn-success btn-icon-split btn-sm" onclick="showPrices('{{ $plan->code }}', '{{ $plan->planName }}', '{{ $plan->typePlanId }}', '{{ number_format($plan->usdcost + $plan->commodecost, 2) }}');">
                                                                <span class="icon text-white-50">
                                                                <i class="fas fa-search"></i>
                                                                </span>
                                                                <span class="text">{{ __('mycompany/assignprices.btn_review') }}</span>
                                                            </button> 
                                                        </td>
                                                    </tr>

                                                @endforeach
                                            </tbody>

                                        </table>
                                        
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    <br /><br />

@endsection

@section('scripts')
    <script>
        let customer = "{{ $customerId }}";
        let modal_btn_savebutton = "{{ __('mycompany/assignprices.modal_btn_savebutton') }}";
        let modal_btn_cancel = "{{ __('mycompany/assignprices.modal_btn_cancel') }}";
        let modal_title = "{{ __('mycompany/assignprices.modal_title') }}";
        let modal_subtitle_cost = "{{ __('mycompany/assignprices.modal_subtitle_cost') }}";
        let modal_table_description = "{{ __('mycompany/assignprices.modal_table_description') }}";
        let modal_table_price = "{{ __('mycompany/assignprices.modal_table_price') }}";
        let modal_addprice = "{{ __('mycompany/assignprices.modal_addprice') }}";
        let modal_moreconnections = "{{ __('mycompany/assignprices.modal_moreconnections') }}";
        let modal_from = "{{ __('mycompany/assignprices.modal_from') }}";
        let modal_to = "{{ __('mycompany/assignprices.modal_to') }}";
        let modal_connections = "{{ __('mycompany/assignprices.modal_connections') }}";
        let modal_months = "{{ __('mycompany/assignprices.modal_months') }}";
        let modal_withoutassign = "{{ __('mycompany/assignprices.modal_withoutassign') }}";
        let modal_typenote = "{{ __('mycompany/assignprices.modal_typenote') }}";
        let modal_typeprice = "{{ __('mycompany/assignprices.modal_typeprice') }}";
        
        let modal_saved = "{{ __('mycompany/assignprices.modal_saved') }}";
        
    </script>
    <script src="/js/mycompany/assignprices.js"></script>
@endsection