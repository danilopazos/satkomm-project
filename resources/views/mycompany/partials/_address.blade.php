<div class="row mb-5">
    <div class="col-xl-9 col-sm-12 mx-auto">
        <div class="card">
            <div class="card-body">

                <form id="formNewAddress">
                @csrf
                <input type="hidden" name="hiddenCodeAddress" id="hiddenCodeAddress" value="@isset($addressInfo->code){{ $addressInfo->code }}@endisset">
                <input type="hidden" name="hiddenCompanyCode" id="hiddenCompanyCode" value="@isset($addressInfo->companyCode){{ $addressInfo->companyCode }}@endisset">

                <div class="row">
                    <div class="col-sm-12 py-2">
                        <h5 class="card-title">{{ __('mycompany/newaddress.subtitle_infoaddress')  }}</h5>
                    </div>
                    <div class="col-sm-12 py-2">
                        <label for="nameCompany">{{ __('mycompany/newaddress.text_companyName')  }}</label>
                        @isset($addressInfo->company)
                            <input type="text" class="form-control" id="nameCompany" name="nameCompany" value="{{ $addressInfo->company }}" readonly>
                        @else
                            <select class="form-control" id="selectCompany" name="selectCompany">
                            @foreach ($allCompanies as $company)
                                @if ($companySelected == $company['code'])
                                    <option value="{{ $company['code'] }}" selected="selected">{{ $company['name'] }}</option>
                                @else
                                    <option value="{{ $company['code'] }}">{{ $company['name'] }}</option>
                                @endif
                            @endforeach
                            </select>
                        @endisset
                    </div>
                    <div class="col-sm-12 py-2">
                        <label for="selectTypeAddres">{{ __('mycompany/newaddress.text_typeaddress')  }}</label>
                        <select class="form-control" id="selectTypeAddres" name="selectTypeAddres">
                            <option value="---">{{ __('mycompany/newaddress.select_textselect') }}</option>
                            @foreach ($typeAddress as $item)
                                @isset($addressInfo->typeaddress_id)
                                    <option value="{{ $item->code }}" @if($addressInfo->typeaddress_id == $item->code) selected="selected" @endif>{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->code }}">{{ $item->name }}</option>
                                @endisset
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-12 py-2">
                        <label for="textFastAddress">{{ __('mycompany/newaddress.text_fastaddress')  }}</label>
                        <input type="text" class="form-control" id="textFastAddress" name="textFastAddress" placeholder="" autocomplete="off" value="@isset($addressInfo->addressname){{ $addressInfo->addressname }}@endisset">
                    </div>
                    <div class="col-sm-12 py-2">
                        <label for="textAddress">{{ __('mycompany/newaddress.text_address')  }}</label>
                        <input type="text" class="form-control" id="textAddress" name="textAddress" placeholder="" autocomplete="off" value="@isset($addressInfo->address){{ $addressInfo->address }}@endisset">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectCountry">{{ __('mycompany/newaddress.text_country')  }}</label>
                        <select class="form-control" id="selectCountry" name="selectCountry" onchange="changeCountry();">
                            @if(!isset($addressInfo->country))
                                <option value="---" selected="selected">{{ __('mycompany/newaddress.select_textselect') }}</option>
                            @endif
                            
                            @foreach ($countries as $country)
                                @isset($addressInfo->country)
                                    <option value="{{ $country->code }}" @if($addressInfo->country == $country->code) selected="selected" @endif>{{ $country->name }}</option>
                                @else
                                    <option value="{{ $country->code }}">{{ $country->name }}</option>
                                @endisset
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectDepartment">{{ __('mycompany/newaddress.text_department')  }}</label>
                        <select class="form-control" id="selectDepartment" name="selectDepartment" onchange="changeDepartment();">
                            @isset($addressInfo->department)
                                @foreach ($departments as $department)
                                    <option value="{{ $department->code }}" @if($addressInfo->department == $department->code) selected="selected" @endif>{{ $department->name }}</option>
                                @endforeach
                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newaddress.select_textselect') }}</option>
                            @endisset
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectProvince">{{ __('mycompany/newaddress.text_province')  }}</label>
                        <select class="form-control" id="selectProvince" name="selectProvince" onchange="changeProvince();">
                            @isset($addressInfo->province)
                                @foreach ($provinces as $province)
                                    <option value="{{ $province->code }}" @if($addressInfo->province == $province->code) selected="selected" @endif>{{ $province->name }}</option>
                                @endforeach
                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newaddress.select_textselect') }}</option>
                            @endisset
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectDistrict">{{ __('mycompany/newaddress.text_district')  }}</label>
                        <select class="form-control" id="selectDistrict" name="selectDistrict">
                            @isset($addressInfo->district)
                                @foreach ($districts as $district)
                                    <option value="{{ $district->code }}" @if($addressInfo->district == $district->code) selected="selected" @endif>{{ $district->name }}</option>
                                @endforeach
                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newaddress.select_textselect') }}</option>
                            @endisset
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textPostalCode">{{ __('mycompany/newaddress.text_postalcode')  }}</label>
                        <input type="text" class="form-control" id="textPostalCode" name="textPostalCode" placeholder="" autocomplete="off" value="@isset($addressInfo->postal_code){{ $addressInfo->postal_code }}@endisset">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textCoordinates">{{ __('mycompany/newaddress.text_coordinates')  }}</label>
                        <input type="text" class="form-control" id="textCoordinates" name="textCoordinates" placeholder="" autocomplete="off" value="@isset($addressInfo->coordinates){{ $addressInfo->coordinates }}@endisset">
                    </div>
                    <div class="col-sm-12 py-4">
                        <button type="button" class="btn btn-primary btn-user btn-block" onclick="{{ $isNewRecord ? 'saveAddress();' : 'updateAddress();' }}">
                            {{ $textBtn  }}
                        </button>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>