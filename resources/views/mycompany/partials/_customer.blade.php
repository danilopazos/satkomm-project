<div class="row mb-5">
    <div class="col-xl-9 col-sm-12 mx-auto">
        <div class="card">
            <div class="card-body">

                <form id="formNewCustomer">
                @csrf
                <input type="hidden" value="@isset($customer->code){{ $customer->code }}@endisset" id="hiddenCodeCustomer" name="hiddenCodeCustomer">
                <input type="hidden" value="@isset($address->code){{ $address->code }}@endisset" id="hiddenCodeAddress" name="hiddenCodeAddress">

                <div class="row">

                    <div class="col-sm-12 py-2">
                        <h5 class="card-title">{{ __('mycompany/newcustomer.subtitle_information') }}</h5>
                    </div>
                    
                    <div class="col-sm-12 py-2">
                        <label for="selectTypeRecord">{{ __('mycompany/mycompany.text_typerecord') }}</label>
                        <select class="form-control" name="selectTypeRecord" id="selectTypeRecord">
                            @foreach($listTypeRecord as $typeRecord)
                                @isset($customer->typecustomer_id)
                                    <option value="{{ $typeRecord->code }}" @if($customer->typecustomer_id == $typeRecord->code) selected="selected" @endif>{{ $typeRecord->description }}</option>

                                @else
                                    <option value="{{ $typeRecord->code }}">{{ $typeRecord->description }}</option>
                                    
                                @endisset
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectTypeDocument">{{ __('mycompany/newcustomer.text_typedocument')  }}</label>
                        <select class="form-control" id="selectTypeDocument" name="selectTypeDocument">
                            @foreach ($documents as $document)
                                @isset($customer->typedocument_id)
                                    <option value="{{ $document->code }}" @if($customer->typedocument_id == $document->code) selected="selected" @endif>{{ $document->description }}</option>

                                @else
                                    <option value="{{ $document->code }}">{{ $document->description }}</option>

                                @endisset
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textNumberDocument">{{ __('mycompany/newcustomer.text_numberdocument')  }}</label>
                        <input type="text" class="form-control" id="textNumberDocument" name="textNumberDocument" value="@isset($customer->taxnumber){{ $customer->taxnumber }}@endisset" autocomplete="off" maxlength="18">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textNameCompany">{{ __('mycompany/newcustomer.text_companyname')  }}</label>
                        <input type="text" class="form-control" id="textNameCompany" name="textNameCompany" maxlength="70" value="@isset($customer->name){{ $customer->name }}@endisset" autocomplete="off" value="">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textBrandCompany">{{ __('mycompany/newcustomer.text_brandname')  }}</label>
                        <input type="text" class="form-control" id="textBrandCompany" name="textBrandCompany" maxlength="70" value="@isset($customer->brand){{ $customer->brand }}@endisset" autocomplete="off" value="">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textMailCompany">{{ __('mycompany/newcustomer.text_contactmail')  }}</label>
                        <input type="text" class="form-control" id="textMailCompany" name="textMailCompany" maxlength="38" value="@isset($customer->mail){{ $customer->mail }}@endisset" autocomplete="off" value="">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textPhoneCompany">{{ __('mycompany/newcustomer.text_contactphone')  }}</label>
                        <input type="text" class="form-control" id="textPhoneCompany" name="textPhoneCompany" maxlength="14" value="@isset($customer->telephone){{ $customer->telephone }}@endisset" autocomplete="off" value="">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textWebsiteCompany">{{ __('mycompany/newcustomer.text_website')  }}</label>
                        <input type="text" class="form-control" id="textWebsiteCompany" name="textWebsiteCompany" maxlength="38" value="@isset($customer->website){{ $customer->website }}@endisset" autocomplete="off" value="">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectCurrency">{{ __('mycompany/newcustomer.text_currency')  }}</label>
                        <select class="form-control" id="selectCurrency" name="selectCurrency">
                            @foreach ($currencies as $currency)
                                @isset($customer->currency_id)
                                    <option value="{{ $currency->code }}" @if($currency->code == $customer->currency_id) selected="selected" @endif>{{ $currency->iso4217." - ".$currency->name }}</option>
                                @else
                                    <option value="{{ $currency->code }}">{{ $currency->iso4217." - ".$currency->name }}</option>

                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textCreditDays">{{ __('mycompany/newcustomer.text_creditdatys')  }}</label>
                        <input type="text" class="form-control" id="textCreditDays" name="textCreditDays" maxlength="2" value="@isset($customer->daystodue){{ $customer->daystodue }}@endisset" autocomplete="off" value="10">
                    </div>
                    <div class="col-sm-12 py-2">
                        <hr />
                        <h5 class="card-title">{{ __('mycompany/newcustomer.subtitle_billing')  }}</h5>
                    </div>
                    <div class="col-sm-12 py-2">
                        <label for="textFastAddress">{{ __('mycompany/newcustomer.text_fastaddress')  }}</label>
                        <input type="text" class="form-control" id="textFastAddress" name="textFastAddress" maxlength="240" value="@isset($address->addressname){{ $address->addressname }}@endisset" autocomplete="off" value="">
                    </div>
                    <div class="col-sm-12 py-2">
                        <label for="textAddress">{{ __('mycompany/newcustomer.text_address')  }}</label>
                        <input type="text" class="form-control" id="textAddress" name="textAddress" maxlength="440" value="@isset($address->address){{ $address->address }}@endisset" autocomplete="off" value="">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectCountry">{{ __('mycompany/newcustomer.text_country')  }}</label>
                        <select class="form-control" id="selectCountry" name="selectCountry" onchange="changeCountry();">
                            @foreach ($countries as $country)
                            @isset($address->country)
                                <option value="{{ $country->code }}" @if($country->code == $address->country) selected="selected" @endif>{{ $country->name }}</option>

                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newcustomer.select_textselect') }}</option>
                                <option value="{{ $country->code }}">{{ $country->name }}</option>

                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectDepartment">{{ __('mycompany/newcustomer.text_department')  }}</label>
                        <select class="form-control" id="selectDepartment" name="selectDepartment" onchange="changeDepartment();">
                            @isset($departments)
                                @foreach($departments as $department)
                                    <option value="{{ $department->code }}" @if($department->code == $address->department) selected="selected" @endif>{{ $department->name }}</option>
                                @endforeach

                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newcustomer.select_textselect') }}</option>

                            @endif
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectProvince">{{ __('mycompany/newcustomer.text_province')  }}</label>
                        <select class="form-control" id="selectProvince" name="selectProvince" onchange="changeProvince();">
                            @isset($provinces)
                                @foreach($provinces as $province)
                                    <option value="{{ $province->code }}" @if($province->code == $address->province) selected="selected" @endif>{{ $province->name }}</option>
                                @endforeach
                                
                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newcustomer.select_textselect') }}</option>

                            @endif
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectDistrict">{{ __('mycompany/newcustomer.text_district')  }}</label>
                        <select class="form-control" id="selectDistrict" name="selectDistrict">
                            @isset($districts)
                                @foreach($districts as $district)
                                    <option value="{{ $district->code }}" @if($district->code == $address->district) selected="selected" @endif>{{ $district->name }}</option>
                                @endforeach
                                
                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newcustomer.select_textselect') }}</option>

                            @endif
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textPostalCode">{{ __('mycompany/newcustomer.text_postalcode')  }}</label>
                        <input type="text" class="form-control" id="textPostalCode" name="textPostalCode" maxlength="18" value="@isset($address->postal_code){{ $address->postal_code }}@endisset" autocomplete="off" value="">
                    </div>

                    <div class="col-sm-12 py-4">
                        <button type="button" class="btn btn-primary btn-user btn-block" onclick="{{ $isNewRecord ? 'saveNewCustomer();' : 'updateCustomer();' }}">
                            {{ $textBtn  }}
                        </button>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>