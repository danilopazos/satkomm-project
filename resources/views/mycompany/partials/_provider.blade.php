<div class="row mb-5">
    <div class="col-xl-9 col-sm-12 mx-auto">
        <div class="card">
            <div class="card-body">

                <form id="formNewProvider">
                @csrf
                <input type="hidden" value="@isset($customer->code) {{ $customer->code }} @endisset" id="hiddenCodeCustomer" name="hiddenCodeCustomer">
                    <input type="hidden" value="@isset($address->code) {{ $address->code }} @endisset" id="hiddenCodeAddress" name="hiddenCodeAddress">
                <div class="row">
                    <div class="col-sm-12 py-2">
                        <h5 class="card-title">{{ __('mycompany/newprovider.subtitle_information')  }}</h5>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectTypeDocument">{{ __('mycompany/newprovider.text_typedocument')  }}</label>
                        <select class="form-control" id="selectTypeDocument" name="selectTypeDocument">
                            @foreach ($documents as $document)
                                @isset($customer->typedocument_id)
                                    <option value="{{ $document->code }}" @if($customer->typedocument_id == $document->code) selected="selected" @endif>{{ $document->description }}</option>

                                @else
                                    <option value="{{ $document->code }}">{{ $document->description }}</option>

                                @endisset

                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textNumberDocument">{{ __('mycompany/newprovider.text_numberdocument')  }}</label>
                        <input type="text" class="form-control" id="textNumberDocument" name="textNumberDocument" placeholder="" autocomplete="off" value="@isset($customer->taxnumber){{ $customer->taxnumber }}@endisset">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textNameCompany">{{ __('mycompany/newprovider.text_companyname')  }}</label>
                        <input type="text" class="form-control" id="textNameCompany" name="textNameCompany" placeholder="" autocomplete="off" value="@isset($customer->name){{ $customer->name }}@endisset">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textBrandCompany">{{ __('mycompany/newprovider.text_brandname')  }}</label>
                        <input type="text" class="form-control" id="textBrandCompany" name="textBrandCompany" placeholder="" autocomplete="off" value="@isset($customer->brand){{ $customer->brand }}@endisset">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textMailCompany">{{ __('mycompany/newprovider.text_contactmail')  }}</label>
                        <input type="text" class="form-control" id="textMailCompany" name="textMailCompany" placeholder="" autocomplete="off" value="@isset($customer->mail){{ $customer->mail }}@endisset">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textPhoneCompany">{{ __('mycompany/newprovider.text_contactphone')  }}</label>
                        <input type="text" class="form-control" id="textPhoneCompany" name="textPhoneCompany" placeholder="" autocomplete="off" value="@isset($customer->telephone){{ $customer->telephone }}@endisset">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textWebsiteCompany">{{ __('mycompany/newprovider.text_website')  }}</label>
                        <input type="text" class="form-control" id="textWebsiteCompany" name="textWebsiteCompany" placeholder="" autocomplete="off" value="@isset($customer->website){{ $customer->website }}@endisset">
                    </div>
                    <div class="col-sm-12 py-2">
                        <hr />
                        <h5 class="card-title">{{ __('mycompany/newprovider.subtitle_billing')  }}</h5>
                    </div>
                    <div class="col-sm-12 py-2">
                        <label for="textFastAddress">{{ __('mycompany/newprovider.text_fastaddress')  }}</label>
                        <input type="text" class="form-control" id="textFastAddress" name="textFastAddress" placeholder="" autocomplete="off" value="@isset($address->addressname){{ $address->addressname }}@endisset">
                    </div>
                    <div class="col-sm-12 py-2">
                        <label for="textAddress">{{ __('mycompany/newprovider.text_address')  }}</label>
                        <input type="text" class="form-control" id="textAddress" name="textAddress" placeholder="" autocomplete="off" value="@isset($address->address){{ $address->address }}@endisset">
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectCountry">{{ __('mycompany/newprovider.text_country')  }}</label>
                        <select class="form-control" id="selectCountry" name="selectCountry" onchange="changeCountry();">
                            <option value="---" selected="selected">{{ __('mycompany/newprovider.select_textselect') }}</option>
                            @foreach ($countries as $country)
                                @isset($address->country)
                                    <option value="{{ $country->code }}" @if($country->code == $address->country) selected="selected" @endif>{{ $country->name }}</option>
                                @else
                                    <option value="{{ $country->code }}">{{ $country->name }}</option>
                                @endisset
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectDepartment">{{ __('mycompany/newprovider.text_department')  }}</label>
                        <select class="form-control" id="selectDepartment" name="selectDepartment" onchange="changeDepartment();">
                            @isset($departments)
                                @foreach ($departments as $department)
                                    <option value="{{ $department->code }}" @if($department->code == $address->department) selected="selected" @endif>{{ $department->name }}</option>
                                @endforeach
                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newprovider.select_textselect') }}</option>
                            @endisset
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectProvince">{{ __('mycompany/newprovider.text_province')  }}</label>
                        <select class="form-control" id="selectProvince" name="selectProvince" onchange="changeProvince();">
                            @isset($provinces)
                                @foreach ($provinces as $province)
                                    <option value="{{ $province->code }}" @if($province->code == $address->province) selected="selected" @endif>{{ $province->name }}</option>
                                @endforeach
                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newprovider.select_textselect') }}</option>
                            @endisset
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="selectDistrict">{{ __('mycompany/newprovider.text_district')  }}</label>
                        <select class="form-control" id="selectDistrict" name="selectDistrict">
                            @isset($districts)
                                @foreach ($districts as $district)
                                    <option value="{{ $district->code }}" @if($district->code == $address->district) selected="selected" @endif>{{ $district->name }}</option>
                                @endforeach
                            @else
                                <option value="---" selected="selected">{{ __('mycompany/newprovider.select_textselect') }}</option>
                            @endisset
                        </select>
                    </div>
                    <div class="col-sm-6 py-2">
                        <label for="textPostalCode">{{ __('mycompany/newprovider.text_postalcode')  }}</label>
                        <input type="text" class="form-control" id="textPostalCode" name="textPostalCode" placeholder="" autocomplete="off" value="@isset($address->postal_code){{ $address->postal_code }}@endisset">
                    </div>
                    <div class="col-sm-12 py-4">
                        <button type="button" class="btn btn-primary btn-user btn-block" onclick="{{ $isNewRecord ? 'saveNewProvider();' : 'updateProvider();' }}">
                            {{ $textBtn  }}
                        </button>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>