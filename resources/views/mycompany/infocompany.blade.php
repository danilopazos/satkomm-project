@extends('layouts.dashboard')
@section('page_title', __('mycompany/mycompany.subtitle_information'))
@section('info_title', __('mycompany/mycompany.subtitle_information'))

@section('putButton')
    <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-th"></i>&nbsp;&nbsp;
                {{ __('vsat/neworderservicevsat.btn_moreoptions') }}
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="/newaddress?code={{ $company->code }}"><i class="fas fa-plus"></i> {{ __('mycompany/mycompany.btn_addaddress') }}</a>
            @if($is_customer)
                <a class="dropdown-item" href="/editcustomer/{{ $company->code }}"><i class="fas fa-pen-square"></i> {{ __('mycompany/mycompany.href_editcustomer') }}</a>
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">{{ __('mycompany/mycompany.option_aditionaltasks') }}</h6>
                <a class="dropdown-item" href="/mycompany/assignprices/{{ $company->code }}"><i class="fas fa-hand-holding-usd"></i> {{ __('mycompany/mycompany.option_listprices') }}</a>
                <a class="dropdown-item" href="#"><i class="fas fa-file-invoice-dollar"></i> {{ __('mycompany/mycompany.option_viewinvoices') }}</a>
                <a class="dropdown-item" href="/mycompany/attachdocuments/{{ $company->code }}"><i class="fas fa-upload"></i> {{ __('mycompany/mycompany.href_uploadhere') }}</a>
            @else
                <a class="dropdown-item" href="/editprovider/{{ $company->code }}"><i class="fas fa-pen-square"></i> {{ __('mycompany/mycompany.href_editcustomer') }}</a>
            @endif

        </div>
    </div>
@endsection

@section('content')

    @if (session('status'))
        @include('partials._successAlert', ['message' => __('mycompany/mycompany.saved_attach')])
    @endif

    <div class="row mb-4">
        <div class="col-sm-12 py-2">

            @if($is_customer)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-danger" role="alert">
                            <h5 class="card-title">
                                <b><i class="fas fa-tasks"></i> {{ __('vsat/neworderservicevsat.title_validation') }}</b>
                            </h5>
                            
                            <table style="margin-left: 20px;">
                                <?php
                                    foreach($docsImportantsForCustomer as $documents){

                                        $found = false; 
                                        foreach($listDocumentsForCustomer as $attached){
                                            
                                            if($documents->code == $attached->typeattach_id){
                                                $found = true;
                                                if($documents->validate_end){
                                                    //valida vencimiento
                                                    if($attached->date_expedition == null){
                                                        $expirationDocument = null;
                                                        $days_due = null;
                                                        
                                                    }else{
                                                        $expirationDocument = date("Y-m-d", strtotime($attached->date_expedition."+ 3 months"));
                                                        $now = new DateTime(date('Y-m-d'));
                                                        $due = new DateTime($expirationDocument);
                                                        $days_due = $due->diff($now)->format("%a");
                                                    }
                                                    if($expirationDocument > date('Y-m-d')){
                                                        //todo bien
                                                        ?>
                                                            <tr>
                                                                <td>
                                                                    <label class="text-success">
                                                                        <i class="fas fa-check-square"></i>
                                                                    </label>
                                                                    <label class="text-gray-800"><b style="text-decoration: underline;">{{ $documents->description }}</b>{{ __('vsat/neworderservicevsat.text_according') }}</label>
                                                                    &nbsp;&nbsp;&nbsp; <a href="/mycompany/downloadcontract/{{ $attached->document_attach }}"><i class="fas fa-download"></i> {{ __('vsat/neworderservicevsat.href_downloadattach') }}</a>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                    }else{
                                                        //documento vencido
                                                        if($days_due != null){
                                                            ?>
                                                                <tr>
                                                                    <td>
                                                                        <label class="text-warning">
                                                                            <i class="fas fa-exclamation-triangle"></i>
                                                                        </label>
                                                                        {{ __('vsat/neworderservicevsat.text_thedocument') }}<b style="text-decoration: underline;">{{ $documents->description }}</b> {{ __('vsat/neworderservicevsat.text_due') }} {{ $days_due }} {{ __('vsat/neworderservicevsat.text_daysdue') }}
                                                                        &nbsp;&nbsp;&nbsp; <a href="/mycompany/attachdocuments/{{ $company->code }}"><i class="fas fa-upload"></i> {{ __('vsat/neworderservicevsat.href_attach') }}</a>
                                                                        &nbsp;&nbsp;&nbsp; <a href="/mycompany/downloadcontract/{{ $attached->document_attach }}"><i class="fas fa-download"></i> {{ __('vsat/neworderservicevsat.href_downloadattach') }}</a>
                                                                    </td>
                                                                </tr>
                                                            <?php
                                                        }else{
                                                            //sin fecha de expedicion
                                                            ?>
                                                                <tr>
                                                                    <td>
                                                                        <label class="text-warning">
                                                                            <i class="fas fa-exclamation-triangle"></i>
                                                                        </label>
                                                                        {{ __('vsat/neworderservicevsat.text_thedocument') }}<b style="text-decoration: underline;">{{ $documents->description }}</b>{{ __('vsat/neworderservicevsat.text_notime') }}
                                                                        &nbsp;&nbsp;&nbsp; <a href="/mycompany/attachdocuments/{{ $company->code }}"><i class="fas fa-upload"></i> {{ __('vsat/neworderservicevsat.href_attach') }}</a>
                                                                        &nbsp;&nbsp;&nbsp; <a href="/mycompany/downloadcontract/{{ $attached->document_attach }}"><i class="fas fa-download"></i> {{ __('vsat/neworderservicevsat.href_downloadattach') }}</a>
                                                                    </td>
                                                                </tr>
                                                            <?php
                                                        }

                                                    }
                                                }else{
                                                    //NUNCA VENCE
                                                    ?>
                                                        <tr>
                                                            <td>
                                                                <label class="text-success">
                                                                    <i class="fas fa-check-square"></i>
                                                                </label>
                                                                <label class="text-gray-800"><b style="text-decoration: underline;">{{ $documents->description }}</b>{{ __('vsat/neworderservicevsat.text_according') }}</label>
                                                                &nbsp;&nbsp;&nbsp; <a href="/mycompany/downloadcontract/{{ $attached->document_attach }}"><i class="fas fa-download"></i> {{ __('vsat/neworderservicevsat.href_downloadattach') }}</a>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                }

                                            }
                                        }
                                        if(!$found){
                                            //NO SE ENCONTRÓ ningún documento
                                            ?>
                                                <tr>
                                                    <td>
                                                        <label class="text-danger">
                                                            <i class="fas fa-window-close"></i>
                                                        </label>
                                                        <label class="text-danger">{{ __('vsat/neworderservicevsat.text_notfound') }} <b style="text-decoration: underline;">{{ $documents->description }}</b>.</label> &nbsp;&nbsp;&nbsp; 
                                                        <a href="/mycompany/attachdocuments/{{ $company->code }}"><i class="fas fa-upload"></i> {{ __('vsat/neworderservicevsat.href_attach') }}</a>
                                                    </td>
                                                </tr>
                                            <?php

                                        }

                                    }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            @endif

            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">{{ __('mycompany/mycompany.subtitle_information')  }}</h5>
                        </div>
                        <div class="col-sm-12 py-2">
                            <table width="100%" style='margin-left:15px;'>
                                @isset($company->typeClient)
                                    <tr>
                                        <td>{{ __('mycompany/mycompany.text_typerecord') }} : </td>
                                        <td>{{ $company->typeClient }}</td>
                                    </tr>
                                @endisset
                                <tr>
                                    <td width="15%">{{ $company->typedocument }} : </td>
                                    <td>{{ $company->taxnumber }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_companyname')  }} : </td>
                                    <td>{{ $company->name }}</td>
                                </tr>
                                @if ( strlen($company->brand) > 0)
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_brandname')  }} : </td>
                                    <td>{{ $company->brand }}</td>
                                </tr>
                                @endif

                                @if ( strlen($company->mail) > 0)
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_contactmail')  }} : </td>
                                    <td>{{ $company->mail }}</td>
                                </tr>
                                @endif

                                @if ( strlen($company->telephone) > 0)
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_contactphone')  }} : </td>
                                    <td>{{ $company->telephone }}</td>
                                </tr>
                                @endif

                                @if ( strlen($company->website) > 0)
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_website')  }} : </td>
                                    <td>{{ $company->website }}</td>
                                </tr>
                                @endif

                                @isset($company->daystodue)
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_creditdays')  }} : </td>
                                    <td>{{ __('mycompany/mycompany.text_days', ['number' => $company->daystodue])  }}</td>
                                </tr>
                                @endisset

                            </table>
                        </div>

                        @if($is_customer and $contracts->count() > 0)
                            <div class="col-sm-12 py-2">
                                <hr />
                                <h5 class="card-title">{{ __('mycompany/mycompany.subtitle_contracts')  }}</h5>
                            </div>
                            
                            <div class="col-sm-12">
                                <table style='margin-left:15px;'>
                                        @foreach($contracts as $contract)
                                            <tr>
                                                <td>
                                                    <a href='/mycompany/downloadcontract/{{ $contract->document_attach }}' target='_blank'><i class="fas fa-download"></i> {{ __('mycompany/mycompany.href_download')  }}</a>
                                                    &nbsp;&nbsp;-&nbsp;&nbsp;
                                                    <b>{{ $contract->description }}</b>
                                                    
                                                </td>
                                            </tr>
                                        @endforeach 
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 py-2">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <h5 class="card-title">{{ __('mycompany/mycompany.subtitle_address')  }}</h5>
                        </div>

                        <div class="col-sm-12 py-2">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <td>{{ __('mycompany/mycompany.table_typeaddress') }}</td>
                                            <td>{{ __('mycompany/mycompany.table_description') }}</td>
                                            <td>{{ __('mycompany/mycompany.table_status') }}</td>
                                            <td>{{ __('mycompany/mycompany.table_address') }}</td>
                                            <td>{{ __('mycompany/mycompany.table_location') }}</td>
                                            <td>{{ __('mycompany/mycompany.table_postal') }}</td>
                                            <td>{{ __('mycompany/mycompany.table_edit') }}</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach( $listAddress as $address )
                                        <tr>
                                            <td>{{ $address->typeaddress }}</td>
                                            <td>{{ $address->addressname }}</td>
                                            <td>
                                                @if($address->status == true)
                                                    <div class="boxAlertGreen">
                                                        <i class="fas fa-check"></i> Activo
                                                    </div>

                                                @else
                                                    <div class="boxAlertRed">
                                                        <i class="fas fa-check"></i> No activa
                                                    </div>

                                                @endif
                                            </td>
                                            <td>{{ $address->address }}</td>
                                            <td>{{ $address->district.", ".$address->province.", ".$address->department." - ".$address->country }}</td>
                                            <td>{{ $address->postal_code }}</td>
                                            <td>
                                                <a href="/editaddress/{{ $address->code }}" class="btn btn-light btn-icon-split btn-sm">
                                                <span class="icon text-white-50">
                                                    <i class="far fa-edit"></i>
                                                </span>
                                                <span class="text">{{ __('mycompany/mycompany.table_edit') }}</span>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script language='javascript'>
        let notFiles = "{{ __('mycompany/mycompany.modal_nofiles') }}";
    </script>
    <script src='/js/mycompany/attachcontracts.js'></script>
@endsection