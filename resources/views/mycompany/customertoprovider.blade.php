@extends('layouts.dashboard')
@section('page_title', __('mycompany/mycustomers.title_page'))
@section('info_title', __('mycompany/mycustomers.title_page'))

@section('content')

    <div class="card shadow mb-4">

            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ __('mycompany/mycustomers.tabletitle_list') }}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if($customers->count() == 0)
                        @include('partials._noresults',[
                            'title' => __('mycompany/mycustomers.text_noresults'),
                            'text' => __('mycompany/mycustomers.text_calltoaction'),
                            'textBtn' => __('mycompany/mycustomers.btn_newcustomer'),
                            'urlBtn' => '/newcustomer'
                        ])
                    @else
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>{{ __('mycompany/mycustomers.table_customer') }}</th>
                                <th>{{ __('mycompany/mycustomers.table_assign') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                                @foreach ($customers as $customer)
                                <tr>
                                    <td>{{ $customer->name }}</td>
                                    <td>
                                        <a href="/likeprovider/{{ $customer->code }}" class="btn btn-info btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-arrow-right"></i>
                                            </span>
                                            <span class="text">{{ __('mycompany/mycustomers.table_assign') }}</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>

                    @endif
                </div>
            </div>
        </div>

    <br /><br />

@endsection
