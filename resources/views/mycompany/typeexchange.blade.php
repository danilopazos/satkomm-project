@extends('layouts.dashboard')
@section('page_title', __('mycompany/typeexchange.title_page'))
@section('info_title', __('mycompany/typeexchange.title_page'))

@section('putButton')
<a href="#" class="btn btn-primary btn-icon-split" onclick="return createNewCurrency();">
    <span class="icon text-white-50">
        <i class="fas fa-dollar-sign"></i>
    </span>
    <span class="text">{{ __('mycompany/typeexchange.btn_newcurrency') }}</span>
</a>
@endsection

@section('content')

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ __('mycompany/typeexchange.tabletitle_list') }}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if($currencies->count() == 0)
                        @include('partials._noresults',[
                            'title' => __('mycompany/typeexchange.text_noresults'),
                            'text' => __('mycompany/typeexchange.text_calltoaction'),
                            'textBtn' => __('mycompany/typeexchange.btn_newcurrency'),
                            'urlBtn' => '#',
                            'onclick' => "onclick='return createNewCurrency();'"
                        ])
                    @else
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>{{ __('mycompany/typeexchange.table_country') }}</th>
                                <th>{{ __('mycompany/typeexchange.table_currency') }}</th>
                                <th>{{ __('mycompany/typeexchange.table_exchange') }}</th>
                                <th>{{ __('mycompany/typeexchange.table_history') }}</th>
                                <th>{{ __('mycompany/typeexchange.table_modify') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($currencies as $item)
                                <tr>
                                    <td>{{ $item->country }}</td>
                                    <td>{{ $item->iso4217." - ".$item->name }}</td>
                                    <td>{{ $item->symbol." ".number_format($item->exchange ,3) }}</td>
                                    <td>
                                        <a href="#" class="btn btn-success btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                            <i class="fas fa-list"></i>
                                            </span>
                                            <span class="text">{{ __('mycompany/typeexchange.table_history') }}</span>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-warning btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                            <i class="far fa-edit"></i>
                                            </span>
                                            <span class="text">{{ __('mycompany/typeexchange.table_modify') }}</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

    <br /><br />

@endsection

@section ('scripts')
    <script language="javascript">
        let modal_title = "{{ __('mycompany/typeexchange.modal_title') }}";

        let modal_selectCur = "{{ __('mycompany/typeexchange.modal_selectCur') }}";
        let modal_exchange = "{{ __('mycompany/typeexchange.modal_exchange') }}";

        let modal_btnsave = "{{ __('mycompany/typeexchange.modal_btnsave') }}";
        let modal_btncancel = "{{ __('mycompany/typeexchange.modal_btncancel') }}";

        let modal_success = "{{ __('mycompany/typeexchange.modal_success') }}";
    </script>
    <script src='/js/mycompany/newexchange.js'></script>
@endsection