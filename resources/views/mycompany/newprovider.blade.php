@extends('layouts.dashboard')
@section('page_title', __('mycompany/newprovider.title_page'))
@section('info_title', __('mycompany/newprovider.title_page'))

@section('content')

    @include('mycompany/partials._provider', [
        'textBtn' => __('mycompany/newprovider.btn_savecustomer'),
        'isNewRecord' =>true
    ])

@endsection

@section('scripts')
    <script language="javascript">
        let selectText = "{{ __('mycompany/newprovider.select_textselect')  }}";
        let modal_saved = "{{ __('mycompany/newcustomer.modal_saved') }}";
        let modal_updated = "{{ __('mycompany/newcustomer.modal_updated') }}";
        let modal_complete = "{{ __('mycompany/newcustomer.modal_complete') }}";
        
    </script>
    <script src="/js/mycompany/newcustomer.js"></script>
@endsection
