@extends('layouts.dashboard')
@section('page_title', __('mycompany/banks.title_page'))
@section('info_title', __('mycompany/banks.title_page'))

@section('putButton')
<a href="#" class="btn btn-primary btn-icon-split" onclick="return createNewCurrency();">
    <span class="icon text-white-50">
        <i class="fas fa-dollar-sign"></i>
    </span>
    <span class="text">{{ __('mycompany/banks.btn_addaccount') }}</span>
</a>
@endsection

@section('content')

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ __('mycompany/banks.subtitle_accountslist') }}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if(count($listBanks) == 0)
                        @include('partials._noresults',[
                            'title' => __('mycompany/banks.title_calltoaction'),
                            'text' => __('mycompany/banks.text_calltoaction'),
                            'textBtn' => __('mycompany/banks.btn_calltoaction'),
                            'urlBtn' => '#',
                            'onclick' => "onclick='return createNewCurrency();'"
                        ])
                    @else
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>{{ __('mycompany/banks.table_image') }}</th>
                                <th>{{ __('mycompany/banks.table_bank') }}</th>
                                <th>{{ __('mycompany/banks.table_currency') }}</th>
                                <th>{{ __('mycompany/banks.table_account') }}</th>
                                <th>{{ __('mycompany/banks.table_cci') }}</th>
                                <th>{{ __('mycompany/banks.table_edit') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($listBanks as $bank)
                                <tr>
                                    <td><img src='{{ $bank->image }}' width='60px'></td>
                                    <td>{{ $bank->bankName }}</td>
                                    <td>{{ $bank->curName.' - '.$bank->iso4217 }}</td>
                                    <td>{{ $bank->number_account }}</td>
                                    <td>{{ $bank->cci_account }}</td>
                                    <td>
                                        <a href="/#/{{ $bank->code }}" class="btn btn-success btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                            <i class="far fa-edit"></i>
                                            </span>
                                            <span class="text">{{ __('mycompany/banks.table_edit') }}</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

    <br /><br />

@endsection

@section ('scripts')
    <script language="javascript">
        let modal_title = "{{ __('mycompany/banks.modal_title') }}";
        let modal_subtitle_bank = "{{ __('mycompany/banks.modal_subtitle_bank') }}";
        let modal_subtitle_currency = "{{ __('mycompany/banks.modal_subtitle_currency') }}";
        let modal_subtitle_number = "{{ __('mycompany/banks.modal_subtitle_number') }}";
        let modal_subtitle_cci = "{{ __('mycompany/banks.modal_subtitle_cci') }}";

        let modal_btnsave = "{{ __('mycompany/banks.modal_btnsave') }}";
        let modal_btncancel = "{{ __('mycompany/banks.modal_btncancel') }}";

        let modal_confirmalert = "{{ __('mycompany/banks.modal_confirmalert') }}";
    </script>
    <script src='/js/mycompany/newbank.js'></script>
@endsection