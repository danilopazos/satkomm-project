@extends('layouts.dashboard')
@section('page_title', __('mycompany/mycompany.title_page'))
@section('info_title', __('mycompany/mycompany.title_page'))

@if(auth()->user()->usertype_id == 'ALLS' or auth()->user()->usertype_id == 'ADSY')
    @section('putButton')
        <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-th"></i>&nbsp;&nbsp;
                    {{ __('mycompany/mycompany.btn_moreoptions') }}
            </button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#"><i class="fas fa-images"></i> {{ __('mycompany/mycompany.option_upload_companylogo') }}</a>
                <a class="dropdown-item" href="#"><i class="far fa-image"></i> {{ __('mycompany/mycompany.option_upload_explainimageinvoice') }}</a>
                <a class="dropdown-item" href="#"><i class="fas fa-building"></i> {{ __('mycompany/mycompany.option_edit_information') }}</a>
                <a class="dropdown-item" href="/mycompany/timefordocumentsdue"><i class="fas fa-calendar-times"></i> {{ __('mycompany/mycompany.option_edit_documentsexpiration') }}</a>
                <a class="dropdown-item" href="/mycompany/costsrecord"><i class="fas fa-dollar-sign"></i> {{ __('mycompany/mycompany.option_recordcost') }}</a>
            </div>
        </div>
    @endsection
@endif

@section('content')

    <div class="row">
        <div class="col-xl-9 col-sm-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <!-- <h5 class="card-title">Información de la cuenta</h5> -->

                    <form method="post" action="">
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <img src="{{ $company->image }}" width="150px">
                        </div>
                        <div class="col-sm-12 py-2">
                            <hr />
                            <h5 class="card-title">{{ __('mycompany/mycompany.subtitle_information')  }}</h5>
                        </div>
                        <div class="col-sm-12 py-2">
                            <table width="98%" align="right">
                                <tr>
                                    <td width="25%">{{ $company->typedocument }}</td>
                                    <td>{{ $company->taxnumber }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_companyname')  }}</td>
                                    <td>{{ $company->name }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_brandname')  }}</td>
                                    <td>{{ $company->brand }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_contactmail')  }}</td>
                                    <td>{{ $company->mail }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_contactphone')  }}</td>
                                    <td>{{ $company->telephone }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_website')  }}</td>
                                    <td><a href="{{ $company->website }}" target="_blank">{{ $company->website }}</a></td>
                                </tr>
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_currency')  }}</td>
                                    <td>{{ $company->iso4217.' - '.$company->curname }}</td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-sm-12 py-2">
                            <hr />
                            <h5 class="card-title">{{ __('mycompany/mycompany.subtitle_billing')  }}</h5>
                        </div>
                        <div class="col-sm-12 py-2">
                            <table width="98%" align="right">
                                <tr>
                                    <td width="25%">{{ __('mycompany/mycompany.text_fastaddress') }}</td>
                                    <td>{{ $address->addressname }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_address') }}</td>
                                    <td>{{ $address->address }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>{{ $address->district.', '.$address->province.', '.$address->department.', '.$address->country }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('mycompany/mycompany.text_postalcode') }}</td>
                                    <td>{{ $address->postal_code }}</td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
