@extends('layouts.dashboard')
@section('page_title', __('mycompany/newcustomer.title_pageedit'))
@section('info_title', __('mycompany/newcustomer.title_pageedit'))

@section('content')

    @include('mycompany/partials._customer', [
        'textBtn' => __('mycompany/newcustomer.btn_updatecustomer'),
        'isNewRecord' => false
    ])

@endsection

@section('scripts')
    <script language="javascript">
        let selectText = "{{ __('mycompany/newaddress.select_textselect') }}";
        let modal_saved = "{{ __('mycompany/newcustomer.modal_saved') }}";
        let modal_updated = "{{ __('mycompany/newcustomer.modal_updated') }}";
        let modal_complete = "{{ __('mycompany/newcustomer.modal_complete') }}";
        
    </script>
    <script src="/js/mycompany/newcustomer.js"></script>
@endsection
