@extends('layouts.dashboard')
@section('page_title', __('mycompany/myproviders.title_page'))
@section('info_title', __('mycompany/myproviders.title_page'))

@section('content')

    <div class="card shadow mb-4">

            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ __('mycompany/myproviders.tabletitle_list') }}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if($providers->count() == 0)
                        @include('partials._noresults',[
                            'title' => __('mycompany/myproviders.text_noresults'),
                            'text' => __('mycompany/myproviders.text_calltoaction'),
                            'textBtn' => __('mycompany/myproviders.btn_newcustomer'),
                            'urlBtn' => '/newprovider'
                        ])
                    @else
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>{{ __('mycompany/myproviders.table_customer') }}</th>
                                <th>{{ __('mycompany/myproviders.table_assign') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                                @foreach ($providers as $provider)
                                <tr>
                                    <td>{{ $provider->name }}</td>
                                    <td>
                                        <a href="/likecustomer/{{ $provider->code }}" class="btn btn-info btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-arrow-right"></i>
                                            </span>
                                            <span class="text">{{ __('mycompany/myproviders.table_assign') }}</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

    <br /><br />

@endsection
