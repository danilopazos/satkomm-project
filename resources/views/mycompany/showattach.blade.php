@extends('layouts.dashboard')
@section('page_title', __('mycompany/showattach.title_page'))
@section('info_title', __('mycompany/showattach.title_page'))

@section('putButton')

    <a href="/reviewcompany/{{ $companyCode }}" type="submit" class="btn btn-primary">
        <i class="fas fa-chevron-left"></i>&nbsp;&nbsp;
        {{ __('mycompany/showattach.btn_return') }}
    </a>
@endsection

@section('content')

    @if (session('status'))
        @include('partials._successAlert', ['message' => __('mycompany/showattach.save_ok')])
    @endif

    <div class="row">
        <div class="col-xl-7 col-sm-12 mx-auto py-2">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="/mycompany/attachcontracts" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="customerCode" id="customerCode" value="{{ $companyCode }}">
                        <div class="row">
                            <div class="col-sm-12 py-2">
                                <h5 class="card-title">{{ __('mycompany/showattach.subtitle_page')  }}</h5>
                            </div>

                            <div class="col-sm-12 py-2">
                                <label for="typeAttach">{{ __('mycompany/showattach.text_typeattach')  }}</label>
                                <select class="form-control" name="typeAttach" id="typeAttach">
                                    <option value="---">{{ __('mycompany/showattach.option_choose')  }}</option>
                                    @foreach($listType as $type)
                                        <option value="{{ $type->code }}">{{ $type->description }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-12 py-2">
                                <label for="textAnotations">{{ __('mycompany/showattach.text_notes')  }}</label>
                                <input type="text" class="form-control" id="textAnotations" name="textAnotations" autocomplete="off">
                            </div>

                            <div class="col-sm-6 py-2">
                                <label for="dateExpedition">{{ __('mycompany/showattach.text_expedition')  }}</label>
                                <input type="date" class="form-control" id="dateExpedition" name="dateExpedition" autocomplete="off">
                            </div>

                            <div class="col-sm-6 py-2">
                                <label for="dateExpiration">{{ __('mycompany/showattach.text_expiration')  }}</label>
                                <input type="date" class="form-control" id="dateExpiration" name="dateExpiration" autocomplete="off">
                            </div>

                            <div class="col-sm-12 py-2">
                                <div class="form-group">
                                    <label for="fileAttach">{{ __('mycompany/showattach.text_attach')  }}</label>
                                    <input type="file" class="form-control-file" id="fileAttach" name="fileAttach">
                                </div>
                            </div>

                            <div class="col-sm-12 py-2">
                                <button type="submit" class="btn btn-primary btn-block" onclick="return uploadAttach();">
                                    <i class="fas fa-upload"></i>&nbsp;&nbsp;
                                    {{ __('mycompany/showattach.btn_attach') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <br /><br />

@endsection

@section('scripts')
    <script language='javascript'>
        let notFiles = "{{ __('mycompany/showattach.modal_message') }}";
    </script>
    <script src='/js/mycompany/attachcontracts.js'></script>
@endsection