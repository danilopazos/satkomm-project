@extends('layouts.dashboard')
@section('page_title', __('mycompany/costrecord.title_page'))
@section('info_title', __('mycompany/costrecord.title_page'))

@section('putButton')
    <div class="btn-group">
        <button class="btn btn-primary btn-icon-split" onclick="newRecord();">
            <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
            </span>
            <span class="text">{{ __('mycompany/costrecord.btn_newrecord') }}</span>
        </button>
    </div>
@endsection

@section('content')

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ __('mycompany/costrecord.subtitle_page') }}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if($costsList->count() == 0)
                        @include('partials._noresults',[
                            'title' => __('mycompany/costrecord.text_noresults'),
                            'text' => __('mycompany/costrecord.text_calltoaction'),
                            'textBtn' => __('mycompany/costrecord.btn_newrecord'),
                            'urlBtn' => '#',
                            'onclick' => "onclick='newRecord();'"
                        ])
                    @else
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>{{ __('mycompany/costrecord.table_detail') }}</th>
                                <th>{{ __('mycompany/costrecord.table_date') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                                @foreach ($costsList as $cost)
                                    <tr>
                                        <td>{{ $cost->description }}</td>
                                        <td>{{ $cost->created_at->format("d")." ".__('vsat/orderconsumptions.month_'.$cost->created_at->format("m"))." ".$cost->created_at->format("Y") }}</td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

    <br /><br />

@endsection

@section('scripts')
    <script>
        let modal_save = "{{ __('mycompany/costrecord.modal_save') }}";
        let modal_cancel = "{{ __('mycompany/costrecord.modal_cancel') }}";
        let modal_title = "{{ __('mycompany/costrecord.modal_title') }}";
        let modal_text = "{{ __('mycompany/costrecord.modal_text') }}";
        let modal_saved = "{{ __('mycompany/costrecord.modal_saved') }}";

    </script>
    <script src="/js/mycompany/costrecord.js"></script>
@endsection