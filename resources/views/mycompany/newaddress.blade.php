@extends('layouts.dashboard')
@section('page_title', __('mycompany/newaddress.title_page'))
@section('info_title', __('mycompany/newaddress.title_page'))

@section('content')

    @include('mycompany/partials._address', [
        'textBtn' => __('mycompany/newaddress.btn_saveaddress'),
        'isNewRecord' => true
    ])

@endsection

@section('scripts')
    <script language="javascript">
        let selectText = "{{ __('mycompany/newaddress.select_textselect') }}";
        let modal_complete = "{{ __('mycompany/newaddress.modal_complete') }}";
        let modal_saved = "{{ __('mycompany/newaddress.modal_saved') }}";
        let modal_updated = "{{ __('mycompany/newaddress.modal_updated') }}";

    </script>
    <script src="/js/mycompany/newcustomer.js"></script>
    <script src="/js/mycompany/newaddress.js"></script>
@endsection
