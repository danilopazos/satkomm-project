@extends('layouts.dashboard')
@section('page_title', __('mycompany/mycustomers.title_page'))
@section('info_title', __('mycompany/mycustomers.title_page'))

@section('putButton')
    <div class="btn-group">
        <a href="/newcustomer" type="button" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
            </span>
            <span class="text">{{ __('mycompany/mycustomers.btn_newcustomer') }}</span>
        </a>
        <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>

        <div class="dropdown-menu">
            <a class="dropdown-item" href="/providerslist"><i class="fas fa-check-square"></i> {{ __('mycompany/mycustomers.btn_assigncustomertoprovider') }}</a>
        </div>
    </div>
@endsection

@section('content')

        @if (session('status'))
            @include('partials._successAlert', ['message' => __('mycompany/mycustomers.message_saveok')])
        @endif

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ __('mycompany/mycustomers.tabletitle_list') }}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if($customers->count() == 0)
                        @include('partials._noresults',[
                            'title' => __('mycompany/mycustomers.text_noresults'),
                            'text' => __('mycompany/mycustomers.text_calltoaction'),
                            'textBtn' => __('mycompany/mycustomers.btn_newcustomer'),
                            'urlBtn' => '/newcustomer'
                        ])
                    @else
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="min-width: 90em;">
                            <thead>
                            <tr>
                                <th>{{ __('mycompany/mycompany.text_typerecord') }}</th>
                                <th>{{ __('mycompany/mycustomers.table_customer') }}</th>
                                <th>{{ __('mycompany/mycustomers.table_code') }}</th>
                                <th>{{ __('mycompany/mycustomers.table_location') }}</th>
                                <th>{{ __('mycompany/mycustomers.table_currency') }}</th>
                                <th>{{ __('mycompany/mycustomers.table_information') }}</th>
                            </tr>
                            </thead>

                            <tbody>

                                @foreach ($customers as $customer)
                                <tr>
                                    <td>{{ $customer->typeClient }}</td>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->typedocument." ".$customer->taxnumber }}</td>
                                    <td>{{ $customer->district.", ".$customer->province }}</td>
                                    <td>{{ $customer->currency }}</td>
                                    <td>
                                        <a href="/reviewcompany/{{ $customer->customer_id }}" class="btn btn-success btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-search"></i>
                                            </span>
                                            <span class="text">{{ __('mycompany/mycustomers.table_information') }}</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

@endsection
