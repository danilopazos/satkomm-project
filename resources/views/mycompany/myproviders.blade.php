@extends('layouts.dashboard')
@section('page_title', __('mycompany/myproviders.title_page'))
@section('info_title', __('mycompany/myproviders.title_page'))

@section('putButton')
    <div class="btn-group">
        <a href="/newprovider" type="button" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
            </span>
            <span class="text">{{ __('mycompany/myproviders.btn_newcustomer') }}</span>
        </a>
        <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>

        <div class="dropdown-menu">
            <a class="dropdown-item" href="/customerslist"><i class="fas fa-check-square"></i> {{ __('mycompany/myproviders.btn_assigncustomertoprovider') }}</a>
        </div>
    </div>
@endsection

@section('content')

    @if (session('status'))
        @include('partials._successAlert', ['message' => __('mycompany/myproviders.message_saveok')])
    @endif

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ __('mycompany/myproviders.tabletitle_list') }}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if ($providers->count() == 0)
                        @include('partials._noresults',[
                            'title' => __('mycompany/myproviders.text_noresults'),
                            'text' => __('mycompany/myproviders.text_calltoaction'),
                            'textBtn' => __('mycompany/myproviders.btn_newcustomer'),
                            'urlBtn' => '/newprovider'
                        ])
                    @else
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="min-width: 70em;">
                            <thead>
                            <tr>
                                <th>{{ __('mycompany/myproviders.table_provider') }}</th>
                                <th>{{ __('mycompany/myproviders.table_code') }}</th>
                                <th>{{ __('mycompany/myproviders.table_location') }}</th>
                                <th width="10%">{{ __('mycompany/myproviders.table_edit') }}</th>
                                <th width="10%">{{ __('mycompany/myproviders.table_information') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                                @foreach ($providers as $provider)
                                <tr>
                                    <td>{{ $provider->name }}</td>
                                    <td>{{ $provider->taxnumber }}</td>
                                    <td>{{ $provider->district.", ".$provider->province }}</td>
                                    <td>
                                        <a href="/editprovider/{{ $provider->provider_id }}" class="btn btn-light btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="far fa-edit"></i>
                                            </span>
                                            <span class="text">{{ __('mycompany/myproviders.table_edit') }}</span>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/reviewcompany/{{ $provider->provider_id }}" class="btn btn-success btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                            <i class="fas fa-search"></i>
                                            </span>
                                            <span class="text">{{ __('mycompany/myproviders.table_information') }}</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

    <br /><br />

@endsection
