@extends('layouts.dashboard')
@section('page_title', __('mycompany/editdocuments.title_page'))
@section('info_title', __('mycompany/editdocuments.title_page'))

@section('content')
    <!-- Content Row -->
    <div class="row">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    
                    <div class="row">
                        <form id="formDueDocuments" onsubmit="return false;">
                            @csrf
                            <div class="col-sm-12 py-2">
                                <h5 class="card-title">{{ __('mycompany/editdocuments.subtitle_page') }}</h5>
                            </div>

                            <div class="col-sm-12 py-2">
                                <table style="margin-left: 15px">
                                    <tr>
                                        <td>{{ __('mycompany/editdocuments.text_term')  }}</td>
                                        <td><input type="text" class="form-control" style="margin-left: 10px;" name="monthsDue" id="monthsDue" autocomplete="off" value="{{ $timeToDue }}"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <button class="btn btn-primary" style="margin-left: 10px; margin-top: 10px;" type="button" onclick="return updateMonths();">
                                                <i class="fas fa-check"></i>&nbsp;&nbsp;&nbsp;{{ __('mycompany/editdocuments.btn_update') }}
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                        <div class="col-sm-12 py-2">
                            <hr />
                        </div>

                        <div class="col-sm-12 py-2">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr style="font-weight: bold; background-color: #f8f9f9;">
                                            <td>{{ __('mycompany/editdocuments.table_description') }}</td>
                                            <td>{{ __('mycompany/editdocuments.table_forcustomer') }}</td>
                                            <td>{{ __('mycompany/editdocuments.table_fororder') }}</td>
                                            <td>{{ __('mycompany/editdocuments.table_due') }}</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($listAttach as $item)
                                        <tr>
                                            <td>{{ $item->description }}</td>
                                            <td>
                                                @if($item->forcustomers)
                                                {{ __('mycompany/editdocuments.text_yes') }}
                                                    @if($item->important)
                                                        : <b style="text-decoration: underline;">{{ __('mycompany/editdocuments.text_required') }}</b>
                                                    @endif
                                                @else
                                                    -
                                                @endif

                                                
                                            </td>
                                            <td>
                                                @if($item->fororders)
                                                    {{ __('mycompany/editdocuments.text_yes') }}
                                                    @if($item->important)
                                                        : <b style="text-decoration: underline;">{{ __('mycompany/editdocuments.text_required') }}</b>
                                                    @endif
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->validate_end)
                                                    {{ __('mycompany/editdocuments.text_yes') }}
                                                    @if($timeToDue > 0)
                                                            : <b style="text-decoration: underline;">{{$timeToDue }} {{ __('mycompany/editdocuments.text_months') }}</b>
                                                    @endif
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <br /><br />

@endsection

@section('scripts')
    <script language="javascript">
        let modal_saveok = "{{ __('mycompany/editdocuments.modal_saveok') }}";
        let modal_minterm = "{{ __('mycompany/editdocuments.modal_minterm') }}";
        let modal_needtype = "{{ __('mycompany/editdocuments.modal_needtype') }}";
    </script>
    <script src="/js/mycompany/updatedocumentdue.js"></script>
@endsection