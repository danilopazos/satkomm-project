@extends('layouts.dashboard')
@section('page_title', __('dashboard/texts.title_dashboard'))
@section('info_title', __('dashboard/texts.title_dashboard')." : ".__('vsat/orderconsumptions.month_'.substr($period, 5, 2))." ".substr($period, 0, 4))

@section('putButton')
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ __('dashboard/texts.btn_report').__('vsat/orderconsumptions.month_'.substr($period, 5, 2))." ".substr($period, 0, 4) }}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @foreach ($months as $month)
                <a class="dropdown-item" href="/home?period={{ date("Y-m", strtotime($month->period)) }}">
                    {{ __('vsat/orderconsumptions.month_'.substr($month->period, 5, 2))." ".substr($month->period, 0, 4) }}
                </a>
            @endforeach
            <input type="hidden" id="hiddenPeriod" name="hiddenPeriod" value="{{ $period }}">
        </div>
    </div>
@endsection

@section('content')
    <!-- Content Row -->
    <div class="row">

        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">{{ __('dashboard/texts.subtitle_orderscomplete').__('vsat/orderconsumptions.month_'.substr($period, 5, 2))." ".substr($period, 0, 4) }}</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <span class="h5 mb-0 mr-1 font-weight-bold text-success">{{ $dataSummary['services'] }}</span>
                                    <span class="h6 mb-0 mr-2 font-weight-bold text-gray">{{ "/ ".$dataSummary['fullServices'] }}</span>
                                </div>
                                <div class="col">
                                   <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: {{ $dataSummary['percentServices'] }}%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-file-invoice-dollar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('dashboard/texts.subtitle_earnings').__('vsat/orderconsumptions.month_'.substr($period, 5, 2))." ".substr($period, 0, 4) }}</div>
                            <div class="h5 mb-0 font-weight-bold text-primary">$ {{ number_format( $dataSummary['earnings'] , 2) }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">{{ __('dashboard/texts.subtitle_totalactive').__('vsat/orderconsumptions.month_'.substr($period, 5, 2))." ".substr($period, 0, 4) }}</div>
                            <div class="h5 mb-0 font-weight-bold text-info">{{ $dataSummary['activations'].__('dashboard/texts.text_totalactive') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-check fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">{{ __('dashboard/texts.subtitle_ordersdue').__('vsat/orderconsumptions.month_'.substr($period, 5, 2))." ".substr($period, 0, 4) }}</div>
                            <div class="h5 mb-0 font-weight-bold text-danger">{{ $dataSummary['toDue'] }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-exclamation-circle fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">{{ __('dashboard/texts.subtitle_historial').__('vsat/orderconsumptions.month_'.substr($period, 5, 2))." ".substr($period, 0, 4) }}</h6>

                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">{{ __('dashboard/texts.leftmenu_periodsvsat') }}</div>
                            <a class="dropdown-item" href="/listinvoicesvsat">{{ __('dashboard/texts.leftmenu_viewinvoices') }}</a>
                            <a class="dropdown-item" href="/viewordersconsumptions">{{ __('dashboard/texts.leftmenu_viewconsumptions') }}</a>
                            <!--
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                            -->
                        </div>
                    </div>

                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myAreaChart"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pie Chart -->
        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">{{ __('dashboard/texts.subtitle_inventory') }}</h6>

                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">{{ __('dashboard/texts.leftmenu_inventoryvsat') }}</div>
                            <a class="dropdown-item" href="/listinventorysvsat">{{ __('dashboard/texts.leftmenu_inventoryview') }}</a>
                            <a class="dropdown-item" href="/newinventoryvsat">{{ __('dashboard/texts.leftmenu_inventorynew') }}</a>
                            <!--
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                            -->
                        </div>

                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie">
                        <canvas id="myPieChart"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section ('scripts')
    <script>
        let text_earnings = "{{ __('vsat/orderconsumptions.text_earnings') }}";
        let text_exits = "{{ __('vsat/orderconsumptions.text_exits') }}";
        let text_total = "{{ __('vsat/orderconsumptions.text_total') }}";
        let text_items = "{{ __('vsat/orderconsumptions.text_items') }}";
        let text_inventory = "{{ __('vsat/orderconsumptions.text_inventory') }}";
        let month_1 = "{{ __('vsat/orderconsumptions.month_1') }}";
        let month_2 = "{{ __('vsat/orderconsumptions.month_2') }}";
        let month_3 = "{{ __('vsat/orderconsumptions.month_3') }}";
        let month_4 = "{{ __('vsat/orderconsumptions.month_4') }}";
        let month_5 = "{{ __('vsat/orderconsumptions.month_5') }}";
        let month_6 = "{{ __('vsat/orderconsumptions.month_6') }}";
        let month_7 = "{{ __('vsat/orderconsumptions.month_7') }}";
        let month_8 = "{{ __('vsat/orderconsumptions.month_8') }}";
        let month_9 = "{{ __('vsat/orderconsumptions.month_9') }}";
        let month_10 = "{{ __('vsat/orderconsumptions.month_10') }}";
        let month_11 = "{{ __('vsat/orderconsumptions.month_11') }}";
        let month_12 = "{{ __('vsat/orderconsumptions.month_12') }}";

    </script>
    <script src="/vendor/chart.js/Chart.min.js"></script>
    <script src="/js/dashboard/history-earnings.js"></script>
    <script src="/js/dashboard/history-inventory.js"></script>
@endsection
