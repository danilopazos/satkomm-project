@extends('layouts.dashboard')
@section('page_title', __('dashboard/texts.title_changepassword'))
@section('info_title', __('dashboard/texts.title_changepassword'))

@section('content')
    <!-- Content Row -->
    <div class="row">

        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <!-- <h5 class="card-title">Información de la cuenta</h5> -->

                    <form id="formChangePassword">
                        @csrf
                    <div class="row">
                        <div class="col-sm-6 py-2">
                            <label for="userName">{{ __('dashboard/texts.text_username') }}</label>
                            <input type="text" class="form-control" id="userName" name="userName" placeholder="" autocomplete="off" value="{{ auth()->user()->email }}" disabled>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="userPassword">{{ __('dashboard/texts.text_password') }}</label>
                            <input type="password" class="form-control" id="userPassword" name="userPassword" placeholder="" autocomplete="off" value="" autofocus>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="userNewPass">{{ __('dashboard/texts.text_newpass') }}</label>
                            <input type="password" class="form-control" id="userNewPass" name="userNewPass" placeholder="" autocomplete="off" value="">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="userRepPass">{{ __('dashboard/texts.text_reppass') }}</label>
                            <input type="password" class="form-control" id="userRepPass" name="userRepPass" placeholder="" autocomplete="off" value="">
                        </div>
                        <div class="col-sm-12 py-4">
                            <button type="button" class="btn btn-primary btn-user btn-block" onclick="changePassword();">
                                {{ __('dashboard/texts.btn_savepass') }}
                            </button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script language="javascript">
        let modal_complete = "{{ __('dashboard/texts.modal_complete') }}";
        let modal_passsaved = "{{ __('dashboard/texts.modal_passsaved') }}";
        let modal_passincorrect = "{{ __('dashboard/texts.modal_passincorrect') }}";
        let modal_passdifferent = "{{ __('dashboard/texts.modal_passdifferent') }}";
    </script>
    <script src="/js/myaccount/changePassword.js"></script>
@endsection
