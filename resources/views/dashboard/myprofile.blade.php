@extends('layouts.dashboard')
@section('page_title', __('dashboard/texts.title_myprofile'))
@section('info_title', __('dashboard/texts.title_myprofile'))

@section('content')
    <!-- Content Row -->
    <div class="row">

        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <!-- <h5 class="card-title">Información de la cuenta</h5> -->

                    <form id="formMyProfile">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 py-2">
                            <img class="img-profile rounded-circle" src="{{ $user->imgprofile }}" width="80px">
                            <hr />
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="userName">{{ __('dashboard/texts.text_name') }}</label>
                            <input type="text" class="form-control" id="userName" name="userName" placeholder="" autocomplete="off" value="{{ $user->name }}" autofocus>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="userLastname">{{ __('dashboard/texts.text_lastname') }}</label>
                            <input type="text" class="form-control" id="userLastname" name="userLastname" placeholder="" autocomplete="off" value="{{ $user->lastname }}">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="userEmail">{{ __('dashboard/texts.text_email') }}</label>
                            <input type="text" class="form-control" id="userEmail" name="userEmail" placeholder="" autocomplete="off" value="{{ $user->email }}" disabled>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="userCargo">{{ __('dashboard/texts.text_jobtitle') }}</label>
                            <input type="text" class="form-control" id="userCargo" name="userCargo" placeholder="" value="{{ $user->description }}" disabled>
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="userCellphone">{{ __('dashboard/texts.text_cellphone') }}</label>
                            <input type="text" class="form-control" id="userCellphone" name="userCellphone" placeholder="" autocomplete="off" value="{{ $user->cellphone }}">
                        </div>
                        <div class="col-sm-6 py-2">
                            <label for="userTelephone">{{ __('dashboard/texts.text_telephone') }}</label>
                            <input type="text" class="form-control" id="userTelephone" name="userTelephone" placeholder="" autocomplete="off" value="{{ $user->telephone }}">
                        </div>
                        <div class="col-sm-6 py-2">
                                <label for="userTelephone">{{ __('dashboard/texts.text_language') }}</label>
                                <select id="selectLanguage" name="selectLanguage" class="form-control">
                                    @foreach($langs as $lang)
                                        @if($lang->code == $user->default_language)
                                            <option value="{{ $lang->code }}" selected="selected">{{ $lang->description }}</option>
                                        @else
                                            <option value="{{ $lang->code }}">{{ $lang->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        <div class="col-sm-12 py-4">
                            <button type="button" class="btn btn-primary btn-user btn-block" onclick="updateMyProfile();">
                                {{ __('dashboard/texts.btn_saveprofile') }}
                            </button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script language="javascript">
        let modal_complete = "{{ __('dashboard/texts.modal_complete') }}";
        let modal_saved = "{{ __('dashboard/texts.modal_saved') }}";
    </script>
    <script src="/js/dashboard/myprofile.js"></script>
@endsection