<?php

class ConfigReader {

    private $main_dbhost;
    private $main_dbport;
    private $main_dbuser;
    private $main_dbpass;
    private $main_dbname;

    function __construct() {
	/*
        $this->main_dbhost = "localhost";
        $this->main_dbport = "3306";
        $this->main_dbuser = "root";
        $this->main_dbpass = "adminadmin";
        // $this->main_dbname = "tp_satstore_20180515";
        $this->main_dbname = "sk_intelkomm_20180528";
	*/

        $this->main_dbhost = "localhost";
        $this->main_dbport = "3306";
        $this->main_dbuser = "root";
        $this->main_dbpass = "";
        $this->main_dbname = "satstore_20190429";


    }//function

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}

