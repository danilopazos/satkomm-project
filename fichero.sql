--tipo de cuenta
create table type_account(
	id char(3) primary key not null,
    description varchar(25) not null
);

insert into type_account(id, description) values ('T01', 'Proveedor');
insert into type_account(id, description) values ('T02', 'Canal de venta');
insert into type_account(id, description) values ('T03', 'Cliente final');

--empresas que tiene la cuenta
create table type_relation_accounts(
	parent_id char(5) not null,
    children_id char(5) not null,
    type_account_id char(3) not null,
    status tinyint,
    primary key (parent_id, children_id, type_account_id)
);

insert into type_relation_accounts (parent_id, children_id, type_account_id, status) values('10003', '10001', 'T01', true);
insert into type_relation_accounts (parent_id, children_id, type_account_id, status) values('10001', '10003', 'T02', true);
insert into type_relation_accounts (parent_id, children_id, type_account_id, status) values('10001', '10003', 'T03', true);

--Categorias de los productos
create table product_categories(
	id decimal(8,0) primary key not null,
    description varchar(25) not null,
    account_id char(5) not null,
    created_at datetime
);

--unidad de medida
create table units(
	id char(3) primary key not null,
    description varchar(15) not null
);

insert into units(id, description) value('U01', 'Unidade(s)');
insert into units(id, description) value('U02', 'Kit(s)');
insert into units(id, description) value('U03', 'Pieza(s)');

--sub detalles del producto
create table sub_details(
	id char(3) primary key not null,
    description varchar(15) not null
);

insert into sub_details(id, description) values('S01', 'Modelo');
insert into sub_details(id, description) values('S02', 'Color');
insert into sub_details(id, description) values('S03', 'Capacidad');

--tipo de movimiento del inventario
create table inventory_movements(
	id char(3) primary key not null,
    description varchar(20) not null
);

insert into inventory_movements(id, description) values('ENT', 'Ingreso');
insert into inventory_movements(id, description) values('EXI', 'Salida');
insert into inventory_movements(id, description) values('MVM', 'Movimiento interno');

--registro de productos
create table parent_products(
	id decimal(8,0) primary key not null,
    product_category_id decimal(8,0) not null,
    description varchar(60) not null,
    anotations longtext,
    unit_id char(3) not null,
    provider_id char(5) not null,
    created_at datetime,
    updated_at datetime
);

--detalle del producto
create table children_products(
	id decimal(8,0) primary key not null,
    parent_product_id decimal(8,0) not null,
    serial_number longtext,
    anotations longtext,
    created_at datetime,
    updated_at datetime
);

--relacion del subdetalle con el padre
create table sub_details_parent(
	id decimal(8,0) primary key not null,
    parent_product_id decimal(8,0) not null,
    sub_detail_id char(3) not null,
    description varchar(240) not null,
    status tinyint
);

--relacion del subdetalle con los productos hijo
create table sub_details_children(
	children_product_id decimal(8,0) not null,
    sub_detail_parent_id decimal(8,0) not null,
    status tinyint
);

--razones para mover el inventario
create table movement_reasons(
	id char(4) primary key not null,
    description varchar(30) not null,
    chk_entry tinyint not null,
    chk_exit tinyint not null,
    chk_movement tinyint not null
);

--tipo de documento
create table type_document(
	id char(3) primary key not null,
    description varchar(25)not null
);

insert into type_document(id, description) values('FAC', 'Factura de venta');
insert into type_document(id, description) values('BOL', 'Boleta de venta');
insert into type_document(id, description) values('GUI', 'Guia de remision');

--tipos de inventarios
create table type_inventories(
	id char(4) primary key not null,
    description varchar(45) not null,
    belongs_to_me tinyint not null,
    available_to_sell tinyint not null
);

insert into type_inventories (id, description, belongs_to_me, available_to_sell) values ('I001', 'Productos nuevos', true, true);
insert into type_inventories (id, description, belongs_to_me, available_to_sell) values ('I002', 'Productos usados', true, true);
insert into type_inventories (id, description, belongs_to_me, available_to_sell) values ('I003', 'Productos alquilados', true, false);
insert into type_inventories (id, description, belongs_to_me, available_to_sell) values ('I004', 'Productos en comodato', true, false);
insert into type_inventories (id, description, belongs_to_me, available_to_sell) values ('I005', 'Productos prestados', true, false);
insert into type_inventories (id, description, belongs_to_me, available_to_sell) values ('I006', 'Productos vendidos', false, false);
insert into type_inventories (id, description, belongs_to_me, available_to_sell) values ('I007', 'Productos malogrados', true, false);

--documentos registrados
create table documents(
	id decimal(8,0) primary key not null,
    type_document_id char(4) not null,
    document_number varchar(25) not null,
    emits_document char(5) not null,
    receives_document char(5) not null,
    anotations longtext,
    user_create varchar(250) not null,
    created_at datetime,
    updated_at datetime
);

--tipos de direccion
create table type_address(
	id char(3) primary key not null,
    description varchar(45) not null
);

insert into type_address (id, description) values ('D01', 'Facturacion');
insert into type_address (id, description) values ('D02', 'Envio');
insert into type_address (id, description) values ('D03', 'Almacen propio');
insert into type_address (id, description) values ('D04', 'Almacen de terceros');

--relaciones de direcciones y tipo de direccion
create table accounts_address(
	address_id int(11) not null,
    type_address_id char(3) not null,
    status tinyint not null,
    primary key(address_id, type_address_id)
);

insert into accounts_address (address_id, type_address_id, status) values (23 , 'D01', true);
insert into accounts_address (address_id, type_address_id, status) values (23 , 'D02', true);
insert into accounts_address (address_id, type_address_id, status) values (23 , 'D03', true);

--pais
create table countries(
	id char(4) primary key not null,
    description varchar(25) not null,
    coi_code char(3) not null,
    language_default char(3) not null,
    currency_default char(3) not null,
    status tinyint not null
);

insert into countries (id, description, coi_code, language_default, currency_default, status) values ('C001', 'Peru', 'PER', 'ESP', 'PEN', true);

--departamento
create table states(
	id char(4) primary key not null,
    description varchar(40) not null,
    country_id char(4) not null
);

insert into states (id, description, country_id) values ('S001', 'Lima', 'C001');
insert into states (id, description, country_id) values ('S002', 'Cusco', 'C001');
insert into states (id, description, country_id) values ('S003', 'Arequipa', 'C001');
insert into states (id, description, country_id) values ('S004', 'Ica', 'C001');

--provincia
create table provinces(
	id char(4) primary key not null,
    description varchar(45) not null,
    state_id char(4) not null
);

insert into provinces (id, description, state_id) values ('P001', 'Lima', 'S001');
insert into provinces (id, description, state_id) values ('P002', 'Callao', 'S001');

--distrito
create table cities(
	id char(4) primary key not null,
    description varchar(45) not null,
    province_id char(4) not null
);

insert into cities (id, description, province_id) values ('C001', 'San Juan de Lurigancho', 'P001');
insert into cities (id, description, province_id) values ('C002', 'Lurin', 'P001');
insert into cities (id, description, province_id) values ('C003', 'Miraflores', 'P001');

--ait_address : update
ALTER TABLE `ait_address` 
    ADD `account_id` CHAR(5) NOT NULL AFTER `id`, 
    ADD `nickname` VARCHAR(45) NOT NULL AFTER `account_id`, 
    ADD `city_id` CHAR(4) NOT NULL AFTER `nickname`;

update ait_address
	set account_id='10003', nickname='Oficina Miraflores', city_id='C003'
    where id=23;

--registro de todos los movimientos realizados al inventario
create table inventory_records(
	id decimal(8,0) primary key not null,
    product_id decimal(8,0) not null,
    user_create varchar(250) not null,
    last_movement tinyint not null,
    inventory_movement_id char(3) not null,
    movement_reason_id char(4) not null,
    type_inventorie_id char(4) not null,
    document_id decimal(8,0),
    address_from int(11),
    address_to int(11),
    anotations longtext,
    datetime_entry datetime,
    datetime_exit datetime,
    datetime_due datetime,
    created_at datetime
);