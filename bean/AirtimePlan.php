<?php

class AirtimePlan{
    
    public $id;
    public $idcrypt;
    public $sku;
    public $accountid;
    public $customeraccountid;
    public $parentplan;
    public $networkcode;
    public $paymentperiodcode;
    public $contractperiodcode;
    public $servicetypecode;
    
    public $customername;
    
    public $isgroup;
    public $grouptype;
    
    public $subscriptionprice;
    public $fixedpayment;
    public $activationpayment;
    public $suspensionpayment;
    public $deactivationpayment;
    public $extensionpayment;
    
    
    public $langcode;
    public $name; 
    public $intro;
    public $description;
    public $createdby;
    public $modifiedby;
    public $deletedby;
    public $datecreated;
    public $datemodified;
    public $datedeleted;
    
    public $version;
    public $aviable;
    public $ttgroup;
        
    public $totalsimcards;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
