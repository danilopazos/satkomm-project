<?php
class AirtimePlanSetupChange{

    public $id;
    public $planidcrypt;
    public $accountidchanger;
    public $useridchanger;
    public $trafficIncrement;
    public $trafficSetup;
    public $trafficAllowance;
    public $trafficAllowanceDetail;
    public $datetimechange;
    public $tonotify; 
    
    public $wasnotified;
    public $notificationdate;
    public $notificationemails;
    public $accountid;
 
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
    
}