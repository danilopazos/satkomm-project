<?php

class TrafficSetup {
    
    public $id;
    public $planid;
    public $planidcrypt;
    public $ttcode_source; //traffictype
    public $description;
    public $ttcode_source_description;
    public $ttcode_dest;
    public $ttcode_dest_description;
    public $tunitcode;
    public $chargecode;
    public $chargeammount;
    public $dateregistered;
    public $createdby;
            
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
