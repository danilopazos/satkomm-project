<?php

class InvoicetcDispatchService {

    public $id;
    public $invoicetcid;
    public $simcarditemid;
    public $simcardserial;
    public $planforaccount;
    public $planforprovider;
    public $activationdate;
    public $activationtime;
    public $description;
    public $deviceitemid;
    public $deviceserial;
    public $userid;
    public $operationdatetime;
    
    public $sku;
    public $productname;
     
    public $devicesku;
    public $deviceproductname;
    
    public $planname;
    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
