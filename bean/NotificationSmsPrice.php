<?php

class NotificationSmsPrice {

    public $iso;
    public $country;
    public $description;
    public $price_sms;   
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
