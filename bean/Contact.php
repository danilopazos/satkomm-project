<?php

class Contact {

    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $accountid;
    public $leadsource;
    public $mobilecc;
    public $mobile;
    public $phonecc;
    public $phone;
    public $phoneext;
    public $fax;
    public $title;
    public $salutation;
    public $department;
    public $dateofbirth;
    public $others;
    public $description;
    public $contactpriority;
    public $ownerid;
    public $createdbyuser;
    public $modifiedbyuser;
    public $deletedbyuser;
    public $createddatetime;
    public $modifieddatetime;
    public $deleteddatetime;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
