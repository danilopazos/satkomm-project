<?php
class Quote {

    public $id; 
    public $accountid;
    public $idcrypt;
    
    //basic info
    public $quoteno;
    public $subject;
    public $status;
    public $quotedate;
    public $quoteuntil;
    public $assignedto;
      
    //Customer data
    public $customerid;
    public $contactid; //userid
    public $contacttelephone;
    public $contactmobile;
    public $contactemail;
    public $contactsalutation;
    
    //Payment ways
    public $paymentterms;
    public $paymenttermsother;
    
    //Payment ways
    public $carriercompany;
    public $carriercompanyother;
    
    public $deliveryterms;
    public $deliverytermsother;
    
    //Shipping ways
    public $operationtype; //sale, rent
    public $shippingenabled;
    
    
    public $createdby;
    public $modifiedby;
    public $deletedby;
    
    //Total operation
    public $discount;
    public $hastaxes;
    public $subtotal;
    public $shippingcost;
    public $taxespercent;
    public $total;
    
    //Obs
    public $obs; 
    public $createddate;
    public $modifieddate;
    public $deleteddate;
    
    //Address
    public $bill_country;
    public $bill_state;
    public $bill_city;
    public $bill_street;
    public $bill_zipcode;
    public $bill_pobox;
    
    
    public $ship_country;
    public $ship_state;
    public $ship_city;
    public $ship_street;
    public $ship_zipcode;
    public $ship_pobox;
    
    public $aProduct;
    public $aService;
   
    public $contact_names;
    public $assigned_names;
    public $customer_name;
    
    public $currency_code = "USD";
    public $currency_exchange = 1;
    public $totalusd ;    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}