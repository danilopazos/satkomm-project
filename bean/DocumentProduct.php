<?php

class DocumentProduct {

    public $id;
    public $docidcrypt;
    public $idcrypt;
    public $doctype;
    public $name;
    public $description;
    public $price;
    public $qty;
    public $months;
    public $subtotal;
    public $operationtype;
    public $itemtype;
    public $itemindex;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
