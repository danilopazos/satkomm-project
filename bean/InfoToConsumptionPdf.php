<?php

class InfoToConsumptionPdf{

    public $providerid;
    public $logoimg;
    public $countrycode;
    public $headerinvoice;
    public $formatinvoice;
    public $taxammount;
    public $currencyDescription;
    public $currencyIso;
    public $currencySymbol;
    public $currencyExchange;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}