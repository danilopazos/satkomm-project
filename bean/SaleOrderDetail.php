<?php

class SaleOrderDetail {

    public $id;
    public $code;
    public $cartid;
    public $sku;
    public $qty;
    public $productid;
    public $planid;
    public $plansku;
    public $productprice;
    public $planprice;
    public $rentalinsurance;
    public $timeinweeks;
    public $pweek;
    public $productname;
    public $planname;
    public $operation;
    public $typeofproduct;
    public $subtotal;
    public $itemid;
    public $mediaid;
    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
