<?php

class Simcard {
    public $inventoryitemid;
    public $iserial;
    public $sku;
    public $voicenumber;
    public $datanumber;
    public $productname;
    public $operationregistered;
    public $datetoexecute;
    public $customername;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
