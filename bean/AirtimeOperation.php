<?php

class AirtimeOperation {

    public $id; 
    public $inventoryitemid;
    public $operation;
    public $operationstatus;
    public $planid;
    public $accountid;
    public $accountassignedid;
    
    public $account;
    public $accountassigned;
    
    public $iserial;
    public $productname;
    public $productid;
    public $planname;
    
    public $networkcode;
    public $servicetype_code;
    
    public $subscription_period;
    public $contract_period;
    public $subscription_price;
    
    public $activation_price;
    public $suspension_price;
    public $deactivation_price;
    
    public $fixed_price;
    public $datetoexecute;
    public $timetoexecute;
    public $subtotal;
    public $taxes;
    public $paymentmethod;
    public $paymentmethod_price;
    public $totalpayment;
    
    
    public $requestedbyuserid;
    public $requesteddatetime;
    public $requeststatus;
    public $processeddatetime;
    public $processedbyuserid;
    
    public $requestedbyuser;
    public $processedbyuser;
    
    public $voicenumber;
    public $datanumber;
    public $faxnumber;
    public $imsi;
    public $expirydate;
    public $puk1;
    public $puk2;
    public $pin;
    
    public $planidcryptforcustomer;
    public $plannameforcustomer;
    
    public $servicestatus;
    
    public $actual_voicenumber;
    public $actual_faxnumber;
    public $actual_datanumber;
    public $actual_imsi;
    public $actual_expirydate;
    public $actual_puk1;
    public $actual_puk2;
    public $actual_pin;
    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
