<?php
class CdrSimcardPeriod{
    
    public $id;
    public $serial;
    public $voicenumber;
    public $datanumber;
    public $code2;
    public $year;
    public $month;
    public $day;
    public $customeraccountid;
    public $provideraccountid;
    public $subscriptionprice;
    public $fixedchargesprice;
    public $additionalchargesprice;
    public $activationprice;
    public $suspensionprice;
    public $deactivationprice;
    public $invoiceid;
    public $planid;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
    
}