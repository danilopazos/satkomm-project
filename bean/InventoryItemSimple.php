<?php

class InventoryItemSimple {

    public $id;
    public $iserial; 
    public $name;
    public $productidcrypt;
    public $servicestatus;
    public $expirydate;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
