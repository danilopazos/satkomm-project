<?php
class QuoteStatus {

    public $id;
    public $quoteidcrypt; 
    public $accountid;
    public $status;
    public $chagedatetime;
    public $userid;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}