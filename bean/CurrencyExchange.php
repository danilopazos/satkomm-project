<?php

class CurrencyExchange {

    public $id;
    public $accountid;
    public $currency_code;
    public $xrate;
    public $registereduser;
    public $registereddatetime;
    public $username;
    public $isactive;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}