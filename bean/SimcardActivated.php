<?php

class SimcardActivated {
    /*
      $query  ="select ao.id , ao.planid, ao.planidcryptforcustomer, ";
      $query .=" ao.inventoryitemid, ao.operation , ao.iserial, ao.voicenumber, ao.datanumber, ";
      $query .="ao.productname, ao.servicetype_code,  ";
      $query .="ao.planname as providerplanname, ao.plannameforcustomer as customerplanname , acc.name as customername ";
      $query .=", ao.processeddatetime ";
     *      */

    
    public $id;
    public $productid;
    public $planid;
    public $planidcryptforcustomer;
    public $inventoryitemid;
    public $operation;
    public $iserial;
    public $voicenumber;
    public $datanumber;
    public $productname;
    public $servicetype_code;
    public $providerplanname;
    public $customerplanname;
    public $customername;
    public $processeddatetime;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
