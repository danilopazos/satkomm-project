<?php
class TrafficAllowanceDetail {

    public $trafficsetupid;
    public $trafficallowanceid;
    public $planidcrypt; 
    public $ttcode_source;
    public $tunitcode;
    public $description;
            
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
