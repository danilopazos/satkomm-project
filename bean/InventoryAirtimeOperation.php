<?php

class InventoryAirtimeOperation {

    public $id;
    public $airtimeordercode;
    public $operation;
    public $datetoexecute;
    public $timetoexecute;
    public $processeddatetime;
    
    public $pp_idcrypt;
    public $pp_sku;
    public $pp_networkcode;
    public $pp_payment_period_code;
    public $pp_contract_period_code;
    public $pp_servicetype_code;
    public $pp_subscription_payment;
    public $pp_fixed_payment;
    public $pp_activation_payment;
    public $pp_suspension_payment;
    public $pp_deactivation_payment;
    public $pp_planname;
    
    public $pc_idcrypt;
    public $pc_sku;
    public $pc_networkcode;
    public $pc_payment_period_code;
    public $pc_contract_period_code;
    public $pc_servicetype_code;
    public $pc_subscription_payment;
    public $pc_fixed_payment;
    public $pc_activation_payment;
    public $pc_suspension_payment;
    public $pc_deactivation_payment;
    public $pc_planname;
     

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
