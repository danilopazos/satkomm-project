<?php
class AllowanceAlert {

    public $id;
    public $customeraccountid;
    public $accountname;
    public $registeruserid;
    public $registerdatetime;
    public $deleted;
    public $deleteduserid;
    public $deleteddatetime;
    public $inventoryitemid;
    public $iserial;
    public $source;
    
    public $trafficallowance_name;
    public $plan_name;
    /*
      public $traffictypecode;
      public $tunitcode;
      public $tunitalertqty;


      public $taidcrypt;
      public $taqty;

      public $additionalcharges;
     */
    public $planidcrypt;
    public $taidcrypt;
    public $taexceed01;
    public $taexceed02;
    public $taexceed03;
    public $acexceed01;
    public $acexceed02;
    public $acexceed03;
    public $emails;
    public $subject;
    public $mobilenumbers;

}