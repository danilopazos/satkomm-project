<?php

class CreditCard {

    public $id;
    public $accountid;
    public $number;
    public $verificationcode;
    public $expmonth;
    public $expyear;
    public $cardholdername;
    public $addressid;
    public $address;
    public $type; //if is debit or credit
    public $registerdatetime;
    public $registeruser;
    public $registerip;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
