<?php

class CdrMovementInvoice {

    public $date;
    public $time;
    public $subscriber; //abonado
    public $numberDestination;
    public $calltype;
    public $plan;
    public $qty_duration;
    public $duration;
    public $additionalCost;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}