<?php

class CdrRecord {

    public $id;
    
    public $numberfrom;
    public $destinationnumber;
    public $ipaddres;
    public $countrycode;
    
    public $sourcetable;
    public $sourceid;
    public $cdrdate;
    public $cdrtime;
    public $numwrdunitcode;
    public $bitnumunitcode;
    public $traffictypesourcecode;
    public $traffictypedestcode;
    public $simcardperiod;
    public $provideraccountid;
    public $customeraccountid;
    public $planidcrypt;
    public $traffictypecode;
    public $traffictypedesc;
    public $numwrd;
    public $bitnum;
    public $numwrdup;
    public $numwrddown;
    public $bitnumup;
    public $bitnumdown;
    public $numwrdinbundle;
    public $numwrdoutofbundle;
    public $bitnuminbundle;
    public $bitnumoutofbundle;
    public $chargeinbundle;
    public $chargeoutofbundle;
    public $qty;
    
    public $qtyunitcode;
    public $chargecode;
    public $servicetype;
    
    
    public $serial;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
