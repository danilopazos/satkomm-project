<?php

class ItemAutocomplete {

    public $id;
    public $sku;
    public $name;
    public $accountid;
    public $customeraccountid;
    public $description;
    public $price;
    public $activationprice;
    public $issimcard;
     
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
