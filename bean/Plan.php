<?php

class Plan {

    public $id;
    public $idcrypt;
    public $sku;
    public $networkcode;
    public $payment_period_code;
    public $contract_period_code;
    public $servicetype_code;
    
    public $isgroup;
    public $grouptype;
    
    public $price;
    public $fixedpayment;
    public $activationpayment;
    public $suspensionpayment;
    public $deactivationpayment;
    public $currencycode;
    public $status;
    public $lang_code;
    public $name;
    public $intro;
    public $description;
    
    public $aPlanSons;
    public $aTrafficType;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
