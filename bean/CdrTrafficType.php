<?php

class CdrTrafficType {

    public $id;
    
    public $code;
    public $provider;
    public $network;
    
    public $servicecode;
    public $servicecode_desc;
    
    public $calltypecode;
    public $calltypecode_desc;
    
    public $cdkind;
    public $cdkind_desc;
    
    public $oceanregioncode;
    public $oceanregioncode_desc;
    
    public $destinationzone;
    public $destinationzone_desc;
    
    public $description;
    
    public $dateregistered;
     
    public $ttgroup;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
