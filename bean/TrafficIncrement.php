<?php

class TrafficIncrement{
    
    public $id;
    public $planid;
    public $planidcrypt;
    public $traffictypecode;
    public $min_unitcode;
    public $min_unitqty;
    public $inc_unitcode;
    public $inc_unitqty;
    public $dateregistered;
    public $createdby;
    public $description;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
