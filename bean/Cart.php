<?php

class Cart {

    public $id;
    public $itemid;
    public $accountid;
    public $qty;
    public $productid;
    public $datecreated;
    public $sessionid;
    public $userip; 
    public $operation;
    public $buyprice;
    public $subtotal;
    public $mediaid;
    public $name;
    public $sku;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
