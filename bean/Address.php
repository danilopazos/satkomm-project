<?php

class Address {

    public $id;
    public $country; 
    public $state;
    public $city;
    public $street;
    public $zipcode;
    public $pobox;
    public $type;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
