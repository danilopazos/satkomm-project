<?php

class Invoice {

    public $id;
    public $idcrypt;
    public $invoicetype;
    public $provideraccountid;
    public $accountid;
    public $accountname;
    public $code;
    public $ordercode;
    public $invoicedate;
    public $invoiceduedate;
    public $invoicepaymentrand;
    public $userid;
    public $userip;
    public $sessionid;
    public $username;
    public $profileid;
    public $firstname;
    public $lastname;
    public $phonenumber;
    public $payment_method;
    public $billing_country;
    public $billing_zipcode;
    public $billing_address;
    public $billing_state;
    public $billing_select_state;
    public $billing_city;
    public $billing_pobox;
    public $shipping_country;
    public $shipping_zipcode;
    public $shipping_address;
    public $shipping_state;
    public $shipping_select_state;
    public $shipping_provider;
    public $shipping_trackingid;
    public $shipping_city;
    public $shipping_pobox;
    public $shipping_method;
    public $totalitems;
    public $subtotal;
    public $taxes;
    public $shipping_cost;
    public $x_ammount;
    public $invoicecomments;
    public $paymenttype;
    public $payment_fee;
    public $status;
    public $paymentobs;
    
    public $dispatchuserid;
    public $dispatchdate;
    public $dispatchdatetime;
    
    public $paymentuserid;
    public $paymentdate;
    public $paymentdatetime;
    
    public $deliveryuserid;
    public $deliverydate;
    public $deliverydatetime;
    
    public $shippingtrackingid;
    public $shippingprovidercode;
    public $shippingprovider_name;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
