<?php

class Product {

    public $idcrypt;
    public $sku;
    public $name;
    public $network_code;
    public $height;
    public $width;
    public $weight;
    public $lang_code;
    public $distprice;
    public $mediaid; 
    public $gawosc;
    public $buy;
    public $buymargin;
    public $rentpermonth;
    public $buyisenabled;
    public $rentpermonthisenabled;
    public $issimcard;
    public $totalcustom;
    public $accountid;
    public $customeraccountid;
    public $accountname;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
