<?php
class InvoicePayment{
    
    public $id;
    public $idcrypt;
    public $type;
    public $invoiceid;
    public $tableinvoice;
    public $provideraccountid;
    public $customeraccountid;
    public $tableinvoiceid;
    public $tableinvoiceidcrypt;
    public $invoiceammount;
    public $invoiceno;
    public $invoicedate;
    public $invoiceduedate;
    public $invoiceref;
    public $paymentfilename;
    public $paymentammount;
    public $paymentref;
    public $paymentdate;
    public $paymentfile;
    public $deleted;
    public $registereduser;
    public $registereddatetime;
    public $customername;
    public $creditlimit;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
}