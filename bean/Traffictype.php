<?php

class Traffictype {
    
    public $id;
    public $networkcode;
    public $name;
    public $description;
    public $code;
    public $airtimeprovider;
    public $ttgroup;
    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
