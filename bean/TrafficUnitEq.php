<?php

class TrafficUnitEq {

    public $codea;
    public $qtya;
    public $codeb;
    public $codebid;
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
