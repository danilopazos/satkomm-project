<?php

class DeviceOnSale {

    public $inventoryitemid;
    public $iserial;
    public $productid;
    public $productname;
    public $sku;
    public $accountid;
    public $customerid;
    public $customername;
    public $providerinvoice;
    public $customerinvoice;
    public $customerinvoiceid;
    public $invoicedate;
    public $invoiceduedate;
    public $operationdatetime;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}