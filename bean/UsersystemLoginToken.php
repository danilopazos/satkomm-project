<?php

class UsersystemLoginToken {

    public $id;
    public $userid;
    public $iptovalidate;
    public $daterequested;
    public $validationcode;
    public $validationtype;
    public $validationdest;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
