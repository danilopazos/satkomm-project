<?php
class TrafficAllowance {

    public $id;
    public $planid;
    public $idcrypt;
    public $seedcode;
    public $tname;
    public $qty;
    public $unitcode;
    public $aTrafficSetup; // es decir el allowance incluye un array de un traffic setup
    public $aTrafficSetupId;
    public $aTrafficAllowanceDetail;
    public $createdby;
    public $datecreated;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}