<?php

class AirtimeOrder {

    public $id;
    public $accountproviderid;
    public $accountcustomerid;
    public $code;
    public $orderdate;
    
    public $accountcustomer;
    public $accountprovider;
    
    public $userid;
    public $userip;
    public $sessionid;
    public $requesteduser;
    
    public $totalitems;
    public $subtotal;
    public $taxes;
    public $payment_method;
    public $convenience_fee;
    public $totalpayment;
    public $ordercomments;
    public $orderstatus;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
