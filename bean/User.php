<?php

class User {

    public $id;
    public $accountid;
    public $parentaccountid;
    public $lang;
    public $profileid;
    public $status;
    public $username;
    public $pass;
    public $firstname;
    public $lastname;
    public $phone;
    public $phoneext;
    public $mobile;
    public $account_shippingcountry;
    public $account_name;
    
    public $position;
    public $country;
    public $state;
    public $city;
    public $street;
    public $zipcode;
    public $pobox;
    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
