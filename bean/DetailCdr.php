<?php

class DetailCdr{
    
    public $numberfrom;
    public $ipaddress;
    public $destinationnumber;
    public $countrycode;
    public $cdrdate;
    public $cdrtime;
    public $unitcode;
    public $traffictypedesc;
    public $qty;
    public $chargeinbunlde;
    public $chargeoutofbunlde;
    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
}

?>