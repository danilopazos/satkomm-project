<?php

class InventoryItem {

    public $id;
    public $accountid;
    public $documentcode;
    public $itemindex;
    public $itemid;
    public $iserial;
    public $productid;
    public $icode2;
    public $sku;
    public $issimcard;
    public $gawosc;
    public $productname;
    public $productprice;
    public $datereceived;
    public $mediaid;
    public $voicenumber;
    public $datanumber;
    public $faxnumber; 
    public $imsi;
    public $puk1;
    public $puk2;
    public $pin;
    public $planid;
    public $planname;
    public $operationregistered;
    public $operationrequested;
    
    //Only for activation
    public $operationdate;
    public $operationtime;
    public $servicetype_code;
  
    public $inventorystatus;
    public $statusdate;
    public $statusdescription;
    public $customerinvoiceid;
    public $atsimcardid;
    public $atstatus;
    public $actiondatetime;
    public $actionuserid;
    
    public $invoicedate;
    public $dispatchdate; 
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
