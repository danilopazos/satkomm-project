<?php

class CountryTaxType {

    public $code;
    public $name;
    public $taxtype;
    public $charlimit;
    
    /*
   public function _construct($_code, $_name, $_taxtype, $_charlimit) {
       $this->code = $_code;
       $this->name = $_name;
       $this->taxtype = $_taxtype;
       $this->charlimit = $_charlimit;
   } 
     */
    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
