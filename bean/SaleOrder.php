<?php

class SaleOrder {

    public $id;
    public $idcrypt;
    
    public $accountid;
    public $accountname;
    public $code;
    
    public $invoice_code;
    public $invoice_dispatchdate;
    public $invoice_deliverydate;
    
    public $orderdate;
    public $orderpaymentrand;
    public $userid;
    public $userip;
    public $sessionid;
    public $username;
    public $profileid;
    public $firstname;
    public $lastname;
    public $phonenumber;
    public $payment_method;
    public $billing_country;
    public $billing_zipcode;
    public $billing_address;
    public $billing_state;
    public $billing_select_state;
    public $billing_city;
    public $billing_pobox;
    public $shipping_country;
    public $shipping_zipcode;
    public $shipping_address;
    public $shipping_state;
    public $shipping_select_state;
    public $shipping_city;
    public $shipping_pobox;
    public $shipping_method;
    public $totalitems;
    public $subtotal;
    public $taxes;
    public $shipping_cost;
    public $x_ammount;
    public $ordercomments;
    
    public $shippingprovidercode;
    public $shippingtrackingid;
    public $shippingprovider_name;
    
    public $orderstatus;
    public $canceldatetime;
    
    public $dispatchnow;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
