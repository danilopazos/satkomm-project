<?php
class DocumentService {
   
    public $id;
    public $docidcrypt;
    public $parentplan;
    public $idcrypt;
    public $doctype;
    public $name;
    public $description;
    public $activationprice;
    public $price;
    public $qty;
    public $subtotal;
    public $operationtype;
    public $itemtype;
    public $itemindex;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}