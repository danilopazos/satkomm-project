<?php

class HeadDocumentExport{
    
    public $paymentMethod;
    public $accountName;
    public $numberDocument;
    public $currencySymbol;
    public $contactFirstName;
    public $contactLastName;
    public $contactEmail;
    public $contactPhone;
    public $billStreet;
    public $billCity;
    public $billState;
    public $billCode; 
    public $billCountry;
    
    public $shippingAddress;
    public $shippingCountry;
    public $shippingState;
    public $shippingCity;
    public $shippingZipCode;
    
    public $phoneContact;
    public $webSiteAccount;
    public $dateDocument;
    public $dateDueDocument;
    public $datePeriod;
    
    public $creadtedFirstName;
    public $creadtedLastName;
    
    public $salesManFisrtName;
    public $salesManLastName;
    
    public $comments;
    
    
    public $taxes;
    public $paymentFee;
    public $parcialtotal;
    public $shippingCost;
    public $total;
    
    public $colorBanner = "#F71907";
    public $textBanner;
    public $fontBanner = 100 ;
    
    public $shippingtrackingid;
    public $shippingprovidercode; 
    public $shippingprovider_name;     
    public $ordercode;

    public $movementsInvoice;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
    
}

?>