<?php

class ProductDetail {

    public $idcrypt;
    public $sku;
    public $name;
    public $network_code;
    public $height;
    public $width;
    public $weight;
    public $lang_code;
    public $distprice;
    
    public $intro;
    public $description;
    public $package;
    public $technicalspecs;
    public $buymargin;
    public $features;
    public $docs;
    public $tags;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
