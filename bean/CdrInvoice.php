<?php
class CdrInvoice{
    public $id;
    public $provideraccountid;
    public $customeraccountid;
    public $invoicedate;
    public $duedate;
    public $year;
    public $month;
    public $customername;
    public $total;
    public $paymentstatus;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
    
}