<?php

class Account {

    public $id;
    public $parentaccountid;
    public $code;
    public $countrycode;
    public $defaultcurrency;
    public $descriptioncurrency;
    public $name;
    public $email;
    public $website;
    public $phone;
    public $phoneext; 
    public $billaddressid;
    public $shipaddressid;

    public $headerinvoice;
    public $formatinvoice;
    public $taxammount;
    
    public $taxid;
    public $taxtypeid;
    public $stylecolor;
    public $billaddress;
    public $shipaddress;
    public $subdomain;
    public $status;
    
    public $ownerid;
    public $createdbyuser;
    public $modifiedbyuser;
    public $deletedbyuser;
    public $createdatetime;
    public $deleteddatetime;
    public $modifieddatetime;
     
    public $creditlimit;
    public $invoiceduedays;
    public $currencyrelation;
    public $confquotevaliddays;
    public $confinvoicedue;
    public $conftaxespercent; 
    
    public $invoiceprefix;
    public $quoteprefix;
    public $invairtimeprefix;
    
    public $customplansactive;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
