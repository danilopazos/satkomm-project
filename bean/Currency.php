<?php

class Currency {

    public $id;
    public $code;
    public $description;
    public $symbol;
    public $country;
    public $xrate;
    public $username;
    public $registereddatetime;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
