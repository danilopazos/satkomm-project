<?php
class Media {
    public $id;
    public $idcrypt;
    public $ismain;
    public $url;
    public $type;
    public $media_id;
    public $lang_code;
    public $name;
    public $title;
    public $alt;
    public $fileext;
    public $filename;
    
    /*
    public $thumb_blob;
    public $common_blob;
    */
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        //	return $this;
    }
}