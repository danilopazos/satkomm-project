<?php
class InventoryItemHistory {

    public $id;
    public $inventoryitemid;
    public $inventorystatus;
    public $statusdate;
    public $statusdescription;
    public $customerinvoiceid;
    public $atsimcardid;
    public $atstatus;
    public $actiondatetime;
    public $actionuserid;
    public $actionuser_firstname;
    public $actionuser_lastname;
    public $customerinvoiceno;
    public $customerinvoiceidcrypt;
    public $simcardserial;
    public $simcardvoicenumber;
    public $simcardoperationrequested;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
}