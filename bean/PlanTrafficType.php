<?php

class PlanTrafficType {

    public $id;
    public $planid;
    public $traffictype_code;
    public $qtyxperiod;
    public $additionalprice;
    public $roundedcode;
    public $roundedqty;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}
