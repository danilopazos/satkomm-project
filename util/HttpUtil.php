<?php

function isAuth() {
    
    if (    trim($_SESSION ["session_userid"]) == ""  ||  
            $_SESSION ["session_userid"] == null  ||  
            !(isset($_SESSION ["session_userid"])) ||  
            
            !(intval($_SESSION ["session_userid"]) > 0)
            
            
            ) {
        redirect("auth", "index");
    }
}

function moneyFormat($_money){
   return number_format( doubleval($_money), 2);
   // return   doubleval($_money );
}

function strtourlencode($text) {
    return urlencode(strtolower($text));
}

function redirect($module, $action, $aParams = false) {

    $paramsLink = "";


    if ($aParams) {
        if (count($aParams) > 0) {

            foreach ($aParams as $key => $value) {
                $paramsLink .="/" . trim($key) . "/" . trim($value);
            }
        }
    }//if

    header("Location: /$module/$action" . $paramsLink);
}

//function

function redirectOutside($url) {
    header("Location: " . $url);
}

//function

function get_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

//function

function toEncrypt($value) {


    /*
      $iv = mcrypt_create_iv(
      mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
      MCRYPT_DEV_URANDOM
      );

      $encrypted = base64_encode(
      $iv .
      mcrypt_encrypt(
      MCRYPT_RIJNDAEL_128,
      hash('sha256', $key, true),
      $value,
      MCRYPT_MODE_CBC,
      $iv
      )
      );

      return $encrypted;
     */
    $key = pack('H*', "acb04b7e103a0cd8b54763051cef08bc55abe029gdebae5e1d417e2ffb2a00a3");

    $plaintext = $value;

    # crear una aleatoria IV para utilizarla co condificación CBC
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

    # crea un texto cifrado compatible con AES (tamaño de bloque Rijndael = 128)
    # para hacer el texto confidencial
    # solamente disponible para entradas codificadas que nunca finalizan con el
    # el valor  00h (debido al relleno con ceros)
    $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext, MCRYPT_MODE_CBC, $iv);

    # anteponer la IV para que esté disponible para el descifrado
    $ciphertext = $iv . $ciphertext;

    # codificar el texto cifrado resultante para que pueda ser representado por un string
    $ciphertext_base64 = base64_encode($ciphertext);

    return $ciphertext_base64;
}

//function

function toDecrypt($encrypted) {
    /*
      $key = "loremipsumdolorasimet2744964$$";

      $data = base64_decode($encrypted);
      $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

      $decrypted = rtrim(
      mcrypt_decrypt(
      MCRYPT_RIJNDAEL_128,
      hash('sha256', $key, true),
      substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
      MCRYPT_MODE_CBC,
      $iv
      ),
      "\0"
      );

      return $decrypted;
     */

    $key = pack('H*', "acb04b7e103a0cd8b54763051cef08bc55abe029gdebae5e1d417e2ffb2a00a3");

    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

    $ciphertext_dec = base64_decode($encrypted);

    # recupera la IV, iv_size debería crearse usando mcrypt_get_iv_size()
    $iv_dec = substr($ciphertext_dec, 0, $iv_size);

    # recupera el texto cifrado (todo excepto el $iv_size en el frente)
    $ciphertext_dec = substr($ciphertext_dec, $iv_size);

    # podrían eliminarse los caracteres con valor 00h del final del texto puro
    $plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);

    return $plaintext_dec;
}

//function

function isValidEmail($email) {
    $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";

    if (eregi($pattern, $email)) {
        return true;
    } else {
        return false;
    }
}

function getCurrentURL() {
    $currentURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    $currentURL .= $_SERVER["SERVER_NAME"];

    if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
        $currentURL .= ":" . $_SERVER["SERVER_PORT"];
    }

    $currentURL .= $_SERVER["REQUEST_URI"];
    return $currentURL;
}

function getCurrentUrlPath() {
    $currentURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    $currentURL .= $_SERVER["SERVER_NAME"];

    if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
        $currentURL .= ":" . $_SERVER["SERVER_PORT"];
    }

    return $currentURL;
}

function getSubdomainName(){
    $hostname = parse_url( getCurrentURL()  , PHP_URL_HOST ) ;    
    
    $aHost = explode('\.', $hostname);
    
    $total  = count($aHost);
    $subdomain = false;
    if($total == 3){
        $subdomain = trim($aHost[0]);
    }
    return $subdomain;
}
