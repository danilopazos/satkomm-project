<?php

function getDocumentEmail($_subject, $_content, $_lang_download_info, $_documentno, $_downloadurl  ){

     $basetemplate  = file_get_contents("../lib/util/mailservice/template2.html", true);
    
      $tittle = $_subject;
     $content = '';
              
     
               $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper"  style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                     
                                    $content .= '<p>'.$_content.'</p> ';
                                    
                                    $content .= '<p>';
                                    $content .=  '<strong>'.$_lang_download_info.'</strong>: <a download class="btn btn-xs btn-primary" href="'.$_downloadurl.'" target="_blank">'.$_documentno.'</a> ';
                                 
                                    $content .= '</p>';          
                                 
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';
     
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;

      return $basetemplate;
}//function




function getCancelOrderEmail( $_ordercode , $_idcrypt, $_title , $_message1, $_message2 , $_lang_code , $_lang_url   ){

     $basetemplate  = file_get_contents("../lib/util/mailservice/template.html", true);
    
      $tittle = $_title;
     $content = '';
              
     
               $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper"  style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    
                                    $urlorder = '<a href="'.  getCurrentUrlPath().'/saleorder/viewr/id/'.$_idcrypt.'">'.$_ordercode.'</a>';
                                    
                                    $content .= '<p>'.$_message1.'</p> ';
                                    
                                    $content .= '<p>';
                                    $content .=  '<strong>'.$_lang_code.'</strong>: '.$_ordercode.'<br />';
                                    $content .=  '<strong>'.$_lang_url. '</strong>:  '.$urlorder.'<br />';
                                    $content .= '</p>';          
                                    
                                    $content .= '<p>'.$_message2.'</p> ';
                                    //$content .= '<p>Remember: You have 3 hours to cancel the order after its creation. After those period of time, any kind of attempt of cancellation will be denied</p> ';
                                    
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';
     
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;

      return $basetemplate;
}//function

function getSentOrderEmail( $_ordercode , $_idcrypt, $_title , $_message1, $_message2 , $_lang_code , $_lang_url   ){

     $basetemplate  = file_get_contents("../lib/util/mailservice/template.html", true);
    
     //$tittle = "Order #  $_ordercode   sent";
     $tittle = $_title;
     $content = '';
               
               $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper"  style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    
                                    $urlorder = '<a href="'.  getCurrentUrlPath().'/saleorder/viewr/id/'.$_idcrypt.'">'.$_ordercode.'</a>';
                                    
                                    $content .= '<p>'.$_message1.'</p> ';
                                    
                                    $content .= '<p>';
                                    $content .=  '<strong>'.$_lang_code.'</strong>: '.$_ordercode.'<br />';
                                    $content .=  '<strong>'.$_lang_url. '</strong>:  '.$urlorder.'<br />';
                                    $content .= '</p>';          
                                    
                                    $content .= '<p>'.$_message2.'</p> ';
                                    //$content .= '<p>Remember: You have 3 hours to cancel the order after its creation. After those period of time, any kind of attempt of cancellation will be denied</p> ';
                                    
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';
     
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;

      return $basetemplate;
}//function

function getAirtimeExceedLimitTest( $_entity ){

     $basetemplate  = file_get_contents("../lib/util/mailservice/template.html", true);
    
     $tittle = "Alerta de consumo ";
     $content = '';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper"  style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">';
                                    $content .= '<!-- LEFT COLUMN -->';
                            
                                                       $content .= '  The number '.$_entity->source.' reached the next limit ';
                                                        
                                                       /*
                                                       if($_entity->traffictypecode != "0"){
                                                           $content .= '<p>';
                                                           $content .=   $_entity->traffictypecode.'  '.$_entity->tunitalertqty.'  '.$_entity->tunitcode ;
                                                           $content .= '</p>';
                                                       }
                                                        if($_entity->chargecode != "0"){
                                                           $content .= '<p>';
                                                           if($_entity->chargecode != "UNIT"){
                                                                $content .=    '  USD '.$_entity->chargealerqty;
                                                           }else{
                                                               $content .=   $_entity->chargecode.'  '.$_entity->chargealerqty;
                                                           }
                                                           $content .= '</p>';
                                                       }
                                                       */
                                                        
                                                           $content .= ' El n&uacute;mero '.$_entity->source.' a excedido el siguiente l&iacute;mite ';

                                                            if ($_entity->additionalcharges  >  0 ) {
                                                                $content .= '<p>';
                                                                $content .= " Cargos adicionales por: $".$_entity->additionalcharges ." ";
                                                                $content .= '</p>';

                                                            }else if ($_entity->taidcrypt != "") {
                                                                $content .= '<p>';
                                                                $content .= " excedio el   ".$_entity->taqty ."% de su consumo ";
                                                                $content .= '</p>';
                                                            }
                                                        
                                                       
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';
     
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;

      return $basetemplate;
}//function
  
function getPasswordReset( $activationlink ){

     $basetemplate  = file_get_contents("../lib/util/mailservice/template.html", true);
    
     $tittle = "Password Reset";
     $content = '';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper"  style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">';
                                    $content .= '<!-- LEFT COLUMN -->';
                            
                                                       $content .= '  Your password recovery link is: '.$activationlink.' ';
     
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';
     
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;

      return $basetemplate;
}//function
 
function getMailTemplateRequestQuote( $name, $email, $phone, $company, $comments ){
    
         $basetemplate  = file_get_contents("../lib/util/mailservice/template.html", true);

        $tittle = "Quote request";
    $content = '';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Name</td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$name.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Email </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$email.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';  
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Phone </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$phone.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';                  
                
                
                if($company !=""){
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Company </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$company.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';                         
                }//if
                
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';

                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Comments <p>'.$comments.' </p></td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';

               
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';  
                
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;
      
      return $basetemplate;
}//function


function getMailTemplateChangeEmail( $name, $email, $subject, $comments ){
    
         $basetemplate  = file_get_contents("../lib/util/mailservice/template.html", true);

        $tittle = "Contact request";
    $content = '';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Name</td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$name.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Email </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$email.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';  
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Phone </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$phone.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';                  
                
                
                if($subject !=""){
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Subject </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$subject.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';                         
                }//if
                
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';

                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Comments <p>'.$comments.' </p></td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';

               
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';  
                
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;
      
      return $basetemplate;
}//function

function getMailTemplatePasswordChanged( $name){
    
         $basetemplate  = file_get_contents("../lib/util/mailservice/template.html", true);

        $tittle = "Password changed";
        $content = '';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                   
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Dear  '.$name.', </p></td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';                   
                   
                   $content .= ' </td>';
                $content .= '</tr>';
    
         
       
                
              
 
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';

                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;" >Your password was changed, plase use your new password to sign in to satkomm portal</a> </p></td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';

               
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';  
                
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;
      
      return $basetemplate;
}//function



function getMailTemplateRequestContact( $token, $email, $tokenlink, $newemail ){
    
         $basetemplate  = file_get_contents("../lib/util/mailservice/template.html", true);

        $tittle = "Change email request";
        $content = '';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                   
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">A email change was requested </p></td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';                   
                   
                   $content .= ' </td>';
                $content .= '</tr>';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Actual Email </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$email.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';  
       
                
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">New Email </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$newemail.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';                  
 
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';

                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;" >To approve the change requested please click in this link, <a href="'.$tokenlink.'">'.$tokenlink.'</a> </p></td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';

               
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';  
                
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;
      
      return $basetemplate;
}//function

function getMailTemplateBecomeDistributor( $name, $phone,  $email, $company, $website,  $comments ){
    
         $basetemplate  = file_get_contents("../lib/util/mailservice/template.html", true);

        $tittle = "Become distributor request";
    $content = '';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Name</td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$name.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Email </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$email.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';  
    
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Phone </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$phone.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';                  
                
                
                if($company !=""){
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Company </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$company.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';                         
                }//if
                
                if($website !=""){
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Website </td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';
                                           $content .= ' </td>';
                                       $content .= ' </tr>';
                                    $content .= '</table>';
                                    $content .= '<!-- RIGHT COLUMN -->';
                                    $content .= '<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">';
                                        $content .= '<tr>';
                                            $content .= '<td style="padding: 0 0 10px 0;">';
                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                        $content .= '<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"> '.$website.'</td>';
                                                    $content .= '</tr>';
                                                $content .= '</table>';
                                            $content .= '</td>';
                                        $content .= '</tr>';
                                    $content .= '</table>';
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';                         
                }//if                
                
                
                
                $content .= '<tr>';
                   $content .= ' <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">';
                        $content .= '<!-- TWO COLUMNS -->';
                        $content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                            $content .= '<tr>';
                                $content .= '<td valign="top" class="mobile-wrapper">';
                                    $content .= '<!-- LEFT COLUMN -->';

                                                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                                                    $content .= '<tr>';
                                                       $content .= ' <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Comments <p>'.$comments.' </p></td>';
                                                    $content .= '</tr>';
                                               $content .= ' </table>';

               
                                $content .= '</td>';
                            $content .= '</tr>';
                        $content .= '</table>';
                   $content .= ' </td>';
                $content .= '</tr>';  
                
                
      $basetemplate = str_replace("{title}", $tittle, $basetemplate)  ;
                
      $basetemplate = str_replace("{content}", $content, $basetemplate)  ;
      
      return $basetemplate;
}//function
