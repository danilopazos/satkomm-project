<?php
//string format on yyyy-mm-dd HH:ii:ss
function stringToTime($text){
     
    if(trim($text) != ""){
        $text = trim($text);
        $text = str_replace("\"", "", $text);
        $text = str_replace(" ", "", $text);
        $text = str_replace(":", "", $text);
        $text = str_replace("-", "", $text);
        
        return doubleval($text);
    }
    return doubleval(0);
}//function