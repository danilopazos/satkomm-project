<?php
function  periodToOption($_code, $_timecode, $_qty){
    if($_qty == 0 && $_timecode=='PREPAID'){
        return 'PREPAID';
    }else{
        $output = "";
        if($_qty > 1){
            $output =  $_qty.' '.$_timecode.'S';
        }else{
            $output =  $_qty.' '.$_timecode;
        }
        return $output;
    }
}