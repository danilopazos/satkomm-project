<?php

//CREADO EN CASO DE MALOGRAR EL ORGINA O PARA GUIA
//ARCHIVO ORIGINAL CustomPdf.php

class CustomPdf extends TCPDF {

    public $documentName = "";
    public $documentNumberText = "";
    public $documentNro = "--";
    public $langWords;
    public $companyInfo;
    public $imageLogo = "";
    public $waterMark = "";

    public function Header() {


        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image 

        $filename = 'temp.jpg';
        //file_put_contents('/tmp/' . $filename, $this->imageLogo);

        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();

        //$this->Image('/tmp/' . $filename, 10, 5, 50, 17);


//        $imgdata = base64_decode( $this->waterMark);
//            
//        $filename = 'temp2.jpg';
//        file_put_contents('/tmp/'.$filename, $this->waterMark);




        $this->SetFont('helvetica', '', 8);
        $this->setCellMargins(0, 0, 0, 0);

        $this->setCellMargins(0, 0, 0, 0);

        //$this->Image('../test/logo/line_separator.jpg', 67, 4, 2, 25);

        $this->setCellMargins(55, 4, 0, 0);

        $this->SetFont('helvetica', '', 7);

        $tableCompanyGenerals = '<table cellpading="0" cellspacing="0">';
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td style="color:#AAAAAA; font-weight:bold;">' . utf8_encode($this->companyInfo->name) . '</td>';
        $tableCompanyGenerals .= '</tr>';

        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td style="color:#AAAAAA;">' . utf8_encode($this->companyInfo->billaddress->street . '<br />' . $this->companyInfo->billaddress->city . ', ' . $this->companyInfo->billaddress->country) . '</td>';
        $tableCompanyGenerals .= '</tr>';
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td style="color:#AAAAAA;">' . utf8_encode($this->companyInfo->billaddress->state . ', ' . $this->companyInfo->billaddress->country) . '</td>';
        $tableCompanyGenerals .= '</tr>';
        $tableCompanyGenerals .= '</table>';

        $this->MultiCell(50, 4, $tableCompanyGenerals, 0, 'L', false, 0, '', '', true, 0, true);

        $this->setCellMargins(0, 0, 0, 0);
        //$this->Image('../test/logo/line_separator.jpg', 120, 4, 2, 25);

        $tableDocumentNumbering = '<table cellpading="0" cellspacing="0">';
        $tableDocumentNumbering .= '<tr>';
        $tableDocumentNumbering .= '<td style="color:#AAAAAA; font-weight:bold;">   ' . utf8_encode($this->documentNro) . '</td>';
        $tableDocumentNumbering .= '</tr>';
        $tableDocumentNumbering .= '</table>';

        $this->setCellMargins(3, 4, 0, 0);
        $this->SetFont('helvetica', '', 7);
        $tableCompanyGenerals = '<table cellpading="0" cellspacing="0">';
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td style="color:#AAAAAA;">' . $this->companyInfo->taxtypeid . ' ' . $this->companyInfo->taxid . ' </td>';
        $tableCompanyGenerals .= '</tr>';
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td style="color:#AAAAAA;">' . $this->langWords->lang_call_center . ':  ' . $this->companyInfo->phone . '</td>';
        $tableCompanyGenerals .= '</tr>';

        if (trim($this->companyInfo->fax) != '') {
            $tableCompanyGenerals .= '<tr>';
            $tableCompanyGenerals .= '<td style="color:#AAAAAA;">';
            $tableCompanyGenerals .= $this->langWords->lang_fax . ':  ' . $this->companyInfo->fax;
            $tableCompanyGenerals .= '</td>';
            $tableCompanyGenerals .= '</tr>';
        }

        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td style="color:#AAAAAA;">' . $this->langWords->lang_email . ':  ' . $this->companyInfo->email . '</td>';
        $tableCompanyGenerals .= '</tr>';
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td style="color:#AAAAAA;">' . $this->langWords->lang_web_site . ': ' . $this->companyInfo->website . '</td>';
        $tableCompanyGenerals .= '</tr>';

        $tableCompanyGenerals .= '</table>';

        $this->MultiCell(49, 4, $tableCompanyGenerals, 0, 'L', false, 0, '', '', true, 0, true);


        $this->MultiCell(75, 4, $tableDocumentNumbering, 0, 'L', false, 0, '', '', true, 0, true);

        $this->setCellMargins(0, 0, 0, 0);

        if (trim($this->header->textBanner) != "") {

            list($fontR, $fontG, $fontB) = sscanf(strtoupper(trim($this->header->colorBanner)), "#%02x%02x%02x");

            $this->SetAlpha(0.3);

            $this->SetTextColor($fontR, $fontG, $fontB);

            $this->SetFont('helvetica', '', $this->header->fontBanner);
            $this->StartTransform();
            $this->Rotate(20, 70, 110);
            $this->SetAlpha(0.5);
            $this->MultiCell(200, 0, strtoupper($this->header->textBanner), 0, 'C', false, 0, -10, 120, true, 0, true);
            $this->StopTransform();
            $this->SetAlpha(1);
        }
            
    }

    public function Footer() {
/*
        $urlBarCode = '/tmp/' . utf8_encode($this->documentNro) . '.png';
        barcode($urlBarCode, utf8_encode($this->documentNro), 20, 'horizontal', 'code128', true);

        $this->SetY(-20);

        $this->SetFont('helvetica', 'I', 8);

        $this->MultiCell(200, 10, '<img src="' . $urlBarCode . '" height="30" width="200"  /> ', 0, 'C', false, 0, '', '', true, 0, true);
*/
        $this->SetFont('helvetica', 'I', 7);
        $this->SetY(-8);
        $this->MultiCell(200, 10, $this->langWords->lang_by . ' ' . $this->header->creadtedFirstName . ' ' . $this->header->creadtedLastName . ',   ' . $this->langWords->lang_page . ' ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 'R', false, 0, '', '', true, 0, true);
        $this->Ln(2);
    }

    public function getDateUSA($date) {
        $aDate = explode("-", substr($date, 0, 10));
        $year = $aDate[0];
        $month = intval($aDate[1]);
        $day = $aDate[2];

        $dateText = $month . "/" . $day . "/" . $year;
        return $dateText;
    }

    public function getDateString($date) {
        $aDate = explode("-", substr($date, 0, 10));
        $year = $aDate[0];
        $month = intval($aDate[1]);
        $day = $aDate[2];

        $monthText = "";
        switch ($month) {
            case 1 : $monthText = $this->langWords->lang_month_01;
                break;
            case 2 : $monthText = $this->langWords->lang_month_02;
                break;
            case 3 : $monthText = $this->langWords->lang_month_03;
                break;
            case 4 : $monthText = $this->langWords->lang_month_04;
                break;
            case 5 : $monthText = $this->langWords->lang_month_05;
                break;
            case 6 : $monthText = $this->langWords->lang_month_06;
                break;
            case 7 : $monthText = $this->langWords->lang_month_07;
                break;
            case 8 : $monthText = $this->langWords->lang_month_08;
                break;
            case 9 : $monthText = $this->langWords->lang_month_09;
                break;
            case 10 : $monthText = $this->langWords->lang_month_10;
                break;
            case 11 : $monthText = $this->langWords->lang_month_10;
                break;
            case 12 : $monthText = $this->langWords->lang_month_12;
                break;
        }
        $dateText = $monthText . " " . $day . ", " . $year;
        return $dateText;
    }

//function

    public function moneyFormat($money) {
//        return number_format(doubleval($money), 2, '.', ',');
        return   number_format( $money, 2);
        //return money_format("%i ", $money);
    }

    public function getTextArray($text, $charsPerLine) {
        if (strlen($text) == 0) {
            return 0;
        }
        $aLines = explode("\n", $text);

        $aFinalLines = array();
        if (count($aLines) > 0) {
            foreach ($aLines as $line) {
                //we need to delimit each line per 170 characters
                if (strlen($line) > $charsPerLine) {

                    $tempIndex = 0;
                    $tempLine = "";
                    $aWords = explode(" ", $line);
                    foreach ($aWords as $word) {

                        //si la palabra es mayor a los caracteres por linea
                        if (strlen($word) > $charsPerLine) {
                            $totalNewLines = ceil(strlen($word) / $charsPerLine);

                            $subIndexStart = 0;
                            for ($i = 0; $i < $totalNewLines; $i++) {
                                $subIndexStart = $i * $charsPerLine;
                                if ($subIndexStart + $charsPerLine < strlen($word)) {
                                    $tempWordLine = substr($word, $subIndexStart, $charsPerLine);
                                } else {
                                    $tempWordLine = substr($word, $subIndexStart);
                                }
                                array_push($aFinalLines, $tempWordLine);
                                $tempIndex ++;
                            }//for
                        } else if (strlen($tempLine) + strlen($word) > $charsPerLine) {
                            array_push($aFinalLines, $tempLine);
                            $tempLine = "";
                        } else if ($tempIndex == 0) {
                            $tempLine .= $word;
                        } else {
                            $tempLine .= " " . $word;
                        }
                        $tempIndex ++;
                    }

                    if (strlen($tempLine) > 0) {
                        array_push($aFinalLines, $tempLine);
                        $tempLine = "";
                    }
                } else {
                    array_push($aFinalLines, $line);
                }
            }//foreach
        }//if
        return $aFinalLines;
    }

//function
}

//class