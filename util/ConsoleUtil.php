<?php
function consoleLog($text) {
    consoleLogEvent(1, $text);
}

function consoleLogEvent($type, $text) {
    $date = date('Y-m-d');
    $typeEvent = "Info";
    if($type == 1){
        $typeEvent = "info";
    }
    else if($type == 2){
        $typeEvent = "warning";
    }    
    else if($type == 3){
        $typeEvent = "error";
    }        
    $filename = $date.'_'.$typeEvent.'.log';
    $output = date('Y-m-d h:i:s')."->".$typeEvent.": ".$text;
    $config = new Config();
    file_put_contents ( $config->main_log.'/'.$filename ,$output."\n" , FILE_APPEND);
}//function
