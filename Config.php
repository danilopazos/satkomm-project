<?php
class Config {

    public $main_dbhost;
    public $main_dbport;
    public $main_dbuser;
    public $main_dbpass;
    public $main_dbname;
    public $main_tempfolderpath;
    public $server_code;
    public $server_token;
    public $server_crm;
    public $main_server;
    public $main_cachefolder;
    public $main_imagefolder;
    public $main_log;
    public $main_cdrfiles;
    public $aMaxtrixHouseProvidersEmail = array("rubenbocanegra@gmail.com", "ruben.bocanegra@satkomm.com");
    
    
    function __construct() {
        $this->main_dbhost = "localhost";
        $this->main_dbport = "3306";
        $this->main_dbuser = "root";
        $this->main_dbpass = "";
        $this->main_dbname = "satstore_20190429";

        
        $this->server_code = "tesam.com";
        $this->server_token = "mg4oz5k8yjjqdh51e55amunmicrdr9cvgiuoes33f242tltbj4i";
        $this->main_server = "http://dashboard.satkomm.site/index.php";
        $this->server_crm = "http://dashboard.satkomm.site";
        $this->main_tempfolderpath = "C:\\xampp\\tempfolder";
        $this->main_cachefolder = $this->main_tempfolderpath."/cache";
        $this->main_imagefolder = $this->main_tempfolderpath."/image";
        $this->main_cdrfiles = $this->main_tempfolderpath."/cdrfiles";
        $this->main_log = $this->main_tempfolderpath."/log";
        
    }//function

    public function loadTempFolder(){

        if(!(file_exists($this->main_cachefolder))){
            mkdir($this->main_cachefolder );
        }//function
        if(!(file_exists($this->main_imagefolder))){
            mkdir($this->main_imagefolder );
        }//function
        if(!(file_exists($this->main_log))){
            mkdir($this->main_log );
        }//function
    }//function

//function

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        //	return $this;
    }

}

//class
?>
