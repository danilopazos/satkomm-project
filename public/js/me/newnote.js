
function saveNote(){
    
    validate = validateInputComplete(['textTitle', 'textBody']);

    if(validate){
        data = $('#formNote').serialize();
        $.ajax({
            type: 'post',
            url: '/me/savenewnote',
            data: data,
            success: function(response){
                if(response !== "not_save"){
                    $('#hiddenCode').val(response);

                    Swal.fire({
                        text: modal_saved,
                        type: 'success',
                        showCancelButton: true,
                        confirmButtonText: modal_behere,
                        cancelButtonText: modal_checkall,

                    }).then((response)=> {
                        if(!response.value){
                            location.href='/me/notes';

                        }

                    });

                }else{
                    showErrorAlert();
                }
            },
            error: function(error){
                showErrorAlert();
            }
        });
    }else{
        showErrorAlertMessage(modal_noblank);
    }
    
}

function viewNote(code)
{
    location.href='/me/viewnote/'+code;
}

function deleteNote(){
    Swal.fire({
        text: modal_askquit,
        type: 'question',
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText: modal_yesdelete,
        cancelButtonText: modal_cancel,

    }).then((response)=> {
        if(response.value){

            data = $('#formNote').serialize();

            $.ajax({
                type: 'post',
                url: '/me/deletenote',
                data: data,
                success: function(response){
                    if(response){
                        location.href='/me/notes';

                    }else{
                        showErrorAlert();
                    }

                },
                error:function(error){
                    showErrorAlert();
                }
            });
        }

    });
    return false;
}

function closeAlertSaved(){
    $('#messageIsSave').slideUp();
}

/*

function autoSaveNote(){

    validate = false;

    data = $('#formNote').serialize();
    $.ajax({
        type: 'post',
        url: '/me/savenewnote',
        data: data,
        success: function(response){
            if(response !== "not_save")
            {

                $('#hiddenCode').val(response);
                validate = true;

            }
        }
    });

    return validate;

}
setInterval(function(){

    title = $('#textTitle').val().trim().length;
    body = $('#textBody').val().trim().length;

    if(title > 0 || body > 0){

        autoSaveNote();


            $('#messageIsSave').slideDown();


        $('#timeToSave').val('0');
        $('#isSaved').val('yes');

    }

}, 10000);

*/
