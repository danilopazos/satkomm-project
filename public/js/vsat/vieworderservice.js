
function uploadAttach(){
    
    inputFile = $('#fileAttach').val().length;

    if(inputFile > 0){
        Swal.showLoading();
        return true;

    }else{
        showErrorAlertMessage(notFiles);
        return false;

    }

}

function approveOrder(typeApprove){

    if(typeApprove == "withErrors"){
        checkWithProblems = $('#checkWithProblems').is(':checked');
        checkReview = $('#checkReview').is(':checked');
        if(checkWithProblems && checkReview){
            Swal.fire({
                text: modal_asksure_1,
                type: 'question',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonText: modal_btnsure,
                cancelButtonText: modal_btncancel,
        
            }).then((firstResponse)=> {
                if(firstResponse.value){
                    swal.fire({
                        type: 'warning',
                        confirmButtonText: modal_btnproceed,
                        confirmButtonColor: "#d4ac0d",
                        cancelButtonText: modal_btncancel,
                        showCancelButton: true,
                        showCloseButton: true,
                        title: userName +modal_asksure_2,
                        html:
                            '<p style="text-align: left; margin-bottom:-10px;">'+modal_text_describemotive+'</p>' +
                                '<textarea id="textAnotations" class="swal2-textarea"></textarea>' +
                            '<p style="text-align: left; margin-bottom:-10px; color: #e74c3c; font-weight: bold;">'+modal_text_warningaccept+'</p>' +
                                '<select class="swal2-input" id="lastCheckApprove">' +
                                    '<option selected="selected" value="false">'+modal_option_notsure+'</option>' +
                                    '<option value="true">'+modal_option_sure+'</option>' +
                                '</select>',
                
                    }).then(function (result) {
                        if(result.value) {
                            notes = $('#textAnotations').val();
                            orderId = $('#orderServiceCode').val();
                            lastCheckApprove = $('#lastCheckApprove').val();
                            url = "/vieworderservicevsat/"+orderId;

                            if(notes.trim().length == 0){
                                showErrorAlertMessage(modal_error_describemotive);
                            
                            }else if(lastCheckApprove=="false"){
                                showErrorAlertMessage(modal_error_warningaccept);

                            }else{
                                $.ajax({
                                    type: 'get',
                                    data: {orderId:orderId, notes:notes},
                                    url: '/api/updateApproveOrder',
                                    cache: false,
                                    async: false,
                                    success: function(response){
                                        if(response){
                                            showSuccessAlertAndRedirect(modal_approved_witherrors, url);
                                        }else{
                                            showErrorAlert();
                                        }
                                    },
                                    error: function(error){
                                        showErrorAlert();
                                    }
                                }); 

                            }
                
                        }
                
                    });
                    
                }
        
            });

        }else{
            showErrorAlertMessage(modal_alert_acceptterms);

        }
    }else if(typeApprove == 'withoutErrors'){
        checkReview = $('#checkReview').is(':checked');
        if(checkReview){
            Swal.fire({
                text: modal_nearto_approve,
                type: 'question',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonText: modal_btncontinue,
                cancelButtonText: modal_btncancel,
        
            }).then((secondResponse)=> {
                if(secondResponse.value){
                    orderId = $('#orderServiceCode').val();
                    url = "/vieworderservicevsat/"+orderId;

                    $.ajax({
                        type: 'get',
                        data: {orderId:orderId},
                        url: '/api/updateApproveOrder',
                        cache: false,
                        async: false,
                        success: function(response){
                            if(response){
                                showSuccessAlertAndRedirect(modal_approved_ok, url);
                            }else{
                                showErrorAlert();
                            }
                        },
                        error: function(error){
                            showErrorAlert();
                        }
                    });
                }
        
            });
        }else{
            showErrorAlertMessage(modal_alert_acceptterms);

        }
    }
}

function disapproveOrder(){

    Swal.fire({
        text: modal_nearto_disapprove,
        type: 'warning',
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText: modal_btncontinue,
        cancelButtonText: modal_btncancel,
        confirmButtonColor: "#ec7063",

    }).then((secondResponse)=> {
        
        if(secondResponse.value){
            orderId = $('#orderServiceCode').val();
            url = "/vieworderservicevsat/"+orderId;

            $.ajax({
                type: 'get',
                data: {orderId:orderId},
                url: '/api/updateDisapproveOrder',
                cache: false,
                async: false,
                success: function(response){
                    if(response){
                        showSuccessAlertAndRedirect(modal_disapproved, url);
                    }else{
                        showErrorAlert();
                    }
                },
                error: function(error){
                    showErrorAlert();
                }
            });
            
        }

    });

}