function updateForm(){

    validate = validateInputComplete(['serialNumber', 'correlativeNumber', 'dateCreate', 'dateDue', 'currency', 'exchange', 'anotations']);
    
    if(validate){
        data = $('#formEditReceipt').serialize();
        $.ajax({
            type: 'post',
            data: data,
            url: '/vsat/updatereceipt',
            success: function(response){
                if(response){
                    url = '/listinvoicesvsat';
                    showSuccessAlertAndRedirect(modal_updated, url)
                }else{
                    showErrorAlert();
                }
            },
            error: function(error){
                showErrorAlert();
            }
        });
    }else{
        showAlertCompleteInfo(modal_complete);
    }

}