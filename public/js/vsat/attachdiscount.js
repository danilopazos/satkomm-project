function validateForm()
{
    validate = validateInputComplete(['textOrderCode', 'textAnotations', 'textDiscount', 
                                        'dateStart', 'dateEnd', 'fileAttach']);
    if(!validate){
        showAlertCompleteInfo(modalComplete);
    }
    return validate;
}