
function createReceipts(){

    Swal.showLoading();

    data = $('#formOrderConsumption').serialize();
    $.ajax({
        type: 'post',
        data: data,
        url: '/createconsumptionsreceipts',
        cache: false,
        async: false,
        success: function(response){
            if(response == "ok"){
                showSuccessAlertAndRedirect(modal_created, '/listinvoicesvsat')
            }else{
                showErrorAlert();
            }

        },
        error: function(error){
            showErrorAlert();
        }
    });

    return false;

}

/*
function createInvoice(e, sanCode, totalPay, period, orderid){
    date = new Date();

    month = (date.getMonth()+1).toString();
    if(month.length == "1"){
        month = ("-0"+month).toString();
    }else{
        month = ("-"+month).toString();
    }

    day = date.getDate().toString();
    if(day.length == "1"){
        day = ("-0"+day).toString();
    }else{
        day = ("-"+day).toString();
    }

    nowDate = date.getFullYear()+month+day;
    swal.fire({
        confirmButtonText: 'Registrar pago',
        cancelButtonText: 'Cancelar',
        showCancelButton: true,
        showCloseButton: true,
        title: 'Registrar pago',
        html:
            '<p>Número de SAN : '+sanCode+'<br />' +

            'Periodo de consumo : '+period+'<br />' +
            'Orden de servicio principal : '+orderid+'</p>' +

            '<p style="text-align: left; margin-bottom:-10px;">Tipo de comprobante</p>' +
                '<select id="selectTypeDocument" class="swal2-input">' +

                '</select>' +
            '<p style="text-align: left; margin-bottom:-10px;">Número del comprobante</p>' +
                '<input id="textNumberDocument" class="swal2-input">' +

            '<p style="text-align: left; margin-bottom:-10px;">Fecha de emisión del comprobante</p>' +
                '<input type="date" id="textDate" class="swal2-input" value="'+nowDate+'">'+

            '<p style="text-align: left; margin-bottom:-10px;">Total a pagar USD</p>' +
                '<input id="textTotalToPay" class="swal2-input" value="'+totalPay+'">' +

            '<p style="text-align: left; margin-bottom:-10px;">Anotaciones</p>' +
                '<input id="textAnotations" class="swal2-input">',

        onOpen: function () {
            $.get('/api/getlisttypeinvoice', {}, function(list){
                qty = list.length;
                for (i = 0; i < qty; i++) {
                    $('#selectTypeDocument').append("<option value='" + list[i]['code'] + "'>" + list[i]['description'] + "</option>");
                }
            });
            $('#selectTypeDocument').focus();
        }
    }).then(function (result) {
        if(result.value) {
            typeDoc = $('#selectTypeDocument').val();
            numberDoc = $('#textNumberDocument').val();
            dateDoc = $('#textDate').val();
            totalToPay = $('#textTotalToPay').val();
            notes = $('#textAnotations').val();

            $.ajax({
                type: 'get',
                url: '/newinvoicevsat',
                data: {sanCode:sanCode, period:period, orderid:orderid, typeDoc:typeDoc, numberDoc:numberDoc, dateDoc:dateDoc, totalToPay:totalToPay, notes:notes},
                success: function(response){

                    if(response){
                        showSuccessAlert('La información ha sido actualizada!');
                        var num = e.parentNode.parentNode.rowIndex;
                        $('#dataTable').find('tr:eq('+num+')').remove();

                    }else{
                        showErrorAlert();
                    }

                },
                error: function(error){
                    showErrorAlert();
                }
            });

        }

    });

    return false;
}
*/
