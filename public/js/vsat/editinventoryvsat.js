function updateInventoryVsat()
{
    data = $('#updateForm').serialize();

    $.ajax({
        type: 'post',
        url: '/updateinventoryvsat',
        data: data,
        success: function(response){
            showSuccessAlertAndRedirect(modal_save, '/listinventorysvsat');
        },
        error: function (data) {
            showErrorAlert();

        }
    });
    
}
