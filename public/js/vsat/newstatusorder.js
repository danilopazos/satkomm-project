function saveStatus(){

    statusBool = false;
    validate = validateInputComplete(['dateCreate', 'selectStatus']);

    if(validate) {
        orderCode = $('#textOrdenCode').val();
        data = $('#formNewStatus').serialize();
        select = $('#selectStatus').val();

        //validate
        if(select == 'S4'){
            $.ajax({
                type: 'get',
                url: '/api/getdatedueorder/'+orderCode,
                data: {},
                async: false,
                cache: false,
                success: function(response){
                    if(response <= 0){
                        statusBool = true;
                        Swal.showLoading();

                    }else{
                        Swal.fire({
                            type: 'error',
                            showCancelButton: true,
                            showCloseButton: true,
                            confirmButtonText: modal_sure,
                            cancelButtonText: modal_cancel,
                            confirmButtonColor: "#ec7063",
                            html: 
                                '<h4 class="text-danger" style="underline; font-weight: bold;">' +modal_title_sure +'</h4>' +
                                '<p>' +
                                    modal_text_1 +'<span style="text-decoration: underline; font-weight: bold;">' +modal_text_2 +'</span>.' +
                                    '<br />' +
                                    '<span style="text-decoration: underline; font-weight: bold;">' +modal_left +response +modal_days +to_due +'</span>'+
                                    '<br />' +
                                '</p>',
                    
                        }).then((alertResponse)=> {
                            if(alertResponse.value){
                                orderId = $('#textOrdenCode').val();
                                url = "/vieworderservicevsat/"+orderId;
                                validateNote = validateInputComplete(['textAnotations', 'inputFile']);

                                if(validateNote){
                                    $('#problems').val('true');
                                    Swal.showLoading();
                                    document.getElementById("formNewStatus").submit(); 

                                }else{
                                    showAlertCompleteInfo(modalComplete);
                                }
                                
                            }
                    
                        });

                    }
                }
            });

        }else{
            if(select == 'S5' || select == 'S3'){
                validate = validateInputComplete(['textAnotations', 'inputFile']);
            }

            if(validate){
                Swal.showLoading();
                statusBool = true;
            }else{
                showAlertCompleteInfo(modalComplete);
            }
        } 

    }else{
        showAlertCompleteInfo(modalComplete);
    }

    return statusBool;

}
