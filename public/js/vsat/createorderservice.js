
listCompanies = new Object();
listCompaniesShow = [];
listAddress = new Object();
listAddressShow = [];
listPlans = new Object();
listPlansShow = [];
listInventory = new Object();
listInventoryShow = [];
listPrices = new Object();
listPricesShow = [];

//**************** Show Div Modal ****************
function showDisplaySearch(elm, selector){
    coordenadas = $(elm).offset();
    alto = $(elm).outerHeight();
    largo = $(elm).width()

    pos_top = (coordenadas.top)+alto;
    pos_left = (coordenadas.left)-30;
    $('#'+selector).show();
    $('#'+selector).offset({
        top: pos_top,
        left: pos_left
    });
    $('#'+selector).width(largo+50)
    return false;
}

//**************** Call On Page Ready ****************
function getListCompanies(){

    $('#customerSelector').empty();
    $.get('/api/getcustomers', function (list) {
        listCompanies = list;
    });
}

//**************** Live Search On Modal ****************
textSave = "";
codeSave = "";
optionSelect = 1;
maxOptions = 0;

function searchDisplayElements(elm, selector, e){

    code = e.keyCode;

    showAllResults = false;
    keydown = false;
    keyup = false;


    if (code === 40 || code === 38){
        if( ! $('#'+selector).is(":hidden") ){
            if( code === 40) {
                keydown = true;
                keyup = false;

            }else if(code === 38){
                keydown = false;
                keyup = true;

            }

        }else{
            showAllResults = true;

        }
    }

    if(selector == "customerSelector") {
        functionToCall = "selectCustomer";
        optionId = "optionCustomer_";
        hiddenCode = "hiddenCodeCustomer";
        searchOn = "customer";

    }else if(selector == "addressSelector") {
        functionToCall = "selectAddress";
        optionId = "optionAddress_";
        hiddenCode = "hiddenCodeAddress";
        searchOn = "address";

    }else if(selector == "planSelector"){
        functionToCall = "selectPlan";
        optionId = "optionPlan_";
        hiddenCode = "hiddenCodePlan";
        searchOn = "plan";

    }else if(selector == "itemSelector"){
        functionToCall = "selectItem";
        optionId = "optionItem_";
        hiddenCode = "hiddenCodeItem";
        searchOn = "item";

    }else if(selector == "priceSelector"){
        functionToCall = "selectPrice";
        optionId = "optionPrice_";
        hiddenCode = "hiddenCodePrice";
        searchOn = "price";

    }

    if(code == 27){
        $(elm).val("");
        $('#'+selector).hide();

    }else if(code === 13){
        if(searchOn == "customer") {
            selectCustomer(textSave, codeSave);
            listAddressShow = [];
            $('#hiddenCodeAddress').val("");
            $('#selectAddress').val("");

        }else if(searchOn == "address"){
            selectAddress(textSave, codeSave);

        }else if(searchOn == "plan"){
            selectPlan(textSave, codeSave);

        }else if(searchOn == "item"){
            selectItem(textSave, codeSave);

        }else if(searchOn == "price"){
            selectPrice(textSave, codeSave);

        }
        textSave = "";
        codeSave = "";
        optionSelect = 1;
        maxOptions = 0;

    }else if(keydown == true || keyup == true){ // Down - Up

        $('#'+optionId+optionSelect).removeClass();
        $('#'+optionId+optionSelect).addClass("optionSelector");

        if(keydown ==true){
            optionSelect ++;
            if(optionSelect > maxOptions){
                optionSelect = maxOptions;
            }
        }else{
            optionSelect --;
            if(optionSelect<1){
                optionSelect = 1;
            }
        }

        $('#' + optionId + optionSelect).removeClass();
        $('#' + optionId + optionSelect).addClass("optionSelectorSelected");

        if(searchOn == "customer") {
            textSave = listCompaniesShow[optionSelect - 1]['name'];
            codeSave = listCompaniesShow[optionSelect - 1]['code'];

        }else if(searchOn == "address") {
            textSave = listAddressShow[optionSelect - 1]['name'];
            codeSave = listAddressShow[optionSelect - 1]['code'];

        }else if(searchOn == "plan"){
            textSave = listPlansShow[optionSelect - 1]['description'];
            codeSave = listPlansShow[optionSelect - 1]['code'];

        }else if(searchOn == "item"){
            textSave = listInventoryShow[optionSelect - 1]['brand'] +" - " +listInventoryShow[optionSelect - 1]['model'] +" - " +listInventoryShow[optionSelect - 1]['code'];
            codeSave = listInventoryShow[optionSelect - 1]['code'];
        }
        else if(searchOn == "price"){
            typePlan = $('#selectTypePlan').val();
            if(typePlan == "PS"){
                textSave = listPricesShow[optionSelect - 1]['months'] + text_months+" - "+ text_from +listPricesShow[optionSelect - 1]['min'] +text_to +listPricesShow[optionSelect - 1]['max'] +text_connections+": $" +listPricesShow[optionSelect - 1]['usdprice'];
                codeSave = listPricesShow[optionSelect - 1]['usdprice'];
                

            }else{
                textSave = listPricesShow[optionSelect - 1]['anotation'] +": $ " +listPricesShow[optionSelect - 1]['usd_price'];
                codeSave = listPricesShow[optionSelect - 1]['usd_price'];
            }
        }

    }else{

        optionSelect = 1;
        maxOptions = 0;

        if(showAllResults == false) {
            search = $(elm).val().trim();
        }else{
            search = " ";
        }
        count = 1;

        if(search.length > 0) {

            $('#'+selector).empty();

            if(searchOn == "customer") {
                listCompaniesShow = [];
                jQuery.map(listCompanies, function (obj) {

                    stringSearch = obj.name.toLowerCase() + " " + obj.taxnumber;

                    if ((stringSearch).includes(search.toLowerCase())) {

                        listCompaniesShow.push(obj);

                        if (count == 1) {
                            textSave = obj.name;
                            codeSave = obj.code;
                        }
                        $('#' + selector).append("<div class='optionSelector' id='" + optionId + count + "' onclick='" + functionToCall + "(\"" + obj.name + "\", \"" + obj.code + "\");'>" + obj.name + "<br />"+ obj.typedocument +": "+ obj.taxnumber +"</div>");

                        count ++;
                    }

                    maxOptions = count - 1;
                    $('#' + optionId + '1').addClass("optionSelectorSelected");

                });
            }else if(searchOn == "address"){
                listAddressShow = [];
                jQuery.map(listAddress, function (obj) {

                    stringSearch = obj.addressname +" "+obj.address +" " +obj.district +" " +obj.province +" " +obj.department +" " +obj.country;

                    if ((stringSearch.toLowerCase()).includes(search.toLowerCase())) {

                        listAddressShow.push(obj);

                        if (count == 1) {
                            textSave = obj.addressname;
                            codeSave = obj.code;
                        }
                        $('#' + selector).append("<div class='optionSelector' id='" + optionId + count + "' onclick='" + functionToCall + "(\"" + obj.addressname + "\", \"" + obj.code + "\");'>" + obj.addressname + "<br />"+ obj.address +" - "+ obj.district +", "+ obj.province +", "+ obj.department +", "+ obj.country +"</div>");
                        count ++;
                    }

                    maxOptions = count - 1;
                    $('#' + optionId + '1').addClass("optionSelectorSelected");

                });
            }else if(searchOn == "plan"){
                listPlansShow = [];

                jQuery.map(listPlans, function (obj) {

                    stringSearch = obj.description +" ";

                    if ((stringSearch.toLowerCase()).includes(search.toLowerCase())) {

                        listPlansShow.push(obj);

                        if (count == 1) {
                            textSave = obj.description;
                            codeSave = obj.code;
                        }
                        $('#' + selector).append("<div class='optionSelector' id='" + optionId + count + "' onclick='" + functionToCall + "(\"" + obj.description + "\", \"" + obj.code + "\");'>" + obj.description + "<br />"+ obj.band +"</div>");
                        count ++;
                    }

                    maxOptions = count - 1;
                    $('#' + optionId + '1').addClass("optionSelectorSelected");

                });

            }else if(searchOn == "item"){
                listInventoryShow = [];

                jQuery.map(listInventory, function (obj) {

                    stringSearch = obj.brand +" " +obj.model +" " +obj.code;
                    if(obj.anotations !== null) {
                        stringSearch = stringSearch +obj.anotations;
                    }

                    if ((stringSearch.toLowerCase()).includes(search.toLowerCase())) {

                        listInventoryShow.push(obj);

                        if (count == 1) {
                            textSave = obj.brand + " - " +obj.model +" - " +obj.code;
                            codeSave = obj.code;
                        }

                        textShow = obj.brand + " - "+ obj.model +"<br />"+text_codejs +obj.code;
                        if(obj.anotations !== null) {
                            textShow = textShow +"<br />"+text_anotationjs +obj.anotations;
                        }
                        $('#' + selector).append("<div class='optionSelector' id='" + optionId + count + "' onclick='" + functionToCall + "(\"" + obj.brand + " - " +obj.model +" - " +obj.code + "\", \"" + obj.code + "\");'>" +textShow +"</div>");
                        count ++;
                    }

                    maxOptions = count - 1;
                    $('#' + optionId + '1').addClass("optionSelectorSelected");

                });

            }else if(searchOn == "price"){
                listPricesShow = [];
                typePlan = $('#selectTypePlan').val();

                jQuery.map(listPrices, function (obj) {

                    if(typePlan == "PS"){
                        stringSearch = obj.months + text_months +text_from + obj.min + text_to +obj.max +" " +obj.usdprice;
                    }else{
                        stringSearch = obj.anotation +" " +obj.usd_price;
                    }

                    if ((stringSearch.toLowerCase()).includes(search.toLowerCase())) {

                        listPricesShow.push(obj);

                        if(typePlan == "PS"){
                            codeShow = obj.usdprice;
                            textShow = text_contractfor +obj.months + text_months + '<br>'+text_from +obj.min + text_to +obj.max + text_connections +' <br> $' +obj.usdprice;
                            textFunction = text_contractfor +obj.months + text_months + ' - '+text_from +obj.min + text_to +obj.max + text_connections +' : $' +obj.usdprice;;
                            if (count == 1){
                                textSave = textFunction;
                                codeSave = codeShow;
                            }
                        }else{
                            codeShow = obj.usd_price;
                            textShow = obj.anotation +'<br>$' +obj.usd_price;
                            textFunction = obj.anotation +' : $' +obj.usd_price;
                            if (count == 1){
                                textSave = textFunction;
                                codeSave = codeShow;
                            }
                        }

                        $('#' + selector).append("<div class='optionSelector' id='" + optionId + count + "' onclick='" + functionToCall + "(\"" + textFunction +"\", \"" + codeShow + "\");'>" +textShow +"</div>");
                        count ++;
                    }

                    maxOptions = count - 1;
                    $('#' + optionId + '1').addClass("optionSelectorSelected");

                });

            }

            if(count>1) {
                showDisplaySearch(elm, selector);

            }else{
                $('#' + selector).append("<div class='optionNoResults'>"+text_noresults+"</div>");
                showDisplaySearch(elm, selector);
            }
        }else{
            $('#' +selector).hide();
        }

    }

}

//**************** Choose An Option On Modal ****************
function selectCustomer(name, code){
    $('#hiddenCodeCustomer').val(code);
    $('#textCustomer').val(name);

    $.get('/api/getaddresscustomers', {code:code}, function (list){
        listAddress = list;
        $('#addressSelector').empty();
        jQuery.map(list, function (obj) {
            $('#addressSelector').append("<div class='optionSelector' onclick='selectAddress(\"" + obj.code + "\", \""+obj.addressname+"\");'>" + obj.addressname + "</div>");

        });
    });

    $('#customerSelector').hide();
    getListPlans();
}

function selectAddress(name, code){
    $('#hiddenCodeAddress').val(code);
    $('#selectAddress').val(name);
    $('#addressSelector').hide();
}

function selectPlan(name, code){
    $('#hiddenCodePlan').val(code);
    $('#selectPlan').val(name);
    $('#planSelector').hide();

    plan = $('#selectTypePlan').val();
    getListPrices(code, plan);

    $('#priceSelector').hide();
    $('#hiddenCodePrice').val("");
    $('#selectPlanPrice').val("");

}

function selectItem(name, code){
    $('#hiddenCodeItem').val(code);
    $('#selectAntenna').val(name);
    $('#itemSelector').hide();
}

function selectPrice(name, code) {
    $('#hiddenCodePrice').val(code);
    $('#selectPlanPrice').val(name);
    $('#priceSelector').hide();
}

//**************** Get Selects Lists ****************
function getListPlans(){

    band = $('#selectBand').val();
    plan = $('#selectTypePlan').val();
    customerCode = $('#hiddenCodeCustomer').val();
    
    if(band.length == 4 && plan.length == 2) {
        $.get('/api/getlistplans', {band: band, plan: plan, customerCode: customerCode}, function (list) {
            listPlans = list;
        });
    }

    $('#planSelector').hide();
    $('#hiddenCodePlan').val("");
    $('#selectPlan').val("");

    $('#priceSelector').hide();
    $('#hiddenCodePrice').val("");
    $('#selectPlanPrice').val("");
}

function getListPrices(code, plan){

    $('#selectPlanPrice').empty();
    $('#selectPlanPrice').append("<option value='---'>"+text_select+"</option>");
    customerCode = $('#hiddenCodeCustomer').val();

    $.get('/api/getlistplanprices', {code:code, plan:plan, customer:customerCode}, function (list){
        listPrices = list;
    });

}

function changeSellCategory(){

    $('#selectTypeProduct').empty();
    $('#selectTypeProduct').append("<option value='---'>"+text_select+"</option>");

    code = $('#selectSellCategories').val();

    $.get('/api/gettypeproducts', {code:code}, function (list){

        qty = list.length;
        for (i = 0; i < qty; i++) {
            $('#selectTypeProduct').append("<option value='" + list[i]['code'] + "'>" + list[i]['description'] + "</option>");
        }

    });

    $('#hiddenCodeItem').val("");
    $('#selectAntenna').val("");
    $('#itemSelector').hide();

}

function getListInventoryItems(){

    category = $('#selectSellCategories').val();
    type = $('#selectTypeProduct').val();

    if(category.length == 4 && type.length == 4){
        $.get('/api/getlistinventoryforneworder', {type:type}, function(list){
            listInventory = list;
        });

    }

}

function changeTypeActivation(){

    code = $('#selectTypeActive').val();
    if(code == "DM"){
        $('#selectPlanPrice').val(text_demoservice +"$ 0.00");
        $('#hiddenCodePrice').val("0.00");
        $('#selectPlanPrice').attr("disabled", true);

    }else{
        $('#selectPlanPrice').val("");
        $('#hiddenCodePrice').val("");
        $('#selectPlanPrice').attr("disabled", false);
    }
}

//**************** Search On Textbox ****************
function searchCodeExist(elm){

    code = $(elm).val();
    if(code.length == 8){
        $('#sendButtom').attr('disabled', true);

        $.get('/api/validateorderservicecode', {code:code}, function (message){

            if( message == "si" ){
                $(elm).css("border", "1px solid #c0392b");
                $('#sendButtom').attr('disabled', true);
                showErrorAlertMessage(modelCodeUsed);
                
            }else{
                $('#sendButtom').attr('disabled', false);
                $(elm).css("border", "1px solid #d5dbdb");

            }

        });

    }else{
        $('#sendButtom').attr('disabled', false);
        $(elm).css("border", "1px solid #d5dbdb");

    }
}

//**************** Delete Plan On Table ****************
function deleteItem(e){

    var num = e.parentNode.parentNode.rowIndex;
    $('#tablePlans').find('tr:eq('+num+')').remove();

    return false;
}

//**************** Button Add Plan ****************
function calculateDue(){

    text = $('#textTimeContract').val();
    if(text.trim().length == 0 || text.trim()==0){
        months = 0;
    }else{
        months = parseInt(text.trim());
    }

        dateStart = new Date($('#dateActivation').val());

        dateStart.setMonth(dateStart.getMonth() + months);

        if (dateStart.getMonth() < 9) {
            month = "0" + String(dateStart.getMonth() + 1);
        } else {
            month = dateStart.getMonth() + 1;
        }

        if (dateStart.getDate() < 10) {
            day = "0" + String(dateStart.getDate());
        } else {
            day = dateStart.getDate();
        }

        dateEnd = dateStart.getFullYear() + "-" + month + "-" + day;

        $('#dateDue').val(dateEnd);


}

//**************** Button Add Plan ****************
function addPlans(){

    codePlan = $('#hiddenCodePlan').val();
    pricePlan = $('#hiddenCodePrice').val();
    namePlan = $('#selectPlan').val();
    num = $('#qtyPlansTable').val();
    num ++;

    if(codePlan.length == 6) {
        if(pricePlan.length == 0){
            pricePlan = (0).toFixed(2);
        }
        //validate
        $('#tablePlans').append(`
        <tr>
            <td><a href='#' onclick='return deleteItem(this);'>` + table_delete + `</a></td>
            <td>` + codePlan + `<input type='hidden' id='codePlanTable_` + num + `' name='codePlanTable_` + num + `' value='` + codePlan + `'></td>
            <td>` + namePlan + `</td>
            <td>$ ` + pricePlan + `<input type='hidden' id='codePriceTable_` + num + `' name='codePriceTable_` + num + `' value='` + pricePlan + `'></td>
        </tr>
        `);

        $('#qtyPlansTable').val(num);

    }

    getListPlans();

}

//**************** Button Save ****************
function validateForm(){
    
    validate = validateInputComplete(['dateCreate', 'selectTypeBilling', 'selectTypeActive']);

    orderCode = $('#textOrdenCode').val();
        if(orderCode.length !== 8){
            validate = false;
            $('#textOrdenCode').css("border", "1px solid #c0392b");
        }else{
            $('#textOrdenCode').css("border", "1px solid #d5dbdb");
        }

    dateCreate = $('#dateCreate').val();
        if(dateCreate.length !== 10){
            validate = false;
            $('#dateCreate').css("border", "1px solid #c0392b");
        }else{
            $('#dateCreate').css("border", "1px solid #d5dbdb");
        }

    customerCode = $('#hiddenCodeCustomer').val();
        if(customerCode.length !== 4){
            validate = false;
            $('#textCustomer').css("border", "1px solid #c0392b");
        }else{
            $('#textCustomer').css("border", "1px solid #d5dbdb");
        }

    addressCode = $('#hiddenCodeAddress').val();
        if(addressCode.length !== 4){
            validate = false;
            $('#selectAddress').css("border", "1px solid #c0392b");
        }else{
            $('#selectAddress').css("border", "1px solid #d5dbdb");
        }

    return validate;

}

function saveOrder(){

    orderCode = $('#textOrdenCode').val();
    validate = validateForm();

    if(validate){
        data = $('#formNewOrder').serialize();
        $.ajax({
            type: 'post',
            url: '/saveneworderservicevsat',
            data: data,
            success: function(response){
                url = "/vieworderservicevsat/"+orderCode;
                showSuccessAlertAndRedirect(modalSaved, url);
            },
            error: function (data) {
                showErrorAlert();
            }
        });

    }else{
        showAlertCompleteInfo(modalComplete);
    }
}

function updateOrder(){

    orderCode = $('#textOrdenCode').val();
    validate = validateForm();

    if(validate){
        data = $('#formNewOrder').serialize();
        $.ajax({
            type: 'post',
            url: '/updateorderservicevsat',
            data: data,
            success: function(response){
                url = "/vieworderservicevsat/"+orderCode;
                showSuccessAlertAndRedirect(modalUpdated, url);
            },
            error: function (data) {
                showErrorAlert();
            }
        });

    }else{
        showAlertCompleteInfo(modalComplete);
    }

}
