/*
Swal.fire(
        'Good job!',
        '',
        'success'
    )
 */

function changeinventory(){

    $('#selectTypeProduct').empty();
    $('#selectTypeProduct').append("<option value='---'>"+optionSelect+"</option>");
    $('#selectBrandProduct').empty();
    $('#selectBrandProduct').append("<option value='---'>"+optionSelect+"</option>");
    $('#selectModelProduct').empty();
    $('#selectModelProduct').append("<option value='---'>"+optionSelect+"</option>");

    code = $('#selectInventory').val();
    $.get('/api/gettypeproducts', {code:code}, function (list){

        qty = list.length;
        for (i = 0; i < qty; i++) {
            $('#selectTypeProduct').append("<option value='" + list[i]['code'] + "'>" + list[i]['description'] + "</option>");
        }

    });

}

function changeproduct(){

    $('#selectBrandProduct').empty();
    $('#selectBrandProduct').append("<option value='---'>"+optionSelect+"</option>");
    $('#selectModelProduct').empty();
    $('#selectModelProduct').append("<option value='---'>"+optionSelect+"</option>");

    code = $('#selectTypeProduct').val();
    $.get('/api/getlistbrands', {code:code}, function (list){

        qty = list.length;
        for (i = 0; i < qty; i++) {
            $('#selectBrandProduct').append("<option value='" + list[i]['code'] + "'>" + list[i]['description'] + "</option>");
        }

    });

}

function changebrand(){

    $('#selectModelProduct').empty();
    $('#selectModelProduct').append("<option value='---'>"+optionSelect+"</option>");

    code = $('#selectBrandProduct').val();
    $.get('/api/getlistmodels', {code:code}, function (list){

        qty = list.length;
        for (i = 0; i < qty; i++) {
            $('#selectModelProduct').append("<option value='" + list[i]['code'] + "'>" + list[i]['description'] + "</option>");
        }

    });
}

function changeentrygroup(){

    code = $('#selectEntryGroup').val();

    if (code == "EGE") {
        $('#divCreateGroup').hide();
        $('#divGroupSelection').show();

    } else if (code == "ENC") {
        $('#divCreateGroup').show();
        $('#divGroupSelection').hide();

    }else{
        $('#divCreateGroup').hide();
        $('#divGroupSelection').hide();

    }

}
