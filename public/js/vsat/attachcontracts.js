
function uploadAttach(){
    
    validate = validateInputComplete(['typeAttach', 'fileAttach']);
    
    if(validate){
        Swal.showLoading();
        return true;

    }else{
        showErrorAlertMessage(notFiles);
        return false;

    }

}