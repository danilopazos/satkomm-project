function changeTypePlan(){

    typeplan = $('#selectTypePlan').val();
    if(typeplan == 'AD') {

        $("#div_download_1").hide();
        $("#div_download_2").hide();

        $("#div_upload_1").hide();
        $("#div_upload_2").hide();

        $("#div_early_1").hide();
        $("#div_early_2").hide();

        $("#div_total_1").hide();
        $("#div_total_2").hide();

        $("#div_cir_1").hide();
        $("#div_cir_2").hide();

        
        if($('#div_alltime_1').is(':visible')){
            $("#div_alltime_1").hide();
        }

        if($('#div_alltime_2').is(':visible')){
            $("#div_alltime_2").hide();
        }

        /*
        $('#div_postpaidplans').hide();
        $('#div_prepaidplans').show();
        */
    }else if(typeplan == 'TK'){
        $("#div_download_1").hide();
        $("#div_download_2").hide();

        $("#div_upload_1").hide();
        $("#div_upload_2").hide();

        $("#div_early_1").hide();
        $("#div_early_2").hide();

        $("#div_total_1").hide();
        $("#div_total_2").hide();

        $("#div_cir_1").hide();
        $("#div_cir_2").hide();

        if(!$('#div_alltime_1').is(':visible')){
            $("#div_alltime_1").show();
        }

        if(!$('#div_alltime_2').is(':visible')){
            $("#div_alltime_2").show();
        }

        /*
        $('#div_postpaidplans').hide();
        $('#div_prepaidplans').show();
        */
    }else if(typeplan == 'PS'){

        $("#div_download_1").show();
        $("#div_download_2").show();

        $("#div_upload_1").show();
        $("#div_upload_2").show();

        $("#div_early_1").show();
        $("#div_early_2").show();

        $("#div_total_1").show();
        $("#div_total_2").show();

        $("#div_cir_1").show();
        $("#div_cir_2").show();

        $("#div_alltime_1").show();
        $("#div_alltime_2").show();

        /*
        $('#div_postpaidplans').show();
        $('#div_prepaidplans').hide();
        */
    }

}

function addcolumn(){

    qty = parseInt($('#hiddenMaxCodePrepaid').val()) + 1;

    $("#prepaidtable").append("<tr>" +
                                "<td>#"+(qty)+"</td>" +
                                "<td><input type='text' class='form-control' id='prepaidnotes_"+qty+"' name='prepaidnotes_"+qty+"' placeholder='-' autocomplete='off'></td>" +
                                "<td><input type='text' class='form-control' id='prepaidprice_"+qty+"' name='prepaidprice_"+qty+"' placeholder='$' autocomplete='off'></td>" +
                              "</tr>");

    $('#hiddenMaxCodePrepaid').val(qty);

}

function savePlan(){
    validate = false;
    typeplan = $('#selectTypePlan').val();

    if(typeplan == 'AD'){
        validate = validateInputComplete(['textPlanName', 'textAnotationPlan', 'planCost']);
    }else if(typeplan == 'PS'){
        validate = validateInputComplete(['textPlanName', 'textAnotationPlan', 'textDownload', 'textUpload', 'planCost', 'commodeCost']);
    }else if(typeplan == 'TK'){
        validate = validateInputComplete(['textPlanName', 'textAnotationPlan', 'textTotal', 'planCost']);
    }

    if(validate){
        data = $('#formSavePlan').serialize();

        $.ajax({
            type: 'post',
            data: data,
            url: '/mycompany/savenewvsatplan',
            success: function(response){
                if(response){
                    showSuccessAlertAndRedirect(modal_saved, '/listplansvsat');
                }else{
                    showErrorAlert();
                }
            },
            error: function(error){
                showErrorAlert();
            }
        });

    }else{
        showAlertCompleteInfo(modal_complete);
    }

    return false;
}

function updatePlan(){
    data = $('#formSavePlan').serialize();

        $.ajax({
            type: 'post',
            data: data,
            url: '/mycompany/updatevsatplan',
            success: function(response){
                if(response){
                    showSuccessAlertAndRedirect(modal_update, '/listplansvsat');
                }else{
                    showErrorAlert();
                }
            },
            error: function(error){
                showErrorAlert();
            }
        });
}