
function saveCost(){

    typeRecord = $('#selectTypeRecord').val();
    if(typeRecord == 'EN'){
        validate = validateInputComplete(['orderCode', 'selectTypeRecord', 'dateCreate', 'selectCost', 
                                        'selectDocument', 'numberDocument', 'selectCurrency', 'costAmount', 'fileAttach']);

    }else{
        validate = validateInputComplete(['orderCode', 'selectTypeRecord', 'dateCreate', 'selectProvider', 'selectCost', 
                                        'selectDocument', 'numberDocument', 'selectCurrency', 'costAmount', 'fileAttach']);
    }
    
    if(validate){
        Swal.showLoading();
        return true;
        
    }else{
        showAlertCompleteInfo(modal_complete);
        return false;
    
    }
}

function changeTypeRecord(){
    typeRecord = $('#selectTypeRecord').val();
    if(typeRecord == 'EN'){
        $('#selectProvider').prop('disabled', true);

    }else{
        $('#selectProvider').prop('disabled', false);

    }

}