
function uploadAttach(){
    
    validate = validateInputComplete(['typeAttach', 'fileAttach', 'dateExpedition']);
    
    if(validate){
        Swal.showLoading();
        return true;

    }else{
        showErrorAlertMessage(notFiles);
        return false;

    }

}