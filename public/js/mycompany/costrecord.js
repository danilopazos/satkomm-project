
function newRecord(){
    
    swal.fire({
        confirmButtonText: modal_save,
        cancelButtonText: modal_cancel,
        showCancelButton: true,
        showCloseButton: true,
        title: modal_title,
        html:
            '<br />' +
            '<p align="left">' +modal_text +
                '<input id="inputDetail" class="swal2-input" maxlength="75">' + 
            '</p>' ,

        onOpen: function () {
            $('#inputDetail').focus();

        }
    }).then(function (result) {

        if(result.value) {
            description = $('#inputDetail').val();

            $.ajax({
                type: 'get',
                url: '/mycompany/savecost',
                data: {description: description},
                async: false,
                cache: false,
                success: function(response){
                    if(response){
                        url = '/mycompany/costsrecord';
                        showSuccessAlertAndRedirect(modal_saved, url);
                    }else{
                        showErrorAlert();
                    }
                },
                error: function(error){
                    showErrorAlert();
                }
            });

        }

    });

}