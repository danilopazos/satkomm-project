
function createNewCurrency(){
    
    swal.fire({
        confirmButtonText: modal_btnsave,
        cancelButtonText: modal_btncancel,
        showCancelButton: true,
        showCloseButton: true,
        title: modal_title,
        html:
            modal_selectCur +
                '<select id="selectCurrency" class="swal2-input"></select>' +

            modal_exchange +
                '<input id="amountExchange" class="swal2-input">', 

        onOpen: function () {
            
            $.get('/api/getlistcurrencies', {}, function(list){
                qty = list.length;
                for (i = 0; i < qty; i++) {
                    $('#selectCurrency').append("<option value='" + list[i]['code'] + "'>" + list[i]['name'] + " - " + list[i]['iso4217'] + "</option>");
                }
            }); 
            $('#textAnotations').focus();
        }
    }).then(function (result) {
        if(result.value) {
            codeCurrency = $('#selectCurrency').val();
            amountExchange = $('#amountExchange').val();
            $.ajax({
                type: 'get',
                data: {codeCurrency:codeCurrency, amountExchange:amountExchange},
                url: '/mycompany/savecurrency',
                success: function(response){
                    if(response){
                        showSuccessAlertAndRedirect(modal_success, '/typeexchange');
                    }else{
                        showErrorAlert();
                    }
                    
                },
                error: function(error){
                    showErrorAlert();
                }
            });
        }

    });

    return false;
}
