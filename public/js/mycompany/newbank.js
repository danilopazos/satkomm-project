
function createNewCurrency(){
    
    swal.fire({
        confirmButtonText: modal_btnsave,
        cancelButtonText: modal_btncancel,
        showCancelButton: true,
        showCloseButton: true,
        title: modal_title,
        html:
            modal_subtitle_bank +
                '<select id="selectBank" class="swal2-input"></select>' +

            modal_subtitle_currency +
                '<select id="selectCurrency" class="swal2-input"></select>' +

            modal_subtitle_number +
                '<input id="numerAccount" class="swal2-input">' +

            modal_subtitle_cci +
                '<input id="cciAccount" class="swal2-input">',

        onOpen: function () {
            $.get('/api/getlistbanks', {}, function(list){
                qty = list.length;
                for (i = 0; i < qty; i++) {
                    $('#selectBank').append("<option value='" + list[i]['code'] + "'>" + list[i]['description'] + "</option>");
                }
            });
            $.get('/api/getmycurrencies', {}, function(list){
                qty = list.length;
                for (i = 0; i < qty; i++) {
                    $('#selectCurrency').append("<option value='" + list[i]['code'] + "'>" + list[i]['name'] +" - "+ list[i]['iso4217'] + "</option>");
                }
            });
            $('#selectBank').focus();
        }
    }).then(function (result) {
        if(result.value) {
            codeBank = $('#selectBank').val();
            codeCurrency = $('#selectCurrency').val();
            numberAccount = $('#numerAccount').val();
            cciAccount = $('#cciAccount').val();
            $.ajax({
                type: 'get',
                data: {codeBank:codeBank, codeCurrency:codeCurrency, numberAccount:numberAccount, cciAccount:cciAccount},
                url: '/bank/save',
                success: function(response){
                    if(response){
                        showSuccessAlertAndRedirect(modal_confirmalert, '/bank/listbanks');
                    }else{
                        showErrorAlert();
                    }
                    
                },
                error: function(error){
                    showErrorAlert();
                }
            });
        }

    });

    return false;
}
