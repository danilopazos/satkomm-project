
function updateMonths(){

    validate = validateInputComplete(['monthsDue']);
    
    if(validate){
        months = $('#monthsDue').val();
        if($.isNumeric( months ) && months > 0){
            data = $('#formDueDocuments').serialize();
            $.ajax({
                type: 'post',
                url: '/mycompany/updatemonthsdue',
                data: data,
                success: function(response){
                    if(response){
                        showSuccessAlertAndRedirect(modal_saveok, '/mycompany/timefordocumentsdue');
                    }else{
                        showErrorAlert();
                    }
                },
                error: function(error){
                    showErrorAlert();
                }
            });
        }else{
            showErrorAlertMessage(modal_minterm);
        }

    }else{
        showAlertCompleteInfo(modal_needtype);
    }

}