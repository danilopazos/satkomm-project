
numberAdds = 0;

function showPrices(code, title, typePlan, cost){

    swal.fire({
        confirmButtonText: modal_btn_savebutton,
        cancelButtonText: modal_btn_cancel,
        showCancelButton: true,
        showCloseButton: true,
        title: modal_title +title,
        width: '600px',
        html:
            '<form id="formModalPrices">' +
                '<h4>' +modal_subtitle_cost +'$ ' +cost +' USD</h4>' +
                '<p>' +
                '<table width="95%" id="tablePostpaid" align="center">' +
                    '<tr height="40px" bgcolor="#f8f9f9" align="left">' +
                        '<td width="50%"><span style="margin-left: 15px;">' +modal_table_description +'</span></td>' +
                        '<td width="50%">' +modal_table_price +'</td>' +
                    '</tr>' +
                '</table>' +
                '</p>'
                
                + '<a href="#" onclick="return addPrice();" style="display: none;" id="btnAddPrice"><i class="fas fa-plus-circle"></i> ' +modal_addprice +'</a>' +
             '</form>'
        ,

        onOpen: function () {
                numberAdds = 0;

                $.ajax({
                    type: 'get',
                    url: '/api/getlistcompletewithprices',
                    data: {code: code, customer: customer, plan: typePlan},
                    cache: false,
                    async: false,
                    success: function(response){
                        qty = response.length;

                        if(typePlan == 'PS'){
                            for(i = 0; i < qty; i++){
                                minText = response[i]['minText'];
                                if(response[i]['maxText'] > 0){
                                    maxText = response[i]['maxText'];
                                }else{
                                    maxText = modal_moreconnections;
                                }
                                monthText = response[i]['monthsText'];
                                textPrice = response[i]['usdprice'];

                                $('#tablePostpaid').append(
                                    '<tr height="40px">' +
                                        '<td align="left"><span style="margin-left: 15px;">' +modal_from +minText +modal_to +maxText +modal_connections +monthText +modal_months +'</span></td>' +
                                        '<td>' +
                                            '<input style="width: 95%;" type="text" class="form-control" name="price_' +code +'_' +response[i]['time_id'] +'_' +response[i]['connection_id'] +'" id="price_' +code +'_' +response[i]['time_id'] +'_' +response[i]['connection_id'] +'" value="' +textPrice +'" placeholder="' +modal_withoutassign +'">' +
                                        '</td>' +
                                    '</tr>'
                                );

                            }

                        }else{
                            $('#btnAddPrice').show();
                            for(i = 0; i < qty; i++){
                                anotationText = response[i]['anotationText'];
                                textPrice = response[i]['usdprice'];

                                $('#tablePostpaid').append(
                                    '<tr height="40px">' +
                                        '<td align="left"><span style="margin-left: 15px;">' +anotationText +'</span></td>' +
                                        '<td>' +
                                            '<input style="width: 95%;" type="text" class="form-control" name="price_' +code +'_' +response[i]['id']+'" id="price_' +code +'_' +response[i]['id']+'" value="' +textPrice +'" placeholder="' +modal_withoutassign +'">' +
                                        '</td>' +
                                    '</tr>'
                                );

                            }

                        }
                    }
                });
        }
    }).then(function (result) {
        if(result.value) {
            data = $('#formModalPrices').serialize();
                data += '&codePlan=' +code;
                data += '&typePlan=' +typePlan;
                data += '&customerId=' +customer;
                data += '&numberAds=' +numberAdds;

            $.ajax({
                type: 'get',
                url: '/mycompany/savelistprices',
                data: data,
                async: false,
                cache: false,
                
                success: function(response){
                    url = '/mycompany/assignprices/' +customer;
                    showSuccessAlertAndRedirect(modal_saved, url);

                },
                error: function(error){
                    showErrorAlert();
                }
            });

        }

    });

}

function addPrice(){
    numberAdds ++;

    $('#tablePostpaid').append(
        '<tr height="40px">' +
            '<td align="left">' +
            '<input style="width: 95%;" type="text" class="form-control" name="anotation_new_' +numberAdds +'" id="anotation_new_' +numberAdds +'" value="" placeholder="' +modal_typenote +'">' +
            '</td>' +
            '<td>' +
                '<input style="width: 95%;" type="text" class="form-control" name="price_new_' +numberAdds +'" id="price_new_' +numberAdds +'" value="" placeholder="' +modal_typeprice +'">' +
            '</td>' +
        '</tr>' 
    );

    return false;
}