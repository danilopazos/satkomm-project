
function validateform(detail){
    if(detail == "complete") {
        validate = validateInputComplete(['selectCompany', 'selectTypeAddres', 'textFastAddress', 'textAddress', 
                                            'selectCountry', 'selectDepartment', 'selectProvince', 'selectDistrict']);
    }else{
        validate = validateInputComplete(['selectTypeAddres', 'textFastAddress', 'textAddress', 
                                            'selectCountry', 'selectDepartment', 'selectProvince', 'selectDistrict']);
    }

    return validate;
}

function updateAddress(){

    codeCompany = $('#hiddenCompanyCode').val();
    validate = validateform("");

    if(validate){

        data = $('#formNewAddress').serialize();

        $.ajax({
            type: 'post',
            url: '/updateaddresscompany',
            data: data,
            success: function(response){
                if(response) {
                    url = "/reviewcompany/"+codeCompany;
                    showSuccessAlertAndRedirect(modal_updated, url);
                }else{
                    showErrorAlert();
                }
            },
            error: function(data){
                showErrorAlert();
            }
        });

    }else{
        showAlertCompleteInfo(modal_complete);
    }

}

function saveAddress(){

    codeCompany = $('#selectCompany').val();
    validate = validateform("complete");

    if(validate){

        data = $('#formNewAddress').serialize();

        $.ajax({
            type: 'post',
            url: '/savenewaddress',
            data: data,
            success: function(response){
                url = "/reviewcompany/"+codeCompany;
                showSuccessAlertAndRedirect(modal_saved, url);
            },
            error: function(data){
                showErrorAlert();
            }
        });

    }else{
        showAlertCompleteInfo(modal_complete);
    }

}
