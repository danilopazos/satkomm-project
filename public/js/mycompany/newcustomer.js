
function validateForm(){

    validateText = validateInputComplete(['textNumberDocument', 'textNameCompany', 'textFastAddress', 'textAddress', 
                                            'selectCountry', 'selectDepartment', 'selectProvince', 'selectDistrict']);
    return validateText;
    
}

function saveNewCustomer(){
    validate = validateForm();

    if(validate){
        data = $('#formNewCustomer').serialize();

        $.ajax({
            type: 'post',
            data: data,
            url: '/savenewcustomer',
            success: function(response){
                if(response){
                    showSuccessAlertAndRedirect(modal_saved, '/mycustomers');
                }else{
                    showErrorAlert();
                }
            },
            error: function(error){
                showErrorAlert();
            }
        });
    }else{
        showAlertCompleteInfo(modal_complete);
    }
}

function updateCustomer(){
    validate = validateForm();

    if(validate){
        data = $('#formNewCustomer').serialize();

        $.ajax({
            type: 'post',
            data: data,
            url: '/updatecustomer',
            success: function(response){
                if(response){
                    showSuccessAlert(modal_updated);
                }else{
                    showErrorAlert();
                }
            },
            error: function(error){
                showErrorAlert();
            }
        });
    }else{
        showAlertCompleteInfo(modal_complete);
    }

}

function saveNewProvider(){
    validate = validateForm();

    if(validate){
        data = $('#formNewProvider').serialize();

        $.ajax({
            type: 'post',
            data: data,
            url: '/savenewprovider',
            success: function(response){
                if(response){
                    showSuccessAlertAndRedirect(modal_saved, '/myproviders');
                }else{
                    showErrorAlert();
                }
            },
            error: function(error){
                showErrorAlert();
            }
        });
    }else{
        showAlertCompleteInfo(modal_complete);
    }

}

function updateProvider(){
    validate = validateForm();

    if(validate){
        data = $('#formNewProvider').serialize();

        $.ajax({
            type: 'post',
            data: data,
            url: '/updateprovider',
            success: function(response){
                if(response){
                    showSuccessAlert(modal_updated);
                }else{
                    showErrorAlert();
                }
            },
            error: function(error){
                showErrorAlert();
            }
        });
    }else{
        showAlertCompleteInfo(modal_complete);
    }

}

function changeCountry(){
    $('#selectDepartment').empty();
    $('#selectDepartment').append("<option value='---'>"+selectText+"</option>");
    $('#selectProvince').empty();
    $('#selectProvince').append("<option value='---'>"+selectText+"</option>");
    $('#selectDistrict').empty();
    $('#selectDistrict').append("<option value='---'>"+selectText+"</option>");

    code = $('#selectCountry').val();

    $.get('/api/getdepartments', {code: code}, function (list) {
        qty = list.length;
        for (i = 0; i < qty; i++) {
            $('#selectDepartment').append("<option value='" + list[i]['code'] + "'>" + list[i]['name'] + "</option>");
        }
    });

}

function changeDepartment(){
    $('#selectProvince').empty();
    $('#selectProvince').append("<option value='---'>"+selectText+"</option>");
    $('#selectDistrict').empty();
    $('#selectDistrict').append("<option value='---'>"+selectText+"</option>");

    code = $('#selectDepartment').val();

    $.get('/api/getprovinces', {code: code}, function (list) {
        qty = list.length;
        for (i = 0; i < qty; i++) {
            $('#selectProvince').append("<option value='" + list[i]['code'] + "'>" + list[i]['name'] + "</option>");
        }
    });

}

function changeProvince(){
    $('#selectDistrict').empty();
    $('#selectDistrict').append("<option value='---'>"+selectText+"</option>");

    code = $('#selectProvince').val();

    $.get('/api/getdistricts', {code: code}, function (list) {
        qty = list.length;
        for (i = 0; i < qty; i++) {
            $('#selectDistrict').append("<option value='" + list[i]['code'] + "'>" + list[i]['name'] + "</option>");
        }
    });

}
