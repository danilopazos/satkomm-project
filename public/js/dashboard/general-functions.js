
function showSuccessAlert(message){
    Swal.fire({
        text: message,
        type: 'success',
        confirmButtonText:  modal_btnok,

    });
}

function showSuccessAlertAndRedirect(message, url){
    Swal.fire({
        text: message,
        type: 'success',
        confirmButtonText:  modal_btnok,

    }).then(() => {
        location.href = url;
    });
}

function showErrorAlertMessage(message){
    Swal.fire({
        html: "<span style='color: #ec7063; font-weight:bold;'>"+message+"<span>",
        type: 'error',
        confirmButtonColor: "#ec7063",
        confirmButtonText: modal_btnok,
    });
}

function showErrorAlert(){
    Swal.fire({
        html: "<span style='color: #ec7063; font-weight:bold;'>"+modal_ups+"<span>",
        type: 'error',
        confirmButtonColor: "#ec7063",
        confirmButtonText: modal_btnok,
    });
}

function showAlertCompleteInfo(message){
    Swal.fire({
        html: "<span style='color: #ec7063; font-weight:bold;'>"+message+"<span>",
        type: 'error',
        confirmButtonColor: "#ec7063",
        confirmButtonText: modal_btnok,

    }).then(()=> {
        $('html, body').animate({scrollTop:0}, 1250);

    });
}

function validateInputComplete(arrayText){
    
    validate = true;
    qty = arrayText.length;
    
    for(i = 0; i < qty; i++){
        inputId = arrayText[i];
        inputValue = $('#'+inputId).val().trim();

        if(inputValue.length == 0 || inputValue == '---'){
            $('#'+inputId).css("border", "1px solid #c0392b");
            validate = false;

        }else{
            $('#'+inputId).css("border", "1px solid #d5dbdb");

        }
    }

    return validate;

}