
countAlerts = 0;

function clickAlertRedirect(code, url){

    $.ajax({
        type: 'get',
        url: '/api/disabledalertid',
        data: {id: code},
        cache: false,
        async: false,
    });
    location.href = url;
    return false;

}

function openMyAlerts(){

    $.ajax({
        type: 'get',
        data: {},
        url: '/api/getalertmessages',
        cache: false,
        async: false,
        success: function (response) {
            qty = response.length;
            if(qty > 0){
                for(i = 0; i < qty; i++){
                    dateComplete = response[i]["created_at"];
                    day = dateComplete.substr(8, 2);
                    month = dateComplete.substr(5, 2);
                    year = dateComplete.substr(0, 4);
                    hour = dateComplete.substr(11, 2);
                    time = "am";
                    if(hour == 12){
                        time = "pm";
                    }
                    else if(hour > 12){
                        hour = hour - 12;
                        time = " pm";
                    }
                    switch(month){
                        case "01": textMonth = "enero"; break;
                        case "02": textMonth = "febrero"; break;
                        case "03": textMonth = "marzo"; break;
                        case "04": textMonth = "abril"; break;
                        case "05": textMonth = "mayo"; break;
                        case "06": textMonth = "junio"; break;
                        case "07": textMonth = "julio"; break;
                        case "08": textMonth = "agosto"; break;
                        case "09": textMonth = "septiembre"; break;
                        case "10": textMonth = "octubre"; break;
                        case "11": textMonth = "noviembre"; break;
                        default: textMonth = "diciembre";
                    }
                    minute = dateComplete.substr(14, 2);
                    datePrint = day+" "+textMonth+" "+year+" - "+hour+":"+minute+time;
                    $('#messagesAlerts').empty();
                    
                    $('#messagesAlerts').append('<a class="dropdown-item d-flex align-items-center" href="#" onclick="return clickAlertRedirect(\''+response[i]["code"]+'\', \''+response[i]["url"]+'\');">' +
                        '                                <div>' +
                        '                                    <span class="font-weight-bold" style="">'+response[i]["title"]+'</span><br />' +
                        '                                    <span class="font">'+response[i]["message"]+'</span><br />' +
                        '                                    <span class="small text-gray-600">'+datePrint+'</span>' +
                        '                                </div>' +
                        '                                <div class="dropdown-list-image mr-0">' +
                        '                                    <div class="rounded-circle text-gray-100"><i class="fas fa-'+response[i]["icon"]+' fa-3x"></i></div>' +
                        '                                </div>' +  
                        '                            </a>');

                }
            }else{
                $('#messagesAlerts').append('<div class="dropdown-item d-flex align-items-center">'+
                                                '<span class="text-gray-800">'+
                                                    '<br /><i class="fas fa-heart-broken"></i>  '+
                                                    'No tienes notificaciones pendientes!'+
                                                '<br />&nbsp;</span>'+
                                            '</div>');
            }
        }
    });

}

function firstTimeQtyAlerts(){
    $.ajax({
        type: 'get',
        data: {},
        url: '/api/getqtyalertmessages',
        cache: false,
        async: false,
        success: function (response) {
            countAlerts = response;
        }
    });

    if (countAlerts > 0) {
        $('#countAlerts').html(countAlerts);
        $('#countAlerts').show();

    }else{
        $('#countAlerts').hide();

    }
}

function getQtyAlerts(){

    if(countAlerts < 99) {

        $.ajax({
            type: 'get',
            data: {},
            url: '/api/getqtyalertmessages',
            success: function (response) {
                if(response !== countAlerts){
                    countAlerts = response;
                    $('#countAlerts').html(response);
                    $('#countAlerts').show();

                }
            }
        });

    }else{
        $('#countAlerts').html('+99');
        $('#countAlerts').show();
    }

}

/*
setInterval(function(){

    getQtyAlerts()

}, 10000);

*/