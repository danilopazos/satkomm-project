function updateMyProfile(){
    validate = validateInputComplete(['userName', 'userLastname']);
    if(validate){
        data = $('#formMyProfile').serialize();
        $.ajax({
            type: 'post',
            data: data,
            url: '/myprofile/update',
            cache: false,
            async: false,
            success: function(response){
                if(response){
                    showSuccessAlertAndRedirect(modal_saved, '/myprofile');
                }else{
                    showErrorAlert();
                }
            },
            error: function(error){
                showErrorAlert();
            }
        });
    }else{
        showAlertCompleteInfo(modal_complete);
    }
}