i=0;
function firstTimeGetQtyPendings(){

    $.ajax({
        type: 'get',
        url: '/api/getcountpendings',
        data: {},
        cache: false,
        async: false,
        success: function(response){
            $('#countPendings').html(response);
            $('#countPendings').show();

        }

    });

}

function getQtyPendings(){

    $.ajax({
        type: 'get',
        url: '/api/getcountpendings',
        data: {},
        success: function(response){
            $('#countPendings').html(response);
            $('#countPendings').show();

        }

    });

}

function openPendings(){

    $.ajax({
        type: 'get',
        url: '/api/getlistpendings',
        data: {},
        async: false,
        cache: false,
        success: function(response){
            qty = response.length;
            
            dateComplete = response[i]["date"];
            day = dateComplete.substr(8, 2);
            month = dateComplete.substr(5, 2);
            year = dateComplete.substr(0, 4);
            
            switch(month){
                case "01": textMonth = "enero"; break;
                case "02": textMonth = "febrero"; break;
                case "03": textMonth = "marzo"; break;
                case "04": textMonth = "abril"; break;
                case "05": textMonth = "mayo"; break;
                case "06": textMonth = "junio"; break;
                case "07": textMonth = "julio"; break;
                case "08": textMonth = "agosto"; break;
                case "09": textMonth = "septiembre"; break;
                case "10": textMonth = "octubre"; break;
                case "11": textMonth = "noviembre"; break;
                default: textMonth = "diciembre";
            }
            datePrint = day+" "+textMonth+" "+year;
            $('#pendingsAlerts').empty();

            for(i = 0; i<qty; i++){
                $('#pendingsAlerts').append('<a class="dropdown-item d-flex align-items-center" href="/vieworderservicevsat/'+response[i]["orderCode"]+'">' +
                                                '<div class="font-weight-bold">' +
                                                    '<div class="text">Orden de servicio : '+response[i]["orderCode"]+'</div>' +
                                                    '<div class="small text-secondary">'+response[i]["companyName"]+'</div>' +
                                                    '<div class="small text-secondary">Creado por : '+response[i]["userName"]+' '+response[i]["userLastname"]+' - '+datePrint+'</div>' +
                                                '</div>' + 
                                            '</a>');

            }

        }
    });

}

/*
setInterval(function(){

    getQtyPendings()

}, 10000);
*/