// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

let arrayDescription = [];
let arrayTotal = [];
let arrayBackColor = [];
let arrayHoverColor = [];

$.ajax({
    type: 'get',
    data: {},
    url: '/api/getlistinventory',
    cache: false,
    async: false,
    success: function(response){

        qty = response.length;
        inventoryTotal = 0;
        for (i = 0; i < qty; i++) {
            inventoryTotal = inventoryTotal + response[i]['total'];
        }


        for (i = 0; i < qty; i++) {

            description = text_inventory +response[i]['description'];
            total = (response[i]['total']);

            if(response[i]['code'] == "S1"){
                backColor = '#52be80';
                hoverColor = '#27ae60';

            }else if(response[i]['code'] == "S2"){
                backColor = '#f7dc6f';
                hoverColor = '#f4d03f';

            }else if(response[i]['code'] == "S3"){
                backColor = '#ec7063';
                hoverColor = '#e74c3c';

            }else if(response[i]['code'] == "S4"){
                backColor = '#5dade2';
                hoverColor = '#3498db';

            }else if(response[i]['code'] == "S5"){
                backColor = '#c39bd3';
                hoverColor = '#af7ac5';

            }

            arrayDescription.push(description);
            arrayTotal.push(total);
            arrayBackColor.push(backColor);
            arrayHoverColor.push(hoverColor);

        }

    }

});

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: arrayDescription,
    datasets: [{
      data: arrayTotal,
      backgroundColor: arrayBackColor,
      hoverBackgroundColor: arrayHoverColor,
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
      title: {
        display: true,
        text: text_total +inventoryTotal +text_items,
        padding: '10',
        fontSize: '16'
      },
    tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
    },
    legend: {
        responsive: true,
        display: true,
        position: 'bottom',
        labels: {
            boxWidth: 20,
            padding: 20
        }
    },
    cutoutPercentage: 70,
  },
});
