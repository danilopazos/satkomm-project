
function changePassword(){

    noMore = true;
    $newPass = $('#userNewPass').val();
    $repPass = $('#userRepPass').val();
    validate = validateInputComplete(['userPassword', 'userNewPass', 'userRepPass']);

    if($newPass !== $repPass){
        validate = false;
        noMore = false;

        $('#userRepPass').css("border", "1px solid #c0392b");
        $('#userNewPass').css("border", "1px solid #c0392b");
        
        showErrorAlertMessage(modal_passdifferent);

    }

    if(validate){

        data = $('#formChangePassword').serialize();

        $.ajax({
            type: 'post',
            data: data,
            url: '/savenewpassword',
            success: function (response){
                if(response == "updated"){
                    showSuccessAlert(modal_passsaved);
                }else{
                    showErrorAlertMessage(modal_passincorrect);
                }

            },
            error: function (error){
                showErrorAlert();
            }
        });

    }else{
        if(noMore) {
            showAlertCompleteInfo(modal_complete);
        }

    }

    $('#userPassword').val("");
    $('#userRepPass').val("");
    $('#userNewPass').val("");

}
