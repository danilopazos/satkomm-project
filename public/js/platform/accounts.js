function blockAccount(userId)
{
    url = '/admin/blockaccount/' +userId;
    $.ajax({
        type: 'get',
        url: url,
        success: function(response){
            showSuccessAlertAndRedirect('Usuario bloqueado!', '/admin/showusers');
        },
        error: function(error){
            showErrorAlert();
        }
    });
}

function resetPassword(userId)
{
    url = '/admin/resetpassword/' +userId;
    $.ajax({
        type: 'get',
        url: url,
        success: function(response){
            showSuccessAlertAndRedirect('Clave actualizada!', '/admin/showusers');
        },
        error: function(error){
            showErrorAlert();
        }
    });
}