<?php

class Datatable {

    public $_aColumns = array();
    public $_aSearch = array();
    public $_sortCol = "id";
    public $_sortDir = "asc";
    public $_limit1 = 0;
    public $_limit2 = 10;
    public $_search = "";

}

class DatatableHelper {

    public function processRequest($_datatableSetup) {

        $_aColumns = $_datatableSetup->_aColumns;
        $_aSearch = $_datatableSetup->_aSearch;
        $_sortCol = $_datatableSetup->_sortCol;
        $_sortDir = $_datatableSetup->_sortDir;
        $_limit1 = $_datatableSetup->_limit1;
        $_limit2 = $_datatableSetup->_limit2;
        $_search = $_datatableSetup->_search;
        
         
        //we check the limits
        /*
        if (isset($_REQUEST['iDisplayStart']) && $_REQUEST['iDisplayLength'] != '-1') {
            $_limit1 = intval($_REQUEST["iDisplayStart"]);
            $_limit2 = intval($_REQUEST["iDisplayLength"]);
        }
        */
        if (isset($_REQUEST['start']) && $_REQUEST['length'] != '-1') {
            $_limit1 = intval($_REQUEST["start"]);
            $_limit2 = intval($_REQUEST["length"]);
            
           // $_limit2 = $_limit1 + $_limit2;
            $_limit2 = $_limit2;
        }
        
        //we check the sorting
        /*
        if (isset($_REQUEST['iSortCol_0'])) {

            for ($i = 0; $i < intval($_REQUEST['iSortingCols']); $i++) {
                if ($_REQUEST['bSortable_' . intval($_REQUEST['iSortCol_' . $i])] == "true") {
                    $_sortCol = $_aColumns[intval($_REQUEST['iSortCol_' . $i])];
                    $_sortDir = $_REQUEST['sSortDir_' . $i];
                }
            }
        }
        */
        /*
        if (isset($_REQUEST['order'])) {
            $requestOrder = $_REQUEST["order"];
            $_sortCol = $_aColumns[intval($requestOrder["column"])];
            $_sortDir = $requestOrder["dir"];
        }
        */
        
        if (isset($_REQUEST['order'])) {
            
            if( isset($_REQUEST["order"][0]) ){
                $requestOrder = $_REQUEST["order"][0];
                $_sortCol = $_aColumns[intval($requestOrder["column"])];
                $_sortDir = $requestOrder["dir"];                
            }
            
        }
        
        
        
        /*
        if ($_REQUEST['sSearch'] != "") {
            $_search = trim($_REQUEST['sSearch']);
        }
        */
        
        if(isset($_REQUEST["search"])){
            $requestSearch = $_REQUEST["search"];
            $_search = $requestSearch["value"];
        }
        
        /* Individual column filtering */
        /*
        for ($i = 0; $i < count($_aColumns); $i++) {
            if ($_REQUEST['bSearchable_' . $i] == "true" && $_REQUEST['sSearch_' . $i] != '') {
                array_push($_aSearch, $_REQUEST['sSearch_' . $i]);
            } else {
                array_push($_aSearch, NULL);
            }
        }
        */
        
        if(isset($_REQUEST["columns"])){
            $requestColumns = $_REQUEST["columns"];
            $totalColumns = count($requestColumns);
            if($totalColumns > 0){
                foreach($requestColumns as $column){
                    $searchValue = $column["search"]["value"];
                    array_push($_aSearch, $searchValue);
                }
            }
        }
        
        
        $_datatableSetup->_aSearch = $_aSearch;
        $_datatableSetup->_sortCol= $_sortCol;
        $_datatableSetup->_sortDir = $_sortDir;
        $_datatableSetup->_limit1 = $_limit1;
        $_datatableSetup->_limit2 = $_limit2;
        $_datatableSetup->_search = $_search;
        
      
        return $_datatableSetup;
        
        
    }//function

}


//----JSON OUTPUT
/*
 array(7) {
  ["draw"]=>
  string(1) "4"
  ["columns"]=>
  array(5) {
    [0]=>
    array(5) {
      ["data"]=>
      string(1) "0"
      ["name"]=>
      string(0) ""
      ["searchable"]=>
      string(4) "true"
      ["orderable"]=>
      string(4) "true"
      ["search"]=>
      array(2) {
        ["value"]=>
        string(0) ""
        ["regex"]=>
        string(5) "false"
      }
    }
    [1]=>
    array(5) {
      ["data"]=>
      string(1) "1"
      ["name"]=>
      string(0) ""
      ["searchable"]=>
      string(4) "true"
      ["orderable"]=>
      string(4) "true"
      ["search"]=>
      array(2) {
        ["value"]=>
        string(0) ""
        ["regex"]=>
        string(5) "false"
      }
    }
    [2]=>
    array(5) {
      ["data"]=>
      string(1) "2"
      ["name"]=>
      string(0) ""
      ["searchable"]=>
      string(4) "true"
      ["orderable"]=>
      string(4) "true"
      ["search"]=>
      array(2) {
        ["value"]=>
        string(0) ""
        ["regex"]=>
        string(5) "false"
      }
    }
    [3]=>
    array(5) {
      ["data"]=>
      string(1) "3"
      ["name"]=>
      string(0) ""
      ["searchable"]=>
      string(4) "true"
      ["orderable"]=>
      string(4) "true"
      ["search"]=>
      array(2) {
        ["value"]=>
        string(0) ""
        ["regex"]=>
        string(5) "false"
      }
    }
    [4]=>
    array(5) {
      ["data"]=>
      string(1) "4"
      ["name"]=>
      string(0) ""
      ["searchable"]=>
      string(4) "true"
      ["orderable"]=>
      string(4) "true"
      ["search"]=>
      array(2) {
        ["value"]=>
        string(0) ""
        ["regex"]=>
        string(5) "false"
      }
    }
  }
  ["order"]=>
  array(1) {
    [0]=>
    array(2) {
      ["column"]=>
      string(1) "1"
      ["dir"]=>
      string(3) "asc"
    }
  }
  ["start"]=>
  string(1) "0"
  ["length"]=>
  string(2) "10"
  ["search"]=>
  array(2) {
    ["value"]=>
    string(1) "u"
    ["regex"]=>
    string(5) "false"
  }
  ["PHPSESSID"]=>
  string(32) "3d34b7c38f307902e674a5dda366e7ce"
}

 */