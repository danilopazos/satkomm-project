<?php

class LangWords{
    public $lang_product_title = 'Product Description';
    public $lang_quantity_title = 'Quantity';
    public $lang_price_unit_title = 'Price unit';
    public $lang_sub_total_title = 'Sub-total';
    
    public $lang_wiretransfer = 'Wire Transfer';
    public $lang_creditcard = 'Credit Card';    
    
    public $lang_paymentfee = 'Convenience fee';    
    public $lang_total_title = 'Total';
    public $lang_taxes_title = 'Taxes';
    public $lang_credit_terms = 'Credit Terms';
    public $lang_attn = 'Attn';
    public $lang_page = "Page";
    public $lang_freigth = 'Freigth'  ;
    public $lang_sales_man = 'Sales man';
    public $lang_nserie = "Serial";
     public $lang_document_intro = "";
     
    public $lang_comments = 'Comment';
    
    public $lang_ship_to = 'Ship to';
    public $lang_phone = 'Phone';
    
    public $lang_web_site = 'Web Site';
    public $lang_call_center = 'Call center';
    public $lang_email = 'Email';
    public $lang_fax = 'Fax';
    public $lang_month_01 = 'January';
    public $lang_month_02 = 'February';
    public $lang_month_03 = 'March';
    public $lang_month_04 = 'April';
    public $lang_month_05 = 'May';
    public $lang_month_06 = 'June';
    public $lang_month_07 = 'July';
    public $lang_month_08 = 'August';
    public $lang_month_09 = 'September';
    public $lang_month_10 = 'October';
    public $lang_month_11 = 'November';
    public $lang_month_12 = 'December';
    
    public $lang_by = 'by';
    
    public $lang_shipping_cost = "Shipping cost";
    public $lang_payment_fee = "Payment fee";
    
    public $lang_conditions = "Conditions";
    
    public $lang_shippingtrackingid    ="Tracking ID";  
    public $lang_ordercode    ="Order #";

    /////////////////////////////////////////////////////////////////////////

    public $lang_title_customer = "CUSTOMER INFORMATION";
    public $lang_account_customer = "Customer account";
    public $lang_name_customer = "Name / Company Name";
    public $lang_address_customer ="Address";
    public $lang_reference_customer = "Reference";
    public $lang_city_customer = "State";
    public $lang_province_customer = "City";
    public $lang_state_customer = "Department";
    public $lang_document_customer = "Tax Id";

    public $lang_number_invoice = "Invoice N°";
    public $lang_date_issued_invoice = "Date issued";
    public $lang_date_expiration_invoice = "Date expiration";
    public $lang_period_invoice = "Period";

    public $lang_tittle_summary = "INVOICE SUMMARY";
    public $lang_table_description_summary = "Description";
    public $lang_table_ammount_summary = "Ammount ";
    public $lang_table_subscriber_summary = "SUSCRIBER: ";

    public $lang_total_tax_title = 'Taxes. ';
    public $lang_total_pay_title = 'Total ammount';

    public $lang_callcenter_center = 'Customer Support: ';
    public $lang_email_center = 'E-Mail: ';

    public $lang_operation_details = 'OPERATIONS DETAILS';
    public $lang_tbtitle_date = 'DATE';
    public $lang_tbtitle_time = 'TIME';
    public $lang_tbtitle_subscriber = 'SUBSCRIBER';
    public $lang_tbtitle_destination = 'DESTINATION';
    public $lang_tbtitle_type = 'TYPE';
    public $lang_tbtitle_duration = 'DURATION';
    public $lang_tbtitle_aditional ='ADDITIONAL';

    public $lang_summary_comsuption_ad = 'Summary of additional consumption';
    public $lang_summary_no_comsuption_ad = 'No additional consumption was generated';

    public $lang_invoicetc_customerweb = 'Website';
    public $lang_invoicetc_namecontact = 'Contact person';
    public $lang_invoicetc_contactphone = 'Contact person phone';
    public $lang_invoicetc_contactmail = 'Contact person mail';
    public $lang_invoicetc_currencyname = 'Currency bill';

} 