<?php

class LangWords{
    
    public $lang_product_title = 'Producto';
    public $lang_quantity_title = 'Cantidad';
    public $lang_price_unit_title = 'Precio Unitario';
    public $lang_sub_total_title = 'Sub-total';
    
    public $lang_wiretransfer = 'Wire Transfer';
    public $lang_creditcard = 'Tarjeta de cr&eacute;dito';
    
    public $lang_paymentfee = 'Cargos por procesamiento';    
    public $lang_total_title = 'Total';
    public $lang_taxes_title = 'Impuesto';
    public $lang_attn = 'Atte.';
    public $lang_page = "P&aacute;gina";
    public $lang_freigth = 'Carga'  ;
    public $lang_credit_terms = 'Condiciones de crédito';
    public $lang_sales_man = 'Ejecutivo de Ventas';
    public $lang_nserie = "Serie";
    public $lang_document_intro = "";
    
    public $lang_comments = 'Comentario';
    
    public $lang_ship_to = 'Enviar a';
    public $lang_phone = 'Tel&eacute;fono';
    
    public $lang_web_site = 'Web';
    public $lang_call_center = 'Tel&eacute;fono';
    public $lang_email = 'Correo';
    public $lang_fax = 'Fax';
    public $lang_month_01 = 'Enero';
    public $lang_month_02 = 'Febrero';
    public $lang_month_03 = 'Marzo';
    public $lang_month_04 = 'Abril';
    public $lang_month_05 = 'Mayo';
    public $lang_month_06 = 'Junio';
    public $lang_month_07 = 'Julio';
    public $lang_month_08 = 'Agosto';
    public $lang_month_09 = 'Septiembre';
    public $lang_month_10 = 'Octubre';
    public $lang_month_11 = 'Noviembre';
    public $lang_month_12 = 'Diciembre';
    
    public $lang_by = 'por';
    
    public $lang_shipping_cost = "Costo de env&iacute;o";
    public $lang_payment_fee = "Cuota de pago";
    
    public $lang_conditions = "Condiciones";
    
    public $lang_shippingtrackingid    ="Tracking ID";
    public $lang_ordercode    ="Orden #";
     
    /////////////////////////////////////////////////////////////////////////

    public $lang_title_customer = "DATOS DEL CLIENTE";
    public $lang_account_customer = "Cuenta del Cliente";
    public $lang_name_customer = "Nombre / Razón Social";
    public $lang_address_customer ="Dirección";
    public $lang_reference_customer = "Referencia";
    public $lang_city_customer = "Distrito";
    public $lang_province_customer = "Provincia";
    public $lang_state_customer = "Departamento";
    public $lang_document_customer = "RUC";

    public $lang_number_invoice = "Recibo N°";
    public $lang_date_issued_invoice = "Fecha de Emisión";
    public $lang_date_expiration_invoice = "Fecha de Vencimiento";
    public $lang_period_invoice = "Periodo Facturado";

    public $lang_tittle_summary = "RESUMEN DEL RECIBO";
    public $lang_table_description_summary = "Descripción";
    public $lang_table_ammount_summary = "Importe en ";
    public $lang_table_subscriber_summary = "ABONADO: ";

    public $lang_total_tax_title = 'I.G.V. ';
    public $lang_total_pay_title = 'Total a pagar';
    
    public $lang_callcenter_center = 'Atención al Cliente: ';
    public $lang_email_center = 'E-Mail: ';

    public $lang_operation_details = 'DETALLE DE OPERACIONES';
    public $lang_tbtitle_date = 'FECHA';
    public $lang_tbtitle_time = 'HORA';
    public $lang_tbtitle_subscriber = 'ABONADO';
    public $lang_tbtitle_destination = 'DESTINO';
    public $lang_tbtitle_type = 'TIPO';
    public $lang_tbtitle_duration = 'DURACIÓN';
    public $lang_tbtitle_aditional ='ADICIONAL';

    public $lang_summary_comsuption_ad = 'Resumen de consumo adicional';
    public $lang_summary_no_comsuption_ad = 'No se generó consumo adicional';

    public $lang_invoicetc_customerweb = 'Sitio web';
    public $lang_invoicetc_namecontact = 'Persona de contacto';
    public $lang_invoicetc_contactphone = 'Teléfono de contacto';
    public $lang_invoicetc_contactmail = 'Correo de contacto';
    public $lang_invoicetc_currencyname = 'Moneda de la factura';

} 