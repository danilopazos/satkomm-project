
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/customerinvoices/index">
                    <?php echo $_lang_index_label_customerinvoices;  ?>
                </a>
            </li>
            <li class="active">
                    <?php echo $_lang_index_label_list;  ?>
            </li>
            <li class="search-box">
               <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
          
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_customerinvoices;  ?>
                <div class="panel-tools">
                     <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th><?php echo $_lang_view_label_invoiceno;  ?></th>
                            <th><?php echo $_lang_view_label_customeraccount;  ?></th>
                            <th><?php echo $_lang_view_label_subject;  ?></th> 
                            <th><?php echo $_lang_view_label_invoicedate;  ?></th> 
                            <th><?php echo $_lang_view_label_paymentstatus;  ?></th> 
                            <th><?php echo $_lang_view_label_total;  ?></th> 
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td> INV-100003 </td>
                            <td> MINERA BUENA VENTURA S.A. </td>
                            <td> 10 ISATPHONE 2 + PLAN 30 MIN </td> 
                            <td> 2017-09-10 </td>
                            <td> PENDIENTE </td>
                            <td> USD 10,000 </td>
                            <td> <a href="#" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        
                        <tr>
                            <td> INV-100004 </td>
                            <td> CRUZ DEL SUR S.A. </td>
                            <td> 3 ISATPHONE 2 </td> 
                            <td> 2017-09-07 </td>
                            <td> PENDIENTE </td>
                            <td> USD 3,200 </td>
                            <td> <a href="#" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        
                        <tr>
                            <td> INV-100005 </td>
                            <td> CRUZ DEL SUR S.A. </td>
                            <td> 10 TARJETA PREPAGO 75 MIN IRIDIUM </td> 
                            <td> 2017-09-10 </td>
                            <td> PAGADO </td>
                            <td> USD 1,200 </td>
                            <td> <a href="#" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        
                        <tr>
                            <td> INV-100006 </td>
                            <td> MINERA BUENAVENTURA S.A. </td>
                            <td> 2 TARJETAS PREPAGO 75 MIN IRIDIUM </td> 
                            <td> 2017-09-17 </td>
                            <td> PAGADO </td>
                            <td> USD 300 </td>
                            <td> <a href="#" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        
                        <tr>
                            <td> INV-100007 </td>
                            <td> CLINICA SAN PABLO S.A. </td>
                            <td> 3 IRIDIUM 9575 + PLAN 60 MIN </td> 
                            <td> 2017-09-12 </td>
                            <td> PAGADO </td>
                            <td> USD 6,000 </td>
                            <td> <a href="#" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
        
    </div>
</div>
<!-- end: PAGE CONTENT-->
