<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
    <!--<![endif]-->

    <head>
        <title><?php echo $_lang_head_title; ?></title>
        <link rel="shortcut icon" href="/assets/images/favicon.png" />
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="<?php echo $_lang_head_description; ?>" name="description" />
        <meta content="<?php echo $_lang_head_author; ?>" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/bootstrap/dist/css/bootstrap.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/font-awesome/css/font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/fonts/clip-font.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/iCheck/skins/all.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/sweetalert/dist/sweetalert.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/css/main.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/css/main-responsive.min.css" />
        <link type="text/css" rel="stylesheet" media="print" href="<?php echo $this->http_host; ?>/assets/css/print.min.css" />
        <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo $this->http_host; ?>/assets/css/theme/light.min.css" />
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    </head>

    <body class="login example1">

        <div class="main-login col-md-8 col-md-offset-2 col-sm-6 col-sm-offset-4">

            <!-- start: LOGIN BOX -->
            <div class="logo">
            <!--CLIP<i class="clip-clip"></i>ONE -->
             <img src="/media/getlogo/accountid/<?php echo $this->global_accountid; ?>" height="70em">
            </div>

            <div class="row">
                <div class="col-md-6">

                    <div class="panel panel-default">
                        <div class="panel-heading"> 
                            <i class="fa fa-lock"></i><strong>FOR YOUR PROTECTION </strong>
                        </div>
                        <div class="panel-body">

                            <!-- start: DYNAMIC TABLE PANEL -->
                            <p>
                                <strong>We don't recognize the computer your're using.</strong><br />
                                We'll need to confirm your identity before you can access your account(s)

                            </p>            
                            <p>
                                <strong>Why has this happened?</strong><br />
                                When we don't recognize the computer you're using, it may be because:
                            <ul>
                                <li>You're using a new computer or one you haven't used before</li>
                                <li>You've switched to a new browser or changed your browser settings</li>
                                <li>You've deleted your cookies</li>
                                <li>You've modified your computer, its operating system, or its software settings</li>
                                <li>Your internet provider changed its system settings affecting our ability to recognize your machine</li>
                            </ul>
                            </p>                                

                        </div>
                    </div>


                </div>
                <div class="col-md-6">


                    <div class="panel panel-default"  id="div_requestcode">
                        <div class="panel-heading"> 
                            <i class="fa fa-laptop"></i><strong>STEP 1: VERIFY THIS COMPUTER</strong>
                        </div>
                        <div class="panel-body">



                            <form class="form-login" action="#" id="form_sendcode">

                                <fieldset>
                                    <div class="form-group">
                                        <p><strong>We need to send you an Identification Code <i class="fa fa-lock"></i></strong> <br />
                                            We need to confirm your identity to ensure your accounts are secure. We do this sending a temporary
                                            IdentificationCode to your telephone number o email address you provided us in the past.
                                        </p>

                                        <strong>
                                            Select Delivery Method
                                        </strong>    

                                        <input type="hidden" name="username" value="<?php echo $this->username; ?>" />
                                        <select name="destination" id="destination" class="form-control">
                                            <?php
                                            if (trim($this->email) != "") {
                                                echo '<option value="email">Email:' . $this->email . '</option>';
                                            }
                                            if (trim($this->mobile) != "") {
                                                echo '<option value="mobile">Telephone:' . $this->mobile . '</option>';
                                            }
                                            ?>
                                        </select>

                                    
                                        <!-- To mark the incorrectly filled input, you must add the class "error" to the input -->
                                        <!-- example: <input type="text" class="login error" name="login" value="Username" /> -->
                                    </div>


                                    <div class="form-actions">

                                        <button type="button" id="btn_sendcode" class="btn btn-bricky pull-right">
                                            Send Identification Code <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>

                                </fieldset>
                            </form>






                        </div>
                    </div>                        



                    <div class="panel panel-default"  id="div_validationcode"  style="display: none;" >
                        <div class="panel-heading"> 
                            <i class="fa fa-unlock"></i><strong>STEP 2: INTRODUCE THE CODE</strong>
                        </div>
                        <div class="panel-body">

                            <div class="errorHandler alert alert-warning no-display " id="message_codesent">
                                <i class="fa fa-remove-sign"></i> <?php echo 'The identification code was sent!'; ?>
                            </div>
                            <div class="errorHandler alert alert-success no-display " id="message_codevalidated">
                                <i class="fa fa-remove-sign"></i> <?php echo 'Identification code validated, now you can sign in'; ?>
                            </div>  
                            <div class="errorHandler alert alert-danger no-display "  id="message_codewrong">
                                <i class="fa fa-remove-sign"></i> <?php echo 'Identification code wrong!'; ?>
                            </div>  
                            
                            <form class="form-login" action="#" id="form_validatecode">

                                <fieldset>
                                    <div class="form-group">
                                        <p> 
                                            <span id="message_mobile">Introduce the identification code sent to your telephone</span> <br />
                                            <span id="message_email">Introduce the identification code sent to your email</span>
                                        </p>

                                        <strong>
                                            Introduce the 6 digits code <i class="fa fa-unlock"></i>
                                        </strong>     

                                        <input type="text" class="form-control" name="identificationcode" id="identificationcode" />
                                        <input type="hidden" name="username" value="<?php echo $this->username; ?>" />
                                      
                                        <!-- To mark the incorrectly filled input, you must add the class "error" to the input -->
                                        <!-- example: <input type="text" class="login error" name="login" value="Username" /> -->
                                    </div>


                                    <div class="form-actions">

                                        <button type="button" id="btn_validatecode" class="btn btn-bricky pull-right">
                                            Validate the identification code <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>

                                </fieldset>
                            </form>






                        </div>
                    </div>                        




                </div>                    
            </div>    


            <!-- end: LOGIN BOX -->


            <!-- start: COPYRIGHT -->
            <div class="copyright">
                <script>
                    document.write(new Date().getFullYear());
                </script> &copy; <?php echo $_lang_index_label_copyright; ?>
            </div>
            <!-- end: COPYRIGHT -->
        </div>

        <?php require_once '../lib/view/template/default/spinner.php'; ?>
        <?php require_once '../lib/view/template/default/validate.php'; ?>



        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
                <script src="../../bower_components/respond/dest/respond.min.js"></script>
                <script src="../../bower_components/Flot/excanvas.min.js"></script>
                <script src="../../bower_components/jquery-1.x/dist/jquery.min.js"></script>
                <![endif]-->
        <!--[if gte IE 9]><!-->
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
        <!--<![endif]-->
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/blockUI/jquery.blockUI.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/iCheck/icheck.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/jquery.cookie/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/js/min/main.min.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="<?php echo $this->http_host; ?>/assets/js/modules/templates/jquery.validate.js"></script>
        <script src="<?php echo $this->http_host; ?>/assets/js/modules/auth/verificationcode.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

        <script>
                    jQuery(document).ready(function () {
                        Main.init();
                        VerificationCode.init();
                    });
        </script>

    </body>

</html>