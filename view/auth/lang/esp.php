<?php
//--- view/index.php
$_lang_index_head_title = 'Telco Admin - Login';
$_lang_index_head_description= 'Telco Admin Dashboard';
$_lang_index_head_author = 'Telco Admin';


$_lang_index_label_title = 'Iniciar sesi&oacute;n con tu cuenta';
$_lang_index_label_description = 'Por favor ingrese su correo y contrase&ntilde;a para iniciar sesi&oacute;n.';
$_lang_index_label_username = 'Correo';
$_lang_index_label_password = 'Contrase&ntilde;a';
$_lang_index_label_forgot= 'Olvid&eacute; mi contrase&ntilde;a';
$_lang_index_label_remember = 'Manternerme conectado';
$_lang_index_button_submit= 'Iniciar Sesi&oacute;n';
$_lang_index_label_copyright = 'Telco Admin';

$_lang_index_label_formerror = ' Tiene algunos errores en su formulario. Por favor verifique.';
$_lang_index_label_errorauth = 'Contrase&ntilde;a y/o correo inv&aacute;lido';


//--- view/forgot.php
$_lang_forgot_head_title = 'Telco Admin - Recuperar contrase&ntilde;a';
$_lang_forgot_head_description= 'Telco Admin Dashboard';
$_lang_forgot_head_author = 'Telco Admin';

$_lang_forgot_label_title = 'Recuperar mi contrase&ntilde;a';
$_lang_forgot_label_description = 'Por favor introduzca su correo para recuperar su contrase&ntilde;a.';
$_lang_forgot_label_username = 'Correo';
$_lang_forgot_button_submit= 'Recuperar mi contrase&ntilde;a';
$_lang_forgot_label_formerror = ' Usted tiene algunos errores. Por favor verifique abajo.';
$_lang_forgot_label_errorforgot = 'Su correo eletr&oacute;nico no est&aacute; registrado';
$_lang_forgot_label_recoverylinksend = 'Su enlace de recuperaci&oacute;n ha sido enviado a su correo';

//--- view/validateforgot.php
$_lang_validateforgot_label_title = "Telco Admin - Cambiar Conta&ntilde;a";
$_lang_validateforgot_label_description = "Por favor introduzca la nueva contrase&ntilde;a";

$_lang_validateforgot_label_tokennotvalid = "El enlace de recuperaci&oacute;n no es v&aacute;lido";

$_lang_validateforgot_label_password = "Contrase&ntilde;a";
$_lang_validateforgot_label_confirmpassword = "Confirmaci&oacute;n de contrase&ntilde;a";

$_lang_validateforgot_label_formerror = "El formulario presenta algunos errores, por favor verifique";
$_lang_validateforgot_label_errorconfirmation = "Problemas cambiando la contrase&ntilde;a";
$_lang_validateforgot_label_errornotmatch = "La contrase&ntilde;a y su confirmaci&oacute;n no coinciden";
$_lang_validateforgot_label_passwordchanged  = "Contrase&ntilde;a cambiada, por favor inicie sesi&oacute;n con la nueva contrase&ntilde;a";
$_lang_validateforgot_button_submit = "Cambiar contrase&ntilde;a";