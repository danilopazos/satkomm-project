<?php
//--- view/index.php
$_lang_index_head_title = 'Telco Admin - Login';
$_lang_index_head_description= 'Telco Admin Dashboard';
$_lang_index_head_author = 'Telco Admin';


$_lang_index_label_title = 'Sign in to your account';
$_lang_index_label_description = 'Please enter your email and password to log in.';
$_lang_index_label_username = 'Email';
$_lang_index_label_password = 'Password';
$_lang_index_label_forgot= 'I forgot my password';
$_lang_index_label_remember = 'Keep me signed in';
$_lang_index_button_submit= 'Login';
$_lang_index_label_copyright = 'Telco Admin';

$_lang_index_label_formerror = ' You have some form errors. Please check below.';
$_lang_index_label_errorauth = 'Invalid email or password';


//--- view/forgot.php
$_lang_forgot_head_title = 'Telco Admin - Forgot Password';
$_lang_forgot_head_description= 'Telco Admin Dashboard';
$_lang_forgot_head_author = 'Telco Admin';

$_lang_forgot_label_title = 'Reset my password';
$_lang_forgot_label_description = 'Please enter your email to reset your password.';
$_lang_forgot_label_username = 'Email';
$_lang_forgot_button_submit= 'Reset my password';
$_lang_forgot_label_formerror = ' You have some form errors. Please check below.';
$_lang_forgot_label_errorforgot = 'Your email is not registered';
$_lang_forgot_label_recoverylinksend = 'Your recovery link was sent to your email account';

//--- view/validateforgot.php
$_lang_validateforgot_label_title = "Telco Admin - Password Change";
$_lang_validateforgot_label_description = "Please introduce the new password";

$_lang_validateforgot_label_tokennotvalid = "The recovery link is not valid";

$_lang_validateforgot_label_password = "Password";
$_lang_validateforgot_label_confirmpassword = "Password confirmation";

$_lang_validateforgot_label_formerror = "You have some form errors. Please check below.";
$_lang_validateforgot_label_errorconfirmation = "Problems changing the password";
$_lang_validateforgot_label_errornotmatch = "The password and the confirmation do not match";
$_lang_validateforgot_label_passwordchanged  = "Password changed, please sign in with the new password";
$_lang_validateforgot_button_submit = "Change password";