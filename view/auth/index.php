<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $_lang_head_title; ?></title>
    <link rel="shortcut icon" href="/assets/images/favicon.png" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="<?php echo $_lang_head_description; ?>" name="description" />
    <meta content="<?php echo $_lang_head_author; ?>" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host;  ?>/assets/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host;  ?>/assets/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host;  ?>/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host;  ?>/assets/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host;  ?>/assets/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host;  ?>/assets/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host;  ?>/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host;  ?>/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo $this->http_host;  ?>/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo $this->http_host;  ?>/assets/css/theme/light.min.css" />
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

</head>

<body class="login example1">

    <div class="main-login col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div class="logo">
            <!--CLIP<i class="clip-clip"></i>ONE -->
            <img src="/media/getlogo/accountid/<?php echo $this->global_accountid; ?>" height="70em">
        </div>
        <!-- start: LOGIN BOX -->
        <div class="box-login">
            <h3><?php echo $_lang_index_label_title; ?></h3>
            <p>
                <?php echo $_lang_index_label_description; ?>
            </p>
            <form class="form-login" action="#" id="form">
                <div class="errorHandler alert alert-danger no-display">
                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_index_label_formerror; ?>
                </div>
                
                <div class="alert alert-danger no-display" id="messageErrorAuth">
                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_index_label_errorauth; ?>
                </div>
                <fieldset>
                    <div class="form-group">
                        <span class="input-icon">
                            <input type="text" class="form-control" name="username" id="username" placeholder="<?php echo $_lang_index_label_username; ?>" autocomplete="off">
                            <i class="fa fa-envelope"></i>
                        </span>
                        <!-- To mark the incorrectly filled input, you must add the class "error" to the input -->
                        <!-- example: <input type="text" class="login error" name="login" value="Username" /> -->
                    </div>
                    <div class="form-group form-actions">
                        <span class="input-icon">
                            <input type="password" class="form-control password" name="password" placeholder="<?php echo $_lang_index_label_password; ?>">
                            <i class="fa fa-lock"></i>
                            <a class="forgot" href="/auth/forgot">
                                <?php echo $_lang_index_label_forgot; ?>
                            </a>
                        </span>
                    </div>
                    
                    <input type="hidden" name="timezonecustomer" id="timezonecustomer" /> 
                    
                    <div class="form-actions">
                        <label for="remember" class="checkbox-inline">
                            <input type="checkbox" class="grey remember" id="remember" name="remember">
                            <?php echo $_lang_index_label_remember; ?>
                        </label>
                        <button type="submit" class="btn btn-bricky pull-right">
                            <?php echo $_lang_index_button_submit; ?> <i class="fa fa-arrow-circle-right"></i>
                        </button>
                    </div>
                 
                </fieldset>
            </form>
        </div>
        <!-- end: LOGIN BOX -->
       
 
        <!-- start: COPYRIGHT -->
        <div class="copyright">
            <script>
                document.write(new Date().getFullYear());
            </script> &copy; <?php echo $_lang_index_label_copyright; ?>
        </div>
        <!-- end: COPYRIGHT -->
    </div>
    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
    <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=zV8OugfLXLmfDPyhgA1nVVsLh8FewT1SEtf3yhUeX1Ev1fqWxSTm8Vw3jNxb"></script></span>
    <?php require_once '../lib/view/template/default/spinner.php'; ?>
    <?php require_once '../lib/view/template/default/validate.php'; ?>
    
    
    
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
            <script src="<?php echo $this->http_host;  ?>/assets/bower_components/respond/dest/respond.min.js"></script>
            <script src=".<?php echo $this->http_host;  ?>/assets/bower_components/Flot/excanvas.min.js"></script>
            <script src="<?php echo $this->http_host;  ?>/assets/bower_components/jquery-1.x/dist/jquery.min.js"></script>
            <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->http_host;  ?>/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo $this->http_host;  ?>/assets/js/modules/templates/jquery.validate.js"></script>
    <script src="<?php echo $this->http_host;  ?>/assets/js/modules/auth/index.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <script>
        jQuery(document).ready(function() {
            Main.init();
            Login.init();
            
            /*
            var timezone_offset_minutes = new Date().getTimezoneOffset();
            timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
            $("#timezonecustomer").val(timezone_offset_minutes);
            // Timezone difference in minutes such as 330 or -360 or 0
            console.log(timezone_offset_minutes); 
            */
            var dd = new Date();
            var d = dd.toString();
            //var d= "(Pacific Standard Time (Mexico))";
            //alert(d);
            var startOff= d.indexOf('(') + 1;
          //  alert(startOff);
            var endOff= d.lastIndexOf(')');
            //alert(startOff+ "  : " + endOff);

            var str = d.substring(startOff , endOff);
            $("#timezonecustomer").val(str);
           
            
        });
    </script>

</body>

</html>