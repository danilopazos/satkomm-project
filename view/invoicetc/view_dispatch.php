<script type="text/javascript">
    var _invoiceid = '<?php echo $this->invoicetc->idcrypt; ?>';
    var _activationdate = '<?php echo $_lang_label_activationdate; ?>';
    var _deviceserial = '<?php echo $_lang_label_device_serial; ?>';
</script>



<!-- Modal Set Simcard on Inventory -->
  
<div class="modal fade" id="modalConfirmDispatch" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div  class="alert alert-info  ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_confirmdispatch_message; ?>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 

                            <button type="button" id="btn_finalconfirmation" class="btn btn-green">
                                <i class="fa fa-plane"></i>   <?php echo $_lang_view_label_confirdispatch; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<!-- Modal Set Simcard on Inventory -->
<div class="modal fade" id="modalActivate" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form name="form_activate"   id="form_activate">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo $_lang_modal_soi_title; ?> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <table class="table table-striped table-bordered table-hover table-full-width"  >

                        
                        
                        <tr>
                            <th colspan="2"><?php echo $_lang_label_simcard_serial; ?></th>
                            <td>
                                <select name="simcarditemid" id="simcarditemid"  class="form-control search-select"></select>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2"><?php echo $_lang_label_device_serial; ?></th>
                            <td>
                                <select name="deviceitemid" id="deviceitemid"  class="form-control search-select"></select>
                            </td>
                        </tr>
                        
                        
                        <tr><th colspan="2"><?php echo $_lang_label_activationdate; ?></th>
                            <td>

                                <div class="input-group" id="picker-container">
                                    <input  name="operationdate" id="operationdate" value="<?php echo date("Y-m-d"); ?>"   type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                </div>
                            </td> 
                        </tr>
                        
                        <tr>
                            <th colspan="2"><?php echo $_lang_label_activationtime; ?></th>
                            <td>
                                <div class="input-group input-append bootstrap-timepicker">
                                    <input  name="operationtime" id="operationtime" type="text" class="form-control time-picker">
                                    <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                </div>
                            </td>
                        </tr>

                        
                        

                        <tr><th colspan="2"><?php echo $_lang_label_description; ?></th>
                            <td>
                                <textarea name="operationdesc" id="operationdesc" class="form-control"></textarea>
                            </td> 
                        </tr>                          

                    </table>


                </div>
                <div class="modal-footer">
                    <input  type="hidden" name="servicerowid" id="servicerowid" value="" >
                    <input  type="hidden" name="planid" id="planfromprovider" value="" >
                    <input  type="hidden" name="planforaccount" id="planforaccount" value="" />
                    <input  type="hidden" name="accountassigned" id="accountassigned" value="<?php echo $this->invoicetc->customerid; ?>" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $_lang_label_activate_cancel; ?></button>
                    <button type="button" class="btn btn-primary" id="btnSaveActivation"><?php echo $_lang_label_activate_save; ?></button>
                </div>

            </form>

        </div>
    </div>
</div>

<!-- Modal Set Device on Inventory -->
<div class="modal fade" id="modalDevice" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form name="form_activate"   id="form_activate">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo $_lang_modal_soi_title; ?> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <table class="table table-striped table-bordered table-hover table-full-width"  >

                     
                        <tr>
                            <th colspan="2"><?php echo $_lang_label_device_serial; ?></th>
                            <td>
                                <select name="productitemid" id="productitemid"  class="form-control search-select"></select>
                            </td>
                        </tr>
                         
                        <tr><th colspan="2"><?php echo $_lang_label_description; ?></th>
                            <td>
                                <textarea name="productdesc" id="productdesc" class="form-control"></textarea>
                            </td> 
                        </tr>                          

                    </table>


                </div>
                <div class="modal-footer">
                    
                    <input  type="hidden" name="productrowid" id="productrowid" value="" >
                    <input  type="hidden" name="productid" id="productid" value="" >
                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $_lang_label_activate_cancel; ?></button>
                    <button type="button" class="btn btn-primary" id="btnSaveDevice"><?php echo $_lang_label_device_save; ?></button>
                </div>

            </form>

        </div>
    </div>
</div>



<!-- Tables ------------------>

<?php 
    $_global_totalservices = 0;
    $_global_totalproducts =  0;

?>

<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-default">


            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_servicesdetails; ?>
                <div class="panel-tools">

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>


            <div class="panel-body  form-horizontal">  


                <table id="tableservices" class="table table-full-width table-responsive table-striped">
                    <thead>
                    <th><?php echo $_lang_service_name; ?></th>
                    <th><?php echo $_lang_service_price; ?></th>
                    <th><?php echo $_lang_service_activation; ?></th>
                    
                    <th><?php echo $_lang_table_info; ?></th>
               
                    <th></th>


                    </thead>
                    <tbody>
                        <?php
                        $aRecords = $this->aService;
                        if ($aRecords) {
                            $total = count($aRecords);
                            if ($total > 0) {
                                foreach ($aRecords as $item) {

                                    $_qty = $item->qty;
                                    for ($i = 0; $i < $_qty; $i++) {
                                        $_rowid = date('ymdhis').''.rand().''.rand();
                                        $_tr = '<tr id="'.$_rowid.'">';

                                        $_tr .= '<td>' . $item->name . '</td>';
                                        $_tr .= '<td>' . moneyFormat($item->price) . '</td>';
                                        $_tr .= '<td>' . moneyFormat($item->activationprice) . '</td>';
                                        $_tr .= '<td id="service_info_'.$_rowid.'"></td>';
                                        $_tr .= '<td><a href="#" id="'.$item->idcrypt.'-'.$this->invoicetc->customerid.'-'.$item->parentplan.'-'.$_rowid.'" class="btn btn-xs btn-primary btn_activate"><i class="fa fa-paper-plane"></i> ' . $_lang_btn_activate . '</a></td>';
                                        
                                        $_tr .= '</tr>';
                                        $_global_totalservices ++;
                                        echo $_tr;
                                    }//for
                                }//foreach
                            }//total
                        }//if
                        ?>
                    </tbody>                    
                </table>


            </div>


        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>   

<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-default">


            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_productdetails; ?>
                <div class="panel-tools">

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>


            <div class="panel-body  form-horizontal">  


                <table id="tableproducts" class="table table-full-width table-responsive table-striped">
                    <thead>
                    <th><?php echo $_lang_product_name; ?></th>
                    <th><?php echo $_lang_product_price; ?></th>


                    <?php
                    if ($this->invoicetc->operationtype == "R") {
                        echo '<th>' . $_lang_product_months . '</th>';
                    }
                    ?>
                    <th></th>
                    <th></th>
                    </thead>
                    <tbody>
                        <?php
                        $aRecords = $this->aProduct;
                        $total = count($aRecords);
                        if ($total > 0) {
                            foreach ($aRecords as $item) {

                                $_qty = $item->qty;
                                for ($i = 0; $i < $_qty; $i++) {
                                    $_rowid = date('ymdhis').''.rand().''.rand();
                                    $_tr = '<tr id="'.$_rowid.'">';
                                    
                                    $_tr .= '<td>' . $item->name . '</td>';
                                    $_tr .= '<td>' . moneyFormat($item->price) . '</td>';
                                    if ($this->invoicetc->operationtype == "R") {
                                        $_tr .= '<td>' . $item->months . '</td>';
                                    }
                                    $_tr .= '<td id="product_info_'.$_rowid.'" class="productinfo"></td>';
                                    $_tr .= '<td><a href="#" id="'.$item->idcrypt.'-'.$_rowid.'" class="btn btn-xs btn-primary btn_dispatch"><i class="fa fa-dropbox"></i> ' . $_lang_btn_dispatch . '</a></td>';
                                    
                                    $_tr .= '</tr>';
                                    $_global_totalproducts ++;
                                    echo $_tr;
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>


            </div>


        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>



<div class="row" id="rowDispatchNow" style="display: none;">
    <div class="col-sm-12">
        <a href="#" class="btn btn-bricky" id="btnDispatchNow" ></a>
    </div>
</div>


<script type="text/javascript">
    var _global_totalservices =  <?php echo $_global_totalservices; ?>;
    var _global_totalproducts =  <?php echo $_global_totalproducts; ?>; 
    
    
    
var _lang_label_savedispatch             = '<?php echo $_lang_label_savedispatch; ?>';
var _lang_label_savedispatchandactivate  =  '<?php echo $_lang_label_savedispatchandactivate; ?>';
var _lang_label_saveactivate             =  '<?php echo $_lang_label_saveactivate; ?>';
    
    
</script>

<form name="form_dispatchnow" id="form_dispatchnow">
    <input type="hidden" id="aservices" name="aservices" value="" />  
    <input type="hidden" id="adevices" name="adevices" value="" /> 
</form>