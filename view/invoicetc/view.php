<!-- Modal to send Email -->

<div class="modal fade" id="modalSendEmail" role="dialog"  >
    
    <div class="modal-dialog">
        
        
        <div class="modal-content">
            
            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        
                        <div  class="alert alert-danger no-display " id="modal_email_sent_alerterror">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_email_senterror; ?>
                        </div>
                        <div  class="alert alert-success no-display " id="modal_email_sent_alert">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_email_sent; ?>
                        </div>
                        
                        <form id="form-email" name="form-email">

                        <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"   >

                            <tr>
                                <th><?php echo $_lang_modal_sendemail_subject; ?></th>
                                <td>
                                    <input value="<?php echo $this->e_subject; ?>"  type="text" name="e_subject" id="e_subject" class="form-control" />
                                </td>
                            </tr>

                            <tr>
                                <th><?php echo $_lang_modal_sendemail_to; ?></th>
                                <td>
                                    <input  value="<?php echo $this->e_to; ?>" type="text" name="e_to" id="e_to" class="form-control" /> 
                                    <?php echo $_lang_modal_sendemail_toinfo; ?>
                                </td> 
                            </tr>                          

                            <tr>
                                <th><?php echo $_lang_modal_sendemail_attach; ?></th>
                                <td>
                                    <a target="_blank" href="../../download/id/<?php echo $this->invoicetc->idcrypt; ?>"  class="btn btn-xs btn-bricky"><i class="fa fa-file-pdf-o"></i> &nbsp;  <?php echo $this->e_attach; ?></a>
                                </td> 
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <textarea class="form-control" name="e_description" id="e_description"></textarea>
                                </td> 
                            </tr>
                        </table>
                        <input type="hidden" value="<?php echo $this->e_from; ?>" name="e_from" id="e_from" />
                        <input type="hidden" value="<?php echo $this->e_fromname; ?>" name="e_fromname" id="e_fromname" />

                        <input type="hidden" value="<?php echo $this->invoicetc->idcrypt; ?>" name="documentid" id="documentid" />
                        <input type="hidden" name="e_attachname" id="e_attachname" value="<?php echo $this->e_attach; ?>" />
                        </form>
                    </div>
                </div>

            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?php echo $_lang_view_label_close; ?>
                </button> 

                <button type="button" id="btn_sendemailnow" class="btn btn-primary">
                    <i class="fa fa-envelope-o"></i>   <?php echo $_lang_view_label_sendemail; ?>
                </button>

            </div>
           
        </div>
        
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    
</div>



<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/invoicetc/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_view; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->


        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<form role="form" class="form-register   box-register" id="form"  >

    <input type="hidden" value="<?php echo $this->invoicetc->idcrypt; ?>" name="id" />

    <div class="tabbable">
        <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
            <li class="active">
                <a data-toggle="tab" href="#panel_documentinfo">
                    <?php echo $_lang_register_label_panel_quoteinfo; ?>
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#panel_documentdetail">
                    <?php echo $_lang_register_label_panel_quotedetail; ?>
                </a>
            </li> 
            <li>
                <a data-toggle="tab" href="#panel_dispatch">
                    <?php echo $_lang_register_label_panel_dispatch; ?>
                </a>
            </li>                 


            <li>
                <a   href="/invoicetc/register/id/<?php echo $this->invoicetc->idcrypt; ?>">
                    <?php echo $_lang_register_label_update; ?>
                </a>
            </li> 
            <li>
                <a data-toggle="modal" href="#quotedelete-modal"   >
                    <?php echo $_lang_register_label_delete; ?>
                </a>
            </li> 
        </ul>


        <div class="tab-content">
            <div id="panel_documentinfo" class="tab-pane in active">

                <?php require_once 'view_info.php'; ?>

            </div>
            <div id="panel_documentdetail" class="tab-pane in">

                <?php require_once 'view_detail.php'; ?>

            </div>
            <div id="panel_dispatch" class="tab-pane in">

                <?php
                if ($this->invoicetc->dispatched == 0) {
                    require_once 'view_dispatch.php';
                } else {
                    require_once 'view_showdispatched.php';
                }
                ?>

            </div>



        </div>

    </div> 


</form>
<!-- end: PAGE CONTENT-->


<div class="modal fade" id="quotedelete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div id="messageDeletedOk" class="alert alert-success no-display ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_deletedok ?>
                        </div>

                        <div   class="alert alert-info  ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_confirmdelete_message ?>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 

                            <button type="button" id="btn_confirmDelete" class="btn btn-green">
                                <i class="fa fa-check"></i>   <?php echo $_lang_view_label_confirmdelete; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
