<?php
//-- Catalog Index
$_lang_index_label_module= "Invoices";
$_lang_index_label_action = "Index";
$_lang_index_label_action_view = "Detail";

$_lang_index_table_title = "Invoices list";

$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Name";
$_lang_index_table_th_networkcode = "Network";
$_lang_index_table_th_price = "Price";

$_lang_view_label_close = "Close";
$_lang_view_label_addcart= "Add Cart";

$_lang_view_block_addcart = "Add Cart";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Package";
$_lang_panel_technicalspecs = "Specs";
$_lang_panel_features= "Features";
$_lang_panel_docs = "Docs";

$_lang_view_label_quantity = "Quantity";
$_lang_view_label_price = "Price";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item added";

$_lang_register_block_quoteinfo = "Invoice info";
$_lang_register_block_customerinfo = "Customer info";
$_lang_register_block_termsinfo = "Terms & conditions";        
        
$_lang_createquote = "Create Invoice";
$_lang_index_label_registeraction = "Register";
$_lang_register_label_quoteno= "Invoice #";
$_lang_register_label_status = "Status";
$_lang_register_label_subject = "Subject";
$_lang_register_label_salesman = "Salesman";
$_lang_register_label_quotedate = "Invoice date";
$_lang_register_label_quoteuntil = "Invoice until valid date";
$_lang_register_label_hastaxes = "Taxes included";
$_lang_register_label_taxespercent = "Taxes %";


$_lang_register_label_customer = "Customer";
$_lang_register_label_selectcustomer = "Select the customer account";

$_lang_register_label_contact = "Contact";
$_lang_register_label_select_contact = "Select the contact";

$_lang_register_label_telephone = "Telephone";
$_lang_register_label_mobile = "Mobile";
$_lang_register_label_email = "Email";
$_lang_register_label_salutation = "Salutation";


$_lang_register_label_paymentterms = "Payment Terms";
$_lang_register_label_paymentterms_select = "Select one";
$_lang_register_label_paymentother = "Other payment terms";

$_lang_register_label_carriercompany = "Carrier Company";
$_lang_register_label_carriercompany_select = "Select one";
$_lang_register_label_carriercompany_other = "Other carrier";
 
$_lang_register_label_quoteobs = "Observations";

$_lang_register_block_billaddress = "Billing Address";
$_lang_register_block_shipaddress = "Shipping Address";
$_lang_register_label_country = "Country";
$_lang_register_label_state = "State";
$_lang_register_label_city = "City";
$_lang_register_label_street = "Street";
$_lang_register_label_zipcode  = "Zip Code";
$_lang_register_label_pobox = "P.O. Box";


$_lang_register_label_panel_quoteinfo = "Invoice info";
$_lang_register_label_panel_quotedetail = "Invoice Detail";
$_lang_register_label_panel_dispatch = "Dispatch";
$_lang_register_label_panel_dispatch_title = "Dispatch Items";


$_lang_register_save = "Save";

$_lang_register_block_operationdetails = "Operation Details";
$_lang_register_block_productdetails = "Product Details";
$_lang_register_block_servicesdetails = "Services Details";
$_lang_register_block_accesoriesdetails = "Accessory Details";

$_lang_register_operationtype = "Operation type";

$_lang_register_operationsale = "Sale";
$_lang_register_operationrent = "Rent";

$_lang_product_addproductbtn = "Add product";
$_lang_product_name = "Product";
$_lang_product_price = "Precio";
$_lang_product_qty = "Qty";
$_lang_product_months = "Months";
$_lang_product_subtotal = "Sub-total";


$_lang_product_addservicebtn = "Add service";
$_lang_service_name = "Service";
$_lang_service_price = "Price";
$_lang_service_activation = "Activation";
$_lang_service_qty = "Qty";
$_lang_service_subtotal = "Sub-total";


$_lang_accessory_addaccessorybtn = "Add Accessory";
$_lang_accessory_name = "Product";
$_lang_accessory_price = "Precio";
$_lang_accessory_qty = "Qty";
$_lang_accessory_months = "Months";
$_lang_accessory_subtotal = "Sub-total";


$_lang_operation_subtotal = "Sub-total";
$_lang_operation_discount = "Discount";
$_lang_operation_shippingcost = "Shipping";
$_lang_operation_total = "Total";

$_lang_quotestatus["C"] = "Created";
$_lang_quotestatus["P"] = "Paid";
$_lang_quotestatus["D"] = "Due"; 
 

$_lang_paymentterms["prepaid"] = "Prepaid";
$_lang_paymentterms["15"] = "Credit 15 days";
$_lang_paymentterms["30"] = "Credit 30 days";
$_lang_paymentterms["45"] = "Credit 45 days";
$_lang_paymentterms["60"] = "Credit 60 days";
$_lang_paymentterms["90"] = "Credit 90 days";
$_lang_paymentterms["00"] = "Other";

$_lang_hastaxes[0]  = "Exportation, no taxes included";
$_lang_hastaxes[1]  = "Taxes included";


$_lang_aoperationtype["R"] = "Rent";
$_lang_aoperationtype["S"] = "Sale";

$_lang_register_label_update = "Update";

$_lang_register_label_delete = "Delete";
$_lang_view_label_close = "Close";

$_lang_view_label_confirmdelete = "Delete";
$_lang_view_label_confirmdelete_message = "Are you sure to delete this invoice?";
$_lang_view_label_deletedok = "The invoice was deleted";

$_lang_register_label_quoteorigin = "Quote Origin";
$_lang_register_label_carriertracking = "Tracking ID";
$_lang_register_label_purchaseorder = "Purchase Order #";


$_lang_quote_title = "Invoice";
$_lang_quote_intro = "This invoice is prepared by: ";
$_lang_label_paymentterms = "Payment terms";
$_lang_label_shippingcompany = "Shipping company";

$_lang_view_label_downloadinvoice = "Download invoice";
$_lang_view_label_download = "Download";


$_lang_invoiceperiod_lblperiod = 'Period';
$_lang_invoiceperiod_lblsubscriptionprice = 'Plan';
$_lang_invoiceperiod_lblfixedchargesprice = 'Fixed charges';
$_lang_invoiceperiod_lbladditionalchargesprice = 'Additional charges';
$_lang_invoiceperiod_lblactivationprice= 'Charges per activation';
$_lang_invoiceperiod_lblsuspensionprice = 'Charges per suspension';
$_lang_invoiceperiod_lbldeactivationprice = 'Charges per deactivation';


$_lang_btn_activate = "Activate & Dispatch";
$_lang_btn_dispatch = "Dispatch";

$_lang_label_activationdate = "Activation date";
$_lang_label_activationtime = "Activation time";
$_lang_label_simcard_serial = "Simcard serial";
$_lang_label_device_serial = "Device serial";
$_lang_label_description = "Description";

$_lang_label_activate_save = "Save activation";
$_lang_label_device_save = "Save device";
$_lang_label_activate_cancel = "Cancel";

$_lang_label_savedispatch ="Dispatch items";
$_lang_label_savedispatchandactivate ="Apply services and dispatch items";
$_lang_label_saveactivate ="Activate";

$_lang_view_label_confirdispatch = "Confirmar dispatch";
$_lang_view_label_confirmdispatch_message = "Please confirm the dispacth item, if a service activation is present, this will be executed after to confirm your airtime operation cart";

$_lang_deliveryterms =  "Delivery Terms";
$_lang_deliveryterms_other =  "Other delivery terms";
$_lang_deliveryterms_option["pickupinoffice"] = "Pickup in office";
$_lang_deliveryterms_option["sendingbycurier"] = "Sending by curier";
$_lang_deliveryterms_option["shippingnotincluded"] = "Shipping not included"; 
$_lang_deliveryterms_option["other"] = "Other"; 

$_lang_currency_code = "Currency"; 
$_lang_currency_exchange = "Exchange rate"; 

//----Email labels ----------------------------
$_lang_view_label_sendemail = "Send Email";
$_lang_view_label_email_sent = "Email Sent";
$_lang_modal_sendemail_title = "Email form";

$_lang_modal_sendemail_subject = "Subject";
$_lang_modal_sendemail_to = "To";
$_lang_modal_sendemail_toinfo = "(Commas separated , )";
$_lang_modal_sendemail_attach = "Attach";
$_lang_modal_sendemail_description = "Description"; 

$_lang_view_label_email_sentsuccess = "Email sent sucessfully";
$_lang_view_label_email_senterror   = "Problems sending the email, please sent it later";


$_lang_msg_selectaccount = "Please select the customer account and the contact first";