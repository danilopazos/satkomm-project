    
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_operationdetails; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
                   
                    <table class="table table-responsive table-striped">
                        <tr>
                            <th><?php echo $_lang_register_operationtype; ?></th><td><?php echo  $_lang_aoperationtype[$this->invoicetc->operationtype];   ?></td>
                            <th><?php echo $_lang_operation_subtotal ; ?></th><td><?php   echo moneyFormat(floatval($this->invoicetc->subtotal)); ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_operation_discount; ?></th><td><?php   echo moneyFormat(floatval($this->invoicetc->discount));  ?></td>
                            <th><?php echo $_lang_operation_shippingcost ; ?></th><td><?php   echo moneyFormat(floatval($this->invoicetc->shippingcost)); ?></td>

                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_hastaxes; ?></th><td><?php   echo $_lang_hastaxes[intval($this->invoicetc->hastaxes)];  ?></td>
                            <th><?php echo $_lang_register_label_taxespercent; ?></th><td><?php   echo  floatval($this->invoicetc->taxespercent);  ?></td>

                        </tr>
                        <tr>
                            <th><?php echo $_lang_currency_code; ?></th>
                            <td><?php 
                            echo $this->invoicetc->currency_code;
                            if($this->invoicetc->currency_code != "USD"){
                                echo '  '.$this->invoicetc->currency_exchange; 
                            }
                            
                            ?>
                            </td>
                            
                            <th><?php echo $_lang_operation_total; ?></th><td><?php echo moneyFormat(floatval($this->invoicetc->total)); ?></td>

                        </tr> 
                    </table>
                    
                    
                </div>
                
                
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>    
     


<?php
$totalProducts = count($this->aProduct); 

if(!$this->aProduct){  $totalProducts = 0;}

if($totalProducts > 0){
?>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_productdetails; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
 
                   
                    <table id="tableproducts" class="table table-full-width table-responsive table-striped">
                        <thead>
                            <th><?php echo $_lang_product_name; ?></th>
                            <th><?php echo $_lang_product_price; ?></th>
                            <th><?php echo $_lang_product_qty; ?></th>
                            
                            <?php 
                            if($this->invoicetc->operationtype == "R"){ 
                                 echo '<th>'.$_lang_product_months.'</th>';
                            }
                            ?>
                            
                            <th><?php echo $_lang_product_subtotal; ?></th>
                        </thead>
                        <tbody>
                            <?php 
                            $aRecords= $this->aProduct;
                            $total = count($aRecords); 
                            if(  $total > 0   ){
                                foreach($aRecords as $item){
                                    $_tr  = '<tr>';
                                    $_tr .= '<td>'.$item->name.'</td>';
                                    $_tr .= '<td>'.moneyFormat($item->price).'</td>';
                                    $_tr .= '<td>'.$item->qty.'</td>';
                                    if($this->invoicetc->operationtype == "R"){ 
                                        $_tr .= '<td>'.$item->months.'</td>';
                                    }
                                    $_tr .= '<td>'.moneyFormat($item->subtotal).'</td>';
                                    $_tr .= '</tr>';
                                    echo $_tr;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    
  
                </div>
                
                
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>    

   
<?php

}//fin de lista de productos




$totalServices = count($this->aService); 

if(!$this->aService){  $totalServices = 0;}

if($totalServices > 0){
?>


    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_servicesdetails; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
 
                   
                    <table id="tableservices" class="table table-full-width table-responsive table-striped">
                        <thead>
                            <th><?php echo $_lang_service_name; ?></th>
                            <th><?php echo $_lang_service_price; ?></th>
                            <th><?php echo $_lang_service_activation; ?></th>
                            <th><?php echo $_lang_service_qty; ?></th>
                            <th><?php echo $_lang_product_subtotal; ?></th>
                        </thead>
                        <tbody>
                            <?php 
                            $aRecords= $this->aService;
                            if($aRecords){
                                $total = count($aRecords); 
                                if(  $total > 0   ){
                                    foreach($aRecords as $item){
                                        $_tr  = '<tr>';
                                        $_tr .= '<td>'.$item->name.'</td>';
                                        $_tr .= '<td>'.moneyFormat($item->price).'</td>';
                                        $_tr .= '<td>'.moneyFormat($item->activationprice).'</td>';
                                        $_tr .= '<td>'.$item->qty.'</td>';
                                        $_tr .= '<td>'.moneyFormat($item->subtotal).'</td>';
                                        $_tr .= '</tr>';
                                        echo $_tr;
                                    }
                                }
                            }
                            ?>
                        </tbody>                    
                    </table>
                    
  
                </div>
                
                
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>   
     
  
<?php
}

?>
    <!--
    <div class="row">
        <div class="col-sm-12">
             <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_accesoriesdetails; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
 
                   
                    <table id="tableaccessory" class="table table-full-width table-responsive table-striped">
                        <thead>
                            <th><?php echo $_lang_accessory_name; ?></th>
                            <th><?php echo $_lang_accessory_price; ?></th>
                            <th><?php echo $_lang_accessory_qty; ?></th>
                            <th><?php echo $_lang_accessory_months; ?></th>
                            <th><?php echo $_lang_accessory_subtotal; ?></th>
                            <th align="center"><a id="btnAddAccessory" href="#" class="btn btn-xs btn-primary"><i class="fa fa-plus-circle">&nbsp;<?php echo $_lang_accessory_addaccessorybtn; ?></i></a></th>
                        </thead>
                        <tbody></tbody>
                        <tfoot><tr><td colspan="3" ></td><th><?php echo $_lang_accessory_subtotal; ?></th><td align="center" id="tdAccessorySubtotal">0</td><td></td></tr></tfoot>
                    </table>
                    
  
                </div>
                
                
            </div>
         </div>
    </div>    
    -->