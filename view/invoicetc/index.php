<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/invoicetc/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
                echo '<a class="btn btn-xs btn-primary" href="/invoicetc/register">';
                echo '<i class="fa fa-plus-circle"></i>&nbsp;' . $_lang_createquote;
                echo '</a>';
             ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_table_title; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th width="10%"> <?php echo $_lang_register_label_quoteno;  ?> </th>
                            <th width="10%"> <?php echo $_lang_register_operationtype;  ?> </th>
                            <th width="20%" class="hidden-xs"><?php echo $_lang_register_label_customer; ?></th>
                            <th><?php echo $_lang_register_label_status; ?></th>
                            <th class="hidden-xs"><?php echo $_lang_register_label_subject; ?> </th>
                            <th width="10%"><?php echo $_lang_register_label_salesman; ?> </th>
                            <th><?php echo $_lang_register_label_quotedate; ?> </th>
                            <th><?php echo $_lang_register_label_quoteuntil; ?> </th>
                            <th><?php echo $_lang_operation_total; ?> </th>
                            <th>  </th>
                        
                        </tr>
                    </thead>
                    <tbody>
        
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->
