<script type="text/javascript">
    var _invoiceid = '<?php echo $this->invoicetc->idcrypt; ?>';
    var _activationdate = '<?php echo $_lang_label_activationdate; ?>';
    var _deviceserial = '<?php echo $_lang_label_device_serial; ?>';
</script>



<!-- Modal Set Simcard on Inventory -->
 


<!-- Tables ------------------>

<?php 
    $_global_totalservices = 0;
    $_global_totalproducts =  0;

?>

<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-default">


            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_servicesdetails; ?>
                <div class="panel-tools">

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>


            <div class="panel-body  form-horizontal">  


                <table id="tableservices" class="table table-full-width table-responsive table-striped">
                    <thead>
                    <th><?php echo $_lang_service_name; ?></th>
                 
                    <th><?php echo $_lang_table_info; ?></th>
               
                    <th></th>


                    </thead>
                    <tbody>
                        <?php
                        $aRecords = $this->aService;
                        if ($aRecords) {
                            $total = count($aRecords);
                            if ($total > 0) {
                                foreach ($aRecords as $item) {

                                    $_qty = $item->qty;
                                    for ($i = 0; $i < $_qty; $i++) {
                                        $_rowid = date('ymdhis').''.rand().''.rand();
                                        $_tr = '<tr id="'.$_rowid.'">';

                                        $_tr .= '<td>' . $item->name . '</td>';
                                        $_tr .= '<td>' . moneyFormat($item->price) . '</td>';
                                        $_tr .= '<td>' . moneyFormat($item->activationprice) . '</td>';
                                        $_tr .= '<td id="service_info_'.$_rowid.'"></td>';
                                        $_tr .= '<td><a href="#" id="'.$item->idcrypt.'-'.$item->parentplan.'-'.$_rowid.'" class="btn btn-xs btn-primary btn_activate"><i class="fa fa-paper-plane"></i> ' . $_lang_btn_activate . '</a></td>';
                                        
                                        $_tr .= '</tr>';
                                        $_global_totalservices ++;
                                        echo $_tr;
                                    }//for
                                }//foreach
                            }//total
                        }//if
                        ?>
                    </tbody>                    
                </table>


            </div>


        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>   

<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-default">


            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_productdetails; ?>
                <div class="panel-tools">

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>


            <div class="panel-body  form-horizontal">  


                <table id="tableproducts" class="table table-full-width table-responsive table-striped">
                    <thead>
                    <th><?php echo $_lang_product_name; ?></th>
                    <th><?php echo $_lang_product_price; ?></th>


                    <?php
                    if ($this->invoicetc->operationtype == "R") {
                        echo '<th>' . $_lang_product_months . '</th>';
                    }
                    ?>
                    <th></th>
                    <th></th>
                    </thead>
                    <tbody>
                        <?php
                        $aRecords = $this->aProduct;
                        $total = count($aRecords);
                        if ($total > 0) {
                            foreach ($aRecords as $item) {

                                $_qty = $item->qty;
                                for ($i = 0; $i < $_qty; $i++) {
                                    $_rowid = date('ymdhis').''.rand().''.rand();
                                    $_tr = '<tr id="'.$_rowid.'">';
                                    
                                    $_tr .= '<td>' . $item->name . '</td>';
                                    $_tr .= '<td>' . moneyFormat($item->price) . '</td>';
                                    if ($this->invoicetc->operationtype == "R") {
                                        $_tr .= '<td>' . $item->months . '</td>';
                                    }
                                    $_tr .= '<td id="product_info_'.$_rowid.'" class="productinfo"></td>';
                                    $_tr .= '<td><a href="#" id="'.$item->idcrypt.'-'.$_rowid.'" class="btn btn-xs btn-primary btn_dispatch"><i class="fa fa-dropbox"></i> ' . $_lang_btn_dispatch . '</a></td>';
                                    
                                    $_tr .= '</tr>';
                                    $_global_totalproducts ++;
                                    echo $_tr;
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>


            </div>


        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>



<div class="row" id="rowDispatchNow" style="display: none;">
    <div class="col-sm-12">
        <a href="#" class="btn btn-bricky" id="btnDispatchNow" ></a>
    </div>
</div>


<script type="text/javascript">
    var _global_totalservices =  <?php echo $_global_totalservices; ?>;
    var _global_totalproducts =  <?php echo $_global_totalproducts; ?>; 
    
    
    
var _lang_label_savedispatch             = '<?php echo $_lang_label_savedispatch; ?>';
var _lang_label_savedispatchandactivate  =  '<?php echo $_lang_label_savedispatchandactivate; ?>';
var _lang_label_saveactivate             =  '<?php echo $_lang_label_saveactivate; ?>';
    
    
</script>

<form name="form_dispatchnow" id="form_dispatchnow">
    <input type="hidden" id="aservices" name="aservices" value="" />  
    <input type="hidden" id="adevices" name="adevices" value="" /> 
</form>