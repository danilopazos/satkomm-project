<script type="text/javascript">
    _select_contact = '<?php echo $_lang_register_label_select_contact ; ?>';
    _quotetaxespercent = <?php echo doubleval( $this->invoicetc->taxespercent ); ?>;
</script>

    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_quoteinfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
  
                    <table class="table table-responsive table-striped">
                        <tr>
                            <th><?php echo $_lang_register_label_quoteno; ?></th><td><?php echo $this->invoicetc->invoiceno; ?></td>  
                            <th><?php echo $_lang_register_label_status; ?></th><td><?php echo $_lang_quotestatus[$this->invoicetc->status]; ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_subject; ?></th><td><?php echo $this->invoicetc->subject; ?></td> 
                            <th><?php echo $_lang_register_label_salesman; ?></th><td><?php echo $this->invoicetc->assigned_names; ?></td></tr>
                        <tr>
                            <th><?php echo $_lang_register_label_quotedate; ?></th><td><?php echo $this->invoicetc->invoicedate; ?></td>  
                            <th><?php echo $_lang_register_label_quoteuntil; ?></th><td><?php echo $this->invoicetc->invoiceduedate; ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_quoteorigin; ?></th>
                            <td>
                                <?php 
                                if($this->invoicetc->quoteidcrypt!= ""){
                                    echo  '<a target="_blank" class="btn btn-xs btn-primary" href="/quote/view/id/'.$this->invoicetc->quoteidcrypt.'">'.$this->invoicetc->quotedesc."</a>"; 
                                }else{
                                    echo "---";
                                }
                                
                                ?>
                            </td>  
                            
                   
                            <th> <?php echo $_lang_register_label_purchaseorder; ?></th>
                            <td> 
                                <?php 
                                if($this->invoicetc->purchaseorderno != ""){
                                    echo  $this->invoicetc->purchaseorderno; 
                                }else{
                                    echo "---";
                                }
                                
                                ?>
                            </td>
                        </tr>
                        
                    <tr>
                        <th><?php echo $_lang_view_label_downloadinvoice; ?></th>
                        <td> 
                            <a target="_blank" href="../../download/id/<?php echo $this->invoicetc->idcrypt; ?>" class="btn btn-xs btn-red"><i class="fa fa-file-pdf-o"></i>&nbsp;<?php echo $_lang_view_label_download; ?></a>
                        </td> 
                        <th> </th>
                        <td>  

                            <a href="#" id="emailModal" class="btn btn-xs btn-primary"><i class="fa fa-envelope-o"></i> &nbsp; <?php echo $_lang_view_label_sendemail; ?></a>
                        </td> 
                    </tr>

                        
                    </table>
                    
                    
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_customerinfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
 
                    <table class="table table-responsive table-striped">
                        <tr>
                            <th><?php echo $_lang_register_label_customer; ?></th><td><?php echo $this->invoicetc->customer_name; ?></td>
                            <th><?php echo $_lang_register_label_contact; ?></th><td><?php echo $this->invoicetc->contact_names; ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_telephone; ?></th><td><?php if($this->invoicetc->contacttelephone != ""){ echo $this->invoicetc->contacttelephone; }else{ echo "---"; } ?></td>
                            <th><?php echo $_lang_register_label_mobile; ?></th><td><?php if($this->invoicetc->contactmobile != ""){ echo $this->invoicetc->contactmobile; }else{ echo "---"; } ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_email; ?></th><td><?php if($this->invoicetc->contactemail != ""){ echo $this->invoicetc->contactemail; }else{ echo "---";}   ?></td>
                            <th><?php echo $_lang_register_label_salutation; ?></th><td><?php if($this->invoicetc->contactsalutation != ""){ echo $this->invoicetc->contactsalutation; }else{ echo "---";  } ?></td>
                        </tr>
                    </table>
  
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>    
    
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_termsinfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  

                    <table class="table table-responsive table-striped">
                        <tr>
                            <th><?php echo $_lang_register_label_paymentterms; ?></th><td><?php if($this->invoicetc->paymentterms != ""){  echo $_lang_paymentterms[$this->invoicetc->paymentterms]; }else{ echo "---"; } ?></td>
                            <th><?php echo $_lang_register_label_paymentother; ?></th><td><?php if($this->invoicetc->paymenttermsother != ""){  echo $this->invoicetc->paymenttermsother; }else{ echo "---";  } ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_carriercompany; ?></th><td><?php   if($this->invoicetc->carriercompany != ""){ echo $this->invoicetc->carriercompany; }else{ echo "---"; } ?></td>
                            <th><?php echo $_lang_register_label_carriercompany_other; ?></th><td><?php if($this->invoicetc->carriercompanyother != ""){ echo $this->invoicetc->carriercompanyother; }else{ echo "---"; } ?></td>
                        </tr>
                        
                    <tr>
                        <th><?php echo $_lang_deliveryterms; ?></th><td><?php
                            if ($this->invoicetc->deliveryterms != "") {
                                echo $_lang_deliveryterms_option[$this->invoicetc->deliveryterms];
                            } else {
                                echo "---";
                            }
                            ?></td>
                        <th><?php echo $_lang_deliveryterms_other; ?></th><td><?php
                            if ($this->invoicetc->deliverytermsother != "") {
                                echo $this->invoicetc->deliverytermsother;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    
                        
                        
                        
                        <tr>
                            <th><?php echo $_lang_register_label_carriertracking; ?></th>
                            <td>
                                <?php 
                                if($this->invoicetc->carriertracking != ""){
                                    echo $this->invoicetc->carriertracking;
                                }else{
                                    echo "---"; 
                                }
                                 ?>
                            </td>  
                            <th> </th><td> </td>
                        </tr>
                        <tr>
                            <th colspan="4"><?php echo $_lang_register_label_quoteobs; ?></th>
                        </tr>
                        <tr>
                            <td colspan="4"><?php if($this->invoicetc->obs != ""){ echo $this->invoicetc->obs; }else{ echo "---"; } ?></td>
                        </tr>
                    </table>
  
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>    
    
    <div class="row">

        <div class="col-sm-6">
            <!-- start: TEXT AREA PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_billaddress; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>
                <div class="panel-body  form-horizontal" >
 
                    <table class="table table-responsive table-striped">
                        <tr>
                            <th><?php echo $_lang_register_label_country; ?></th><td><?php if($this->invoicetc->bill_country != ""){  echo $this->invoicetc->bill_country; } else{ echo "---"; } ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_state; ?></th><td><?php if($this->invoicetc->bill_state != ""){  echo $this->invoicetc->bill_state; } else{ echo "---"; }  ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_city; ?></th><td><?php if($this->invoicetc->bill_city != ""){  echo $this->invoicetc->bill_city; } else{ echo "---"; }  ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_street; ?></th><td><?php if($this->invoicetc->bill_street != ""){  echo $this->invoicetc->bill_street; } else{ echo "---"; }  ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_zipcode; ?></th><td><?php if($this->invoicetc->bill_zipcode != ""){  echo $this->invoicetc->bill_zipcode; } else{ echo "---"; }  ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_pobox; ?></th><td><?php if($this->invoicetc->bill_pobox != ""){  echo $this->invoicetc->bill_pobox; } else{ echo "---"; }  ?></td>
                        </tr>  
                    </table>
                    
                </div>
            </div>
            <!-- end: TEXT AREA PANEL -->
        </div>
        
        
        <?php 
        if($this->invoicetc->shippingenabled=="on"){
        ?>
        <div class="col-sm-6">
            <!-- start: TEXT AREA PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_shipaddress; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>
                        


                    </div>
                </div>
                <div class="panel-body  form-horizontal" >

                    <table class="table table-responsive table-striped">
                        <tr>
                            <th><?php echo $_lang_register_label_country; ?></th><td><?php if($this->invoicetc->ship_country != ""){  echo $this->invoicetc->ship_country; } else{ echo "---"; } ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_state; ?></th><td><?php if($this->invoicetc->ship_state != ""){  echo $this->invoicetc->ship_state; } else{ echo "---"; }  ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_city; ?></th><td><?php if($this->invoicetc->ship_city != ""){  echo $this->invoicetc->ship_city; } else{ echo "---"; }  ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_street; ?></th><td><?php if($this->invoicetc->ship_street != ""){  echo $this->invoicetc->ship_street; } else{ echo "---"; }  ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_zipcode; ?></th><td><?php if($this->invoicetc->ship_zipcode != ""){  echo $this->invoicetc->ship_zipcode; } else{ echo "---"; }  ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_pobox; ?></th><td><?php if($this->invoicetc->ship_pobox != ""){  echo $this->invoicetc->ship_pobox; } else{ echo "---"; }  ?></td>
                        </tr>  
                    </table>

                </div>
            </div>
            <!-- end: TEXT AREA PANEL -->
        </div>

        <?php
        }
        ?>

    </div>