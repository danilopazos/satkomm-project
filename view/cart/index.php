<!--- Product Add Cart Modal Window -->
<div class="modal fade" id="confirmorder-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->
 
                        <div id="messageProcessOk" class="alert alert-info  ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_confirmorder_message; ?>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 

                            <button type="button" id="btn_confirmorder" class="btn btn-green">
                                <i class="fa fa-check"></i>   <?php echo $_lang_view_label_confirmorder; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            if ($this->session_accountid != $this->id) {
                echo '<a class="btn btn-xs btn-default" href="/account/index">';
                echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->

<form role="form" class=" form-placeorder  box-user-register" id="form-placeorder"  >

    <input type="hidden" value="<?php echo count($this->aCart); ?>"  name="totalitem"  id="totalitem"   />
    <input type="hidden" value="<?php echo $this->accountid; ?>"  name="accountid"    />

    <!-- start: PAGE CONTENT -->
    <div class="row">
        <div class="col-sm-12">


            <!-- start: DYNAMIC TABLE PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_index_table_title; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                        <thead>
                            <tr>
                                <th style="text-align: center;"> </th>
                                <th> </th>
                                <th ><?php echo $_lang_index_table_th_product; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_label_quantity; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_label_subtotal; ?> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = count($this->aCart);
                            if ($total > 0) {
                                $totalsum = 0;
                                $index = 1;
                                foreach ($this->aCart as $cart) {

                                    $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $cart->mediaid . '/type/thumb"  >';

                                    $tr = '<tr id="tr_' . $cart->itemid . '"    >';

                                    $tr .='<td align="center">  <a href="#"  id="delete_' . $cart->itemid . '"  class="btn   btn-bricky  user-viewmodal  deleteitem  "  ><i class="fa fa-trash-o"></i></a>    </td>';
                                    $tr .='<td>' . $image_product . '</td>';

                                    $tr .='<td>';
                                    $tr .= '<strong>' . $cart->name . '</strong><br />';
                                    $tr .='SKU: ' . $cart->sku . '<br />';
                                    $tr .=$_lang_view_label_price . ': ' . moneyFormat($cart->buyprice) . '<br />';
                                    $tr .='</td>';

                                    //$tr .='<td align="center">'.$cart->qty.'</td>';
                                    $tr .='<td  align="center">';
                                    $tr .='<input type="button"  value="-" class="    qtyminus" field="quantity"      id="qtyminus_' . $cart->itemid . '"  />';
                                    $tr .='<input type="text" name="quantity"   id="qty_' . $cart->itemid . '" value="' . $cart->qty . '" class="qty"  style="width: 4em; text-align: center;" />';
                                    $tr .='<input type="button"   value="+"   class="   qtyplus"  field="quantity"     id="qtyplus_' . $cart->itemid . '"  />';
                                    $tr .='</td>';



                                    $tr .='<td align="center">';
                                    $tr .= '<span id="st_' . $cart->itemid . '">' . moneyFormat($cart->subtotal) . '</span>';

                                    $tr .='<input type="hidden"   name="' . $index . '_operation"   value="buy"  />';
                                    $tr .='<input type="hidden"   name="' . $index . '_productname"   value="' . $cart->name . '"  />';
                                    $tr .='<input type="hidden"   name="' . $index . '_itemid"   value="' . $cart->itemid . '"  />';
                                    $tr .='<input type="hidden"   name="' . $index . '_sku"   value="' . $cart->sku . '"  />';
                                    $tr .='<input type="hidden" id="deleted_' . $cart->itemid . '"  name="' . $index . '_deleted"   value="0"  />';
                                    $tr .='<input type="hidden" id="productid_' . $cart->itemid . '"  name="' . $index . '_productid"   value="' . $cart->productid . '"  />';
                                    $tr .='<input type="hidden" id="price_' . $cart->itemid . '"  name="' . $index . '_price"    value="' . $cart->buyprice . '"    />';
                                    $tr .='<input type="hidden" id="qty2_' . $cart->itemid . '"     name="' . $index . '_qty"   value="' . $cart->qty . '"    />';
                                    $tr .='<input type="hidden"   class="subtotal_item"   id="subtotal_' . $cart->itemid . '"   name="' . $index . '_subtotal"    value="' . $cart->subtotal . '"    />';

                                    $tr .='</td>';

                                    $tr .= '</tr>';

                                    echo $tr;

                                    $totalsum = $totalsum + $cart->subtotal;
                                    $index ++;
                                }

                                $tr = '<tr>';
                                $tr .= '<td colspan="4" align="right"><strong>' . $_lang_view_label_total . '</strong></td>';
                                $tr .= '<td  align="center"> <span id="totalsum"> ' . moneyFormat($totalsum) . '    </span></td>';
                                $tr .= '</tr>';
                                echo $tr;
                            }
                            ?>
                            
                            <tr>
                                <td colspan="2" align="right"><?php echo $_lang_view_label_overnight_dispatch; ?></td>
                                <td colspan="3">
                                    
                                    <select name="dispatchnow" class="form-control">
                                        <option value="0"><?php echo $_lang_view_label_overnight_dispatch_no; ?></option>
                                        <option value="1"><?php echo $_lang_view_label_overnight_dispatch_yes; ?></option>

                                    </select>
                                    
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end: DYNAMIC TABLE PANEL -->

        </div>
    </div>
    <!-- end: PAGE CONTENT-->


    <div class="row">
        <div class="col-sm-12">


            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
            </div>
            <div id="messageProcessOk" class="alert alert-success no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
            </div>




            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_ordercomments; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <textarea class="form-control" name="ordercomments" ></textarea>  

                </div>
            </div>



        </div>

    </div>

    <div class="row">

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_billaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td><input  class="form-control"  type="text"  name="bill_country"  id="bill_country"  value="<?php echo $this->account->billaddress->country; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><input  class="form-control"  type="text"  name="bill_state" id="bill_state"  value="<?php echo $this->account->billaddress->state; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><input  class="form-control"  type="text"  name="bill_city"  id="bill_city" value="<?php echo $this->account->billaddress->city; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><input  class="form-control"  type="text"  name="bill_street" id="bill_street"  value="<?php echo $this->account->billaddress->street; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><input  class="form-control"  type="text"  name="bill_zipcode"  id="bill_zipcode"  value="<?php echo $this->account->billaddress->zipcode; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_pobox; ?></td>
                                <td><input  class="form-control"  type="text"  name="bill_pobox"   id="bill_pobox"  value="<?php echo $this->account->billaddress->pobox; ?>" /></td> 
                            </tr>


                        </tbody>
                    </table>

                </div>
            </div>



        </div>

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_shipaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">


                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td><input  class="form-control"  type="text"  name="ship_country"  id="ship_country"  value="<?php echo $this->account->shipaddress->country; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><input  class="form-control"  type="text"  name="ship_state"  id="ship_state"    value="<?php echo $this->account->shipaddress->state; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><input  class="form-control"  type="text"  name="ship_city"  id="ship_city"   value="<?php echo $this->account->shipaddress->city; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><input  class="form-control"  type="text"  name="ship_street"  id="ship_street"    value="<?php echo $this->account->shipaddress->street; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><input  class="form-control"  type="text"  name="ship_zipcode"    id="ship_zipcode"   value="<?php echo $this->account->shipaddress->zipcode; ?>" /></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_pobox; ?></td>
                                <td><input  class="form-control"  type="text"  name="ship_pobox"    id="ship_pobox"   value="<?php echo $this->account->shipaddress->pobox; ?>" /></td> 
                            </tr>


                        </tbody>
                    </table>

                </div> </div>


        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">


            <div class="panel panel-default">
                <div class="panel-footer">

                    <button type="submit" class="btn btn-primary ">
                        <i class="fa fa-cart-plus"></i>&nbsp;<?php echo $_lang_index_label_action; ?> 
                    </button>

                </div>
            </div>


        </div>
    </div>

</form>



