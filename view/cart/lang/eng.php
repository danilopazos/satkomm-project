<?php
//-- Catalog Index
$_lang_index_label_module= "My Cart";
$_lang_index_label_action = "Place Order";

$_lang_index_cartemptytitle = "Your cart is empty";
$_lang_index_cartemptytitle_desc = "Please select items in the catalog";

$_lang_index_table_title = "My Cart";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Name";
$_lang_index_table_th_networkcode = "Network";
$_lang_index_table_th_price = "Price";
$_lang_index_table_th_product = "Product";

$_lang_view_label_close = "Close";
$_lang_view_label_addcart= "Add Cart";

$_lang_register_block_ordercomments  = "Order Comments";
$_lang_view_block_addcart = "Add Cart";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Package";
$_lang_panel_technicalspecs = "Specs";
$_lang_panel_features= "Features";
$_lang_panel_docs = "Docs";

$_lang_view_label_quantity = "Quantity";
$_lang_view_label_price = "Price";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item added";
$_lang_view_label_total = "Total";


$_lang_register_block_billaddress = "Billing Address";
$_lang_register_block_shipaddress = "Shipping Address";
$_lang_register_label_country = "Country";
$_lang_register_label_state = "State";
$_lang_register_label_city = "City";
$_lang_register_label_street = "Street";
$_lang_register_label_zipcode  = "Zip Code";
$_lang_register_label_pobox = "P.O. Box";

$_lang_register_label_processok = "Order Placed";
$_lang_register_label_formerror = "Problems, please check the empty fields";
$_lang_view_label_confirmorder_message = "Please confirm to place your order, when the order is confirmed, you accept the terms and conditions detailed in the agreement, thanks";
$_lang_view_label_confirmorder = "Confirm Order";

$_lang_view_label_overnight_dispatch = "Dispatch Overnight";
$_lang_view_label_overnight_dispatch_no = "No";
$_lang_view_label_overnight_dispatch_yes = "Yes, I understand the terms and conditions";

