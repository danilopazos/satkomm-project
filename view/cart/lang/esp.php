<?php
//-- Catalog Index
$_lang_index_label_module= "Mi carrito";
$_lang_index_label_action = "Enviar Orden";

$_lang_index_cartemptytitle = "Su carrito esta vacio";
$_lang_index_cartemptytitle_desc = "Por favor seleccione los art&iacute;culos del cat&aacute;logo";

$_lang_index_table_title = "Mi Carrito";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Nombre";
$_lang_index_table_th_networkcode = "Red";
$_lang_index_table_th_price = "Precio";
$_lang_index_table_th_product = "Producto";

$_lang_view_label_close = "Cerrar";
$_lang_view_label_addcart= "A&ntilde;adir al carrito";

$_lang_register_block_ordercomments  = "Comentarios";
$_lang_view_block_addcart = "A&ntilde;adir al carrito";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Paquete";
$_lang_panel_technicalspecs = "Especs.";
$_lang_panel_features= "Caracter&iacute;sticas";
$_lang_panel_docs = "Documentos";

$_lang_view_label_quantity = "Cantidad";
$_lang_view_label_price = "Precio";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Art&iacute;culo a&ntilde;adido";
$_lang_view_label_total = "Total";


$_lang_register_block_billaddress = "Direcci&oacute;n de facturaci&oacute;n";
$_lang_register_block_shipaddress = "Direcci&oacute;n de env&iacute;o";
$_lang_register_label_country = "Pais";
$_lang_register_label_state = "Estado/Regi&oacute;n";
$_lang_register_label_city = "Ciudad";
$_lang_register_label_street = "Calle";
$_lang_register_label_zipcode  = "C&oacute;digo postal";
$_lang_register_label_pobox = "P.O. Box";

$_lang_register_label_processok = "Ordem Enviada";
$_lang_register_label_formerror = "Problemas, por favor revise los campos vacios";
$_lang_view_label_confirmorder_message = "Confirme para enviar su orden, al confirmar la orden usted acepta los t&eacute;rminos y condiciones estipulados en el contrato de distribuci&oacute;n, gracias";
$_lang_view_label_confirmorder = "Confirmar Orden";

$_lang_view_label_overnight_dispatch = "Despacho Inmediato";
$_lang_view_label_overnight_dispatch_no = "No";
$_lang_view_label_overnight_dispatch_yes = "Si, entiendo los t&eacute;rminos y condiciones";