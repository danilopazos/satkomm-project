<script type="text/javascript">
    _var_accountid = <?php echo $this->accountid; ?>
</script>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dropbox"></i>
                <a href="/airtimeoperation/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_mysimcards; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $this->entity->iserial; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                    <tbody>


                        <tr>
                            <td  rowspan="2" class="header_product_image" width="10%">  
                            
                          <img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/<?php echo  $this->entity->productid ; ?>/type/thumb"  > 

                            
                            </td>
                            <th ><?php echo $_lang_index_label_iserial; ?></th>
                            <td class="device_iserial"><?php echo $this->entity->iserial;?></td>
                        </tr>
                         
                        <tr> 
                            <th ><?php echo $_lang_index_label_productname; ?></th>
                            <td class="device_productname"><?php echo $this->entity->productname;?></td>
                        </tr>

                        <tr> <th  colspan="2"><?php echo $_lang_view_planname; ?></th><td class="modal_planname"><a href="#" class="btn btn-xs btn-default"><?php echo $this->entity->planname;?></a></td></tr>
                        <tr> <th  colspan="2"><?php echo $_lang_view_plantype; ?></th><td class="modal_plantype"><?php echo $this->entity->servicetype_code;?></td></tr>
                        <tr> <th  colspan="2"><?php echo $_lang_view_agreementduration; ?> </th><td class="modal_contractperiod"><?php echo $this->entity->contract_period;?></td></tr>
                        <tr> <th  colspan="2"><?php echo $_lang_view_paymentperiod; ?></th><td class="modal_subscriptionperiod"><?php echo $this->entity->subscription_period;?></td></tr>
                        <tr> <th  colspan="2"><?php echo $_lang_view_paymentprice; ?></th><td class="modal_subscriptionpayment"><?php echo $this->entity->subscription_price;?></td></tr>
                        <tr> <th  colspan="2"><?php echo $_lang_view_activationcharges; ?></th><td class="modal_activationcharges"><?php echo $this->entity->activation_price;?> </td></tr>
                        <tr> <th  colspan="2"><?php echo $_lang_view_fixedprice; ?></th><td class="modal_fixedcharges">  <?php echo $this->entity->fixed_price;?>   </td></tr>
                        
                        
                        
                        <tr>
                            <td colspan="2"><strong>Terms & Conditions to apply</strong></td>
                            <td ><a href="#" class="btn btn-xs btn-default"><?php echo $_lang_view_termsinfo; ?></a></td></tr>

                        <tr><th colspan="2"><?php echo $_lang_view_accountassigned; ?></th>
                            <td>
                                <?php echo $this->entity->accountassigned->name; ?>
                                </select>

                            </td> 
                        </tr>
                        
                          <tr><th colspan="2"><?php echo $_lang_view_paymentmethod; ?></th>
                            <td>
                                <?php echo $this->entity->paymentmethod; ?>
                                </select>

                            </td> 
                        </tr>  
                        
                        
                        <tr><th colspan="2"><?php echo $_lang_view_activationdate; ?></th>
                            <td>
                                <?php echo $this->entity->datetoexecute; ?>
                            </td> 
                        </tr>


                        <tr> <th  colspan="2"><?php echo $_lang_view_subtotal; ?></th><td><?php echo $this->entity->subtotal; ?></td></tr>
                        <tr> <th  colspan="2"><?php echo $_lang_view_taxes; ?></th><td><?php echo doubleval($this->entity->taxes); ?></td></tr>
                        <tr> <th  colspan="2">Credit card operation fee</th><td><?php echo doubleval($this->entity->paymentmethod_price); ?></td></tr>
                        
                        <tr> <th  colspan="2"><?php echo $_lang_view_total; ?></th><td id="modal_totaltopay"><?php echo $this->entity->totalpayment; ?></td></tr>



                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->

<?php
require 'simcard_activate.php';
