<?php
$_lang_index_label_module = "Operaciones de Airtime";
$_lang_index_label_action = "Detalle";
$_lang_index_label_action_myoperations = "Mis Operaciones solicitadas";
$_lang_index_label_action_vieworder = "Detalle de la Orden";
$_lang_index_label_action_myorders = "Mis Ordenes";

$_lang_index_label_goback = "Volver";

$_lang_index_label_action_mycart = "Mi Carro";
 

$_lang_index_label_status = "Status";
$_lang_index_label_total = "Total";
$_lang_index_label_operation = "Operacion";
$_lang_index_label_planname = "Plan";
$_lang_index_label_productname = "Producto";
$_lang_index_label_datetoexecuted = "Fecha de ejecuci&oacute;n";
$_lang_index_label_requesteddatetime = "Fecha de requerimiento";
$_lang_index_label_accountassigned = "Cuenta asignada";
$_lang_index_label_iserial = "Serial";
$_lang_index_label_requestedby = "Ejecutivo ";


$_lang_view_planintro = "Intro";
$_lang_view_plandescription = "Descripci&oacute;n";

$_lang_view_planname = "Nombre del plan";
$_lang_view_plantype = "Tipo de plan";
$_lang_view_paymentperiod = "Periodo de subscripci&oacute;n";
$_lang_view_paymentprice = "Precio de subscripci&oacute;n";
$_lang_view_activationcharges = "Cargos de activaci&oacute;n";
$_lang_view_fixedprice = "Cargos fijos";
$_lang_view_agreementduration = "Duraci&oacute;n del acuerdo";
$_lang_view_trafficdetail = "Detalle del tr&aacute;fico";

$_lang_view_traffic = "Tr&aacute;fico";
$_lang_view_qtyperperiod = "Cant. por periodo";
$_lang_view_additionalprice = "Precio adicional";
$_lang_view_roundedto = "Redondeado a";
$_lang_view_roundedcode = "C&oacute;digo de redondeo";

$_lang_view_activatewiththisplan = "Activar con este plan";

$_lang_view_modaltitle = "Solicitud de operaci&oacute;n";
$_lang_view_operationactivate = "Activar Simcard";
$_lang_view_operationsuspend = "Suspender Simcard";
$_lang_view_operationdeactivate = "Desactivar Simcard";

$_lang_view_closemodal = "Cerrar";
$_lang_view_termsinfo = "T&eacute;rminos y condiciones para activar con este plan";
$_lang_view_acceptandconfirm = "Aceptar los t&eacute;rminos y condiciones para activar";
$_lang_view_activationdate = "Fecha de activaci&oacute;n";
$_lang_view_accountassigned = "Cuenta asignada";


$_lang_view_paymentmethod = "M&eacute;todo de pago";
$_lang_view_subtotal = "Subtotal";
$_lang_view_creditcard = "Cargos-  Tarjeta de cr&eacute;dito";
$_lang_view_wiretransfer = "Cargos -  Wire Transfer";
$_lang_view_cash = "Pago al contado";

$_lang_view_network = "Red";

$_lang_view_taxes = "Impuestos";
$_lang_view_total = "Total a pagar";
$_lang_view_totalitems = "Total de operaciones";

$_lang_index_mycart_plandetails = "Detalles del plan";
$_lang_index_mycart_operationdetails = "Detalles de la operacion";
$_lang_index_mycart_table_title = "Operaci&oacute;n solicitada";
$_lang_index_mycart_placeorder = "Enviar orden";
$_lang_register_block_mycartcomments = "Commentarios";

$_lang_deleteitem = "Eliminar item";
$_lang_ordercheckout = "Detalle del pago";

$_lang_view_label_confirmorder_message = "Por favor confirme la orden para enviarla";
$_lang_view_label_close = "Cerrar";
$_lang_view_label_confirmorder = "Confirm la orden";

$_lang_register_label_formerror = "Problemas procesando la orden, por favor si el problema persiste, cont&aacute;tenos";
$_lang_view_label_formregistered = "Exito!, orden enviada";

$_lang_orderview_orderheader = "Informaci&oacute;n de la orden";

$_lang_status_orderstatus = "Estado de la orden";
$_lang_status_operationshold = "Operaciones pendientes";
$_lang_status_operationsexecuted = "Operaciones ejecutadas";
$_lang_status_ordered = "Pendiente";
$_lang_status_applied = "Aplicada";
$_lang_orderdate = "Fecha de la orden";
$_lang_orderno = "Orden #";
$_lang_invoiceno = "Factura #";
$_lang_customername = "Cuenta";

$_lang_view_block_deactivateblock = "Desactivar";
$_lang_view_block_suspendblock = "Suspender";
$_lang_view_block_activateblock = "Activar";
$_lang_index_label_myorders = "Mi Airtime - &Oacute;rdenes";
$_lang_view_label_voicenumber = "N&uacute;mero de voz";
$_lang_view_label_datanumber = "N&uacute;mero de datos";
$_lang_view_label_faxnumber = "N&uacute;mero de fax";
$_lang_view_label_imsi = "Imsi";
$_lang_view_label_puk1 = "PUK 1";
$_lang_view_label_puk2 = "PUK 2";
$_lang_view_label_pin = "PIN";
$_lang_view_label_expirydate = "Expiraci&oacute;n";


$_lang_activate_label_voicenumber_placeholder = "Introduzca el n&uacute;mero de voz";
$_lang_view_label_activatenow = "Activar ahora";
$_lang_view_label_suspendnow = "Suspender ahora";

$_lang_vieworder_processeddate = "Fecha de procesamiento";

$_lang_vieworder_modal_erroractivate = "Por favor introduzca el n&uacute;mero de voz asignado";


$_lang_index_cartemptytitle = "Su carro esta vacio";
$_lang_index_cartemptytitle_desc = "Por favor necesita a&ntilde;adir solicitud de operaciones de airtime";

$_lang_vieworder_modal_oksuspend = "Operaci&oacute;n ejecutada exitosamente";
$_lang_vieworder_modal_errorsuspend = "Problemas ejecutando la suspensi&oacute;n";

$_lang_view_suspensionprice = "Cargos por suspensi&oacute;n";
$_lang_view_label_deactivatenow= "Desactivar ahora";


$_lang_view_assigned_account = "Cliente";
$_lang_view_applicant_account = "Distribuidor";

 
 
$_lang_view_reload = "Recarga";
