<?php 
$_lang_index_label_module = "Airtime Operations";
$_lang_index_label_action = "Detail";
$_lang_index_label_action_myoperations = "My Operations Requested";
$_lang_index_label_action_vieworder = "OrderDetail";
$_lang_index_label_action_myorders = "My Orders";

$_lang_index_label_goback = "Go Back";

$_lang_index_label_action_mycart = "My Cart";



$_lang_index_label_status = "Status";
$_lang_index_label_total = "Total";
$_lang_index_label_operation = "Operation";
$_lang_index_label_planname = "Plan";
$_lang_index_label_productname = "Product";
$_lang_index_label_datetoexecuted = "Date to execute";
$_lang_index_label_requesteddatetime = "Date Requested";
$_lang_index_label_accountassigned = "Account assigned";
$_lang_index_label_iserial = "Serial";
$_lang_index_label_requestedby = "Executive ";


$_lang_view_planintro = "Intro";
$_lang_view_plandescription = "Description";

$_lang_view_planname = "Plan name";
$_lang_view_plantype = "Plan type";
$_lang_view_paymentperiod = "Subscription period";
$_lang_view_paymentprice = "Subscription price";
$_lang_view_activationcharges = "Activation Charges";
$_lang_view_fixedprice = "Fixed Charges";
$_lang_view_agreementduration = "Duration Agreement";
$_lang_view_trafficdetail = "Traffic Detail";

$_lang_view_traffic = "Traffic";
$_lang_view_qtyperperiod = "Qty per period";
$_lang_view_additionalprice = "Additional price";
$_lang_view_roundedto = "Rounded to";
$_lang_view_roundedcode = "Rounded code";

$_lang_view_activatewiththisplan = "Activate with this plan";

$_lang_view_modaltitle = "Request of operation";
$_lang_view_operationactivate = "Activate Simcard";
$_lang_view_operationsuspend = "Suspend Simcard";
$_lang_view_operationdeactivate = "Deactivate Simcard";

$_lang_view_closemodal = "Close";
$_lang_view_termsinfo = "Terms and Conditions to activate with this plan";
$_lang_view_acceptandconfirm = "Accept the terms and conditions and Confirm the operation";
$_lang_view_activationdate = "Activation date";
$_lang_view_accountassigned = "Account assigned";


$_lang_view_paymentmethod = "Payment method";
$_lang_view_subtotal = "Subtotal";
$_lang_view_creditcard = "Convenience fee -  Credit Card";
$_lang_view_wiretransfer = "Convenience fee -  Wire Transfer";
$_lang_view_cash = "Cash Payment";

$_lang_view_network = "Network";

$_lang_view_taxes = "Taxes";
$_lang_view_total = "Total to pay";
$_lang_view_totalitems = "Total operations";

$_lang_index_mycart_plandetails = "Plan Details";
$_lang_index_mycart_operationdetails = "Operation Details";
$_lang_index_mycart_table_title = "Operations Requested";
$_lang_index_mycart_placeorder = "Place order";
$_lang_register_block_mycartcomments = "Comments";

$_lang_deleteitem = "Delete Item";
$_lang_ordercheckout = "Order Payment Detail";

$_lang_view_label_confirmorder_message = "Please confirm the order, to place it";
$_lang_view_label_close = "Close";
$_lang_view_label_confirmorder = "Confirm the order";

$_lang_register_label_formerror = "Error processing the order, please if the problem persist call to technical support";
$_lang_view_label_formregistered = "Sucess, order place it";

$_lang_orderview_orderheader = "Order Info";

$_lang_status_orderstatus = "Order status";
$_lang_status_operationshold = "Pending Operations";
$_lang_status_operationsexecuted = "Operations Executed";
$_lang_status_ordered = "Pending in order";
$_lang_status_applied = "Applied";
$_lang_orderdate = "Order Date";
$_lang_orderno = "Order #";
$_lang_invoiceno = "Invoice #";
$_lang_customername = "Customer";

$_lang_view_block_deactivateblock = "Deactivate";
$_lang_view_block_suspendblock = "Suspend";
$_lang_view_block_activateblock = "Activate";
$_lang_index_label_myorders = "My Airtime - Orders";
$_lang_view_label_voicenumber = "Voice number";
$_lang_view_label_datanumber = "Data number";
$_lang_view_label_faxnumber = "Fax number";
$_lang_view_label_imsi = "Imsi";
$_lang_view_label_puk1 = "PUK 1";
$_lang_view_label_puk2 = "PUK 2";
$_lang_view_label_pin = "PIN";
$_lang_view_label_expirydate = "Expiry date";


$_lang_activate_label_voicenumber_placeholder = "Introduce the voice number";
$_lang_view_label_activatenow = "Activate now";
$_lang_view_label_suspendnow = "Suspend now";


$_lang_vieworder_processeddate = "Processed Date";

$_lang_vieworder_modal_erroractivate = "Please introduce the phone number assignated";


$_lang_index_cartemptytitle = "Your cart is empty";
$_lang_index_cartemptytitle_desc = "Please you need to add some airtime operations";

$_lang_vieworder_modal_oksuspend = "Operation executed sucessfully";
$_lang_vieworder_modal_errorsuspend = "Problems executing the suspension";

$_lang_view_suspensionprice = "Suspension Charges";
$_lang_view_label_deactivatenow= "Deactivate now";


$_lang_view_assigned_account = "Customer";
$_lang_view_applicant_account = "Distributor";


$_lang_view_reload = "Reload";
