<!--- Confirm Order -->
<div class="modal fade" id="confirmorder-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div id="messageProcessOk" class="alert alert-info  ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_confirmorder_message; ?>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 

                            <button type="button" id="btn_confirmorder" class="btn btn-green">
                                <i class="fa fa-plane"></i>   <?php echo $_lang_view_label_confirmorder; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!---   Activate Window    activate-modal-->
<div class="modal fade" id="activate-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">



            <form role="form" class="form-activate  box-register" id="form-activate"  >

                <div class="modal-body">

                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_vieworder_modal_erroractivate; ?>
                    </div>

                    <div id="messageUsernameProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_vieworder_modal_okactivate; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_view_block_activateblock; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div class="form-group">

                                        <label class="col-sm-4 control-label" for="iserial">
                                            <?php echo $_lang_index_label_iserial; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="activate_iserial"  disabled="true" class="form-control">
                                        </div> 
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="voicenumber">
                                            <?php echo $_lang_view_label_voicenumber; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="activate_voicenumber" placeholder="<?php echo $_lang_activate_label_voicenumber_placeholder; ?>" name="voicenumber" class="form-control">
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="datanumber">
                                            <?php echo $_lang_view_label_datanumber; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="activate_datanumber" placeholder="<?php echo $_lang_activate_label_datanumber_placeholder; ?>" name="datanumber" class="form-control">
                                        </div> 
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="faxnumber">
                                            <?php echo $_lang_view_label_faxnumber; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="activate_faxnumber" placeholder="<?php echo $_lang_activate_label_faxnumber_placeholder; ?>" name="faxnumber" class="form-control">
                                        </div> 
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="imsi">
                                            <?php echo $_lang_view_label_imsi; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="activate_imsi" placeholder="<?php echo $_lang_activate_label_imsi_placeholder; ?>" name="imsi" class="form-control">
                                        </div> 
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="imsi">
                                            <?php echo $_lang_view_label_expirydate . ' ( yyyy-mm-dd ) '; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="activate_expirydate" placeholder="<?php echo $_lang_activate_label_expirydate_placeholder; ?>" name="expirydate" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                        </div> 
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="puk1">
                                            <?php echo $_lang_view_label_puk1; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="activate_puk1" placeholder="<?php echo $_lang_activate_label_puk1_placeholder; ?>" name="puk1" class="form-control">
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="puk2">
                                            <?php echo $_lang_view_label_puk2; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="activate_puk2" placeholder="<?php echo $_lang_activate_label_puk2_placeholder; ?>" name="puk2" class="form-control">
                                        </div> 
                                    </div>                                                                      
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="pin">
                                            <?php echo $_lang_view_label_pin; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="activate_pin" placeholder="<?php echo $_lang_activate_label_pin_placeholder; ?>" name="pin" class="form-control">
                                        </div> 
                                    </div>                                      



                                </div>

                            </div>

                        </div>
                    </div>


                    <input type="hidden" name="operationid" id="activate_operationid" />
                    <input type="hidden" name="operation" id="activate_operation" value="activate"  />

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_view_label_activatenow; ?>
                    </button>
                </div>

            </form>



        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!---   Suspend Window    suspend-modal-->
<div class="modal fade" id="suspend-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">



            <form role="form" class="form-suspend  box-register" id="form-suspend"  >

                <div class="modal-body">

                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_vieworder_modal_errorsuspend; ?>
                    </div>

                    <div id="messageSuspendProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_vieworder_modal_oksuspend; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_view_block_suspendblock; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div class="form-group">

                                        <label class="col-sm-4 control-label" for="iserial">
                                            <?php echo $_lang_index_label_iserial; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="suspend_iserial"  disabled="true" class="form-control">
                                        </div> 
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>


                    <input type="hidden" name="operationid" id="suspend_operationid" />
                    <input type="hidden" name="operation" id="suspend_operation" value="suspend"  />

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="button" id="btn_suspendnow" class="btn btn-primary">
                        <?php echo $_lang_view_label_suspendnow; ?>
                    </button>
                </div>

            </form>



        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!---   Deactivate Window    suspend-modal-->
<div class="modal fade" id="deactivate-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">



            <form role="form" class="form-deactivate  box-register" id="form-deactivate"  >

                <div class="modal-body">

                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_vieworder_modal_errordeactivate; ?>
                    </div>

                    <div id="messageDeactivateProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_vieworder_modal_okdeactivate; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_view_block_deactivateblock; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div class="form-group">

                                        <label class="col-sm-4 control-label" for="iserial">
                                            <?php echo $_lang_index_label_iserial; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="deactivate_iserial"  disabled="true" class="form-control">
                                        </div> 
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>


                    <input type="hidden" name="operationid" id="deactivate_operationid" />
                    <input type="hidden" name="operation" id="deactivate_operation" value="deactivate"  />

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="button" id="btn_deactivatenow" class="btn btn-primary">
                        <?php echo $_lang_view_label_deactivatenow; ?>
                    </button>
                </div>

            </form>



        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-shopping-cart"></i>
                <a href="/airtimeoperation/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_mycart; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            if ($this->session_accountid != $this->id) {
                echo '<a class="btn btn-xs btn-default" href="/account/index">';
                echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->

<form role="form" class=" form-placeorder " id="form-placeorder"  >

    <input type="hidden" value="<?php echo count($this->aCart); ?>"  name="totalitems"  id="totalitems"   />
    <input type="hidden" name="orderid" id="orderid" value="<?php echo $this->entity->id; ?>" />

    <div class="row">

        <div class="col-sm-8">


            <div class="panel panel-default ">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_orderview_orderheader; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <table class=" table table-hover  "  >

                        <tr>
                            <th>Code</th>
                            <td><?php echo $this->entity->code ?></td>
                            <th>Date</th>
                            <td><?php echo substr($this->entity->orderdate, 0, 10); ?></td>
                        </tr>

                        <tr>
                            <th>Account</th>
                            <td> <?php echo $this->entity->accountcustomer->name; ?>  </td>
                            <th>Requested by</th>
                            <td>  
                                <?php
                                $userinfo = '<span class="input-help">';
                                $contactinfo = '';
                                if ($this->entity->requesteduser->mobile != "") {
                                    $contactinfo = $this->entity->requesteduser->mobile;
                                } else if ($this->entity->requesteduser->phone != "") {
                                    $contactinfo .= $this->entity->requesteduser->phone;
                                    if ($this->entity->requesteduser->phoneext != "") {
                                        $contactinfo .= ' Ext: ' . $this->entity->requesteduser->phoneext;
                                    }
                                } else {
                                    $contactinfo = "---";
                                }
                                $userinfo .= '<span class="  popovers" title="" data-content="' . $contactinfo . '" data-placement="right" data-trigger="hover" data-rel="popover" data-original-title="' . $this->entity->requesteduser->username . '"><a href="#" class="btn btn-xs btn-default">' . $this->entity->requesteduser->firstname . '  ' . $this->entity->requesteduser->lastname . '</a></span >';
                                $userinfo .= '</span>';

                                echo $userinfo;
                                ?>


                            </td>
                        </tr>

                        <tr>
                            <th><?php echo $_lang_status_orderstatus; ?></th>                            
                            <td>

                                <?php
                                if ($this->entity->orderstatus == "HOLD") {
                                    echo '<a href="#" class="btn btn-xs btn-orange"  >' . $_lang_status_operationshold . '</a>';
                                } else if ($this->entity->orderstatus == "EXECUTED") {
                                    echo '<a href="#" class="btn btn-xs btn-green"  >' . $_lang_status_operationsexecuted . '</a>';
                                }
                                ?>

                            </td>

                            <?php
                            if ($this->invoice) {
                                echo '<th>' . $_lang_invoiceno . '</th>';
                                echo '<td><a target="_blank" href="/invoice/view/idcrypt/' . $this->invoice->idcrypt . '" class="btn btn-xs btn-primary" >' . $this->invoice->code . '</a></td>';
                            } else {
                                echo '<td></td>';
                                echo '<td></td>';
                            }
                            ?>

                        </tr>

                    </table>



                </div>

            </div>

        </div>

        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_mycartcomments; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo $this->entity->ordercomments; ?>

                </div>
            </div>
        </div>



    </div>


    <!-- start: PAGE CONTENT -->
    <div class="row">


        <div class="col-sm-8">


            <!-- start: DYNAMIC TABLE PANEL -->
            <div class="panel panel-default ">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_index_mycart_table_title; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">
                    <table class=" table table-hover  " id="mainTable">

                        <tbody>
                            <?php
                            $total = count($this->aCart);

                            if ($total > 0) {
                                $totalsum = 0;
                                $index = 1;
                                foreach ($this->aCart as $cart) {

                                    $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/' . $cart->productid . '/type/thumb"  >';


                                    $_operationName = strtoupper($cart->operation);
                                    if ($cart->servicestatus == 'activate') {
                                        $_operationName .= " [" . $_lang_view_reload . "] ";
                                    }


                                    $tr = '<tr class="rowitem_' . $cart->id . '  rowitem ">';
                                    $tr .= '<th valign="top" class="panel-heading">' . strtoupper($_lang_index_label_operation) . '</th>';
                                    $tr .= '<td valign="top" class="panel-heading" colspan="3" >' . $_operationName . '</td>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .= '<th valign="top" > ' . $_lang_index_label_planname . '</th>';
                                    $tr .= '<td valign="top" colspan="2">' . $cart->planname . '</td>';
                                    $tr .= '<td align="right" rowspan="3"  >' . $image_product . '</td>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . '  rowitem  ">';
                                    $tr .= '<th valign="top" > ' . $_lang_index_label_productname . '</th>';
                                    $tr .= '<td valign="top" colspan="2" >' . $cart->productname . '</td>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .= '<th valign="top" > ' . $_lang_index_label_iserial . '</th>';
                                    $tr .= '<td valign="top" colspan="2" id="lbliserial_' . $cart->id . '">' . $cart->iserial . '</td>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .= '<th colspan="4" >' . strtoupper($_lang_index_mycart_plandetails) . '</th>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .= '<th>' . $_lang_view_network . '</th>';
                                    $tr .= '<td>' . $cart->networkcode . ' </td>';
                                    $tr .= '<th>' . $_lang_view_agreementduration . '</th>';
                                    $tr .= '<td>' . $_lang_period_option[$cart->contract_period] . '</td>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .= '<th>' . $_lang_view_paymentprice . '</th>';
                                    $tr .= '<td>' . moneyFormat($cart->subscription_price) . '</td>';
                                    $tr .= '<th>' . $_lang_view_paymentperiod . '</th>';
                                    $tr .= '<td>' . $_lang_period_option[$cart->subscription_period] . ' </td>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . '  rowitem   ">';
                                    $tr .= '<th>' . $_lang_view_activationcharges . '</th>';
                                    $tr .= '<td>' . moneyFormat($cart->activation_price) . ' </td>';
                                    $tr .= '<th>' . $_lang_view_fixedprice . '</th>';
                                    $tr .= '<td> ' . moneyFormat($cart->fixed_price) . '</td>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . '">';
                                    $tr .= '<th colspan="4" >' . strtoupper($_lang_index_mycart_operationdetails) . '</th>';
                                    $tr .= '</tr>';


                                    $tr .= '<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .= '<th>' . $_lang_index_label_datetoexecuted . '</th>';
                                    $tr .= '<td>' . $cart->datetoexecute . ' ' . trim($cart->timetoexecute) . ' </td>';
                                    $tr .= '<th>' . $_lang_view_applicant_account . '</th>';
                                    $tr .= '<td>' . $cart->account->name . '</td>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .= '<th>' . $_lang_index_label_requesteddatetime . ' </th>';
                                    $tr .= '<td>' . $cart->requesteddatetime . ' </td>';
                                    $tr .= '<th>' . $_lang_view_assigned_account . '</th>';
                                    $tr .= '<td>' . $cart->accountassigned->name . '</td>';
                                    $tr .= '</tr>';

                                    $tr .= '<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .= '<th>' . $_lang_index_label_requestedby . ' </th>';
                                    $tr .= '<td colspan="3">';
                                    $tr .= '<span class="input-help">';
                                    $contactinfo = '';
                                    if ($cart->requestedbyuser->mobile != "") {
                                        $contactinfo = $cart->requestedbyuser->mobile;
                                    } else if ($cart->requestedbyuser->phone != "") {
                                        $contactinfo .= $cart->requestedbyuser->phone;
                                        if ($cart->requestedbyuser->phoneext != "") {
                                            $contactinfo .= ' Ext: ' . $cart->requestedbyuser->phoneext;
                                        }
                                    } else {
                                        $contactinfo = "---";
                                    }
                                    $tr .= '<span class="  popovers" title="" data-content="' . $contactinfo . '" data-placement="right" data-trigger="hover" data-rel="popover" data-original-title="' . $cart->requestedbyuser->username . '"><a href="#" class="btn btn-xs btn-default">' . $cart->requestedbyuser->firstname . '  ' . $cart->requestedbyuser->lastname . '</a></span >';
                                    $tr .= '</span>';

                                    $tr .= '</td>';
                                    $tr .= '</tr>';


                                    if ($cart->operationstatus == 1) {

                                        $tr .= '<tr>';
                                        $tr .= '<th>';
                                        $tr .= $_lang_vieworder_processeddate;
                                        $tr .= '</th>';
                                        $tr .= '<td colspan="3">';
                                        $tr .= $cart->processeddatetime;
                                        $tr .= '</td>';
                                        $tr .= '</tr>';
                                    }



                                    $tr .= '<tr class="rowitem_' . $cart->id . '  rowitem " >';


                                    $tr .= '<td>';

                                    if ($this->processOrder) {
                                        // && $cart->requeststatus != "executed"
                                        if ($cart->operation == "activate" && $cart->requeststatus != "executed") {
                                            $tr .= '<a href="#"  id="btnactivate_' . $cart->id . '" class="btnactivate btn btn-xs btn-green">' . $_lang_view_operationactivate . '</a>';
                                        } else if ($cart->operation == "suspend" && $cart->requeststatus != "executed") {
                                            $tr .= '<a href="#" id="btnsuspend_' . $cart->id . '" class="btnsuspend btn btn-xs btn-orange">' . $_lang_view_operationsuspend . '</a>';
                                        } else if ($cart->operation == "deactivate" && $cart->requeststatus != "executed") {
                                            $tr .= '<a href="#" id="btndeactivate_' . $cart->id . '" class="btndeactivate btn-xs btn-dark-grey">' . $_lang_view_operationdeactivate . '</a>';
                                        }
                                    }

                                    $tr .= '</td >';


                                    $tr .= '<td  colspan="2"  align="right">';

                                    if ($cart->requeststatus == 'ordered') {
                                        $tr .= '<a href="#" class="btn btn-xs btn-orange">' . $_lang_status_ordered . '</a>';
                                    } else if ($cart->requeststatus == 'applied') {
                                        $tr .= '<a href="#" class="btn btn-xs btn-green">' . $_lang_status_applied . '</a>';
                                    }

                                    $tr .= '</td>';
                                    $tr .= '<td  align="left">';
                                    $tr .= '<strong>SUB-TOTAL</strong>&nbsp;' . moneyFormat($cart->subtotal);

                                    $tr .= '<input type="hidden" name="cartid_' . $index . '"  id="cartid_' . $index . '"   value="' . $cart->id . '"  ">';
                                    $tr .= '<input type="hidden" name="aosubtotal_' . $cart->id . '"  class="aosubtotal"   id="aosubtotal_' . $cart->id . '"      value="' . $cart->subtotal . '"  ">';
                                    $tr .= '<input type="hidden" name="aoid_' . $cart->id . '"  id="aoid_' . $cart->id . '"   value="' . $cart->id . '"  ">';
                                    $tr .= '<input type="hidden" name="aodeleted_' . $cart->id . '"  id="aodeleted_' . $cart->id . '"     value="0"  ">';
                                    
                                    
                                    $tr .= '<input type="hidden"   id="actual_voicenumber_' . $cart->id . '"      value="' . $cart->actual_voicenumber . '"  ">';
                                    $tr .= '<input type="hidden"   id="actual_datanumber_' . $cart->id . '"      value="' . $cart->actual_datanumber . '"  ">';
                                    $tr .= '<input type="hidden"   id="actual_faxnumber_' . $cart->id . '"      value="' . $cart->actual_faxnumber . '"  ">';
                                    $tr .= '<input type="hidden"   id="actual_imsi_' . $cart->id . '"      value="' . $cart->actual_imsi . '"  ">';
                                    $tr .= '<input type="hidden"   id="actual_expirydate_' . $cart->id . '"      value="' . $cart->actual_expirydate . '"  ">';
                                    
                                    $tr .= '<input type="hidden"   id="actual_puk1_' . $cart->id . '"      value="' . $cart->actual_puk1 . '"  ">';
                                    $tr .= '<input type="hidden"   id="actual_puk2_' . $cart->id . '"      value="' . $cart->actual_puk2 . '"  ">';
                                    $tr .= '<input type="hidden"   id="actual_pin_' . $cart->id . '"      value="' . $cart->actual_pin . '"  ">';
                                   
                                    
                                    
                                    $tr .= ' </td>';
                                    $tr .= '</tr>';


                                    echo $tr;

                                    $totalsum = $totalsum + $cart->subtotal;
                                    $index ++;
                                }
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end: DYNAMIC TABLE PANEL -->

        </div>

        <!-- end: PAGE CONTENT-->


        <div class="col-sm-4">



            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_ordercheckout; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

<?php
$tr = '<table  class=" table table-hover  " >';
$tr .= '<tr>';
$tr .= '<td  width="30%"  ><strong>' . $_lang_view_subtotal . '</strong></td>';
$tr .= '<td   align="left"> <span id="totalsum"> ' . moneyFormat($totalsum) . '    </span></td>';
$tr .= '</tr>';

$totaltaxes = $totalsum * 0;

$tr .= '<tr>';
$tr .= '<td    width="30%"  ><strong>' . $_lang_view_taxes . '</strong></td>';
$tr .= '<td    align="left"> <span id="lbl_taxes"> ' . moneyFormat($totaltaxes) . '    </span></td>';
$tr .= '</tr>';


$tr .= '<tr>';


$tr .= '<td  width="70%"  >';


$tr .= '<strong>';

if ($this->entity->payment_method == "creditcard") {
    $tr .= $_lang_view_creditcard;
} else if ($this->entity->payment_method == "cash") {
    $tr .= $_lang_view_cash;
} else {
    $tr .= $_lang_view_wiretransfer;
}


$conveniencefee = ($totalsum + $totaltaxes ) * 0.04;

$totalpayment = $totalsum + $totaltaxes + $conveniencefee;

$tr .= '</strong></td>';
$tr .= '<td   align="left">';

$tr .= '<input type="hidden" name="subtotal"   id="subtotal"   value="' . $totalsum . '" />';
$tr .= '<input type="hidden" name="taxes"   id="taxes"    value="' . $totaltaxes . '"  />';
$tr .= '<input type="hidden" class=" " id="convenience_fee" name="convenience_fee" value=" ' . $conveniencefee . '    "  />  ';
$tr .= '<input type="hidden" name="totalpayment"   id="totalpayment"    value="' . $totalpayment . '"  />';

$tr .= '<span id="lbl_convenience_fee"> ' . moneyFormat($conveniencefee) . '    </span>';


$tr .= '</td>';
$tr .= '</tr>';



$tr .= '<tr>';
$tr .= '<td   width="30%"  ><strong>' . $_lang_view_total . '</strong></td>';
$tr .= '<td    align="left"> <span id="lbl_totalpayment"> ' . moneyFormat($totalpayment) . '    </span></td>';
$tr .= '</tr>';
$tr .= '</table>';
echo $tr;
?>


                    <div class="panel panel-default">
                        <div class="panel-footer">


                        </div>
                    </div>



                </div>
            </div>

        </div>



    </div>


</form>