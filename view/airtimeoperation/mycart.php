<!--- Product Add Cart Modal Window -->
<div class="modal fade" id="confirmorder-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div id="messageProcessOk" class="alert alert-info  ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_confirmorder_message; ?>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 

                            <button type="button" id="btn_confirmorder" class="btn btn-green">
                                <i class="fa fa-plane"></i>   <?php echo $_lang_view_label_confirmorder; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-shopping-cart"></i>
                <a href="/airtimeoperation/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_mycart; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            if ($this->session_accountid != $this->id) {
                echo '<a class="btn btn-xs btn-default" href="/account/index">';
                echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->


<div class="errorHandler  alert alert-danger  no-display" id="errorHandler">
    <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
</div>
<div id="messageOk" class="alert alert-success no-display">
    <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_formregistered; ?>
</div>


<form role="form" class=" form-placeorder  box-user-register" id="form-placeorder"  >

    <input type="hidden" value="<?php echo count($this->aCart); ?>"  name="totalitems"  id="totalitems"   />

    <!-- start: PAGE CONTENT -->
    <div class="row">
        <div class="col-sm-8">


            <!-- start: DYNAMIC TABLE PANEL -->
            <div class="panel panel-default ">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_index_mycart_table_title; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">
                    <table class=" table table-hover  " id="mainTable">

                        <tbody>
                            <?php
                            $total = count($this->aCart);

                            if ($total > 0) {
                                $totalsum = 0;
                                $index = 1;
                                foreach ($this->aCart as $cart) {

                                    $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/' . $cart->productid . '/type/thumb"  >';
                                    
                                    
                                    $_operationName = strtoupper($cart->operation);
                                    if($cart->servicestatus == 'activate'){
                                        $_operationName .= " [".$_lang_view_reload."] ";
                                    }
                                    
                                    $tr = '<tr class="rowitem_' . $cart->id . '  rowitem ">';
                                    $tr .='<th valign="top" class="panel-heading">' . strtoupper($_lang_index_label_operation) . '</th>';
                                    $tr .='<td valign="top" class="panel-heading" colspan="3" >' . strtoupper($_operationName) . '</td>';
                                    $tr .='</tr>';

                                    $tr .='<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .='<th valign="top" > ' . $_lang_index_label_planname . '</th>';
                                    $tr .='<td valign="top" colspan="2">' . $cart->planname . '</td>';
                                    $tr .='<td align="right" rowspan="3"  >' . $image_product . '</td>';
                                    $tr .='</tr>';

                                    $tr .='<tr class="rowitem_' . $cart->id . '  rowitem  ">';
                                    $tr .='<th valign="top" > ' . $_lang_index_label_productname . '</th>';
                                    $tr .='<td valign="top" colspan="2" >' . $cart->productname . '</td>';
                                    $tr .='</tr>';

                                    $tr .='<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .='<th valign="top" > ' . $_lang_index_label_iserial . '</th>';
                                    $tr .='<td valign="top" colspan="2">' . $cart->iserial . '</td>';
                                    $tr .='</tr>';

                                    $tr .='<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .='<th colspan="4" >' . strtoupper($_lang_index_mycart_plandetails) . '</th>';
                                    $tr .='</tr>';

                                    $tr .='<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .='<th>' . $_lang_view_network . '</th>';
                                    $tr .='<td>' . $cart->networkcode . ' </td>';
                                    $tr .='<th>' . $_lang_view_agreementduration . '</th>';
                                    $tr .='<td>' . $cart->contract_period . '</td>';
                                    $tr .='</tr>';

                                    $tr .='<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .='<th>' . $_lang_view_paymentprice . '</th>';
                                    $tr .='<td>' . moneyFormat($cart->subscription_price) . '</td>';
                                    $tr .='<th>' . $_lang_view_paymentperiod . '</th>';
                                    $tr .='<td>' . $cart->subscription_period . ' </td>';
                                    $tr .='</tr>';


                                    if ($cart->operation == "activate") {
                                    
                                        $tr .='<tr class="rowitem_' . $cart->id . '  rowitem   ">';
                                        $tr .='<th>' . $_lang_view_activationcharges . '</th>';
                                        $tr .='<td>' . moneyFormat($cart->activation_price) . ' </td>';
                                        $tr .='<th>' . $_lang_view_fixedprice . '</th>';
                                        $tr .='<td> ' . moneyFormat($cart->fixed_price) . '</td>';
                                        $tr .='</tr>';
                                    
                                    } else if ($cart->operation == "suspend") {

                                        $tr .='<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                        $tr .='<th>' . $_lang_view_suspensionprice . '</th>';
                                        $tr .='<td>' . moneyFormat($cart->suspension_price) . '</td>';
                                        $tr .='<th> </th>';
                                        $tr .='<td>  </td>';
                                        $tr .='</tr>';
                                    } else if ($cart->operation == "deactivate") {
                                        
                                    }
 

                                    $tr .='<tr class="rowitem_' . $cart->id . '">';
                                    $tr .='<th colspan="4" >' . strtoupper($_lang_index_mycart_operationdetails) . '</th>';
                                    $tr .='</tr>';


                                    $tr .='<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .='<th>' . $_lang_index_label_datetoexecuted . '</th>';
                                    //$tr .='<td>' . $cart->datetoexecute . ' ' . $cart->timetoexecute . ' </td>';
                                    $tr .='<td>' . $cart->datetoexecute . ' </td>';
                                    $tr .='<th>Applicant Account</th>';
                                    $tr .='<td>' . $cart->account->name . '</td>';
                                    $tr .='</tr>';

                                    $tr .='<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .='<th>' . $_lang_index_label_requesteddatetime . ' </th>';
                                    $tr .='<td>' . $cart->requesteddatetime . ' </td>';
                                    $tr .='<th>Assigned Account</th>';
                                    $tr .='<td>' . $cart->accountassigned->name . '</td>';
                                    $tr .='</tr>';

                                    $tr .='<tr class="rowitem_' . $cart->id . ' rowitem  ">';
                                    $tr .='<th>' . $_lang_index_label_requestedby . ' </th>';
                                    $tr .='<td colspan="3">';
                                    $tr .='<span class="input-help">';
                                    $contactinfo = '';
                                    if ($cart->requestedbyuser->mobile != "") {
                                        $contactinfo = $cart->requestedbyuser->mobile;
                                    } else if ($cart->requestedbyuser->phone != "") {
                                        $contactinfo .= $cart->requestedbyuser->phone;
                                        if ($cart->requestedbyuser->phoneext != "") {
                                            $contactinfo .= ' Ext: ' . $cart->requestedbyuser->phoneext;
                                        }
                                    } else {
                                        $contactinfo = "---";
                                    }
                                    $tr .='<span class="  popovers" title="" data-content="' . $contactinfo . '" data-placement="right" data-trigger="hover" data-rel="popover" data-original-title="' . $cart->requestedbyuser->username . '"><a href="#" class="btn btn-xs btn-default">' . $cart->requestedbyuser->firstname . '  ' . $cart->requestedbyuser->lastname . '</a></span >';
                                    $tr .='</span>';

                                    $tr .='</td>';
                                    $tr .='</tr>';

                                    $tr .='<tr class="rowitem_' . $cart->id . '  rowitem " >';
                                    $tr .='<td  colspan="3"  align="right"><a href="#"  id="btndelete_' . $cart->id . '"    class="btn btn-xs btn-bricky   btndelete  " >' . $_lang_deleteitem . '</a></td>';
                                    $tr .='<td  align="left">';
                                    $tr .='<strong>SUB-TOTAL</strong>&nbsp;' . moneyFormat($cart->subtotal);

                                    $tr .='<input type="hidden" name="inventoryitemid_' . $index . '"  id="inventoryitemid_' . $index . '"   value="' . $cart->inventoryitemid . '"  ">';
                                    $tr .='<input type="hidden" name="cartid_' . $index . '"  id="cartid_' . $index . '"   value="' . $cart->id . '"  ">';
                                    $tr .='<input type="hidden" name="aosubtotal_' . $cart->id . '"  class="aosubtotal"   id="aosubtotal_' . $cart->id . '"      value="' . $cart->subtotal . '"  ">';
                                    $tr .=' </td>';
                                    $tr .='</tr>';


                                    echo $tr;

                                    $totalsum = $totalsum + $cart->subtotal;
                                    $index ++;
                                }
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end: DYNAMIC TABLE PANEL -->

        </div>

        <!-- end: PAGE CONTENT-->


        <div class="col-sm-4">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_mycartcomments; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <textarea class="form-control" name="ordercomments" ></textarea>  

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_ordercheckout; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

<?php
$tr = '<table  class=" table table-hover  " >';
$tr .= '<tr>';
$tr .= '<td  width="30%"  ><strong>' . $_lang_view_subtotal . '</strong></td>';
$tr .= '<td   align="left"> <span id="totalsum"> ' . moneyFormat($totalsum) . '    </span></td>';
$tr .= '</tr>';

$totaltaxes = $totalsum * ( ( doubleval($this->taxes->saletaxes) + doubleval($this->taxes->communicationtaxes)) / 100 );

$tr .= '<tr>';
$tr .= '<td    width="30%"  ><strong>' . $_lang_view_taxes . '</strong></td>';
$tr .= '<td    align="left"> <span id="lbl_taxes"> ' . moneyFormat($totaltaxes) . '    </span></td>';
$tr .= '</tr>';


$tr .= '<tr>';


$tr .= '<td  width="30%"  >';

$tr .= '<select name="payment_method" id="payment_method"  >';
$tr .= '<option value="creditcard">' . $_lang_view_creditcard . '</option>';
$tr .= '<option value="banktransfer">' . $_lang_view_wiretransfer . '</option>';
$tr .= '<option value="cash">' . $_lang_view_cash . '</option>';
$tr .= '</select>';

$conveniencefee = ($totalsum + $totaltaxes ) * (doubleval($this->fees->creditcard) / 100);

$totalpayment = $totalsum + $totaltaxes + $conveniencefee;

$tr .='</strong></td>';
$tr .= '<td   align="left">';

$tr .='<input type="hidden" name="subtotal"   id="subtotal"   value="' . $totalsum . '" />';
$tr .='<input type="hidden" name="taxes"   id="taxes"    value="' . $totaltaxes . '"  />';
$tr .= '<input type="hidden" class=" " id="convenience_fee" name="convenience_fee" value=" ' . $conveniencefee . '    "  />  ';
$tr .='<input type="hidden" name="totalpayment"   id="totalpayment"    value="' . $totalpayment . '"  />';

$tr .='<span id="lbl_convenience_fee"> ' . moneyFormat($conveniencefee) . '    </span>';


$tr .= '</td>';
$tr .= '</tr>';



$tr .= '<tr>';
$tr .= '<td   width="30%"  ><strong>' . $_lang_view_total . '</strong></td>';
$tr .= '<td    align="left"> <span id="lbl_totalpayment"> ' . moneyFormat($totalpayment) . '    </span></td>';
$tr .= '</tr>';
$tr .= '</table>';
echo $tr;
?>


                    <div class="panel panel-default">
                        <div class="panel-footer">

                            <button type="button" id="placeorder" class="btn btn-primary ">
                                <i class="  fa fa-plane"></i>&nbsp;<?php echo $_lang_index_mycart_placeorder; ?> 
                            </button>

                        </div>
                    </div>



                </div>
            </div>

        </div>



    </div>


</form>


<script type="text/javascript">
    var _global_creditcard_fee = <?php echo doubleval($this->fees->creditcard) ?>;
    var _global_wiretransfer_fee = <?php echo doubleval($this->fees->wiretransfer) ?>;
    var _global_saletaxes_fee = <?php echo doubleval($this->taxes->saletaxes) ?>;
    var _global_communicationtaxes_fee = <?php echo doubleval($this->taxes->communicationtaxes) ?>;
</script>
