 
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            echo '<a class="btn btn-xs btn-default" href="/system/fees">';
            echo '<i class="fa fa-arrow-circle-left"></i>&nbsp;' . $_lang_goback;
            echo '</a>';
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->




<div class="row">
    <div class="col-sm-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_edit_block_fees; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">

                <form role="form" class="form-register   box-register" id="form"  >

                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div class="alert alert-success no-display" id="messageProcessOk">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_messageprocessok; ?>
                    </div>
                    
                    

                    <div class="panel-body  form-horizontal">  
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="creditcard">
                                <?php echo $_lang_creditcard; ?> 
                            </label>
                            <div class="col-sm-6">
                                <input type="text"  id="creditcard" name="creditcard" value="<?php echo $this->entity->creditcard; ?>" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="wiretransfer">
                                <?php echo $_lang_wiretransfer; ?> 
                            </label>
                            <div class="col-sm-6">
                                <input type="text"  id="wiretransfer" name="wiretransfer" value="<?php echo $this->entity->wiretransfer; ?>" class="form-control">
                            </div>
                        </div>                    


                    </div>

                    <div class="row">
                        <div class="col-sm-12">


                            <div class="panel panel-default">
                                <div class="panel-footer right ">

                                    <button type="submit" class="btn btn-primary  ">
                                        <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_save; ?> 
                                    </button>

                                </div>
                            </div>


                        </div>
                    </div>
                </form>


            </div>
        </div>

    </div>

</div>






