<?php
//-- Catalog Index
$_lang_index_label_module= "System";
$_lang_index_label_action = "Sale Taxes";
 

$_lang_goback = "Go back";


$_lang_edit_block_taxes = "Taxes configuration";
$_lang_saletaxes = "Sale Taxes (%)";
$_lang_communicationtaxes = "Communication Taxes (%)";
$_lang_save = "Save";


$_lang_edit_block_fees = "Fees configuration";
$_lang_creditcard = "Credit card (%)";
$_lang_wiretransfer = "Wire transfer (ammount in USD)";

$_lang_register_label_messageprocessok = "The taxes were updated successfully";
$_lang_register_label_formerror  ="Please introduce the required fields";
$_lang_btn_taxesedit = "Change taxes value";
$_lang_btn_feesedit = "Change fees values";

 