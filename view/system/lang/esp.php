<?php
//-- Catalog Index
$_lang_index_label_module= "Sistema";
$_lang_index_label_action = "Impuesto a las ventas";
 

$_lang_goback = "Volver";


$_lang_edit_block_taxes = "Configuraci&oacute;n de impuestos";
$_lang_saletaxes = "Impuesto a las ventas (%)";
$_lang_communicationtaxes = "Impuestos a la comunicaci&oacute;n (%)";
$_lang_save = "Guardar";


$_lang_edit_block_fees = "Configuraci&oacute;n de cargos";
$_lang_creditcard = "Tarjeta de cr&eacute;dito (%)";
$_lang_wiretransfer = "Transferencia bancaria (montos en USD)";

$_lang_register_label_messageprocessok = "Los impuestos fueron actualizados satisfactoriamente";
$_lang_register_label_formerror  ="Por favor introduzca los campos requeridos";
$_lang_btn_taxesedit ="Actualizar impuestos";
$_lang_btn_feesedit = "Actualizar cargos";

 