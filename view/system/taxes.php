<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php

                echo '<a class="btn  btn-green btn-xs" href="/system/taxesedit">';
                echo '<i class="fa fa-pencil"></i>&nbsp;' . $_lang_btn_taxesedit;
                echo '</a>';

            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<form role="form" class=" form-placeorder  box-user-register" id="form-placeorder"  >
 
    <div class="row">
        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_edit_block_taxes; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <th><?php echo $_lang_saletaxes; ?></th>
                                <td> <?php echo $this->entity->saletaxes; ?> </td> 
                                
                            </tr>

                            <tr>
                                <th><?php echo $_lang_communicationtaxes; ?></th>
                                <td> <?php echo $this->entity->communicationtaxes; ?> </td> 
                               
                            </tr>



                        </tbody>
                    </table>



                </div>
            </div>

        </div>

    </div>

 

</form>



