<!-- Modal Set on Delete -->
<div class="modal fade" id="modalAllowanceAlert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form name="form_modalAllowanceAlert"   id="form_modalAllowanceAlert">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo $_lang_modal_title; ?> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <?php echo $_lang_modal_description; ?> 


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $_lang_modal_close; ?></button>
                    <!--<button type="button" class="btn btn-bricky" id="btnSetDispatch"><?php echo $_lang_modal_delete; ?></button>-->
                    <input type="hidden" id="delete_id" name="delete_id"  />
                    <a href="#" id="btnDeleteAlert" class="btn  btn-bricky"><i class="fa fa-trash"></i> <?php echo $_lang_modal_delete; ?></a>

                </div>

            </form>

        </div>
    </div>
</div>






<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-warning"></i>
                <a href="/allowancealert/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <a class="btn btn-xs btn-primary" href="/allowancealert/register">
                <i class="fa fa-plus-circle"></i> <?php echo $_lang_index_registernew; ?>
            </a>
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_records; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                        <thead>
                            <tr>
                                <th><?php echo $_lang_index_registerdatetime; ?></th>
                                <th><?php echo $_lang_index_accountname; ?></th>

                                <th><?php echo $_lang_index_planname; ?></th>
                                <th><?php echo $_lang_index_trafficallowancename; ?></th>


                                <th><?php echo $_lang_index_source; ?></th>
                                <th><?php echo $_lang_index_subject; ?></th>
                                <th><?php echo $_lang_index_emails; ?></th>
                                <th><?php echo $_lang_index_mobilenumbers; ?></th>
                                <th> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> - </td>
                                <td> - </td>
                                <td> - </td>
                                <td> - </td>
                                <td> - </td>
                                <td> - </td>
                                <td> - </td>
                                <td> - </td>
                                <td> - </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->
