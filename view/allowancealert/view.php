<script type="text/javascript">
    _var_accountid = <?php echo $this->accountid; ?>
</script>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-warning"></i>
                <a href="/allowancealert/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_view; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-6">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_block_label_airtimedevice; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                    <tbody>
                        <tr>
                            <td id="header_product_image" width="10%"> <?php echo $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/' . $this->inventoryitem->productid . '/type/thumb"  >'; ?> </td>
                            <td id="header_product_content"> 
                                <?php
                                $hasvoicenumber = false;
                                $hasdatanumber = false;


                                echo '<strong>' . $_lang_index_iserial . '</strong>' . ': <span id="device_iserial">' . $this->inventoryitem->iserial . '</span><br>';
                                echo '<strong>' . $_lang_index_sku . '</strong>' . ': <span  id="device_sku">' . $this->inventoryitem->sku . '</span><br>';
                                echo '<strong>' . $_lang_index_productname . '</strong>' . ': <span  id="device_productname">' . $this->inventoryitem->productname . '</span><br>';
                                if ($this->inventoryitem->voicenumber != "") {
                                    echo '<strong>' . $_lang_index_voicenumber . '</strong>' . ': <span  id="device_productname">' . $this->inventoryitem->voicenumber . '</span><br>';
                                }
                                if ($this->inventoryitem->datanumber != "") {
                                    echo '<strong>' . $_lang_index_datanumber . '</strong>' . ': <span  id="device_productname">' . $this->inventoryitem->datanumber . '</span><br>';
                                }
                                ?>
                                <input type="hidden" name="inventoryitemid" id="inventoryitemid" value="<?php echo $this->inventoryitemid; ?>" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_block_label_notification; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                    <tbody>
                        <tr>
                            <td id="header_product_content"> 
                                <?php
                                $hasvoicenumber = false;
                                $hasdatanumber = false;


                                echo '<strong>' . $_lang_index_lbl_email . '</strong>' . ': <span id="device_iserial">' . $this->entity->emails . '</span><br>';

                                echo '<strong>' . $_lang_index_lbl_subject . '</strong>' . ': <span  id="device_productname">' . $this->entity->subject . '</span><br>';
                                if ($this->entity->source != "") {
                                    echo '<strong>' . $_lang_index_voicenumber . '</strong>' . ': <span  id="device_productname">' . $this->entity->source . '</span><br>';
                                }
                                ?>
                                <input type="hidden" name="inventoryitemid" id="inventoryitemid" value="<?php echo $this->inventoryitemid; ?>" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_block_label_config; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                    <tbody>
                         
                                <?php
                                    if($this->entity->traffictypecode !="" && $this->entity->traffictypecode !="0"){
                                        echo '<tr><td><strong>'.$_lang_index_block_label_airtimelimitbytraffic.'</strong>: ';
                                        echo $this->entity->traffictypecode.'  '.$this->entity->tunitalertqty.' '.$this->entity->tunitcode;
                                        echo '</td></tr>';
                                        
                                    }
                                    
                                    if($this->entity->chargecode !=""   && $this->entity->chargecode !="0"){
                                        echo '<tr><td><strong>'.$_lang_index_block_label_airtimelimitbycharge.'</strong>: ';
                                            if ($this->entity->chargecode != "UNIT") {
                                                
                                                echo   '  USD ' . $this->entity->chargealerqty;
                                            } else {
                                                echo   $this->entity->chargecode . '  ' . $this->entity->chargealerqty;
                                            }
                                        echo '</td></tr>';                                       
                                    }
                                    
                                ?>
                         
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- end: PAGE CONTENT-->

<?php
if ($this->entity->operationrequested == "airtime_cart" || $this->entity->operationrequested == "airtime_order") {

    if ($this->entity->operationrequested == "airtime_cart") {
        echo '<a href="/airtimeoperation/mycart" class="btn btn-xs btn-green"  > <i class="fa fa-shopping-cart"></i>  This item is your airtime cart</a>';
    } else if ($this->entity->operationrequested == "airtime_order") {
        echo '<a href="/airtimeoperation/searchairtimeorder/iserial/' . $this->entity->iserial . '" class="btn btn-xs btn-primary"  > <i class="fa fa-plane"></i>  This item is an pending order</a>';
    }
} else {

    if ($this->entity->operationregistered == "activate") {
        require 'simcard_suspend.php';
    } else {
        require 'simcard_activate.php';
    }
}
