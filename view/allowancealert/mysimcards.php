<script type="text/javascript">
_var_accountid = <?php echo $this->accountid; ?>
</script>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dropbox"></i>
                <a href="/allowancealert/index">
                    <?php echo $_lang_index_label_module;  ?>
                </a>
            </li>
            <li class="active">
                    <?php echo $_lang_index_label_actionchose;  ?>
            </li>
            <li class="search-box">
               <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
          
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_block_label_choose;  ?>
                <div class="panel-tools">
                     <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                 <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover  table-full-width dt-responsive nowrap" id="mainTable">
                    <thead>
                        <tr>
                            <th class="hidden-xs"></th>
                            <th><?php echo $_lang_index_iserial;  ?></th>
                            <th class="hidden-xs"><?php echo $_lang_index_productname;  ?></th>
                            <th class="hidden-xs"><?php echo $_lang_index_plan;  ?></th>
                            <th class="hidden-xs"><?php echo $_lang_index_plan;  ?></th>
                            <th><?php echo $_lang_index_voicenumber;  ?></th>
                            <th class="hidden-xs" ><?php echo $_lang_index_datanumber;  ?> </th>
                            <th class="hidden-xs"><?php echo $_lang_index_status;  ?> </th> 
                                                        
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="hidden-xs"> - </td>
                            <td> - </td>
                             <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                            <td > - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>   
                             
                        </tr>
                    </tbody>
                </table>
                 </div>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
        
    </div>
</div>
<!-- end: PAGE CONTENT-->
