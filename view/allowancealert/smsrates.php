
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-warning"></i>
                <a href="/allowancealert/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_smsrates; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_smsrates; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">

                <p> We are connected to mobile carriers globally so that you can alert  sending a SMS to your mobile numbers wherever they are. <br />
                    * Possible delays in SMS reception occurred by your operator are not our responsibility
                </p> 



                <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                        <thead>
                            <th>ISO</th>
                            <th>COUNTRY</th>
                            <th>DESC</th>
                            <th>PRICE (USD)</th>
                        </thead>

                        <tbody>
                            <?php 
                            foreach($this->aPrices as $record){
                                echo '<tr>';
                                echo '<td>'.$record->iso.'</td>';
                                echo '<td>'.$record->country.'</td>';
                                echo '<td>'.$record->description.'</td>';
                                echo '<td>'.$record->price_sms.'</td>';
                                echo '</tr>';
                            }
                            ?>
                        </tbody>

                    </table>

                </div>

           

            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->
