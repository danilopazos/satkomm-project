<?php
$_lang_index_label_module= "Allowance Alert";
$_lang_index_label_action = "List";
$_lang_index_label_actionchose = "Choose";
$_lang_index_label_smsrates = "Sms Rates";
$_lang_index_label_view = " Config Details";

$_lang_index_label_actionregister = "Register";
 
$_lang_index_records= "Alerts registered";
$_lang_index_registernew =  "Register new alert";

$_lang_index_registerdatetime= "Date record";
$_lang_index_sku= "Sku";
$_lang_index_iserial= "Serial/ID";
$_lang_index_productname= "Product";
$_lang_index_plantype= "Type";
$_lang_index_plan= "Plan";
$_lang_index_voicenumber= "Voice #";
$_lang_index_datanumber= "Data #";
$_lang_index_status= "Status";
$_lang_index_accountname  ="Account";

$_lang_index_planname  ="Plan";
$_lang_index_trafficallowancename  ="Traffic";


$_lang_index_source= "Voice/Data/Serial/IMEI #";
$_lang_index_subject= "Subject";
$_lang_index_emails= "Emails";
$_lang_index_mobilenumbers= "Mobile #";

$_lang_index_traffictype = "Traffic Type";

$_lang_index_tunit = "Unit";
$_lang_index_tunitalertqty = "Alert Qty";

$_lang_index_chargecode = "Charge";
$_lang_index_chargealertqty = "Chage Ammount";

$_lang_index_block_label_choose = "Choose the airtime device";
$_lang_index_block_label_airtimedevice = "Airtime Device";

$_lang_index_block_label_airtimelimitbytraffic = "Alert for airtime included";
$_lang_index_block_label_airtimelimitbycharge = "Alert for additional charges";

$_lang_index_block_label_notification = "Notification recepiet";

$_lang_index_lbl_chargecode = "Charge";
$_lang_index_lbl_chargeqty = "Ammount";
$_lang_index_lbl_unitqty = "Quantity";

$_lang_index_select = "Select";

$_lang_index_lbl_email = "Email";
$_lang_index_lbl_email_help = "Introduce emails separated by commas (,)";
$_lang_index_lbl_subject = "Subject";
$_lang_index_lbl_mobilenumbers  = "Mobile Number -  SMS  *";
$_lang_index_lbl_mobilenumbers_help  = "Introduce mobile numbers separated by commas (,)";
$_lang_index_lbl_mobilenumbers_info  = "* The SMS notification alerts will incur in charges, please see the table of rates per SMS and destination";
$_lang_index_lbl_mobilenumbers_tablerate  = "prices per SMS";

$_lang_index_lbl_optional = "optional";

$_lang_register_save = "Save";

$_lang_register_test = "Test";
        

$_lang_register_label_formerror_destination = "Please introduce the correctly the emails and the subject";
$_lang_register_label_formerror_airtimebytraffic = "Please select the traffic type, the unit and the quantity";
$_lang_register_label_formerror_airtimebycharge = "Please select the type of charge and the ammount";
$_lang_register_label_formerror_chooseone = "Please config by traffic type or charges (also could be both)";


$_lang_register_label_success_test = "Test Sent!, verify if you can  check the alert messages";
$_lang_register_label_error_test = "Error, please check the configuration or try later";
$_lang_register_label_error_savebeforetest = "Error, please save the configuration before test it";

$_lang_register_label_success_save = "Alert configuration saved";
$_lang_register_label_error_save = "Problems saving the alert configuration, please try again";

$_lang_index_block_label_config = "Alert configuration";

$_lang_index_alert_allowance  = "Alerts when exceeds "; 
$_lang_index_alert_additionalcharges = "Alerts when additional charge exceeds ";


$_lang_modal_close = "Close";
$_lang_modal_delete = "Delete";
$_lang_modal_title = "Delete alert";
$_lang_modal_description = "Are you sure to delete this alert?";


$_lang_smsalert_ta_1 ="Airtime alert ";
$_lang_smsalert_ta_2 ="exceeded on ";


$_lang_smsalert_ac_1 ="Airtime alert ";
$_lang_smsalert_ac_2 ="additional airtime exceeded on ";



