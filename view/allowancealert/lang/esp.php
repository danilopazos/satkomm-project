<?php
$_lang_index_label_module= "Alerta de consumo";
$_lang_index_label_action = "Lista";
$_lang_index_label_actionchose = "Escoger";
$_lang_index_label_smsrates = "Tarifas de SMS";
$_lang_index_label_view = "Detalles de configuraci&oacute;n";

$_lang_index_label_actionregister = "Registrar";
 
$_lang_index_records= "Alertas registradas";
$_lang_index_registernew =  "Registrar nueva alerta";

$_lang_index_registerdatetime= "Fecha del registro";
$_lang_index_sku= "Sku";
$_lang_index_iserial= "Serial/ID";
$_lang_index_productname= "Producto";
$_lang_index_plantype= "Tipo";
$_lang_index_plan= "Plan";
$_lang_index_voicenumber= "Voz #";
$_lang_index_datanumber= "Datos #";
$_lang_index_status= "Estado";
$_lang_index_accountname  ="Cuenta";

$_lang_index_planname  ="Plan";
$_lang_index_trafficallowancename  ="Tr&aacute;fico";

$_lang_index_source= "Voz/Datos/Serial/IMEI #";
$_lang_index_subject= "Asunto";
$_lang_index_emails= "Correos";
$_lang_index_mobilenumbers= "M&oacute;vil #";

$_lang_index_traffictype = "Tipo de tr&aacute;fico";

$_lang_index_tunit = "Unidades";
$_lang_index_tunitalertqty = "Cantidad";

$_lang_index_chargecode = "Cargos";
$_lang_index_chargealertqty = "Montos del cargo";

$_lang_index_block_label_choose = "Seleccione el equipo";
$_lang_index_block_label_airtimedevice = "Equipo airtime";

$_lang_index_block_label_airtimelimitbytraffic = "Alerta por tiempo de aire incluido";
$_lang_index_block_label_airtimelimitbycharge = "Alerta por consumo adicional";

$_lang_index_block_label_notification = "Destino de la notificaci&oacute;n";

$_lang_index_lbl_chargecode = "Cargos";
$_lang_index_lbl_chargeqty = "Montos";
$_lang_index_lbl_unitqty = "Cantidad";

$_lang_index_select = "Seleccionar";

$_lang_index_lbl_email = "Correo";
$_lang_index_lbl_email_help = "Introduzca los correos separados por commas (,)";
$_lang_index_lbl_subject = "Asunto";
$_lang_index_lbl_mobilenumbers  = "N&uacute;mero m&oacute;vil -  SMS  *";
$_lang_index_lbl_mobilenumbers_help  = "Introduzca el n&uacutelmero m&oacute;vil separado por comas (,)";
$_lang_index_lbl_mobilenumbers_info  = "* La notificaci&oacute;n por alertas SMS incurriran en costos, por favor revise la tarifa de envios SMS";
$_lang_index_lbl_mobilenumbers_tablerate  = "precios por SMS";

$_lang_index_lbl_optional = "opcional";

$_lang_register_save = "Guardar";

$_lang_register_test = "Probar";
        

$_lang_register_label_formerror_destination = "Por favor introduzca correctamente los correos y el asunto";
$_lang_register_label_formerror_airtimebytraffic = "Por favor seleccione los umbrales de alertas";
$_lang_register_label_formerror_airtimebycharge = "Por favor seleccione el tipo de cargo y el monto";
$_lang_register_label_formerror_chooseone = "Por favor seleccione el tipo de tr&aacute;fico";


$_lang_register_label_success_test = "Prueba enviada!, verifique los mensajes de alerta enviados";
$_lang_register_label_error_test = "Error, por favor verifique la configuraci&oacute;n e intente nuevamente";
$_lang_register_label_error_savebeforetest = "Error, por favor guarde la configuracion antes de probarla";

$_lang_register_label_success_save = "Configuraci&oacute;n de alerta guardada";
$_lang_register_label_error_save = "Problemas guardando la configuraci&oacute;n de alerta";

$_lang_index_block_label_config = "Configuraci&oacute;n de alerta";

$_lang_index_alert_allowance  = "Alertar cuando exceda el "; 
$_lang_index_alert_additionalcharges = "Alerta cuando sobrepase los ($)";


$_lang_modal_close = "Cerrar";
$_lang_modal_delete = "Eliminar";
$_lang_modal_title = "Eliminar Alerta";
$_lang_modal_description = "Esta seguro de eliminar la alerta?";


$_lang_smsalert_ta_1 ="Alerta de tiempo de aire ";
$_lang_smsalert_ta_2 ="excedido en ";


$_lang_smsalert_ac_1 ="Alerta de tiempo de aire ";
$_lang_smsalert_ac_2 ="tiempo de aire adicional excedido en  ";