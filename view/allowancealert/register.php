<script type="text/javascript">
    var _servicetype_code = '<?php echo $this->inventoryitem->servicetype_code ; ?>';
</script> 


<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-warning"></i>
                <a href="/allowancealert/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_actionregister; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->


        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<form role="form" class="form-register   box-register" id="form"  >



    <div id="messageProcessOk" class="alert alert-success no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_block_label_airtimedevice; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>

                <div class="panel-body  form-horizontal">  

                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                        <tbody>
                            <tr>
                                <td id="header_product_image" width="10%"> <?php echo $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/' . $this->inventoryitem->productid . '/type/thumb"  >'; ?> </td>
                                <td id="header_product_content"> 
                                    
                                    <?php
                                    $hasvoicenumber = false;
                                    $hasdatanumber = false;
 
                                    ?>
                                    
                                    <table class="table table-striped table-bordered table-hover table-full-width">
                                        <tr>
                                            <td><?php echo '<strong>' . $_lang_index_iserial . '</strong>'; ?></td>
                                            <td><?php echo '<span id="device_iserial">' . $this->inventoryitem->iserial . '</span>'; ?></td>
                                        </tr> 
                                        
                                        <tr>
                                            <td><?php echo '<strong>' . $_lang_index_sku . '</strong>'; ?></td>
                                            <td><?php echo '<span  id="device_sku">' . $this->inventoryitem->sku . '</span>'; ?></td>
                                        </tr>                                                
                                        
                                        <tr>
                                            <td><?php echo '<strong>' . $_lang_index_productname . '</strong>'; ?></td>
                                            <td><?php echo '  <span  id="device_productname">' . $this->inventoryitem->productname . '</span>'; ?></td>
                                        </tr>                                                
                                      
                                     <?php    
                                        if ($this->inventoryitem->voicenumber != "") {
                                            echo '<tr>';
                                            echo '<td><strong>' . $_lang_index_voicenumber . '</strong>' . '</td><td> <span  id="device_productname">' . $this->inventoryitem->voicenumber . '</span></td>';
                                            echo '</tr>';
                                        }
                                        if ($this->inventoryitem->datanumber != "") {
                                            echo '<tr>';
                                            echo '<td><strong>' . $_lang_index_datanumber . '</strong>' . '</td><td><span  id="device_productname">' . $this->inventoryitem->datanumber . '</span></td>';
                                            echo '</tr>';
                                        }                                        
                                      ?>  
                                        
                                        <tr>
                                            <td><?php echo '<strong>' . $_lang_index_plantype . '</strong>'; ?></td>
                                            <td><?php echo '  <span  id="device_productname">' . $this->inventoryitem->servicetype_code . '</span>'; ?></td>
                                        </tr>                                             
                                        
                                        <tr>
                                            <td><?php echo '<strong>' . $_lang_index_plan . '</strong>'; ?></td>
                                            <td><?php echo '  <span  id="device_productname">' . $this->inventoryitem->planname . '</span>'; ?></td>
                                        </tr>                                          
                                        
                                    </table>
                                    

                                    <input type="hidden" name="inventoryitemid" id="inventoryitemid" value="<?php echo $this->inventoryitemid; ?>" />
                                </td>

                            </tr>
                        </tbody>
                    </table>

                </div>

            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>

        <div class="col-sm-6">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_block_label_notification; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>

                <div class="panel-body  form-horizontal">  

                    <div class="errorHandler alert alert-danger no-display" id="alert_destination_alert">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror_destination; ?>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="emails">
                            <?php echo $_lang_index_lbl_email; ?>
                        </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="emails" id="emails" value="<?php echo $this->entity->emails; ?>" />

                            <?php echo $_lang_index_lbl_email_help; ?>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="subject">
                            <?php echo $_lang_index_lbl_subject; ?>
                        </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="subject" id="subject" value="<?php echo $this->entity->subject; ?>" />
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="mobilenumbers">
                            <?php echo $_lang_index_lbl_mobilenumbers . ' (' . $_lang_index_lbl_optional . ')'; ?>
                        </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="mobilenumbers" id="mobilenumbers"  value="<?php echo $this->entity->mobilenumbers; ?>" />
                            <?php
                            echo $_lang_index_lbl_mobilenumbers_help . '<br />';
                            echo $_lang_index_lbl_mobilenumbers_info . ' <a href="/allowancealert/smsrates" target="_blank" class="btn btn-xs btn-default">' . $_lang_index_lbl_mobilenumbers_tablerate . '</a>';
                            ?>
                        </div>

                    </div>  

                </div>

            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_block_label_airtimelimitbytraffic; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>

                <div class="panel-body  form-horizontal">  

                    <div class="errorHandler alert alert-danger no-display" id="alert_limitbytraffic_alert">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror_airtimebytraffic; ?>
                    </div>
                    
                    
                    <div class="errorHandler alert alert-danger no-display" id="alert_choose_traffictype">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror_chooseone; ?>
                    </div>                    



                    <?php 
                    if($this->inventoryitem->servicetype_code != "PREPAID"){
                     ?>   
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="traffictypecode">
                            <?php echo $_lang_index_traffictype; ?>
                        </label>
                        <div class="col-sm-8">
                            
                           
                            <select name="taidcrypt" id="taidcrypt"   class="form-control">
                                <option value=""><?php echo $_lang_index_select; ?></option>

                                <?php
                                $taidcrypt = $this->entity->taidcrypt;
                                foreach ($this->aTrafficAllowance as $record) {
                                    $selected = '';
                                    if( $record->idcrypt ==  $taidcrypt ){
                                        $selected = ' selected = "true" ';
                                    }
                                    echo '<option '.$selected.' value="' . $record->idcrypt . '">' . $record->tname . ' - Included: ' . $record->qty . '  ' . $record->unitcode . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                     <?php   
                    }
                    ?>

                    
                    
                    <?php  $_aPercent = array(10,20,30,40,50,60,70,90,100); ?>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="alertallowance01">
                            <?php echo $_lang_index_alert_allowance; ?>
                        </label>
                        <div class="col-sm-8">
                            <select name="alertallowance01" id="alertallowance01"   class="form-control"    >
                                <option value="0"><?php echo $_lang_index_select; ?></option>

                                <?php
                                foreach ($_aPercent as $percent) {
                                    
                                    $selected = '  ';
                                    if($percent ==  intval($this->entity->taexceed01) ){
                                        $selected = ' selected = "true" ';
                                    }
                                    
                                    echo '<option '.$selected.' value="' . $percent. '">' . $percent . ' % ' . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="alertallowance02">
                            <?php echo $_lang_index_alert_allowance; ?>
                        </label>
                        <div class="col-sm-8">
                            <select name="alertallowance02" id="alertallowance02"   class="form-control">
                                <option value="0"><?php echo $_lang_index_select; ?></option>

                                <?php
                                 
                                foreach ($_aPercent as $percent) {
                                    
                                    $selected = '  ';
                                    if($percent ==  intval($this->entity->taexceed02) ){
                                        $selected = ' selected = "true" ';
                                    }                                       
                                    
                                    echo '<option '.$selected.' value="' . $percent. '">' . $percent . ' % ' . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="alertallowance03">
                            <?php echo $_lang_index_alert_allowance; ?>
                        </label>
                        <div class="col-sm-8">
                            <select name="alertallowance03" id="alertallowance03"   class="form-control">
                                <option value="0"><?php echo $_lang_index_select; ?></option>

                                <?php
                                foreach ($_aPercent as $percent) {
                                    
                                    $selected = '  ';
                                    if($percent ==  intval($this->entity->taexceed03) ){
                                        $selected = ' selected = "true" ';
                                    }                                      
                                    
                                    echo '<option '.$selected.'  value="' . $percent. '">' . $percent . ' % ' . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    
                 
                </div>

            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>

                    <?php 
                    if($this->inventoryitem->servicetype_code != "PREPAID"){
                     ?>           
        
        <div class="col-sm-6">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_block_label_airtimelimitbycharge; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>

                <div class="panel-body  form-horizontal">  

                    <div class="errorHandler alert alert-danger no-display" id="alert_limitbycharge_alert">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror_airtimebycharge; ?>
                    </div>
 
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="alertadditionalcharges01">
                            <?php echo $_lang_index_alert_additionalcharges; ?>
                        </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  value="<?php echo doubleval( $this->entity->acexceed01 ) ?>"   name="alertadditionalcharges01" id="alertadditionalcharges01" />
                        </div>
                    </div>        
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="alertadditionalcharges02">
                            <?php echo $_lang_index_alert_additionalcharges; ?>
                        </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo doubleval( $this->entity->acexceed02 ) ?>" name="alertadditionalcharges02" id="alertadditionalcharges02" />
                        </div>
                    </div> 
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="alertadditionalcharges03">
                            <?php echo $_lang_index_alert_additionalcharges; ?>
                        </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo doubleval( $this->entity->acexceed03 ) ?>" name="alertadditionalcharges03" id="alertadditionalcharges03" />
                        </div>
                    </div> 
                    
                    

                </div>

            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
        
        <?php 
                    }
        ?>
        
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="errorHandler alert alert-danger no-display" id="alert_chooseone_alert">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror_chooseone; ?>
            </div>


            <div class="  alert alert-success no-display" id="alert_test_success">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_success_test; ?>
            </div>
            <div class="  alert alert-danger no-display" id="alert_test_error">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_error_test; ?>
            </div>
            <div class="  alert alert-danger no-display" id="alert_test_savebeforetest">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_error_savebeforetest; ?>
            </div>            
            

            <div class="  alert alert-success no-display" id="alert_save_success">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_success_save; ?>
            </div>
            <div class="  alert alert-danger no-display" id="alert_save_error">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_error_save; ?>
            </div>

            <div class="panel panel-default">



                <div class="panel-footer">
                    <input type="hidden" value="<?php echo $this->entity->id; ?>" id="id" name="id" />
                    
                    <button type="button" id="btn_test" class="btn btn-green ">
                        <i class="fa fa-plane"></i>&nbsp;<?php echo $_lang_register_test; ?> 
                    </button>
                    &nbsp; &nbsp;
                    <button type="button"  id="btn_save"  class="btn btn-primary ">
                        <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                    </button>

                </div>
            </div>


        </div>
    </div>




</form>
<!-- end: PAGE CONTENT-->
