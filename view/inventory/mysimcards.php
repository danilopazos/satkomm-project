<script type="text/javascript">
    _var_accountid = <?php echo $this->accountid; ?>
</script>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dropbox"></i>
                <a href="/inventory/mysimcards">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_mysimcards; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->

<!-- Modal Set on Inventory -->
<div class="modal fade" id="modalSetOnInventory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <form name="form_setoninventory"   id="form_setoninventory">
            
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo $_lang_modal_soi_title; ?> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <table class="table table-striped table-bordered table-hover table-full-width"  >


                        <tr><th colspan="2"><?php echo $_lang_modal_soi_date; ?></th>
                            <td>

                                <div class="input-group" id="picker-container">
                                    <input type="hidden" id="inventoryitemid" name="inventoryitemid" />
                                    <input type="hidden" id="inventoryaction" name="inventoryaction" value="1" />
                                    <input  name="statusdate" id="statusdate" value="<?php echo date("Y-m-d"); ?>"   type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                </div>
                            </td> 
                        </tr>    
                        
                        <tr><th colspan="2"><?php echo $_lang_modal_soi_description; ?></th>
                            <td>

                                <textarea name="statusdescription" id="statusdescription" class="form-control"></textarea>
                                
                            </td> 
                        </tr>                          
                        
                    </table>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $_lang_modal_soi_close; ?></button>
                    <button type="button" class="btn btn-primary" id="btnSetOnInventory"><?php echo $_lang_modal_soi_button; ?></button>
                </div>
            
            </form>
            
        </div>
    </div>
</div>


<!-- Modal Set on Dispatch Inventory -->
<div class="modal fade" id="modalSetDispatchFromOperation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <form name="form_setdispatchmovement"   id="form_setdispatchmovement">
            
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo $_lang_modal_soi_title; ?> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <table class="table table-striped table-bordered table-hover table-full-width"  >

                        <tr>
                            <th colspan="2"><?php echo $_lang_modal_soo_type; ?>
                                
                            <td>
                                <select name="inventoryaction" id="dinventoryaction" class="form-control" >
                                <?php 
                                $opindex = 0;
                                foreach($_lang_inventory_status_op as $op){
                                    if($opindex > 2){
                                        echo '<option value="'.$opindex.'">'.$op.'</option>';
                                    }
                                    $opindex ++;
                                }
                                ?>
                                </select>
                            </td>
                            
                        </th>
                        
                        
                        <tr><th colspan="2"><?php echo $_lang_modal_soo_date; ?></th>
                            <td>

                                <div class="input-group" id="picker-container">
                                    <input type="hidden" id="dinventoryitemid" name="inventoryitemid" />
                                    <input  name="statusdate" id="dstatusdate" value="<?php echo date("Y-m-d"); ?>"   type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                </div>
                            </td> 
                        </tr>    
                        
                        <tr><th colspan="2"><?php echo $_lang_modal_soi_description; ?></th>
                            <td>

                                <textarea name="statusdescription" id="dstatusdescription" class="form-control"></textarea>
                                
                            </td> 
                        </tr>                          
                        
                    </table>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $_lang_modal_soi_close; ?></button>
                    <button type="button" class="btn btn-primary" id="btnSetDispatch"><?php echo $_lang_modal_soo_button; ?></button>
                </div>
            
            </form>
            
        </div>
    </div>
</div>


<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
 
        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                
                <li>
                    <a data-toggle="tab" href="#panel_notinventory">
                        <?php echo $_lang_panel_notinventory; ?>
                    </a>
                </li>                
                
                <li class="active">
                    <a data-toggle="tab" href="#panel_inventory">
                        <?php echo $_lang_panel_inventory; ?>
                    </a>
                </li>
               
                
                <li>
                    <a data-toggle="tab" href="#panel_pendingtoactivate">
                        <?php echo $_lang_panel_pendingtoactivate; ?>
                    </a>
                </li>                
                
                
                <li>
                    <a data-toggle="tab" href="#panel_activated">
                        <?php echo $_lang_panel_activated; ?>
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#panel_suspend">
                        <?php echo $_lang_panel_suspend; ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_deactivated">
                        <?php echo $_lang_panel_deactivated; ?>
                    </a>
                </li> 
            </ul>
            <div class="tab-content">
                
                <div id="panel_notinventory" class="tab-pane in ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_action_mysimcards; ?>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">

                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="pendingToArriveTable">
                                    <thead>
                                        <tr>
                                            <th width="10%" class="hidden-xs"></th>
                                            <th width="10%"><?php echo $_lang_index_label_simcardnumber; ?></th>
                                            <th width="10%" class="hidden-xs"><?php echo $_lang_index_label_documentno; ?></th>
                                            <th width="10%" class="hidden-xs"><?php echo $_lang_index_label_sku; ?></th>
                                            <th><?php echo $_lang_index_label_productname; ?></th>
                                            <th width="10%" class="hidden-xs" ><?php echo $_lang_index_label_productprice; ?> </th>
                                            <th width="10%"  > <?php echo $_lang_index_label_invoicedate; ?> </th>
                                            <th width="10%"  ><?php echo $_lang_index_label_dispatchdate; ?>  </th>
                                            <th width="10%"  >   </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="hidden-xs"> - </td>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td > - </td>
                                            <td > - </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="panel_inventory" class="tab-pane in active">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_action_mysimcards; ?>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">

                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="tableInventory">
                                    <thead>
                                        <tr>
                                            <th width="10%" class="hidden-xs"></th>
                                            <th width="10%"><?php echo $_lang_index_label_simcardnumber; ?></th>
                                            <th width="10%" class="hidden-xs"><?php echo $_lang_index_label_documentno; ?></th>
                                            <th width="10%" class="hidden-xs"><?php echo $_lang_index_label_sku; ?></th>
                                            <th><?php echo $_lang_index_label_productname; ?></th>
                                            <th width="10%" class="hidden-xs" ><?php echo $_lang_index_label_productprice; ?> </th>
                                            <th width="10%"  > <?php echo $_lang_index_label_invoicedate; ?> </th>
                                            <th width="10%"  ><?php echo $_lang_index_label_dispatchdate; ?>  </th>
                                            <th width="10%"  ><?php echo $_lang_index_label_datereceived; ?>  </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="hidden-xs"> - </td>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>   
                                            <td > - </td>
                                            <td > - </td> 
                                        </tr>
                                    </tbody>
                                </table>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
                                
                <div id="panel_pendingtoactivate" class="tab-pane in ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_action_mysimcards; ?>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">

                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="pendingToActivate">
                                    <thead>
                                        <tr>
                                            <th  width="10%" class="hidden-xs"></th>
                                            <th   width="10%"><?php echo $_lang_index_label_simcardnumber; ?></th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_view_label_voicenumber; ?></th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_view_label_datanumber; ?></th>
                                            <th  width="10%"><?php echo $_lang_index_label_productname; ?></th>
                                            <th  width="10%" class="hidden-xs" ><?php echo $_lang_activatesimcard_plantype  ; ?> </th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_index_label_providerplan; ?> </th> 
                                            <th   width="10%" > <?php echo $_lang_index_label_customerplan; ?></th>
                                            <th   width="10%"><?php echo $_lang_index_label_customername; ?> </th>                          
                                            <th  width="10%"><?php echo $_lang_index_label_processingdate; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="hidden-xs"> - </td>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>   
                                            <td > - </td>
                                            <td > - </td>
                                            <td > - </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>                
                                
                <div id="panel_activated" class="tab-pane in ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_action_mysimcards; ?>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">

                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="mainTable">
                                    <thead>
                                        <tr>
                                            <th  width="10%" class="hidden-xs"></th>
                                            <th   width="10%"><?php echo $_lang_index_label_simcardnumber; ?></th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_view_label_voicenumber; ?></th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_view_label_datanumber; ?></th>
                                            <th  width="10%"><?php echo $_lang_index_label_productname; ?></th>
                                            <th  width="10%" class="hidden-xs" ><?php echo $_lang_activatesimcard_plantype  ; ?> </th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_index_label_providerplan; ?> </th> 
                                            <th   width="10%" > <?php echo $_lang_index_label_customerplan; ?></th>
                                            <th   width="10%"><?php echo $_lang_index_label_customername; ?> </th>                          
                                            <th  width="10%"><?php echo $_lang_index_label_processingdate; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="hidden-xs"> - </td>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>   
                                            <td > - </td>
                                            <td > - </td>
                                            <td > - </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="panel_suspend" class="tab-pane in  ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_action_mysimcards; ?>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">

                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="tableSuspend">
                                    <thead>
                                        <tr>
                                            <th  width="10%" class="hidden-xs"></th>
                                            <th   width="10%"><?php echo $_lang_index_label_simcardnumber; ?></th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_view_label_voicenumber; ?></th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_view_label_datanumber; ?></th>
                                            <th  width="10%"><?php echo $_lang_index_label_productname; ?></th>
                                            <th  width="10%" class="hidden-xs" ><?php echo $_lang_activatesimcard_plantype  ; ?> </th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_index_label_providerplan; ?> </th> 
                                            <th   width="10%" > <?php echo $_lang_index_label_customerplan; ?></th>
                                            <th   width="10%"><?php echo $_lang_index_label_customername; ?> </th>                          
                                            <th  width="10%"><?php echo $_lang_index_label_processingdate; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="hidden-xs"> - </td>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>   
                                            <td > - </td>
                                            <td > - </td>
                                            <td > - </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="panel_deactivated" class="tab-pane in  ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_action_mysimcards; ?>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">

                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="tableDeactivated">
                                    <thead>
                                        <tr>
                                            <th  width="10%" class="hidden-xs"></th>
                                            <th   width="10%"><?php echo $_lang_index_label_simcardnumber; ?></th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_view_label_voicenumber; ?></th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_view_label_datanumber; ?></th>
                                            <th  width="10%"><?php echo $_lang_index_label_productname; ?></th>
                                            <th  width="10%" class="hidden-xs" ><?php echo $_lang_activatesimcard_plantype  ; ?> </th>
                                            <th  width="10%" class="hidden-xs"><?php echo $_lang_index_label_providerplan; ?> </th> 
                                            <th   width="10%" > <?php echo $_lang_index_label_customerplan; ?></th>
                                            <th   width="10%"><?php echo $_lang_index_label_customername; ?> </th>                          
                                            <th  width="10%"><?php echo $_lang_index_label_processingdate; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="hidden-xs"> - </td>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>   
                                            <td > - </td>
                                            <td > - </td>
                                            <td > - </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>    
 
        </div>
    </div>
   