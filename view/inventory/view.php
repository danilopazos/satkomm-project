<script type="text/javascript">
    _var_accountid = <?php echo $this->accountid; ?>
</script>
<?php
$_haspendingorder = false;
?>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dropbox"></i>
                <a href="/inventory/mydevices">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <h4><?php echo $this->entity->productname; ?> <small><?php echo $_lang_index_label_sku . ': ' . $this->entity->sku . ' | ' . $_lang_index_label_iserial . ': ' . $this->entity->iserial; ?></small></h4>
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">


        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" >

                <li  class="active">
                    <a data-toggle="tab" href="#panel_movements">
                        <?php echo $_lang_inventory_info_movement; ?>
                    </a>
                </li>                
                <?php
                if ($this->entity->issimcard == 1 || $this->entity->gawosc == 1) {
                    ?>
                    <li>
                        <a data-toggle="tab" href="#panel_simcard">
                            <?php echo $_lang_panel_simcardinfo; ?>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>

            <div class="tab-content">


                <div id="panel_movements" class="tab-pane in active">

                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable"> 
                        <thead>
                        <th><?php echo $_lang_modal_soo_date; ?></th>
                        <th><?php echo $_lang_modal_soo_type; ?></th>
                        <th><?php echo $_lang_modal_soi_description; ?></th>
                        <th><?php echo $_lang_index_label_user; ?></th>
                        </thead>
                        <tbody>

                            <?php
                            $totalHistory = count($this->inventoryHistory);
                            if ($totalHistory > 0) {

                                foreach ($this->inventoryHistory as $e) {
                                    echo '<tr>';
                                    echo '<td width="10%">' . $e->statusdate . '</td>';
                                    echo '<td width="10%">' . $_lang_inventory_status_op[$e->inventorystatus] . '</td>';

                                    if ($e->customerinvoiceidcrypt != "") {
                                        $invoicelink = '<a target="_blank" href="/invoicetc/view/id/' . $e->customerinvoiceidcrypt . '" class="btn btn-xs btn-default">' . $e->customerinvoiceno . '</a>';
                                        echo '<td>' . $invoicelink . '</td>';
                                    } else {
                                        echo '<td>' . $e->statusdescription . '</td>';
                                    }

                                    echo '<td  width="10%">' . $e->actionuser_firstname . ' ' . $e->actionuser_lastname . '</td>';
                                    echo '</tr>';
                                }

                                echo '<tr>';
                                echo '<td width="10%">' . substr($this->invoice->dispatchdate, 0, 10) . '</td>';
                                echo '<td width="10%">' . $_lang_invoice_dispatch . ' </td>';
                                //$_dispatchlink = '<a target="_blank" href="/invoice/view/code/' . $this->invoice->code . '" class="btn btn-xs btn-default">' . $this->invoice->code. '</a>';
                                echo '<td colspan="2">';
                                $showtrackinginfo = false;
                                if ($this->invoice->shippingprovidercode != "") {
                                    echo '<strong>' . $this->invoice->shippingprovider_name . '</strong>';
                                    $showtrackinginfo = true;
                                }
                                if ($this->invoice->shippingtrackingid != "") {
                                    echo '&nbsp;';
                                    if ($this->invoice->shippingprovidercode == "FEDEX") {
                                        echo '<a target="_blank" href="https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=' . $this->invoice->shippingtrackingid . '&clienttype=ivother">' . $this->invoice->shippingtrackingid . '</a>';
                                    } else if ($this->invoice->shippingprovidercode == "UPS") {
                                        echo '<a target="_blank" href="https://www.ups.com/WebTracking/track?loc=en_US">' . $this->invoice->shippingtrackingid . '</a>';
                                    } else {
                                        echo $this->invoice->shippingtrackingid;
                                    }
                                    $showtrackinginfo = true;
                                }
                                if (!$showtrackinginfo) {
                                    echo '--';
                                }
                                echo '</td>';

                                echo '</tr>';

                                echo '<tr>';
                                echo '<td width="10%">' . substr($this->invoice->invoicedate, 0, 10) . '</td>';
                                echo '<td width="10%">' . $_lang_invoice_buy . ' </td>';
                                $invoicelink = '<a target="_blank" href="/invoice/view/code/' . $this->invoice->code . '" class="btn btn-xs btn-default">' . $this->invoice->code . '</a>';
                                echo '<td colspan="2">' . $invoicelink . '</td>';
                                echo '</tr>';
                            }
                            ?>

                        </tbody>
                    </table>

                </div>



                <?php
                if ($this->entity->issimcard == 1 || $this->entity->gawosc == 1) {
                    ?>
                    <div id="panel_simcard" class="tab-pane in ">


                        <?php
                        //else{

                        if ($this->entity->inventorystatus > 0) {



                            if ($this->entity->operationregistered == "activate" || $this->entity->operationregistered == "suspend"  || $this->entity->operationregistered == "deactivate") {
                                ?>

                                <div class="row">
                                    <div class="col-sm-8">

                                        <!--- -->

                                        <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable"> 
                                            <thead>
                                            <th><?php echo $_lang_index_label_simcardnumber; ?></th>
                                            <th><?php echo $_lang_view_label_voicenumber; ?></th>
                                            <th><?php echo $_lang_view_label_datanumber; ?></th>
                                            
                                            <th><?php echo $_lang_view_label_faxnumber; ?></th>
                                            <th><?php echo $_lang_view_label_imsi; ?></th>
                                            
                                            <th><?php echo $_lang_view_label_puk1; ?></th>
                                            <th><?php echo $_lang_view_label_puk2; ?></th>
                                            <th><?php echo $_lang_view_label_pin; ?></th>
                                            <th><?php echo $_lang_index_label_status; ?></th>
                                            </thead>
                                            <tbody>

                                                <tr>
                                                    <td><?php echo $this->entity->iserial; ?></td>
                                                    <td><?php echo $this->entity->voicenumber; ?></td>
                                                    <td><?php echo $this->entity->datanumber; ?></td>
                                                    
                                                    <td><?php echo $this->entity->faxnumber; ?></td>
                                                    <td><?php echo $this->entity->imsi; ?></td>
                                                    
                                                    <td><?php echo $this->entity->puk1; ?></td>
                                                    <td><?php echo $this->entity->puk2; ?></td>
                                                    <td><?php echo $this->entity->pin; ?></td>
                                                    <td><?php echo $this->entity->operationregistered; ?></td>
                                                </tr>

                                            </tbody>
                                        </table>



            <?php
            if ($this->aAirtimeOperation) {

                foreach ($this->aAirtimeOperation as $ao) {
                    ?>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-external-link-square"></i>
                    <?php
                    echo $_lang_table_airtimeoperation . ' <strong>' . $ao->processeddatetime . ' | ' . $ao->operation . ' </strong>';
                    ?>
                                                        <div class="panel-tools">
                                                            <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                                                        </div>
                                                    </div>
                                                    <div class="panel-body">   


                                                        <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable"> 
                                                            <thead>
                                                            <th><?php echo $_lang_table_label_ordercode; ?></th>
                                                            <th><?php echo $_lang_table_label_operation; ?></th>
                                                            <th><?php echo $_lang_table_label_requestdatetime; ?></th>
                                                            <th><?php echo $_lang_table_label_operationdatetime; ?></th> 
                                                            </thead>
                                                            <tbody>

                                                                <tr>
                                                                    <td><?php echo $ao->airtimeordercode; ?></td>
                                                                    <td><?php echo $ao->operation; ?></td>
                                                                    <td><?php echo $ao->datetoexecute . ' ' . $ao->timetoexecute; ?></td>
                                                                    <td><?php echo $ao->processeddatetime; ?></td> 
                                                                </tr>
                    <?php
                    $_hasproviderplan = false;
                    $_hascustomerplan = false;
                    if ($ao->pp_idcrypt != "") {
                        $_hasproviderplan = true;
                    }
                    if ($ao->pc_idcrypt != "") {
                        $_hascustomerplan = true;
                    }

                    $_table = '';


                    $_table .= '<tr>';

                    $_table .= '<th colspan="2">';
                    $_table .= $_lang_table_label_providerplan;
                    $_table .= '</th>';
                    $_table .= '<th colspan="2">';
                    if ($_hascustomerplan) {
                        $_table .= $_lang_table_label_customerplan;
                    }
                    $_table .= '</th>';

                    $_table .= '</tr>';



                    $_table .= '<tr>';
                    $_table .= '<td colspan="2">';
                    $_table .= $ao->pp_sku . ' | ' . $ao->pp_planname;
                    $_table .= '</td>';

                    $_table .= '<td colspan="2">';
                    if ($_hascustomerplan) {
                        $_table .= $ao->pc_sku . ' | ' . $ao->pc_planname;
                    }
                    $_table .= '</td>';
                    $_table .= '</tr>';


                    $_table .= '<tr>';
                    $_table .= '<th>';
                    $_table .= $_lang_activatesimcard_paymentperiod;
                    $_table .= '</th>';
                    $_table .= '<td>';
                    $_table .= $_lang_period_option[$ao->pp_payment_period_code];
                    $_table .= '</td>';

                    if ($_hascustomerplan) {
                        $_table .= '<th>';
                        $_table .= $_lang_activatesimcard_paymentperiod;
                        $_table .= '</th>';
                        $_table .= '<td>';
                        $_table .= $_lang_period_option[$ao->pc_payment_period_code];
                        $_table .= '</td>';
                    } else {
                        $_table .= '<th colspan="2"></th>';
                    }
                    $_table .= '</tr>';



                    $_table .= '<tr>';
                    $_table .= '<th>';
                    $_table .= $_lang_activatesimcard_agreementduration;
                    $_table .= '</th>';
                    $_table .= '<td>';
                    $_table .= $_lang_period_option[$ao->pp_contract_period_code];
                    $_table .= '</td>';

                    if ($_hascustomerplan) {
                        $_table .= '<th>';
                        $_table .= $_lang_activatesimcard_agreementduration;
                        $_table .= '</th>';
                        $_table .= '<td>';
                        $_table .= $_lang_period_option[$ao->pc_contract_period_code];
                        $_table .= '</td>';
                    } else {
                        $_table .= '<th colspan="2"></th>';
                    }

                    $_table .= '</tr>';

                    if ($ao->operation == 'activate') {

                        $_table .= '<tr>';
                        $_table .= '<th>';
                        $_table .= $_lang_activatesimcard_paymentprice;
                        $_table .= '</th>';
                        $_table .= '<td>';
                        $_table .= 'USD ' . moneyFormat($ao->pp_subscription_payment);
                        $_table .= '</td>';

                        if ($_hascustomerplan) {
                            $_table .= '<th>';
                            $_table .= $_lang_activatesimcard_paymentprice;
                            $_table .= '</th>';
                            $_table .= '<td>';
                            $_table .= 'USD ' . moneyFormat($ao->pc_subscription_payment);
                            $_table .= '</td>';
                        } else {
                            $_table .= '<th colspan="2"></th>';
                        }

                        $_table .= '</tr>';



                        $_table .= '<tr>';
                        $_table .= '<th>';
                        $_table .= $_lang_activatesimcard_fixedprice;
                        $_table .= '</th>';
                        $_table .= '<td>';
                        $_table .= 'USD ' . moneyFormat($ao->pp_fixed_payment);
                        $_table .= '</td>';

                        if ($_hascustomerplan) {
                            $_table .= '<th>';
                            $_table .= $_lang_activatesimcard_fixedprice;
                            $_table .= '</th>';
                            $_table .= '<td>';
                            $_table .= 'USD ' . moneyFormat($ao->pc_fixed_payment);
                            $_table .= '</td>';
                        } else {
                            $_table .= '<th colspan="2"></th>';
                        }

                        $_table .= '</tr>';



                        $_table .= '<tr>';
                        $_table .= '<th>';
                        $_table .= $_lang_activatesimcard_activationcharges;
                        $_table .= '</th>';
                        $_table .= '<td>';
                        $_table .= 'USD ' . moneyFormat($ao->pp_activation_payment);
                        $_table .= '</td>';

                        if ($_hascustomerplan) {
                            $_table .= '<th>';
                            $_table .= $_lang_activatesimcard_activationcharges;
                            $_table .= '</th>';
                            $_table .= '<td>';
                            $_table .= 'USD ' . moneyFormat($ao->pc_activation_payment);
                            $_table .= '</td>';
                        } else {
                            $_table .= '<th colspan="2"></th>';
                        }

                        $_table .= '</tr>';
                    }



                    if ($ao->operation == 'suspend') {

                        $_table .= '<tr>';
                        $_table .= '<th>';
                        $_table .= $_lang_activatesimcard_suspensionprice;
                        $_table .= '</th>';
                        $_table .= '<td>';
                        $_table .= 'USD ' . moneyFormat($ao->pp_suspension_payment);
                        $_table .= '</td>';

                        if ($_hascustomerplan) {
                            $_table .= '<th>';
                            $_table .= $_lang_activatesimcard_suspensionprice;
                            $_table .= '</th>';
                            $_table .= '<td>';
                            $_table .= 'USD ' . moneyFormat($ao->pc_suspension_payment);
                            $_table .= '</td>';
                        } else {
                            $_table .= '<th colspan="2"></th>';
                        }

                        $_table .= '</tr>';
                    }


                    if ($ao->operation == 'deactivate') {

                        $_table .= '<tr>';
                        $_table .= '<th>';
                        $_table .= $_lang_activatesimcard_deactivationprice;
                        $_table .= '</th>';
                        $_table .= '<td>';
                        $_table .= 'USD ' . moneyFormat($ao->pp_deactivation_payment);
                        $_table .= '</td>';

                        if ($_hascustomerplan) {
                            $_table .= '<th>';
                            $_table .= $_lang_activatesimcard_deactivationprice;
                            $_table .= '</th>';
                            $_table .= '<td>';
                            $_table .= 'USD ' . moneyFormat($ao->pc_deactivation_payment);
                            $_table .= '</td>';
                        } else {
                            $_table .= '<th colspan="2"></th>';
                        }

                        $_table .= '</tr>';
                    }
                    echo $_table;
                    ?>



                                                            </tbody>
                                                        </table>                                                                



                                                    </div>
                                                </div>                                        
                                                <?php
                                            }
                                        }
                                        ?>


                                        <!--- -->
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        if ($this->entity->operationrequested == "airtime_cart" || $this->entity->operationrequested == "airtime_order") {

                                            if ($this->entity->operationrequested == "airtime_cart") {
                                                echo '<p>';
                                                echo '<a href="/airtimeoperation/mycart" class="btn btn-xs btn-green"  > <i class="fa fa-shopping-cart"></i>  ' . $_lang_btn_message_airtimecart . '</a>';
                                                echo '</p>';
                                                $_haspendingorder = true;
                                            } else if ($this->entity->operationrequested == "airtime_order") {
                                                echo '<p>';
                                                echo '<a href="/airtimeoperation/searchairtimeorder/iserial/' . $this->entity->iserial . '" class="btn btn-xs btn-primary"  > <i class="fa fa-plane"></i>  ' . $_lang_btn_message_pendingorder . '</a>';
                                                echo '</p>';
                                                $_haspendingorder = true;
                                            }
                                        }

                                        if(!$_haspendingorder){
                                            
                                            if($this->entity->operationregistered == "activate"){
                                                
                                                require 'simcard_suspend.php';
                                                require 'simcard_deactivate.php';
                                                
                                            }
                                            if($this->entity->operationregistered == "suspend"){
                                                
                                                require 'simcard_deactivate.php';
                                                
                                            }
                                            
                                        }
                                        ?>   
                                    </div>    
                                </div>

                                <?php
                            } else {
                                require 'simcard_activate.php';
                            }//else
                        }//if
                        //}                    
                        ?>                   



                    </div>
    <?php
}
?>


            </div>

        </div>    




    </div>
</div>
<!-- end: PAGE CONTENT-->
