<script type="text/javascript">
    var _lang_activatesimcard_traffic = '<?php echo $_lang_activatesimcard_traffic; ?> ';
    var _lang_activatesimcard_qtyperperiod = '<?php echo $_lang_activatesimcard_qtyperperiod; ?> ';
    var _lang_activatesimcard_additionalprice = '<?php echo $_lang_activatesimcard_additionalprice; ?> ';
    var _lang_activatesimcard_roundedto = '<?php echo $_lang_activatesimcard_roundedto; ?> ';
    var _lang_activatesimcard_roundedcode = '<?php echo $_lang_activatesimcard_roundedcode; ?> ';

</script>


<!-- register update -->
<div class="modal fade" id="requestserviceoperation-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $_lang_activatesimcard_modaltitle; ?></h4>
            </div>


            <form role="form" class="form-user  box-user-register" id="form-requestoperation"  >

                <div class="modal-body">
                    
                    <div id="messageProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_activatesimcard_total; ?>
                    </div>
                    
                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                        <tbody>
                            <tr>
                                <td  rowspan="3" class="header_product_image" width="10%">  </td>
                                <th ><?php echo $_lang_index_label_iserial; ?></th>
                                <td class="device_iserial"></td>
                            </tr>
                            <tr> 
                                <th ><?php echo $_lang_index_label_sku; ?></th>
                                <td class="device_sku"></td>
                            </tr>
                            <tr> 
                                <th ><?php echo $_lang_index_label_productname; ?></th>
                                <td class="device_productname"></td>
                            </tr>

                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_planname; ?></th><td class="modal_planname"><?php echo $this->airtimeoperation->planname; ?></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_plantype; ?></th><td class="modal_plantype"><?php echo $this->airtimeoperation->servicetype_code; ?></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_agreementduration; ?> </th><td class="modal_contractperiod"><?php echo $this->airtimeoperation->contract_period; ?></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_paymentperiod; ?></th><td class="modal_subscriptionperiod"><?php echo $this->airtimeoperation->subscription_period; ?></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_paymentprice; ?></th><td class="modal_subscriptionpayment"><?php echo moneyFormat($this->airtimeoperation->subscription_price); ?></td></tr>
                            
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_suspensionprice; ?></th><td class="modal_suspensionpayment"><?php echo moneyFormat($this->plan->suspensionpayment); ?></td></tr>
                            
                             <tr><td colspan="3"><a href="#" class="btn btn-xs btn-default"><?php echo $_lang_activatesimcard_termsinfo; ?></a></td></tr>
                         
                        
                            <tr><th colspan="2"><?php echo $_lang_activatesimcard_activationdate; ?></th>
                                <td>

                                    <div class="input-group" id="picker-container">
                                        <input  name="operationdate" id="modal_operationdate" value="<?php echo date("Y-m-d"); ?>"   type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    </div>
                                </td> 
                            </tr>
                            <tr>
                                <th colspan="2"><?php echo $_lang_activatesimcard_activationtime; ?></th>
                                <td>
                                    <div class="input-group input-append bootstrap-timepicker">
                                        <input  name="operationtime" id="modal_operationtime" type="text" class="form-control time-picker">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </td>
                            </tr>
                            
                        </tbody>

                    </table>


                </div>

                <div class="modal-footer">
                    <input type="hidden" name="airtimeoperationid"  id="airtimeoperationid" value="<?php echo $this->airtimeoperation->id ?>" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_activatesimcard_closemodal; ?>
                    </button>
                    <button type="button" id="btnRequestSuspend" class="btn btn-primary">
                        <?php echo $_lang_activatesimcard_acceptandconfirm; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_simcardsuspend; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                    <tbody>
                        <tr>
                            <td><?php echo $_lang_lbl_suspend_message; ?></td>
                            

                            <td width="10%"><a href="#" id="btn_modalsuspend" class="  btn  btn-orange"><i class="fa fa-unplug"></i>&nbsp;<?php echo $_lang_lbl_suspend_btn; ?></a></td>

                        </tr>

             

                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>