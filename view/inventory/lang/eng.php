<?php

//-- Catalog Index
$_lang_index_label_module = "Inventory";
$_lang_index_label_action = "Detail";
$_lang_index_label_action_mydevices = "My Devices";
$_lang_index_label_action_mysimcards = "My Simcards";
$_lang_index_label_packinglist = "Packing List";
$_lang_index_label_simcardnumber = "Simcard #";


$_lang_table_toarrive = "Pending to arrive";
$_lang_table_oninventory = "On Inventory";
$_lang_table_oncustomer = "Sale/Rent";
$_lang_table_other = "Other";

$_lang_index_label_goback = "Go Back";

$_lang_index_label_iserial = "Serial";
$_lang_index_label_sku = "Sku";
$_lang_index_label_productname = "Product";
$_lang_index_label_productprice = "Price";
$_lang_index_label_documentno = "Invoice #";
$_lang_index_label_datereceived = "Date Received";

$_lang_index_label_simcardactivate = "Activate Simcard";
$_lang_activatesimcard_aviableplans = "Avaible Plans";
$_lang_activatesimcard_chooseplan = "Please choose a plan";

$_lang_activatesimcard_planintro = "Intro";
$_lang_activatesimcard_plandescription = "Description";

$_lang_activatesimcard_planname = "Plan name";
$_lang_activatesimcard_plantype = "Plan type";
$_lang_activatesimcard_paymentperiod = "Subscription period";
$_lang_activatesimcard_paymentprice = "Subscription price";
$_lang_activatesimcard_activationcharges = "Activation Charges";
$_lang_activatesimcard_fixedprice = "Fixed Charges";
$_lang_activatesimcard_agreementduration = "Duration Agreement";
$_lang_activatesimcard_trafficdetail = "Traffic Detail";

$_lang_activatesimcard_traffic = "Traffic";
$_lang_activatesimcard_qtyperperiod = "Qty per period";
$_lang_activatesimcard_additionalprice = "Additional price";
$_lang_activatesimcard_roundedto = "Rounded to";
$_lang_activatesimcard_roundedcode = "Rounded code";

$_lang_activatesimcard_activatewiththisplan = "Activate with this plan";

$_lang_activatesimcard_modaltitle = "Request of operation";
$_lang_activatesimcard_operationactivate = "Activate Simcard";

$_lang_activatesimcard_closemodal = "Close";
$_lang_activatesimcard_termsinfo = "Terms and Conditions to activate with this plan";
$_lang_activatesimcard_acceptandconfirm = "Accept the terms and conditions and Confirm the operation";
$_lang_activatesimcard_activationdate = "Execution date";
$_lang_activatesimcard_accountassigned = "Account assigned";

$_lang_activatesimcard_subtotal = "Subtotal";
$_lang_activatesimcard_creditcard = "Payment with Credit Card";
$_lang_activatesimcard_wiretransfer = "Payment with Wire Transfer";
$_lang_activatesimcard_taxes = "Taxes";
$_lang_activatesimcard_total = "Total to pay";


$_lang_view_label_voicenumber = "Voice number";
$_lang_view_label_datanumber = "Data number";
$_lang_view_label_faxnumber = "Fax number";
$_lang_view_label_imsi = "Imsi";


$_lang_view_label_puk1 = "PUK 1";
$_lang_view_label_puk2 = "PUK 2";
$_lang_view_label_pin = "PIN";

$_lang_index_label_status = "Status";

$_lang_index_label_simcardsuspend = "Suspend";

$_lang_lbl_suspend_message = "Please click the button \"Suspend\" to aplicate this operation";
$_lang_lbl_suspend_btn = "Suspend now";

$_lang_activatesimcard_activationtime = "Execution time";

$_lang_activatesimcard_suspensionprice = "Suspension charges";
$_lang_activatesimcard_deactivationprice = "Deactivation charges";

$_lang_deactivatesimcard_modaltitle = "Deactivate";
$_lang_index_label_simcarddeactivate = "Deactivate";
$_lang_lbl_deactivate_message = "If th simcard is deactivate, all the pending fees, charges and subscriptions fees related to this simcard will be charged";
$_lang_lbl_deactivate_btn = "Deactivate";
$_lang_activatesimcard_deactivationprice = "Deactivation charges(does not include pending charges)";

$_lang_panel_notinventory = "Pending to arrive";
$_lang_panel_inventory = "On Inventory";
$_lang_panel_activated = "Activated";
$_lang_panel_suspend = "Suspend";
$_lang_panel_deactivated = "Deactivated";


$_lang_index_label_oninventory = "On Inventory";

$_lang_activatesimcard_planforcustomer = "Apply this plan for account";


$_lang_modal_soi_title = "Inventory";
$_lang_modal_soi_date  = "Enter date";
$_lang_modal_soi_button = "Enter inventory";
$_lang_modal_rfi_button = "Remove from inventory";
$_lang_modal_soi_close = "Close";

$_lang_inventory_no = "No";
$_lang_inventory_yes = "Yes";
$_lang_inventory_registerentrance = "Register inventory";
$_lang_inventory_applydispatch = "Register Dispatch";

$_lang_modal_soi_description = "Observations";

$_lang_inventory_registermovement = "Register movement";

$_lang_inventory_info_detail ="Detail";
$_lang_inventory_info_movement ="Movement";

$_lang_inventory_status_op[0] = "Adquired";
$_lang_inventory_status_op[1] = "On inventory";
$_lang_inventory_status_op[2] = "On Sale/Rent";
$_lang_inventory_status_op[3] = "On RMA";
$_lang_inventory_status_op[4] = "Demo";
$_lang_inventory_status_op[5] = "Borrow";
$_lang_inventory_status_op[6] = "Other";

$_lang_modal_soo_date = "Movement date";
$_lang_modal_soo_type = "Movement type";
$_lang_modal_soo_button ="Register movement";

$_lang_index_label_invoicedate = "Invoice Date";
$_lang_index_label_dispatchdate = "Dispatch Date";
 
$_lang_index_label_providerplan = "Provider Plan";
$_lang_index_label_customerplan = "Customer Plan";
$_lang_index_label_customername= "Customer name";
$_lang_index_label_customerinvoice= "Customer Invoice"; 

$_lang_index_label_cinvoicedate= "C. Invoice Date"; 
$_lang_index_label_cinvoiceduedate= "C. Due Date"; 
$_lang_index_label_cinvoicedispacth= "C. Dispatch"; 

$_lang_index_label_processingdate= "Processing date";

$_lang_index_label_user= "User";
$_lang_invoice_buy = "Buy";
$_lang_invoice_dispatch = "Dispatch";
$_lang_panel_simcardinfo = "Simcard Info";

$_lang_table_airtimeoperation= "Airtime Operation";
$_lang_table_airtimeoperation_norecords= "No records";

$_lang_table_label_ordercode= "Order #";
$_lang_table_label_operation = "Operation";
$_lang_table_label_requestdatetime = "Request datetime";
$_lang_table_label_operationdatetime = "Operation datetime";

$_lang_table_label_providerplan = "Provider plan";
$_lang_table_label_customerplan = "Customer plan";

$_lang_btn_message_airtimecart = 'This item is your airtime cart';
$_lang_btn_message_pendingorder = 'This item is an pending order';

$_lang_panel_pendingtoactivate = "Pending to activate";