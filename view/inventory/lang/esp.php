<?php

//-- Catalog Index
$_lang_index_label_module = "Inventario";
$_lang_index_label_action = "Detalle";
$_lang_index_label_action_mydevices = "Mis Equipos";
$_lang_index_label_action_mysimcards = "Mis Simcards";
$_lang_index_label_packinglist = "Packing list";
$_lang_index_label_simcardnumber = "Simcard #";


$_lang_table_toarrive = "Pendientes de arrivo";
$_lang_table_oninventory = "En inventario";
$_lang_table_oncustomer = "Venta/Renta";
$_lang_table_other = "Otros";


$_lang_index_label_goback = "Volver";

$_lang_index_label_iserial = "Serie";
$_lang_index_label_sku = "Sku";
$_lang_index_label_productname = "Producto";
$_lang_index_label_productprice = "Precio";
$_lang_index_label_documentno = "Factura proveedor #";
$_lang_index_label_datereceived = "Fecha de ingreso";

$_lang_index_label_simcardactivate = "Activate Simcard";
$_lang_activatesimcard_aviableplans = "Avaible Plans";
$_lang_activatesimcard_chooseplan = "Please choose a plan";

$_lang_activatesimcard_planintro = "Intro";
$_lang_activatesimcard_plandescription = "Description";

$_lang_activatesimcard_planname = "Nombre del plan";
$_lang_activatesimcard_plantype = "Tipo del plan";
$_lang_activatesimcard_paymentperiod = "Periodo de subscripci&oacute;n";
$_lang_activatesimcard_paymentprice = "Precio de subscripci&oacute;n";
$_lang_activatesimcard_activationcharges = "Cargos por activaci&oacute;n";
$_lang_activatesimcard_fixedprice = "Cargos fijos";
$_lang_activatesimcard_agreementduration = "Duraci&oacute;n del acuerdo";
$_lang_activatesimcard_trafficdetail = "Detalle del tr&aacute;fico";

$_lang_activatesimcard_traffic = "Tr&aacute;fico";
$_lang_activatesimcard_qtyperperiod = "Cantidad por periodo";
$_lang_activatesimcard_additionalprice = "Precio adicional";
$_lang_activatesimcard_roundedto = "Redondeado a";
$_lang_activatesimcard_roundedcode = "C&oacute;digo de redondeo";

$_lang_activatesimcard_activatewiththisplan = "Activar con este plan";

$_lang_activatesimcard_modaltitle = "Solicitar operaci&oacute;n";
$_lang_activatesimcard_operationactivate = "Activar Simcard";

$_lang_activatesimcard_closemodal = "Cerrar";
$_lang_activatesimcard_termsinfo = "T&eacute;rminos y condiciones para activar con este plan";
$_lang_activatesimcard_acceptandconfirm = "Aceptar los t&eacute;rminos y condiciones y ejecutar esta operaci&oacute;n";
$_lang_activatesimcard_activationdate = "Fecha de ejecuci&oacute;n";
$_lang_activatesimcard_accountassigned = "Cuenta asignada";

$_lang_activatesimcard_subtotal = "Subtotal";
$_lang_activatesimcard_creditcard = "Pagar con tarjeta de cr&eacute;dito";
$_lang_activatesimcard_wiretransfer = "Pagar con transferencia bancaria";
$_lang_activatesimcard_taxes = "Impuestos";
$_lang_activatesimcard_total = "Total a pagar";


$_lang_view_label_voicenumber = "N&uacute;mero de voz";
$_lang_view_label_datanumber = "N&uacute;mero de datos";
$_lang_view_label_puk1 = "PUK 1";
$_lang_view_label_puk2 = "PUK 2";
$_lang_view_label_pin = "PIN";
$_lang_index_label_status = "Estado";


$_lang_index_label_simcardsuspend = "Suspender";

$_lang_lbl_suspend_message = "Por favor haga clic en el bot&oacute;n \"Suspender\" para aplicar esta operaci&oacute;n";
$_lang_lbl_suspend_btn = "Suspender";

$_lang_activatesimcard_activationtime = "Hora de ejecuci&oacute;n";

$_lang_activatesimcard_suspensionprice = "Cargos por suspensi&oacute;n";
$_lang_activatesimcard_deactivationprice = "Cargos por desactivaci&oacute;n";

$_lang_deactivatesimcard_modaltitle = "Desactivar";
$_lang_index_label_simcarddeactivate = "Desactivar";
$_lang_lbl_deactivate_message = "Si el simcard es desactivado, todos los cargos, subscripciones pendientes y pagos relacionados al simcard serán cargados en la siguiene factura";
$_lang_lbl_deactivate_btn = "Desactivar";
$_lang_activatesimcard_deactivationprice = "Cargos por desactivaci&oacute;n (no incluye cargos pendientes)"; 


$_lang_panel_notinventory = "Pendiente de arrivo";
$_lang_panel_inventory= "Inventario";
$_lang_panel_activated = "Activados";
$_lang_panel_suspend = "Suspendidos";
$_lang_panel_deactivated = "Desactivados";

$_lang_index_label_oninventory = "En inventario";

$_lang_activatesimcard_planforcustomer = "Aplicar el siguiente plan a la cuenta";


$_lang_modal_soi_title = "Inventario";
$_lang_modal_soi_date  = "Fecha de ingreso";
$_lang_modal_soi_button = "Ingresar a inventario";
$_lang_modal_rfi_button = "Retirar del inventario";
$_lang_modal_soi_close = "Cerrar";

$_lang_inventory_no = "No";
$_lang_inventory_yes = "Si";
$_lang_inventory_registerentrance = "Registrar ingreso";
$_lang_inventory_applydispatch = "Registrar salida";

$_lang_modal_soi_description = "Observaciones";

$_lang_inventory_registermovement = "Registrar movimiento";

$_lang_inventory_info_detail ="Detalle";
$_lang_inventory_info_movement ="Movimientos";

$_lang_inventory_status_op[0] = "Adquirido";
$_lang_inventory_status_op[1] = "En inventario";
$_lang_inventory_status_op[2] = "Salida";
$_lang_inventory_status_op[3] = "En RMA";
$_lang_inventory_status_op[4] = "Demo";
$_lang_inventory_status_op[5] = "Pr&eacute;stamo";
$_lang_inventory_status_op[6] = "Otro";


$_lang_modal_soo_date = "Fecha del movimiento";
$_lang_modal_soo_type = "Tipo de movimiento";
$_lang_modal_soo_button ="Registrar movimiento";

$_lang_index_label_invoicedate = "Fecha Factura";
$_lang_index_label_dispatchdate = "Fecha Despacho";

$_lang_index_label_providerplan = "Proveedor Plan";
$_lang_index_label_customerplan = "Cliente Plan";

$_lang_index_label_customername= "Cliente";
$_lang_index_label_customerinvoice= "Customer Invoice"; 

$_lang_index_label_cinvoicedate= "C. Factura fecha"; 
$_lang_index_label_cinvoiceduedate= "C. Factura Venc."; 
$_lang_index_label_cinvoicedispacth= "C. Despacho"; 

$_lang_index_label_processingdate= "Fecha de procesamiento";

$_lang_index_label_user= "Usuario";
$_lang_invoice_buy = "Compra";
$_lang_invoice_dispatch = "Despacho";
$_lang_panel_simcardinfo = "Simcard Info";


$_lang_table_airtimeoperation= "Operaci&oacute;n de Tiempo de Aire";
$_lang_table_airtimeoperation_norecords= "Sin registros";


$_lang_table_label_ordercode= "Orden #";
$_lang_table_label_operation = "Operaci&oacute;n";
$_lang_table_label_requestdatetime = "Fecha de solicitud";
$_lang_table_label_operationdatetime = "Fecha de ejecuci&oacute;n";

$_lang_table_label_providerplan = "Plan proveedor";
$_lang_table_label_customerplan = "Plan cliente";

$_lang_btn_message_airtimecart = 'Este item se encuentra en su carro de tiempo de aire';
$_lang_btn_message_pendingorder = 'Este item se encuentra en una orden de tiempo de aire pendiente';

$_lang_panel_pendingtoactivate = "Pendiente de activaci&oacute;n";