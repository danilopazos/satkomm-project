<script type="text/javascript">
    var _lang_activatesimcard_traffic = '<?php echo $_lang_activatesimcard_traffic; ?> ';
    var _lang_activatesimcard_qtyperperiod = '<?php echo $_lang_activatesimcard_qtyperperiod; ?> ';
    var _lang_activatesimcard_additionalprice = '<?php echo $_lang_activatesimcard_additionalprice; ?> ';
    var _lang_activatesimcard_roundedto = '<?php echo $_lang_activatesimcard_roundedto; ?> ';
    var _lang_activatesimcard_roundedcode = '<?php echo $_lang_activatesimcard_roundedcode; ?> ';

</script>


<!-- register update -->
<div class="modal fade" id="requestserviceoperation-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $_lang_activatesimcard_modaltitle; ?></h4>
            </div>


            <form role="form" class="form-user  box-user-register" id="form-requestoperation"  >

                <div class="modal-body">

                    <div id="messageProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_activatesimcard_total; ?>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                        <tbody>
                          
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_planname; ?></th><td class="modal_planname"></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_plantype; ?></th><td class="modal_plantype"></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_agreementduration; ?> </th><td class="modal_contractperiod"></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_paymentperiod; ?></th><td class="modal_subscriptionperiod"></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_paymentprice; ?></th><td class="modal_subscriptionpayment"></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_activationcharges; ?></th><td class="modal_activationcharges"></td></tr>
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_fixedprice; ?></th><td class="modal_fixedcharges"></td></tr>
                            <tr><td colspan="3"><a href="#" class="btn btn-xs btn-default"><?php echo $_lang_activatesimcard_termsinfo; ?></a></td></tr>

                            <tr><th colspan="2"><?php echo $_lang_activatesimcard_accountassigned; ?></th>
                                <td>
                                    <select name="accountassigned" id="accountassigned" class="form-control">
                                        <?php
                                        foreach ($this->customerAccounts as $entity) {
                                            echo '<option value="' . $entity->id . '">' . $entity->name . '</option>';
                                        }
                                        ?>
                                    </select>

                                </td> 
                            </tr>

                            <tr><th colspan="2"><?php echo $_lang_activatesimcard_planforcustomer; ?></th>
                                <td>
                                    <select name="planforaccount" id="planforaccount" class="form-control"></select>

                                </td> 
                            </tr>
                            
                            
                            
                            <tr><th colspan="2"><?php echo $_lang_activatesimcard_activationdate; ?></th>
                                <td>

                                    <div class="input-group" id="picker-container">
                                        <input  name="operationdate" id="modal_operationdate" value="<?php echo date("Y-m-d"); ?>"   type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    </div>
                                </td> 
                            </tr>
                            <tr>
                                <th colspan="2"><?php echo $_lang_activatesimcard_activationtime; ?></th>
                                <td>
                                    <div class="input-group input-append bootstrap-timepicker">
                                        <input  name="operationtime" id="modal_operationtime" type="text" class="form-control time-picker">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </td>
                            </tr>


                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_subtotal; ?></th><td><span id="modal_subtotal"></span></td></tr>
                            <tr style="display:none;"> <th  colspan="2"><?php echo $_lang_activatesimcard_taxes; ?></th><td id="modal_taxes"></td></tr>
                            <tr style="display:none;"> 
                                <th  colspan="2">
                                    <select name="paymentmethod" id="modal_select_paymentmethod">
                                        <option value="creditcard"><?php echo $_lang_activatesimcard_creditcard; ?></option>
                                        <option value="wiretransfer"><?php echo $_lang_activatesimcard_wiretransfer; ?></option>
                                    </select>
                                    <input type="hidden" name="planid" id="operation_planid"    />
                                    <input type="hidden" name="inventoryid" id="operation_inventoryid"    value="<?php echo $this->id; ?>" /> 
                                    <input type="hidden" name="accountid" id="operation_accountid"  value="<?php echo $this->accountid; ?>" />
                                    <input type="hidden" name="subtotal" id="operation_subtotal" />
                                    <input type="hidden" name="paymentmethod_price" id="operation_paymentmethod_price" />
                                    <input type="hidden" name="taxes" id="operation_taxes" />
                                    <input type="hidden" name="totaltopay" id="operation_totaltopay" />
                                </th>
                                <td id="modal_paymentmethod_price"></td>
                            </tr>
                            <tr style="display:none;"> <th  colspan="2"><?php echo $_lang_activatesimcard_total; ?></th><td id="modal_totaltopay"></td></tr>


                        </tbody>

                    </table>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_activatesimcard_closemodal; ?>
                    </button>
                    <button type="button" id="btnRequestOperation" class="btn btn-primary">
                        <?php echo $_lang_activatesimcard_acceptandconfirm; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_simcardactivate; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                    <tbody>
                        <tr>
                            <th width="20%"> <?php echo $_lang_activatesimcard_aviableplans; ?> </th>
                            <td colspan="2"> 
                                <select name="planid" id="planid" class="form-control "  > 
                                    <option value="0"><?php echo $_lang_activatesimcard_chooseplan; ?></option>
                                    <?php
                                    if ($this->aviableplans) {
                                        foreach ($this->aviableplans as $plan) {
                                            echo '<option value="' . $plan->idcrypt . '">' . $plan->name . '</option>';
                                        }
                                    }
                                    ?>
                                </select>  
                            </td>

                            <td><a href="#" id="btn_activatewithplan" class="container_simcard btn  btn-green"><i class="fa fa-plug"></i>&nbsp;<?php echo $_lang_activatesimcard_activatewiththisplan; ?></a></td>

                        </tr>

                        <tr  class="container_simcard">
                            <th width="20%"><?php echo $_lang_activatesimcard_planname; ?> </th>
                            <td  > <span id="planname"></span></td>
                            <th width="20%"> <?php echo $_lang_activatesimcard_plantype; ?> </th>
                            <td><span id="plantype"></span></td>
                        </tr>

                        <tr  class="container_simcard  container_postpaid">
                            <th width="20%">    <?php echo $_lang_activatesimcard_paymentperiod; ?> </th>
                            <td><span id="paymentperiod"></span></td>
                            <th width="20%">  <?php echo $_lang_activatesimcard_paymentprice; ?> </th>
                            <td><span id="paymentprice"></span></td>
                        </tr>


                        <tr  class="container_simcard  container_postpaid">
                            <th width="20%"> <?php echo $_lang_activatesimcard_agreementduration; ?> </th>
                            <td ><span id="agreementduration"></span></td>

                            <th width="20%"><?php echo $_lang_activatesimcard_fixedprice; ?> </th>
                            <td><span id="fixedprice"></span></td>
                        </tr>

                        <tr class="container_simcard">
                            <th width="20%"><?php echo $_lang_activatesimcard_activationcharges; ?> </th>
                            <td  colspan="3" ><span id="activationcharges"></span></td>
                        </tr>

                        <tr class="container_simcard">
                            <th width="20%"><?php echo $_lang_activatesimcard_trafficdetail; ?></th>
                            <td  colspan="3"><span id="trafficdetail"></span></td>
                        </tr>

                        <tr id=" containter_planintro" class="container_simcard">
                            <th width="20%"><?php echo $_lang_activatesimcard_planintro; ?></th>
                            <td  colspan="3"><span id="planintro"></span></td>
                        </tr>
                        <tr  id=" containter_plandescription"  class="container_simcard">
                            <th width="20%"><?php echo $_lang_activatesimcard_plandescription; ?></th>
                            <td  colspan="3"><span id="plandescription"></span></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>