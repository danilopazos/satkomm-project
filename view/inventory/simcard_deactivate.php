<script type="text/javascript">
    var _lang_activatesimcard_traffic = '<?php echo $_lang_activatesimcard_traffic; ?> ';
    var _lang_activatesimcard_qtyperperiod = '<?php echo $_lang_activatesimcard_qtyperperiod; ?> ';
    var _lang_activatesimcard_additionalprice = '<?php echo $_lang_activatesimcard_additionalprice; ?> ';
    var _lang_activatesimcard_roundedto = '<?php echo $_lang_activatesimcard_roundedto; ?> ';
    var _lang_activatesimcard_roundedcode = '<?php echo $_lang_activatesimcard_roundedcode; ?> ';

</script>


<!-- register update -->
<div class="modal fade" id="requestserviceoperation-deactivate-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $_lang_deactivatesimcard_modaltitle; ?></h4>
            </div>


            <form role="form" class="form-user  box-user-register" id="form-requestoperation-deactivate"  >

                <div class="modal-body">
                    
                    <div id="messageDeactivateOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_deactivatesimcard_total; ?>
                    </div>
                    
                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                        <tbody>
                   
                            <tr> <th  colspan="2"><?php echo $_lang_activatesimcard_deactivationprice; ?></th><td class="modal_deactivationpayment"><?php echo 'USD '.moneyFormat($this->plan->deactivationpayment); ?></td></tr>
                          
                            <tr><th colspan="2"><?php echo $_lang_activatesimcard_activationdate; ?></th>
                                <td>

                                    <div class="input-group" id="picker-container">
                                        <input  name="operationdate" id="modal_operationdate" value="<?php echo date("Y-m-d"); ?>"   type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    </div>
                                </td> 
                            </tr>
                            <tr>
                                <th colspan="2"><?php echo $_lang_activatesimcard_activationtime; ?></th>
                                <td>
                                    <div class="input-group input-append bootstrap-timepicker">
                                        <input  name="operationtime" id="modal_operationtime" type="text" class="form-control time-picker">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </td>
                            </tr>
                            
                        </tbody>

                    </table>


                </div>

                <div class="modal-footer">
                    <input type="hidden" name="airtimeoperationid"  id="airtimeoperationid" value="<?php echo $this->airtimeoperation->id ?>" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_activatesimcard_closemodal; ?>
                    </button>
                    <button type="button" id="btnRequestDeactivate" class="btn btn-primary">
                        <?php echo $_lang_activatesimcard_acceptandconfirm; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_simcarddeactivate; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                    <tbody>
                        <tr>
                            <td><?php echo $_lang_lbl_deactivate_message; ?></td>
                            

                            <td  width="10%"><a href="#" id="btn_modaldeactivate" class="  btn  btn-orange"><i class="fa fa-unplug"></i>&nbsp;<?php echo $_lang_lbl_deactivate_btn; ?></a></td>

                        </tr>

             

                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>