<script type="text/javascript">
    _var_accountid = <?php echo $this->accountid; ?>
</script>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dropbox"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $this->entity->iserial; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable"> 
                    <thead>
                        <tr>
                            <th colspan="2"><?php echo $_lang_inventory_info_detail; ?></th>
                            <th><?php echo $_lang_inventory_info_movement; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="10%"> <?php echo $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $this->entity->mediaid . '/type/thumb"  >'; ?> </td>
                            <td width="20%"> 
                                <?php
                                echo '<strong>' . $_lang_index_label_iserial . '</strong>' . ': ' . $this->entity->iserial . '<br>';
                                echo '<strong>' . $_lang_index_label_sku . '</strong>' . ': ' . $this->entity->sku . '<br>';
                                echo '<strong>' . $_lang_index_label_productname . '</strong>' . ': ' . $this->entity->productname . '<br>';
                                echo '<strong>' . $_lang_index_label_documentno . '</strong>' . ': <a class="btn btn-xs btn-default" href="/invoice/view/code/' . $this->entity->documentcode . '">' . $this->entity->documentcode . '</a><br>';
                                echo '<strong>' . $_lang_index_label_productprice . '</strong>' . ': ' . moneyFormat($this->entity->productprice) . '<br>';
                                echo '<strong>' . $_lang_index_label_datereceived . '</strong>' . ': ' . $this->entity->datereceived . '<br>';
                                ?>
                            </td>
                            <td >
                            <?php 
                            
                                
                                $totalHistory = count($this->inventoryHistory);
                                if($totalHistory > 0){
                                    echo '<table  class="table table-striped table-bordered table-hover table-full-width" >';
                                    foreach($this->inventoryHistory as $e){
                                        echo '<tr>';
                                        echo '<td>'.$e->statusdate.'</td>';
                                        echo '<td>'.$_lang_inventory_status_op[$e->inventorystatus].'</td>';
                                        echo '<td>'.$e->statusdescription.'</td>';
                                        
                                        if($e->customerinvoiceidcrypt != ""){
                                            $invoicelink = '<a target="_blank" href="/invoicetc/view/id/'.$e->customerinvoiceidcrypt.'" class="btn btn-xs btn-default">'.$e->customerinvoiceno.'</a>';
                                        }else{
                                            $invoicelink = '---';
                                        }
                                        
                                        echo '<td>'.$invoicelink.'</td>';
                                        echo '</tr>';
                                    }
                                    echo '</table>';
                                }
                               
                            ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->
