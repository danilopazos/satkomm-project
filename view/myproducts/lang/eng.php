<?php
//-- Catalog Index
$_lang_index_label_module= "My products";
$_lang_index_label_action = "Search";

$_lang_view_label_customer = "Customer";
$_lang_view_label_customer_all = "All customers";
$_lang_index_table_title = "Product List";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Name";
$_lang_index_table_th_networkcode = "Network";
$_lang_index_table_th_cost = "Product cost";

$_lang_view_label_close = "Close";
$_lang_view_label_addcart= "Save prices";

$_lang_view_block_addcart = "Update prices";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Package";
$_lang_panel_technicalspecs = "Specs";
$_lang_panel_features= "Features";
$_lang_panel_docs = "Docs";

$_lang_view_label_quantity = "Quantity";
$_lang_view_label_cost = "Product cost";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item added";

$_lang_view_label_saleprice = "Sale price";
$_lang_view_label_rentmonthprice = "Rent price monthly";
$_lang_view_label_rentweekprice = "Rent price weekly";

$_lang_view_label_pricesupdated = "Prices updated";

$_lang_view_label_margin = "Margin";

$_lang_view_label_specialprices = "Precio especial";

$_lang_index_specialprice_accountname = "Account";
$_lang_index_specialprice_cost = "Cost";
$_lang_index_specialprice_sale = "Sale";
$_lang_index_specialprice_rent = "Rent";
$_lang_index_specialprice_margin = "Margin";