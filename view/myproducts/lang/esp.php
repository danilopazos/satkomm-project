<?php
//-- Catalog Index
$_lang_index_label_module= "My products";
$_lang_index_label_action = "Buscar";

$_lang_view_label_customer = "Cliente";
$_lang_view_label_customer_all = "Todos los clientes";
$_lang_index_table_title = "Product List";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Nombre";
$_lang_index_table_th_networkcode = "Red";
$_lang_index_table_th_cost = "Costo del producto";

$_lang_view_label_close = "Cerrar";
$_lang_view_label_addcart= "Guardar precios";

$_lang_view_block_addcart = "Actualizar precios";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Paquete";
$_lang_panel_technicalspecs = "Especs";
$_lang_panel_features= "Caracter&iacute;sticas";
$_lang_panel_docs = "Documentos";

$_lang_view_label_quantity = "Cantidad";
$_lang_view_label_cost = "Costo del producto";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item a&ntilde;adido";

$_lang_view_label_saleprice = "Precio de venta";
$_lang_view_label_rentmonthprice = "Precio renta mensual";
$_lang_view_label_rentweekprice = "Precio renta semanal";

$_lang_view_label_pricesupdated = "Precios actualizados";

$_lang_view_label_margin = "Margen";

$_lang_view_label_specialprices = "Precios especiales";

$_lang_index_specialprice_accountname = "Cuenta";
$_lang_index_specialprice_cost = "Costo";
$_lang_index_specialprice_sale = "Venta";
$_lang_index_specialprice_rent = "Renta";
$_lang_index_specialprice_margin = "Margen";