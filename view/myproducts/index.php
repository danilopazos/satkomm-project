<!--- Product Description -->
<div class="modal fade" id="productdetail-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title"  id="panel_producttitle">   </h4>
                        </div>


                        <div class="tabbable">
                            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">

                                <li class="active">
                                    <a data-toggle="tab" href="#panel_intro">
                                        <?php echo $_lang_panel_intro; ?>
                                    </a>
                                </li>

                                <li>
                                    <a data-toggle="tab" href="#panel_description">
                                        <?php echo $_lang_panel_description; ?>
                                    </a>
                                </li> 
                                <li>
                                    <a data-toggle="tab" href="#panel_package">
                                        <?php echo $_lang_panel_package; ?>
                                    </a>
                                </li>   
                                <li>
                                    <a data-toggle="tab" href="#panel_technicalspecs">
                                        <?php echo $_lang_panel_technicalspecs; ?>
                                    </a>
                                </li>   
                                <li>
                                    <a data-toggle="tab" href="#panel_features">
                                        <?php echo $_lang_panel_features; ?>
                                    </a>
                                </li>   
                                <li>
                                    <a data-toggle="tab" href="#panel_docs">
                                        <?php echo $_lang_panel_docs; ?>
                                    </a>
                                </li>   

                            </ul>


                            <div class="tab-content">

                                <div id="panel_intro" class="tab-pane in active">

                                </div>
                                <div id="panel_description" class="tab-pane">

                                </div>
                                <div id="panel_package" class="tab-pane">

                                </div>
                                <div id="panel_technicalspecs" class="tab-pane">

                                </div>
                                <div id="panel_features" class="tab-pane">

                                </div>
                                <div id="panel_docs" class="tab-pane">

                                </div>
                            </div>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 
                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--- Product Modify prices -->
<div class="modal fade" id="addcart-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title"  id="panel_addcart_title">   </h4>
                        </div>

                        <form role="form" class="form-user  box-user-register" id="form-updateprices"  >

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_view_block_addcart; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  

                                    <div id="messagePricesUpdatedOk" class="alert alert-success no-display">
                                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_pricesupdated; ?>
                                    </div>

                                    <div class="form-group" id="customerid_row">
                                        <label class="col-sm-4 control-label" for="cost">
                                            <?php echo $_lang_view_label_customer; ?>  
                                        </label>
                                        <div class="col-sm-8"  >
                                            <select name="customeraccountid" id="customeraccountid" class="form-control">
                                                <option value="0">--- <?php echo $_lang_view_label_customer_all; ?> ---</option>
                                                <?php
                                                $_totalcustomer = intval($this->aCustomer);
                                                if ($_totalcustomer > 0) {

                                                    foreach ($this->aCustomer as $customer) {

                                                        echo '<option value="' . $customer->id . '">' . $customer->name . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="cost">
<?php echo $_lang_view_label_cost; ?> USD
                                        </label>
                                        <div class="col-sm-4"  >
                                            <input type="text"  class="form-control"  id="form-cost"  disabled="true"  value=""    />  
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="saleprice">
<?php echo $_lang_view_label_saleprice; ?> USD
                                        </label>
                                        <div class="col-sm-4"  >
                                            <input type="text"  class="form-control"  id="form-saleprice" name ="buy"  value=""    />  
                                        </div>
                                        <div class="col-sm-4"  >
                                            <input id="switch-state1" name="buyisenabled" type="checkbox" checked>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="rentmonthprice">
<?php echo $_lang_view_label_rentmonthprice; ?> USD
                                        </label>
                                        <div class="col-sm-4"  >
                                            <input type="text"  class="form-control"  id="form-rentmonthprice"  name="rentpermonth"  value=""    />  
                                        </div>
                                        <div class="col-sm-4"  >
                                            <input id="switch-state2" name="rentpermonthisenabled" type="checkbox" checked>
                                        </div>
                                    </div>


                                </div>

                                <input type="hidden" name="idcrypt" id="form-addcart-idcrypt"   />
                                <input type="hidden" name="price"    id="form-addcart-price"  />


                            </div>

                        </form>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
<?php echo $_lang_view_label_close; ?>
                            </button> 

                            <button type="button" id='btn_saveprices' class="btn btn-primary">
                                <i class="fa fa-save"></i>   <?php echo $_lang_view_label_addcart; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--- specialprices-modal  -->
<div class="modal fade" id="specialprices-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title"  id="panel_specialprice_title">   </h4>
            </div>
            
            
            <div class="modal-body">
                
                <table class="table table-striped table-bordered table-hover table-full-width" id="table_specialprices" >
                    <thead>
                        <tr>
                             
                            <th><?php echo $_lang_index_specialprice_accountname; ?></th>
                            <th width="15%"><?php echo $_lang_index_specialprice_cost; ?> </th>
                            <th width="15%"> <?php echo $_lang_index_specialprice_sale; ?></th>
                            <th width="15%"> <?php echo $_lang_index_specialprice_rent; ?></th>
                            <th width="15%"> <?php echo $_lang_index_specialprice_margin; ?></th>  

                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                
                
            </div>

            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">
                <?php echo $_lang_view_label_close; ?>
                </button> 
 
            </div>            
            
            
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                    <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
           
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_table_title; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th width="10%"> </th>
                            <th width="10%" class="hidden-xs"><?php echo $_lang_index_table_th_sku; ?></th>
                            <th><?php echo $_lang_index_table_th_name; ?></th>
                            <th class="hidden-xs"><?php echo $_lang_index_table_th_networkcode; ?> </th>
                            <th width="10%"><?php echo $_lang_index_table_th_cost; ?> </th>
                            <th  width="10%" > <?php echo $_lang_view_label_saleprice; ?></th>
                            <th  width="10%" > <?php echo $_lang_view_label_rentmonthprice; ?></th>
                            <th  width="10%" > <?php echo $_lang_view_label_margin; ?></th> 
                            <th> <?php echo $_lang_view_label_specialprices; ?></th> 

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->
