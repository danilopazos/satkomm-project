
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/cdr/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <strong><?php echo $_lang_index_label_title; ?></strong>
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_action; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <form id="form-report" name="form-report" method="post" action="/cdr/generateCdrTemporary"> 
                    <table class="table ">
                   
                    <tbody>
                        <tr>
                            <th width="20%">
                                Buscar por <br />
                                <select class="form-control" name="wheresearch" id="wheresearch">
                                    <option value="SCRD" <?php if($this->whSearch=="SCRD") echo "selected"; ?>>Sim Card</option>
                                    <option value="IMSI" <?php if($this->whSearch=="IMSI") echo "selected"; ?>>IMSI</option>
                                </select>
                            </th>
                            <th width="30%">
                                Comando de búsqueda <br />
                                <input type="text" name="simcard" id="simcard" class="form-control" autcomplete="off" value="<?php echo $this->commandSearch; ?>"> 
                            </th>
                            <th width="30%">
                                Intervalo de fechas <br />
                                <div class="input-group">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    <input class="form-control date-time-range" name="daterange" id="daterange" type="text" autcomplete="off" value="<?php echo $this->daterange; ?>" readonly>
                                </div>
                            </th>    
                            <th width="20%">
                                Tipo de búsqueda <br />
                                <select class="form-control" name="whatsearch" id="whatsearch">
                                    <option value="CDR" <?php if($this->wtSearch=="CDR") echo "selected"; ?>>CDR Facturado</option>
                                    <option value="NOF" <?php if($this->wtSearch=="NOF") echo "selected"; ?>>CDR Crudo</option>
                                </select>
                            </th>                            
                            
                    
                        </tr>
                        
                        <tr>
                            <td colspan="4">
                                <!-- <a href="#" class="btn btn-primary" id="btnGenerateReport"  ><?php echo $_lang_btn_generatereport; ?></a> -->
                                <button class="btn btn-primary" id="btnGenerateReport" type="submit"><?php echo $_lang_view_startquery; ?></button>                           
                            </td>
                        </tr>
                        <?php if(isset($this->errorMessage)){ ?>
                        <tr>
                            <td colspan="4">
                                <p>
                                    <b style="font-size:14px; color:#E74C3C;"><?php echo $this->errorMessage; ?></b>
                                </p>                      
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                </form>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->


<?php if(isset($this->information)){ ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
        
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th><?php echo $_lang_view_table_date; ?></th>
                            <th><?php echo $_lang_view_table_simcard; ?></th>
                            <th><?php echo $_lang_view_table_subscriber; ?></th>
                            <th><?php echo $_lang_view_table_destination; ?></th>
                            <th><?php echo $_lang_view_table_type; ?></th>
                            <th><?php echo $_lang_view_table_duration; ?></th>
                            <th><?php echo $_lang_view_table_ammount; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $summary_details = array();
                        
                        foreach ($this->information as $item) {
                            $i=0;
                            $index = 0;
                            foreach($summary_details as $detail){
                                $i ++;
                                if($detail == utf8_decode($item['traffictypedesc'])){
                                    $index = $i;
                                }
                            }
                                
                            if($index > 0){
                                $sd_time = ($summary_details[$index][0])+$item['qty'];
                                $sd_ammount = ($summary_details[$index][1])+$item['chargeoutofbundle'];
                                $array_replace = array($index => array(0 => $sd_time,  1 => $sd_ammount));
                                $summary_details = array_replace($summary_details, $array_replace);
                            }else{
                                array_push($summary_details, utf8_decode($item['traffictypedesc']));
                                array_push($summary_details, array(0 => $item['qty'], 1 => $item['chargeoutofbundle']));
                            }
                                
                            ?>
                            <tr>
                                <td><?php echo $item['cdrdate']." ".$item['cdrtime']; ?></td>
                                <td><?php echo $item['serial']; ?></td>
                                <td><?php echo $item['voicenumber']; ?></td>
                                <td><?php echo $item['destinationnumber']; ?></td>
                                <td><?php echo utf8_decode($item['traffictypedesc']); ?></td>
                                <td><?php echo $item['qty']." ".$item['qtyunitcode']; ?></td>
                                <td>USD <?php echo number_format($item['chargeoutofbundle'], 2); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    
    <div class="panel-body">
    <table class="table table-striped table-bordered table-hover table-full-width">
        <tr>
            <td colspan="2"><b><?php echo $_lang_view_summary_title; ?></b></td>
        </tr>
        <tr>
            <td width="25%"><?php echo $_lang_view_summary_range; ?></td>
            <td width="75%"><?php echo $_lang_view_summary_since.$this->startdate.$_lang_view_summary_to.$this->enddate; ?></td>
        </tr>
        <?php $total_ammoumnt=$this->planDetail['price']; ?>
        <tr>
            <td><?php echo $_lang_view_summary_plan.$this->planDetail['name']; ?></td>
            <td>USD <?php echo number_format($this->planDetail['price'], 2); ?></td>
        </tr>
        <?php
        $cant_details = count($summary_details);
        $index=0;
        while($index<$cant_details){
            $total_ammoumnt += $summary_details[$index+1][1];
            ?>
            <tr>
            <td><?php echo $_lang_view_summary_aditional.$summary_details[$index]; ?></td>
            <td>USD <?php echo number_format($summary_details[$index+1][1], 2); ?></td>            
            </tr>
            <?php
            $index = $index +2;
        }
        ?>
        
        <tr>
            <td><?php echo $_lang_view_summary_total; ?></td>
            <td>USD <?php echo number_format($total_ammoumnt ,2); ?></td>
        </tr>
    </table>
                <a href="/cdr/generateExcelReport/?whSearch=<?php echo $this->whSearch; ?>&commandSearch=<?php echo $this->commandSearch; ?>&dateRange=<?php echo $this->daterange; ?>&wtSearch=<?php echo $this->wtSearch; ?>" target="_blank" class="btn btn-primary">
                    Descargar reporte en Excel
                </a>
                <br />&nbsp;
    </div>

    </div>
</div>
<?php }else if(isset($this->consStratosInformation)){
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
        
            <div class="panel-body">
                <p>
                <b style="font-size:14px; color:#E74C3C;"><?php echo $lang_view_importantinfo; ?></b>
                <br />&nbsp;
                </p>
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th><?php echo $_lang_view_table_date; ?></th>
                            <th><?php echo $_lang_view_table_imsi; ?></th>
                            <th><?php echo $_lang_view_table_subscriber; ?></th>
                            <th><?php echo $_lang_view_table_destination; ?></th>
                            <th><?php echo $_lang_view_table_product; ?></th>
                            <th><?php echo $_lang_view_table_type; ?></th>
                            <th><?php echo $_lang_view_table_durationsession; ?></th>
                            <th><?php echo $_lang_view_table_consumption; ?></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $consTotal = array();
                        
                        foreach($this->consStratosInformation as $item){
                            
                            if($item['sessionclosuredate']!="1980-01-01"){
                                $durationText = "";
                                $durationHou = 0;
                                $durationMin = 0;
                                $durationSeg = (strtotime($item['sessionclosuredate']." ".$item['sessionclosuretime']))-(strtotime($item['cdrdate']." ".$item['cdrtime']));
                                while($durationSeg>59){
                                    $durationMin ++;
                                    $durationSeg = $durationSeg - 60;
                                }
                                while ($durationMin>59){
                                    $durationHou ++;
                                    $durationMin = $durationMin - 60;
                                }
                                if($durationHou>0){
                                    $durationText = $durationHou."h ";
                                }
                                if($durationHou>0 or $durationMin>0){
                                    $durationText .= $durationMin."m ";
                                }
                                $durationText .= $durationSeg."s";
                            }else{
                                $durationText = "-";
                            }                            

                        ?>
                            <tr align="center">
                                <td><?php echo $item['cdrdate']." ".$item['cdrtime']; ?></td>
                                <td><?php echo $item['calsig']; ?></td>
                                <td><?php echo $item['numberfrom']; ?></td>
                                <td><?php echo $item['destinationnumber']; ?></td>
                                <td><?php echo $item['product']; ?></td>
                                <td><?php echo $item['calltype']; ?></td>
                                <td align="right"><?php echo $durationText; ?></td>
                                <?php 
                                    if($item['calltype']=='001') {
                                        $increment = $item['numwrdup'];
                                        $secCall = $item['numwrdup'];
                                        $minCall = 0;
                                        $durCall = "";
                                        while($secCall > 59){
                                            $minCall ++;
                                            $secCall = $secCall - 60;
                                        }
                                        if($minCall>0) $durCall = $minCall."m ";
                                        $durCall .= $secCall."s";
                                        echo "<td align='right'>".$durCall."</td>";
                                    }
                                    else if($item['calltype']=='014') {
                                        $increment = $item['numwrdup'];
                                        echo "<td align='right'>".$item['numwrdup']." sms</td>";
                                    }
                                    else if($item['calltype']=='035') {
                                        $increment = $item['bitnum'];                                        
                                        $temp_bytes = ($item['bitnum'])/8;
                                        $mbytes = $temp_bytes / 125000;
                                        echo "<td align='right'>".number_format($mbytes,2)." MB</td>";
                                    }
                                ?>
                            </tr>
                        <?php

                            $index = 0;
                            $i = 0;
                            foreach($consTotal as $ceil){
                                $i ++;
                                if($ceil === $item['calltype']){
                                    $index = $i;
                                }
                            }

                            if($index>0){
                                $newTotal = $consTotal[$index] + $increment;
                                $arr_replace = array($index => $newTotal);
                                $consTotal = array_replace($consTotal, $arr_replace);
                            }else{
                                array_push($consTotal, $item['calltype']);
                                array_push($consTotal, $increment);
                            }

                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel-body">
                <p>
                <b style="font-size:14px; color:#E74C3C;"><?php echo $lang_view_importantinfo; ?></b>
                <br />&nbsp;
                </p>
            <table class="table table-striped table-bordered table-hover table-full-width">
                <tr>
                    <td colspan="2"><b><?php echo $_lang_view_summary_title; ?></b></td>
                </tr>
                <tr>
                    <td width="25%"><?php echo $_lang_view_summary_range; ?></td>
                    <td width="75%"><?php echo $_lang_view_summary_since.$this->startdate.$_lang_view_summary_to.$this->enddate; ?></td>
                </tr>
                <?php
                    $index = 0;
                    $cant = count($consTotal);
                    while($index<$cant){
                    ?>
                    <tr>
                        <td width="25%"><?php echo $_lang_view_typeconsumption; ?>(<?php echo $consTotal[$index]; ?>)</td>
                        <td width="75%">
                            <?php 
                                if($consTotal[$index]=="001"){
                                    $totSeg = $consTotal[$index+1];
                                    $totMin = 0;
                                    $totHor = 0;
                                    $totTxt = "";
                                    while($totSeg>59){
                                        $totMin ++;
                                        $totSeg = $totSeg - 60;
                                    }
                                    while ($totMin>59){
                                        $totHor ++;
                                        $totMin = $totMin - 60;
                                    }
                                    if($totHor > 0) $totTxt = $totHor."h ";
                                    if($totMin > 0 or $totHor > 0) $totTxt .= $totMin."m ";
                                    $totTxt .= $totSeg."s ";
                                    echo $totTxt;
                                }
                                
                                else if($consTotal[$index]=="014"){ 
                                    echo $consTotal[$index+1]." sms"; 
                                }

                                else if($consTotal[$index]=="035"){
                                    $totbyte = ($consTotal[$index+1] / 8); 
                                    $totmb = $totbyte / 125000;
                                    echo number_format($totmb, 2)." MB"; 
                                }
                            ?>
                        </td>
                    </tr>
                    <?php
                    $index = $index +2;
                    }
                ?>
            </table>
            
                <a href="/cdr/generateExcelReport/?whSearch=<?php echo $this->whSearch; ?>&commandSearch=<?php echo $this->commandSearch; ?>&dateRange=<?php echo $this->daterange; ?>&wtSearch=<?php echo $this->wtSearch; ?>" target="_blank" class="btn btn-primary">
                    Descargar reporte en Excel
                </a>  
            
            <br />&nbsp;
        </div>

    </div>
</div>
<?php
} ?>

<div class="row">
    <div class="col-md-12">

        <p><?php echo $_lang_view_cdrinfo; ?></p>

    </div>
</div>