<?php
$_lang_view_servicetype = "Service";
$_lang_view_service_prepaid = "Prepaid";
$_lang_view_service_postpaid = "Postpaid";

$_lang_view_account = "Account";
$_lang_view_serial = "Simcard #";
$_lang_view_voicen = "Voice #";
$_lang_view_datan = "Data #";
$_lang_view_daterange = "Date";
$_lang_btn_generatereport = "Generate Report";


//-- Catalog Index
$_lang_index_label_module = "Cdr";
$_lang_index_label_action = "Report";

$_lang_index_label_title = "Cdr Report";

$_lang_view_label_startdate = "Start Date";
$_lang_view_label_starttime = "Start Time";
$_lang_view_label_mobilenumber = "Mobile number";
$_lang_view_label_callednumber = "Called number";
$_lang_view_label_countrydialedname = "Country Dialed Name";
$_lang_view_label_ratingcalltype = "Rating call type";
$_lang_view_label_end_date = "End Date";
$_lang_view_label_end_time = "End Time";
$_lang_view_label_units = "Units";
$_lang_view_label_totalcharge = "Total Charge";

$_lang_view_datetime ="Date";
$_lang_view_source ="Origen";
$_lang_view_destination ="Destination";
$_lang_view_type ="Type";
$_lang_view_plan ="Plan";
$_lang_view_duration ="Duration";
$_lang_view_additionalcharges ="Additional airtime";
$_lang_view_total ="Total";


$_lang_view_cdrinfo = "You have to consider a  latency between the cdrs recorded in the system and real-time cdrs, this latency may differ between satellite networks, i.e.  ";
$_lang_view_cdrinfo .= " Inmarsat CDRs with a latency of 2 hours, Iridium CDRs with a latency of 48 hours. This info is only referencial ";


$_lang_view_dateintervals = "Date intervals";
$_lang_view_startquery = "Consult";

$_lang_view_table_date = "Datetime";
$_lang_view_table_imsi = "IMSI";
$_lang_view_table_simcard = "Sim Card";
$_lang_view_table_subscriber = "Number";
$_lang_view_table_destination = "Destination";
$_lang_view_table_product = "Product";
$_lang_view_table_type = "Type";
$_lang_view_table_duration = "Duration";
$_lang_view_table_durationsession = "Session duration";
$_lang_view_table_ammount = "Ammount USD";
$_lang_view_table_consumption = "Consumption";

$_lang_view_summary_title = "Consumption summary";
$_lang_view_summary_range = "Date range";
$_lang_view_summary_since = "Sice ";
$_lang_view_summary_to = " to ";
$_lang_view_summary_plan = "Plan: ";
$_lang_view_summary_aditional = "Aditional: ";
$_lang_view_summary_total = "Total (without taxes)";

$lang_view_importantinfo = "IMPORTANT: This information is for internal use.";

$_lang_view_typeconsumption = "Type consumption: ";
