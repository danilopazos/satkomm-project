
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/cdr/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <strong><?php echo $_lang_index_label_title; ?></strong>
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_action; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th><?php echo $_lang_view_datetime; ?></th>
                            <th> <?php echo $_lang_view_source; ?></th>
                            <th> <?php echo $_lang_view_destination; ?></th>
                           
                            <th><?php echo $_lang_view_type; ?></th>
                            <th><?php echo $_lang_view_duration; ?></th>

                            <th class="hidden-xs"><?php echo $_lang_view_additionalcharges; ?></th> 
                            <th class="hidden-xs"><?php echo $_lang_view_total; ?></th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total = count($this->aRecord);
                        $currency_symbol = "USD";
                        $table = "";
                        if ($total > 0) {
                            $table = "";
                            foreach ($this->aRecord as $detail) {
                                
                                
                                $table .="<tr>";
                                $unitcode = "";
                                if ($detail->numwrd > 0) {
                                    $unitcode = $detail->numwrdunitcode;
                                    if ($unitcode == "SECOND") {
                                        $unitcode = "MIN";
                                    }
                                }
                                if ($detail->bitnum > 0) {
                                    $unitcode = $detail->bitnumunitcode;
                                    if ($unitcode == "BIT") {
                                        $unitcode = "MB";
                                    }
                                }


                                $table .="<td>".$detail->cdrdate."</td>";
                                $table .="<td>".$detail->numberfrom."</td>";
                                $table .="<td>".$detail->destinationnumber."</td>";
                                $table .="<td>".$detail->traffictypedesc."</td>";
                                $table .="<td>".$detail->qty ."</td>";
                                $table .="<td>".$currency_symbol . " " .  moneyFormat($detail->chargeoutofbundle)."</td>";
                                $table .="<td>".$currency_symbol . " " .  moneyFormat($detail->chargeoutofbundle)."</td>";
                            
                                $table .="</tr>";
                                
                            }
                        }
                        
                        echo $table;
                        
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->


<div class="row">
    <div class="col-md-12">

        <p><?php echo $_lang_view_cdrinfo; ?></p>

    </div>
</div>