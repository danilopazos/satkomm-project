
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/cdr/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <strong><?php echo $_lang_index_label_title; ?></strong>
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_action; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <form id="form-report" name="form-report" method="post" action="/cdr/generate" target="_blank"> 
                    <table class="table table-striped table-bordered table-hover table-full-width" >
                   
                    <tbody>

                       <tr>
                            <th><?php echo $_lang_view_account; ?></th>
                            <td colspan="3">

                                <select name="accountid" id="accountid" class="form-control search-select" >
                                    
                                    <?php 
                                    $_total = count($this->aAccount);
                                    for($i=0; $i<$_total; $i++){
                                        $e = $this->aAccount[$i];
                                        
                                        echo '<option value="'.$e->id.'">'.$e->name.'</option>';
                                        
                                    }
                                    
                                    ?>
                                    
                                </select>

                            </td>
                    
                            <th><?php echo $_lang_view_servicetype; ?></th>
                            <td>
                                <select name="servicetype" id="servicetype" class="form-control" >
                                    
                                    <option value="0"><?php echo $_lang_view_service_prepaid; ?></option>
                                    <option value="1"><?php echo $_lang_view_service_postpaid; ?></option>
                                    
                                </select>
                            </td>                            
                            
                    
                        </tr>
                        <tr>
                            <th><?php echo $_lang_view_serial; ?></th>
                            <td>
                                <input type="text" name="simcard" id="simcard" class="form-control" /> 
                            </td>

                            <th><?php echo $_lang_view_voicen; ?></th>
                            <td><input type="text" name="voice" id="voice" class="form-control" /> </td>

                            <th><?php echo $_lang_view_datan; ?></th>
                            <td><input type="text" name="data" id="data" class="form-control" /> </td>

                        </tr>
                        <tr>
                            <th><?php echo $_lang_view_daterange; ?></th>
                            <td colspan="5">

                                <div class="input-group">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    <input class="form-control date-time-range" name="daterange" type="text">
                                </div>

                            </td>

                        </tr>
                        <tr>
                            <td colspan="6">
                                <a href="#" class="btn btn-primary" id="btnGenerateReport"  ><?php echo $_lang_btn_generatereport; ?></a>
                            </td>
                        </tr>

                    </tbody>
                </table>
                </form>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->


<div class="row">
    <div class="col-md-12">

        <p><?php echo $_lang_view_cdrinfo; ?></p>

    </div>
</div>