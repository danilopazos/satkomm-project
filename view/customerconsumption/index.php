
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/customerinvoices/index">
                    <?php echo $_lang_index_label_customerinvoices;  ?>
                </a>
            </li>
            <li class="active">
                    <?php echo $_lang_index_label_list;  ?>
            </li>
            <li class="search-box">
               <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
          
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_customerinvoices;  ?>
                <div class="panel-tools">
                     <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th><?php echo $_lang_view_label_invoiceno;  ?></th>
                            <th><?php echo $_lang_view_label_customeraccount;  ?></th>
                            <th><?php echo $_lang_view_label_period;  ?></th> 
                            <th><?php echo $_lang_view_label_invoicedate;  ?></th>
                            <!-- <th><?php echo $_lang_view_label_paymentstatus;  ?></th> -->
                            <th><?php echo $_lang_view_label_total;  ?></th> 
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php 
                        if( count($this->aRecord) > 0){
                            foreach ($this->aRecord as $e  ){
                            
                            $invoiceno = $e->provideraccountid.''.$e->customeraccountid.''.$e->year.''.$e->month;    
                                
                            echo '<tr>';
                                echo '<td>'.$invoiceno.' </td>';
                                echo '<td> '.$e->customername.' </td>';
                                echo '<td> '.$e->year.' - '.$e->month.'   </td> ';
                                echo '<td> '.$e->invoicedate.' </td>';
                                /*
                                if($e->paymentstatus == 0){
                                    echo '<td> Pending </td>';
                                }else{
                                    echo '<td> Paid </td>';
                                }
                                */
                                echo '<td> USD '.$e->total.' </td>';
                                echo '<td> <a target="_blank" href="../customerconsumption/invoicepdf/id/'.$e->id.'" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>';
                                echo '<td> <a target="_blank" href="../customerconsumption/invoicecdrpdf/id/'.$e->id.'" class="btn btn-xs btn-primary"> Descargar CDR &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>';
                            echo '</tr>';


                            }
                        }
                        
                        ?>
                        
                        
                        <!--
                        <tr>
                            <td> IFAC-100003 </td>
                            <td> MINERA BUENA VENTURA S.A. </td>
                            <td> 2017 -> 1 AGOSTO - 31 AGOSTO </td> 
                            <td> 2017-09-01 </td>
                            <td> PENDIENTE </td>
                            <td> USD 5,000 </td>
                            <td> <a target="_blank" href="../../report.xls" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        
                        <tr>
                            <td> IFAC-100004 </td>
                            <td> CRUZ DEL SUR S.A. </td>
                            <td> 2017 -> 1 AGOSTO - 31 AGOSTO </td> 
                            <td> 2017-09-01 </td>
                            <td> PENDIENTE </td>
                            <td> USD 1,100 </td>
                            <td> <a target="_blank"  href="../../report.xls" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        
                        <tr>
                            <td> IFAC-100005 </td>
                            <td> CRUZ DEL SUR S.A. </td>
                            <td> 2017 -> 1 JULIO - 31 JULIO </td> 
                            <td> 2017-08-01 </td>
                            <td> PAGADO </td>
                            <td> USD 1,200 </td>
                            <td> <a target="_blank"  href="../../report.xls" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        
                        <tr>
                            <td> IFAC-100006 </td>
                            <td> MINERA BUENAVENTURA S.A. </td>
                            <td> 2017 -> 1 JULIO - 31 JULIO </td> 
                            <td> 2017-08-01 </td>
                            <td> PAGADO </td>
                            <td> USD 4,300 </td>
                            <td> <a  target="_blank" href="../../report.xls" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        
                        <tr>
                            <td> IFAC-100007 </td>
                            <td> CLINICA SAN PABLO S.A. </td>
                            <td> 2017 -> 1 AGOSTO - 31 AGOSTO </td> 
                            <td> 2017-09-01 </td>
                            <td> PAGADO </td>
                            <td> USD 2,000 </td>
                            <td> <a target="_blank"  href="../../report.xls" class="btn btn-xs btn-primary"> Descargar &nbsp; <i class="fa fa-file-pdf-o"></i> </a> </td>
                        </tr>
                        -->
                        
                        
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
        
    </div>
</div>
<!-- end: PAGE CONTENT-->
