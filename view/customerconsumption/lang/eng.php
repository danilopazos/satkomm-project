<?php

//-- Catalog Index
$_lang_index_label_module = "Invoice";
$_lang_index_label_action = "Detail";
$_lang_index_label_packinglist = "Packing List";

$_lang_index_label_goback = "Go Back";


$_lang_index_label_processorder = "Process Order";
$_lang_index_label_generateinvoice = "Generate Invoice";
$_lang_index_label_myorders = "My Invoices";
$_lang_index_label_list = "List";
$_lang_index_label_customerinvoices = "Customer Invoices";

$_lang_index_table_title = "Invoice Detail";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Name";
$_lang_index_table_th_networkcode = "Network";
$_lang_index_table_th_price = "Price";
$_lang_index_table_th_product = "Product";

$_lang_view_label_close = "Close";
$_lang_view_label_addcart = "Add Cart";

$_lang_register_block_ordercomments = "Order Comments";
$_lang_view_block_addcart = "Add Cart";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Package";
$_lang_panel_technicalspecs = "Specs";
$_lang_panel_features = "Features";
$_lang_panel_docs = "Docs";

$_lang_view_label_quantity = "Quantity";
$_lang_view_label_price = "Price";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item added";
$_lang_view_label_subttotal = "Sub-total";
$_lang_view_label_total = "Total";


$_lang_register_block_billaddress = "Billing Address";
$_lang_register_block_shipaddress = "Shipping Address";
$_lang_register_label_country = "Country";
$_lang_register_label_state = "State";
$_lang_register_label_city = "City";
$_lang_register_label_street = "Street";
$_lang_register_label_zipcode = "Zip Code";
$_lang_register_label_pobox = "P.O. Box";

$_lang_register_label_processok = "Order Placed";
$_lang_register_label_formerror = "Problems, please check the empty fields";
$_lang_view_label_confirmorder_message = "Please confirm to place your order, thanks";
$_lang_view_label_confirmorder = "Confirm Order";

$_lang_register_block_orderinfo = "Invoice Info";

$_lang_view_label_accountname = "Account";
$_lang_view_label_invoiceno = "Invoice #";
$_lang_view_label_orderno = "Order #";
$_lang_view_label_invoicedate = "Invoice Date";
$_lang_view_label_totalitems = "Total items";
$_lang_view_label_orderuser = "By";
$_lang_view_label_customeraccount = "Customer Account";
$_lang_label_printer = "Print";

$_lang_view_label_paymentdate = "Payment Date";
$_lang_view_label_dispatchdate = "Dispatch Date";
$_lang_view_label_deliverydate = "Delivery Date";


$_lang_btn_processorder = "Process Order";

//Invoice Labels
$_lang_view_label_invoicenum = "Invoice #";
$_lang_view_label_invoiceduedate = "Due Date";


$_lang_register_block_invoiceinfo = "Invoice Info";
$_lang_register_block_invoicedetail = "Invoice Details";

$_lang_view_label_shipping_cost = "Shipping Cost";
$_lang_view_label_distpatch_item = "Register Items";
$_lang_view_label_distpatch_itemdispatched = "Items Registered";

$_lang_view_payment_method = "Payment Method";
$_lang_view_payment_creditcard = "Payment process fee - (Credit Card)";
$_lang_view_payment_wireless = "Payment process fee - Bank Transfer";
$_lang_register_block_invoicecomments = "Invoice Comments";
$_lang_index_label_generateInvoice = "Generate Invoice";
$_lang_view_label_dispatchitems = "Save Items Id";


$_lang_view_dispatch_head_serial = "Serial/ESN/IMEI";
$_lang_view_dispatch_head_code2 = "Auth/Code2/Other codes";

$_lang_view_label_formregistered = "Items registered";
$_lang_register_label_serialrepeated = "The form presents Serial numbers repeated";

$_lang_btn_dispatchinvoice = "Dispatch Invoice";
$_lang_btn_confirmdispatch = "Confirm Dispatch";
$_lang_dispatch_msg_confirmdispatch = "Are you sure to confirm the dispatch, these items will be added to the customer inventory";
$_lang_dispatch_msg_btnconfirm = "Yes, dispatch items now";

$_lang_view_label_packegelist = "Package List";
$_lang_view_label_setdeliverydateOk = "Delivery Date registered";
$_lang_view_label_setdeliverydate = "Set Delivery Date";
$_lang_view_btn_setdeliverydate = "Set delivery date";
$_lang_view_msg_dispatchdatenotyet = "Please, set the dispatch date first";

$_lang_view_label_dispatchdate = "Dispatch Date";
$_lang_view_label_deliverydate = "Delivery Date";

$_lang_view_label_trackinginfo = "Shipping tracking";
$_lang_view_label_tracking_selectprovider = "Select the shipping provider (optional)";
$_lang_view_label_tracking_trackingid_info = "Introduce the tracking id (optional)";

$_lang_view_label_distributor = "FREIGHT BY DISTRIBUTOR";
$_lang_view_label_shippingnotdefined = "Not Defined";

$_lang_view_label_period = "Period";
$_lang_view_label_paymentstatus = "Payment status";

$_lang_view_download = "Download";
