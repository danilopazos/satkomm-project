<script type="text/javascript">
    var _lang_view_dispatch_head_serial = '<?php echo $_lang_view_dispatch_head_serial; ?>';
    var _lang_view_dispatch_head_code2 = '<?php echo $_lang_view_dispatch_head_code2; ?>';

    var _lang_view_label_distpatch_itemdispatched = '<?php echo $_lang_view_label_distpatch_itemdispatched; ?>';
    var _lang_view_label_distpatch_item = '<?php echo $_lang_view_label_distpatch_item; ?>';

<?php
echo ' var _deliverydate_min   =  new Date(); ';
if ($this->invoice->dispatchdate != "") {
    $aDispatchDate = split("-", substr(trim($this->invoice->dispatchdate), 0, 10));
    echo '   _deliverydate_min   =  new Date(' . $aDispatchDate[0] . ', ' . intval($aDispatchDate[1]) . ', ' . intval($aDispatchDate[2]) . '); ';
}
?>


</script>

<!-- Dispatch Item Model -->
<div class="modal fade" id="modal-dispatchitem" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-dispatch-items  box-user-register" id="form-dispatch-items"  >

                <div class="modal-body">

                    <div class="errorHandlerItemDispatched  alert alert-danger " id="errorHandlerSerialRepeated">
                        <?php echo $_lang_register_label_serialrepeated; ?> <span id="repeated_content"></span>
                    </div>
                    <div class="errorHandlerItemDispatched  alert alert-danger " id="errorHandlerItemDispatched">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div id="messageItemDispatchedOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_formregistered; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <span id="dispatch_form_title"></span>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal" id="dispatch_form_content">  


                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">

                    <input type="hidden" name="documentcode" id="documentcode" value="<?php echo $this->invoice->code; ?>" />
                    <input type="hidden" name="itemid" id="itemidByBatch" value="" />

                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="button" class="btn btn-primary"  id="btn_dispatch_items"  >
                        <?php echo $_lang_view_label_dispatchitems; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Set Delivery Date -->
<div class="modal fade" id="modal-setdeliverydate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form method="post" action="/invoice/setdeliverydate/code/<?php echo $this->invoice->code; ?>"       role="form" class="form-setdeliverydate  box-user-register" id="form-setdeliverydate"  >

                <div class="modal-body">

                    <div class="errorHandlerDeliveryDate  alert alert-danger no-display " id="errorHandlerDeliveryDate">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>

                    <div id="messageSetDeliveryDateOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_setdeliverydateOk; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">
                               
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>  <?php echo $_lang_view_label_setdeliverydate; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal" id="dispatch_form_content">  

                                    <?php if ($this->invoice->dispatchdate != "") { 
                                        
                                        $_deliverydate = '';
                                        if($this->invoice->deliverydate !=""){
                                            $_deliverydate = $this->invoice->deliverydate;
                                        }else{
                                            $_deliverydate = date('Y-m-d');
                                        }
                                        
                                        ?>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="input-group" id="picker-container">
                                                <input  id="deliverydate" name="deliverydate"  value="<?php echo $_deliverydate; ?>"  type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="shippingprovidercode" id="shippingprovidercode" class="form-control" >
                                                <option value=""><?php echo $_lang_view_label_tracking_selectprovider; ?></option>
                                                <?php 
                                                $tshippingprovider = count($this->aShippingProvider);
                                                if($tshippingprovider > 0){
                                                    foreach($this->aShippingProvider as $sp){
                                                        
                                                        $spValue = $sp->name;
                                                        $selected = '';
                                                        if($this->invoice->shippingprovidercode == $sp->code){
                                                            $selected = ' selected="true"  ';
                                                        }
                                                        
                                                        if($sp->code =="ND"){
                                                            $spValue = $_lang_view_label_shippingnotdefined ;
                                                        }
                                                        if($sp->code =="DISTRIBUTOR"){
                                                            $spValue = $_lang_view_label_distributor ;
                                                        }
                                                        
                                                        echo '<option '.$selected.' value="'.$sp->code.'">'.$spValue.'</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                         </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input class="form-control" type="text" name="shippingtrackingid" id="shippingtrackingid" value="<?php echo $this->invoice->shippingtrackingid ?>" 
                                                   placeholder="<?php echo $_lang_view_label_tracking_trackingid_info; ?>" />
                                         </div>
                                    </div>                                    
                                    
                                    
                                    <?php } else {
                                        ?>
                                        <div class="errorHandlerItemDispatched  alert alert-warning " >
                                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_msg_dispatchdatenotyet; ?>
                                        </div>
                                        <?php }
                                    ?>

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">

                    <input type="hidden" name="code" value="<?php echo $this->invoice->code; ?>" />

                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>
                    <?php if ($this->invoice->dispatchdate != "") { ?>
                        <button type="submit" class="btn btn-primary"  >
                            <?php echo $_lang_view_btn_setdeliverydate; ?>
                        </button>
                    <?php } ?>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            echo $_lang_view_label_invoiceno.'&nbsp;<strong>'.$this->invoice->code.'</strong>&nbsp;';
            
            //if ($this->canDispatchInvoice && $this->invoice->dispatchdate == "" && $this->processInvoice) {
            if ( $this->invoice->dispatchdate == "" && $this->processInvoice) {
                
                if ($this->canDispatchInvoice){
                    echo '<a class="btn btn-xs  btn-green" href="/invoice/dispatch/code/' . $this->invoice->code . '" id="btn_dispatch_invoice">';
                    echo '<i class="fa fa-shopping-basket"></i>&nbsp;' . $_lang_btn_dispatchinvoice;
                    echo '</a>';
                }else{
                    echo '<a style="display:none;" class="btn btn-xs  btn-green" href="/invoice/dispatch/code/' . $this->invoice->code . '" id="btn_dispatch_invoice">';
                    echo '<i class="fa fa-shopping-basket"></i>&nbsp;' . $_lang_btn_dispatchinvoice;
                    echo '</a>';
                }
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->


<form role="form" class=" form-placeorder  box-user-register" id="form-placeorder"  >

    <input type="hidden" value="<?php echo count($this->detail); ?>"  name="totalitem"  id="totalitem"   />

    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_orderinfo; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <th><?php echo $_lang_view_label_accountname; ?></th>
                                <td> <?php echo $this->invoice->accountname; ?> </td> 
                                <th><?php echo $_lang_view_label_invoiceno; ?></th>
                                <td> <?php echo $this->invoice->code; ?> </td> 

                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_invoicedate; ?></th>
                                <td> <?php echo substr($this->invoice->invoicedate, 0, 10); ?> </td> 
                                <th><?php echo $_lang_view_label_orderuser; ?></th>
                                <td> <?php echo $this->invoice->firstname . '  ' . $this->invoice->lastname; ?> </td> 
                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_invoiceduedate; ?></th>
                                <td> <?php echo substr($this->invoice->invoiceduedate, 0, 10); ?> </td> 
                                <th><?php echo $_lang_view_label_orderno; ?></th>
                                <td> <a href="/saleorder/view/code/<?php echo $this->invoice->ordercode; ?>" class="btn btn-xs btn-default"><?php echo $this->invoice->ordercode; ?> </a> </td> 
                            </tr>


                            <tr>
                                <th><?php echo $_lang_view_label_paymentdate; ?></th>
                                <td> 
                                    <?php
                                    if ($this->invoice->paymentdate != "") {
                                        echo substr($this->invoice->paymentdate, 0, 10);
                                    } else {
                                        echo '--';
                                    }
                                    ?> 
                                </td> 
                                <th><?php echo $_lang_view_label_dispatchdate; ?></th>
                                <td> <?php
                                    if ($this->invoice->dispatchdate != "") {
                                        echo '<a   class="btn  btn-green btn-xs"   href="/invoice/dispatch/code/' . $this->invoice->code . '"> <i class="fa fa-shopping-basket"></i>&nbsp;' . $this->invoice->dispatchdate . '</a>';
                                    } else {
                                        echo '--';
                                    }
                                    ?> 
                                </td> 
                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_deliverydate; ?></th>
                                <td> <?php
                                    if ($this->processInvoice) {

                                        if ($this->invoice->deliverydate == "") {

                                            echo '<a  data-toggle="modal" data-target="#modal-setdeliverydate" class="btn  btn-bricky btn-xs"   href="#"> <i class="fa fa-truck"></i>&nbsp;' . $_lang_view_btn_setdeliverydate . '</a>';
                                        } else {

                                            echo '<a  data-toggle="modal" data-target="#modal-setdeliverydate" class="btn  btn-green btn-xs"   href="#"> <i class="fa fa-truck"></i>&nbsp;' . $this->invoice->deliverydate . '</a>';
                                        }
                                    } else {
                                        if ($this->invoice->deliverydate != "") {
                                            echo $this->invoice->deliverydate;
                                        } else {
                                            echo '--';
                                        }
                                    }
                                    ?> 
                                </td> 
                                <th> <?php echo $_lang_view_label_trackinginfo; ?></th>
                                <td>
                                
                                <?php 
                                $showtrackinginfo = false;
                                if($this->invoice->shippingprovidercode !=""){
                                    echo '<strong>'.$this->invoice->shippingprovider_name.'</strong>';
                                    $showtrackinginfo = true;
                                }
                                if($this->invoice->shippingtrackingid !=""){
                                        echo  '&nbsp;';
                                    if($this->invoice->shippingprovidercode =="FEDEX"){
                                        
                                        echo  '<a target="_blank" href="https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers='.$this->invoice->shippingtrackingid.'&clienttype=ivother">'.$this->invoice->shippingtrackingid.'</a>';  
                                        
                                    }else if($this->invoice->shippingprovidercode =="UPS"){
                                        
                                        echo  '<a target="_blank" href="https://www.ups.com/WebTracking/track?loc=en_US">'.$this->invoice->shippingtrackingid.'</a>';  
                                        
                                    }else{
                                        
                                        echo  $this->invoice->shippingtrackingid ;
                                        
                                    }                                    
                                    $showtrackinginfo = true;
                                }
                                if(!$showtrackinginfo){
                                    echo '--';
                                }
                                ?>
                                
                                
                                </td> 
                            </tr>



                        </tbody>
                    </table>



                </div>
            </div>

        </div>

    </div>



    <div class="row">
        <div class="col-sm-12">



            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_index_table_title; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">


                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable" width="100%"   >
                        <thead>
                            <tr>
                                <th width="15%"> </th>
                                <th ><?php echo $_lang_index_table_th_product; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_label_quantity; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_label_subtotal; ?> </th>
                            </tr>
                        </thead>
                        <tbody>
<?php
$total = count($this->detail);
if ($total > 0) {
    $totalsum = 0;
    $index = 1;
    foreach ($this->detail as $cart) {

        $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $cart->mediaid . '/type/thumb"  >';

        $tr = '<tr id="tr_' . $cart->itemid . '"    >';

        //$tr .='<td><a href="#" class="btn  btn-bricky btn-xs link_dispatch_item "  id="item_'.$cart->itemid.'"  ><i class="fa fa-shopping-basket"></i>&nbsp;'.$_lang_view_label_distpatch_item.'</a></td>';

        $tr .='<td  width="15%" align="center">' . $image_product;
        $tr .= '<br/>';

        $_status_item_dispatched_value = 0;
        if ($this->invoice->dispatchdate == "" && $this->processInvoice) {
            if ($cart->totaldispatched == $cart->qty) {
                $_status_item_dispatched_value = 1;
                $tr .='<a href="#" class="btn  btn-green btn-xs link_dispatch_item "  id="item_' . $cart->itemid . '"  ><i class="fa fa-shopping-basket"></i>&nbsp;' . $_lang_view_label_distpatch_itemdispatched . '</a>';
            } else {
                $tr .='<a href="#" class="btn  btn-bricky btn-xs link_dispatch_item "  id="item_' . $cart->itemid . '"  ><i class="fa fa-shopping-basket"></i>&nbsp;' . $_lang_view_label_distpatch_item . '</a>';
            }
        }

        $tr .= '<input type="hidden" class="status_item_dispatched" id="status_item_' . $cart->itemid . '"  value="'.$_status_item_dispatched_value.'"  >';
        $tr .= '</td>';

        $tr .='<td>';
        $tr .= '<strong>' . $cart->productname . '</strong><br />';
        $tr .='SKU: ' . $cart->sku . '<br />';
        $tr .=$_lang_view_label_price . ': ' . moneyFormat($cart->productprice) . '<br />';
        $tr .='</td>';

        $tr .='<td  align="center">';
        $tr .= $cart->qty;
        $tr .='</td>';



        $tr .='<td align="center">';
        $tr .= '<span id="st_' . $cart->itemid . '">' . moneyFormat($cart->subtotal) . '</span>';

        $tr .='<input type="hidden"   name="' . $index . '_operation"   value="buy"  />';
        $tr .='<input type="hidden"   name="' . $index . '_productname"   id="productname_' . $cart->itemid . '"    value="' . $cart->productname . '"  />';
        $tr .='<input type="hidden"   name="' . $index . '_itemid"   value="' . $cart->itemid . '"  />';
        $tr .='<input type="hidden"   name="' . $index . '_sku"   value="' . $cart->sku . '"  />';
        $tr .='<input type="hidden" id="deleted_' . $cart->itemid . '"  name="' . $index . '_deleted"   value="0"  />';
        $tr .='<input type="hidden" id="productid_' . $cart->itemid . '"  name="' . $index . '_productid"   value="' . $cart->productid . '"  />';
        $tr .='<input type="hidden" id="price_' . $cart->itemid . '"  name="' . $index . '_price"    value="' . $cart->buyprice . '"    />';
        $tr .='<input type="hidden" id="qty_' . $cart->itemid . '"     name="' . $index . '_qty"   value="' . $cart->qty . '"    />';
        $tr .='<input type="hidden"   class="subtotal_item"   id="subtotal_' . $cart->itemid . '"   name="' . $index . '_subtotal"    value="' . $cart->subtotal . '"    />';

        $tr .='</td>';

        $tr .= '</tr>';

        echo $tr;

        $totalsum = $totalsum + $cart->subtotal;
        $index ++;
    }

    $tr = '<tr>';
    $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_subtotal . '</strong></td>';
    $tr .= '<td  align="center"> <span id="subtotal"> ' . moneyFormat($this->invoice->subtotal) . '    </span></td>';
    $tr .= '</tr>';

    $tr .= '<tr>';
    $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_shipping_cost . '</strong></td>';
    $tr .= '<td  align="center"> <span id="subtotal"> ' . moneyFormat($this->invoice->shipping_cost) . '    </span></td>';
    $tr .= '</tr>';

    $tr .= '<tr>';
    if ($this->invoice->payment_method == "creditcard") {
        $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_payment_creditcard . '</strong></td>';
    } else {
        $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_payment_wireless . '</strong></td>';
    }

    $tr .= '<td  align="center"> <span id="subtotal"> ' . moneyFormat($this->invoice->payment_fee) . '    </span></td>';
    $tr .= '</tr>';


    $tr .= '<tr>';
    $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_total . '</strong></td>';
    $tr .= '<td  align="center"> <span id="subtotal"> ' . moneyFormat($this->invoice->x_ammount) . '    </span></td>';
    $tr .= '</tr>';

    echo $tr;
}
?>

                        </tbody>
                    </table>

                </div>
            </div>
            <!-- end: DYNAMIC TABLE PANEL -->

        </div>
    </div>
    <!-- end: PAGE CONTENT-->


    <div class="row">
        <div class="col-sm-12">


            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
            </div>
            <div id="messageProcessOk" class="alert alert-success no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
            </div>



<?php if (trim($this->invoice->invoicecomments) != "") { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_invoicecomments; ?>
                        <div class="panel-tools">
                            <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                        </div>
                    </div>
                    <div class="panel-body">
    <?php echo $this->invoice->invoicecomments; ?>
                    </div>
                </div>
<?php } ?>


        </div>

    </div>

    <div class="row">

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_billaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td> <?php echo $this->invoice->billing_country; ?> </td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><?php echo $this->invoice->billing_state; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><?php echo $this->invoice->billing_city; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><?php echo $this->invoice->billing_address; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><?php echo $this->invoice->billing_zipcode; ?></td> 
                            </tr>

<?php if ($this->invoice->billing_pobox != "") { ?>
                                <tr>
                                    <td><?php echo $_lang_register_label_pobox; ?></td>
                                    <td><?php echo $this->invoice->billing_pobox; ?></td> 
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                </div>
            </div>



        </div>

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_shipaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">


                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td> <?php echo $this->invoice->shipping_country; ?> </td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><?php echo $this->invoice->shipping_state; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><?php echo $this->invoice->shipping_city; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><?php echo $this->invoice->shipping_address; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><?php echo $this->invoice->shipping_zipcode; ?></td> 
                            </tr>

<?php if ($this->invoice->shipping_pobox != "") { ?>
                                <tr>
                                    <td><?php echo $_lang_register_label_pobox; ?></td>
                                    <td><?php echo $this->invoice->shipping_pobox; ?></td> 
                                </tr>
                            <?php } ?>


                        </tbody>
                    </table>

                </div> </div>


        </div>
    </div>


</form>



