<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChartActivationsPerDealer);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChartActivationsPerDealer() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
            <?php 
           
           $total = count($this->aActivationsPerDealer);
           for($i=0; $i<$total; $i++){
               $row = $this->aActivationsPerDealer[$i];
               echo "  ['".$row["name"]."', ".$row["total"]."]    ";
               if( ($i+1) < $total){
                   echo " , ";
               }
           }
           ?>
        ]);

        // Set chart options
        var options = {'title': '<?php echo $_lang_index_label_block_services_activationsperdealer; ?>',
            pieHole: 0.2,
            'width': 400,
            'height': 300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_activationsperdealer'));
        chart.draw(data, options);
    }
</script>
<!--Div that will hold the pie chart-->
<div id="chart_activationsperdealer"></div>