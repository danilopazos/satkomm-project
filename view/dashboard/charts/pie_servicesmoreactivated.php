<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
           
           <?php 
           
           $total = count($this->aServicesMoreActivated);
           for($i=0; $i<$total; $i++){
               $row = $this->aServicesMoreActivated[$i];
               echo "  ['".$row["planname"]."', ".$row["total"]."]    ";
               if( ($i+1) < $total){
                   echo " , ";
               }
           }
           ?>
           
        ]);

        // Set chart options
        var options = {'title': '<?php echo $_lang_index_label_block_services_more_activated; ?>',
             pieHole: 0.2,
            'width': 400,
            'height': 300,
             legend: {position: 'rigth', textStyle: {fontSize: 10  } }
            };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        
        
        
        
        chart.draw(data, options);
    }
</script>
<!--Div that will hold the pie chart-->
<div id="chart_div"></div>
<?php 
if($total == 0 || $this->aServicesMoreActivated == false){ echo '<p><a href="#" class="btn btn-xs btn-default">'.$_lang_nodatatoshow.'</a></p>'; }
?>
