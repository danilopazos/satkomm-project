 <script type="text/javascript">
      google.charts.load('current', {
        'packages':['geochart'],
        // Note: you will need to get a mapsApiKey for your project.
        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
      });
      google.charts.setOnLoadCallback(drawRegionsMap);

      function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable([
          ['<?php echo $_lang_index_label_block_activationspercountry_country; ?>', '<?php echo $_lang_index_label_block_activationspercountry_activations; ?>'],
            ['Peru', 120],
            ['United States', 30],
            ['Ecuador', 50],
            ['Bolivia', 30],
            ['Colombia', 60]
        ]);

        var options = {};

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
      }
    </script>
<!--Div that will hold the pie chart-->
<div id="regions_div" style="width: 100%; height: 30%;"></div>