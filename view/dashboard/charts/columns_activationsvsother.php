<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChartActivationsVsOther);

      function drawChartActivationsVsOther() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Activations', 'Supensions', 'Deactivations'],
          ['May',  <?php echo rand (100,300) ?>, <?php echo rand (20,70) ?>, <?php echo rand (10,30) ?>],
          ['June', <?php echo rand (100,300) ?>, <?php echo rand (20,70) ?>, <?php echo rand (10,30) ?>],
          ['July', <?php echo rand (100,300) ?>, <?php echo rand (20,70) ?>, <?php echo rand (10,30) ?>]
        ]);

        var options = {
          chart: {
            title: '<?php echo $_lang_index_label_block_services_activationsvsother; ?>',
            subtitle: 'Activations vs Suspensiones vs Deactivations Profit: May-July (2017)',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_activationsvsother'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
<!--Div that will hold the pie chart-->
<div id="columnchart_activationsvsother" style="width: 50em; height: 30em;"></div>