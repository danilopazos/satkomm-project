
<?php 
if(!$this->aCustomersActivity){
    
    echo '<p><a href="#" class="btn btn-xs btn-default">'.$_lang_nodatatoshow.'</a></p>';
    
}else{
?>
<table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">        

    <thead>
        <tr>
            <th><?php echo $_lang_index_label_block_customerwithmoreinvoices_customername; ?></th>
            <th><?php echo $_lang_index_label_block_customerwithmoreinvoices_creditstatus; ?></th> 
            <th><?php echo $_lang_index_label_block_customerwithmoreinvoices_totalinvoice; ?></th>  
        </tr>
    </thead>                    

    <?php
    $total = count($this->aCustomersActivity);

    $_totalpendingammount = 0;
    $_totalpositivebalance = 0;

    if ($total > 0) {
        for ($i = 0; $i < $total; $i++) {
            $e = $this->aCustomersActivity[$i];

            $style_ammountpending = "";
            $style_positivebalance = "";

            $_positivebalance = 0;
            $_ammountpending = 0;


            $_pendingammount = $e->invoiceammount - $e->paymentammount;
            $_positivebalance = $e->paymentammount - $e->invoiceammount;


            if ($_pendingammount <= 0) {
                $_pendingammount = 0;
            }
            if ($_positivebalance <= 0) {
                $_positivebalance = 0;
            }

            $_creditlimit_status = ($_pendingammount * 100) / $e->creditlimit;




            $tr = "";
            $tr .= "<tr>";
            $tr .= '<td><a class="btn btn-xs btn-default"  href="/invoicepayment/customerinvoices/customerid/' . $e->customeraccountid . '">' . $e->customername . '</a></td>';

            if ($_creditlimit_status > 100) {
                $tr .= '<td><span style="color:red">' . moneyFormat($_creditlimit_status) . "%" . '</span></td>';
            } else {
                $tr .= '<td><span style="color:blue">' . moneyFormat($_creditlimit_status) . "%" . '</span></td>';
            }
 
            $tr .= "<td $style_ammountpending >USD " . moneyFormat($e->invoiceammount) . "</td>"; 
            $tr .= "</tr>";
            echo $tr;
        }
    } else {
        
    }
    ?>

</table>

<?php 

}