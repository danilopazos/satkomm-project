<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="row">
    <div class="col-sm-12">
        <div class="panel-body">
            
            <form name="form_applyfiter" id="form_applyfiter" method="post"  >
            <div class="col-sm-10">
                <div class="row">
                    
                    <select name="daysago" id="daysago" class="form-control" >
                        <?php 
                        $selected = '';
                        if($this->daysago == 30){ $selected = ' selected = "true" '; }else{ $selected= '';   }
                        echo '<option value="30"  '.$selected.' >'.$_lang_index_applyfilter_lastmonth.'</option>';
                        
                        if($this->daysago == 7){ $selected = ' selected = "true" '; }else{ $selected= '';   }
                        echo '<option value="7" '.$selected.' >'.$_lang_index_applyfilter_lastweek.'</option>';
                        
                        
                        if($this->daysago == 60){ $selected = ' selected = "true" '; }else{ $selected= '';   }
                        echo '<option value="60" '.$selected.' >'.$_lang_index_applyfilter_last2month.'</option>';
                        
                        ?>
                    </select> 
                    
                </div>
            </div>
            <div class="col-sm-2">
                
                <button type="submit" class="btn btn-primary"><?php echo $_lang_index_applyfilter; ?></button>
                
            </div>
            </form>
        </div>
    </div>
</div>


<div class="row">


    <div class="col-sm-4">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_services_more_activated; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>
                    
                    <a class="btn btn-xs btn-link panel-expand" onClick="printdiv('chart_div');" href="#">
                        <i class="fa fa-print"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <?php require_once 'charts/pie_servicesmoreactivated.php'; ?>


                </div>

            </div>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
    </div>

    <div class="col-sm-4">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_services_activationsperdealer; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                    <a class="btn btn-xs btn-link panel-expand" onClick="printdiv('chart_activationsperdealer');" href="#">
                        <i class="fa fa-print"></i>
                    </a>
                    
                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <?php require_once 'charts/pie_activationsperdealer.php'; ?>


                </div>

            </div>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
    </div>

  <div class="col-sm-4">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_services_devicesmoreselled; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" onClick="printdiv('chart_devicesbestselled');" href="#">
                        <i class="fa fa-print"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <?php require_once 'charts/pie_devicesmoreselled.php'; ?>


                </div>

            </div>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
    </div>
 
    
</div>



<div class="row">

    
 <div class="col-sm-4">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_distributorspersales; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" onClick="printdiv('chart_distributorspersales');" href="#">
                        <i class="fa fa-print"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <?php require_once 'charts/pie_distributorspersales.php'; ?>


                </div>

            </div>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
    </div>
    
    <div class="col-sm-8">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_customerwithmoreinvoices; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <?php require_once 'charts/pie_customerswithmoreactivty.php'; ?>
 
                </div>

            </div>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
    </div>
    
</div> 