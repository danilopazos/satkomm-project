<?php
$_lang_index_label_module = "Dashboard";
$_lang_index_label_action = "Modules";
$_lang_index_label_block_modules = "Modules status";
$_lang_index_label_block_manage = "Manage";



$_lang_index_label_block_services_more_activated = 'Services more activated';

$_lang_index_label_block_activationspercountry = 'Activations per country';
$_lang_index_label_block_activationspercountry_country = 'Country';
$_lang_index_label_block_activationspercountry_activations = 'Activations';


$_lang_index_label_block_services_activationsperdealer = "Activations per dealer";

$_lang_index_label_block_services_devicesmoreselled = "Devices best selled";

$_lang_index_label_block_services_activationsvsother = "Activations vs Deactivations";

$_lang_index_label_block_distributorspersales="Distributors vs Sales (Devices) en USD";

$_lang_index_label_block_customerwithmoreinvoices = "Customers with more activity";

$_lang_index_label_block_customerwithmoreinvoices_customername = "Customer";
$_lang_index_label_block_customerwithmoreinvoices_creditstatus = "Credit";
$_lang_index_label_block_customerwithmoreinvoices_totalinvoice = "Total";


$_lang_index_applyfilter = "Apply date range filter";

$_lang_index_applyfilter_lastweek = "Last week";
$_lang_index_applyfilter_lastmonth =  "Last month";
$_lang_index_applyfilter_last2month =  "Last two months";

$_lang_nodatatoshow="No data to show";