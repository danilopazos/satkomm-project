<?php
$_lang_index_label_module = "Dashboard";
$_lang_index_label_action = "M&oacute;dulos";
$_lang_index_label_block_modules = "Estados";
$_lang_index_label_block_manage = "Administrar";


$_lang_index_label_block_services_more_activated = 'Servicios más vendidos';

$_lang_index_label_block_activationspercountry = 'Activaciones por país';
$_lang_index_label_block_activationspercountry_country = 'País';
$_lang_index_label_block_activationspercountry_activations = 'Activaciones';

$_lang_index_label_block_services_activationsperdealer = "Activaciones por distribuidor";

$_lang_index_label_block_services_devicesmoreselled = "Equipos más vendidos";

$_lang_index_label_block_services_activationsvsother = "Activaciones vs Desactivaciones";

$_lang_index_label_block_distributorspersales="Distribuidores vs ventas (Equipos) en dólares";

$_lang_index_label_block_customerwithmoreinvoices = "Clientes con m&aacute;s facturaci&oacute;n (5 max)";

$_lang_index_label_block_customerwithmoreinvoices_customername = "Cliente";
$_lang_index_label_block_customerwithmoreinvoices_creditstatus = "Cr&eacute;dito";
$_lang_index_label_block_customerwithmoreinvoices_totalinvoice = "Total facturado";

$_lang_index_applyfilter = "Aplicar filtro de fechas";

$_lang_index_applyfilter_lastweek = "&Uacute;ltima semana";
$_lang_index_applyfilter_lastmonth =  "&Uacute;ltimo mes";
$_lang_index_applyfilter_last2month =  "&Uacute;ltimos dos meses";

$_lang_nodatatoshow="No hay informaci&oacute;n para mostrar";