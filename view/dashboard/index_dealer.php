<div class="row">
    <div class="col-sm-12">
        <div class="panel-body">
            
            <form name="form_applyfiter" id="form_applyfiter" method="post"  >
            <div class="col-sm-10">
                <div class="row">
                    
                    <select name="daysago" id="daysago" class="form-control" >
                        <?php 
                        $selected = '';
                        if($this->daysago == 30){ $selected = ' selected = "true" '; }else{ $selected= '';   }
                        echo '<option value="30"  '.$selected.' >'.$_lang_index_applyfilter_lastmonth.'</option>';
                        
                        if($this->daysago == 7){ $selected = ' selected = "true" '; }else{ $selected= '';   }
                        echo '<option value="7" '.$selected.' >'.$_lang_index_applyfilter_lastweek.'</option>';
                        
                        
                        if($this->daysago == 60){ $selected = ' selected = "true" '; }else{ $selected= '';   }
                        echo '<option value="60" '.$selected.' >'.$_lang_index_applyfilter_last2month.'</option>';
                        
                        ?>
                    </select> 
                    
                </div>
            </div>
            <div class="col-sm-2">
                
                <button type="submit" class="btn btn-primary"><?php echo $_lang_index_applyfilter; ?></button>
                
            </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_modules; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="col-sm-3">
                        <a href="/cart/mycart">
                            <button class="btn btn-icon btn-block">
                                <i class="fa fa-shopping-cart"></i>
                                Cart <span class="badge badge-danger"> <?php echo $this->totalcart; ?> </span>
                            </button>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="/inventory/mydevices">
                            <button class="btn btn-icon btn-block">
                                <i class="fa fa-barcode"></i>
                                Devices <span class="badge badge-warning">  <?php echo $this->totaldevices; ?> </span>
                            </button>
                        </a>
                    </div>



                    <div class="col-sm-3">
                        <a href="/allowancealert/index">
                            <button class="btn btn-icon btn-block">
                                <i class="fa fa-exclamation-triangle"></i>
                                Config Alerts <span class="badge badge-success"> <?php echo $this->totalallowancealert; ?> </span>
                            </button>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="/inventory/mysimcards">
                            <button class="btn btn-icon btn-block">
                                <i class="fa fa-tasks"></i>
                                Simcards <span class="badge badge-danger"> <?php echo $this->totalsimcards; ?>  </span>
                            </button>
                        </a>
                    </div>



                </div>

            </div>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
    </div>
</div>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="row">


    <div class="col-sm-4">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_services_more_activated; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <?php require_once 'charts/pie_servicesmoreactivated.php'; ?>


                </div>

            </div>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
    </div>

    <div class="col-sm-4">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_services_devicesmoreselled; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <?php require_once 'charts/pie_devicesmoreselled.php'; ?>


                </div>

            </div>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
    </div>


    <div class="col-sm-4">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_customerwithmoreinvoices; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <?php require_once 'charts/pie_customerswithmoreactivty.php'; ?>


                </div>

            </div>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
    </div>





</div>


<div class="row">
    <div class="col-sm-12">
        <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_manage; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-4">
                        <div class="core-box">
                            <div class="heading">
                                <i class="clip-user-4 circle-icon circle-green"></i>
                                <h2>Manage Users</h2>
                            </div>
                            <div class="content">
                                Create and update users information
                            </div>
                            <a class="btn btn-xs btn-primary" href="/account/myaccount">
                                Go <i class="clip-arrow-right-2"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="core-box">
                            <div class="heading">
                                <i class="clip-clip circle-icon circle-teal"></i>
                                <h2>Manage Orders</h2>
                            </div>
                            <div class="content">
                                Check your orders status
                            </div>
                            <a class="btn btn-xs btn-primary" href="/saleorder/myorders">
                                Go <i class="clip-arrow-right-2"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="core-box">
                            <div class="heading">
                                <i class="clip-database circle-icon circle-bricky"></i>
                                <h2>View CDRS</h2>
                            </div>
                            <div class="content">
                                View the cdrs generated by your airtime
                            </div>
                            <a class="btn btn-xs btn-primary" href="/cdr/index">
                                Go <i class="clip-arrow-right-2"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>


<!--
<div class="row">


    <div class="col-sm-8">
     
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_index_label_block_services_activationsvsother; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

<?php require_once 'charts/columns_activationsvsother.php'; ?>


                </div>

            </div>
        </div>
      
    </div>
 
    
    
</div>
-->