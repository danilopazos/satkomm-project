<?php

//-----REMITANCE INFO-----------------------------------------------------------

$ship_y = $ship_y +  7;
 if (  ($pdf->GetY()  + 50)  >= ( $pdf->getPageHeight()   - $minusPageHeight  ) ){
    $pdf->AddPage();
    $temp_y = 32;
    $ship_y = $temp_y;
}  

        $remittanceadvice  = "<strong>REMITTANCE ADVICE</strong><br />";
        $remittanceadvice .= "Remittance should be mailed to:<br /><br />";
        $remittanceadvice .= "TESAM GROUP LLC<br />";
        $remittanceadvice .= "15800 Pines Blv. Suite 348.<br />";
        $remittanceadvice .= "Pembroke Pines, FL 33027<br />";
        $remittanceadvice .= "Florida, United States<br />";
	
	/*
        $bankAccounts  = "<strong>BANK ACCOUNTS</strong><br />";
        $bankAccounts .= "For Wire transfers, please remit to:<br /><br />";
        $bankAccounts .= "JPMorgan Chase Bank NA,<br />";
        $bankAccounts .= "270 Park Avenue<br />";
        $bankAccounts .= "New York, NY 10017, United States<br />";      
        $bankAccounts .= "ABA  021000021<br />";       
        $bankAccounts .= "Swift Code = CHASUS33<br />";       
        $bankAccounts .= "Account # 878033559<br />";       
	*/

        $bankAccounts  = "<strong>BANK ACCOUNTS</strong><br />";
        $bankAccounts .= "For Wire transfers, please remit to:<br /><br />";
        $bankAccounts .= "Bank of America<br />";
        $bankAccounts .= "18291 Pines Blvd. <br />";
        $bankAccounts .= "Pembroke Pines FL 33029, United States<br />";      
        $bankAccounts .= "ABA  026009593<br />";       
        $bankAccounts .= "Swift Code = BOFAUS3N<br />";       
        $bankAccounts .= "Account # 8980 8787 5130<br />";       

        $creditcards  = "<strong>CREDIT CARDS ACCEPTED</strong><br />";
        $creditcards .= '<br /><img src="http://crm.satkomm.com/webservice/imgs/creditcards.png" width="300px" height="45px">';
        //$creditcards .= "We added 4% of the total for processing charges";               
        
        
        $pdf->SetFont('helvetica', '', 8);
        $tableCompanyGenerals = '<table cellpading="0" cellspacing="0">';
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td >' . $remittanceadvice.'</td>';
 
        $tableCompanyGenerals .= '<td >' . $bankAccounts.'</td>';
        
        $tableCompanyGenerals .= '<td >' . $creditcards.'</td>';
        
        $tableCompanyGenerals .= '</tr>';
        $tableCompanyGenerals .= '</table>';

        $pdf->MultiCell(190, 7, $tableCompanyGenerals, 1 , 'L', false, 0, 10,  $ship_y, true, 0, true);    
        
        
//-----BILLING INFO-----------------------------------------------------------
        
  //      $ship_y = $ship_y +  43;
   //     if (  ($pdf->GetY()  + 50)  >= ( $pdf->getPageHeight()   - $minusPageHeight  ) ){
           $pdf->AddPage();
           $temp_y = 32;
           $ship_y = $temp_y;
   //     }  

        $titleBlock  = "<strong>BILLING INQUIRIES</strong><br />";
        $titleBlock .= "Please contact the Customer Service group with any query or dispute regarding this invoice within 30 days from due date via one of the means listed
below. Please reference your account number / invoice number in any correspondence.<br /><br />";
 

         $telephone = "<strong>By Telephone:</strong>  (954) 246-0021<br />";
         $telephone .= "<strong>By Fax:</strong> (954) 637-1842";
 
        $mail = "<strong>By  Mail:</strong><br />";
        $mail .= "15800 Pines Bvld. Suite 348<br />";
        $mail .= "Pembroke Pines, FL 33027<br />";
        $mail .= "Florida, United States<br />";    
        
        $email = "<strong>By Email:</strong><br />";
        $email .= "billing@satkomm.com<br />"; 
        
        $footerBlock  = "<strong>Billing hours of operations:</strong><br />";
        $footerBlock .= "Mon-Fri  8:00 - 17:00 (-5 GMT) Eastern Time<br />"; 
        
        
        $pdf->SetFont('helvetica', '', 8);
        $tableCompanyGenerals = '<table cellpading="0" cellspacing="0">';
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td colspan="3" >' . $titleBlock.'</td>'; 
        $tableCompanyGenerals .= '</tr>';       
        
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td >' . $telephone.'</td>';
        $tableCompanyGenerals .= '<td >' . $mail.'</td>';
        $tableCompanyGenerals .= '<td >' . $email.'</td>';
        $tableCompanyGenerals .= '</tr>';
        
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td colspan="3" >' . $footerBlock.'</td>'; 
        $tableCompanyGenerals .= '</tr>';             
        
        $tableCompanyGenerals .= '</table>';

        $pdf->MultiCell(190, 7, $tableCompanyGenerals, 1 , 'L', false, 0, 10,  $ship_y, true, 0, true);    
        
        
        
        //-----TECHNICAL INFO-----------------------------------------------------------

        
         $ship_y = $ship_y +  57;
        if (  ($pdf->GetY()  + 57)  >= ( $pdf->getPageHeight()   - $minusPageHeight  ) ){
           $pdf->AddPage();
           $temp_y = 32;
           $ship_y = $temp_y;
        }  

        $titleBlock  = "<strong>TECHNICAL SUPPORT</strong><br />";
        $titleBlock .= "Satkomm is dedicated to delivering to you the best customer support possible by going beyond your expectations.<br /><br />";
 

         $telephone = "<strong>By Telephone:</strong>  (954) 246-0021<br />";
         $telephone .= "<strong>By Fax:</strong> (954) 637-1842";
         
         
        
        $email = "<strong>By Email:</strong><br />";
        $email .= "support@satkomm.com<br />"; 
        
        $footerBlock  = ""; 
        
        
        $pdf->SetFont('helvetica', '', 8);
        $tableCompanyGenerals = '<table cellpading="0" cellspacing="0">';
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td colspan="3" >' . $titleBlock.'</td>'; 
        $tableCompanyGenerals .= '</tr>';       
        
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td >' . $telephone.'</td>';
         $tableCompanyGenerals .= '<td >' . $email.'</td>';
        $tableCompanyGenerals .= '</tr>';
        
        $tableCompanyGenerals .= '<tr>';
        $tableCompanyGenerals .= '<td colspan="3" >' . $footerBlock.'</td>'; 
        $tableCompanyGenerals .= '</tr>';             
        
        $tableCompanyGenerals .= '</table>';

        $pdf->MultiCell(190, 7, $tableCompanyGenerals, 1 , 'L', false, 0, 10,  $ship_y, true, 0, true);    