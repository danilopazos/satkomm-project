<?php

$pdf = new CustomPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


$pdf->waterMark = $this->waterMark;
$pdf->langWords = $this->langWords;
$pdf->imageLogo = $this->blobcontent;
$pdf->companyInfo = $this->account;
$pdf->documentNumberText = $this->langwords->documentQuoteNumberText;
$pdf->header = $this->headDocument;
ob_clean();
$pdf->documentNro = $this->code;
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('DOCUMENT');
$pdf->SetTitle('DOCUMENT ' . strtoupper($this->typeDocument) . ' - ' . $this->headDocument->accountName . " - " . $this->code);
$pdf->SetSubject('DOCUMENT' . $this->headDocument->accountName);
$pdf->SetKeywords('DOCUMENT' . $this->headDocument->accountName);

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(10, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

$pdf->SetFont('helvetica', '', 9);
$pdf->SetFillColor(255, 255, 255);

$pdf->startPageGroup();

$pdf->AddPage();
$pageHeight = $pdf->getPageHeight();


$_subtotal = 0;
$_taxes = doubleval($this->headDocument->taxes);
$_freight = doubleval($this->header->s_h_amount);

$currency_symbol = $this->currency_code.' ';

//-------------------------------------------------------------------
//-----------ACCOUNT DETAILS ------------------------------
$pdf->Ln(4);
$pdf->SetFont('helvetica', '', 12);
$pdf->MultiCell(180, 7, htmlentities($_lang_invoice_to), 0, 'L', false, 0, '', '', true, 0, true);
$pdf->SetFont('helvetica', '', 8);
$pdf->Ln(6);

//$salutation
$ship_y = 41;
$head_y = 0;

$document_contact = utf8_decode($this->langWords->lang_attn) . ": " . utf8_encode(trim(" " . $this->contact_salutation . " " . $this->headDocument->contactFirstName));
if ($this->headDocument->contactFirstName != "") {
    $pdf->MultiCell(180, 7, htmlentities($document_contact), 0, 'L', false, 0, '', '', true, 0, true);
    $pdf->Ln(4);
    $head_y = $head_y + 5;
}
$document_phone_contact = trim($this->headDocument->contactPhone);
if ($document_phone_contact != "") {
    $pdf->MultiCell(180, 7, htmlentities($document_phone_contact), 0, 'L', false, 0, '', '', true, 0, true);
    $pdf->Ln(4);
    $head_y = $head_y + 5;
}
$document_contact_email = trim($this->headDocument->contactEmail);
if ($document_contact_email != "") {
    $pdf->MultiCell(180, 7, htmlentities($document_contact_email), 0, 'L', false, 0, '', '', true, 0, true);
    $pdf->Ln(4);
    $head_y = $head_y + 5;
}

$pdf->Ln(2);
$pdf->SetFont('helvetica', '', 10);
$pdf->MultiCell(180, 7, htmlentities(utf8_encode($this->headDocument->accountName)), 0, 'L', false, 0, '', '', true, 0, true);
$pdf->SetFont('helvetica', '', 8);
$pdf->Ln(5);

$document_street = $this->headDocument->billStreet;
if ($document_street != "") {
    $pdf->MultiCell(180, 7, str_replace(array("--", "---", "----"), array("-", "-", "-"), utf8_encode(htmlentities($document_street))), 0, 'L', false, 0, '', '', true, 0, true);
    $head_y = $head_y + 5;
}

$pdf->Ln(5);
$document_street = "";
if (trim($this->headDocument->billCity) != "") {
    $document_street .= utf8_encode($this->headDocument->billCity);
}

if ($this->headDocument->billState != "") {
    $document_street .=", " . utf8_encode(trim($this->headDocument->billState));
}

if ($this->headDocument->billCode != "") {
    $document_street .="  " . trim($this->headDocument->billCode);
}
if ($document_street != "") {
    $pdf->MultiCell(180, 7, htmlentities($document_street), 0, 'L', false, 0, '', '', true, 0, true);
    $pdf->Ln(4);
    $head_y = $head_y + 5;
}

$document_street = "";
if ($this->headDocument->billCountry != "") {
    $document_street = utf8_encode(htmlentities($this->headDocument->billCountry));
}
if ($document_street != "") {
    $pdf->MultiCell(180, 7, $document_street, 0, 'L', false, 0, '', '', true, 0, true);
    $pdf->Ln(5);
    $head_y = $head_y + 5;
}

if ($this->headDocument->phoneContact != "") {
    $pdf->MultiCell(180, 7, $this->langWords->lang_phone . ": " . htmlentities($this->headDocument->phoneContact), 0, 'L', false, 0, '', '', true, 0, true);
    $pdf->Ln(5);
    $head_y = $head_y + 5;
}

if ($this->headDocument->webSiteAccount != "") {
    $pdf->MultiCell(180, 7, htmlentities($this->headDocument->webSiteAccount), 0, 'L', false, 0, '', '', true, 0, true);
    $pdf->Ln(5);
    $head_y = $head_y + 5;
}

//----------------QUOTE DETAILS AND SHIP TO -------------------------------------------------------------------------
$pdf->SetFont('helvetica', '', 12);
$pdf->MultiCell(60, 7, $this->lang_index_table, 0, 'R', false, 0, '140', '31', true, 0, true);
$pdf->SetFont('helvetica', '', 8);

$pdf->MultiCell(40, 7, $this->lang_invoice_number . ": ", 0, 'L', false, 0, '140', '37', true, 0, true);
$pdf->MultiCell(50, 7, htmlentities($this->code), 0, 'R', false, 0, '150', '37', true, 0, true);


//$img = '/home/paulo/desktop/flower.jpg';
//file_put_contents($img, file_get_contents($imageUrl));
//    $pdf->MultiCell(200, 10, '<img src="'.$imageUrl.'" height="80" width="500"  /> ' , 0, 'C', false, 0, '', '', true, 0, true);



if ($this->headDocument->dateDocument != "") {
    $pdf->MultiCell(40, 7, $this->lang_invoice_date . ": ", 0, 'L', false, 0, '140', $ship_y, true, 0, true);
    $pdf->MultiCell(50, 7, htmlentities($pdf->getDateString($this->headDocument->dateDocument)), 0, 'R', false, 0, '150', $ship_y, true, 0, true);

    $ship_y = $ship_y + 5;
    $pdf->MultiCell(40, 7, utf8_decode($this->lang_invoice_due_date . ": "), 0, 'L', false, 0, '140', $ship_y, true, 0, true);
    $pdf->MultiCell(50, 7, htmlentities($pdf->getDateString($this->headDocument->dateDueDocument)), 0, 'R', false, 0, '150', $ship_y, true, 0, true);
} else {
    $pdf->MultiCell(40, 7, htmlentities($this->lang_invoice_date . ": "), 0, 'L', false, 0, '140', $ship_y, true, 0, true);
    $pdf->MultiCell(50, 7, htmlentities($pdf->getDateString($this->headDocument->dateDocument)), 0, 'R', false, 0, '150', $ship_y, true, 0, true);
}

if (trim($this->headDocument->shippingZipCode != "") || trim($this->headDocument->shippingCity != "") || trim($this->headDocument->shippingState != "") || trim($this->headDocument->shippingCountry != "") || trim($this->headDocument->shippingAddress) != "") {
    $ship_y = $ship_y + 7;
    $pdf->SetFont('helvetica', '', 10);
    $pdf->MultiCell(60, 7, htmlentities($this->langWords->lang_ship_to), 0, 'R', false, 0, '140', $ship_y, true, 0, true);
    $pdf->SetFont('helvetica', '', 8);

    if (trim($this->headDocument->shippingAddress) != "") {
        $ship_y = $ship_y + 5;
        $pdf->MultiCell(80, 7, utf8_encode(htmlentities($this->headDocument->shippingAddress)), 0, 'R', false, 0, '120', $ship_y, true, 0, true);
        if (strlen(trim($this->headDocument->shippingAddress)) > 40) {
            $ship_y = $ship_y + 4;
        }
    }

    $ship_address = "";
    if (trim($this->headDocument->shippingCity) != "") {
        $ship_address .= utf8_encode($this->headDocument->shippingCity);
    }
    if (trim($this->headDocument->shippingState) != "") {
        $ship_address .= ", " . utf8_encode($this->headDocument->shippingState);
    }
    if (trim($this->headDocument->shippingZipCode) != "") {
        $ship_address .= ", " . $this->headDocument->shippingZipCode;
    }
    if ($ship_address != "") {
        $ship_y = $ship_y + 4;
        $pdf->MultiCell(80, 7, htmlentities($ship_address), 0, 'R', false, 0, '120', $ship_y, true, 0, true);
    }

    if (trim($this->headDocument->shippingCountry) != "") {
        $ship_y = $ship_y + 4;
        $pdf->MultiCell(60, 7, utf8_encode(htmlentities($this->headDocument->shippingCountry)), 0, 'R', false, 0, '140', $ship_y, true, 0, true);
    }

    $ship_contact = utf8_decode($this->langwords->attn) . ": " . utf8_encode(trim($salutation . " " . $this->header->contact_firstname . " " . $this->header->contact_lastname));
    if (trim($this->header->contact_firstname) != "") {
        $ship_y = $ship_y + 4;
        $pdf->MultiCell(60, 7, htmlentities($ship_contact), 0, 'R', false, 0, '140', $ship_y, true, 0, true);
    }
} else {
    $ship_y = $ship_y + 22;
}

//****** DOCUMENT DETAILS **********************
//***********************************************


$ship_y = $ship_y + 7;
$pdf->SetFont('helvetica', '', 7);
$pdf->MultiCell(60, 7, $this->lang_document_intro, 0, 'L', false, 0, '', $ship_y, true, 0, true);
$pdf->SetFont('helvetica', '', 7);

$pdf->setCellPaddings(1, 1, 1, 1);
$borders = ' T,B';
$pdf->SetFillColor(128, 128, 128);
$pdf->SetTextColor(255, 255, 255);

$ship_y = $ship_y + 5;

$pdf->SetLineStyle(array('width' => 0, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 128, 128)));

$pdf->MultiCell(110, 7, "<b>" . $this->langWords->lang_product_title . "</b>", $borders, 'L', true, 0, '', $ship_y, true, 0, true);
$pdf->MultiCell(20, 7, "<b>" . htmlentities($this->langWords->lang_quantity_title) . "</b>", $borders, 'C', true, 0, 120, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, "<b>" . htmlentities($this->langWords->lang_price_unit_title) . "</b>", $borders, 'C', true, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, "<b>" . htmlentities($this->langWords->lang_total_title) . "</b>", $borders, 'C', true, 0, 170, $ship_y, true, 0, true);

$pdf->SetFillColor(255, 255, 255);
$pdf->SetTextColor(0, 0, 0);
$pdf->setCellPaddings(0, 0, 0, 0);

$totalProducts = count($this->detailDocument);
if ($totalProducts > 0) {

    $pdf->SetLineStyle(array('width' => 0, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 128, 128)));

    $indice = 0;
    foreach ($this->detailDocument as $detail) {
        $indice++;
        $pdf->setCellPaddings(1, 1, 1, 1);
        if ($indice == 1) {
            $ship_y = $ship_y + 7;
        } else {
            $ship_y = $ship_y + 5;
        }
         
        //--------------------------------------------------------        
        $aDescription = explode(PHP_EOL, $detail->description);
        $blockHeight = 4;
        if (trim($detail->codeProduct) != "") {
            $blockHeight = $blockHeight + 4;
        }
        $indiceDetail = 0;
        if (trim($detail->description) != "") {
            for ($i = 0; $i < count($aDescription); $i++) {
                $temp = str_replace("<br />", "", $aDescription[$i]);
                if (trim($temp) != "") {
                    $blockHeight = $blockHeight + 4;
                    $indiceDetail++;
                    if( strlen( $temp ) > 94  ){
                        $indice = 1 ;
                        if( strlen( $temp ) > 94  ){
                            $indiceDetail++;
                            $resultLine =  intval( strlen($temp) / 94 )  ; 
                            for($j = 0 ; $j < $resultLine ; $j++ ){
                                $blockHeight = $blockHeight + 4;
                            } 
                         }    
                    }
                }
            }
            if ($indiceDetail == 1) {
                $blockHeight = $blockHeight + 4;
            }
        }        
        $borders = ' ';  
        //--------------------------------------------------------
        
        
        $ship_y = checkAddPage($pdf, $ship_y, 15, $blockHeight, 35);
        $status = "";
        if ($ship_y == 35) {
            $status = " - ok";
            $borders = ' B  ';
        }

        $pdf->SetFont('helvetica', '', 7);

        if ($indiceDetail > 0) {
            $blockHeight = $blockHeight - 3;
        } else {
            $blockHeight = $blockHeight + 1;
        }

        $pdf->MultiCell(110, $blockHeight, "<b>" . utf8_encode($detail->nameProduct) . "</b>", $borders, 'L', false, 0, '', $ship_y, true, 0, true);
        $pdf->MultiCell(20, $blockHeight, intval($detail->quantity), $borders, 'C', false, 0, 120, $ship_y, true, 0, true);
        $pdf->MultiCell(30, $blockHeight, $currency_symbol . '' . $pdf->moneyFormat($detail->price), $borders, 'C', false, 0, 140, $ship_y, true, 0, true);
        $pdf->MultiCell(30, $blockHeight, $currency_symbol . '' . $pdf->moneyFormat((doubleval($detail->totalPrice))), $borders, 'C', false, 0, 170, $ship_y, true, 0, true);
        $pdf->SetFont('helvetica', '', 7);
        if (trim($detail->codeProduct) != "") {
            $borders = ' ';
            $ship_y = $ship_y + 4;
            $pdf->MultiCell(110, 8, utf8_encode($detail->codeProduct), "", 'L', false, 0, '', $ship_y, true, 0, true);
            if ($indiceDetail == 0) {
                $pdf->MultiCell(110, 0, "", " ", 'C', false, 0, 10, $ship_y, true, 0, true);
                $pdf->MultiCell(20, 0, "", " ", 'C', false, 0, 120, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 140, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 170, $ship_y, true, 0, true);
            }
        }
        if ($indiceDetail > 0) {
            $indiceAdd = $blockHeight;
            if ($indiceDetail == 1) {
                $ship_y = $ship_y + 4.5;
                $pdf->MultiCell(110, 0, trim($detail->description), "", 'L', false, 0, '', $ship_y, true, 0, true);
                $pdf->MultiCell(110, -1, "", " ", 'C', false, 0, 10, $ship_y, true, 0, true);
                $pdf->MultiCell(20, 0, "", " ", 'C', false, 0, 120, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 140, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 170, $ship_y, true, 0, true);
            } else {
                $index = 0; //
                for ($i = 0; $i < count($aDescription); $i++) {
                    $index++;
                    if (trim($aDescription[$i]) != "") {
                        $ship_y = $ship_y + 4.5;
                        $temp = str_replace("<br />", "", $aDescription[$i]);
                        if ($index == count($aDescription)) {

                            $pdf->MultiCell(110, 0, $temp, "", 'L', false, 0, '', $ship_y, true, 0, true);

                            $pdf->MultiCell(110, 0, "", " ", 'C', false, 0, 10, $ship_y, true, 0, true);
                            $pdf->MultiCell(20, 0, "", " ", 'C', false, 0, 120, $ship_y, true, 0, true);
                            $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 140, $ship_y, true, 0, true);
                            $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 170, $ship_y, true, 0, true);
                        } else {
                            $pdf->MultiCell(110, 0, $temp, "", 'L', false, 0, '', $ship_y, true, 0, true);
                        }
                    }
                } // die();
            }
        }
        //we calcule the sub-total
        $_subtotal = $_subtotal + ( doubleval($detail->price) * intval($detail->quantity) );

        $index ++;
    }//foreach
}//if
$temp_y = 0;

//****** SUB-TOTAL & TERMS TITLE***************
//***********************************************



$ship_y = $ship_y + 11;
$ship_y = checkAddPage($pdf, $ship_y, 15, 45, 35);
//$borders = 'L,R,B';
$borders = 'B';
$pdf->MultiCell(30, 7, '<b>' . $this->langWords->lang_sub_total_title . '</b>', $borders, 'C', false, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, $currency_symbol . '' . $pdf->moneyFormat($this->headDocument->parcialtotal), $borders, 'C', false, 0, 170, $ship_y, true, 0, true);

//$pdf->MultiCell(60,  7, "<b>TERMS :</b>", 0, 'L', false, 0, 10,  $ship_y, true, 0, true);    
//****** TAXES && PAYMENT WAY*******************
//*********************************************** 
$ship_y = $ship_y + 7;
$ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
$pdf->MultiCell(30, 7, '<b>' . $this->langWords->lang_taxes_title . '</b>', $borders, 'C', false, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, doubleval($this->headDocument->taxes) . " %", $borders, 'C', false, 0, 170, $ship_y, true, 0, true);



//****** FREIGHT  && DELIVER TERMS*****************
//*********************************************** 
//$pdf->SetFont('helvetica', '', 8);


$ship_y = $ship_y + 7;
$ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);

$pdf->MultiCell(30, 7, '<b>' . $this->langWords->lang_shipping_cost . '</b>', $borders, 'C', false, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, $currency_symbol . '' . $pdf->moneyFormat(doubleval($this->headDocument->shippingCost)), $borders, 'C', false, 0, 170, $ship_y, true, 0, true);


$ship_y = $ship_y + 7;
$ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
$pdf->MultiCell(30, 7, "<b>" . $this->langWords->lang_total_title . "</b>", $borders, 'C', false, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, "<b>" . $currency_symbol . '' . $pdf->moneyFormat(doubleval($this->headDocument->total)) . "</b>", $borders, 'C', false, 0, 170, $ship_y, true, 0, true);


//--------------
if ($this->credit_terms_info != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
    $pdf->MultiCell(60, 7, "<b>" . $this->credit_terms . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $pdf->MultiCell(30, 7, $this->credit_terms_info, 0, 'L', false, 0, 50, $ship_y, true, 0, true);
}

if ($this->shipping_company_info != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
    $pdf->MultiCell(60, 7, "<b>" . $this->shipping_company . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $pdf->MultiCell(30, 7, $this->shipping_company_info, 0, 'L', false, 0, 50, $ship_y, true, 0, true);
}


//$ship_y = $ship_y + 7;  
$ship_y = checkAddPage($pdf, $ship_y);


//******  DESCRIPTION  ***********************************
//****************************************************** 
if (trim($this->headDocument->comments) != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y);

    $borders = 'L,R,B,T';

    $aLines = $pdf->getTextArray(trim($this->headDocument->comments), 145);
    $blockHeight = 5;
    if (count($aLines) > 0) {
        $blockHeight += ( count($aLines) * 4.2 ) + 0;
    }

    $pdf->MultiCell(190, 7, "<b>" . $this->langWords->lang_conditions . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $ship_y = $ship_y + 7;
    $pdf->MultiCell(190, $blockHeight, "", 0, 'L', false, 0, 10, $ship_y, true, 0, true);

    $temp_y = $ship_y;
    if ($this->headDocument->comments != "") {
        $pdf->SetFont('helvetica', '', 8);
        if (count($aLines) > 0) {
            foreach ($aLines as $line) {
                $borders = 'L,R';

                $ship_y = checkAddPage($pdf, $ship_y);

                $pdf->MultiCell(190, 7, utf8_encode($line), 0, 'J', false, 0, '', $temp_y, true, 0, true);
            }//foreach
        }//if
    }//if($product->comment !="")    
    $ship_y = $temp_y;
}//if
//******  TERMS AND CONDITIONS  ***********************************
//**************************************************************** 
if (trim($this->header->terms_conditions) != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y);
    $borders = 'L,R,B,T';

    $aLines = $pdf->getTextArray(trim($this->header->terms_conditions), 145);
    $blockHeight = 10;
    if (count($aLines) > 0) {
        $blockHeight += ( count($aLines) * 4.2 ) + 0;
    }

    $pdf->MultiCell(190, 7, "<b>" . $this->langwords->conditions . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $ship_y = $ship_y + 7;
    $pdf->MultiCell(190, $blockHeight, "", 0, 'L', false, 0, 10, $ship_y, true, 0, true);

    $temp_y = $ship_y;
    if ($this->header->terms_conditions != "") {
        if (count($aLines) > 0) {
            foreach ($aLines as $line) {
                $borders = 'L,R';

                $pdf->MultiCell(190, 7, utf8_encode($line), 0, 'J', false, 0, '', $temp_y, true, 0, true);
                $temp_y = $temp_y + 4;

                $ship_y = checkAddPage($pdf, $ship_y);
            }//foreach
        }//if
    }//if($product->comment !="")    
    $ship_y = $temp_y;
}//if
//******  TERMS AND CONDITIONS  ***********************************
//**************************************************************** 
//include_once 'footer01.php';
//****** LAST PAGE ***********************************
//*********************************************** 
$pdf->lastPage();
$file = $this->typeDocument . "_" . $this->account->name . "_" . $this->code . ".pdf";
$pdf->Output($file, 'I');

function replace_carriage_return($replace, $string) {
    return str_replace(array("\n\r", "\n", "\r"), $replace, $string);
}

function checkAddPage($pdf, $position, $minuspage = 15, $bloqueAlto = 0, $start = 20) {

    $minusPageHeight = 40;
    if ($pdf->getPage() == 1) {
        $minusPageHeight = 40;
    }
    if ($pdf->getPage() == 1) {
        $minusPageHeight = 40;
    }

    if ($pdf->GetY() + $bloqueAlto >= $pdf->getPageHeight() - $minusPageHeight) {
        $pdf->AddPage();
        $pdf->Ln($start);
        $position = $start;
        $pdf->setCellMargins(0, 0, 0, 0);
    }

    return $position;
}
