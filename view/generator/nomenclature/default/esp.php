<?php

class NomenclatureCountry{
    public $lang_nomenclature_namecustomer = "Cliente";
    public $lang_nomenclature_address = "Dirección";
    public $lang_nomenclature_place = "Distrito";
    public $lang_nomenclature_city = "Provincia";
    public $lang_nomenclature_state = "Departamento";
    public $lang_nomenclature_companyid = "Código fiscal";

    public $lang_nomenclature_importtitle = "Total sin impuestos";

    public $lang_nomenclature_subtotal = "Sub total";
    public $lang_nomenclature_taxes = "Impuestos ";
    public $lang_nomenclature_total = "Total a pagar";

}