<?php

class NomenclatureCountry{
    public $lang_nomenclature_namecustomer = "Customer";
    public $lang_nomenclature_address = "Address";
    public $lang_nomenclature_place = "Place";
    public $lang_nomenclature_city = "City";
    public $lang_nomenclature_state = "State";
    public $lang_nomenclature_companyid = "Customer id";

    public $lang_nomenclature_importtitle = "Total without taxes";

    public $lang_nomenclature_subtotal = "Sub total";
    public $lang_nomenclature_taxes = "Taxes";
    public $lang_nomenclature_total = "Total to pay";
}