<?php

class NomenclatureCountry{
    public $lang_nomenclature_namecustomer = "Nombre / Razón Social";
    public $lang_nomenclature_address = "Dirección";
    public $lang_nomenclature_place = "Distrito";
    public $lang_nomenclature_city = "Provincia";
    public $lang_nomenclature_state = "Departamento";
    public $lang_nomenclature_companyid = "RUC";

    public $lang_nomenclature_importtitle = "Total sin impuestos";

    public $lang_nomenclature_subtotal = "Sub total";
    public $lang_nomenclature_taxes = "I.G.V. ";
    public $lang_nomenclature_total = "Total a pagar";

}