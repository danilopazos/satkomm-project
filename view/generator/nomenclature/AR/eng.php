<?php

class NomenclatureCountry{
    public $lang_nomenclature_namecustomer = "Customer";
    public $lang_nomenclature_address = "Address";
    public $lang_nomenclature_place = "District";
    public $lang_nomenclature_city = "City";
    public $lang_nomenclature_state = "State";
    public $lang_nomenclature_companyid = "C.U.I.T.";

    public $lang_nomenclature_importtitle = "Total without taxes ";

    public $lang_nomenclature_subtotal = "Subtotal";
    public $lang_nomenclature_taxes = "I.V.A.";
    public $lang_nomenclature_total = "Total to pay";
}