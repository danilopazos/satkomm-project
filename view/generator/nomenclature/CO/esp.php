<?php

class NomenclatureCountry{
    public $lang_nomenclature_namecustomer = "Cliente";
    public $lang_nomenclature_address = "Dirección";
    public $lang_nomenclature_place = "Distrito";
    public $lang_nomenclature_city = "Provincia";
    public $lang_nomenclature_state = "Departamento";
    public $lang_nomenclature_companyid = "NIT o CC";

    public $lang_nomenclature_importtitle = "Valor sin impuestos";

    public $lang_nomenclature_subtotal = "Valor";
    public $lang_nomenclature_taxes = "I.V.A. ";
    public $lang_nomenclature_total = "Valor a pagar";

    //Only for Colombia
    public $lang_nomenclature_voicecons = "Impuesto al Consumo de Voz 4%";
    public $lang_nomenclature_trafficcons = "Impuesto al Consumo de Datos 4%";

}