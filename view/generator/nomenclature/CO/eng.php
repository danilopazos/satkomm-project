<?php

class NomenclatureCountry{
    public $lang_nomenclature_namecustomer = "Customer";
    public $lang_nomenclature_address = "Address";
    public $lang_nomenclature_place = "Place";
    public $lang_nomenclature_city = "City";
    public $lang_nomenclature_state = "State";
    public $lang_nomenclature_companyid = "NIT o CC";

    public $lang_nomenclature_importtitle = "Value without taxes";

    public $lang_nomenclature_subtotal = "Total value";
    public $lang_nomenclature_taxes = "I.V.A.";
    public $lang_nomenclature_total = "Value to pay";

    //Only for Colombia
    public $lang_nomenclature_voicecons = "Voice consumption taxes 4%";
    public $lang_nomenclature_trafficcons = "Data consumption taxes 4%";
    
}