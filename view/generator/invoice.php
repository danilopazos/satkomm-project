<?php

$pdf = new CustomPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


$pdf->waterMark = $this->waterMark;
$pdf->langWords = $this->langWords;
$pdf->imageLogo = $this->blobcontent;
$pdf->companyInfo = $this->account;
$pdf->documentNumberText = $this->langwords->documentQuoteNumberText;
$pdf->header = $this->headDocument;
ob_clean();
$pdf->documentNro = $this->code;
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('DOCUMENT');
$pdf->SetTitle('DOCUMENT ' . strtoupper($this->typeDocument) . ' - ' . $this->headDocument->accountName . " - " . $this->code);
$pdf->SetSubject('DOCUMENT' . $this->headDocument->accountName);
$pdf->SetKeywords('DOCUMENT' . $this->headDocument->accountName);

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(10, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

$pdf->SetFont('helvetica', '', 9);
$pdf->SetFillColor(255, 255, 255);

$pdf->startPageGroup();

$pdf->AddPage();
$pageHeight = $pdf->getPageHeight();

//para integración
$base_url=str_replace("view\generator", "util/TCPDF-master/image/", __DIR__);
$header_image=$base_url.$this->logo_header;

/*
//para producción
$base_url = "/var/www/html/www.intelkomm.com/lib/util/TCPDF-master/image/";
$header_image = $base_url."logo_satkomm.png";
*/

$pdf->Image($header_image, 10, 10, 49, 20, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);

$_subtotal = 0;
$_taxes = doubleval($this->headDocument->taxes);
$_freight = doubleval($this->header->s_h_amount);

$currency_symbol = $this->currencyIso." ";
$currency_exchange = $this->currencyExchange;

//-------------------------------------------------------------------
//-----------ACCOUNT DETAILS ------------------------------
$pdf->Ln(10);
$pdf->SetFont('helvetica', 'B', 9);
$pdf->MultiCell(180, 7, ""."DATOS DEL CLIENTE", 0, 'L', false, 0, '', '', true, 0, true);
$pdf->SetFont('helvetica', '', 8);
$pdf->Ln(6);

$document_street = "";
if ($this->headDocument->billState != "") {
    $document_street .=", " . (trim($this->headDocument->billState));
}

if ($this->headDocument->billCode != "") {
    $document_street .="  " . trim($this->headDocument->billCode);
}


$tableCustomer =  '<table cellpading="2" cellspacing="2">';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td width="130" style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_namecustomer).'</td>';
$tableCustomer .= '<td width="300">'.($this->headDocument->accountName).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td style="font-weight:bold;" rowspan="3">'.($this->nomenclature->lang_nomenclature_address).'</td>';
$tableCustomer .= '<td>'.utf8_encode($this->headDocument->billStreet).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td>'.utf8_encode($this->headDocument->billState.$document_street).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td>'.utf8_encode($this->headDocument->billCountry).'</td>';
$tableCustomer .= '</tr>';
if($this->headDocument->phoneContact != ""){
    $tableCustomer .= '<tr>';
    $tableCustomer .= '<td style="font-weight:bold;">'.$this->langWords->lang_phone.'</td>';
    $tableCustomer .= '<td>'.$this->headDocument->phoneContact.'</td>';
    $tableCustomer .= '</tr>';
}
if($this->headDocument->webSiteAccount != ""){
    $tableCustomer .= '<tr>';
    $tableCustomer .= '<td style="font-weight:bold;">'.$this->langWords->lang_invoicetc_customerweb.'</td>';
    $tableCustomer .= '<td>'.($this->headDocument->webSiteAccount).'</td>';
    $tableCustomer .= '</tr>';
}
if($this->headDocument->contactFirstName != ""){
    $tableCustomer .= '<tr>';
    $tableCustomer .= '<td style="font-weight:bold;">'.$this->langWords->lang_invoicetc_namecontact.'</td>';
    $tableCustomer .= '<td>'.($this->headDocument->contactFirstName).'</td>';
    $tableCustomer .= '</tr>';
}
if($this->headDocument->contactPhone != ""){
    $tableCustomer .= '<tr>';
    $tableCustomer .= '<td style="font-weight:bold;">'.$this->langWords->$lang_invoicetc_contactphone.'</td>';
    $tableCustomer .= '<td>'.$this->headDocument->contactPhone.'</td>';
    $tableCustomer .= '</tr>';
}
if($document_contact_email != ""){
    $tableCustomer .= '<tr>';
    $tableCustomer .= '<td style="font-weight:bold;">'.$this->langWords->lang_invoicetc_contactmail.'</td>';
    $tableCustomer .= '<td>'.$document_contact_email.'</td>';
    $tableCustomer .= '</tr>';
}
$tableCustomer .= '<tr>';
$tableCustomer .= '<td style="font-weight:bold;">'.$this->langWords->lang_invoicetc_currencyname.'</td>';
$tableCustomer .= '<td>'.$this->currencyDescription.' ('.$this->currencyIso.')</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '</table>';
$pdf->MultiCell(120, 7, $tableCustomer, 1, 'L', false, 0, '', '', true, 0, true);


//$salutation
$ship_y = 55;
$head_y = 0;

//----------------QUOTE DETAILS AND SHIP TO -------------------------------------------------------------------------

$tableInvoice =  '<table cellpading="2" cellspacing="2">';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td style="font-weight:bold;">'.($this->lang_index_table).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td bgcolor="#E5E8E8">'.($this->code).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td>&nbsp;</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td style="font-weight:bold;">'.($this->langWords->lang_date_issued_invoice).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td bgcolor="#E5E8E8">'.date("d/m/Y", strtotime($this->headDocument->dateDocument)).'</td>';
$tableInvoice .= '</tr>';
if ($this->headDocument->dateDueDocument != "") {
    $tableInvoice .= '<tr>';
    $tableInvoice .= '<td>&nbsp;</td>';
    $tableInvoice .= '</tr>';
    $tableInvoice .= '<tr>';
    $tableInvoice .= '<td style="font-weight:bold;">'.($this->langWords->lang_date_expiration_invoice).'</td>';
    $tableInvoice .= '</tr>';
    $tableInvoice .= '<tr>';
    $tableInvoice .= '<td bgcolor="#E5E8E8">'.date("d/m/Y", strtotime($this->headDocument->dateDueDocument)).'</td>';
    $tableInvoice .= '</tr>';
}
$tableInvoice .= '</table>';

$pdf->MultiCell(40, 7, $tableInvoice, 0, 'C', false, 0, '150', '36', true, 0, true);




if (trim($this->headDocument->shippingZipCode != "") || trim($this->headDocument->shippingCity != "") || trim($this->headDocument->shippingState != "") || trim($this->headDocument->shippingCountry != "") || trim($this->headDocument->shippingAddress) != "") {
    $ship_y = $ship_y + 7;
    $pdf->SetFont('helvetica', '', 10);
    $pdf->MultiCell(60, 7, htmlentities($this->langWords->lang_ship_to), 0, 'R', false, 0, '140', $ship_y, true, 0, true);
    $pdf->SetFont('helvetica', '', 8);

    if (trim($this->headDocument->shippingAddress) != "") {
        $ship_y = $ship_y + 5;
        $pdf->MultiCell(80, 7, utf8_encode(htmlentities($this->headDocument->shippingAddress)), 0, 'R', false, 0, '120', $ship_y, true, 0, true);
        if (strlen(trim($this->headDocument->shippingAddress)) > 40) {
            $ship_y = $ship_y + 4;
        }
    }

    $ship_address = "";
    if (trim($this->headDocument->shippingCity) != "") {
        $ship_address .= utf8_encode($this->headDocument->shippingCity);
    }
    if (trim($this->headDocument->shippingState) != "") {
        $ship_address .= ", " . utf8_encode($this->headDocument->shippingState);
    }
    if (trim($this->headDocument->shippingZipCode) != "") {
        $ship_address .= ", " . $this->headDocument->shippingZipCode;
    }
    if ($ship_address != "") {
        $ship_y = $ship_y + 4;
        $pdf->MultiCell(80, 7, htmlentities($ship_address), 0, 'R', false, 0, '120', $ship_y, true, 0, true);
    }

    if (trim($this->headDocument->shippingCountry) != "") {
        $ship_y = $ship_y + 4;
        $pdf->MultiCell(60, 7, utf8_encode(htmlentities($this->headDocument->shippingCountry)), 0, 'R', false, 0, '140', $ship_y, true, 0, true);
    }

    $ship_contact = utf8_decode($this->langwords->attn) . ": " . utf8_encode(trim($salutation . " " . $this->header->contact_firstname . " " . $this->header->contact_lastname));
    if (trim($this->header->contact_firstname) != "") {
        $ship_y = $ship_y + 4;
        $pdf->MultiCell(60, 7, htmlentities($ship_contact), 0, 'R', false, 0, '140', $ship_y, true, 0, true);
    }
}else{
    $ship_y = $ship_y + 15;
}
//****** DOCUMENT DETAILS **********************
//***********************************************


$ship_y = $ship_y + 7;
$pdf->SetFont('helvetica', '', 7);
$pdf->MultiCell(60, 7, $this->lang_document_intro, 0, 'L', false, 0, '', $ship_y, true, 0, true);
$pdf->SetFont('helvetica', '', 7);

$pdf->setCellPaddings(1, 1, 1, 1);
$borders = ' B';
$pdf->SetFillColor(128, 128, 128);
$pdf->SetTextColor(255, 255, 255);

$ship_y = $ship_y + 5;

$pdf->SetLineStyle(array('width' => 0 , 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 128, 128)));

$pdf->MultiCell(110, 7, "<b>" . $this->langWords->lang_product_title . "</b>", $borders, 'L', true, 0, '', $ship_y, true, 0, true);
$pdf->MultiCell(20, 7, "<b>" . htmlentities($this->langWords->lang_quantity_title) . "</b>", $borders, 'C', true, 0, 120, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, "<b>" . htmlentities($this->langWords->lang_price_unit_title) . "</b>", $borders, 'C', true, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, "<b>" . htmlentities($this->nomenclature->lang_nomenclature_importtitle) . "</b>", $borders, 'C', true, 0, 170, $ship_y, true, 0, true);

$pdf->SetFillColor(255, 255, 255);
$pdf->SetTextColor(0, 0, 0);
$pdf->setCellPaddings(0, 0, 0, 0);

$totalProducts = count($this->detailDocument);
if ($totalProducts > 0) {

    $pdf->SetLineStyle(array('width' => 0 , 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 128, 128)));

    $indice = 0;
     
    foreach ($this->detailDocument as $detail) {
        $indice++;
        $pdf->setCellPaddings(1, 1, 1, 1);
      
        if ($indice == 1) {
            $ship_y = $ship_y + 7;
        } else {
            $ship_y = $ship_y + 5;
        }
         
         
        //--------------------------------------------------------        
        $aDescription = explode(PHP_EOL, $detail->description);
        $blockHeight = 4;
        if (trim($detail->codeProduct) != "") {
            $blockHeight = $blockHeight + 4;
        }
        $indiceDetail = 0;
        $break_a=false;
        $newdescription = "";
        if (trim($detail->description) != "") {
            
            for ($i = 0; $i < count($aDescription); $i++) {
                
                if((int)strlen($aDescription[$i])==0) {
                    
                    $break_a=true;
                }
                
                if(!$break_a){
                    $temp = str_replace("<br />", "", $aDescription[$i]);
                    $newdescription .= $temp."**";
                    if (trim($temp) != "") {
                        //$blockHeight = $blockHeight + 4; //+4
                        $indiceDetail++;
                        if( strlen( $temp ) > 94  ){
                            $indice = 1 ;
                            if( strlen( $temp ) > 94  ){
                                $indiceDetail++;
                                $resultLine =  intval( strlen($temp) / 94 )  ; 
                                for($j = 0 ; $j < $resultLine ; $j++ ){
                                    $blockHeight = $blockHeight + 4;
                                } 
                            }    
                        }
                    }
                }
                
            } 
            
            if ($indiceDetail == 1) {
                $blockHeight = $blockHeight + 4;
            }
        }    
          
        $borders = '  ';  
        //--------------------------------------------------------
        
        
        $ship_y = checkAddPage($pdf, $ship_y, 15, $blockHeight, 35);
        $status = "";
        if ($ship_y == 35) {
            $status = " - ok";
            $borders = ' B  ';
        }

        $pdf->SetFont('helvetica', '', 7);

        /*
        if ($indiceDetail > 0) {
            $blockHeight = $blockHeight - 3;
        } else {
            $blockHeight = $blockHeight + 1;
        }
        */
        $ship_y = $ship_y + 2;
        $pdf->MultiCell(110, $blockHeight, "<b>" . utf8_encode($detail->nameProduct) . "</b>", $borders, 'L', false, 0, '', $ship_y, true, 0, true);
        $pdf->MultiCell(20, $blockHeight, intval($detail->quantity), $borders, 'C', false, 0, 120, $ship_y, true, 0, true);
        $pdf->MultiCell(30, $blockHeight, $currency_symbol . '' . $pdf->moneyFormat($detail->price*$currency_exchange), $borders, 'C', false, 0, 140, $ship_y, true, 0, true);
        $pdf->MultiCell(30, $blockHeight, $currency_symbol . '' . $pdf->moneyFormat((doubleval($detail->totalPrice*$currency_exchange))), $borders, 'C', false, 0, 170, $ship_y, true, 0, true);
        $pdf->SetFont('helvetica', '', 7);
        if (trim($detail->codeProduct) != "") {
            $borders = ' ';
            $ship_y = $ship_y + 5;
            $pdf->MultiCell(110, 8, utf8_encode($detail->codeProduct), "", 'L', false, 0, '', $ship_y, true, 0, true);
            if ($indiceDetail == 0) {
                $pdf->MultiCell(110, 0, "", " ", 'C', false, 0, 10, $ship_y, true, 0, true);
                $pdf->MultiCell(20, 0, "", " ", 'C', false, 0, 120, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 140, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 170, $ship_y, true, 0, true);
            }
        }
        if ($indiceDetail > 0) {
            $indiceAdd = $blockHeight;
            if ($indiceDetail == 1) {
                $ship_y = $ship_y + 5;
                $pdf->MultiCell(110, 0, trim($detail->description), "", 'L', false, 0, '', $ship_y, true, 0, true);
                $pdf->MultiCell(110, -1, "", " ", 'C', false, 0, 10, $ship_y, true, 0, true);
                $pdf->MultiCell(20, 0, "", " ", 'C', false, 0, 120, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 140, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 170, $ship_y, true, 0, true);
            } else {
                $ship_y = $ship_y + 5;
                $newdescription=substr($newdescription, 0, -2);
                $array_tmp=explode("**", $newdescription);
                foreach($array_tmp as $item){
                    $pdf->MultiCell(110, 0, $item, '', 'L', false, 0, 15, $ship_y, true, 0, true);
                    $spaces=strlen($item);
                    $ship_y = $ship_y + 4;
                    while($spaces>90){
                        $ship_y = $ship_y + 3;
                        $spaces = $spaces - 90;
                    }
                }
                /*
                $index = 0; //
                for ($i = 0; $i < count($aDescription); $i++) {
                    $index++;
                    if (trim($aDescription[$i]) != "") {
                        $ship_y = $ship_y + 4.5;
                        $temp = str_replace("<br />", "", $aDescription[$i]);
                        if ($index == count($aDescription)) {

                            $pdf->MultiCell(110, 0, $temp, "", 'L', false, 0, '', $ship_y, true, 0, true);

                            $pdf->MultiCell(110, 0, "", " ", 'C', false, 0, 10, $ship_y, true, 0, true);
                            $pdf->MultiCell(20, 0, "", " ", 'C', false, 0, 120, $ship_y, true, 0, true);
                            $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 140, $ship_y, true, 0, true);
                            $pdf->MultiCell(30, 0, "", " ", 'C', false, 0, 170, $ship_y, true, 0, true);
                        } else {
                            
                            $pdf->MultiCell(110, 0, $temp, "", 'L', false, 0, '', $ship_y, true, 0, true);
                            
                        }
                    }
                } // die(); */
            }
        }
        //$ship_y = $ship_y + 2;
        $pdf->MultiCell(190, 0, "", "B", 'C', false, 0, '', $ship_y, true, 0, true);
        //we calcule the sub-total
        $_subtotal = $_subtotal + ( doubleval($detail->price) * intval($detail->quantity) );

        $index ++;
    }//foreach    
    
    
}//if
$temp_y = 0;

//****** SUB-TOTAL & TERMS TITLE***************
//***********************************************



$ship_y = $ship_y + 7;
$ship_y = checkAddPage($pdf, $ship_y, 15, 45, 35);
//$borders = 'L,R,B';
$borders = '';
$pdf->MultiCell(30, 7, '<b>' . $this->nomenclature->lang_nomenclature_subtotal . '</b>', $borders, 'C', false, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, $currency_symbol . '' . $pdf->moneyFormat($this->headDocument->parcialtotal*$currency_exchange), $borders, 'C', false, 0, 170, $ship_y, true, 0, true);

//$pdf->MultiCell(60,  7, "<b>TERMS :</b>", 0, 'L', false, 0, 10,  $ship_y, true, 0, true);    
//****** TAXES && PAYMENT WAY*******************
//*********************************************** 
$ship_y = $ship_y + 7;
$ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
$pdf->MultiCell(30, 7, '<b>' . $this->nomenclature->lang_nomenclature_taxes." ".doubleval($this->headDocument->taxes) . '% </b>', $borders, 'C', false, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, $currency_symbol." ".$pdf->moneyFormat($this->headDocument->parcialtotal*$currency_exchange*$this->headDocument->taxes/100), $borders, 'C', false, 0, 170, $ship_y, true, 0, true);



//****** FREIGHT  && DELIVER TERMS*****************
//*********************************************** 
//$pdf->SetFont('helvetica', '', 8);

if($this->langWords->lang_shipping_cost>0){
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
    $pdf->MultiCell(30, 7, '<b>' . $this->langWords->lang_shipping_cost . '</b>', $borders, 'C', false, 0, 140, $ship_y, true, 0, true);
    $pdf->MultiCell(30, 7, $currency_symbol . '' . $pdf->moneyFormat(doubleval($this->headDocument->shippingCost*$currency_exchange)), $borders, 'C', false, 0, 170, $ship_y, true, 0, true);
}

$pdf->SetFillColor(229,232,232);
$pdf->SetFont('helvetica', 'B', 9);
$ship_y = $ship_y + 7;
$ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
$pdf->MultiCell(30, 7, "<b>" . $this->nomenclature->lang_nomenclature_total . "</b>", $borders, 'C', true, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, "<b>" . $currency_symbol . '' . $pdf->moneyFormat(doubleval($this->headDocument->total*$currency_exchange)) . "</b>", $borders, 'C', true, 0, 170, $ship_y, true, 0, true);


//--------------
if ($this->credit_terms_info != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
    $pdf->MultiCell(60, 7, "<b>" . $this->credit_terms . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $pdf->MultiCell(30, 7, $this->credit_terms_info, 0, 'L', false, 0, 50, $ship_y, true, 0, true);
}

if ($this->shipping_company_info != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
    $pdf->MultiCell(60, 7, "<b>" . $this->shipping_company . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $pdf->MultiCell(30, 7, $this->shipping_company_info, 0, 'L', false, 0, 50, $ship_y, true, 0, true);
}

if ($this->purchaseorder_info != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
    $pdf->MultiCell(60, 7, "<b>" . $this->purchaseorder . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $pdf->MultiCell(30, 7, $this->purchaseorder_info, 0, 'L', false, 0, 50, $ship_y, true, 0, true);
}

if ($this->shipping_tracking_info != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
    $pdf->MultiCell(60, 7, "<b>" . $this->shipping_tracking . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $pdf->MultiCell(30, 7, $this->shipping_tracking_info, 0, 'L', false, 0, 50, $ship_y, true, 0, true);
}


//$ship_y = $ship_y + 7;  
$ship_y = checkAddPage($pdf, $ship_y);


//******  DESCRIPTION  ***********************************
//****************************************************** 
if (trim($this->headDocument->comments) != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y);

    $borders = 'L,R,B,T';

    $aLines = $pdf->getTextArray(trim($this->headDocument->comments), 145);
    $blockHeight = 5;
    if (count($aLines) > 0) {
        $blockHeight += ( count($aLines) * 4.2 ) + 0;
    }

    $pdf->MultiCell(190, 7, "<b>" . $this->langWords->lang_conditions . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $ship_y = $ship_y + 7;
    $pdf->MultiCell(190, $blockHeight, "", 0, 'L', false, 0, 10, $ship_y, true, 0, true);

    $temp_y = $ship_y;
    if ($this->headDocument->comments != "") {
        $pdf->SetFont('helvetica', '', 8);
        if (count($aLines) > 0) {
            foreach ($aLines as $line) {
                $borders = 'L,R';

                $ship_y = checkAddPage($pdf, $ship_y);

                $pdf->MultiCell(190, 7, utf8_encode($line), 0, 'J', false, 0, '', $temp_y, true, 0, true);
            }//foreach
        }//if
    }//if($product->comment !="")    
    $ship_y = $temp_y;
}//if
//******  TERMS AND CONDITIONS  ***********************************
//**************************************************************** 
if (trim($this->header->terms_conditions) != "") {
    $ship_y = $ship_y + 7;
    $ship_y = checkAddPage($pdf, $ship_y);
    $borders = 'L,R,B,T';

    $aLines = $pdf->getTextArray(trim($this->header->terms_conditions), 145);
    $blockHeight = 10;
    if (count($aLines) > 0) {
        $blockHeight += ( count($aLines) * 4.2 ) + 0;
    }

    $pdf->MultiCell(190, 7, "<b>" . $this->langwords->conditions . "</b>", 0, 'L', false, 0, 10, $ship_y, true, 0, true);
    $ship_y = $ship_y + 7;
    $pdf->MultiCell(190, $blockHeight, "", 0, 'L', false, 0, 10, $ship_y, true, 0, true);

    $temp_y = $ship_y;
    if ($this->header->terms_conditions != "") {
        if (count($aLines) > 0) {
            foreach ($aLines as $line) {
                $borders = 'L,R';

                $pdf->MultiCell(190, 7, utf8_encode($line), 0, 'J', false, 0, '', $temp_y, true, 0, true);
                $temp_y = $temp_y + 4;

                $ship_y = checkAddPage($pdf, $ship_y);
            }//foreach
        }//if
    }//if($product->comment !="")    
    $ship_y = $temp_y;
}//if
//******  TERMS AND CONDITIONS  ***********************************
//**************************************************************** 
//include_once 'footer01.php';
//****** LAST PAGE ***********************************
//*********************************************** 
$pdf->lastPage();
$file = $this->typeDocument . "_" . $this->account->name . "_" . $this->code . ".pdf";
$pdf->Output($file, 'I');

function replace_carriage_return($replace, $string) {
    return str_replace(array("\n\r", "\n", "\r"), $replace, $string);
}

function checkAddPage($pdf, $position, $minuspage = 15, $bloqueAlto = 0, $start = 20) {

    $minusPageHeight = 40;
    if ($pdf->getPage() == 1) {
        $minusPageHeight = 40;
    }
    if ($pdf->getPage() == 1) {
        $minusPageHeight = 40;
    }

    if ($pdf->GetY() + $bloqueAlto >= $pdf->getPageHeight() - $minusPageHeight) {
        $pdf->AddPage();
        $pdf->Ln($start);
        $position = $start;
        $pdf->setCellMargins(0, 0, 0, 0);
    }

    return $position;
}
