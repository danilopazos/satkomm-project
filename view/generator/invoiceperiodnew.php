<?php

$pdf = new CustomPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


$pdf->waterMark = $this->waterMark;
$pdf->langWords = $this->langWords;
$pdf->imageLogo = $this->blobcontent;
$pdf->companyInfo = $this->account;
$pdf->documentNumberText = $this->langwords->documentQuoteNumberText;
$pdf->header = $this->headDocument;
ob_clean();
$pdf->documentNro = $this->code;
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('DOCUMENT');
$pdf->SetTitle('DOCUMENT ' . strtoupper($this->typeDocument) . ' - ' . $this->headDocument->accountName . " - " . $this->code);
$pdf->SetSubject('DOCUMENT' . $this->headDocument->accountName);
$pdf->SetKeywords('DOCUMENT' . $this->headDocument->accountName);

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(10, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

$pdf->SetFont('helvetica', '', 9);
$pdf->SetFillColor(255, 255, 255);

$pdf->startPageGroup();

$pdf->AddPage();
$pageHeight = $pdf->getPageHeight();

//inicialize
$voiceconsumption = $this->voiceconsumption;
$dataconsumption = $this->dataconsumption;
$invoicetotalammount = ($this->headDocument->total + $voiceconsumption + $dataconsumption);

/*
//para integración
$base_url=str_replace("view\generator", "util/TCPDF-master/image/", __DIR__);
$header_image=$base_url.$this->logo_header;
$invoice_image=$base_url."invoice_art.png";
$payment_image=$base_url."credit_cards.png";
*/


//para producción
$base_url = "/var/www/html/www.intelkomm.com/lib/util/TCPDF-master/image/";
$header_image = $base_url."logo_satkomm.png";
$invoice_image = $base_url."invoice_art.png";
$payment_image = $base_url."credit_cards.png";


$pdf->Image($header_image, 10, 10, 64, 24, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);

$_subtotal = 0;
$_taxes = doubleval($this->headDocument->taxes);
$_freight = doubleval($this->header->s_h_amount);

$currency_symbol = $this->currencyIso." ";
$currency_exchange = $this->currencyExchange;

//-----------CUSTOMER DETAILS ------------------------------
$pdf->Ln(14);
$pdf->SetFont('helvetica', 'B', 9);
$pdf->MultiCell(140, 7, ($this->langWords->lang_title_customer), 0, 'L', false, 0, '', '', true, 0, true);
$pdf->SetFont('helvetica', '', 8);
$pdf->Ln(6);

$tableCustomer =  '<table cellpading="2" cellspacing="2">';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td width="150px" style="font-weight:bold;">'.($this->langWords->lang_account_customer).'</td>';
$tableCustomer .= '<td>'.($this->headDocument->numberDocument).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_namecustomer).'</td>';
$tableCustomer .= '<td>'.($this->headDocument->accountName).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_address).'</td>';
$tableCustomer .= '<td>'.($this->headDocument->billStreet).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td style="font-weight:bold;">'.($this->langWords->lang_reference_customer).'</td>';
$tableCustomer .= '<td>'.('-').'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_place).'</td>';
$tableCustomer .= '<td>'.($this->headDocument->billCity).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_city).'</td>';
$tableCustomer .= '<td>'.($this->headDocument->billState).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_state).'</td>';
$tableCustomer .= '<td>'.($this->headDocument->billState).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '<tr>';
$tableCustomer .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_companyid).'</td>';
$tableCustomer .= '<td>'.($this->headDocument->numberDocument).'</td>';
$tableCustomer .= '</tr>';
$tableCustomer .= '</table>';
$pdf->MultiCell(120, 7, $tableCustomer, 1, 'L', false, 0, '', '', true, 0, true);

//----------------INVOICE DETAILS------------------------------------------------------

$tableInvoice =  '<table cellpading="2" cellspacing="2">';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td style="font-weight:bold;">'.($this->langWords->lang_number_invoice).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td bgcolor="#E5E8E8">'.($this->code).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td>&nbsp;</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td style="font-weight:bold;">'.($this->langWords->lang_date_issued_invoice).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td bgcolor="#E5E8E8">'.date("d/m/Y", strtotime($this->headDocument->dateDocument)).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td>&nbsp;</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td style="font-weight:bold;">'.($this->langWords->lang_date_expiration_invoice).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td bgcolor="#E5E8E8">'.date("d/m/Y", strtotime($this->headDocument->dateDueDocument)).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td>&nbsp;</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td style="font-weight:bold;">'.($this->langWords->lang_period_invoice).'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td bgcolor="#E5E8E8">'.$this->headDocument->datePeriod.'</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '<tr>';
$tableInvoice .= '<td>&nbsp;</td>';
$tableInvoice .= '</tr>';
$tableInvoice .= '</table>';

$pdf->MultiCell(40, 7, $tableInvoice, 0, 'C', false, 0, '150', '40', true, 0, true);

 
//----------------INVOICE SUMMARY------------------------------------------------------

$qty_summary_details = 0;
$ship_y = 94;
$head_y = 0;
$pdf->SetFont('helvetica', 'B', 9);
$pdf->MultiCell(60, 7, ($this->langWords->lang_tittle_summary), 0, 'L', false, 0, '', $ship_y, true, 0, true);
$pdf->SetFont('helvetica', 'B', 8);
$pdf->setCellPaddings(1, 1, 1, 1);
$borders = 'L,R,T';
//$pdf->SetFillColor(128, 128, 128);
//$pdf->SetTextColor(255, 255, 255);

$ship_y = $ship_y + 6;

//$pdf->SetLineStyle(array('width' => 0, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 128, 128)));

$pdf->MultiCell(160, 5, '<b>'.($this->langWords->lang_table_description_summary).'</b>', '', 'L', true, 0, '', $ship_y, true, 0, true);
$pdf->MultiCell(30, 5, '<b>'.($this->nomenclature->lang_nomenclature_importtitle).'</b>', '', 'C', true, 0, 170, $ship_y, true, 0, true);

//$pdf->SetFillColor(255, 255, 255);
//$pdf->SetTextColor(0, 0, 0);
//$pdf->setCellPaddings(0, 0, 0, 0);

$qty_subscribers = array();

$totalProducts = count($this->detailDocument);
if ($totalProducts > 0) {

    $pdf->SetLineStyle(array('width' => 0 , 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 128, 128)));

    $indice = 0;
    foreach ($this->detailDocument as $detail) {
        $qty_summary_details ++;

        if($qty_summary_details>5){


            $ship_y += 20;
            $ship_y = checkAddPage($pdf, $ship_y, 15, 35, 35);
            $pdf->SetFont('helvetica', '', 8);
            $tableAbstract =  '<table cellpading="2" cellspacing="2">';
            $tableAbstract .= '<tr>';
            $tableAbstract .= '<td style="font-weight:bold;" width="150">'.($this->langWords->lang_account_customer).'</td>';
            $tableAbstract .= '<td width="170">'.($this->headDocument->numberDocument).'</td>';
            $tableAbstract .= '<td width="100" rowspan="6"></td>';
            $tableAbstract .= '</tr>';
            $tableAbstract .= '<tr>';
            $tableAbstract .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_namecustomer).'</td>';
            $tableAbstract .= '<td>'.($this->headDocument->accountName).'</td>';
            $tableAbstract .= '</tr>';
            $tableAbstract .= '<tr>';
            $tableAbstract .= '<td style="font-weight:bold;">'.($this->langWords->lang_number_invoice).'</td>';
            $tableAbstract .= '<td>'.($this->code).'</td>';
            $tableAbstract .= '</tr>';
            $tableAbstract .= '<tr>';
            $tableAbstract .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_total).'</td>';
            $tableAbstract .= '<td>'.$currency_symbol . ' ' . $pdf->moneyFormat(doubleval($invoicetotalammount*$currency_exchange)).'</td>';
            $tableAbstract .= '</tr>';
            $tableAbstract .= '<tr>';
            $tableAbstract .= '<td style="font-weight:bold;">'.($this->langWords->lang_date_issued_invoice).'</td>';
            $tableAbstract .= '<td>'.date("d/m/Y", strtotime($this->headDocument->dateDocument)).'</td>';
            $tableAbstract .= '</tr>';
            $tableAbstract .= '<tr>';
            $tableAbstract .= '<td style="font-weight:bold;">'.($this->langWords->lang_date_expiration_invoice).'</td>';
            $tableAbstract .= '<td>'.date("d/m/Y", strtotime($this->headDocument->dateDueDocument)).'</td>';
            $tableAbstract .= '</tr>';
            $tableAbstract .= '</table>';
            $pdf->MultiCell('140', 7, $tableAbstract, 1, 'L', false, 0, '', $ship_y, true, 0, true);

            //invoice-code //invoice-expire //total-ammount
            $expire_qr=date("dmY", strtotime($this->headDocument->dateDueDocument));
            $ammount_qr=str_replace(".", "", $this->headDocument->total);
            $code_qr=(($this->code)).($expire_qr).($ammount_qr);
            $pdf->write2DBarcode($code_qr, 'QRCODE,L', 125, ($ship_y+4), 20, 20, $style, 'N');
            $pdf->AddPage();
            $pdf->Image($header_image, 10, 10, 64, 24, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
            $ship_y=35;
            $qty_summary_details = 1;
        }

        $indice++;
        $pdf->setCellPaddings(1, 1, 1, 1);
        if ($indice == 1) {
            $ship_y = $ship_y + 5;
        } else {
            $ship_y = $ship_y + 5;
        }

        $aDescription = explode(PHP_EOL, $detail->description);
        //we get the height of the block
        $blockHeight = 4;
        if (trim($detail->codeProduct) != "") {
            $blockHeight = $blockHeight + 5;
        }
        $indiceDetail = 0;
        if (trim($detail->description) != "") {
            for ($i = 0; $i < count($aDescription); $i++) {
                $temp = str_replace("<br />", "", $aDescription[$i]);
                if (trim($temp) != "") {
                    $blockHeight = $blockHeight + 5;
                    $indiceDetail++;
                }
            }
            if ($indiceDetail == 1) {
                $blockHeight = $blockHeight + 5;
            }
        }
        $borders = '   '; //,T
        $ship_y = checkAddPage($pdf, $ship_y, 15, $blockHeight, 35);
        $status = "";
        if ($ship_y == 35) {
            $status = " - ok";
            $borders = '';
        }

        $pdf->SetFont('helvetica', '', 7);

        if ($indiceDetail > 0) {
            $blockHeight = $blockHeight +4;
        } else {
            $blockHeight = $blockHeight + 10;
        }
        
        array_push($qty_subscribers, (substr($detail->nameProduct, -14, 12)));
        $ship_y += 1;
        $pdf->MultiCell(160, $blockHeight, "<b>" .($this->langWords->lang_table_subscriber_summary). ($detail->nameProduct) . "</b>", 'T,L', 'L', false, 0, '', $ship_y, true, 0, true);
        $pdf->MultiCell(30, $blockHeight, $currency_symbol . '' . $pdf->moneyFormat((doubleval($detail->totalPrice*$currency_exchange))), 'T,R', 'C', false, 0, 170, $ship_y, true, 0, true);
        $pdf->SetFont('helvetica', '', 7);
        if (trim($detail->codeProduct) != "") {
            $borders = ' ';
            $ship_y = $ship_y + 4;
            $pdf->MultiCell(110, 8, ($detail->codeProduct), "", 'L', false, 0, '', $ship_y, true, 0, true);
            if ($indiceDetail == 0) {
                $pdf->MultiCell(110, 0, "", "B", 'C', false, 0, 10, $ship_y, true, 0, true);
                $pdf->MultiCell(20, 0, "", "B", 'C', false, 0, 120, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", "B", 'C', false, 0, 140, $ship_y, true, 0, true);
                $pdf->MultiCell(30, 0, "", "B", 'C', false, 0, 170, $ship_y, true, 0, true);
            }
        }
        if ($indiceDetail > 0) {
            $indiceAdd = $blockHeight;
            if ($indiceDetail == 1) {
                $ship_y = $ship_y + 4;
                $pdf->MultiCell(140, 0, trim($detail->description), "", 'L', false, 0, '', $ship_y, true, 0, true);
                $pdf->MultiCell(110, -1, "", "B", 'C', false, 0, 10, $ship_y + 10, true, 0, true);
                $pdf->MultiCell(20, 0, "", "B", 'C', false, 0, 120, $ship_y + 10, true, 0, true);
                $pdf->MultiCell(30, 0, "", "B", 'C', false, 0, 140, $ship_y + 10, true, 0, true);
                $pdf->MultiCell(30, 0, "", "B", 'C', false, 0, 170, $ship_y + 10, true, 0, true);
            } else {
                $index = 0; //
                for ($i = 0; $i < count($aDescription); $i++) {
                    $index++;
                    if (trim($aDescription[$i]) != "") {
                        $ship_y = $ship_y + 4;
                        $temp = str_replace("<br />", "", $aDescription[$i]);
                        if ($index == count($aDescription)) {

                            $pdf->MultiCell(110, 0, $temp, "", 'L', false, 0, '', $ship_y, true, 0, true);

                            $pdf->MultiCell(110, 0, "", "", 'C', false, 0, 10, $ship_y, true, 0, true);
                            $pdf->MultiCell(20, 0, "", "", 'C', false, 0, 120, $ship_y, true, 0, true);
                            $pdf->MultiCell(30, 0, "", "", 'C', false, 0, 140, $ship_y, true, 0, true);
                            $pdf->MultiCell(30, 0, "", "", 'C', false, 0, 170, $ship_y, true, 0, true);
                        } else {
                            $pdf->MultiCell(110, 0, $temp, "", 'L', false, 0, '', $ship_y, true, 0, true);
                        }
                    }
                } // die();
            }
        }
        $ship_y += 10;
        //we calcule the sub-total
        $_subtotal = $_subtotal + ( doubleval($detail->price) * intval($detail->quantity) );

        $index ++;
    }//foreach
}//if
$temp_y = 0;

//----------------------------TOTALS--------------------------



$ship_y = $ship_y + 6;
$ship_y = checkAddPage($pdf, $ship_y, 15, 35, 35);
//$borders = 'L,R,B';
$borders = 'B';
$pdf->MultiCell(30, 7, '<b>' . ($this->nomenclature->lang_nomenclature_subtotal) . '</b>', '', 'C', false, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, $currency_symbol . ' ' . $pdf->moneyFormat($this->headDocument->parcialtotal*$currency_exchange), '', 'C', false, 0, 170, $ship_y, true, 0, true);

$ship_y = $ship_y + 6;
$ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
$pdf->MultiCell(30, 7, '<b>'.($this->nomenclature->lang_nomenclature_taxes). doubleval($this->headDocument->taxes) . '% </b>', '', 'C', false, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, $currency_symbol . ' ' .$pdf->moneyFormat(($this->headDocument->parcialtotal*($this->headDocument->taxes/100))*$currency_exchange), '', 'C', false, 0, 170, $ship_y, true, 0, true);

if($this->countryprovider=="CO"){

    //Consumption taxes only for Colombia
    $ship_y = $ship_y + 6;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
    $pdf->MultiCell(30, 7, '<b>'.($this->nomenclature->lang_nomenclature_voicecons) . '</b>', '', 'C', false, 0, 140, $ship_y, true, 0, true);
    $pdf->MultiCell(30, 7, $currency_symbol . ' ' .$pdf->moneyFormat($voiceconsumption*$currency_exchange), '', 'C', false, 0, 170, $ship_y, true, 0, true);

    $ship_y = $ship_y + 8;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
    $pdf->MultiCell(30, 7, '<b>'.($this->nomenclature->lang_nomenclature_trafficcons) . '</b>', '', 'C', false, 0, 140, $ship_y, true, 0, true);
    $pdf->MultiCell(30, 7, $currency_symbol . ' ' .$pdf->moneyFormat($dataconsumption*$currency_exchange), '', 'C', false, 0, 170, $ship_y, true, 0, true);

    $ship_y = $ship_y + 4;
}

$ship_y = $ship_y + 6;
$ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35);
$pdf->SetFont('helvetica', 'B', 8);
$pdf->SetFillColor(229,232,232);
$pdf->MultiCell(30, 7, "<b>" . ($this->nomenclature->lang_nomenclature_total) . "</b>", '', 'C', true, 0, 140, $ship_y, true, 0, true);
$pdf->MultiCell(30, 7, "<b>" . $currency_symbol . ' ' . $pdf->moneyFormat(doubleval(($invoicetotalammount*$currency_exchange))) . "</b>", '', 'C', true, 0, 170, $ship_y, true, 0, true);


$ship_y += 20;
$ship_y = checkAddPage($pdf, $ship_y, 15, 35, 35);
$pdf->SetFont('helvetica', '', 8);
$tableAbstract =  '<table cellpading="2" cellspacing="2">';
$tableAbstract .= '<tr>';
$tableAbstract .= '<td style="font-weight:bold;" width="150">'.($this->langWords->lang_account_customer).'</td>';
$tableAbstract .= '<td width="170">'.($this->headDocument->numberDocument).'</td>';
$tableAbstract .= '<td width="100" rowspan="6"></td>';
$tableAbstract .= '</tr>';
$tableAbstract .= '<tr>';
$tableAbstract .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_namecustomer).'</td>';
$tableAbstract .= '<td>'.($this->headDocument->accountName).'</td>';
$tableAbstract .= '</tr>';
$tableAbstract .= '<tr>';
$tableAbstract .= '<td style="font-weight:bold;">'.($this->langWords->lang_number_invoice).'</td>';
$tableAbstract .= '<td>'.($this->code).'</td>';
$tableAbstract .= '</tr>';
$tableAbstract .= '<tr>';
$tableAbstract .= '<td style="font-weight:bold;">'.($this->nomenclature->lang_nomenclature_total).'</td>';
$tableAbstract .= '<td>'.$currency_symbol . ' ' . $pdf->moneyFormat(doubleval($invoicetotalammount*$currency_exchange)).'</td>';
$tableAbstract .= '</tr>';
$tableAbstract .= '<tr>';
$tableAbstract .= '<td style="font-weight:bold;">'.($this->langWords->lang_date_issued_invoice).'</td>';
$tableAbstract .= '<td>'.date("d/m/Y", strtotime($this->headDocument->dateDocument)).'</td>';
$tableAbstract .= '</tr>';
$tableAbstract .= '<tr>';
$tableAbstract .= '<td style="font-weight:bold;">'.($this->langWords->lang_date_expiration_invoice).'</td>';
$tableAbstract .= '<td>'.date("d/m/Y", strtotime($this->headDocument->dateDueDocument)).'</td>';
$tableAbstract .= '</tr>';
$tableAbstract .= '</table>';
$pdf->MultiCell('140', 7, $tableAbstract, 1, 'L', false, 0, '', $ship_y, true, 0, true);

//invoice-code //invoice-expire //total-ammount
$expire_qr=date("dmY", strtotime($this->headDocument->dateDueDocument));
$ammount_qr=str_replace(".", "", $this->headDocument->total);
$code_qr=(($this->code)).($expire_qr).($ammount_qr);
$pdf->write2DBarcode($code_qr, 'QRCODE,L', 125, ($ship_y+4), 20, 20, $style, 'N');


//-----------------------PAYMENT INFORMATION TEXT---------------------------------------------------
$pdf->SetPrintHeader(false);
$pdf->AddPage();

$pdf->SetFont('helvetica', '', 9);
$tableInformation =  '<table cellpading="2" cellspacing="2">';
$tableInformation .= '<tr>';
$tableInformation .= '<td width="360" style="font-size:14px;"><b>CONCEPTOS FACTURABLES</b></td>';
$tableInformation .= '<td width="290" rowspan="3" align="right"><br /><br /><img src="'.$invoice_image.'" width="280"></td>';
$tableInformation .= '</tr>';
$tableInformation .= '<tr>';
$tableInformation .= '
                    <td><p align="justify"><b>Activación del Servicio</b><br />
                        Cargo facturado al cliente por única vez cuando se activa el servicio del teléfono.
                        <br /><br />
                        <b>Cargo Fijo Mensual</b><br />
                        Cargo mensual facturado al cliente por mantenimiento de cada teléfono. Cargo promocional calculado desde la fecha de inicio del servicio hasta el siguiente cierre de facturación.
                        <br /><br />
                        <b>Plan Mensual</b><br />
                        Cargo mensual factura al cliente por plan contratado de cada teléfono. Cargo promocional calculado desde la fecha de inicio del servicio hasta el siguiente cierre de facturación.
                        <br /><br />
                        <b>Cargo por Llamadas Adicionales</b><br />
                        Cargo por tráfico de voz, datos y mensaje de texto que no se encuentren comprendidos dentro del plan.
                        <br /><br />
                        <b>Otros Cargos</b><br />
                        <b>Cargo de reconexión:</b> Cargo facturado si el cliente cancela un recibo después de habérsele cortado por deuda.
                        <br /><br />
                        <b>Renta Fraccionaria por Cambio de Plan:</b> Cargo facturando si durante el ciclo de facturación el cliente cambió de plan tarifario mensual distinto al que ya tenía.
                        <br /><br />
                        <b>Servicio de Telefonía:</b> Cargo factura correspondiente al tráfico y a otros conceptos mensuales generados por un determinado contrato suscrito al cliente.
                    </p></td>';
$tableInformation .= '</tr>';
$tableInformation .= '<tr><td colspan="2">&nbsp;</td></tr>';
$tableInformation .= '<tr>';
$tableInformation .= '<td colspan="2" style="font-size:14px;"><b>NOTAS AL CLIENTE</b></td>';
$tableInformation .= '</tr>';
$tableInformation .= '<tr>';
$tableInformation .= '<td colspan="2"><p align="justify">AS TELECOM le informa que en caso de uso indebido o fraudulento del servicio contratado, ASTELECOM podrá resolver el contrato, suspender y/o cortar definitivamente el servicio de una o de todas las líneas contratadas, así como rechazar contratar con usted en el futuro.
Documento válido para efectos tributarios según Resolución SUNAT N° 007-99/SUNAT. Documento no afecto al Régimen de Retención del I.G.V. según Resolución N° 037-2002/SUNAT.
                        </p>
                    </td>';
$tableInformation .= '</tr>';
$tableInformation .= '<tr><td colspan="2">&nbsp;</td></tr>';
$tableInformation .= '<tr>';
$tableInformation .= '<td colspan="2" style="font-size:14px;"><b>LUGARES DE PAGO</b></td>';
$tableInformation .= '</tr>';
$tableInformation .= '<tr>';
$tableInformation .= '<td colspan="2"><p align="justify">Av. 2 de Mayo 516 Dpto. 201 Miraflores, Lima - Perú</p></td>';
$tableInformation .= '</tr>';
$tableInformation .= '<tr><td colspan="2">&nbsp;</td></tr>';
$tableInformation .= '<tr>';
$tableInformation .= '<td colspan="2" style="font-size:14px;"><b>MEDIOS DE PAGO</b></td>';
$tableInformation .= '</tr>';
$tableInformation .= '<tr>';
$tableInformation .= '<td colspan="2"><p align="justify">BBVA - BANCO CONTINENTAL CUENTA CORRIENTE EN SOLES 0011-0341-0100028381<br />
                        CCI 011-341-000100028381-50
                        <br /><br />
                        BBVA - BANCO CONTINENTAL CUENTA CORRIENTE EN DÓLARES 0011-0359-0100026115<br />
                        CCI 011-359-000100026115-74
                        <br /><br />
                        SCT - BANCO SCOTIABANK CUENTA CORRIENTE EN SOLES 0007-7003021<br />
                        CCI 009-234-000007003021-68
                        <br /><br />
                        SCT - BANCO SCOTIABANK CUENTA CORRIENTE EL DÓLARES 000-4924708<br />
                        CCI 009-234-000004924708-63
                        </p></td>';
$tableInformation .= '</tr>';
$tableInformation .= '<tr><td colspan="2">&nbsp;</td></tr>';
$tableInformation .= '<tr>';
$tableInformation .= '<td colspan="2" style="font-size:14px;"><b>TARJETAS DE CRÉDITO ACEPTADAS</b></td>';
$tableInformation .= '</tr>';
$tableInformation .= '<tr><td colspan="2"><img src="'.$payment_image.'" width="240"></td></tr>';
$tableInformation .= '</table>';

$pdf->MultiCell('190', 0, $tableInformation, 0, 'L', false, 0, '', 14, true, 0, true);



//---------------------------INVOICE MOVEMENTS DETAILS-------------------------------------
$pdf->SetPrintHeader(true);
$pdf->AddPage();
$movimientos = $this->headDocument->movementsInvoice;
$pdf->Image($header_image, 10, 10, 64, 24, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);

$ship_y = 35;

$pdf->SetFont('helvetica', 'B', 9);
$pdf->MultiCell(60, 7, ($this->langWords->lang_operation_details), 0, 'L', false, 0, '', $ship_y, true, 0, true);
$ship_y = $ship_y + 5;

foreach($qty_subscribers as $subscriber){
    $summary_totals = array();
    $minutes_total = 0;
    $aditional_cost = 0;

    $ship_y = $ship_y + 5;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 10, 35, $header_image);
    $pdf->SetFont('helvetica', 'B', 8);
    $pdf->MultiCell(60, 7, ($this->langWords->lang_table_subscriber_summary.$subscriber), 0, 'L', false, 0, '', $ship_y, true, 0, true);
    $pdf->SetFont('helvetica', 'B', 7);
    $ship_y = $ship_y + 7;
    $pdf->MultiCell('20', 3, $this->langWords->lang_tbtitle_date, 'B', 'C', true, 0, '', $ship_y, true, 0, true);
    $pdf->MultiCell('20', 3, $this->langWords->lang_tbtitle_time, 'B', 'C', true, 0, '30', $ship_y, true, 0, true);
    $pdf->MultiCell('30', 3, $this->langWords->lang_tbtitle_subscriber, 'B', 'C', true, 0, '50', $ship_y, true, 0, true);
    $pdf->MultiCell('35', 3, $this->langWords->lang_tbtitle_destination, 'B', 'C', true, 0, '80', $ship_y, true, 0, true);
    $pdf->MultiCell('35', 3, $this->langWords->lang_tbtitle_type, 'B', 'C', true, 0, '115', $ship_y, true, 0, true);
    $pdf->MultiCell('20', 3, $this->langWords->lang_tbtitle_duration, 'B', 'C', true, 0, '150', $ship_y, true, 0, true);
    $pdf->MultiCell('20', 3, $this->langWords->lang_tbtitle_aditional, 'B', 'C', true, 0, '170', $ship_y, true, 0, true);
    $ship_y = $ship_y + 5;
    
    foreach($movimientos as $movimiento){
        
        
        if($subscriber == $movimiento->subscriber){
            $i = 0;
            $array_key = 0;
            foreach($summary_totals as $detail){
                $i ++;
                if($detail == utf8_decode($movimiento->plan)){
                    $array_key = $i;
                }
            }
            if($array_key > 0){
                $array_qty = ((int)$summary_totals[$array_key][0]) + $movimiento->qty_duration;
                $array_ammount = ((float)$summary_totals[$array_key][1]) + $movimiento->additionalCost;
                $array_replace = array($array_key => array(0 => $array_qty, 1 => $array_ammount));
                $summary_totals = array_replace($summary_totals, $array_replace);
            }else{
                array_push($summary_totals, utf8_decode($movimiento->plan));
                array_push($summary_totals, array(0 => $movimiento->qty_duration, 1 => $movimiento->additionalCost));
            }

            $pdf->SetFont('helvetica', '', 7);
            $pdf->MultiCell('20', 3, date("d/m/Y", strtotime($movimiento->date)), 'B', 'C', false, 0, '', $ship_y, true, 0, true);
            $pdf->MultiCell('20', 3, $movimiento->time, 'B', 'C', false, 0, '30', $ship_y, true, 0, true);
            $pdf->MultiCell('30', 3, $movimiento->subscriber, 'B', 'C', false, 0, '50', $ship_y, true, 0, true);
            $pdf->MultiCell('35', 3, $movimiento->numberDestination, 'B', 'C', false, 0, '80', $ship_y, true, 0, true);
            $pdf->MultiCell('35', 3, utf8_decode($movimiento->plan), 'B', 'C', false, 0, '115', $ship_y, true, 0, true);
            $pdf->MultiCell('20', 3, $movimiento->qty_duration.utf8_decode($movimiento->duration), 'B', 'C', false, 0, '150', $ship_y, true, 0, true);
            $pdf->MultiCell('20', 3, $currency_symbol.number_format(($movimiento->additionalCost*$currency_exchange),2), 'B', 'C', false, 0, '170', $ship_y, true, 0, true);
            $ship_y = $ship_y + 5;
            $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35, $header_image);
            $aditional_cost = $aditional_cost + $movimiento->additionalCost;
            if($movimiento->duration == " MINUTE"){
                $minutes_total = $minutes_total + $movimiento->qty_duration;
            }
        }
    }
    $pdf->SetFont('helvetica', 'B', 8);
    $ship_y = $ship_y + 2;
    $ship_y = checkAddPage($pdf, $ship_y, 15, 0, 35, $header_image);
    $cn_array = count($summary_totals);
    $i = 0;
    $total_aditional = 0;
    while($i<$cn_array){
        $total_aditional = $total_aditional + (float)$summary_totals[$i][1];
        $i ++;
    }
    if($total_aditional > 0){
        $pdf->MultiCell('80', 3, "<b>".$this->langWords->lang_summary_comsuption_ad."<b>", 'R,L,B', 'C', true, 0, '110', $ship_y, true, 0, true);
        $ship_y = $ship_y + 6;
        $pdf->SetFont('helvetica', '', 7);
        $i = 0;
        while($i<$cn_array){
            $pdf->MultiCell('40', 3, $summary_totals[$i], 'B', 'C', false, 0, '110', $ship_y, true, 0, true);
            $i ++;
            $pdf->MultiCell('20', 3, $summary_totals[$i][0], 'B', 'C', false, 0, '150', $ship_y, true, 0, true);
            $pdf->MultiCell('20', 3, $currency_symbol.number_format(($summary_totals[$i][1]*$currency_exchange), 2), 'B', 'C', false, 0, '170', $ship_y, true, 0, true);
            $ship_y = $ship_y + 6;
            $i ++;
        }
        $pdf->MultiCell('20', 3, "<b>".$this->langWords->lang_total_title."</b>", '', 'C', false, 0, '150', $ship_y, true, 0, true);
        $pdf->MultiCell('20', 3, "<b>".$currency_symbol.number_format(($total_aditional*$currency_exchange), 2)."</b>", '', 'C', true, 0, '170', $ship_y, true, 0, true);
        $ship_y = $ship_y + 5;
    }else{
        $pdf->MultiCell('80', 3, "<b>".$this->langWords->lang_summary_no_comsuption_ad."<b>", 'R,L,B', 'C', false, 0, '110', $ship_y, true, 0, true);
        $ship_y = $ship_y + 5;
    }
}




$pdf->lastPage();
$file = $this->typeDocument . "_" . $this->account->name . "_" . $this->code . ".pdf";
$pdf->Output($file, 'I');












function replace_carriage_return($replace, $string) {
    return str_replace(array("\n\r", "\n", "\r"), $replace, $string);
}

function checkAddPage($pdf, $position, $minuspage = 15, $bloqueAlto = 0, $start = 20, $image = null) {

    $minusPageHeight = 40;
    if ($pdf->getPage() == 1) {
        $minusPageHeight = 40;
    }
    if ($pdf->getPage() == 1) {
        $minusPageHeight = 40;
    }

    if ($pdf->GetY() + $bloqueAlto >= $pdf->getPageHeight() - $minusPageHeight) {
        $pdf->AddPage();
        if($image!=null){
            $pdf->Image($image, 10, 10, 64, 24, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        $pdf->Ln($start);
        $position = $start;
        $pdf->setCellMargins(0, 0, 0, 0);
    }

    return $position;
}
