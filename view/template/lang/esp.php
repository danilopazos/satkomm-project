<?php

//--Validation messages-------
$_lang_validate_msg_required = "Este campo es requerido.";
$_lang_validate_msg_remote = "Por favor corrija este campo.";
$_lang_validate_msg_email = "Por favor ingrese un correo electronico valido.";
$_lang_validate_msg_url = "Por favor ingrese una URL valida";
$_lang_validate_msg_date = "Por favor ingrese una fecha valida.";
$_lang_validate_msg_dateISO = "Please enter a valid date ( ISO ).";
$_lang_validate_msg_number = "Please enter a valid number.";
$_lang_validate_msg_digits = "Please enter only digits.";
$_lang_validate_msg_equalTo = "Please enter the same value again.";
$_lang_validate_msg_maxlength = "Please enter no more than {0} characters.";
$_lang_validate_msg_minlength = "Please enter at least {0} characters.";
$_lang_validate_msg_rangelength = "Please enter a value between {0} and {1} characters long.";
$_lang_validate_msg_range = "Please enter a value between {0} and {1}.";
$_lang_validate_msg_max = "Please enter a value less than or equal to {0}.";
$_lang_validate_msg_min = "Please enter a value greater than or equal to {0}.";
$_lang_validate_msg_step = "Please enter a multiple of {0}.";
 
//-- Javascript Labels --
$_lang_dataTables_filter_search = "Busqueda basica";
$_lang_dataTables_sLengthMenu = "Mostrar _MENU_ Registros";

//-- HTML Components--------
$_lang_head_title = 'Intelkomm';
$_lang_head_description= 'Intelkomm';
$_lang_head_author = 'Intelkomm';
 

//-- Status--------
$_lang_label_status = 'Estado';
$_lang_status_active = 'Activo';
$_lang_status_inactive = 'Inactivo'; 

//-- Period ---
$_lang_period_p0 = "Periodo prepago";
$_lang_period_p12month = "12 Meses";
$_lang_period_p18month = "18 Meses";
$_lang_period_p1month = "1 Mes";
$_lang_period_p24month = "24 Meses";
$_lang_period_p3month = "3 Meses";
$_lang_period_p6month = "6 Meses";
$_lang_period_pweek = "Semana";

$_lang_postpaid = "Postpago";
$_lang_prepaid = "Prepago";


$_lang_period_option["p0"] = "Prepago";
$_lang_period_option["pweek"] = "Semanal";
$_lang_period_option["p12month"] = "12 Meses";
$_lang_period_option["p18month"] = "18 Meses";
$_lang_period_option["p1month"] = "1 Mes";
$_lang_period_option["p24month"] = "24 Meses";
$_lang_period_option["p3month"] = "3 Meses";
$_lang_period_option["p6month"] = "6 Meses";