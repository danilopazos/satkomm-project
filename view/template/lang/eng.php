<?php

//--Validation messages-------
$_lang_validate_msg_required = "This field is required.";
$_lang_validate_msg_remote = "Please fix this field.";
$_lang_validate_msg_email = "Please enter a valid email address.";
$_lang_validate_msg_url = "Please enter a valid URL.";
$_lang_validate_msg_date = "Please enter a valid date.";
$_lang_validate_msg_dateISO = "Please enter a valid date ( ISO ).";
$_lang_validate_msg_number = "Please enter a valid number.";
$_lang_validate_msg_digits = "Please enter only digits.";
$_lang_validate_msg_equalTo = "Please enter the same value again.";
$_lang_validate_msg_maxlength = "Please enter no more than {0} characters.";
$_lang_validate_msg_minlength = "Please enter at least {0} characters.";
$_lang_validate_msg_rangelength = "Please enter a value between {0} and {1} characters long.";
$_lang_validate_msg_range = "Please enter a value between {0} and {1}.";
$_lang_validate_msg_max = "Please enter a value less than or equal to {0}.";
$_lang_validate_msg_min = "Please enter a value greater than or equal to {0}.";
$_lang_validate_msg_step = "Please enter a multiple of {0}.";
 
//-- Javascript Labels --
$_lang_dataTables_filter_search = "Basic Search";
$_lang_dataTables_sLengthMenu = "Show _MENU_ Rows";

//-- HTML Components--------
$_lang_head_title = 'Intelkomm';
$_lang_head_description= 'Intelkomm';
$_lang_head_author = 'Intelkomm';
 

//-- Status--------
$_lang_label_status = 'Status';
$_lang_status_active = 'Active';
$_lang_status_inactive = 'Inactive'; 

//-- Period ---
$_lang_period_p0 = "Prepaid Period";
$_lang_period_p12month = "12 Month";
$_lang_period_p18month = "18 Month";
$_lang_period_p1month = "1 Month";
$_lang_period_p24month = "24 Months";
$_lang_period_p3month = "3 Months";
$_lang_period_p6month = "6 Months";
$_lang_period_pweek = "Week";

$_lang_postpaid = "Postpaid";
$_lang_prepaid = "Prepaid";

$_lang_period_option["p0"] = "Prepaid";
$_lang_period_option["pweek"] = "Weekly";
$_lang_period_option["p12month"] = "12 Months";
$_lang_period_option["p18month"] = "18 Months";
$_lang_period_option["p1month"] = "1 Month";
$_lang_period_option["p24month"] = "24 Months";
$_lang_period_option["p3month"] = "3 Months";
$_lang_period_option["p6month"] = "6 Months";