<style type="text/css">
    .spinner {
    position: fixed;
    top: 50%;
    left: 50%;
  
    text-align:center;
    vertical-align: central;
    z-index:1234;
    overflow: auto;
    padding: 1em 1em 1em 1em;
    background-color: white;
   border-radius: 3em;
}
</style>
<div id="spinner" class="spinner" style="display:none;">
    <img id="img-spinner" src="<?php echo $this->http_host;  ?>/assets/images/loading_process.gif" alt="Loading"/>
</div>