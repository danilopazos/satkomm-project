<script type="text/javascript">
    var validate_msg_required = '<?php echo $_lang_validate_msg_required; ?>';
    var validate_msg_remote = '<?php echo $_lang_validate_msg_remote; ?>';
    var validate_msg_email = '<?php echo $_lang_validate_msg_email; ?>';
    var validate_msg_url = '<?php echo $_lang_validate_msg_url; ?>';
    var validate_msg_date = '<?php echo $_lang_validate_msg_date; ?>';
    var validate_msg_dateISO = '<?php echo $_lang_validate_msg_dateISO; ?>';
    var validate_msg_number = '<?php echo $_lang_validate_msg_number; ?>';
    var validate_msg_digits = '<?php echo $_lang_validate_msg_digits; ?>';
    var validate_msg_equalTo = '<?php echo $_lang_validate_msg_equalTo; ?>';
    var validate_msg_maxlength = '<?php echo $_lang_validate_msg_maxlength; ?>';
    var validate_msg_minlength = '<?php echo $_lang_validate_msg_minlength; ?>';
    var validate_msg_rangelength = '<?php echo $_lang_validate_msg_rangelength; ?>';
    var validate_msg_range = '<?php echo $_lang_validate_msg_range; ?>';
    var validate_msg_max = '<?php echo $_lang_validate_msg_max; ?>';
    var validate_msg_min = '<?php echo $_lang_validate_msg_min; ?>';
    var validate_msg_step = '<?php echo $_lang_validate_msg_step; ?>';

    var _lang_period_p0 = '<?php echo $_lang_period_p0; ?>';
    var _lang_period_p12month = '<?php echo$_lang_period_p12month; ?>';
    var _lang_period_p18month = '<?php echo$_lang_period_p18month; ?>';
    var _lang_period_p1month = '<?php echo$_lang_period_p1month; ?>';
    var _lang_period_p24month = '<?php echo$_lang_period_p24month; ?>';
    var _lang_period_p3month = '<?php echo$_lang_period_p3month; ?>';
    var _lang_period_p6month = '<?php echo$_lang_period_p6month; ?>';
    var _lang_period_pweek = '<?php echo$_lang_period_pweek; ?>';

    var _lang_postpaid = '<?php echo $_lang_postpaid; ?> ';
    var _lang_prepaid = '<?php echo $_lang_prepaid; ?>';
    
</script>
