<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
    <!--<![endif]-->

    <head>
        <title><?php echo $_lang_head_title; ?></title>
        <link rel="shortcut icon" href="/assets/images/favicon.png" />
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="<?php echo $_lang_head_description; ?>" name="description" />
        <meta content="<?php echo $_lang_head_author; ?>" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/bootstrap/dist/css/bootstrap.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/font-awesome/css/font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/fonts/clip-font.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/iCheck/skins/all.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/bower_components/sweetalert/dist/sweetalert.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/css/main.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->http_host; ?>/assets/css/main-responsive.min.css" />
        <link type="text/css" rel="stylesheet" media="print" href="<?php echo $this->http_host; ?>/assets/css/print.min.css" />
        <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo $this->http_host; ?>/assets/css/theme/<?php echo $this->stylecolor; ?>.min.css" />
        <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo $this->http_host; ?>/assets/css/main-custom.css" />
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

        <?php
        if (count($this->aCss) > 0) {
            foreach ($this->aCss as $css) {
                if (strpos($css, "http") > -1) {
                    echo '<link href="' . $css . '" rel="stylesheet" type="text/css" />';
                } else {
                    echo '<link href="' . $this->http_host . '/' . $css . '"" rel="stylesheet" type="text/css" />';
                }//else
            }//foreach
        }//if
        ?>

    </head>

    <body>

        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- start: LOGO -->
                    <?php
                    $this->loadWidget("toplogo");
                    ?>

                    <!-- end: LOGO -->
                </div>
                <div class="navbar-tools">
                    <!-- start: TOP NAVIGATION MENU -->
                    <ul class="nav navbar-right">
                        <!-- start: TO-DO DROPDOWN -->
                        <?php //$this->loadwidget("tododropdown"); ?>
                        <!-- end: TO-DO DROPDOWN-->
                        <!-- start: NOTIFICATION DROPDOWN -->
                        <?php //$this->loadwidget("notificationdropdown"); ?>
                        <!-- end: NOTIFICATION DROPDOWN -->
                        <!-- start: MESSAGE DROPDOWN -->
                        <?php //$this->loadwidget("messagedropdown"); ?>
                        <!-- end: MESSAGE DROPDOWN -->
                        <!-- start: USER DROPDOWN -->
                        <?php $this->loadwidget("userdropdown"); ?>
                        <!-- end: USER DROPDOWN -->
                        <!-- start: PAGE SIDEBAR TOGGLE -->
                        <li>
                            <a class="sb-toggle" href="#" style="visibility: hidden;"><i class="fa fa-outdent"></i></a>
                        </li>
                        <!-- end: PAGE SIDEBAR TOGGLE -->
                    </ul>
                    <!-- end: TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                <div class="main-navigation navbar-collapse collapse">
                    <!-- start: MAIN MENU TOGGLER BUTTON -->
                     
                    <div class="navigation-toggler">
                        <i class="clip-chevron-left"></i>
                        <i class="clip-chevron-right"></i>
                    </div>
                    <!-- end: MAIN MENU TOGGLER BUTTON -->
                    <!-- start: MAIN NAVIGATION MENU -->
                    <?php $this->loadwidget("mainnavigationmenu"); ?>
                    <!-- end: MAIN NAVIGATION MENU -->
                </div>
                <!-- end: SIDEBAR -->
            </div>

            <!-- start: PAGE -->
            <div class="main-content">

                <!-- end: SPANEL CONFIGURATION MODAL FORM -->
                <div class="container">
                    <?php
                    $this->loadContent($module, $action);
                    ?>
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <div class="footer clearfix">
            <div class="footer-inner">
                <!--
                <script>
                    document.write(new Date().getFullYear()) 
                </script> <!--&copy; --> 
                
            </div>
            <div class="footer-items">
                <span class="go-top"><i class="clip-chevron-up"></i></span>
            </div>
        </div>
        <!-- end: FOOTER -->
        <!-- start: RIGHT SIDEBAR -->
        <?php //$this->loadwidget("rightsidebar"); ?>
        <!-- end: RIGHT SIDEBAR -->
        <!--
        <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
        <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=zV8OugfLXLmfDPyhgA1nVVsLh8FewT1SEtf3yhUeX1Ev1fqWxSTm8Vw3jNxb"></script></span>
        -->
        <?php require_once '../lib/view/template/default/spinner.php'; ?>
        <?php require_once '../lib/view/template/default/validate.php'; ?>
        <?php require_once '../lib/view/template/default/jslabels.php'; ?>


        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
                    <script src="<?php echo $this->http_host; ?>/assets/bower_components/respond/dest/respond.min.js"></script>
                    <script src="<?php echo $this->http_host; ?>/assets/bower_components/Flot/excanvas.min.js"></script>
                    <script src="<?php echo $this->http_host; ?>/assets/bower_components/jquery-1.x/dist/jquery.min.js"></script>
                    <![endif]-->
        <!--[if gte IE 9]><!-->
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
        <!--<![endif]-->
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/blockUI/jquery.blockUI.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/iCheck/icheck.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/jquery.cookie/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/bower_components/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/js/min/main.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/assets/js/modules/templates/util.js"></script>


        <?php
        if (count($this->aJavascript) > 0) {
            foreach ($this->aJavascript as $javascript) {

                if (strpos($javascript, "http") > -1) {
                    echo '<script src="' . $javascript . '" type="text/javascript"></script>';
                } else {
                    echo '<script src="' . $this->http_host . '/' . $javascript . '" type="text/javascript"></script>';
                }//else
            }//foreach
        }//if
        ?>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

        <script>
                    jQuery(document).ready(function () {
                        Main.init();
                        
                        <?php
                        if ($this->bodyjquery != "") {
                           echo $this->bodyjquery;
                        }
                        ?>
                        
                    });


        </script>

    </body>

</html>

