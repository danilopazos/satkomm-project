
<div id="messageUpdateAlertAirtimeOk" class="alert alert-success no-display">
    <i class="fa fa-remove-sign"></i> <?php echo $_lang_productalert_ActionAlertOk; ?>
</div>  


<div class="row">
    <div class="col-sm-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_productalert_airtimeat50; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body  form-horizontal">  

                <form role="form" action="#" method="post" id="form-productassoc"  >    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="productid">
                            <?php echo $_lang_productalert_actionselect; ?>
                        </label>
                        <div class="col-sm-8">
                            <select name="actiontoexecute" id="actiontoexecute" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <option selected ="true" value="1"><?php echo $_lang_productalert_action_1_notifycustomer; ?></option>
                                <option value="2"><?php echo $_lang_productalert_action_2_notifycustomer; ?></option>
                                <option value="3"><?php echo $_lang_productalert_action_3_notifycustomer; ?></option>

                            </select>
                        </div>
                        <div class="col-sm-2"> 
                            <input type="hidden" name="planid" id="alertassocplanid" value="<?php echo $this->entity->idcrypt; ?>" />
                            <input type="button" id="btn_alertassocid" class="btn btnapplyaction   btn-primary" value="<?php echo $_lang_productalert_setaction; ?>" />
                        </div>

                    </div>
                </form>


            </div>
        </div>

    </div>
</div>



<div class="row">
    <div class="col-sm-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_productalert_airtimeat75; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body  form-horizontal">  

                <form role="form" action="#" method="post" id="form-productassoc"  >    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="productid">
                            <?php echo $_lang_productalert_actionselect; ?>
                        </label>
                        <div class="col-sm-8">
                            <select name="actiontoexecute" id="actiontoexecute" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <option ><?php echo $_lang_productalert_action_1_notifycustomer; ?></option>
                                <option  selected ="true" value="1" value="2"><?php echo $_lang_productalert_action_2_notifycustomer; ?></option>
                                <option value="3"><?php echo $_lang_productalert_action_3_notifycustomer; ?></option>

                            </select>
                        </div>
                        <div class="col-sm-2"> 
                            <input type="hidden" name="planid" id="alertassocplanid" value="<?php echo $this->entity->idcrypt; ?>" />
                            <input type="button" id="btn_alertassocid" class="btn btnapplyaction  btn-primary" value="<?php echo $_lang_productalert_setaction; ?>" />
                        </div>

                    </div>
                </form>


            </div>
        </div>

    </div>
</div>



<div class="row">
    <div class="col-sm-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_productalert_airtimeat90; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body  form-horizontal">  

                <form role="form" action="#" method="post" id="form-productassoc"  >    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="productid">
                            <?php echo $_lang_productalert_actionselect; ?>
                        </label>
                        <div class="col-sm-8">
                            <select name="actiontoexecute" id="actiontoexecute" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <option ><?php echo $_lang_productalert_action_1_notifycustomer; ?></option>
                                <option  value="1" value="2"><?php echo $_lang_productalert_action_2_notifycustomer; ?></option>
                                <option   selected ="true" value="3"><?php echo $_lang_productalert_action_3_notifycustomer; ?></option>

                            </select>
                        </div>
                        <div class="col-sm-2"> 
                            <input type="hidden" name="planid" id="alertassocplanid" value="<?php echo $this->entity->idcrypt; ?>" />
                            <input type="button" id="btn_alertassocid" class="btn btnapplyaction   btn-primary" value="<?php echo $_lang_productalert_setaction; ?>" />
                        </div>

                    </div>
                </form>


            </div>
        </div>

    </div>
</div>

