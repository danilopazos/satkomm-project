<?php echo $_lang_register_label_sku . ': ' . $this->entity->sku; ?> <br />
<?php echo $_lang_register_label_networkcode . ': ' . $this->entity->networkcode; ?> <br />
<?php echo $_lang_register_label_contractperiodcode . ': ' . $_lang_period_option[$this->entity->contractperiodcode]; ?><br />
<?php echo $_lang_register_label_paymentperiodcode . ': ' . $_lang_period_option[$this->entity->paymentperiodcode]; ?>  <br />
<?php echo $_lang_register_label_servicetypecode . ': ' . $this->entity->servicetypecode; ?>  <br />
<?php echo html_entity_decode($_lang_register_label_subscriptionprice)  . ':  ' . moneyFormat(doubleval($this->entity->subscriptionprice)); ?>  <br />
<?php echo html_entity_decode($_lang_register_label_fixedpayment) . ':  ' . moneyFormat($this->entity->fixedpayment); ?>  <br />
<?php echo html_entity_decode($_lang_register_label_activationpayment) . ':  ' . moneyFormat($this->entity->activationpayment); ?>  <br />
<?php echo html_entity_decode($_lang_register_label_suspensionpayment) . ':  ' . moneyFormat($this->entity->suspensionpayment); ?>  <br />
<?php echo html_entity_decode($_lang_register_label_deactivationpayment) . ':  ' . moneyFormat($this->entity->deactivationpayment); ?>  <br />

<?php
if ($this->entity->description != "") {
    echo html_entity_decode($_lang_register_label_description) . ':  '; ?> <br /> <?php 
    echo html_entity_decode($this->entity->description) . '  '; ?> <br /> <?php 
}


?>
<br />
----------------------- <br />
<?php
$_total = count($this->trafficAllowance);
if($_total >0 ){
echo html_entity_decode($_lang_register_block_allowance); ?> 
<br /><?php
echo '-----------------------';?>
<br />
<?php 
}
for ($i = 0; $i < $_total; $i++) {
    $e = $this->trafficAllowance[$i];
?>
<?php
echo $e->tname . ': '.$e->qty; ?> 
<br /> <?php 
    $_totaldetail = count($e->aTrafficAllowanceDetail);
    for ($j = 0; $j < $_totaldetail; $j++) {
        $detail = $e->aTrafficAllowanceDetail[$j];
echo $detail->ttcode_source.' '.$detail->description.' '.$detail->tunitcode; ?> 
<br />  <?php 
    }
}


?>
<br />
-----------------------<br /> 
<?php
$_total = count($this->trafficSetup);
if($_total >0 ){
echo html_entity_decode($_lang_register_block_trafficsetup); ?> 
<br /><?php
echo '-----------------------';?>
<br />
<?php 
}
for ($i = 0; $i < $_total; $i++) {
    $e = $this->trafficSetup[$i];
    $chargelabel = 'USD'; if($e->chargecode!="money"){ $chargelabel = 'UNIT'; }
echo $e->description . '  '.$e->tunitcode. '  '.$chargelabel. '  '.$e->chargeammount; ?><br />  
<?php   
}

?>
<br />
-----------------------<br /> 
<?php
$_total = count($this->trafficIncrement);
if($_total >0 ){
echo html_entity_decode($_lang_register_block_increment); ?> 
<br /><?php
echo '-----------------------';?>
<br />
<?php 
}
for ($i = 0; $i < $_total; $i++) {
    $e = $this->trafficIncrement[$i];
echo $e->description . ' ('.html_entity_decode("MIN").':  '.$e->min_unitcode. '  '.$e->min_unitqty. ',  '.html_entity_decode("INC").':  '.$e->inc_unitcode. '  '.$e->inc_unitqty.')'; ?><br />  
<?php   
}