<form role="form" class="form-register   box-register" id="form-trafficincrement"  >

    <div id="errorSetupIncrementProblems" class="alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
    </div>

    <div id="messageSetupIncrementOk" class="alert alert-success no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
    </div>            

    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">


        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_trafficincrement; ?>
            <div class="panel-tools">

                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>

            </div>
        </div>


        <div class="panel-body  form-horizontal">  

            <table class="table table-bordered table-responsive" id="table_increment">
                <thead>
                <th><?php echo $_lang_trafficincrement_traffictype; ?></th>
                <th colspan="2"><?php echo $_lang_trafficincrement_minimunduration; ?></th>
                <th colspan="2"><?php echo $_lang_trafficincrement_increment; ?></th>
                <th></th>
                </thead>
                <tbody>
                    <?php
                    $totalTrafficIncrement = count($this->aTrafficIncrement);
                    $_tr = '';
                    for ($i = 0; $i < $totalTrafficIncrement; $i++) {
                        
                        $entity = $this->aTrafficIncrement[$i];
                        
                        $controls = '<input type="hidden" name="ti_deleted_' . $i . '" id="ti_deleted_' . $i . '" value="0" />';
                        //$controls .= '<a href="#" onclick="fnc_deleteincrement(\'' . $i . '\')" class="btn btn-xs btn-bricky"><i class="fa fa-trash"></i></a>';

                        $ttcontrol = '<select name="ti_traffictypecode_' . $i . '" class="form-control">';
                        $total = count($this->aTrafficType);
                        for ($x = 0; $x < $total; $x++) {
                            $e = $this->aTrafficType[$x];
                            $selected = '';
                            if($e->code == $entity->traffictypecode){
                                $selected = ' selected="true" ';
                            }
                            $ttcontrol .= '<option '.$selected.' value="' . $e->code . '">' . $e->description . '</option>';
                        }
                        $ttcontrol .= '</select>';

                        $ttmin_unitcode = '<select name="ti_min_unitcode_' . $i . '" class="form-control">';
                        $total = count($this->aTrafficUnit);
                        for ($x = 0; $x < $total; $x++) {
                            $e = $this->aTrafficUnit[$x];
                            $selected = '';
                            if($e->code == $entity->min_unitcode){
                                $selected = ' selected="true" ';
                            }                            
                            $ttmin_unitcode .= '<option '.$selected.' value="' . $e->code . '">' . $e->description . '</option>';
                        }
                        $ttmin_unitcode .= '</select>';
                        $ttmin_unitqty = '<input value="'.$entity->min_unitqty.'" type="text" name="ti_min_unitqty_' . $i . '"  id="ti_min_unitqty_' . $i . '"  class="form-control  min_unitqty" />';


                        $ttinc_unitcode = '<select name="ti_inc_unitcode_' . $i . '" class="form-control">';
                        $total = count($this->aTrafficUnit);
                        for ($x = 0; $x < $total; $x++) {
                            $e = $this->aTrafficUnit[$x];
                            $selected = '';
                            if($e->code == $entity->inc_unitcode){
                                $selected = ' selected="true" ';
                            }                                                        
                            $ttinc_unitcode .= '<option '.$selected.' value="' . $e->code . '">' . $e->description . '</option>';
                        }
                        $ttinc_unitcode .= '</select>';
                        $ttinc_unitqty = '<input value="'.$entity->inc_unitqty.'" type="text" name="ti_inc_unitqty_' . $i . '"   id="ti_inc_unitqty_' . $i . '" class="form-control inc_unitqty" />';

                        $_tr .= '<tr id="ti_tr_' . $i . '">';
                        $_tr .= '<td>' . $ttcontrol . '</td>';
                        $_tr .= '<td>' . $ttmin_unitcode . '</td>';
                        $_tr .= '<td>' . $ttmin_unitqty . '</td>';
                        $_tr .= '<td>' . $ttinc_unitcode . '</td>';
                        $_tr .= '<td>' . $ttinc_unitqty . '</td>';
                        $_tr .= '<td>' . $controls . '</td>';
                        $_tr .= '</tr>';
                    }

                    echo $_tr;
                    ?>
                </tbody>
            </table>
            <input type="hidden"  id="ttincrement_planidcrypt" name="planidcrypt" value="<?php echo $this->entity->idcrypt; ?>">
            <input type="hidden"  id="ttincrement_total" name="ttincrement_total" value="<?php echo $this->trafficincrement_total; ?>">            

            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-footer">

                            <button type="button" class="btn btn-primary " id="btn_register_trafficincrement">
                                <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                            </button>

                        </div>
                    </div>
                </div>
            </div>               
            
            
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->

</form>