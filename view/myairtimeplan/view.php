<script type="text/javascript">
    var _label_contractperiodcode = '<?php echo $_lang_register_label_contractperiodcode; ?>';
    var _label_validityperiodcode = '<?php echo $_lang_register_label_validityperiodcode; ?>';


    var _lang_error_msg_sku_error = '<?php echo $_lang_error_msg_sku_error; ?>';
    var _lang_error_msg_planname_error = '<?php echo $_lang_error_msg_planname_error; ?>';
    var _lang_error_msg_subscription_error = '<?php echo $_lang_error_msg_subscription_error; ?>';
    var _lang_error_msg_fixed_error = '<?php echo $_lang_error_msg_fixed_error; ?>';
    var _lang_error_msg_activation_error = '<?php echo $_lang_error_msg_activation_error; ?>';
    var _lang_error_msg_suspension_error = '<?php echo $_lang_error_msg_suspension_error; ?>';
    var _lang_error_msg_deactivation_error = '<?php echo $_lang_error_msg_deactivation_error; ?>';

    var _lang_error_msg_ta_name_error = '<?php echo $_lang_error_msg_ta_name_error; ?>';
    var _lang_error_msg_ts_margin_error = '<?php echo $_lang_error_msg_ts_margin_error; ?>';


</script>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-plane"></i>
                <a href="/myairtimeplan/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_register; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            echo '<a class="btn btn-xs btn-default" href="/myairtimeplan/index">';
            echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
            echo '</a>';
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->

  
<div class="modal fade" id="modalDeletePlan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div  class="alert alert-info  ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_confirmdelete_message; ?>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 

                            <a href="/myairtimeplan/delete/idcrypt/<?php echo $this->entity->idcrypt; ?>" id="btn_deletenow" class="btn btn-red">
                                <i class="fa fa-trash"></i>   <?php echo $_lang_view_label_confirmdelete; ?>
                            </a>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<div class="tabbable">
    <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#panel_info">
                <?php echo $_lang_register_label_panel_info; ?>
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#panel_simcard">
                <?php echo $_lang_register_label_panel_simcard; ?>
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#panel_devices">
                <?php echo $_lang_register_label_panel_devices; ?>
            </a>
        </li>
        
        
    </ul>
    <div class="tab-content">

        <div id="panel_info" class="tab-pane in active">

            <!-- start: PAGE CONTENT -->
            <form role="form" class="form-register   box-register" id="form-register"  >

                <div class="row">

                    <div class="col-sm-7">

                        <div id="errorProcessProblems" class="alert alert-danger no-display"></div>

                        <div id="messageProcessOk" class="alert alert-success no-display"></div>

                        <!-- start: TEXT FIELDS PANEL -->
                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_planinfo; ?>
                                <div class="panel-tools">

                                    <a class="btn btn-xs btn-link panel-expand" href="#">
                                        <i class="fa fa-resize-full"></i>
                                    </a>

                                </div>
                            </div>


                            <div class="panel-body  form-horizontal">  

                                <table class="table table-striped table-bordered table-hover table-full-width">

                                    <tr>
                                        <th width="20%"></th>
                                        <th> <?php echo $_lang_register_baseplan; ?></th>
                                        <th> <?php echo $_lang_register_planinfo; ?> </th>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_assignedcustomer; ?></th>

                                        <td colspan="2">
                                           <?php 
                                           if(intval($this->customeraccountid) > 0){
                                               echo $this->customername;
                                           }else{
                                               echo $_lang_view_label_customer_assignedall;
                                           }
                                           ?>
                                            <input type="hidden" id="customeraccountid" name="customeraccountid" value="<?php echo intval($this->customeraccountid) ; ?>" />
                                        </td>
                                    </tr>                          
                         
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_sku; ?></th>
                                        <td><?php echo $this->parent->sku; ?></td>
                                        <td><?php echo $this->entity->sku; ?></td>
                                    </tr>
                                    
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_version; ?></th>
                                        <td><?php echo $this->parent->version; ?></td>
                                        <td><?php echo $this->entity->version; ?></td>
                                    </tr>
                                    
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_aviable; ?></th>
                                        <td>
                                            
                                        <?php 
                                                                               
                                        if (intval($this->parent->aviable) == 1){
                                            echo '<a href="#" class="btn btn-green btn-xs">'.$_lang_register_label_aviable_yes.'</a>';
                                        }else{
                                            echo '<a href="#" class="btn btn-red btn-xs">'.$_lang_register_label_aviable_no.'</a>';        
                                        }
                                        
                                        ?>
                                            
                                        </td>
                                        <td> 
                                        
                                        <?php 
                                                                               
                                        if (intval($this->entity->aviable) == 1){
                                            echo '<a href="#" class="btn btn-green btn-xs">'.$_lang_register_label_aviable_yes.'</a>';
                                        }else{
                                            echo '<a href="#" class="btn btn-red btn-xs">'.$_lang_register_label_aviable_no.'</a>';        
                                        }
                                        
                                        ?>                                        
                                        
                                        
                                        </td>
                                    </tr>                                    
                                    
                                    
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_networkcode; ?></th>
                                        <td colspan="2"><?php echo $this->parent->networkcode; ?></td> 
                                    </tr>
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_contractperiodcode; ?></th>
                                        <td colspan="2">

                                            <?php echo $_lang_period_option[$this->parent->contractperiodcode]; ?>

                                        </td> 
                                    </tr>
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_paymentperiodcode; ?></th>
                                        <td colspan="2">

                                            <?php echo $_lang_period_option[$this->parent->paymentperiodcode]; ?>

                                        </td> 
                                    </tr>
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_servicetypecode; ?></th>
                                        <td colspan="2">

                                            <?php echo $this->parent->servicetypecode; ?>

                                        </td> 
                                    </tr>
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_name; ?></th>
                                        <td>
                                            <?php echo $this->parent->name; ?>

                                        </td>
                                        <td>
                                            <?php echo $this->entity->name; ?>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_subscriptionprice; ?></th>
                                        <td id="subscriptionprice_cost"><?php echo $this->parent->subscriptionprice; ?></td>
                                        <td>
                                            <?php echo $this->entity->subscriptionprice; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_fixedpayment; ?></th>
                                        <td><?php echo $this->parent->fixedpayment; ?></td>
                                        <td><?php echo $this->entity->fixedpayment; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_activationpayment; ?></th>
                                        <td id="activationpayment_cost"><?php echo $this->parent->activationpayment; ?></td>
                                        <td  ><?php echo $this->entity->activationpayment; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_suspensionpayment; ?></th>
                                        <td id="suspensionpayment_cost"><?php echo $this->parent->suspensionpayment; ?></td>
                                        <td id="suspensionpayment_cost"><?php echo $this->entity->suspensionpayment; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_deactivationpayment; ?></th>
                                        <td id="deactivationpayment_cost"><?php echo $this->parent->deactivationpayment; ?></td>
                                        <td id="deactivationpayment_cost"><?php echo $this->entity->deactivationpayment; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="20%"><?php echo $_lang_register_label_description; ?></th>
                                        <td><?php echo $this->parent->description; ?></td>
                                        <td><?php echo $this->entity->description; ?></td>
                                    </tr>
                                    
                                    <?php 
                                    if($this->totalsimcards == 0){
                                     ?>   
                                    <tr>
                                        <td colspan="2">
                                            <a href="/myairtimeplan/update/idcrypt/<?php echo $this->entity->idcrypt; ?>"  class="btn btn-primary" > <i class="fa fa-pencil"></i> &nbsp;  <?php echo $_lang_btn_edit; ?>  </a>
                                        </td>
                                        <td colspan="1">
                                            <a href="#" id="btnModalDelete"  class="btn btn-red" > <i class="fa fa-trash"></i> &nbsp; <?php echo $_lang_btn_delete; ?>  </a>
                                        </td>
                                    </tr>
                                     <?php    
                                    }else{
                                        ?>
                                        <tr>
                                            <td colspan="3" ><?php echo $_lang_btn_noteditinfo; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>




                                </table>


                            </div>
                        </div>
                        <!-- end: TEXT FIELDS PANEL -->
                    </div>

                    <div class="col-sm-5">


                        <!-- start: TEXT FIELDS PANEL -->
                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_allowance; ?>
                                <div class="panel-tools">

                                    <a class="btn btn-xs btn-link panel-expand" href="#">
                                        <i class="fa fa-resize-full"></i>
                                    </a>

                                </div>
                            </div>


                            <div class="panel-body  form-horizontal">  

                                <table class="table table-striped table-bordered table-hover table-full-width">

                                    <?php
                                    $_total = count($this->trafficAllowance);
                                    for ($i = 0; $i < $_total; $i++) {
                                        $e = $this->trafficAllowance[$i];
                                        ?>

                                        <tr><th colspan="3"><?php echo $e->tname; ?></th></tr>
                                        <tr>
                                            <td><?php echo $_lang_plan_allowancename; ?></td>
                                            <td><?php echo $e->tname; ?></td>   
                                            <td>
                                                <?php
                                                $o = $this->e_trafficAllowance[$i];
                                                echo $o->tname;
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $_lang_plan_allowanceqty; ?></td>
                                            <td><span class="allowancebaseqty" id="allowancebaseqty_<?php echo $e->id; ?>"  ><?php echo $e->qty; ?></span> </td>
                                            <td>
                                                <?php
                                                $o = $this->e_trafficAllowance[$i];
                                                echo $o->qty;
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"> 
                                                <?php
                                                $_totaldetail = count($e->aTrafficAllowanceDetail);
                                                for ($j = 0; $j < $_totaldetail; $j++) {
                                                    $detail = $e->aTrafficAllowanceDetail[$j];
                                                    ?>
                                            <tr>
                                                <td><?php echo $detail->ttcode_source; ?></td>
                                                <td><?php echo $detail->description; ?></td>
                                                <td><?php echo $detail->tunitcode; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </td> 
                                        </tr>

                                        <?php
                                    }
                                    ?>

                                </table>

                            </div>
                        </div>


                    </div>


                </div>


                <div class="row">
                    <div class="col-sm-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_trafficsetup; ?>
                                <div class="panel-tools">

                                    <a class="btn btn-xs btn-link panel-expand" href="#">
                                        <i class="fa fa-resize-full"></i>
                                    </a>

                                </div>
                            </div>


                            <div class="panel-body  form-horizontal">  
                                <?php
                                $_defaultMargin = 20;
                                ?>

                                <table class="table table-striped table-bordered table-hover table-full-width">
                                    <tr>
                                        <th colspan="3"><?php echo $_lang_register_trafficlabel; ?></th>
                                        <th><?php echo $_lang_register_trafficcharge; ?></th>
                                        <th><?php echo $_lang_register_trafficcost; ?></th>
                                        <th width="10%"  > <?php echo $_lang_register_trafficprice; ?>  </th>
                                        <th> <?php echo $_lang_register_trafficmargin; ?>  </th>
                                    </tr>
                                    <?php
                                    $_total = count($this->trafficSetup);
                                    for ($i = 0; $i < $_total; $i++) {
                                        $e = $this->trafficSetup[$i];
                                        ?>

                                        <tr> 
                                            <td><?php echo $e->ttcode_source; ?></td>
                                            <td><?php echo $e->description; ?></td>
                                            <td><?php echo $e->tunitcode; ?></td>
                                            <td><?php echo $e->chargecode; ?></td>
                                            <td><span class="ts_base" id="ts_base_<?php echo $e->ttcode_source; ?>"><?php echo $e->chargeammount; ?></span></td>
                                            <td width="10%"  > 
                                                <?php
                                                $_ts_cost = doubleval($e->chargeammount);

                                                $o = $this->e_trafficSetup[$i];
                                                $_ts_chargeammount = number_format(doubleval($o->chargeammount), 4);


                                                echo $_ts_chargeammount;
                                                ?>
                                            </td>
                                            <td> 
                                                <?php
                                                //we need to calculate the margin
                                                $_margin = number_format((($_ts_chargeammount - $_ts_cost) * 100) / $_ts_cost, 2);

                                                if ($_margin > 0) {
                                                    echo '<a href="#" id="margin_' . $e->ttcode_source . '" class="btn btn-xs btn-green">' . $_margin . '%</a>';
                                                } else {
                                                    echo '<a href="#" id="margin_' . $e->ttcode_source . '" class="btn btn-xs btn-red">' . $_margin . '%</a>';
                                                }
                                                ?>
                                            </td>
                                        </tr>

                                        <?php
                                    }
                                    ?>

                                </table>


                            </div>
                        </div>


                    </div>

                   
                </div>
                
                <div class="row">
                     <div class="col-sm-12">


                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_increment; ?>
                                <div class="panel-tools">

                                    <a class="btn btn-xs btn-link panel-expand" href="#">
                                        <i class="fa fa-resize-full"></i>
                                    </a>

                                </div>
                            </div>


                            <div class="panel-body  form-horizontal">  


                                <table class="table table-striped table-bordered table-hover table-full-width">
                                    <tr>
                                        <th colspan="2"><?php echo $_lang_plan_inctraffic; ?></th>
                                        <th colspan="2"><?php echo $_lang_plan_incmindur; ?></th>
                                        <th colspan="2"><?php echo $_lang_plan_incinc; ?></th> 
                                    </tr>
                                    <?php
                                    $_total = count($this->trafficIncrement);
                                    for ($i = 0; $i < $_total; $i++) {
                                        $e = $this->trafficIncrement[$i];
                                        ?>

                                        <tr> 
                                            <td><?php echo $e->traffictypecode; ?></td>
                                            <td><?php echo $e->description; ?></td>
                                            <td><?php echo $e->min_unitcode; ?></td>
                                            <td><?php echo $e->min_unitqty; ?></td>
                                            <td><?php echo $e->inc_unitcode; ?></td>
                                            <td><?php echo $e->inc_unitqty; ?></td> 
                                        </tr>

                                        <?php
                                    }
                                    ?>

                                </table>


                            </div>
                        </div>



                    </div>
                    
                </div>    
                
                <input type="hidden" name="parentplan" value="<?php echo $this->parentplan; ?>" />
                <input type="hidden" name="idcrypt" id="idcrypt"  value="<?php echo $this->entity->idcrypt; ?>" />

            </form>
            <!-- end: PAGE CONTENT-->

        </div>

        <div id="panel_simcard" class="tab-pane">



            <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%" id="mainTable">
                <thead>
                    <tr>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_serial; ?></th>
                        
                        <th ><?php echo $_lang_index_table_th_voicenumber; ?></th>
                        <th ><?php echo $_lang_index_table_th_datanumber; ?></th>
                        <th ><?php echo $_lang_index_table_th_sku; ?></th>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_productname; ?> </th>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_status; ?> </th>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_customername; ?> </th>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_operationdate; ?> </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td  class="hidden-xs"> - </td>
                        <td> - </td>
                        <td  class="hidden-xs"> - </td>
                        <td> - </td>
                         <td> - </td>
                        <td  class="hidden-xs"> - </td>
                        <td> - </td>
                        <td  class="hidden-xs"> - </td>
                    </tr>
                </tbody>
            </table>



        </div>
        
        <div id="panel_devices" class="tab-pane">



            <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%" id="mainTable">
                <thead>
                    <tr>
    
                        <th width="10%" ><?php echo $_lang_index_table_th_sku; ?></th>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_productname; ?> </th>
                        <th width="20%" ></th>
                </thead>
                <tbody>
                    
                    <?php
                    
                    $_totalDevices = count($this->aDevices);
                    foreach($this->aDevices as $item){
                        
                        echo '<tr>';
                        
                        echo '<td width="10%">'.$item->sku.'</td>';
                        echo '<td>'.$item->name.'</td>';
                        
                        $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyproductid/productid/'.$item->idcrypt.'/type/thumb"  >';
                        
                        echo '<td>'.$image_product.'</td>';
                        
                        echo '</tr>';
                        
                        
                    }
                    
                    ?>
                    
                
                </tbody>
            </table>



        </div>        
        
        
    </div>
</div>

