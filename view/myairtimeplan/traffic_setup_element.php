 
<form role="form" class="form-register   box-register" id="form-trafficelement"  >

    <div id="errorSetupElementProblems" class="alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
    </div>

    <div id="messageSetupElementOk" class="alert alert-success no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
    </div>            

    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">


        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_trafficelement; ?>
            <div class="panel-tools">

                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>

            </div>
        </div>


        <div class="panel-body  form-horizontal">  

            <table class="table table-bordered table-responsive" id="table_element">
                <thead>
                <th><?php echo $_lang_trafficelement_ttcode_source; ?></th>
                <th><?php echo $_lang_trafficelement_ttcode_dest; ?></th>
                <th><?php echo $_lang_trafficelement_tunitcode; ?></th>
                <th><?php echo $_lang_trafficelement_chargecode; ?></th>
                 <th><?php echo $_lang_trafficelement_chargecost; ?></th>
                  <th><?php echo $_lang_trafficelement_chargeprice; ?></th>
                <th></th>
                </thead>
                <tbody>
                    <?php
                   
                    $totalTrafficElement = count($this->aTrafficElement);
                    $_tr = '';
                    for ($i = 0; $i < $totalTrafficElement; $i++) {
                        
                        $entity = $this->aTrafficElement[$i];
                        
                        $controls = '<input type="hidden" name="te_deleted_' . $i . '" id="te_deleted_' . $i . '" value="0" />';
                        //$controls .= '<a href="#" onclick="fnc_deleteelement(\'' . $i . '\')" class="btn btn-xs btn-bricky"><i class="fa fa-trash"></i></a>';

                        $ttcode_source = '<select name="te_ttcode_source_' . $i . '" class="form-control">';
                        $total = count($this->aTrafficType);
                        for ($x = 0; $x < $total; $x++) {
                            $e = $this->aTrafficType[$x];
                            $selected = '';
                            if($e->code == $entity->ttcode_source){
                                $selected = ' selected="true" ';
                            }
                            $ttcode_source .= '<option '.$selected.' value="' . $e->code . '">' . $e->description . '</option>';
                        }
                        $ttcode_source .= '</select>';

                        
                        $ttcode_dest = '<select name="te_ttcode_dest_' . $i . '" class="form-control">';
                        $total = count($this->aTrafficType);
                        for ($x = 0; $x < $total; $x++) {
                            $e = $this->aTrafficType[$x];
                            $selected = '';
                            if($e->code == $entity->ttcode_dest){
                                $selected = ' selected="true" ';
                            }
                            $ttcode_dest .= '<option '.$selected.' value="' . $e->code . '">' . $e->description . '</option>';
                        }
                        $ttcode_dest .= '</select>';         
                        
                        
                        $tunitcode = '<select name="te_tunitcode_' . $i . '" class="form-control">';
                        $total = count($this->aTrafficUnit);
                        for ($x = 0; $x < $total; $x++) {
                            $e = $this->aTrafficUnit[$x];
                            $selected = '';
                            if($e->code == $entity->tunitcode){
                                $selected = ' selected="true" ';
                            }
                            $tunitcode .= '<option '.$selected.' value="' . $e->code . '">' . $e->description . '</option>';
                        }
                        $tunitcode .= '</select>';                           
                        

                        $chargecode = '<select name="te_chargecode_' . $i . '" class="form-control">';
                        $total = count($this->aCharge);
                        for ($x = 0; $x < $total; $x++) {
                            $e = $this->aCharge[$x];
                            $selected = '';
                            if($e->code == $entity->chargecode){
                                $selected = ' selected="true" ';
                            }
                            $chargecode .= '<option '.$selected.' value="' . $e->code . '">' . $e->name . '</option>';
                        }
                        $chargecode .= '</select>';                           
                        
                        $chargecost = '';
                        $totalParentTrafficElement = count($this->aParentTrafficElement);
                        if($totalParentTrafficElement>0){
                            foreach($this->aParentTrafficElement as $pte){
                                if(     $pte->chargecode == $entity->chargecode && $entity->tunitcode == $pte->tunitcode 
                                        && $entity->ttcode_dest == $pte->ttcode_dest  && $entity->ttcode_source == $pte->ttcode_source  ){
                                        $chargecost = '<input value="'.$pte->chargeammount.'" type="text" name="te_chargecost_' . $i . '"  id="te_chargecost_' . $i . '"  class="form-control  cost_value" />';
                                        $chargecost .= '<input value="'.$pte->chargeammount.'" type="hidden"   id="te_chargecost_' . $i . '_original"    />';

                                }
                            }
                        }
                        
                        $chargeammount = '<input value="'.$entity->chargeammount.'" type="text" name="te_chargeammount_' . $i . '"  id="te_chargeammount_' . $i . '"  class="form-control  " />';
 
                        $_tr .= '<tr id="te_tr_' . $i . '">';
                        $_tr .= '<td>' .$ttcode_source. '</td>';
                        $_tr .= '<td>' .$ttcode_dest. '</td>';
                        $_tr .= '<td>' .$tunitcode.'</td>';
                        $_tr .= '<td>' .$chargecode. '</td>';
                        $_tr .= '<td>' .$chargecost.'</td>';
                        $_tr .= '<td>' .$chargeammount.'</td>';
                        $_tr .= '<td>'.$controls. '</td>';
                        $_tr .= '</tr>';
            
                    }

                    echo $_tr;
                     
                     
                    ?>
                </tbody>
            </table>
            <input type="hidden"  id="ttelement_planidcrypt" name="planidcrypt" value="<?php echo $this->entity->idcrypt; ?>">
            <input type="hidden"  id="ttelement_total" name="ttelement_total" value="<?php echo $this->trafficelement_total; ?>">            

            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-footer">

                            <button type="button" class="btn btn-primary " id="btn_register_trafficelement">
                                <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                            </button>

                        </div>
                    </div>
                </div>
            </div>               
            
            
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->

</form>