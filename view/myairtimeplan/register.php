<script type="text/javascript">
    var _label_contractperiodcode = '<?php echo $_lang_register_label_contractperiodcode; ?>';
    var _label_validityperiodcode = '<?php echo $_lang_register_label_validityperiodcode; ?>';
</script>


<!-- register update -->
<div class="modal fade" id="alert-selectaccount" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                 <div  class="alert alert-danger ">
                    <i class="fa fa-remove-sign"></i><?php echo $_lang_msg_selectaccount; ?>
                </div> 
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?php echo $_lang_view_label_accept; ?>
                </button>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-plane"></i>
                <a href="/myairtimeplan/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_register; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
                echo '<a class="btn btn-xs btn-default" href="/myairtimeplan/index">';
                    echo '<i class="fa fa-arrow-circle-left"></i>'.$_lang_register_goback;
                echo '</a>';
            ?>
            
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<form role="form" class="form-register   box-register" id="formloadplan"  >
     
    <div class="row">
        <div class="col-sm-12">
            
            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
            </div>
            <div id="errorCodeRepeated" class="alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_coderepeated; ?>
            </div>
            <div id="errorProcessProblems" class="alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
            </div>

            <div id="messageProcessOk" class="alert alert-success no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
            </div>            
            
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_airtimeplaninfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="customeraccountid">
                            <?php echo $_lang_view_label_customer; ?>
                        </label>
                        <div class="col-sm-8">
                            
                            <select name="customeraccountid" id="customeraccountid" class="form-control">
                                <option value="0">--- <?php echo $_lang_view_label_customer_all; ?> ---</option>
                                <?php
                                $_totalcustomer = intval($this->aCustomer);
                                if ($_totalcustomer > 0) {

                                    foreach ($this->aCustomer as $customer) {

                                        echo '<option value="' . $customer->id . '">' . $customer->name . '</option>';
                                    }
                                }
                                ?>
                            </select>
                            
                        </div>
                    </div>                      
                    
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="planbase">
                            <?php echo $_lang_register_label_selectplanbase; ?>
                        </label>
                        <div class="col-sm-8">

                            <select name="parentplan" id="parentplan" class="form-control" >
                                <?php
                                if($this->aPlan){
                                    foreach($this->aPlan as $plan){
                                        echo '<option value="'.$plan->idcrypt.'">'.$plan->name.'</option>';
                                    }
                                }
                                ?>
                                
                            </select>
                        </div>
                    </div>
            
      

                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>

     

    <div class="row">
        <div class="col-sm-12">


            <div class="panel panel-default">
                <div class="panel-footer">

                    <a href="#" class="btn btn-primary " id="btnLoadPlan">
                            <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_loadthisplan; ?> 
                        </a>
                    
                </div>
            </div>


        </div>
    </div>
    
    <?php
    if($this->id != ""){
        echo '<input type="hidden" name="id" id="accountid" value="'.$this->id.'" />';
    }
    ?>

</form>
<!-- end: PAGE CONTENT-->
