<script type="text/javascript">
    var _label_contractperiodcode = '<?php echo $_lang_register_label_contractperiodcode; ?>';
    var _label_validityperiodcode = '<?php echo $_lang_register_label_validityperiodcode; ?>';


    var _lang_error_msg_sku_error = '<?php echo $_lang_error_msg_sku_error; ?>';
    var _lang_error_msg_planname_error = '<?php echo $_lang_error_msg_planname_error; ?>';
    var _lang_error_msg_subscription_error = '<?php echo $_lang_error_msg_subscription_error; ?>';
    var _lang_error_msg_fixed_error = '<?php echo $_lang_error_msg_fixed_error; ?>';
    var _lang_error_msg_activation_error = '<?php echo $_lang_error_msg_activation_error; ?>';
    var _lang_error_msg_suspension_error = '<?php echo $_lang_error_msg_suspension_error; ?>';
    var _lang_error_msg_deactivation_error = '<?php echo $_lang_error_msg_deactivation_error; ?>';

    var _lang_error_msg_ta_name_error = '<?php echo $_lang_error_msg_ta_name_error; ?>';
    var _lang_error_msg_ts_margin_error = '<?php echo $_lang_error_msg_ts_margin_error; ?>';


</script>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-plane"></i>
                <a href="/myairtimeplan/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_register; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            echo '<a class="btn btn-xs btn-default" href="/myairtimeplan/index">';
            echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
            echo '</a>';
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<form role="form" class="form-register   box-register" id="form-register"  >

    <div class="row">

        <div class="col-sm-8">
 
            <div id="errorProcessProblems" class="alert alert-danger no-display"></div>

            <div id="messageProcessOk" class="alert alert-success no-display"></div>

            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_planinfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  

                    <table class="table table-striped table-bordered table-hover table-full-width">

                        <tr>
                            <th width="20%"></th>
                            <th> <?php echo $_lang_register_baseplan; ?></th>
                            <th> <?php echo $_lang_register_planinfo; ?> </th>
                        </tr>
                        
                        
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_assignedcustomer; ?></th>
                            
                            <td colspan="2">
                               <?php 
                               if(intval($this->customeraccountid) > 0){
                                   echo $this->customername;
                               }else{
                                   echo $_lang_view_label_customer_assignedall;
                               }
                               ?>
                                <input type="hidden" id="customeraccountid" name="customeraccountid" value="<?php echo intval($this->customeraccountid) ; ?>" />
                            </td>
                        </tr>                        
                        
                        
                        
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_sku; ?></th>
                            <td><?php echo $this->parent->sku; ?></td>
                            <td>
                                <?php
                                $_sku = $this->newsku;
                                ?>
                                <input type="text" class="form-control" id="sku" name="sku" value="<?php echo $_sku; ?>" /> 
                            </td>
                        </tr>
                        
                        
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_networkcode; ?></th>
                            <td colspan="2"><?php echo $this->parent->networkcode; ?></td> 
                        </tr>
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_contractperiodcode; ?></th>
                            <td colspan="2">

                                <?php echo $_lang_period_option[$this->parent->contractperiodcode]; ?>

                            </td> 
                        </tr>
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_paymentperiodcode; ?></th>
                            <td colspan="2">

                                <?php echo $_lang_period_option[$this->parent->paymentperiodcode]; ?>

                            </td> 
                        </tr>
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_servicetypecode; ?></th>
                            <td colspan="2">

                                <?php echo $this->parent->servicetypecode; ?>

                            </td> 
                        </tr>
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_name; ?></th>
                            <td>
                                <?php echo $this->parent->name; ?>

                            </td>
                            <td>
                                <?php
                                $_plan_name = $this->parent->name;
                                ?>
                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $_plan_name; ?>" /> 
                            </td>
                        </tr> 
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_subscriptionprice; ?></th>
                            <td id="subscriptionprice_cost"><?php echo $this->parent->subscriptionprice; ?></td>
                            <td>
                                <?php
                                $_plan_subscriptionprice = doubleval($this->parent->subscriptionprice);
                                ?>
                                <input type="text" class="form-control" id="subscriptionprice" name="subscriptionprice" value="<?php echo $_plan_subscriptionprice; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_fixedpayment; ?></th>
                            <td><?php echo $this->parent->fixedpayment; ?></td>
                            <td id="fixedpayment_cost">
                                <?php
                                $_plan_fixedpayment = doubleval($this->parent->fixedpayment);
                                ?>
                                <input type="text" class="form-control" id="fixedpayment" name="fixedpayment" value="<?php echo $_plan_fixedpayment; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_activationpayment; ?></th>
                            <td id="activationpayment_cost"><?php echo $this->parent->activationpayment; ?></td>
                            <td>
                                <?php
                                $_plan_activationpayment = doubleval($this->parent->activationpayment);
                                ?>
                                <input type="text" class="form-control" id="activationpayment" name="activationpayment" value="<?php echo $_plan_activationpayment; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_suspensionpayment; ?></th>
                            <td id="suspensionpayment_cost"><?php echo $this->parent->suspensionpayment; ?></td>
                            <td>
                                <?php
                                $_plan_suspensionpayment = doubleval($this->parent->suspensionpayment);
                                ?>
                                <input type="text" class="form-control" id="suspensionpayment" name="suspensionpayment" value="<?php echo $_plan_suspensionpayment; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_deactivationpayment; ?></th>
                            <td id="deactivationpayment_cost"><?php echo $this->parent->deactivationpayment; ?></td>
                            <td>
                                <?php
                                $_plan_deactivationpayment = doubleval($this->parent->deactivationpayment);
                                ?>
                                <input type="text" class="form-control" id="deactivationpayment" name="deactivationpayment" value="<?php echo $_plan_deactivationpayment; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <th width="20%"><?php echo $_lang_register_label_description; ?></th>
                            <td><?php echo $this->parent->description; ?></td>
                            <td>
                                <?php
                                $_plan_description = trim($this->parent->description);
                                ?>
                                <textarea id="description" name="description" class="form-control"><?php echo $_plan_description; ?></textarea>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3">
                                <a href="#" id="btnSavePlan" class="btn btn-primary" > <?php echo $_lang_btn_save;  ?>  </a>
                            </td>
                        </tr>

                    </table>


                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>

        <div class="col-sm-4">


            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_allowance; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  

                    <table class="table table-striped table-bordered table-hover table-full-width">

                        <?php
                        $_total = count($this->trafficAllowance);
                        for ($i = 0; $i < $_total; $i++) {
                            $e = $this->trafficAllowance[$i];
                            ?>

                            <tr><th colspan="3"><?php echo $e->tname; ?></th></tr>
                            <tr>
                                <td><?php echo $_lang_plan_allowancename; ?></td>
                                <td><?php echo $e->tname; ?></td>   
                                <td>
                                <?php
                                $_allowance_name = $e->tname;
                                ?>
                                    <input type="text" class="form-control" name="allowancename_<?php echo $e->id; ?>" id="allowancename_<?php echo $e->id; ?>" value="<?php echo $_allowance_name; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $_lang_plan_allowanceqty; ?></td>
                                <td><span class="allowancebaseqty" id="allowancebaseqty_<?php echo $e->id; ?>"  ><?php echo $e->qty; ?></span> </td>
                                <td>
                                <?php
                                $_allowance_qty = intval($e->qty);
                                ?>
                                    <input class="allowanceqty"  type="text" class="form-control" name="allowanceqty_<?php echo $e->id; ?>" id="allowanceqty_<?php echo $e->id; ?>"  value="<?php echo $_allowance_qty; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"> 
                                    <table class="table table-striped table-bordered table-hover table-full-width">
                                    <?php
                                    $_totaldetail = count($e->aTrafficAllowanceDetail);
                                    for ($j = 0; $j < $_totaldetail; $j++) {
                                        $detail = $e->aTrafficAllowanceDetail[$j];
                                        ?>
                                    <tr>
                                        <td><?php echo $detail->ttcode_source; ?></td>
                                        <td><?php echo $detail->description; ?></td>
                                        <td><?php echo $detail->tunitcode; ?></td>
                                    </tr>
                                        <?php
                                    }
                                    ?>
                                    </table>
                                    
                            </td> 
                            </tr>

                                <?php
                            }
                            ?>

                    </table>

                </div>
            </div>


        </div>


    </div>


    <div class="row">
        <div class="col-sm-8">

            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_trafficsetup; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
                    <?php
                    $_defaultMargin = 20;
                    ?>

                    <table class="table table-striped table-bordered table-hover table-full-width">
                        <tr>
                            <th colspan="3"><?php echo $_lang_register_trafficlabel; ?></th>
                            <th><?php echo $_lang_register_trafficcharge; ?></th>
                            <th><?php echo $_lang_register_trafficcost; ?></th>
                            <th width="10%"  > <?php echo $_lang_register_trafficprice; ?>  </th>
                            <th> <?php echo $_lang_register_trafficmargin; ?>  </th>
                        </tr>
                            <?php
                            $_total = count($this->trafficSetup);
                            for ($i = 0; $i < $_total; $i++) {
                                $e = $this->trafficSetup[$i];
                                ?>

                            <tr> 
                                <td><?php echo $e->ttcode_source; ?></td>
                                <td><?php echo $e->description; ?></td>
                                <td><?php echo $e->tunitcode; ?></td>
                                <td><?php echo $e->chargecode; ?></td>
                                <td><span class="ts_base" id="tsbase_base_<?php echo $e->ttcode_source; ?>"><?php echo $e->chargeammount; ?></span></td>
                                <td width="10%"  > 
                                    <?php
                                    $_ts_cost  = doubleval($e->chargeammount);
                                    $_ts_price =  number_format(doubleval($_ts_cost/0.8), 2);
                                    $_margin = 20;
                                    /*
                                    $_ts_chargeammount = $_ts_cost + ($_ts_cost * ($_defaultMargin / 100) );

                                    $_ts_chargeammount = number_format($_ts_chargeammount, 4);
                                    */
                                    ?>
                                    <input type="text" class="form-control td_charge" name="ts_<?php echo $e->ttcode_source; ?>" id="ts_price_<?php echo $e->ttcode_source; ?>" value="<?php echo $_ts_price; ?>"     />   
                                </td>
                                <td> 
                                    <?php
                                    //we need to calculate the margin
                                    //$_margin = number_format((($_ts_chargeammount - $_ts_cost) * 100) / $_ts_cost, 2);

                                    if ($_margin > 0) {
                                        echo '<a href="#" id="margin_'.$e->ttcode_source.'" class="btn btn-xs btn-green">' . $_margin . '%</a>';
                                    } else {
                                        echo '<a href="#" id="margin_'.$e->ttcode_source.'" class="btn btn-xs btn-red">' . $_margin . '%</a>';
                                    }
                                    ?>
                                </td>
                            </tr>

                                    <?php
                                }
                                ?>

                    </table>


                </div>
            </div>


        </div>

        <div class="col-sm-4">


            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_increment; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  


                    <table class="table table-striped table-bordered table-hover table-full-width">
                        <tr>
                            <th colspan="2"><?php echo $_lang_plan_inctraffic; ?></th>
                            <th colspan="2"><?php echo $_lang_plan_incmindur; ?></th>
                            <th colspan="2"><?php echo $_lang_plan_incinc; ?></th> 
                        </tr>
                    <?php
                    $_total = count($this->trafficIncrement);
                    for ($i = 0; $i < $_total; $i++) {
                        $e = $this->trafficIncrement[$i];
                        ?>

                            <tr> 
                                <td><?php echo $e->traffictypecode; ?></td>
                                <td><?php echo $e->description; ?></td>
                                <td><?php echo $e->min_unitcode; ?></td>
                                <td><?php echo $e->min_unitqty; ?></td>
                                <td><?php echo $e->inc_unitcode; ?></td>
                                <td><?php echo $e->inc_unitqty; ?></td> 
                            </tr>

                        <?php
                    }
                    ?>

                    </table>


                </div>
            </div>



        </div>
    </div>
    
    <input type="hidden" name="parentplan" value="<?php echo $this->parentplan; ?>" />
    
</form>
<!-- end: PAGE CONTENT-->
