<?php
//--- View Index
$_lang_index_label_module = 'Airtime Plans';
$_lang_index_label_action = 'List'; 
$_lang_index_label_action_register = "Register";

$_lang_index_link_addairtimeplan = "Register airtime plan";

$_lang_index_table_title = "Airtime plans";

$_lang_error_msg_plan_error = "Problems, processing the plan";
$_lang_error_msg_plan_success = "Plan updated correctly";

$_lang_index_table_th_sku = "SKU";
$_lang_index_table_th_contractperiodcode = "Contract Period";
$_lang_index_table_th_networkcode = "Network";
$_lang_index_table_th_servicetypecode= "Type";
$_lang_index_table_th_name = "Name";
$_lang_index_table_th_price = "Price";


$_lang_index_table_th_subscriptionprice = "Subscription Price";
$_lang_index_table_th_activationprice = "Activation Price";


//--- View Register
$_lang_register_block_accountinfo = "Airtime Plan info";
$_lang_register_block_baseplan = "Base plan info";
$_lang_register_block_planinfo = "Informaci&oacute;n del plan";


$_lang_register_baseplan = "Informaci&oacute;n del plan base";
$_lang_register_planinfo = "Informaci&oacute;n del plan";
$_lang_register_block_increment = "Conf. increment";
$_lang_register_block_allowance = "Traffic allowance";
$_lang_register_block_trafficsetup = "Traffic additional";

$_lang_plan_allowancename = "Name";
$_lang_plan_allowanceqty = "Quantity";


$_lang_register_trafficlabel = "Traffic";
$_lang_register_trafficcharge = "Charge";
$_lang_register_trafficcost = "Cost";
$_lang_register_trafficprice = "Price";
$_lang_register_trafficmargin = "Margin";



$_lang_plan_inctraffic = "Traffic";
$_lang_plan_incmindur = "Minimun";
$_lang_plan_incinc = "Increment"; 
//--------------------------------------------



$_lang_register_goback = "Go back";
$_lang_register_save = "Save record";
$_lang_register_saveandnew = "Save and new";

$_lang_register_label_formerror  ="You have some form errors. Please check below.";
$_lang_register_label_coderepeated  ="Other Plan have the same SKU, please change it.";
$_lang_register_label_processproblems  ="The requested operation present problems, please try later";
$_lang_register_label_processok  ="Success, the artime was registered";

$_lang_register_block_airtimeplaninfo = "Airtime plan info";
$_lang_register_block_airtimeplandesc = "Airtime plan description";

$_lang_register_label_sku = "Sku";
$_lang_register_label_networkcode = "Network";
$_lang_register_label_paymentperiodcode = "Payment period";
$_lang_register_label_contractperiodcode = "Contract period";
$_lang_register_label_validityperiodcode = "Validity period";
$_lang_register_label_servicetypecode = "Service type";
$_lang_register_label_subscriptionprice = "Subscription price USD";
$_lang_register_label_fixedpayment = "Fixed payment USD";
$_lang_register_label_activationpayment = "Activation payment USD";
$_lang_register_label_name = "Name";
$_lang_register_label_intro = "Intro";
$_lang_register_label_description = "Description";
$_lang_register_label_langcode = "Language";
$_lang_register_label_createdby = "Created by";
$_lang_register_label_modifiedby = "Modified by";

$_lang_register_label_select = "Select";
$_lang_register_label_selectempty = "---";

$_lang_register_label_panel_esp = "Spanish";
$_lang_register_label_panel_eng = "English";

$_lang_register_label_panel_info = "Plan info";
$_lang_register_label_panel_trafficsetup = "Traffic Setup";
$_lang_register_label_panel_trafficallowance =  "Traffic Allowance";
$_lang_register_label_panel_productassoc =  "Devices";


$_lang_trafficincrement_traffictype = "Traffic Type";
$_lang_trafficincrement_minimunduration = "Minimun duration";
$_lang_trafficincrement_increment = "Increment";
$_lang_trafficincrement_add = "Add Increment";

$_lang_register_block_trafficincrement = "Traffic Increment";
$_lang_register_block_trafficelement = "Traffic Elements";
$_lang_register_block_trafficallowance = "Traffic Allowance";


$_lang_trafficelement_ttcode_source = "Traffic Source";
$_lang_trafficelement_ttcode_dest = "Traffic Dest";
$_lang_trafficelement_tunitcode = "Unit";
$_lang_trafficelement_chargecode = "Charge";
$_lang_trafficelement_chargeammount = "Ammount";


$_lang_trafficallowance_element = "Traffic Configuration";
$_lang_trafficallowance_qty = "Qty";
 
$_lang_device_select = 'Select device';
$_lang_device_addproduct = 'Add Product';

$_lang_lbl_deleteassocproduct = "Delete";


$_lang_lbl_sku = "Sku";
$_lang_lbl_productname = "Product";

$_lang_register_block_productassoc_delete= "Delete product";
$_lang_register_block_productassoc_msg = "When a association is delete, this product won't be related with the actual plan";
$_lang_view_label_close = "Close";
$_lang_view_label_save = "Save";
$_lang_view_label_delete = "Delete";

$_lang_register_label_suspensionpayment = "Suspension charges USD";
$_lang_register_label_deactivationpayment = "Deactivation charges USD";


$_lang_register_label_panel_alert = "Airtime Alert";
$_lang_productalert_airtimeat50 = "Airtime at 50%";
$_lang_productalert_airtimeat75 = "Airtime at 75%";
$_lang_productalert_airtimeat90 = "Airtime at 90%";
$_lang_productalert_actionselect = "Select Action to execute";


$_lang_productalert_action_1_notifycustomer = "Notify Customer";
$_lang_productalert_action_2_notifycustomer = "Notify Customer and Airtime Admin";
$_lang_productalert_action_3_notifycustomer = "Suspend Simcard";

$_lang_productalert_setaction = "Apply Action";

$_lang_productalert_ActionAlertOk = "Alert configuration saved";

$_lang_trafficelement_chargecost = "Cost";
$_lang_trafficelement_chargeprice = "Price";

$_lang_register_label_subscriptionprice_cost = 'Subscription cost';

$_lang_register_label_selectplanbase = "Select the base plan (Only for postpaid plans)";

$_lang_register_loadthisplan = "Load this plan";

$_lang_register_block_isbag = "Only if is a airtime bag";
$_lang_register_isbag = "Is a bag?";
$_lang_register_isbag_option[0] = "No, is a regular plan";
$_lang_register_isbag_option[1] = "Yes!, is a bag";

$_lang_register_bagtype= "Type:";
$_lang_register_bagtype_option[0] = "The artime included is fixed per period";
$_lang_register_bagtype_option[1] = "Each subscriber add the allowance airtime per period";
$_lang_register_bagtype_option[2] = "The airtime included is fixed per agreement duration";

$_lang_trafficallowance_name = "Description";


$_lang_error_msg_sku_error = "Ingrese un c&oacute;digo sku v&aacute;lido";
$_lang_error_msg_planname_error = "Ingrese el nombre del plan";
$_lang_error_msg_subscription_error = "El precio de la subscripci&oacute;n no puede ser menor que el costo";
$_lang_error_msg_fixed_error =   "El cargo fijo no puede ser menor que el costo";
$_lang_error_msg_activation_error =   "El precio de la activaci&oacute;n no puede ser menor que el costo";
$_lang_error_msg_suspension_error = "El precio de la suspensi&oacute;n no puede ser menor que el costo";
$_lang_error_msg_deactivation_error = "El precio de la desactivaci&oacute;n no puede ser menor que el costo";

$_lang_error_msg_ta_name_error = "Ingrese el nombre del tr&aacute;fico incluido";
$_lang_error_msg_ts_margin_error = "El precio del tr&aacute;fico adicional no puede ser menor al costo";

$_lang_btn_save = "Save plan";
$_lang_btn_edit = "Modify plan";
$_lang_btn_delete= "Delete plan";
$_lang_btn_noteditinfo= "The plan can not be deleted or modify if has simcards associated";        


$_lang_register_label_panel_simcard = "Simcard";
 
$_lang_index_table_simcards = "Simcards";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_serial = "Serial";
$_lang_index_table_th_productname = "Description";


$_lang_index_table_th_voicenumber = "Voice #";
$_lang_index_table_th_datanumber = "Data #";
$_lang_index_table_th_status = "Status";
$_lang_index_table_th_customername = "Customer";
$_lang_index_table_th_operationdate = "Operation Date";


$_lang_view_label_confirmdelete_message = "¿Are you sure to delete the plan?";

$_lang_view_label_confirmdelete ="Delete Plan";

$_lang_view_label_customer = "Assign customer";
$_lang_view_label_customer_all = "Assign to all customers";
$_lang_view_label_customer_assignedall = "Assigned to all customers";
$_lang_register_label_assignedcustomer  ="Assigned to customer";

$_lang_msg_selectaccount = "The plan created will be asigned only to the  selected customer, later will not be able to change the client";
$_lang_view_label_accept = "Accept";


$_lang_index_table_th_status = "Status";
$_lang_index_table_th_operationdate = "Operation Date";

$_lang_index_table_th_voicenumber = "Voice #";
$_lang_index_table_th_datanumber = "Data #";
$_lang_index_table_th_customername = "Customer";

$_lang_index_table_th_totalsimcards = "Total Sims";

$_lang_register_label_version = "Version";
$_lang_register_label_aviable = "Available";
 
$_lang_register_label_noaviable ="Not available";

$_lang_register_label_aviable_yes ="Available";
$_lang_register_label_aviable_no ="Not available";


$_lang_register_label_panel_devices = "Devices";