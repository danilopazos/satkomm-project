<?php
//--- View Index
$_lang_index_label_module = 'Planes';
$_lang_index_label_action = 'Lista'; 
$_lang_index_label_action_register = "Registrar";

$_lang_index_link_addairtimeplan = "Registrar plan de tiempo de aire";

$_lang_index_table_title = "Mis planes de tiempo de aire";

$_lang_error_msg_plan_error = "Problemas procesando el plan";
$_lang_error_msg_plan_success = "Plan actualizado correctamente";


$_lang_index_table_th_sku = "SKU";
$_lang_index_table_th_contractperiodcode = "Periodo de contrato";
$_lang_index_table_th_networkcode = "Red";
$_lang_index_table_th_servicetypecode= "Tipo";
$_lang_index_table_th_name = "Nombre";
$_lang_index_table_th_price = "Precio";

$_lang_index_table_th_subscriptionprice = "Precio de subscripci&oacute;n";
$_lang_index_table_th_activationprice = "Precio de activaci&oacute;n";

//--- View Register
$_lang_register_block_accountinfo = "Informaci&oacute;n del plan";
$_lang_register_block_baseplan = "Informaci&oacute;n del plan base";
$_lang_register_block_planinfo = "Informaci&oacute;n del plan";
$_lang_register_block_increment = "Conf. del incremento";
$_lang_register_block_allowance = "Trafico incluido";
$_lang_register_block_trafficsetup = "Trafico adicional";

$_lang_register_baseplan = "Informaci&oacute;n del plan base";
$_lang_register_planinfo = "Informaci&oacute;n del plan";

$_lang_register_trafficlabel = "Tr&aacute;fico";
$_lang_register_trafficcharge = "Cargo";
$_lang_register_trafficcost = "Costo";
$_lang_register_trafficprice = "Precio";
$_lang_register_trafficmargin = "Margen";



$_lang_plan_allowancename = "Nombre";
$_lang_plan_allowanceqty = "Cantidad";
 

$_lang_plan_inctraffic = "Tr&aacute;fico";
$_lang_plan_incmindur = "M&iacute;nino";
$_lang_plan_incinc = "Incremento"; 
//--------------------------------------------



$_lang_register_goback = "Volver";
$_lang_register_save = "Guardar nuevo";
$_lang_register_saveandnew = "Guardar y nuevo";

$_lang_register_label_formerror  ="Presentar algunos errores en el formulario, por favor revise";
$_lang_register_label_coderepeated  ="Otro plan presenta el mismo SKU, por favor revise.";
$_lang_register_label_processproblems  ="La presente operaci&oacute;n presenta problemas, por favor intente m&aacute;s tarde";
$_lang_register_label_processok  ="Exito, el plan fue registrado";

$_lang_register_block_airtimeplaninfo = "Informaci&oacute;n del plan";
$_lang_register_block_airtimeplandesc = "Descripci&oacute;n del plan";
$_lang_register_label_panel_productassoc =  "Equipos";


$_lang_register_label_sku = "Sku";
$_lang_register_label_networkcode = "Red";
$_lang_register_label_paymentperiodcode = "Periodo de pago";
$_lang_register_label_contractperiodcode = "Periodo de contrato";
$_lang_register_label_validityperiodcode = "Periodo de validez";
$_lang_register_label_servicetypecode = "Tipo de servicio";
$_lang_register_label_subscriptionprice = "Precio de subscripcion USD";
$_lang_register_label_fixedpayment = "Cargos fijos USD";
$_lang_register_label_activationpayment = "Cargos por activacion USD";
$_lang_register_label_name = "Nombre";
$_lang_register_label_intro = "Intro";
$_lang_register_label_description = "Descripcion";
$_lang_register_label_langcode = "Idioma";
$_lang_register_label_createdby = "Creado por";
$_lang_register_label_modifiedby = "Modificado por";

$_lang_register_label_select = "Seleccione";
$_lang_register_label_selectempty = "---";

$_lang_register_label_panel_esp = "Espa&ntilde;ol";
$_lang_register_label_panel_eng = "Ingl&eacute;s";


$_lang_register_label_panel_info = "Informaci&oacute;n del plan";
$_lang_register_label_panel_trafficsetup = "Configuraci&oacute;n del tr&aacute;fico";
$_lang_register_label_panel_trafficallowance = "Trafico incluido";

$_lang_trafficincrement_traffictype = "Tr&aacute;fico";
$_lang_trafficincrement_minimunduration = "Duraci&oacute;n m&iacute;nima";
$_lang_trafficincrement_increment = "Incremento";
$_lang_trafficincrement_add = "A&ntilde;adir incremento";

$_lang_register_block_trafficincrement = "Tr&aacute;fico (Incrementos)";
$_lang_register_block_trafficelement = "Tr&aacute;fico (Elementos)";
$_lang_register_block_trafficallowance = "Tr&aacute;fico (Incluido)";

$_lang_trafficelement_ttcode_source = "Origen";
$_lang_trafficelement_ttcode_dest = "Destino";
$_lang_trafficelement_tunitcode = "Unidades";
$_lang_trafficelement_chargecode = "Cargos";
$_lang_trafficelement_chargeammount = "Monto";
  
$_lang_trafficallowance_element = "Configuraci&oacute;n de tr&aacute;fico";
$_lang_trafficallowance_qty = "Qty";
 
$_lang_device_select = 'Seleccione el equipo';
$_lang_device_addproduct = 'A&ntilde;adir producto';

$_lang_lbl_deleteassocproduct = "Eliminar";


$_lang_lbl_sku = "Sku";
$_lang_lbl_productname = "Producto";


$_lang_register_block_productassoc_delete= "Eliminar asociaci&oacute;n";
$_lang_register_block_productassoc_msg = "Cuando se elimine la asociaci&oacute;n del producto, este plan no podr&aacute; ser asociado con el producto";
$_lang_view_label_close = "Cerrar";
$_lang_view_label_save = "Guardar";
$_lang_view_label_delete = "Eliminar";

$_lang_register_label_suspensionpayment = "Cargos por suspension USD";
$_lang_register_label_deactivationpayment = "Cargos por desactivacion USD";

$_lang_register_label_panel_alert = "Alerta Tiempo de Aire";
$_lang_productalert_airtimeat50 = "Consumo al  50%";
$_lang_productalert_airtimeat75 = "Consumo al  75%";
$_lang_productalert_airtimeat90 = "Consumo al 90%";
$_lang_productalert_actionselect = "Seleccione la opci&ouacute;n a ejecutar";

$_lang_productalert_action_1_notifycustomer = "Notidicar al Cliente";
$_lang_productalert_action_2_notifycustomer = "Notificar al Cliente y al administrador del airtime";
$_lang_productalert_action_3_notifycustomer = "Suspender Simcard";

$_lang_productalert_setaction = "Aplicar acci&oacute;n";

$_lang_productalert_ActionAlertOk = "Configuraci&oacute;n de alerta guardada";

$_lang_trafficelement_chargecost = "Costo";
$_lang_trafficelement_chargeprice = "Precio";

$_lang_register_label_subscriptionprice_cost = 'Costo de subscripci&oacute;n';

$_lang_register_label_selectplanbase = "Selecciona el plan base (solo aplica para planes postpago)";

$_lang_register_loadthisplan = "Cargar este plan";

$_lang_register_block_isbag = "Aplica solo si es una bolsa de tiempo de aire";
$_lang_register_isbag = "Es una bolsa?";
$_lang_register_isbag_option[0] = "No, es un plan regular";
$_lang_register_isbag_option[1] = "Si!, es una bolsa";

$_lang_register_bagtype= "Tipo:";
$_lang_register_bagtype_option[0] = "El tiempo de aire incluido es fijo por cada periodo";
$_lang_register_bagtype_option[1] = "Cada subscripci&oacute;n agrega tiempo incluido al tiempo de aire por periodo ";
$_lang_register_bagtype_option[2] = "El tiempo de aire incluido es fijo por contrato";

$_lang_trafficallowance_name = "Descripci&oacute;n";


$_lang_error_msg_sku_error = "Ingrese un c&oacute;digo sku v&aacute;lido";
$_lang_error_msg_planname_error = "Ingrese el nombre del plan";
$_lang_error_msg_subscription_error = "El precio de la subscripci&oacute;n no puede ser menor que el costo";
$_lang_error_msg_fixed_error =   "El cargo fijo no puede ser menor que el costo";
$_lang_error_msg_activation_error =   "El precio de la activaci&oacute;n no puede ser menor que el costo";
$_lang_error_msg_suspension_error = "El precio de la suspensi&oacute;n no puede ser menor que el costo";
$_lang_error_msg_deactivation_error = "El precio de la desactivaci&oacute;n no puede ser menor que el costo";

$_lang_error_msg_ta_name_error = "El trafico incluido no puede ser menor";
$_lang_error_msg_ts_margin_error = "El precio del trafico adicional no puede ser menor al costo";

$_lang_btn_save = "Guardar plan";
$_lang_btn_edit = "Modificar plan";
$_lang_btn_delete= "Eliminar plan";
$_lang_btn_noteditinfo= "El plan no puede ser editado o eliminado mientras hayan simcards activos asociados a &eacute;l";        


$_lang_register_label_panel_simcard = "Simcard";
 
$_lang_index_table_simcards = "Simcards";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_serial = "Serial";
$_lang_index_table_th_productname = "Descripci&oacute;n";
 

$_lang_view_label_confirmdelete_message = "¿Esta seguro de eliminar el plan?";

$_lang_view_label_confirmdelete ="Eliminar Plan";
 

$_lang_view_label_customer = "Asignar cliente";
$_lang_view_label_customer_all = "Todos los clientes";
$_lang_view_label_customer_assignedall = "Asignado a todos los clientes";
$_lang_register_label_assignedcustomer  ="Asignado al cliente";

$_lang_msg_selectaccount = "El plan ha crear solo ser&aacute; asignado al cliente seleccionado, posteriormente no podr&aacute; cambiar de cliente";
$_lang_view_label_accept = "Aceptar";

$_lang_index_table_th_status = "Estado";
$_lang_index_table_th_operationdate = "Fecha";
$_lang_index_table_th_voicenumber = "Voz #";
$_lang_index_table_th_datanumber = "Datos #";
$_lang_index_table_th_customername = "Cliente";

$_lang_index_table_th_totalsimcards = "Total Sims";

$_lang_register_label_version = "Versi&oacute;n";
$_lang_register_label_aviable ="Disponible";

$_lang_register_label_aviable_yes ="Disponible";
$_lang_register_label_aviable_no ="No Disponible";

$_lang_register_label_noaviable ="No Disponible";


$_lang_register_label_panel_devices = "Equipos";