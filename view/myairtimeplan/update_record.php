         <form role="form" class="form-register   box-register" id="form"  >
            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
            </div>
            <div id="errorCodeRepeated" class="alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_coderepeated; ?>
            </div>
            <div id="errorProcessProblems" class="alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
            </div>

            <div id="messageProcessOk" class="alert alert-success no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
            </div>            

            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_airtimeplaninfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  


                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="sku">
                            <?php echo $_lang_register_label_sku; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="sku" name="sku" class="form-control" value="<?php echo $this->entity->sku; ?>" />
                        </div>
                    </div>
                    
                  
                    
                    
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="networkcode">
                            <?php echo $_lang_register_label_networkcode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="networkcode" id="networkcode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php
                                foreach ($this->aNetwork as $network) {
                                    $selected = '';
                                    if ($this->entity->networkcode == $network->code) {
                                        $selected = ' selected = "true" ';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option ' . $selected . ' value="' . $network->code . '">' . $network->description . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="servicetypecode">
                            <?php echo $_lang_register_label_servicetypecode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="servicetypecode" id="servicetypecode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php
                                foreach ($this->aServiceType as $serviceType) {
                                    $selected = '';
                                    if ($this->entity->servicetypecode == $serviceType->code) {
                                        $selected = ' selected = "true" ';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option ' . $selected . ' value="' . $serviceType->code . '">' . $serviceType->code . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>                    

                    <div class="form-group" id="div_paymentperiodcode">
                        <label class="col-sm-6 control-label" for="paymentperiodcode">
                            <?php echo $_lang_register_label_paymentperiodcode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="paymentperiodcode" id="paymentperiodcode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php
                                foreach ($this->aPeriod as $period) {
                                    $selected = '';
                                    if ($this->entity->paymentperiodcode == $period->code) {
                                        $selected = ' selected = "true" ';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option ' . $selected . ' value="' . $period->code . '">' . periodToOption($period->code, $period->timecode, $period->qty) . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="contractperiodcode" id="lbl_contractperiodcode">
                            <?php echo $_lang_register_label_contractperiodcode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="contractperiodcode" id="contractperiodcode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php
                                foreach ($this->aPeriod as $period) {
                                    $selected = '';
                                    if ($this->entity->contractperiodcode == $period->code) {
                                        $selected = ' selected = "true" ';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option ' . $selected . '  value="' . $period->code . '">' . periodToOption($period->code, $period->timecode, $period->qty) . '</option>';
                                }
                                ?>
                            </select>                        
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="subscriptionprice">
                            <?php echo $_lang_register_label_subscriptionprice; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="subscriptionprice" name="subscriptionprice" class="form-control" value="<?php echo $this->entity->subscriptionprice; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="fixedpayment">
                            <?php echo $_lang_register_label_fixedpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="fixedpayment" name="fixedpayment" class="form-control" value="<?php echo $this->entity->fixedpayment; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="activationpayment">
                            <?php echo $_lang_register_label_activationpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="activationpayment" name="activationpayment" class="form-control" value="<?php echo $this->entity->activationpayment; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="suspensionpayment">
                            <?php echo $_lang_register_label_suspensionpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="suspensionpayment" name="suspensionpayment" class="form-control" value="<?php echo $this->entity->suspensionpayment; ?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="deactivationpayment">
                            <?php echo $_lang_register_label_deactivationpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="deactivationpayment" name="deactivationpayment" class="form-control" value="<?php echo $this->entity->deactivationpayment; ?>">
                        </div>
                    </div>                       
                    
                    
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->

            
            
            

            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_isbag; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  


                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="sku">
                            <?php echo $_lang_register_isbag; ?>
                        </label>
                        <div class="col-sm-6">
                            
                            <select name="isgroup" class="form-control">
                                
                                
                                <?php
                                    $bagOptionIndex = 0;
                                    foreach($_lang_register_isbag_option as $_isbagoption){
                                        
                                        $_selected = '';
                                        if( $this->entity->isgroup == $bagOptionIndex ){
                                            $_selected = ' selected = "true" ';
                                        }else{
                                            $_selected = '  ';
                                        }
                                        
                                        echo '<option '.$_selected.' value="'.$bagOptionIndex.'">'.$_lang_register_isbag_option[$bagOptionIndex].'</option>';
                                        
                                        $bagOptionIndex ++;
                                    }
                                
                                ?>
                                
                            </select>
                            
                            
                        </div>
                    </div>
             
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="sku">
                            <?php echo $_lang_register_bagtype; ?>
                        </label>
                        <div class="col-sm-6">
                            
                            <select name="grouptype" class="form-control">
                                
                                
                                <?php
                                    $bagOptionIndex = 0;
                                    foreach($_lang_register_bagtype_option as $_isbagtypeoption){
                                        
                                        $_selected = '';
                                        if( $this->entity->grouptype == $bagOptionIndex ){
                                            $_selected = ' selected = "true" ';
                                        }else{
                                            $_selected = '  ';
                                        }
                                        
                                        echo '<option '.$_selected.' value="'.$bagOptionIndex.'">'.$_lang_register_bagtype_option[$bagOptionIndex].'</option>';
                                        
                                        $bagOptionIndex ++;
                                    }
                                
                                ?>
                                
                            </select>
                            
                            
                        </div>
                    </div>
                    
                    
                    
                    
                    
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
            
            
            
            
            
            
            
            
            <div class="row">
                <div class="col-sm-12">


                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <input type="hidden"  id="idcrypt" name="idcrypt" value="<?php echo $this->entity->idcrypt; ?>">
                            <button type="submit" class="btn btn-primary ">
                                <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                            </button>

                        </div>
                    </div>


                </div>
            </div>            


        </form>
 




<!-- end: PAGE CONTENT-->
