<?php

//-- Catalog Index
$_lang_index_label_module = "Orden";
$_lang_index_label_action = "Detalle";

$_lang_index_label_processorder = "Procesar Orden";
$_lang_index_label_generateinvoice = "Generar Factura";
$_lang_index_label_myorders = "Mis Ordenes";
$_lang_index_label_list = "Lista";
$_lang_index_label_customerorders = "Ordenes de clientes";

$_lang_index_table_title = "Detalle de la orden";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Nombre";
$_lang_index_table_th_networkcode = "Red";
$_lang_index_table_th_price = "Precio";
$_lang_index_table_th_product = "Producto";

$_lang_view_label_close = "Cerrar";
$_lang_view_label_addcart = "A&ntilde;adir al carrito";

$_lang_register_block_ordercomments = "Comentarios de la orden";
$_lang_view_block_addcart = "A&ntilde;adir al carrito";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Paquete";
$_lang_panel_technicalspecs = "Especs";
$_lang_panel_features = "Caracter&iacute;sticas";
$_lang_panel_docs = "Documentos";

$_lang_view_label_quantity = "Cantidad";
$_lang_view_label_price = "Precio";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item a&ntilde;adido";
$_lang_view_label_subttotal = "Sub-total";
$_lang_view_label_total = "Total";


$_lang_register_block_billaddress = "Direcci&oacute;n de facturaci&oacute;n";
$_lang_register_block_shipaddress = "Direcci&oacute;n de env&iacute;o";
$_lang_register_label_country = "Pa&iacute;s";
$_lang_register_label_state = "Estado/Region";
$_lang_register_label_city = "Ciudad";
$_lang_register_label_street = "Calle";
$_lang_register_label_zipcode = "C&oacute;digo postal";
$_lang_register_label_pobox = "P.O. Box";

$_lang_register_label_processok = "Orden enviada";
$_lang_register_label_formerror = "Problemas, por favor revise los campos vacios";
$_lang_view_label_confirmorder_message = "Por favor confirme para enviar su orden, gracias";
$_lang_view_label_confirmorder = "Confirmar Orden";

$_lang_register_block_orderinfo = "Informaci&oacute;n de la orden";

$_lang_view_label_accountname = "Cuenta";
$_lang_view_label_orderno = "Orden #";
$_lang_view_label_orderdate = "Fecha de la orden";
$_lang_view_label_totalitems = "Total de art&iacute;culos";
$_lang_view_label_orderuser = "Por";
$_lang_view_label_customeraccount = "Cuenta cliente";
$_lang_label_printer = "Imprimir";


$_lang_btn_processorder = "Procesar orden";

//Invoice Labels
$_lang_view_label_invoicenum = "Factura #";
$_lang_view_label_invoiceduedate = "Fecha de vencimiento";
$_lang_view_label_invoicedate = "Fecha de la factura";

$_lang_register_block_invoiceinfo = "Informaci&oacute;n de la factura";
$_lang_register_block_invoicedetail = "Detalles de la factura";

$_lang_view_label_shipping_cost = "Costos de env&iacute;o";


$_lang_view_payment_creditcard = "Cargos por procesamiento - (Tarjeta de cr&eacute;dito)";
$_lang_view_payment_wireless = "Cargos por procesamiento - (Transferencia bancaria)";
$_lang_register_block_invoicecomments = "Comentarios de la factura";
$_lang_index_label_generateInvoice = "Generar Factura";


$_lang_view_label_dispatchdate = "Fecha de despacho";
$_lang_view_label_deliverydate = "Fecha de entrega";
$_lang_view_label_invoiceno = "Factura #";
$_lang_btn_viewinvoice = "Ver Factura";
$_lang_view_label_orderstatus = "Estado";


$_lang_index_labels = "Info";

$_lang_index_label_btn_blue = "Orden despachada";
$_lang_index_label_btn_orange = "La orden tiene factura pero no ha sido procesada";
$_lang_index_label_btn_red = "La orden a&uacute;n no ha sido despachada";
$_lang_index_label_btn_grey = "La orden fue  cancelada";

$_lang_view_label_trackinginfo = "Tracking";


$_lang_view_label_cancelorder = 'Cancelar la Orden';
$_lang_view_label_cancelorder_info = 'Usted tiene tres horas para cancelar la orden desde su creaci&oacute;n, despu&eacute;s de ese periodo de tiempo, cualquier tipo de requerimiento de cancelaci&oacute;n ser&aacute; denegado';
$_lang_view_label_cancelorder_providerinfo = 'Esta orden esta dentro del periodo de cancelaci&oacute;n';

$_lang_view_message_cancelorder_confirm = "Esta seguro de cancelar esta orden, la operaci&oacute;n es irreversible";
$_lang_view_btn_cancelordernow = "Cancelar la orden ahora";

$_lang_view_label_ordercancelled_datetime = "Esta orden fue cancelada. Fecha de cancelaci&oacute;n ";


$_lang_view_label_distributor = "CARGUERO DISTRIBUIDOR";
$_lang_view_label_shippingnotdefined = "No definido";

$_lang_panel_created = "Creadas";
$_lang_panel_readytodispatch = "Listas para procesar";
$_lang_panel_dispatched = "Procesadas";
$_lang_panel_delivered = "Despachadas";
$_lang_panel_cancelled = "Canceladas";
$_lang_view_label_dispatchnow = "Esta orden tiene requerimiento de despacho inmediato";

$_lang_email_tocustomer_subject_titlepart= "Su orden fue recibida ";
$_lang_email_tocustomer_message1 = 'Su orden fue recibida, para ver los detalles por favor haga click en el siguiente enlace ';
$_lang_email_tocustomer_message2 = 'Recuerde: Usted tiene 3 horas para cancelar la orden despues de recibida. Despu&eacute;s de ese periodo de tiempo, cualquier tipo de intenci&oacute;n de cancelaci&oacute;n ser&aacute; denegada.';
$_lang_email_tocustomer_dispatchnow = 'Esta es una orden del tipo DESPACHAR AHORA, cualquier intento de cancelaci&oacute;n ser&aacute; denegada.';



$_lang_email_toprovider_subject_titlepart= "Orden enviada ";
$_lang_email_toprovider_message1 = 'Solicitud de orden de compra, para ver los detalles por favor haga click en el siguiente enlace ';
$_lang_email_toprovider_dispatchnow = 'Esta es una orden del tipo DESPACHAR AHORA.';
$_lang_code ="Orden #: ";
$_lang_url ="Enlace: "; 