<?php

//-- Catalog Index
$_lang_index_label_module = "Order";
$_lang_index_label_action = "Detail";

$_lang_index_label_processorder = "Process Order";
$_lang_index_label_generateinvoice = "Generate Invoice";
$_lang_index_label_myorders = "My Orders";
$_lang_index_label_list = "List";
$_lang_index_label_customerorders = "Customer Orders";

$_lang_index_table_title = "Order Detail";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Name";
$_lang_index_table_th_networkcode = "Network";
$_lang_index_table_th_price = "Price";
$_lang_index_table_th_product = "Product";

$_lang_view_label_close = "Close";
$_lang_view_label_addcart = "Add Cart";

$_lang_register_block_ordercomments = "Order Comments";
$_lang_view_block_addcart = "Add Cart";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Package";
$_lang_panel_technicalspecs = "Specs";
$_lang_panel_features = "Features";
$_lang_panel_docs = "Docs";

$_lang_view_label_quantity = "Quantity";
$_lang_view_label_price = "Price";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item added";
$_lang_view_label_subttotal = "Sub-total";
$_lang_view_label_total = "Total";


$_lang_register_block_billaddress = "Billing Address";
$_lang_register_block_shipaddress = "Shipping Address";
$_lang_register_label_country = "Country";
$_lang_register_label_state = "State";
$_lang_register_label_city = "City";
$_lang_register_label_street = "Street";
$_lang_register_label_zipcode = "Zip Code";
$_lang_register_label_pobox = "P.O. Box";

$_lang_register_label_processok = "Order Placed";
$_lang_register_label_formerror = "Problems, please check the empty fields";
$_lang_view_label_confirmorder_message = "Please confirm to place your order, thanks";
$_lang_view_label_confirmorder = "Confirm Order";

$_lang_register_block_orderinfo = "Order Info";

$_lang_view_label_accountname = "Account";
$_lang_view_label_orderno = "Order #";
$_lang_view_label_orderdate = "Order Date";
$_lang_view_label_totalitems = "Total items";
$_lang_view_label_orderuser = "By";
$_lang_view_label_customeraccount = "Customer Account";
$_lang_label_printer = "Print";


$_lang_btn_processorder = "Process Order";

//Invoice Labels
$_lang_view_label_invoicenum = "Invoice #";
$_lang_view_label_invoiceduedate = "Due Date";
$_lang_view_label_invoicedate = "Invoice Date";

$_lang_register_block_invoiceinfo = "Invoice Info";
$_lang_register_block_invoicedetail = "Invoice Details";

$_lang_view_label_shipping_cost = "Shipping Cost";


$_lang_view_payment_creditcard = "Payment process fee - (Credit Card)";
$_lang_view_payment_wireless = "Payment process fee - Bank Transfer";
$_lang_register_block_invoicecomments = "Invoice Comments";
$_lang_index_label_generateInvoice = "Generate Invoice";


$_lang_view_label_dispatchdate = "Dispatch date";
$_lang_view_label_deliverydate = "Delivery date";
$_lang_view_label_invoiceno = "Invoice #";
$_lang_view_label_orderstatus = "Status";
$_lang_btn_viewinvoice = "View Invoice";


$_lang_index_labels = "Info";

$_lang_index_label_btn_blue = "Order delivered and dispatched";
$_lang_index_label_btn_orange = "Order with invoice but not delivered";
$_lang_index_label_btn_red = "Order not processed yet";
$_lang_index_label_btn_grey = "Sale order cancelled";


$_lang_view_label_trackinginfo = "Tracking";

$_lang_view_label_cancelorder = 'Cancel Order';
$_lang_view_label_cancelorder_info = 'You have three hours to cancel the order since its creation, after that period of time any kind of cancellation request will be denied';
$_lang_view_label_cancelorder_providerinfo = 'This order can be cancelled by the customer';

$_lang_view_message_cancelorder_confirm = "Are you sure to cancel this order, this operation is irreversible";
$_lang_view_btn_cancelordernow = "Cancel order now";

$_lang_view_label_ordercancelled_datetime = "This order was cancelled. Cancellation date: ";


$_lang_view_label_distributor = "FREIGHT BY DISTRIBUTOR";
$_lang_view_label_shippingnotdefined = "Not Defined";

$_lang_panel_created = "Created";
$_lang_panel_readytodispatch = "Ready to process";
$_lang_panel_dispatched = "Processed";
$_lang_panel_delivered = "Dispatched";
$_lang_panel_cancelled = "Cancelled";

$_lang_view_label_dispatchnow = "This order was selected to dispatch immediately";

$_lang_email_tocustomer_subject_titlepart= "Your order was received";
$_lang_email_tocustomer_message1 = 'Your order was received, to see the details, please click in the next link';
$_lang_email_tocustomer_message2 = 'Remember: Remember: You have 3 hours to cancel the order after its creation. After those period of time, any kind of attempt of cancellation will be denied.';
$_lang_email_tocustomer_dispatchnow = 'This is a DISPATCH NOW ORDER, any kind of attempt of cancellation will be denied.';



$_lang_email_toprovider_subject_titlepart= "Order sent ";
$_lang_email_toprovider_message1 = 'Request of sale order, to send the details plase clic in the next link ';
$_lang_email_toprovider_dispatchnow = 'This is a dispatch now order.';
$_lang_code ="Order #: ";
$_lang_url ="Link: "; 
