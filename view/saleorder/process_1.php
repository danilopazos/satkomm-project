<!--- Product Add Cart Modal Window -->
<div class="modal fade" id="confirmorder-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div id="messageProcessOk" class="alert alert-info  ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_confirmorder_message; ?>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 

                            <button type="button" id="btn_confirmorder" class="btn btn-green">
                                <i class="fa fa-check"></i>   <?php echo $_lang_view_label_confirmorder; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_processorder; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_generateinvoice; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
           
            echo '<a class="btn btn-xs   btn-default" href="/saleorder/view/code/' . $this->saleorder->code . '">';
            echo '<i class="fa fa-arrow-circle-left"></i>&nbsp;' . $_lang_index_label_module;
            echo '</a>&nbsp;';
            
            echo $_lang_view_label_orderno.'&nbsp;<strong>'.$this->saleorder->code.'</strong>&nbsp;';
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->


<form role="form" class=" form-generateinvoice  box-user-register" id="form-generateinvoice"  >

    <input type="hidden" value="<?php echo count($this->detail); ?>"  name="totalitem"  id="totalitem"   />

    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_invoiceinfo; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>


                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <th><?php echo $_lang_view_label_invoicenum; ?></th>
                                <td> <?php echo $this->invoicecode; ?>  </td> 
                                <th><?php echo $_lang_view_label_invoiceduedate; ?></th>
                                <td> 
                                    <div class="input-group" id="picker-container">
                                        <input  value="<?php echo date('Y-m-d'); ?>"  name="invoiceduedate"  id="invoiceduedate" type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    </div>
                                </td> 
                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_accountname; ?></th>
                                <td> <?php echo $this->saleorder->accountname; ?> </td> 
                                <th><?php echo $_lang_view_label_orderno; ?></th>
                                <td> <?php echo $this->saleorder->code; ?> </td> 
                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_invoicedate; ?></th>
                                <td> <?php echo substr($this->saleorder->orderdate, 0, 10); ?> </td> 
                                <th><?php echo $_lang_view_label_orderuser; ?></th>
                                <td> 
                                    <span class="input-help">
                                        <?php
                                        $contactinfo = '';
                                        if ($this->user->mobile != "") {
                                            $contactinfo = $this->user->mobile;
                                        } else if ($this->user->phone != "") {
                                            $contactinfo .= $this->user->phone;
                                            if ($this->user->phoneext != "") {
                                                $contactinfo .= ' Ext: ' . $this->user->phoneext;
                                            }
                                        } else {
                                            $contactinfo = "---";
                                        }
                                        ?>
                                        <span class="  popovers" title="" data-content="<?php echo $contactinfo; ?>" data-placement="right" data-trigger="hover" data-rel="popover" data-original-title="<?php echo $this->user->username; ?>"><?php echo $this->saleorder->firstname . '  ' . $this->saleorder->lastname; ?> </span >                                    
                                    </span>
                                </td> 
                            </tr>



                        </tbody>
                    </table>



                </div>
            </div>

        </div>

    </div>



    <div class="row">
        <div class="col-sm-12">



            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_invoicedetail; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">


                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable" width="100%"   >
                        <thead>
                            <tr>
                                <th> </th>
                                <th ><?php echo $_lang_index_table_th_product; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_label_quantity; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_label_subtotal; ?> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = count($this->detail);
                            if ($total > 0) {
                                $totalsum = 0;
                                $index = 1;
                                foreach ($this->detail as $cart) {

                                    $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $cart->mediaid . '/type/thumb"  >';

                                    $tr = '<tr id="tr_' . $cart->itemid . '"    >';

                                    $tr .='<td  align="center">' . $image_product . '</td>';

                                    $tr .='<td>';
                                    $tr .= '<strong>' . $cart->productname . '</strong><br />';
                                    $tr .='SKU: ' . $cart->sku . '<br />';
                                    $tr .=$_lang_view_label_price . ': ' . moneyFormat($cart->productprice) . '<br />';
                                    $tr .='</td>';

                                    $tr .='<td  align="center">';
                                    $tr .= $cart->qty;
                                    $tr .='</td>';



                                    $tr .='<td align="center" width="10%">';
                                    $tr .= '<span id="st_' . $cart->itemid . '">' . moneyFormat($cart->subtotal) . '</span>';

                                    $tr .='<input type="hidden"   name="' . $index . '_operation"   value="buy"  />';
                                    $tr .='<input type="hidden"   name="' . $index . '_productname"   value="' . $cart->name . '"  />';
                                    $tr .='<input type="hidden"   name="' . $index . '_itemid"   value="' . $cart->itemid . '"  />';
                                    $tr .='<input type="hidden"   name="' . $index . '_sku"   value="' . $cart->sku . '"  />';
                                    $tr .='<input type="hidden" id="deleted_' . $cart->itemid . '"  name="' . $index . '_deleted"   value="0"  />';
                                    $tr .='<input type="hidden" id="productid_' . $cart->itemid . '"  name="' . $index . '_productid"   value="' . $cart->productid . '"  />';
                                    $tr .='<input type="hidden" id="price_' . $cart->itemid . '"  name="' . $index . '_price"    value="' . $cart->buyprice . '"    />';
                                    $tr .='<input type="hidden" id="qty_' . $cart->itemid . '"     name="' . $index . '_qty"   value="' . $cart->qty . '"    />';
                                    $tr .='<input type="hidden"   class="subtotal_item"   id="subtotal_' . $cart->itemid . '"   name="' . $index . '_subtotal"    value="' . $cart->subtotal . '"    />';

                                    $tr .='</td>';

                                    $tr .= '</tr>';

                                    echo $tr;

                                    $totalsum = $totalsum + $cart->subtotal;
                                    $index ++;
                                }

                                $tr = '<tr>';
                                $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_subttotal . '</strong></td>';
                                $tr .= '<td  align="center" width="10%"> <span id="subtotal"> ' . moneyFormat($totalsum) . '    </span></td>';
                                $tr .= '</tr>';

                                
                               $tr .= '<tr>';
                                $tr .= '<td colspan="3" align="right">';

                                $tr .= '<select name="payment_method" id="paymenttype"  >';
                                $tr .= '<option value="creditcard">' . $_lang_view_payment_creditcard . '</option>';
                                $tr .= '<option value="banktransfer">' . $_lang_view_payment_wireless . '</option>';
                                $tr .= '</select>';

                                $payment_fee = $totalsum * 0.04;

                                $tr .='</strong></td>';
                                $tr .= '<td  align="center" width="10%">';
                                $tr .= '<input type="text" class=" " id="payment_fee" name="payment_fee" value="0"  />  ';
                                $tr .= '</td>';
                                $tr .= '</tr>';
                                
                                
                                $tr .= '<tr>';
                                $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_shipping_cost . '</strong></td>';
                                $tr .= '<td  align="center" width="10%">';
                                $tr .= '<input type="hidden" class=" " id="invoice_subtotal" value="' . $totalsum . '" name="invoice_subtotal"   />  ';
                                $tr .= '<input type="text" class=" " id="shipping_cost" name="shipping_cost"  value=" ' . floatval($payment_fee) . '    "  />  ';
                                $tr .= '<input type="hidden" class=" " id="totalinvoice" name="totalinvoice"  value="' . $totalsum . '"    />  ';
                                $tr .= '<input type="hidden" class=" " id="ordercode" name="ordercode"  value="' . $this->saleorder->code . '"    />  ';
                                $tr .= '</td>';
                                $tr .= '</tr>';


 


                                $tr .= '<tr>';
                                $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_total . '</strong></td>';
                                $tr .= '<td  align="center" width="10%"> <span id="lbl_totalinvoice"> ' . moneyFormat($totalsum + $payment_fee) . '    </span></td>';
                                $tr .= '</tr>';

                                echo $tr;
                            }
                            ?>

                            <tr>
                                <td colspan="3"></td>
                                <td>

                                    <button type="submit" class="btn btn-primary ">
                                        <i class="fa fa-list-alt"></i>&nbsp;<?php echo $_lang_index_label_generateInvoice; ?> 
                                    </button>
                                </td>
                            </tr>

                        </tbody>
                    </table>

                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div id="messageProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
                    </div>

                </div>
            </div>
            <!-- end: DYNAMIC TABLE PANEL -->

        </div>
    </div>
    <!-- end: PAGE CONTENT-->


    <div class="row">
        <div class="col-sm-12">



            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_invoicecomments; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">
                    <textarea class="form-control" name="invoicecomments"  id="invoicecomments"><?php echo $this->saleorder->ordercomments; ?></textarea>

                </div>
            </div>



        </div>

    </div>

    <div class="row">

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_billaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td> <?php echo $this->saleorder->billing_country; ?> </td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><?php echo $this->saleorder->billing_state; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><?php echo $this->saleorder->billing_city; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><?php echo $this->saleorder->billing_address; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><?php echo $this->saleorder->billing_zipcode; ?></td> 
                            </tr>

                            <?php if ($this->saleorder->billing_pobox != "") { ?>
                                <tr>
                                    <td><?php echo $_lang_register_label_pobox; ?></td>
                                    <td><?php echo $this->saleorder->billing_pobox; ?></td> 
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                </div>
            </div>



        </div>

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_shipaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">


                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td> <?php echo $this->saleorder->shipping_country; ?> </td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><?php echo $this->saleorder->shipping_state; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><?php echo $this->saleorder->shipping_city; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><?php echo $this->saleorder->shipping_address; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><?php echo $this->saleorder->shipping_zipcode; ?></td> 
                            </tr>

                            <?php if ($this->saleorder->shipping_pobox != "") { ?>
                                <tr>
                                    <td><?php echo $_lang_register_label_pobox; ?></td>
                                    <td><?php echo $this->saleorder->shipping_pobox; ?></td> 
                                </tr>
                            <?php } ?>


                        </tbody>
                    </table>

                </div> </div>


        </div>
    </div>





</form>

<script type="text/javascript">
var _global_creditcard_fee = <?php echo doubleval($this->fees->creditcard) ?>  ;
var _global_wiretransfer_fee = <?php echo  doubleval($this->fees->wiretransfer) ?>  ;
var _global_saletaxes_fee = <?php echo  doubleval($this->taxes->saletaxes) ?>  ;
var _global_communicationtaxes_fee = <?php echo  doubleval($this->taxes->communicationtaxes) ?>  ;
</script>


