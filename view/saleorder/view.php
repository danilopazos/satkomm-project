<!-- Set Delivery Date -->
<div class="modal fade" id="modal-cancelorder" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form method="post" action="/saleorder/cancelorder/code/<?php echo $this->saleorder->code; ?>"       role="form" class="form-setdeliverydate  box-user-register" id="form-setcancelorder"  >

                <div class="modal-body">


                    <div id="messageSetDeliveryDateOk" class="alert alert-warning ">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_message_cancelorder_confirm; ?>
                    </div>

                </div>

                <div class="modal-footer">

                    <input type="hidden" name="code" value="<?php echo $this->saleorder->code; ?>" />

                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>

                    <button type="submit" class="btn btn-primary"  >
                        <?php echo $_lang_view_btn_cancelordernow; ?>
                    </button>

                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--- Product Add Cart Modal Window -->
<div class="modal fade" id="confirmorder-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div id="messageProcessOk" class="alert alert-info  ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_confirmorder_message; ?>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 

                            <button type="button" id="btn_confirmorder" class="btn btn-green">
                                <i class="fa fa-check"></i>   <?php echo $_lang_view_label_confirmorder; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<?php
$ispossibleprocess = true;
$ismyaccount = false;

if ($this->session_accountid == $this->saleorder->accountid) {
    $ismyaccount = true;
}


$now = new DateTime(null, new DateTimeZone($this->session_timezonename));
$nowdatetime = $now->format('Y-m-d H:i:s');

$to_time = strtotime($nowdatetime);
$from_time = strtotime($this->saleorder->orderdate);
$totalminutes = round(abs($to_time - $from_time) / 60, 2);
$dispatchnow = intval($this->saleorder->dispatchnow);

if ($this->saleorder->orderstatus == 1) {

    if ($this->saleorder->invoice_code == "" && $totalminutes < 180 && $dispatchnow == 0) {
        $ispossibleprocess = false;
    }
} else {
    $ispossibleprocess = false;
}
?>


<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            echo $_lang_view_label_orderno . '&nbsp;<strong>' . $this->saleorder->code . '</strong>&nbsp;';
            if ($this->processOrder && $this->saleorder->orderstatus == 1) {

                if ($ispossibleprocess) {
                    if ($this->saleorder->invoice_code != "") {

                        echo '<a class="btn btn-xs  btn-green" href="/invoice/view/code/' . $this->saleorder->invoice_code . '">';
                        echo '<i class="fa fa-shopping-bag"></i>&nbsp;' . $_lang_btn_viewinvoice;
                        echo '</a>';
                    } else {
                        echo '<a class="btn btn-xs  btn-green" href="/saleorder/process/code/' . $this->saleorder->code . '">';
                        echo '<i class="fa fa-shopping-bag"></i>&nbsp;' . $_lang_btn_processorder;
                        echo '</a>';
                    }
                }
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->


<form role="form" class=" form-placeorder  box-user-register" id="form-placeorder"  >

    <input type="hidden" value="<?php echo count($this->detail); ?>"  name="totalitem"  id="totalitem"   />

    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_orderinfo; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <th><?php echo $_lang_view_label_accountname; ?></th>
                                <td> <?php echo $this->saleorder->accountname; ?> </td> 
                                <th><?php echo $_lang_view_label_orderno; ?></th>
                                <td> <?php echo $this->saleorder->code; ?> </td> 
                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_orderdate; ?></th>
                                <td> <?php echo $this->saleorder->orderdate; ?> </td> 
                                <th><?php echo $_lang_view_label_orderuser; ?></th>
                                <td> <?php echo $this->saleorder->firstname . '  ' . $this->saleorder->lastname; ?> </td> 
                            </tr>

                            <?php
                            if ($this->saleorder->orderstatus == 1) {

                                if ($this->saleorder->invoice_code == "" && $totalminutes < 180 && $dispatchnow == 0 && $ismyaccount) {
                                    ?>
                                    <tr>

                                        <td> <?php echo '<a href="#" data-toggle="modal" data-target="#modal-cancelorder"  class="btn btn-xs btn-dark-grey"      >' . $_lang_view_label_cancelorder . '</a>'; ?>   </td> 
                                        <td colspan="3" > </td>
                                    </tr>
                                    <tr>

                                        <td colspan="4" > <?php echo $_lang_view_label_cancelorder_info; ?></td>
                                    </tr>
                                    <?php
                                } else if ($dispatchnow == 1) {
                                    ?>
                                    <tr>
                                        <td colspan="4" > <a href="#" class="btn btn-xs btn-red"><?php echo $_lang_view_label_dispatchnow; ?></a></td>
                                    </tr>  
                                    <?php
                                } else if ($this->saleorder->invoice_code == "" && $totalminutes < 180 && $dispatchnow == 0 ) {
                                    ?>
                                    <tr>
                                        <td colspan="4" > <?php echo $_lang_view_label_cancelorder_providerinfo; ?></td>
                                    </tr>                                    
                                    <?php
                                }
                            } 
                            else {
                                ?>
                                <tr>

                                    <td colspan="4" > <?php echo '<a href="#"   class="btn btn-xs btn-dark-grey"      >' . $_lang_view_label_ordercancelled_datetime . ' ' . $this->saleorder->canceldatetime . '</a>'; ?>   </td> 

                                </tr>
                                <?php
                            }
                            ?>


                        </tbody>
                    </table>



                </div>
            </div>

        </div>

    </div>



    <div class="row">
        <div class="col-sm-12">



            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_index_table_title; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">


                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable" width="100%"   >
                        <thead>
                            <tr>
                                <th> </th>
                                <th ><?php echo $_lang_index_table_th_product; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_label_quantity; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_label_subtotal; ?> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = count($this->detail);
                            if ($total > 0) {
                                $totalsum = 0;
                                $index = 1;
                                foreach ($this->detail as $cart) {

                                    $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $cart->mediaid . '/type/thumb"  >';

                                    $tr = '<tr id="tr_' . $cart->itemid . '"    >';

                                    $tr .= '<td  align="center">' . $image_product . '</td>';

                                    $tr .= '<td>';
                                    $tr .= '<strong>' . $cart->productname . '</strong><br />';
                                    $tr .= 'SKU: ' . $cart->sku . '<br />';
                                    $tr .= $_lang_view_label_price . ': ' . moneyFormat($cart->productprice) . '<br />';
                                    $tr .= '</td>';

                                    $tr .= '<td  align="center">';
                                    $tr .= $cart->qty;
                                    $tr .= '</td>';



                                    $tr .= '<td align="center">';
                                    $tr .= '<span id="st_' . $cart->itemid . '">' . moneyFormat($cart->subtotal) . '</span>';

                                    $tr .= '<input type="hidden"   name="' . $index . '_operation"   value="buy"  />';
                                    $tr .= '<input type="hidden"   name="' . $index . '_productname"   value="' . $cart->name . '"  />';
                                    $tr .= '<input type="hidden"   name="' . $index . '_itemid"   value="' . $cart->itemid . '"  />';
                                    $tr .= '<input type="hidden"   name="' . $index . '_sku"   value="' . $cart->sku . '"  />';
                                    $tr .= '<input type="hidden" id="deleted_' . $cart->itemid . '"  name="' . $index . '_deleted"   value="0"  />';
                                    $tr .= '<input type="hidden" id="productid_' . $cart->itemid . '"  name="' . $index . '_productid"   value="' . $cart->productid . '"  />';
                                    $tr .= '<input type="hidden" id="price_' . $cart->itemid . '"  name="' . $index . '_price"    value="' . $cart->buyprice . '"    />';
                                    $tr .= '<input type="hidden" id="qty_' . $cart->itemid . '"     name="' . $index . '_qty"   value="' . $cart->qty . '"    />';
                                    $tr .= '<input type="hidden"   class="subtotal_item"   id="subtotal_' . $cart->itemid . '"   name="' . $index . '_subtotal"    value="' . $cart->subtotal . '"    />';

                                    $tr .= '</td>';

                                    $tr .= '</tr>';

                                    echo $tr;

                                    $totalsum = $totalsum + $cart->subtotal;
                                    $index ++;
                                }

                                $tr = '<tr>';
                                $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_total . '</strong></td>';
                                $tr .= '<td  align="center"> <span id="totalsum"> ' . moneyFormat($totalsum) . '    </span></td>';
                                $tr .= '</tr>';
                                echo $tr;
                            }
                            ?>

                        </tbody>
                    </table>

                </div>
            </div>
            <!-- end: DYNAMIC TABLE PANEL -->

        </div>
    </div>
    <!-- end: PAGE CONTENT-->


    <div class="row">
        <div class="col-sm-12">


            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
            </div>
            <div id="messageProcessOk" class="alert alert-success no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
            </div>



            <?php if (trim($this->saleorder->ordercomments) != "") { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_ordercomments; ?>
                        <div class="panel-tools">
                            <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->saleorder->ordercomments; ?>
                    </div>
                </div>
            <?php } ?>


        </div>

    </div>

    <div class="row">

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_billaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td> <?php echo $this->saleorder->billing_country; ?> </td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><?php echo $this->saleorder->billing_state; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><?php echo $this->saleorder->billing_city; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><?php echo $this->saleorder->billing_address; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><?php echo $this->saleorder->billing_zipcode; ?></td> 
                            </tr>

                            <?php if ($this->saleorder->billing_pobox != "") { ?>
                                <tr>
                                    <td><?php echo $_lang_register_label_pobox; ?></td>
                                    <td><?php echo $this->saleorder->billing_pobox; ?></td> 
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                </div>
            </div>



        </div>

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_shipaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">


                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td> <?php echo $this->saleorder->shipping_country; ?> </td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><?php echo $this->saleorder->shipping_state; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><?php echo $this->saleorder->shipping_city; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><?php echo $this->saleorder->shipping_address; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><?php echo $this->saleorder->shipping_zipcode; ?></td> 
                            </tr>

                            <?php if ($this->saleorder->shipping_pobox != "") { ?>
                                <tr>
                                    <td><?php echo $_lang_register_label_pobox; ?></td>
                                    <td><?php echo $this->saleorder->shipping_pobox; ?></td> 
                                </tr>
                            <?php } ?>


                        </tbody>
                    </table>

                </div> </div>


        </div>
    </div>


</form>



