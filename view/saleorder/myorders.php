
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_myorders; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_list; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li class="active">
                    <a data-toggle="tab" href="#panel_created">
                        <?php echo $_lang_panel_created; ?>
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#panel_readytodispatch">
                        <?php echo $_lang_panel_readytodispatch; ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_dispatched">
                        <?php echo $_lang_panel_dispatched; ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_delivered">
                        <?php echo $_lang_panel_delivered; ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_cancelled">
                        <?php echo $_lang_panel_cancelled; ?>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div id="panel_created" class="tab-pane in active">
                    <!-- start: DYNAMIC TABLE PANEL -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_myorders; ?>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                            </div>
                        </div>
                        <div class="panel-body">
                                    <div class="table-responsive">

                            <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="mainTable">
                                <thead>
                                    <tr>
                                        <th><?php echo $_lang_view_label_orderno; ?></th>
                                        <th class="hidden-xs"><?php echo $_lang_view_label_orderdate; ?></th>
                                        <th class="hidden-xs"><?php echo $_lang_view_label_dispatchdate; ?></th>
                                        <th><?php echo $_lang_view_label_deliverydate; ?></th>
                                        <th><?php echo $_lang_view_label_trackinginfo; ?></th>
                                        <th class="hidden-xs"><?php echo $_lang_view_label_invoiceno; ?></th>
                                        <th class="hidden-xs"><?php echo $_lang_view_label_totalitems; ?> </th>
                                        <th  ><?php echo $_lang_view_label_total; ?></th> 
                                        <th class="hidden-xs"><?php echo $_lang_view_label_orderstatus; ?></th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> - </td>
                                        <td class="hidden-xs"> - </td>
                                        <td class="hidden-xs"> - </td>
                                        <td > - </td>
                                        <td > - </td>
                                        <td class="hidden-xs"> - </td>
                                        <td class="hidden-xs"> - </td>
                                        <td > - </td>
                                        <td > - </td>
                                    </tr>
                                </tbody>
                            </table>
                                        
                                    </div>
                        </div>
                    </div>
                    <!-- end: DYNAMIC TABLE PANEL -->
                </div>


                <div id="panel_readytodispatch" class="tab-pane in ">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_myorders; ?>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="tableReadyToDispatch">
                                    <thead>
                                        <tr>
                                            <th><?php echo $_lang_view_label_orderno; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_orderdate; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_dispatchdate; ?></th>
                                            <th><?php echo $_lang_view_label_deliverydate; ?></th>
                                            <th><?php echo $_lang_view_label_trackinginfo; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_invoiceno; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_totalitems; ?> </th>
                                            <th  ><?php echo $_lang_view_label_total; ?></th> 
                                            <th class="hidden-xs"><?php echo $_lang_view_label_orderstatus; ?></th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td > - </td>
                                        </tr>
                                    </tbody>
                                </table>
                                    </div>
                            </div>
                        </div>
                        <!-- end: DYNAMIC TABLE PANEL -->
                </div>
                
                <div id="panel_dispatched" class="tab-pane in ">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_myorders; ?>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="tableDispatched">
                                    <thead>
                                        <tr>
                                            <th><?php echo $_lang_view_label_orderno; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_orderdate; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_dispatchdate; ?></th>
                                            <th><?php echo $_lang_view_label_deliverydate; ?></th>
                                            <th><?php echo $_lang_view_label_trackinginfo; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_invoiceno; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_totalitems; ?> </th>
                                            <th  ><?php echo $_lang_view_label_total; ?></th> 
                                            <th class="hidden-xs"><?php echo $_lang_view_label_orderstatus; ?></th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td > - </td>
                                        </tr>
                                    </tbody>
                                </table>
                                    </div>
                            </div>
                        </div>
                        <!-- end: DYNAMIC TABLE PANEL -->
                </div>
                
                <div id="panel_delivered" class="tab-pane in ">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_myorders; ?>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="tableDelivered">
                                    <thead>
                                        <tr>
                                            <th><?php echo $_lang_view_label_orderno; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_orderdate; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_dispatchdate; ?></th>
                                            <th><?php echo $_lang_view_label_deliverydate; ?></th>
                                            <th><?php echo $_lang_view_label_trackinginfo; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_invoiceno; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_totalitems; ?> </th>
                                            <th  ><?php echo $_lang_view_label_total; ?></th> 
                                            <th class="hidden-xs"><?php echo $_lang_view_label_orderstatus; ?></th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td > - </td>
                                        </tr>
                                    </tbody>
                                </table>
                                    </div>
                            </div>
                        </div>
                        <!-- end: DYNAMIC TABLE PANEL -->
                </div>
                
                <div id="panel_cancelled" class="tab-pane in ">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_myorders; ?>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                                </div>
                            </div>
                            <div class="panel-body">
                                 <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"  id="tableCancelled">
                                    <thead>
                                        <tr>
                                            <th><?php echo $_lang_view_label_orderno; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_orderdate; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_dispatchdate; ?></th>
                                            <th><?php echo $_lang_view_label_deliverydate; ?></th>
                                            <th><?php echo $_lang_view_label_trackinginfo; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_invoiceno; ?></th>
                                            <th class="hidden-xs"><?php echo $_lang_view_label_totalitems; ?> </th>
                                            <th  ><?php echo $_lang_view_label_total; ?></th> 
                                            <th class="hidden-xs"><?php echo $_lang_view_label_orderstatus; ?></th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td > - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td class="hidden-xs"> - </td>
                                            <td > - </td>
                                            <td > - </td>
                                        </tr>
                                    </tbody>
                                </table>
                                     </div>
                            </div>
                        </div>
                        <!-- end: DYNAMIC TABLE PANEL -->
                </div>
                
            </div>
        </div>



    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <table class="table table-hover table-full-width" id="mainTable">

            <tbody>
                <tr>
                    <td width="5%"> <a href="#" class="btn btn-xs btn-primary"><?php echo $_lang_view_label_orderno; ?></a> </td>
                    <td> <?php echo $_lang_index_label_btn_blue; ?> </td>
                </tr>
                <tr>
                    <td width="5%"> <a href="#" class="btn btn-xs btn-yellow"><?php echo $_lang_view_label_orderno; ?></a> </td>
                    <td> <?php echo $_lang_index_label_btn_orange; ?> </td>
                </tr>
                <tr>
                    <td width="5%"> <a href="#" class="btn btn-xs btn-red"><?php echo $_lang_view_label_orderno; ?></a> </td>
                    <td> <?php echo $_lang_index_label_btn_red; ?> </td>
                </tr>     

                <tr>
                    <td width="5%"> <a href="#" class="btn btn-xs btn-dark-grey"><?php echo $_lang_view_label_orderno; ?></a> </td>
                    <td> <?php echo $_lang_index_label_btn_grey; ?> </td>
                </tr>   

            </tbody>
        </table>

    </div>
</div>

<!-- end: PAGE CONTENT-->
