<!--- Product Detail Modal Window -->
<div class="modal fade" id="productdetail-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title"  id="panel_producttitle">   </h4>
                        </div>


                        <div class="tabbable">
                            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">

                                <li class="active">
                                    <a data-toggle="tab" href="#panel_intro">
                                        <?php echo $_lang_panel_intro; ?>
                                    </a>
                                </li>

                                <li>
                                    <a data-toggle="tab" href="#panel_description">
                                        <?php echo $_lang_panel_description; ?>
                                    </a>
                                </li> 
                                <li>
                                    <a data-toggle="tab" href="#panel_package">
                                        <?php echo $_lang_panel_package; ?>
                                    </a>
                                </li>   
                                <li>
                                    <a data-toggle="tab" href="#panel_technicalspecs">
                                        <?php echo $_lang_panel_technicalspecs; ?>
                                    </a>
                                </li>   
                                <li>
                                    <a data-toggle="tab" href="#panel_features">
                                        <?php echo $_lang_panel_features; ?>
                                    </a>
                                </li>   
                                <li>
                                    <a data-toggle="tab" href="#panel_docs">
                                        <?php echo $_lang_panel_docs; ?>
                                    </a>
                                </li>   

                            </ul>


                            <div class="tab-content">

                                <div id="panel_intro" class="tab-pane in active">

                                </div>
                                <div id="panel_description" class="tab-pane">

                                </div>
                                <div id="panel_package" class="tab-pane">

                                </div>
                                <div id="panel_technicalspecs" class="tab-pane">

                                </div>
                                <div id="panel_features" class="tab-pane">

                                </div>
                                <div id="panel_docs" class="tab-pane">

                                </div>
                            </div>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 
                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--- Product Add Cart Modal Window -->
<div class="modal fade" id="addcart-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title"  id="panel_addcart_title">   </h4>
                        </div>

                        <form role="form" class="form-user  box-user-register" id="form-addcart"  >

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_view_block_addcart; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  

                                    <div id="messageItemAddedOk" class="alert alert-success no-display">
                                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_cartitem; ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="quantity">
                                            <?php echo $_lang_view_label_price; ?>
                                        </label>
                                        <div class="col-sm-4"  >
                                            <input type="text"  class="form-control"  id="form-addcart-labelprice"  disabled="true"  value=""    />  
                                        </div>
                                    
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="quantity">
                                            <?php echo $_lang_view_label_quantity; ?>
                                        </label>
                                        <div class="col-sm-4">
                                        
                                            <input type='button' value='-' class='    qtyminus' field='quantity' />
                                            <input type='text' name='quantity'   id='form-addcart-qty' value='1' class=' qty'  style="width: 4em; text-align: center;" />
                                            <input type='button' value='+' class='   qtyplus' field='quantity' />
                                             
                                        </div>
                                    
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="quantity">
                                            <?php echo $_lang_view_label_subtotal; ?>
                                        </label>
                                        <div class="col-sm-4"  >
                                            <input type="text"  class="form-control "  id="form-addcart-subtotal"  disabled="true"  value=""    />
                                        </div>
                                    
                                    </div>
                                    
                                    
                                </div>
                                
                                <input type="hidden" name="idcrypt" id="form-addcart-idcrypt"   />
                                <input type="hidden" name="price"    id="form-addcart-price"  />
                                
                            </div>
                            
                        </form>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_view_label_close; ?>
                            </button> 

                            <button type="button" id='btn_additem' class="btn btn-primary">
                                <i class="fa fa-cart-plus"></i>   <?php echo $_lang_view_label_addcart; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            if ($this->session_accountid != $this->id) {
                echo '<a class="btn btn-xs btn-default" href="/account/index">';
                echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_table_title; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th width="10%"> </th>
                            <th width="30%" class="hidden-xs"><?php echo $_lang_index_table_th_sku; ?></th>
                            <th><?php echo $_lang_index_table_th_name; ?></th>
                            <th class="hidden-xs"><?php echo $_lang_index_table_th_networkcode; ?> </th>
                            <th width="10%"><?php echo $_lang_index_table_th_price; ?> </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->
