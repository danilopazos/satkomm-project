<?php
//-- Catalog Index
$_lang_index_label_module= "Catalog";
$_lang_index_label_action = "Search";

$_lang_index_table_title = "Product Catalog";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Name";
$_lang_index_table_th_networkcode = "Network";
$_lang_index_table_th_price = "Price";

$_lang_view_label_close = "Close";
$_lang_view_label_addcart= "Add Cart";

$_lang_view_block_addcart = "Add Cart";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Package";
$_lang_panel_technicalspecs = "Specs";
$_lang_panel_features= "Features";
$_lang_panel_docs = "Docs";

$_lang_view_label_quantity = "Quantity";
$_lang_view_label_price = "Price";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item added";