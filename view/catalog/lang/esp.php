<?php
//-- Catalog Index
$_lang_index_label_module= "Cat&aacute;logo";
$_lang_index_label_action = "Buscar";

$_lang_index_table_title = "Cat&aacute;logo de productos";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Nombre";
$_lang_index_table_th_networkcode = "Red";
$_lang_index_table_th_price = "Precio";

$_lang_view_label_close = "Cerrar";
$_lang_view_label_addcart= "A&ntilde;adir al carro";

$_lang_view_block_addcart = "A&ntilde;adir al carro";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Paquete";
$_lang_panel_technicalspecs = "Especs";
$_lang_panel_features= "Caracter&iacute;sticas";
$_lang_panel_docs = "Documentos";

$_lang_view_label_quantity = "Cantidad";
$_lang_view_label_price = "Precio";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item a&ntilde;adido";