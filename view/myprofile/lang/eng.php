<?php
//-- Catalog Index
$_lang_index_label_module= "My Profile";
$_lang_index_label_action = "Info"; 

$_lang_index_label_goback = "Go Back";

$_lang_index_tab_overview = "Overview";
$_lang_index_tab_edit = "Edit";
$_lang_index_tab_updatepassword = "Update Password";

$_lang_index_userinformation = "User Information";
$_lang_index_editinformation = "User Information";
$_lang_index_editpassword= "Update Password";
$_lang_index_username = "Username";
$_lang_index_firstname = "Firstname";
$_lang_index_lastname = "Lastname";
$_lang_index_mobile = "Mobile";
$_lang_index_phone = "Phone";
$_lang_index_phoneext = "Phone ext";
$_lang_index_required = "(required)";
$_lang_index_lang = "Language";

$_lang_index_btnupdateinfo = "Update Info";
$_lang_index_btnupdatepassword = "Update Password";

$_lang_index_actualpassword = "Actual Password";
$_lang_index_newpassword = "New Password";
$_lang_index_confirmpassword = "Confirm new Password";

$_lang_index_label_formerror = "Please introduce the required fields";
$_lang_index_label_userprocessok = "Success! the user info was updated";
$_lang_index_label_passwordprocessok = "The password was updated";
$_lang_index_passwordvalidate_error = "The actual password is not valid";