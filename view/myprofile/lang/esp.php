<?php
//-- Catalog Index
$_lang_index_label_module= "Mi Perfil";
$_lang_index_label_action = "Info"; 

$_lang_index_label_goback = "Volver";

$_lang_index_tab_overview = "Info";
$_lang_index_tab_edit = "Actualizar";
$_lang_index_tab_updatepassword = "Actualizar Contrase&ntilde;a";

$_lang_index_userinformation = "Informaci&oacute;n del Usuario";
$_lang_index_editinformation = "Editar Informaci&oacute;n";
$_lang_index_editpassword= "Actualizar Contrase&ntilde;a";
$_lang_index_username = "Nombre de usuario";
$_lang_index_firstname = "Nombres";
$_lang_index_lastname = "Apellidos";
$_lang_index_mobile = "M&oacute;vil";
$_lang_index_phone = "Tel&eacute;fono";
$_lang_index_phoneext = "Tel&eacute;fono anexo";
$_lang_index_required = "(requerido)";
$_lang_index_lang = "Idioma";

$_lang_index_btnupdateinfo = "Actualizar Informaci&oacute;n";
$_lang_index_btnupdatepassword = "Actualizar Contrase&ntilde;a";

$_lang_index_actualpassword = "Contrase&ntilde;a actual";
$_lang_index_newpassword = "Nueva contrase&ntilde;a";
$_lang_index_confirmpassword = "Confirmar nueva contrase&ntilde;a";

$_lang_index_label_formerror = "Por favor introduce los campos requeridos";
$_lang_index_label_userprocessok = "Exito! la informaci&oacute;n del usuario ha sido actualizada";
$_lang_index_label_passwordprocessok = "La contrase&ntilde;a ha sido actualizada";
$_lang_index_passwordvalidate_error = "La contrase&ntilde;a actual ingresada no es v&aacute;lida";