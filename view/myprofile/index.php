 
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-users"></i>
                <a href="/myprofile/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <form class="sidebar-search">
                    <div class="form-group">
                        <input type="text" placeholder="Start Searching...">
                        <button class="submit">
                            <i class="clip-search-3"></i>
                        </button>
                    </div>
                </form>
            </li>
        </ol>
        <div class="page-header">
            <!-- <h1>User Profile <small>user profile page</small></h1> -->
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li class="active">
                    <a data-toggle="tab" href="#panel_overview">
                        <?php echo $_lang_index_tab_overview; ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_edit_account">
                        <?php echo $_lang_index_tab_edit; ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_edit_password">
                        <?php echo $_lang_index_tab_updatepassword; ?>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div id="panel_overview" class="tab-pane in active">
                    <div class="row">

                        <div class=" col-md-8">
                            <div class="user-left">

                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3"><?php echo $_lang_index_userinformation; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th width="20%"><?php echo $_lang_index_username; ?></th>
                                            <td>
                                                <?php echo $this->entity->username; ?>
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <th width="20%"><?php echo $_lang_index_firstname; ?></th>
                                            <td>
                                                <?php echo $this->entity->firstname; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th width="20%"><?php echo $_lang_index_lastname; ?></th>
                                            <td>
                                                <?php echo $this->entity->lastname; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th width="20%"><?php echo $_lang_index_mobile; ?></th>
                                            <td>
                                                <?php echo $this->entity->mobile; ?>
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <th width="20%"><?php echo $_lang_index_phone; ?></th>
                                            <td>
                                                <?php
                                                if ($this->entity->phone != "") {
                                                    echo $this->entity->phone;
                                                } else {
                                                    echo '---';
                                                }
                                                ?>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <th width="20%"><?php echo $_lang_index_phoneext; ?></th>
                                            <td>
                                                <?php
                                                if ($this->entity->phoneext != "") {
                                                    echo $this->entity->phoneext;
                                                } else {
                                                    echo '---';
                                                }
                                                ?>
                                            </td>
                                        </tr>   
                                        
                                        <tr>
                                            <th width="20%"><?php echo $_lang_index_lang; ?></th>
                                            <td>
                                                <?php
                                                if ($this->entity->lang != "") {
                                                    echo $this->entity->lang;
                                                } else {
                                                    echo '---';
                                                }
                                                ?>
                                            </td>
                                        </tr>  
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>

                <div id="panel_edit_account" class="tab-pane">
                    <form action="#" role="form" id="formuser">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><?php echo $_lang_index_editinformation; ?></h3>
                                <hr>
                            </div>
                            <div class="col-md-6">

                                <div id="userFormError"  class="errorHandler alert alert-danger no-display">
                                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_index_label_formerror; ?>
                                </div>             
                                <div id="userMessageProcessOk" class="alert alert-success no-display">
                                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_index_label_userprocessok; ?>
                                </div>                                

                                <div class="form-group">
                                    <label class="control-label">
                                        <?php echo $_lang_index_firstname.' '.$_lang_index_required; ?> 
                                    </label>
                                    <input type="text" placeholder="" class="form-control" id="firstname" name="firstname" value="<?php echo $this->entity->firstname; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        <?php echo $_lang_index_lastname.' '.$_lang_index_required; ?>
                                    </label>
                                    <input type="text" placeholder="" class="form-control" id="lastname" name="lastname" value="<?php echo $this->entity->lastname; ?>">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        <?php echo $_lang_index_mobile.' '.$_lang_index_required; ?>
                                    </label>
                                    <input type="text" placeholder="" class="form-control" id="mobile" name="mobile" value="<?php echo $this->entity->mobile; ?>"> 
                                </div>                                
                                <div class="form-group">
                                    <label class="control-label">
                                        <?php echo $_lang_index_phone; ?>
                                    </label>
                                    <input type="text" placeholder="" class="form-control" id="phone" name="phone" value="<?php echo $this->entity->phone; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        <?php echo $_lang_index_phoneext; ?>
                                    </label>
                                    <input type="text" placeholder="" class="form-control" id="phoneext" name="phoneext" value="<?php echo $this->entity->phoneext; ?>">
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label">
                                        <?php echo $_lang_index_lang; ?>
                                    </label>
                                    <select name="lang" id="lang" class="form-control"  >
                                        
                                        <?php 
                                        foreach($this->aLang as $lang){
                                            $selected = '';
                                            if($this->entity->lang == $lang->code){
                                                $selected = ' selected = "true" ';
                                            }
                                            echo '<option '.$selected.'  value="'.$lang->code.'">'.$lang->name.'</option>';
                                        }
                                        
                                        ?>
                                        
                                    </select>
                                </div>                                
                                
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <input type="hidden" name="userid" value="<?php echo $this->entity->id; ?>" />
                                <button class="btn btn-teal btn-block" type="submit">
                                    <?php echo $_lang_index_btnupdateinfo; ?> <i class="fa fa-user"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="panel_edit_password" class="tab-pane">
                    <form action="#" role="form" id="formpassword">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><?php echo $_lang_index_editpassword; ?></h3>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                
                                <div id="passwordFormError"  class="errorHandler alert alert-danger no-display">
                                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_index_label_formerror; ?>
                                </div>       
                                <div id="passwordValidateFormError"  class="errorHandler alert alert-danger no-display">
                                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_index_passwordvalidate_error; ?>
                                </div>                                    
                                <div id="passwordMessageProcessOk" class="alert alert-success no-display">
                                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_index_label_passwordprocessok; ?>
                                </div>      
                                
                                <div class="form-group">
                                    <label class="control-label">
                                        <?php echo $_lang_index_actualpassword; ?>
                                    </label>
                                    <input type="password" placeholder="" class="form-control" id="actualpassword" name="actualpassword">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        <?php echo $_lang_index_newpassword; ?>
                                    </label>
                                    <input type="password" placeholder="" class="form-control" id="newpassword" name="newpassword">
                                </div>                                
                                <div class="form-group">
                                    <label class="control-label">
                                       <?php echo $_lang_index_confirmpassword; ?>
                                    </label>
                                    <input type="password" placeholder="" class="form-control" id="confirmpassword" name="confirmpassword">
                                </div>

                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-4">
                                <input type="hidden" name="userid" value="<?php echo $this->entity->id; ?>" />
                                <button class="btn btn-teal btn-block" type="submit">
                                    <?php echo $_lang_index_btnupdatepassword; ?> <i class="fa fa-lock"></i>
                                </button>
                            </div>
                        </div>        
                    </form>    
                </div>

            </div>
        </div>
    </div>
    <!-- end: PAGE CONTENT-->
    
</div>    
