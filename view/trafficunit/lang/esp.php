<?php
//-- Catalog Index
$_lang_index_label_module= "Unidades (Tr&aacute;fico)";
$_lang_index_label_action = "Lista";
 
$_lang_index_block_table= "Lista";

$_lang_index_label_code = "C&oacute;digo";
$_lang_index_label_networkcode = "Red";
$_lang_index_label_description = "Descripci&oacute;n";
$_lang_index_link_add = "Registrar unidad";

$_lang_label_code = "C&oacute;digo";
$_lang_label_network = "Red";
$_lang_label_description = "Descripci&oacute;n";
$_lang_label_save = "Guardar";
$_lang_label_cancel = "Cancelar";
$_lang_label_selectoption = "Seleccione";

$_lang_register_block_form_title ="Formulario de registro";
$_lang_register_label_formerror ="Problemas revise los campos requeridos";
$_lang_label_errorcoderepeated ="El c&oacute;digo introducido es utilizado por otro registro";
$_lang_label_errorcodenamerepeated ="La descripci&oacute;n introducida es utilizado por otro registro";
$_lang_label_messageregisterok ="El registro fue realizado satisfactoriamente";

$_lang_label_message_confirm_delete_title ="Confirmaci&oacute;n";
$_lang_label_message_confirm_delete ="Esta seguro de eliminar el presente registro?";
$_lang_label_message_confirm_btndelete ="Eliminar";
$_lang_label_message_confirm_btncancel ="Cancelar";
$_lang_label_messagedeletedok = "El registro fue eliminado satisfactoriamente";

$_lang_register_block_equivalence_title = "Equivalencias";

$_lang_messageequivalence_registererror = "Problemas guardando la equivalencia, por favor intente nuevamente";
$_lang_messageequivalence_registerok ="La equivalencia se guard&oacute; satisfactoriamente";
