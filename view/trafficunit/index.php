
<?php require_once 'modal_equivalence.php'; ?>
<!-- register update -->
<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-user  box-user-register" id="form-register"  >

                <div class="modal-body">

                    <div class="errorHandlerUser alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div id="errorCodeRepeated" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_label_errorcoderepeated; ?>
                    </div>
                    <div id="errorNameRepeated" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_label_errorcodenamerepeated; ?>
                    </div>
                    <div id="messageRegisterOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_label_messageregisterok; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_form_title; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="code">
                                            <?php echo $_lang_label_code; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="code" name="code" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="description">
                                            <?php echo $_lang_label_description; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="description" name="description" class="form-control">
                                        </div>
                                    </div>                                    



                                    <input type="hidden" name="id" id="register_id"   />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_label_cancel; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- register delete -->
<div class="modal fade" id="register-delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-user  box-user-register" id="form-delete"  >

                <div class="modal-body">

                    <div id="messageDeletedOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_label_messagedeletedok; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_label_message_confirm_delete_title; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div   class="alert alert-warning">
                                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_label_message_confirm_delete; ?>
                                    </div>

                                    <input type="hidden" name="id" id="registertodelete_id"   />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_label_cancel; ?>
                    </button>
                    <button type="button" class="btn btn-primary" id="btndelete">
                        <?php echo $_lang_label_message_confirm_btndelete; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/traffictype/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <a class="btn btn-xs btn-primary" data-toggle="modal"    href="#register-modal">
                <i class="fa fa-plus-circle"></i> <?php echo $_lang_index_link_add; ?>
            </a>
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_block_table; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th><?php echo $_lang_index_label_code; ?></th>
                            <th><?php echo $_lang_index_label_description; ?></th> 
                            <td> - </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                       
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->
