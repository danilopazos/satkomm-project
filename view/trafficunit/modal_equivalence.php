<!-- register update -->
<div class="modal fade" id="register-equivalence" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-equivalence  box-user-register" id="form-equivalence" name="form_equivalence"  >

                <div class="modal-body">

                    <div id="messageEquivalenceRegisterFalse" class="errorHandlerUser alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_messageequivalence_registererror; ?>
                    </div>
                    <div id="messageEquivalenceRegisterOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_messageequivalence_registerok; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_equivalence_title; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  
                                    <table class="table-responsive table">

                                    <?php 
                                    $total = count($this->aList);
                                    if($total > 0){
                                     $table = "";
                                     $index = 0;
                                     foreach($this->aList as $record){
                                         $table .="<tr id='treq_".$record->id."' class='treq'>";
                                         $table .="<td width='30%' class='label_unit'></td>";
                                         $table .="<td width='10%'> x </td>";
                                         $table .="<td width='30%'> <input type='hidden' name='eqindex_".$index."' value='".trim($record->code)."' /> </td>";
                                         $table .="<td width='30%'> <input type='text' name='eq_".$record->code."' id='eq_".$record->id."' class='form-control' /> </td>";
                                         $table .="<td width='10%'> = </td>";
                                         $table .="<td width='20%'> 1 (".$record->description.")</td>";
                                         $table .="</tr>";
                                         $index ++;
                                     }
                                     echo $table;
                                    }
                                    ?>
                                    </table>
                                    <input type="hidden" name="id" id="record_eq_id" value="" />
                                    <input type="hidden" name="codea" id="record_eq_codea" value="" />
                                    <input type="hidden" name="total" id="total" value="<?php echo $total; ?>" />
                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_label_cancel; ?>
                    </button>
                    <button type="button" id="btnsubmiteq" class="btn btn-primary">
                        <?php echo $_lang_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>