<div class="form-group">
    <label class="col-sm-2 control-label" for="servicecode">
        <?php echo $_lang_label_code_service; ?>
    </label>
    <div class="col-sm-10">
        <select  class="form-control" name="servicecode" id="servicecode">
            <option value=""><?php echo $_lang_label_selectoption; ?></option>
            <?php
            foreach ($this->_aService as $record) {
                echo '<option value="' . $record->code . '">' .$record->code.' | '.$record->description . '</option>';
            }
            ?>
        </select>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label" for="calltypecode">
        <?php echo $_lang_label_code_calltype; ?>
    </label>
    <div class="col-sm-10">
        <select  class="form-control" name="calltypecode" id="calltypecode">
            <option value=""><?php echo $_lang_label_selectoption; ?></option>
            <?php
            foreach ($this->_aCalltype as $record) {
                echo '<option value="' . $record->code . '">' .$record->code.' | '.$record->description . '</option>';
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="cdkind">
        <?php echo $_lang_label_code_cdkind; ?>
    </label>
    <div class="col-sm-10">
        <select  multiple="multiple" class="form-control" name="cdkind[]" id="cdkind">
            <option value=""><?php echo $_lang_label_selectoption; ?></option>
            <?php
            foreach ($this->_aCdkind as $record) {
                echo '<option value="' . $record->code . '">' .$record->code.' | '.$record->description . '</option>';
            }
            ?>
        </select>
    </div>
</div>                                    

<div class="form-group">
    <label class="col-sm-2 control-label" for="oceanregioncode">
        <?php echo $_lang_label_code_oceanregion; ?>
    </label>
    <div class="col-sm-10">
        <select  class="form-control" name="oceanregioncode" id="oceanregioncode">
            <option value=""><?php echo $_lang_label_selectoption; ?></option>
            <?php
            foreach ($this->_aOceanregion as $record) {
                echo '<option value="' . $record->code . '">' .$record->code.' | '.$record->description . '</option>';
            }
            ?>
        </select>
    </div>
</div>                                       

<div class="form-group">
    <label class="col-sm-2 control-label" for="description">
        <?php echo $_lang_label_description; ?>
    </label>
    <div class="col-sm-10">
        <input type="text"  id="description" name="description" class="form-control">
    </div>
</div>    

<input type="hidden" name="code" id="code" value="<?php echo $this->_code; ?>" />
<input type="hidden" name="provider" id="provider" value="<?php echo $this->_provider; ?>" />
<input type="hidden" name="ttgroup" id="ttgroup" value="<?php echo $this->_ttgroup; ?>" />
<input type="hidden" name="network" id="network" value="<?php echo $this->_network; ?>" />