 

<div class="form-group">
    <label class="col-sm-2 control-label" for="calltype">
        <?php echo $_lang_label_code_calltype; ?>
    </label>
    <div class="col-sm-10">
        <select  class="form-control" name="calltype" id="calltype">
            <option value=""><?php echo $_lang_label_selectoption; ?></option>
            <?php
            foreach ($this->_aCalltype as $record) {
                echo '<option value="' . $record->code . '">' . $record->description . '</option>';
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="cdkind">
        <?php echo $_lang_label_code_destinationzone; ?>
    </label>
    <div class="col-sm-10">
        <select  class="form-control" name="cdkind" id="cdkind">
            <option value=""><?php echo $_lang_label_selectoption; ?></option>
            <?php
            foreach ($this->_aCdkind as $record) {
                echo '<option value="' . $record->code . '">' . $record->description . '</option>';
            }
            ?>
        </select>
    </div>
</div>                                    

<div class="form-group">
    <label class="col-sm-2 control-label" for="oceanregion">
        <?php echo $_lang_label_code_oceanregion; ?>
    </label>
    <div class="col-sm-10">
        <select  class="form-control" name="oceanregion" id="oceanregion">
            <option value=""><?php echo $_lang_label_selectoption; ?></option>
            <?php
            foreach ($this->_aOceanregion as $record) {
                echo '<option value="' . $record->code . '">' . $record->description . '</option>';
            }
            ?>
        </select>
    </div>
</div>                                       

<div class="form-group">
    <label class="col-sm-2 control-label" for="description">
        <?php echo $_lang_label_description; ?>
    </label>
    <div class="col-sm-10">
        <input type="text"  id="description" name="description" class="form-control">
    </div>
</div>    

<input type="hidden" value="code" id="code" />
<input type="hidden" name="ttgroup" id="ttgroup" value="<?php echo $this->_ttgroup; ?>" />