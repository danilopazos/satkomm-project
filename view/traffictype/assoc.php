<!-- register update -->
<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-user  box-user-register" id="form-register"  >

                <div class="modal-body">

                    <div class="errorHandlerUser alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div id="errorForm" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_label_errortraffictype; ?>
                    </div>

                    <div id="messageRegisterOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_label_messageregisterok; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_form_title; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  

                                    <?php
                                    if ($this->_network == "INMARSAT" && $this->_provider == "STRATOS") {
                                        require_once 'assoc_stratos_inmarsat.php';
                                    } else if ($this->_network == "IRIDIUM" && $this->_provider == "STRATOS") {
                                        require_once 'assoc_stratos_iridium.php';
                                    } else if ($this->_network == "IRIDIUM" && $this->_provider == "IRIDIUM") {
                                        require_once 'assoc_iridium_iridium.php';
                                    }
                                    ?>

                                    <input type="hidden" name="id" id="register_id"   />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_label_cancel; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- register delete -->
<div class="modal fade" id="register-delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-user  box-user-register" id="form-delete"  >

                <div class="modal-body">

                    <div id="messageDeletedOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_label_messagedeletedok; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_label_message_confirm_delete_title; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div   class="alert alert-warning">
                                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_label_message_confirm_delete; ?>
                                    </div>

                                    <input type="hidden" name="id" id="registertodelete_id"   />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_label_cancel; ?>
                    </button>
                    <button type="button" class="btn btn-primary" id="btndelete">
                        <?php echo $_lang_label_message_confirm_btndelete; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/traffictype/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_assoc; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
                <h1><?php echo $this->_code; ?> <small><?php echo $this->_codename; ?></small></h1> 
            <a class="btn btn-xs btn-primary" data-toggle="modal"    href="#register-modal">
                <i class="fa fa-plus-circle"></i> <?php echo $_lang_index_link_add_assoc; ?>
            </a>
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_block_table; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                         
                            <th><?php echo $_lang_label_code_service; ?></th>
                            <th><?php echo $_lang_label_code_calltype; ?></th>
                            <th><?php echo $_lang_label_code_cdkind; ?></th> 
                            <th><?php echo $_lang_label_code_oceanregion; ?></th> 
                            <th><?php echo $_lang_label_code_destinationzone; ?></th> 
                            <td> - </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $_total = count($this->aList);
                        for ($i = 0; $i < $_total; $i++) {
                            $_record = $this->aList[$i];
                            ?>
                            <tr>
                               
                                <td> <?php echo $_record->servicecode.' | '.$_record->servicecode_desc; ?>  </td>
                                <td> <?php echo $_record->calltypecode.' | '.$_record->calltypecode_desc; ?>  </td>
                                <td> <?php echo $_record->cdkind.' | '.$_record->cdkind_desc;  ?>  </td>
                                <td> <?php echo $_record->oceanregioncode.' | '.$_record->oceanregioncode_desc;  ?>  </td>
                                <td> <?php echo $_record->destinationzone.' | '.$_record->destinationzone_desc; ?>  </td>
                                <td> <a class="btn-xs btn-red"  href="/traffictype/assocdelete/code/<?php echo $this->_code; ?>/network/<?php echo $this->_network; ?>/provider/<?php echo $this->_provider; ?>/id/<?php echo $_record->id; ?>" ><i class="fa fa-trash"></i></a>  </td>
                            </tr>


                            <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->
