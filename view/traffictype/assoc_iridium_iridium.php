<div class="form-group">
    <label class="col-sm-2 control-label" for="servicecode">
        <?php echo $_lang_label_code_service; ?>
    </label>
    <div class="col-sm-10">
       <input type="text"  id="servicecode" name="servicecode" class="form-control">
    </div>
</div>
                                

<div class="form-group">
    <label class="col-sm-2 control-label" for="description">
        <?php echo $_lang_label_description; ?>
    </label>
    <div class="col-sm-10">
        <input type="text"  id="description" name="description" class="form-control">
    </div>
</div>    

<input type="hidden" name="code" id="code" value="<?php echo $this->_code; ?>" />
<input type="hidden" name="provider" id="provider" value="<?php echo $this->_provider; ?>" />
<input type="hidden" name="ttgroup" id="ttgroup" value="<?php echo $this->_ttgroup; ?>" />
<input type="hidden" name="network" id="network" value="<?php echo $this->_network; ?>" />