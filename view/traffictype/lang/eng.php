<?php
//-- Catalog Index
$_lang_index_label_module= "Traffic Type";
$_lang_index_label_action = "List";
 
$_lang_index_block_table= "List";

$_lang_index_label_code = "Code";
$_lang_index_label_networkcode = "Network";
$_lang_index_label_description = "Description";
$_lang_index_link_add = "Add Traffic Type";


$_lang_label_code = "Code";
$_lang_label_network = "Network";
$_lang_label_description = "Description";
$_lang_label_save = "Save";
$_lang_label_cancel = "Cancel";
$_lang_label_selectoption = "Select Option";

$_lang_register_block_form_title ="Register form";
$_lang_register_label_formerror ="Form problems, please check the required fields";
$_lang_label_errorcoderepeated ="The code introduced was used for other record";
$_lang_label_errorcodenamerepeated ="The description introduced was used for other record";
$_lang_label_messageregisterok ="The record was registered successfully";


$_lang_label_message_confirm_delete_title ="Delete confirmation";
$_lang_label_message_confirm_delete ="Are you sure to delete the current record?";
$_lang_label_message_confirm_btndelete ="Delete";
$_lang_label_message_confirm_btncancel ="Cancel";
$_lang_label_messagedeletedok = "The record was successfully deleted";
$_lang_label_airtimeprovider  = "Airtime Provider";

$_lang_index_label_action_assoc = "Assoc";

$_lang_index_link_add_assoc = "Add";
$_lang_label_code_service = "Service";
$_lang_label_code_calltype = "Call Type";
$_lang_label_code_cdkind = "Cd Kind";
$_lang_label_code_oceanregion = "Ocean Region";
$_lang_label_code_destinationzone = "Destination Zone";
$_lang_label_errortraffictype = "Problems saving the traffic type";

$_lang_label_ttgroup  ="Group";

$_lang_register_label_select  = "All";