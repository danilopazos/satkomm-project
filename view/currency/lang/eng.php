<?php
 
$_lang_index_label_module= "Exchange currency";
$_lang_index_label_action = "Register";

$_lang_index_label_block_list = "Currency list";

$_lang_index_currencycode = 'Code';
$_lang_index_currencycountry = 'Country';
$_lang_index_currencysymbol = 'Symbol';
$_lang_index_xrate = 'Exchange rate to USD';
$_lang_index_user = 'User';
$_lang_index_datetime = 'Register datetime';

$_lang_index_registered = 'Introduce exchange';
$_lang_index_listhistory = 'History';

$_lang_view_label_close = "Close";
$_lang_view_label_save = "Save exchange rate";

$_lang_view_label_registerok = "Exchange rate saved";
$_lang_view_label_error = "Please introduce correct values";