<?php
 
$_lang_index_label_module= "Tipo de cambio";
$_lang_index_label_action = "Registro";

$_lang_index_label_block_list = "Lista de tipo de cambio";

$_lang_index_currencycode = 'C&oacute;digo';
$_lang_index_currencycountry = 'Pa&iacute;s';
$_lang_index_currencysymbol = 'S&iacute;mbolo';
$_lang_index_xrate = 'Tipo de cambio a USD';
$_lang_index_user = 'Usuario';
$_lang_index_datetime = 'Fecha de registro';

$_lang_index_registered = 'Registrar tipo de cambio';
$_lang_index_listhistory = 'Listar hist&oacute;rico';

$_lang_view_label_close = "Cerrar";
$_lang_view_label_save = "Guardar tipo de cambio";

$_lang_view_label_registerok = "Tipo de cambio registrado";
$_lang_view_label_error = "Por favor ingrese valores v&aacute;lidos";