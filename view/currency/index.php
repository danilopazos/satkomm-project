<script type="text/javascript">
    
    _lang_index_currencycode = '<?php echo $_lang_index_currencycode; ?>';
    _lang_index_xrate  = '<?php echo $_lang_index_xrate; ?>';
    _lang_index_user = '<?php echo $_lang_index_user; ?>';
    _lang_index_datetime = '<?php echo $_lang_index_datetime; ?>';
    
</script>




<!--- History Modal-->
<div class="modal fade" id="historylist-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12" id="table_history">
                        
                        
                        
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?php echo $_lang_view_label_close; ?>
                </button> 
 

            </div>



        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!--- Register form Modal-->
<div class="modal fade" id="registerform-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

        <form id="registerform" name="registerform">

            <div class="modal-content">

                <div class="modal-body">

                    <div id="messageProcessError" class="alert alert-danger no-display  ">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_error; ?> 
                    </div>
                    <div id="messageProcessOk" class="alert alert-info no-display  ">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_registerok; ?> 
                    </div>                    

                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                        <thead>

                            <tr>
                                <th><?php echo $_lang_index_currencycode; ?></th>
                                <td>
                                    <span id="lbl_currencycode" ></span>
                                    <input type="hidden" id="currencycode" name="currencycode"  />
                                </td>
                            </tr>   

                            <tr>
                                <th><?php echo $_lang_index_xrate; ?></th>
                                <td>
                                    <input type="text" id="xrate" name="xrate" class="form-control"  />
                                </td>
                            </tr>   

                        </thead>        
                    </table>


                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button> 

                    <button type="button" id="btn_registerform" class="btn btn-green">
                        <i class="fa fa-check"></i>   <?php echo $_lang_view_label_save; ?>
                    </button>

                </div>            


            </div>

        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            if ($this->session_accountid != $this->id) {
                echo '<a class="btn btn-xs btn-default" href="/account/index">';
                echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->



<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">


        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_block_list; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th ><?php echo $_lang_index_currencycountry; ?></th>
                            <th ><?php echo $_lang_index_currencycode; ?></th>
                            <th ><?php echo $_lang_index_currencysymbol; ?></th>
                            <th ><?php echo $_lang_index_xrate; ?></th>
                            <th ><?php echo $_lang_index_user; ?> </th>
                            <th ><?php echo $_lang_index_datetime; ?> </th>

                            <th></th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total = count($this->aCurrency);
                        if ($total > 0) {
                            $tr = '';
                            $index = 1;
                            foreach ($this->aCurrency as $e) {

                                $tr .= '<tr>';

                                $tr .= '<td>' . $e->country . '</td>';
                                $tr .= '<td>' . $e->code . '</td>';
                                $tr .= '<td>' . $e->symbol . '</td>';

                                if ($e->username != "") {
                                    $tr .= '<td>' . $e->xrate . '</td>';
                                    $tr .= '<td>' . $e->username . '</td>';
                                    $tr .= '<td>' . $e->registereddatetime . '</td>';
                                } else {
                                    $tr .= '<td> -- </td>';
                                    $tr .= '<td> -- </td>';
                                    $tr .= '<td> -- </td>';
                                }

                                if ($e->code != "USD") {

                                    $tr .= '<td>  <a onclick="fnc_registerform(  \'' . $e->code . '\'  );" href="#" class="btn btn-xs btn-green"  >' . $_lang_index_registered . '</a>  </td>';
                                    $tr .= '<td>  <a onclick="fnc_historylist( \'' . $e->code . '\'  );" href="#" class="btn btn-xs btn-yellow"  >' . $_lang_index_listhistory . '</a>  </td>';
                                } else {
                                    $tr .= '<td> -- </td>';
                                    $tr .= '<td> -- </td>';
                                }
                                $tr .= '</tr>';

                                echo $tr;
                                $tr = '';
                            }
                        }
                        ?>


                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT--> 