<script type="text/javascript">
    var _lang_view_dispatch_head_serial = '<?php echo $_lang_view_dispatch_head_serial; ?>';
    var _lang_view_dispatch_head_code2 = '<?php echo $_lang_view_dispatch_head_code2; ?>';

    var _lang_view_label_distpatch_itemdispatched = '<?php echo $_lang_view_label_distpatch_itemdispatched; ?>';
    var _lang_view_label_distpatch_item = '<?php echo $_lang_view_label_distpatch_item; ?>';

</script>

<!-- Dispatch Item Model -->

<div class="modal fade" id="modal-confirmdispatch" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

 
                <div class="modal-body">

                   
                    <div class="errorHandlerItemDispatched  alert alert-warning "  >
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_dispatch_msg_confirmdispatch; ?>
                    </div>
                   
                </div>
                <div class="modal-footer">
 
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>
                    <a  class="btn btn-primary"  href="/invoice/confirmdispatch/code/<?php echo  $this->invoice->code; ?>" >
                        <?php echo $_lang_dispatch_msg_btnconfirm; ?>
                    </a>
                    
                </div>

          

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_packinglist; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
                echo '<a class="btn  btn-default   btn-xs"   href="/invoice/view/code/'.$this->invoice->code.'" >';
                echo '<i class="fa fa-arrow-circle-left"></i>&nbsp;' . $_lang_index_label_goback;
                echo '</a>';
            
                echo '&nbsp;'.$_lang_view_label_invoiceno.'<strong>'.$this->invoice->code.'</strong>';
        
            if ($this->invoice->dispatchdate == "")  {
                echo '&nbsp;';
                echo '<a class="btn  btn-green   btn-xs" data-toggle="modal" data-target="#modal-confirmdispatch" >';
                echo '<i class="fa fa-shopping-basket"></i>&nbsp;' . $_lang_btn_confirmdispatch;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->


<form role="form" class=" form-placeorder  box-user-register" id="form-placeorder"  >

    <input type="hidden" value="<?php echo count($this->detail); ?>"  name="totalitem"  id="totalitem"   />

    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_orderinfo; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <th><?php echo $_lang_view_label_accountname; ?></th>
                                <td> <?php echo $this->invoice->accountname; ?> </td> 
                                <th><?php echo $_lang_view_label_invoiceno; ?></th>
                                <td> <?php echo $this->invoice->code; ?> </td> 

                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_invoicedate; ?></th>
                                <td> <?php echo substr($this->invoice->invoicedate, 0, 10); ?> </td> 
                                <th><?php echo $_lang_view_label_orderuser; ?></th>
                                <td> <?php echo $this->invoice->firstname . '  ' . $this->invoice->lastname; ?> </td> 
                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_invoiceduedate; ?></th>
                                <td> <?php echo substr($this->invoice->invoiceduedate, 0, 10); ?> </td> 
                                <th><?php echo $_lang_view_label_orderno; ?></th>
                                <td> <?php echo $this->invoice->ordercode; ?> </td> 
                            </tr>


                            <tr>
                                <th><?php echo $_lang_view_label_paymentdate; ?></th>
                                <td> <?php
            if ($this->invoice->paymentdate != "") {
                echo substr($this->invoice->paymentdate, 0, 10);
            } else {
                echo '--';
            }
            ?> 
                                </td> 
                                <th><?php echo $_lang_view_label_dispatchdate; ?></th>
                                <td> <?php
                                    if ($this->invoice->dispatchdate != "") {
                                        echo $this->invoice->dispatchdate;
                                    } else {
                                        echo '--';
                                    }
            ?> 
                                </td> 
                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_deliverydate; ?></th>
                                <td> <?php
                                    if ($this->invoice->deliverydate != "") {
                                        echo $this->invoice->deliverydate;
                                    } else {
                                        echo '--';
                                    }
            ?> 
                                </td> 
                                <th> </th>
                                <td>  </td> 
                            </tr>

                            <tr>
                                <th><?php echo $_lang_view_label_downloadinvoice; ?></th>
                                <td> 
                                    <a target="_blank" href="../../downloadpackingslip/code/<?php echo $this->invoice->code; ?>" class="btn btn-xs btn-red"><i class="fa fa-file-pdf-o"></i>&nbsp;<?php echo $_lang_view_label_download; ?></a>
                                </td> 
                                <th> </th>
                                <td>  
                                </td> 
                            </tr>

                        </tbody>
                    </table>



                </div>
            </div>

        </div>

    </div>



    <div class="row">
        <div class="col-sm-12">



            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_view_label_packegelist; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">


                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable" width="100%"   >
                        <thead>
                            <tr>

                                <th ><?php echo $_lang_index_table_th_sku; ?></th>
                                <th ><?php echo $_lang_index_table_th_product; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_dispatch_head_serial; ?></th>
                                <th style="text-align: center;"><?php echo $_lang_view_label_price; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = count($this->inventoryItems);
                            if ($total > 0) {
                                $totalsum = 0;
                                $index = 1;
                                foreach ($this->inventoryItems as $cart) {

                                    $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $cart->mediaid . '/type/thumb"  >';

                                    $tr = '<tr id="tr_' . $cart->itemid . '"    >';
 
                                    $tr .='<td>';

                                    $tr .= $cart->sku;
                                    $tr .='</td>';

                                    $tr .='<td  >';
                                    $tr .= $cart->productname;
                                    $tr .='</td>';



                                    $tr .='<td align="center">';
                                    $tr .=$cart->iserial;
                                    $tr .='</td>';

                                    $tr .='<td align="center">';
                                    $tr .=moneyFormat($cart->productprice);
                                    $tr .='</td>';

                                    $tr .= '</tr>';

                                    echo $tr;
 
                                    $index ++;
                                }

                       
                               // echo $tr;
                            }
                            ?>

                        </tbody>
                    </table>

                </div>
            </div>
            <!-- end: DYNAMIC TABLE PANEL -->

        </div>
    </div>
    <!-- end: PAGE CONTENT-->


    <div class="row">
        <div class="col-sm-12">


            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
            </div>
            <div id="messageProcessOk" class="alert alert-success no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
            </div>



            <?php if (trim($this->invoice->invoicecomments) != "") { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_invoicecomments; ?>
                        <div class="panel-tools">
                            <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->invoice->invoicecomments; ?>
                    </div>
                </div>
            <?php } ?>


        </div>

    </div>

    <div class="row">

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_billaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">

                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td> <?php echo $this->invoice->billing_country; ?> </td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><?php echo $this->invoice->billing_state; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><?php echo $this->invoice->billing_city; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><?php echo $this->invoice->billing_address; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><?php echo $this->invoice->billing_zipcode; ?></td> 
                            </tr>

                            <?php if ($this->invoice->billing_pobox != "") { ?>
                                <tr>
                                    <td><?php echo $_lang_register_label_pobox; ?></td>
                                    <td><?php echo $this->invoice->billing_pobox; ?></td> 
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                </div>
            </div>



        </div>

        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i><?php echo $_lang_register_block_shipaddress; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                    </div>
                </div>
                <div class="panel-body">


                    <table class="table table-condensed table-hover">

                        <tbody>

                            <tr>
                                <td><?php echo $_lang_register_label_country; ?></td>
                                <td> <?php echo $this->invoice->shipping_country; ?> </td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_state; ?></td>
                                <td><?php echo $this->invoice->shipping_state; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_city; ?></td>
                                <td><?php echo $this->invoice->shipping_city; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_street; ?></td>
                                <td><?php echo $this->invoice->shipping_address; ?></td> 
                            </tr>

                            <tr>
                                <td><?php echo $_lang_register_label_zipcode; ?></td>
                                <td><?php echo $this->invoice->shipping_zipcode; ?></td> 
                            </tr>

                            <?php if ($this->invoice->shipping_pobox != "") { ?>
                                <tr>
                                    <td><?php echo $_lang_register_label_pobox; ?></td>
                                    <td><?php echo $this->invoice->shipping_pobox; ?></td> 
                                </tr>
                            <?php } ?>


                        </tbody>
                    </table>

                </div> </div>


        </div>
    </div>


</form>



