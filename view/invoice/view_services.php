<table class="table table-striped table-bordered table-hover table-full-width" id="mainTable" width="100%"   >
    <thead>
        <tr>
            <th><?php echo $_lang_index_table_th_product; ?></th>
            <th colspan="2" style="text-align: center;"><?php echo $_lang_view_label_subtotal; ?> </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $_currency = 'USD';
        $total = count($this->detail);

        if ($total > 0 && $this->invoice->invoicetype == "SERVICES") {

            $_tr = '';
            
            $_subtotal = 0;
            
            foreach ($this->detail as $detail) {
                
                $_subtotal_price = 0;
                
                $_tr .= '<tr>';
           
                $_tr .= '<td>';

                $_tr .= '<strong>'.$_lang_plan.'</strong>: '.$detail->planname . '<br />';
                
                
                $_lbl_contractperiod = $_lang_contractperiodcode;
                if($detail->subscription_period=="p0"){
                   $_lbl_contractperiod = $_lang_validityperiodcode; 
                }
               
                
                $_tr .= '<strong>'.$_lang_paymentperiodcode.'</strong>: '.$_lang_period_option[$detail->subscription_period] . '<br />';
                
                $_tr .= '<strong>'.$_lbl_contractperiod.'</strong>: '.$_lang_period_option[$detail->contract_period] . '<br />';
                
                if($detail->subscription_price >0){
                    $_tr .= '<strong>'.$_lang_subscriptionprice.'</strong>: '.$_currency.' '.$detail->subscription_price . '<br />';
                }
                
                if($detail->activation_price >0){
                    $_tr .= '<strong>'.$_lang_activationpayment.'</strong>: '.$_currency.' '.$detail->activation_price . '<br />';
                }
                
                if($detail->fixed_price >0){
                    $_tr .= '<strong>'.$_lang_fixedpayment.'</strong>: '.$_currency.' '.$detail->fixed_price . '<br />';
                }
                
                if($detail->suspension_price >0){
                    $_tr .= '<strong>'.$_lang_suspensionpayment.'</strong>: '.$_currency.' '.$detail->suspension_price . '<br />';
                }
                
                if($detail->deactivation_price >0){
                    $_tr .= '<strong>'.$_lang_deactivationpayment.'</strong>: '.$_currency.' '.$detail->deactivation_price . '<br />';
                }
                
                if($detail->iserial != ""){
                    $_tr .= '<strong>'.$_lang_view_dispatch_head_serial.'</strong>: '.$detail->iserial . '<br />';
                }
                 
                $_subtotal_price = $_subtotal_price + doubleval($detail->subscription_price);
                $_subtotal_price = $_subtotal_price + doubleval($detail->activation_price);
                $_subtotal_price = $_subtotal_price + doubleval($detail->fixed_price);
                $_subtotal_price = $_subtotal_price + doubleval($detail->suspension_price);
                $_subtotal_price = $_subtotal_price + doubleval($detail->deactivation_price);
                
                $_subtotal = $_subtotal + $_subtotal_price;
                
                $_tr .= '</td>';
                
            
                
                $_tr .= '<td width="35%" style="text-align: center;" colspan="2">'.$_currency.' '.$_subtotal_price.'</td>';

                $_tr .= '</tr>';
            }
            echo $_tr;
            
            
            $_taxes = doubleval($this->invoice->taxes);
            $_paymentfee = doubleval($this->invoice->payment_fee);
            $_total = doubleval($this->invoice->x_ammount);
            
            $_tr = '';
            
            $_tr .= '<tr>';
            $_tr .= '<td></td>';
            $_tr .= '<th width="25%">'.$_lang_subtotal.'</th><td>USD '.$_subtotal.'</td>';
            $_tr .= '</tr>';
            
            $_tr .= '<tr>';
            $_tr .= '<td></td>';
            $_tr .= '<th width="25%">'.$_lang_taxes.'</th><td>USD '.$_taxes.'</td>';
            $_tr .= '</tr>';
            
            
            $_lbl_paymentmethod = "";
            if ($this->invoice->payment_method == "creditcard") {
                $_lbl_paymentmethod =  $_lang_view_payment_creditcard;
            } else {
                $_lbl_paymentmethod = $_lang_view_payment_wireless;
            }            
            
            $_tr .= '<tr>';
            $_tr .= '<td></td>';
            $_tr .= '<th width="25%">'.$_lbl_paymentmethod.'</th><td>USD '.$_paymentfee.'</td>';
            $_tr .= '</tr>';            
            
            $_tr .= '<tr>';
            $_tr .= '<td></td>';
            $_tr .= '<th width="25%">'.$_lang_total.'</th><td>USD '.$_total.'</td>';
            $_tr .= '</tr>';  
            
            echo $_tr;
            
        }
        ?>

    </tbody>
</table>