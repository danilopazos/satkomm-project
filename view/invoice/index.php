
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_customerinvoices;  ?>
                </a>
            </li>
            <li class="active">
                    <?php echo $_lang_index_label_list;  ?>
            </li>
            <li class="search-box">
               <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
          
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_customerinvoices;  ?>
                <div class="panel-tools">
                     <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th><?php echo $_lang_view_label_invoiceno;  ?></th>
                            <th><?php echo $_lang_view_label_customeraccount;  ?></th>
                            <th><?php echo $_lang_view_label_invoicedate;  ?></th>
                            <th><?php echo $_lang_view_label_dispatchdate;  ?></th>
                            <th><?php echo $_lang_view_label_deliverydate;  ?></th>
                            <th class="hidden-xs"><?php echo $_lang_view_label_totalitems;  ?> </th>
                            <th class="hidden-xs"><?php echo $_lang_view_payment_method;  ?> </th>
                            <th class="hidden-xs"><?php echo $_lang_view_label_total;  ?></th> 
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
        
    </div>
</div>
<!-- end: PAGE CONTENT-->
