<?php

//-- Catalog Index
$_lang_index_label_module = "Factura";
$_lang_index_label_action = "Detalle";
$_lang_index_label_packinglist = "Detalle del paquete";

$_lang_index_label_goback = "Volver";


$_lang_index_label_processorder = "Procesar orden";
$_lang_index_label_generateinvoice = "Generar Factura";
$_lang_index_label_myorders = "Mis Facturas";
$_lang_index_label_list = "Lista";
$_lang_index_label_customerinvoices = "Facturas de clientes";

$_lang_index_table_title = "Detalle de la factura";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Nombre";
$_lang_index_table_th_networkcode = "Red";
$_lang_index_table_th_price = "Precio";
$_lang_index_table_th_product = "Producto";

$_lang_view_label_close = "Cerrar";
$_lang_view_label_addcart = "A&ntilde;adir al carrito";

$_lang_register_block_ordercomments = "Comentarios de la orden";
$_lang_view_block_addcart = "A&ntilde;adir al carrito";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Paquete";
$_lang_panel_technicalspecs = "Especs";
$_lang_panel_features = "Caracter&iacute;sticas";
$_lang_panel_docs = "Docs";

$_lang_view_label_quantity = "Cantidad";
$_lang_view_label_price = "Precio";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item a&ntilde;adido";
$_lang_view_label_subttotal = "Sub-total";
$_lang_view_label_total = "Total";


$_lang_register_block_billaddress = "Direcci&oacute;n de facturaci&oacute;n";
$_lang_register_block_shipaddress = "Direcci&oacute;n de env&iacute;o";
$_lang_register_label_country = "Pais";
$_lang_register_label_state = "Estado";
$_lang_register_label_city = "Ciudad";
$_lang_register_label_street = "Calle";
$_lang_register_label_zipcode = "C&oacute;digo postal";
$_lang_register_label_pobox = "P.O. Box";

$_lang_register_label_processok = "Orden Enviada";
$_lang_register_label_formerror = "Problemas, por favor verifique los campos en blanco";
$_lang_view_label_confirmorder_message = "Por favor confirme para enviar su orden, gracias";
$_lang_view_label_confirmorder = "Confirmar Orden";

$_lang_register_block_orderinfo = "Informaci&oacute;n de la factura";

$_lang_view_label_accountname = "Cuenta";
$_lang_view_label_invoiceno = "Factura #";
$_lang_view_label_orderno = "Orden #";
$_lang_view_label_invoicedate = "Fecha de la factura";
$_lang_view_label_totalitems = "Total de items";
$_lang_view_label_orderuser = "Por";
$_lang_view_label_customeraccount = "Cuenta del cliente";
$_lang_label_printer = "Imprimir";

$_lang_view_label_paymentdate = "Fecha del pago";
$_lang_view_label_dispatchdate = "Fecha de despacho";
$_lang_view_label_deliverydate = "Fecha de salida de almac&eacute;n";


$_lang_btn_processorder = "Procesar Orden";

//Invoice Labels
$_lang_view_label_invoicenum = "Factura #";
$_lang_view_label_invoiceduedate = "Fecha de vencimiento";


$_lang_register_block_invoiceinfo = "Informaci&oacute;n de la factura";
$_lang_register_block_invoicedetail = "Detalles de la factura";

$_lang_view_label_shipping_cost = "Costo de env&iacute;o";
$_lang_view_label_distpatch_item = "Registrar Items";
$_lang_view_label_distpatch_itemdispatched = "Items Registrados";

$_lang_view_payment_method = "M&eacute;todo de pago";
$_lang_view_payment_creditcard = "Cargos por procesamiento - (Tarjeta de c&eacute;dito)";
$_lang_view_payment_wireless = "Cargos por procesamiento  - (Transferencia bancaria)";
$_lang_register_block_invoicecomments = "Comentarios de la factura";
$_lang_index_label_generateInvoice = "Generar Factura";
$_lang_view_label_dispatchitems = "Guardar Items";


$_lang_view_dispatch_head_serial = "Serial/ESN/IMEI";
$_lang_view_dispatch_head_code2 = "Auth/Code2/Otros c&oacute;digos";

$_lang_view_label_formregistered = "Items registrados";
$_lang_register_label_serialrepeated = "El formulario presenta n&uacute;meros seriales repetidos";

$_lang_btn_dispatchinvoice = "Despachar factura";
$_lang_btn_confirmdispatch = "Confirmar despacho";
$_lang_dispatch_msg_confirmdispatch = "Esta seguro de confirmar el despacho, los items ser&aacute;n a&ntilde;adidos al inventario del cliente";
$_lang_dispatch_msg_btnconfirm = "Si, despachar items ahora";

$_lang_view_label_packegelist = "Detalle del paquete";
$_lang_view_label_setdeliverydateOk = "Salida de Almac&eacute;n registrada";
$_lang_view_label_setdeliverydate = "Fijar salida de Almac&eacute;n";
$_lang_view_btn_setdeliverydate = "Fijar salida de Almac&eacute;n";
$_lang_view_msg_dispatchdatenotyet = "Por favor, indique la fecha de despacho primero";

$_lang_view_label_trackinginfo = "Seguimiento de la entrega";
$_lang_view_label_tracking_selectprovider = "Escoja al proveedor de la entrega (opcional)";
$_lang_view_label_tracking_trackingid_info = "Ingrese el ID del tracking  (opcional)";

$_lang_view_label_distributor = "CARGUERO DISTRIBUIDOR";
$_lang_view_label_shippingnotdefined = "No definido";

$_lang_view_label_setpaymentdate = "Fijar fecha de pago";
$_lang_view_label_paymentobs = "Observaciones";

$_lang_view_label_setpaymentdateok = "Fecha de pago registrada";
$_lang_view_btn_setpaymentdate = "Fijar fecha de pago";

$_lang_view_label_download = "Descargar";
$_lang_view_label_downloadinvoice = "Descargar factura";



$_lang_plan = "Plan";
$_lang_paymentperiodcode = "Periodo de pago";
$_lang_contractperiodcode = "Periodo de contrato";
$_lang_validityperiodcode = "Periodo de validez";
$_lang_servicetypecode = "Tipo de servicio";
$_lang_subscriptionprice = "Precio de subscripci&oacute;n";
$_lang_fixedpayment = "Cargos fijos";
$_lang_activationpayment = "Cargos por activaci&oacute;n";
$_lang_suspensionpayment = "Cargos por suspension";
$_lang_deactivationpayment = "Cargos por deactivation";


$_lang_subtotal = "Sub-total";
$_lang_taxes = "Impuestos";
$_lang_paymentfee = "Cargos por procesamiento";
$_lang_total = "Total";
