<table class="table table-striped table-bordered table-hover table-full-width" id="mainTable" width="100%"   >
    <thead>
        <tr>
            <th width="15%"> </th>
            <th ><?php echo $_lang_index_table_th_product; ?></th>
            <th style="text-align: center;"><?php echo $_lang_view_label_quantity; ?></th>
            <th style="text-align: center;"><?php echo $_lang_view_label_subtotal; ?> </th>
        </tr>
    </thead>
    <tbody>
        <?php
        //---   DETALLE DE LA FACTURA DE BIENES
        $total = count($this->detail);

        if ($total > 0 && $this->invoice->invoicetype == "GOODS") {
            $totalsum = 0;
            $index = 1;
            foreach ($this->detail as $cart) {

                $image_product = '<img width="90" height="90" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" src="/media/imagebyid/id/' . $cart->mediaid . '/type/thumb"  >';

                $tr = '<tr id="tr_' . $cart->itemid . '"    >';

                $tr .= '<td  width="15%" align="center">' . $image_product;
                $tr .= '<br/>';

                $_status_item_dispatched_value = 0;
                if ($this->invoice->dispatchdate == "" && $this->processInvoice) {
                    if ($cart->totaldispatched == $cart->qty) {
                        $_status_item_dispatched_value = 1;
                        $tr .= '<a href="#" class="btn  btn-green btn-xs link_dispatch_item "  id="item_' . $cart->itemid . '"  ><i class="fa fa-shopping-basket"></i>&nbsp;' . $_lang_view_label_distpatch_itemdispatched . '</a>';
                    } else {
                        $tr .= '<a href="#" class="btn  btn-bricky btn-xs link_dispatch_item "  id="item_' . $cart->itemid . '"  ><i class="fa fa-shopping-basket"></i>&nbsp;' . $_lang_view_label_distpatch_item . '</a>';
                    }
                }

                $tr .= '<input type="hidden" class="status_item_dispatched" id="status_item_' . $cart->itemid . '"  value="' . $_status_item_dispatched_value . '"  >';
                $tr .= '</td>';

                $tr .= '<td>';
                $tr .= '<strong>' . $cart->productname . '</strong><br />';
                $tr .= 'SKU: ' . $cart->sku . '<br />';
                $tr .= $_lang_view_label_price . ': ' . moneyFormat($cart->productprice) . '<br />';
                $tr .= '</td>';

                $tr .= '<td  align="center">';
                $tr .= $cart->qty;
                $tr .= '</td>';



                $tr .= '<td align="center">';
                $tr .= '<span id="st_' . $cart->itemid . '">' . moneyFormat($cart->subtotal) . '</span>';

                $tr .= '<input type="hidden"   name="' . $index . '_operation"   value="buy"  />';
                $tr .= '<input type="hidden"   name="' . $index . '_productname"   id="productname_' . $cart->itemid . '"    value="' . $cart->productname . '"  />';
                $tr .= '<input type="hidden"   name="' . $index . '_itemid"   value="' . $cart->itemid . '"  />';
                $tr .= '<input type="hidden"   name="' . $index . '_sku"   value="' . $cart->sku . '"  />';
                $tr .= '<input type="hidden" id="deleted_' . $cart->itemid . '"  name="' . $index . '_deleted"   value="0"  />';
                $tr .= '<input type="hidden" id="productid_' . $cart->itemid . '"  name="' . $index . '_productid"   value="' . $cart->productid . '"  />';
                $tr .= '<input type="hidden" id="price_' . $cart->itemid . '"  name="' . $index . '_price"    value="' . $cart->buyprice . '"    />';
                $tr .= '<input type="hidden" id="qty_' . $cart->itemid . '"     name="' . $index . '_qty"   value="' . $cart->qty . '"    />';
                $tr .= '<input type="hidden"   class="subtotal_item"   id="subtotal_' . $cart->itemid . '"   name="' . $index . '_subtotal"    value="' . $cart->subtotal . '"    />';

                $tr .= '</td>';

                $tr .= '</tr>';

                echo $tr;

                $totalsum = $totalsum + $cart->subtotal;
                $index ++;
            }

            
            $_taxes = doubleval($this->invoice->taxes);
            $_shippingcost = doubleval($this->invoice->shipping_cost);
            $_paymentfee = doubleval($this->invoice->payment_fee);
            $_total = doubleval($this->invoice->x_ammount) ;



            $_tr = '';
            
            $_tr .= '<tr>';
            $_tr .= '<td colspan="2"></td>';
            $_tr .= '<th  width="25%">'.$_lang_subtotal.'</th><td>USD '.moneyFormat($this->invoice->subtotal) .'</td>';
            $_tr .= '</tr>';
            
            $_tr .= '<tr>';
            $_tr .= '<td colspan="2"></td>';
            $_tr .= '<th  width="25%">'.$_lang_view_label_shipping_cost.'</th><td>USD '.moneyFormat($this->invoice->shipping_cost) .'</td>';
            $_tr .= '</tr>';            
            
            $_tr .= '<tr>';
            $_tr .= '<td  colspan="2"></td>';
            $_tr .= '<th width="25%">'.$_lang_taxes.'</th><td>USD '.$_taxes.'</td>';
            $_tr .= '</tr>';
            
            
            $_lbl_paymentmethod = "";
            if ($this->invoice->payment_method == "creditcard") {
                $_lbl_paymentmethod =  $_lang_view_payment_creditcard;
            } else {
                $_lbl_paymentmethod = $_lang_view_payment_wireless;
            }            
            
            $_tr .= '<tr>';
            $_tr .= '<td  colspan="2"></td>';
            $_tr .= '<th width="25%">'.$_lbl_paymentmethod.'</th><td>USD '.$_paymentfee.'</td>';
            $_tr .= '</tr>';            
            
            $_tr .= '<tr>';
            $_tr .= '<td  colspan="2"></td>';
            $_tr .= '<th width="25%">'.$_lang_total.'</th><td>USD '.$_total.'</td>';
            $_tr .= '</tr>';  
            
            echo $_tr;
            
            
            
            /*
            $tr = '<tr>';
            $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_subtotal . '</strong></td>';
            $tr .= '<td  align="center"> <span id="subtotal"> ' . moneyFormat($this->invoice->subtotal) . '    </span></td>';
            $tr .= '</tr>';

            $tr .= '<tr>';
            $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_shipping_cost . '</strong></td>';
            $tr .= '<td  align="center"> <span id="subtotal"> ' . moneyFormat($this->invoice->shipping_cost) . '    </span></td>';
            $tr .= '</tr>';

            $tr .= '<tr>';
            if ($this->invoice->payment_method == "creditcard") {
                $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_payment_creditcard . '</strong></td>';
            } else {
                $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_payment_wireless . '</strong></td>';
            }

            $tr .= '<td  align="center"> <span id="subtotal"> ' . moneyFormat($this->invoice->payment_fee) . '    </span></td>';
            $tr .= '</tr>';


            $tr .= '<tr>';
            $tr .= '<td colspan="3" align="right"><strong>' . $_lang_view_label_total . '</strong></td>';
            $tr .= '<td  align="center"> <span id="subtotal"> ' . moneyFormat($this->invoice->x_ammount) . '    </span></td>';
            $tr .= '</tr>';

            echo $tr;
             
             */
        }
        ?>

    </tbody>
</table>