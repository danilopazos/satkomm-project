<!--- Product Add Cart Modal Window -->
<div class="modal fade" id="confirmorder-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: TEXT FIELDS PANEL -->

                        <div id="messageProcessOk" class="alert alert-info  ">
                            <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_confirmorder_message; ?>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $_lang_index_cancel; ?>
                            </button> 

                            <button type="button" id="btn_confirmorder" class="btn btn-green">
                                <i class="fa fa-check"></i>   <?php echo $_lang_index_savepayment; ?>
                            </button>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/invoicepayment/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            if ($this->session_accountid != $this->id) {
                echo '<a class="btn btn-xs btn-default" href="/invoicepayment/index">';
                echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
 

<div class="row">
    <div class="col-sm-12">


        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_module; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">        
                    
                    <thead>
                        <tr>
                            <th><?php echo $_lang_index_customername;  ?></th>
                            <th><?php echo $_lang_index_creditlimitstatus;  ?></th>
                            <th><?php echo$_lang_index_creditlimit;  ?></th>
                            <th><?php echo $_lang_index_ammountpending;  ?></th> 
                            <th><?php echo $_lang_index_positivebalance;  ?></th> 
                        </tr>
                    </thead>                    
                    
                    <?php
                    $total = count($this->aInvoices);
                    
                    $_totalpendingammount = 0;
                    $_totalpositivebalance = 0;
                    
                    if($total > 0){
                        for ($i = 0; $i < $total; $i++) {
                            $e = $this->aInvoices[$i];

                            $style_ammountpending ="";
                            $style_positivebalance ="";
                            
                            $_positivebalance = 0;
                            $_ammountpending = 0;
                          
                            
                            $_pendingammount = $e->invoiceammount - $e->paymentammount;
                            $_positivebalance = $e->paymentammount - $e->invoiceammount  ;
                            
                            
                            if($_pendingammount <= 0){ $_pendingammount = 0; }
                            if($_positivebalance <= 0){ $_positivebalance = 0; }
                            
                            $_creditlimit_status = ($_pendingammount*100)/$e->creditlimit;
                             

                            
                            
                            $tr = "";
                            $tr .= "<tr>";
                            $tr .= '<td><a class="btn btn-xs btn-default"  href="/invoicepayment/customerinvoices/customerid/'.$e->customeraccountid.'">'.$e->customername.'</a></td>';
                            
                            if($_creditlimit_status > 100){
                                $tr  .='<td><span style="color:red">'.moneyFormat($_creditlimit_status)."%".'</span></td>'; 
                            }else{
                                $tr  .='<td><span style="color:blue">'.moneyFormat($_creditlimit_status)."%".'</span></td>'; 
                            } 
                            
                            $tr .= "<td>USD ".moneyFormat($e->creditlimit)."</td>";
                            $tr .= "<td $style_ammountpending >USD ".moneyFormat($_pendingammount)."</td>";
                            $tr .= "<td $_totalpositivebalance >USD ".moneyFormat($_positivebalance)."</td>"; 
                            $tr .= "</tr>";
                            echo $tr;
                        }
                    }else{
                        
                    }
                    
               
                    ?>
                 
                </table>
            </div>
        </div>

    </div>

</div> 