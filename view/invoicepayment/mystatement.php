<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/invoicepayment/mystatement">
                    <?php echo $_lang_index_label_modulemystatement; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
            <h4><?php echo $_lang_index_label_modulemystatement; ?>  </h4>
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<?php
$style_ammountpending = "";
$style_positivebalance = "";

$style_ammountinvoice = ' style=" color:black;" ';
$style_ammountpayment = ' style=" color:black;" ';
?>

<div class="row">
    <div class="col-sm-12">


        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_table_header; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">        

                    <thead>
                        <tr>
                            <th><?php echo $_lang_index_type; ?></th>
                            <th>#</th>
                            <th><?php echo $_lang_index_date; ?></th>
                            <th><?php echo $_lang_index_duedate; ?></th>
                            <th><?php echo $_lang_index_paymentammount; ?></th>
                            <th><?php echo $_lang_index_invoiceammount; ?></th>
                            <th></th> 
                        </tr>
                    </thead>                    

                    <?php
                    $total = count($this->aInvoices);

                    $_totalpaid = 0;
                    $_totalinvoices = 0;
                    $tempinvoiceid = 0;
                    if ($total > 0) {
                        for ($i = 0; $i < $total; $i++) {
                            $e = $this->aInvoices[$i];

                            $_docno = $e->invoiceno;
                            $_idcrypt = $e->idcrypt;

                            $style_ammountpending = ' style=" color:red;" ';


                            $style_positivebalance = ' style=" color:blue;" ';


                            $_operationtype = $_lang_index_type_invoice;
                            if ($e->type == "payment") {
                                $_operationtype = $_lang_index_type_payment;
                                $_docno = $e->id;
                            }

                            $tr = "";
                            $tr .= "<tr>";
                            $tr .= "<td>" . $_operationtype . "</td>";

                            if ($e->type != "payment") {
                                $tr .= '<td> <a target="_blank" class="btn btn-xs btn-default" href="/invoice/view/idcrypt/' . $_idcrypt . '">' . $_docno . '</a></td>';
                            } else {
                                $tr .= '<td> </td>';
                            }

                            $tr .= "<td>" . $e->invoicedate . "</td>";
                            $tr .= "<td>" . $e->invoiceduedate . "</td>";


                            if ($e->type == "payment") {
                                $tr .= "<td $style_ammountpayment >USD " . moneyFormat($e->invoiceammount) . "</td>";
                                $tr .= "<td  colspan='2' > </td>";
                                $_totalpaid = $_totalpaid + $e->invoiceammount;
                            } else {
                                $tr .= "<td></td>";
                                $tr .= "<td $style_ammountinvoice  colspan='2'  >USD " . moneyFormat($e->invoiceammount) . "</td>";

                                $_totalinvoices = $_totalinvoices + $e->invoiceammount;
                            }



                            $tr .= "</tr>";

                            echo $tr;

                            $tempinvoiceid = $e->invoiceid;
                        }
                    } else {
                        
                    }

                    $_resultpending = 0;
                    $_resultpositive = 0;

                    $_resultpending = $_totalinvoices - $_totalpaid;
                    $_resultpositive = $_totalpaid - $_totalinvoices;

                    if ($_resultpending < 0) {
                        $_resultpending = 0;
                    }
                    if ($_resultpositive < 0) {
                        $_resultpositive = 0;
                    }
                    ?>
                    <tfoot>
                        <tr>
                            <th   colspan="4"><?php echo $_lang_index_totalsum; ?></th>                            
                            <th <?php echo $style_ammountpayment; ?>><?php echo "USD " . moneyFormat($_totalpaid); ?></th>
                            <th <?php echo $style_ammountinvoice; ?> colspan="2"><?php echo "USD " . moneyFormat($_totalinvoices); ?></th>
                        </tr>

                        <tr>
                            <th  colspan="1"><?php echo $_lang_index_positivebalance; ?></th>                            
                            <th colspan="6" ><?php echo "USD " . moneyFormat($_resultpositive); ?></th> 
                        </tr>
                        <tr>
                            <th  colspan="1"><?php echo $_lang_index_ammountpending; ?></th>                            
                            <th colspan="6" ><?php echo "USD " . moneyFormat($_resultpending); ?></th> 
                        </tr>
                        <tr>
                            <th  colspan="1"><?php echo $_lang_index_creditlimit; ?></th>                            
                            <th colspan="6" ><?php echo "USD " . moneyFormat($this->creditlimit); ?></th> 
                        </tr>
                        <tr>
                            <th  colspan="1"><?php echo $_lang_index_creditlimitstatus; ?></th>                            
                            <th colspan="6" ><?php
                                $_creditlimit_status = ($_resultpending * 100) / $this->creditlimit;


                                if ($_creditlimit_status > 100) {

                                    echo '<span style="color:red">' . moneyFormat($_creditlimit_status) . "%" . '</span>';
                                } else {
                                    echo '<span style="color:blue">' . moneyFormat($_creditlimit_status) . "%" . '</span>';
                                }
                                ?></th> 
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>

</div> 