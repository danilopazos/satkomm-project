<?php
$_lang_index_label_modulemystatement= "My account statement";
$_lang_index_label_module= "Payments";
$_lang_index_label_modulemypayments= "Payments";
$_lang_index_label_action = "Payment history";
 
$_lang_index_label_table_header= "Payments and Invoices";


$_lang_index_providername = "Provider name";
$_lang_index_customername = "Customer name";
$_lang_index_invoicetype = "Invoice type";

$_lang_index_invoiceno = "Invoice no";
$_lang_index_invoiceammount = "Invoice ammount";
$_lang_index_invoicedate = "Invoice date";
$_lang_index_invoiceduedate = "Invoice due date";

$_lang_index_invoiceref = "Invoice reference";
$_lang_index_paymentammount = "Payment ammount";
$_lang_index_paymentref = "Payment reference";
$_lang_index_paymentdate = "Payment date";
$_lang_index_paymentfile = "Payment voucher";
$_lang_index_totalsum= "Total";

$_lang_index_registeruser = "Registered by";
$_lang_index_registerdatetime = "Registered time";

$_lang_index_creditlimit = "Credit limit";
$_lang_index_ammountpending = "Ammount pending";
$_lang_index_positivebalance = "Positive balance";
$_lang_index_creditlimitstatus= "Credit limit status";


$_lang_index_savepayment = "Save payment";
$_lang_index_cancel = "Cancel";

$_lang_index_duedate= "Due date";
$_lang_index_date = "Date";
$_lang_index_ammount = "Ammount";
$_lang_index_type ="Operation type";
$_lang_index_type_invoice ="Invoice";
$_lang_index_type_payment ="Payment";