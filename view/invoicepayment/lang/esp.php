<?php
$_lang_index_label_modulemystatement= "Mi estado de cuenta";
$_lang_index_label_module= "Pagos";
$_lang_index_label_modulemypayments= "Mis pagos";
$_lang_index_label_action = "Hist&oacute;rico de pagos";
 
$_lang_index_label_table_header= "Facturas y pagos";

$_lang_index_providername = "Proveedor";
$_lang_index_customername = "Cliente";
$_lang_index_invoicetype = "Tipo de factura";
$_lang_index_creditlimit = "L&iacute;mite de cr&eacute;dito";
$_lang_index_invoiceno = "Nro Factura";
$_lang_index_invoiceammount = "Monto de la factura";
$_lang_index_invoicedate = "Fecha de la factura";
$_lang_index_invoiceduedate = "Fecha de vencimiento";
$_lang_index_invoiceref = "Referencia de la factura";
$_lang_index_paymentammount = "Monto del pago";
$_lang_index_paymentref = "Referencia del pago";
$_lang_index_paymentdate = "Fecha del pago";
$_lang_index_paymentfile = "Voucher del pago";
$_lang_index_totalsum= "Totales";

$_lang_index_registeruser = "Registrado por";
$_lang_index_registerdatetime = "Fecha de registro";

$_lang_index_creditlimit = "L&iacute;nea de cr&eacute;dito";
$_lang_index_ammountpending = "Monto pendiente de pago";
$_lang_index_positivebalance = "Saldo a favor";
$_lang_index_creditlimitstatus= "Estado de l&iacute;nea de cr&eacute;dito";
 
$_lang_index_makepayment = "Registrar pago";
$_lang_index_savepayment = "Guardar pago";
$_lang_index_cancel = "Cancelar";

$_lang_index_duedate= "Vencimiento";
$_lang_index_date = "Fecha";
$_lang_index_ammount = "Monto";
$_lang_index_type ="Tipo de operaci&oacute;n";
$_lang_index_type_invoice ="Factura";
$_lang_index_type_payment ="Pago";