<!--- Product Add Cart Modal Window -->
<div class="modal fade" id="invoicepayment-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form name="savepayment" id="savepayment" method="post" action="/invoicepayment/savepayment">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->

                            <div class="modal-body">

                                <table class="table table-striped table-bordered table-hover table-full-width"  >

                                    <tr>
                                        <th colspan="2"><?php echo $_lang_index_paymentammount; ?>

                                        <td>
                                            <input type="text" id="paymentammount" name="paymentammount" class="form-control" />
                                        </td>

                                        </th>


                                    <tr><th colspan="2"><?php echo $_lang_index_paymentdate; ?></th>
                                        <td>

                                            <div class="input-group" id="picker-container">
                                                <input  name="paymentdate" id="paymentdate" value="<?php echo date("Y-m-d"); ?>"   type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                            </div>                                  

                                        </td> 
                                    </tr>    

                                    <tr><th colspan="2"><?php echo $_lang_index_paymentref; ?></th>
                                        <td>
                                            <input type="hidden" id="customerid" name="customerid" value="<?php echo $this->customerid; ?>" />
                                            <input type="hidden" id="invoiceid" name="invoiceid" value="0" />
                                            <input type="hidden" id="tableinvoice" name="tableinvoice" value="invoicetocustomer" />
                                            <textarea name="paymentref" id="paymentref" class="form-control"></textarea>

                                        </td> 
                                    </tr>                          

                                </table>


                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    <?php echo $_lang_index_cancel; ?>
                                </button> 

                                <button type="submit" id="btn_confirmorder" class="btn btn-green">
                                    <i class="fa fa-check"></i>   <?php echo $_lang_index_savepayment; ?>
                                </button>

                            </div>

                        </div>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-list-alt"></i>
                <a href="/invoicepayment/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
            <h4><?php echo $this->accountname; ?>  </h4>
            <?php
            echo '<a class="btn btn-xs btn-default" href="/invoicepayment/index">';
            echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
            echo '</a>';
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<?php
$style_ammountpending = "";
$style_positivebalance = "";
 
$style_ammountinvoice = ' style=" color:black;" ';
$style_ammountpayment = ' style=" color:black;" ';
?>

<div class="row">
    <div class="col-sm-12">


        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_module; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">        

                    <thead>
                        <tr>
                            <th><?php echo $_lang_index_type; ?></th>
                            <th>#</th>
                            <th><?php echo $_lang_index_date; ?></th>
                            <th><?php echo $_lang_index_duedate; ?></th>
                            <th><?php echo $_lang_index_paymentammount; ?></th>
                            <th><?php echo $_lang_index_invoiceammount; ?></th>
                            <th><a href="#" class="btn btn-xs btn-primary" onclick="modal_setpayment();"  ><?php echo $_lang_index_makepayment; ?></a></th> 
                        </tr>
                    </thead>                    

                    <?php
                    $total = count($this->aInvoices);

                    $_totalpaid = 0;
                    $_totalinvoices = 0;
                    $tempinvoiceid = 0;
                    if ($total > 0) {
                        for ($i = 0; $i < $total; $i++) {
                            $e = $this->aInvoices[$i];

                            $_docno = $e->invoiceno;
                            $_idcrypt = $e->idcrypt;

                            $style_ammountpending = ' style=" color:red;" ';


                            $style_positivebalance = ' style=" color:blue;" ';


                            $_operationtype = $_lang_index_type_invoice;
                            if ($e->type == "payment") {
                                $_operationtype = $_lang_index_type_payment;
                                $_docno = $e->id;
                            }

                            $tr = "";
                            $tr .= "<tr>";
                            $tr .= "<td>" . $_operationtype . "</td>";
                            
                            if ($e->type != "payment") {
                                
                                if(intval($this->session_accountid) == 10001){
                                    $tr .= '<td> <a target="_blank" class="btn btn-xs btn-default" href="/invoice/view/idcrypt/'.$_idcrypt.'">' . $_docno . '</a></td>';
                                }else{
                                    $tr .= '<td> <a target="_blank" class="btn btn-xs btn-default" href="/invoicetc/view/id/'.$_idcrypt.'">' . $_docno . '</a></td>';
                                }
                                
                                
                            }else{
                                $tr .= '<td> </td>';
                            }
                            
                            $tr .= "<td>" . $e->invoicedate . "</td>";
                            $tr .= "<td>" . $e->invoiceduedate . "</td>";


                            if ($e->type == "payment") {
                                $tr .= "<td $style_ammountpayment >USD " . moneyFormat($e->invoiceammount) . "</td>";
                                $tr .= "<td  colspan='2' > </td>";
                                $_totalpaid = $_totalpaid + $e->invoiceammount;
                            } else {
                                $tr .= "<td></td>";
                                $tr .= "<td $style_ammountinvoice  colspan='2'  >USD " . moneyFormat($e->invoiceammount) . "</td>";

                                $_totalinvoices = $_totalinvoices + $e->invoiceammount;
                            }

                            
 
                            $tr .= "</tr>";

                            echo $tr;

                            $tempinvoiceid = $e->invoiceid;
                        }
                    } else {
                        
                    }
 
                    $_resultpending = 0;
                    $_resultpositive = 0;
 
                    $_resultpending = $_totalinvoices - $_totalpaid;
                    $_resultpositive = $_totalpaid - $_totalinvoices;
                    
                    if($_resultpending < 0){ $_resultpending = 0; } 
                    if($_resultpositive < 0){ $_resultpositive = 0; } 
                    
                    ?>
                    <tfoot>
                        <tr>
                            <th   colspan="4"><?php echo $_lang_index_totalsum; ?></th>                            
                            <th <?php echo $style_ammountpayment; ?>><?php echo "USD " . moneyFormat($_totalpaid); ?></th>
                            <th <?php echo $style_ammountinvoice; ?> colspan="2"><?php echo "USD " . moneyFormat($_totalinvoices); ?></th>
                        </tr>
                        
                        <tr>
                            <th  colspan="1"><?php echo $_lang_index_positivebalance; ?></th>                            
                            <th colspan="6" ><?php echo "USD " . moneyFormat($_resultpositive); ?></th> 
                        </tr>
                        <tr>
                            <th  colspan="1"><?php echo $_lang_index_ammountpending; ?></th>                            
                            <th colspan="6" ><?php echo "USD " . moneyFormat($_resultpending); ?></th> 
                        </tr>
                        <tr>
                            <th  colspan="1"><?php echo $_lang_index_creditlimit; ?></th>                            
                            <th colspan="6" ><?php echo "USD " . moneyFormat($this->creditlimit); ?></th> 
                        </tr>
                        <tr>
                            <th  colspan="1"><?php echo $_lang_index_creditlimitstatus; ?></th>                            
                            <th colspan="6" ><?php 
                            
                            $_creditlimit_status = ($_resultpending*100)/$this->creditlimit;
                            
                            
                            if($_creditlimit_status > 100){
                                
                                echo  '<span style="color:red">'.moneyFormat($_creditlimit_status)."%".'</span>'; 
                            }else{
                                echo  '<span style="color:blue">'.moneyFormat($_creditlimit_status)."%".'</span>'; 
                            } 
                            ?></th> 
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>

</div> 