<script type="text/javascript">
    _select_contact = '<?php echo $_lang_register_label_select_contact; ?>';
    _quotetaxespercent = <?php echo doubleval($this->quote->taxespercent); ?>;
</script>




<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-default">


            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_quoteinfo; ?>
                <div class="panel-tools">

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>


            <div class="panel-body  form-horizontal">  

                <table class="table table-responsive table-striped">
                    <tr>
                        <th><?php echo $_lang_register_label_quoteno; ?></th><td><?php echo $this->quote->quoteno; ?></td>  
                        <th><?php echo $_lang_register_label_status; ?></th><td><?php echo $_lang_quotestatus[$this->quote->status]; ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $_lang_register_label_subject; ?></th><td><?php echo $this->quote->subject; ?></td> 
                        <th><?php echo $_lang_register_label_salesman; ?></th><td><?php echo $this->quote->assigned_names; ?></td></tr>
                    <tr>
                        <th><?php echo $_lang_register_label_quotedate; ?></th><td><?php echo $this->quote->quotedate; ?></td>  
                        <th><?php echo $_lang_register_label_quoteuntil; ?></th><td><?php echo $this->quote->quoteuntil; ?></td>
                    </tr>

                    <tr>
                        <th><?php echo $_lang_view_label_downloadquote; ?></th>
                        <td> 
                            <a target="_blank" href="../../download/id/<?php echo $this->quote->idcrypt; ?>" class="btn btn-xs btn-red"><i class="fa fa-file-pdf-o"></i>&nbsp;<?php echo $_lang_view_label_download; ?></a>
                        </td> 
                        <th> </th>
                        <td>  

                            <a href="#" id="emailModal" class="btn btn-xs btn-primary"><i class="fa fa-envelope-o"></i> &nbsp; <?php echo $_lang_view_label_sendemail; ?></a>
                        </td> 
                    </tr>


                </table>


            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-default">


            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_customerinfo; ?>
                <div class="panel-tools">

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>


            <div class="panel-body  form-horizontal">  

                <table class="table table-responsive table-striped">
                    <tr>
                        <th><?php echo $_lang_register_label_customer; ?></th><td><?php echo $this->quote->customer_name; ?></td>
                        <th><?php echo $_lang_register_label_contact; ?></th><td><?php echo $this->quote->contact_names; ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $_lang_register_label_telephone; ?></th><td><?php
                            if ($this->quote->contacttelephone != "") {
                                echo $this->quote->contacttelephone;
                            } else {
                                echo "---";
                            }
                            ?></td>
                        <th><?php echo $_lang_register_label_mobile; ?></th><td><?php
                            if ($this->quote->contactmobile != "") {
                                echo $this->quote->contactmobile;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $_lang_register_label_email; ?></th><td><?php
                            if ($this->quote->contactemail != "") {
                                echo $this->quote->contactemail;
                            } else {
                                echo "---";
                            }
                            ?></td>
                        <th><?php echo $_lang_register_label_salutation; ?></th><td><?php
                            if ($this->quote->contactsalutation != "") {
                                echo $this->quote->contactsalutation;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                </table>

            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>    

<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-default">


            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_termsinfo; ?>
                <div class="panel-tools">

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>


            <div class="panel-body  form-horizontal">  

                <table class="table table-responsive table-striped">
                    <tr>
                        <th><?php echo $_lang_register_label_paymentterms; ?></th><td><?php
                            if ($this->quote->paymentterms != "") {
                                echo $_lang_paymentterms[$this->quote->paymentterms];
                            } else {
                                echo "---";
                            }
                            ?></td>
                        <th><?php echo $_lang_register_label_paymentother; ?></th><td><?php
                            if ($this->quote->paymenttermsother != "") {
                                echo $this->quote->paymenttermsother;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    
                    
                    <tr>
                        <th><?php echo $_lang_deliveryterms; ?></th><td><?php
                            if ($this->quote->deliveryterms != "") {
                               echo $_lang_deliveryterms_option[$this->quote->deliveryterms];
                            } else {
                                echo "---";
                            }
                            ?></td>
                        <th><?php echo $_lang_deliveryterms_other; ?></th><td><?php
                            if ($this->quote->deliverytermsother != "") {
                                echo $this->quote->deliverytermsother;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    
                    
                    
                    
                    <tr>
                        <th><?php echo $_lang_register_label_carriercompany; ?></th><td><?php
                            if ($this->quote->carriercompany != "") {
                                echo $this->quote->carriercompany;
                            } else {
                                echo "---";
                            }
                            ?></td>
                        <th><?php echo $_lang_register_label_carriercompany_other; ?></th><td><?php
                            if ($this->quote->carriercompanyother != "") {
                                echo $this->quote->carriercompanyother;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <th colspan="4"><?php echo $_lang_register_label_quoteobs; ?></th>
                    </tr>
                    <tr>
                        <td colspan="4"><?php
                            if ($this->quote->obs != "") {
                                echo $this->quote->obs;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                </table>

            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>    

<div class="row">

    <div class="col-sm-6">
        <!-- start: TEXT AREA PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_billaddress; ?>
                <div class="panel-tools">

                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>

                </div>
            </div>
            <div class="panel-body  form-horizontal" >

                <table class="table table-responsive table-striped">
                    <tr>
                        <th><?php echo $_lang_register_label_country; ?></th><td><?php
                            if ($this->quote->bill_country != "") {
                                echo $this->quote->bill_country;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $_lang_register_label_state; ?></th><td><?php
                            if ($this->quote->bill_state != "") {
                                echo $this->quote->bill_state;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $_lang_register_label_city; ?></th><td><?php
                            if ($this->quote->bill_city != "") {
                                echo $this->quote->bill_city;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $_lang_register_label_street; ?></th><td><?php
                            if ($this->quote->bill_street != "") {
                                echo $this->quote->bill_street;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $_lang_register_label_zipcode; ?></th><td><?php
                            if ($this->quote->bill_zipcode != "") {
                                echo $this->quote->bill_zipcode;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $_lang_register_label_pobox; ?></th><td><?php
                            if ($this->quote->bill_pobox != "") {
                                echo $this->quote->bill_pobox;
                            } else {
                                echo "---";
                            }
                            ?></td>
                    </tr>  
                </table>

            </div>
        </div>
        <!-- end: TEXT AREA PANEL -->
    </div>


    <?php
    if ($this->quote->shippingenabled == "on") {
        ?>
        <div class="col-sm-6">
            <!-- start: TEXT AREA PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_shipaddress; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>



                    </div>
                </div>
                <div class="panel-body  form-horizontal" >

                    <table class="table table-responsive table-striped">
                        <tr>
                            <th><?php echo $_lang_register_label_country; ?></th><td><?php
                                if ($this->quote->ship_country != "") {
                                    echo $this->quote->ship_country;
                                } else {
                                    echo "---";
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_state; ?></th><td><?php
                                if ($this->quote->ship_state != "") {
                                    echo $this->quote->ship_state;
                                } else {
                                    echo "---";
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_city; ?></th><td><?php
                                if ($this->quote->ship_city != "") {
                                    echo $this->quote->ship_city;
                                } else {
                                    echo "---";
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_street; ?></th><td><?php
                                if ($this->quote->ship_street != "") {
                                    echo $this->quote->ship_street;
                                } else {
                                    echo "---";
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_zipcode; ?></th><td><?php
                                if ($this->quote->ship_zipcode != "") {
                                    echo $this->quote->ship_zipcode;
                                } else {
                                    echo "---";
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $_lang_register_label_pobox; ?></th><td><?php
                                if ($this->quote->ship_pobox != "") {
                                    echo $this->quote->ship_pobox;
                                } else {
                                    echo "---";
                                }
                                ?></td>
                        </tr>  
                    </table>

                </div>
            </div>
            <!-- end: TEXT AREA PANEL -->
        </div>

        <?php
    }
    ?>

</div>