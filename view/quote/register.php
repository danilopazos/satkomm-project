<!-- register update -->
<div class="modal fade" id="alert-selectaccount" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                 <div  class="alert alert-danger ">
                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_msg_selectaccount; ?>
                </div> 
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?php echo $_lang_view_label_close; ?>
                </button>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<script type="text/javascript">
<?php
echo ' var _quotedate   =  new Date();   ';
echo ' var _quoteuntil   =  new Date();  ';

$_quotedate = date('Y-m-d');
$_quoteuntil = date('Y-m-d');

if (intval($this->accountconfig->confquotevaliddays) > 0) {
    $_quoteuntildays = intval($this->accountconfig->confquotevaliddays);
    $_quoteuntil = date('Y-m-d', strtotime($_quoteuntil . ' + ' . $_quoteuntildays . ' days'));
}


if ($this->quote->quotedate != "") {
    $aDate = split("-", substr(trim($this->quote->quotedate), 0, 10));
    $_quotedate = substr($this->quote->quotedate, 0, 10);
    echo '   _quotedate   =  new Date(' . $aDate[0] . ', ' . intval($aDate[1]) . ', ' . intval($aDate[2]) . '); ';
}
if ($this->quote->quoteuntil != "") {
    $aDate = split("-", substr(trim($this->quote->quoteuntil), 0, 10));
    $_quoteuntil = substr($this->quote->quoteuntil, 0, 10);
    echo '   _quoteuntil   =  new Date(' . $aDate[0] . ', ' . intval($aDate[1]) . ', ' . intval($aDate[2]) . '); ';
}
?>
</script>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/quote/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            if ($this->session_accountid != $this->id) {
                echo '<a class="btn btn-xs btn-default" href="/account/index">';
                echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<form role="form" class="form-register   box-register" id="form"  >

    <div class="errorHandler alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
    </div>
    <div id="errorCodeRepeated" class="alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_coderepeated; ?>
    </div>
    <div id="errorProcessProblems" class="alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
    </div>
    <div id="errorSubdomainRepeated" class="alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
    </div>
    <div id="messageProcessOk" class="alert alert-success no-display">
        <i class="fa fa-remove-sign"></i> Quote registered
    </div>


    <div class="tabbable">
        <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
            <li class="active">
                <a data-toggle="tab" href="#panel_quoteinfo">
                    <?php echo $_lang_register_label_panel_quoteinfo; ?>
                </a>
            </li>

            <li>
                <a data-toggle="tab" href="#panel_quotedetail">
                    <?php echo $_lang_register_label_panel_quotedetail; ?>
                </a>
            </li> 
        </ul>


        <div class="tab-content">
            <div id="panel_quoteinfo" class="tab-pane in active">

                <?php require_once 'quote_info.php'; ?>

            </div>
            <div id="panel_quotedetail" class="tab-pane in">

                <?php require_once 'quote_detail.php'; ?>

            </div>

        </div>

    </div> 




    <?php
    if ($this->id != "") {
        echo '<input type="hidden" name="id" id="accountid" value="' . $this->id . '" />';
    }
    ?>

</form>
<!-- end: PAGE CONTENT-->