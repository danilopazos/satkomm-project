<script type="text/javascript">
    _select_contact = '<?php echo $_lang_register_label_select_contact ; ?>';
    _quotetaxespercent = <?php echo doubleval( $this->quote->taxespercent ); ?>;
</script>

    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_quoteinfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  


                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="code">
                        <?php echo $_lang_register_label_quoteno; ?>
                        </label>
                        <div class="col-sm-4">
                            <input type="text"  id="code" name="code" class="form-control" value="<?php echo $this->quote->quoteno; ?>">
                        </div>
                        
                        
                        <?php 
                        if($this->quote->idcrypt ==""){
                            
                            
                            
                        }else{
                            ?>
                            <label class="col-sm-2 control-label" for="status">
                            <?php echo $_lang_register_label_status; ?>
                            </label>
                            <div class="col-sm-4">
                                <select name="status" id="status" class="form-control">
                                    <?php
                                    $total = count($this->aStatus);
                                    if ($total > 0) {
                                        foreach ($this->aStatus as $status) {
                                            $selected = '';
                                            if ($status == $this->quote->status) {
                                                $selected = ' selected = "true" ';
                                            }
                                            echo '<option ' . $selected . ' value="' . $status . '">' . $_lang_quotestatus[$status] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>  
                            <?php
                        }
                        
                        ?>
                        
                      

                    </div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="subject">
                         <?php echo $_lang_register_label_subject; ?>
                        </label>                   
                        <div class="col-sm-4">
                            <input value="<?php echo $this->quote->subject; ?>"  type="text"  id="subject"   name="subject" class="form-control">
                        </div> 


                        <label class="col-sm-2 control-label" for="salesman">
                        <?php echo $_lang_register_label_salesman; ?>
                        </label>
                        <div class="col-sm-4">
                            <select name="salesman" id="salesman" class="form-control">
                            <?php
                            $total = count($this->aSalesman);
                            if ($total > 0) {
                                foreach ($this->aSalesman as $salesman) {
                                    $selected = '';
                                    if ($salesman->id == $this->quote->assignedto) {
                                        $selected = ' selected = "true" ';
                                    }
                                    echo '<option ' . $selected . ' value="' . $salesman->id . '">' . $salesman->firstname . ' ' . $salesman->lastname . '</option>';
                                }
                            }
                            ?>
                            </select>
                        </div> 
                    </div>




                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="quotedate">
                        <?php echo $_lang_register_label_quotedate; ?>
                        </label>
                        <div class="col-sm-4">
                            <div class="input-group" id="picker-container">
                                <input  id="quotedate" name="quotedate"  value="<?php echo $_quotedate; ?>"  type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            </div>                        
                        </div>

                        <label class="col-sm-2 control-label" for="quoteuntil">
                        <?php echo $_lang_register_label_quoteuntil; ?>
                        </label>
                        <div class="col-sm-4">
                            <div class="input-group" id="picker-container">
                                <input  id="quoteuntil" name="quoteuntil"  value="<?php echo $_quoteuntil; ?>"  type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            </div>                        
                        </div>

                    </div>
 

  
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_customerinfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  


                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="customer">
                        <?php echo $_lang_register_label_customer; ?>
                        </label>
                        <div class="col-sm-4">
                            
                            <input type="input" value="<?php echo $this->quote->customer_name; ?>" name="customer" id="customer" class=" ui-autocomplete-input form-control "  /> 
                            
                            <!--<select name="customer" id="customer" class="form-control"></select>-->
                            <input type="hidden" name="customerid" id="customerid" value ="<?php echo intval($this->quote->customerid); ?>" />
                        </div>         

                        <label class="col-sm-2 control-label" for="contactid">
                        <?php echo $_lang_register_label_contact; ?>
                        </label>
                        <div class="col-sm-4">
                            <select name="contactid" id="contactid" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select_contact; ?></option>
                                <?php 
                                $totalContact = count($this->aContact);
                                if($totalContact > 0){
                                    foreach($this->aContact as $contact){
                                        $selected = '  ';
                                        if($this->quote->contactid == $contact->id){
                                            $selected = ' selected = "true" ';
                                        } 
                                        echo '<option '.$selected.' value="'.$contact->id.'">'.$contact->firstname.' '.$contact->lastname.'</option>';
                                    }
                                }
                                ?>
                            </select>   
                        </div>                        

                    </div>

                    
                    
                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="telephone">
                         <?php echo $_lang_register_label_telephone; ?>
                        </label>                   
                        <div class="col-sm-4">
                            <input value="<?php echo $this->quote->contacttelephone; ?>"  type="text"  id="contacttelephone"   name="contacttelephone" class="form-control">
                        </div> 

                        <label class="col-sm-2 control-label" for="mobile">
                         <?php echo $_lang_register_label_mobile; ?>
                        </label>                   
                        <div class="col-sm-4">
                            <input value="<?php echo $this->quote->contactmobile; ?>"  type="text"  id="contactmobile"   name="contactmobile" class="form-control">
                        </div> 

                    </div>

 
                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="email">
                         <?php echo $_lang_register_label_email; ?>
                        </label>                   
                        <div class="col-sm-4">
                            <input value="<?php echo $this->quote->contactemail; ?>"  type="text"  id="contactemail"   name="contactemail" class="form-control">
                        </div> 

                        <label class="col-sm-2 control-label" for="salutation">
                         <?php echo $_lang_register_label_salutation; ?>
                        </label>                   
                        <div class="col-sm-4">
                            <input value="<?php echo $this->quote->contactsalutation; ?>"  type="text"  id="contactsalutation"   name="contactsalutation" class="form-control">
                        </div> 

                    </div>                    
                    
                    
  
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>    
    
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_termsinfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  


                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="paymentterms">
                        <?php echo $_lang_register_label_paymentterms; ?>
                        </label>
                        <div class="col-sm-4">
                            <select name="paymentterms" id="paymentterms" class="form-control">
                                 <?php
                                $total = count($this->aPaymentTerms);
                                if ($total > 0) {
                                    echo '<option  value="">' . $_lang_register_label_paymentterms_select . '</option>';
                                    foreach ($this->aPaymentTerms as $paymentterms) {
                                        $selected = '';
                                        if ($paymentterms  == $this->quote->paymentterms) {
                                            $selected = ' selected = "true" ';
                                        }
                                        echo '<option ' . $selected . ' value="' . $paymentterms . '">' . $_lang_paymentterms[$paymentterms] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>         

                        <label class="col-sm-2 control-label" for="paymenttermsother">
                        <?php echo $_lang_register_label_paymentother; ?>
                        </label>
                        <div class="col-sm-4">
                            <input value="<?php echo $this->quote->paymenttermsother; ?>"  type="text"  id="paymenttermsother"   name="paymenttermsother" class="form-control">  
                        </div>                        

                    </div>

           
                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="deliveryterms">
                        <?php echo $_lang_deliveryterms; ?>
                        </label>
                        <div class="col-sm-4">
                            <select name="deliveryterms" id="deliveryterms" class="form-control">
                                 <?php
                                $total = count($this->aDeliveryTerms);
                                if ($total > 0) {
                                    echo '<option  value="">' . $_lang_register_label_paymentterms_select . '</option>';
                                    foreach ($this->aDeliveryTerms as $e) {
                                        $selected = '';
                                        if ($e  == $this->quote->deliveryterms) {
                                            $selected = ' selected = "true" ';
                                        }
                                        echo '<option ' . $selected . ' value="' . $e . '">' . $_lang_deliveryterms_option[$e] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>         

                        <label class="col-sm-2 control-label" for="deliverytermsother">
                        <?php echo $_lang_deliveryterms_other; ?>
                        </label>
                        <div class="col-sm-4">
                            <input value="<?php echo $this->quote->deliverytermsother; ?>"  type="text"  id="deliverytermsother"   name="deliverytermsother" class="form-control">  
                        </div>                        

                    </div>
                    
                    
                    
 
                   <div class="form-group">

                        <label class="col-sm-2 control-label" for="carriercompany">
                        <?php echo $_lang_register_label_carriercompany; ?>
                        </label>
                        <div class="col-sm-4">
                            <select name="carriercompany" id="carriercompany" class="form-control">
                                 <?php
                                $total = count($this->aCarrierCompany);
                                if ($total > 0) {
                                    echo '<option  value="">' . $_lang_register_label_carriercompany_select . '</option>';
                                    foreach ($this->aCarrierCompany as $carriercompany) {
                                        $selected = '';
                                        if ($carriercompany->code  == $this->quote->carriercompany) {
                                            $selected = ' selected = "true" ';
                                        }
                                        echo '<option ' . $selected . ' value="' . $carriercompany->code . '">' . $carriercompany->name . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>         

                        <label class="col-sm-2 control-label" for="carriercompanyother">
                        <?php echo $_lang_register_label_carriercompany_other; ?>
                        </label>
                        <div class="col-sm-4">
                            <input value="<?php echo $this->quote->carriercompanyother; ?>"  type="text"  id="carriercompanyother"   name="carriercompanyother" class="form-control">  
                        </div>                        

                    </div>                
                    
                   <div class="form-group">

                        <label class="col-sm-2 control-label" for="quoteobs">
                        <?php echo $_lang_register_label_quoteobs; ?>
                        </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="obs" id="obs"><?php echo $this->quote->obs; ?></textarea>
                        </div>         
                       
                    </div> 
                    
  
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>    
    
    <div class="row">

        <div class="col-sm-6">
            <!-- start: TEXT AREA PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_billaddress; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>
                <div class="panel-body  form-horizontal" >

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_country">
                        <?php echo $_lang_register_label_country; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value="<?php echo $this->quote->bill_country; ?>"     type="text"  id="bill_country" name="bill_country" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_state">
                        <?php echo $_lang_register_label_state; ?>
                        </label>
                        <div class="col-sm-5">
                            <input   value="<?php echo $this->quote->bill_state; ?>" type="text"  id="bill_state" name="bill_state" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_city">
                        <?php echo $_lang_register_label_city; ?>
                        </label>
                        <div class="col-sm-5">
                            <input     value="<?php echo $this->quote->bill_city; ?>"      type="text"  id="bill_city" name="bill_city" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_street">
                        <?php echo $_lang_register_label_street; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value="<?php echo $this->quote->bill_street; ?>"       type="text"  id="bill_street" name="bill_street" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_zipcode">
                        <?php echo $_lang_register_label_zipcode; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value="<?php echo $this->quote->bill_zipcode; ?>"    type="text"  id="bill_zipcode" name="bill_zipcode" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_pobox">
                        <?php echo $_lang_register_label_pobox; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value="<?php echo $this->quote->bill_pobox; ?>"    type="text"  id="bill_pobox" name="bill_pobox" class="form-control">
                        </div> 
                    </div>


                </div>
            </div>
            <!-- end: TEXT AREA PANEL -->
        </div>

        <div class="col-sm-6">
            <!-- start: TEXT AREA PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_shipaddress; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>
                        


                    </div>
                </div>
                
                <?php 
                $shipdisabled = ' disabled="true"  ';
                if($this->quote->shippingenabled =="on"){
                     $shipdisabled = ' ';
                }
                ?>
                
                <div class="panel-body  form-horizontal" >

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_country">
                        <?php echo $_lang_register_label_country; ?>
                        </label>
                        <div class="col-sm-5">
                            <input   <?php echo $shipdisabled; ?>  value="<?php echo  $this->quote->ship_country; ?>"    type="text"  id="ship_country" name="ship_country" class="form-control">
                        </div>
                        
                        <div class="col-sm-5">
                            <?php 
                                if($this->quote->shippingenabled =="on"){
                                   echo '<input id="controlControlShipping" name="controlControlShipping" type="checkbox" checked>'; 
                                }else{
                                    echo '<input id="controlControlShipping" name="controlControlShipping" type="checkbox" >'; 
                                }
                            ?>
                            
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_state">
<?php echo $_lang_register_label_state; ?>
                        </label>
                        <div class="col-sm-5">
                            <input  <?php echo $shipdisabled; ?>   value="<?php echo $this->quote->ship_state; ?>"  type="text"  id="ship_state" name="ship_state" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_city">
<?php echo $_lang_register_label_city; ?>
                        </label>
                        <div class="col-sm-5">
                            <input  <?php echo $shipdisabled; ?> value="<?php echo  $this->quote->ship_city; ?>"     type="text"  id="ship_city" name="ship_city" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_street">
<?php echo $_lang_register_label_street; ?>
                        </label>
                        <div class="col-sm-5">
                            <input <?php echo $shipdisabled; ?>   value="<?php echo  $this->quote->ship_street; ?>"     type="text"  id="ship_street" name="ship_street" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_zipcode">
<?php echo $_lang_register_label_zipcode; ?>
                        </label>
                        <div class="col-sm-5">
                            <input <?php echo $shipdisabled; ?>    value="<?php echo  $this->quote->ship_zipcode; ?>"  type="text"  id="ship_zipcode" name="ship_zipcode" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_pobox">
<?php echo $_lang_register_label_pobox; ?>
                        </label>
                        <div class="col-sm-5">
                            <input  <?php echo $shipdisabled; ?>   value="<?php echo  $this->quote->ship_pobox; ?>"  type="text"  id="ship_pobox" name="ship_pobox" class="form-control">
                        </div>
                    </div>


                </div>
            </div>
            <!-- end: TEXT AREA PANEL -->
        </div>
        
          <input type="hidden" name="id" value="<?php echo $this->quote->idcrypt; ?>" id="id" />
        
        
    </div>
 