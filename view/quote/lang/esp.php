<?php
//-- Catalog Index
$_lang_index_label_module= "Cotizaciones";
$_lang_index_label_action = "Registrar";
$_lang_index_label_action_view = "Detalle";

$_lang_index_table_title = "Lista de cotizaciones";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_name = "Nombre";
$_lang_index_table_th_networkcode = "Red";
$_lang_index_table_th_price = "Precio";

$_lang_view_label_close = "Cerrar";
$_lang_view_label_addcart= "A&ntilde;adir al carro";

$_lang_view_block_addcart = "A&ntilde;adir al carro";

$_lang_panel_intro = "Intro";
$_lang_panel_description = "Desc";
$_lang_panel_package = "Paquete";
$_lang_panel_technicalspecs = "Especs";
$_lang_panel_features= "Caracter&iacute;sticas";
$_lang_panel_docs = "Documentos";

$_lang_view_label_quantity = "Cantidad";
$_lang_view_label_price = "Precio";
$_lang_view_label_subtotal = "Sub-total";
$_lang_view_label_cartitem = "Item a&ntilde;adido";


$_lang_register_block_quoteinfo = "Informaci&oacute;n";
$_lang_register_block_customerinfo = "Informaci&oacute;n del cliente";
$_lang_register_block_termsinfo = "T&eacute;rminos y condiciones";        
        
$_lang_createquote = "Crear Cotizaci&oacute;n";
$_lang_index_label_registeraction = "Registrar";
$_lang_register_label_quoteno= "Cotizaci&oacute;n #";
$_lang_register_label_status = "Estado";
$_lang_register_label_subject = "Asunto";
$_lang_register_label_salesman = "Ejecutivo";
$_lang_register_label_quotedate = "Fecha";
$_lang_register_label_quoteuntil = "Validez";
$_lang_register_label_hastaxes = "Impuestos incluidos";
$_lang_register_label_taxespercent = "Impuestos %";


$_lang_register_label_customer = "Cliente";
$_lang_register_label_selectcustomer = "Selecciona la cuenta del cliente";

$_lang_register_label_contact = "Contacto";
$_lang_register_label_select_contact = "Selecciona el contacto";

$_lang_register_label_telephone = "Tel&eacute;fono";
$_lang_register_label_mobile = "Celular";
$_lang_register_label_email = "Email";
$_lang_register_label_salutation = "Saludo/T&iacute;tulo";


$_lang_register_label_paymentterms = "T&eacute;rminos de pago";
$_lang_register_label_paymentterms_select = "Seleccione";
$_lang_register_label_paymentother = "Otros t&eacute;rminos de pago";

$_lang_register_label_carriercompany = "Empresa de envio";
$_lang_register_label_carriercompany_select = "Seleccione";
$_lang_register_label_carriercompany_other = "Otra empresa";
 
$_lang_register_label_quoteobs = "Observaciones";

$_lang_register_block_billaddress = "Direcci&oacute;n de Facturaci&oacute;n";
$_lang_register_block_shipaddress = "Direcci&oacute;n de Envio";
$_lang_register_label_country = "Pa&iacute;s";
$_lang_register_label_state = "Estado";
$_lang_register_label_city = "Ciudad";
$_lang_register_label_street = "Calle";
$_lang_register_label_zipcode  = "Zip Code";
$_lang_register_label_pobox = "P.O. Box";


$_lang_register_label_panel_quoteinfo = "Informaci&oacute;n";
$_lang_register_label_panel_quotedetail = "Items";

$_lang_register_save = "Guardar";

$_lang_register_block_operationdetails = "Detalle de la Operaci&oacute;n";
$_lang_register_block_productdetails = "Productos";
$_lang_register_block_servicesdetails = "Servicios";
$_lang_register_block_accesoriesdetails = "Accesorios";

$_lang_register_operationtype = "Tipos de Operaci&oacute;n";

$_lang_register_operationsale = "Venta";
$_lang_register_operationrent = "Renta";

$_lang_product_addproductbtn = "A&ntilde;adir producto";
$_lang_product_name = "Producto";
$_lang_product_price = "Precio";
$_lang_product_qty = "Cantidad";
$_lang_product_months = "Meses";
$_lang_product_subtotal = "Sub-total";


$_lang_product_addservicebtn = "A&ntilde;adir servicio";
$_lang_service_name = "Servicio";
$_lang_service_price = "Precio";
$_lang_service_activation = "Activaci&oacute;n";
$_lang_service_qty = "Cantidad";
$_lang_service_subtotal = "Sub-total";


$_lang_accessory_addaccessorybtn ="A&nti;de;adir accesorio";
$_lang_accessory_name = "Producto";
$_lang_accessory_price = "Precio";
$_lang_accessory_qty = "Cantidad";
$_lang_accessory_months = "Meses";
$_lang_accessory_subtotal = "Sub-total";


$_lang_operation_subtotal = "Sub-total";
$_lang_operation_discount = "Descuento";
$_lang_operation_shippingcost = "Env&iacute;o";
$_lang_operation_total = "Total";

$_lang_quotestatus["C"] = "Creada";
$_lang_quotestatus["S"] = "Enviada";
$_lang_quotestatus["A"] = "Aprobada";
$_lang_quotestatus["R"] = "Rechazada";



$_lang_paymentterms["prepaid"] = "Pago por adelantado";
$_lang_paymentterms["15"] = "Cr&eacute;dito 15 dias";
$_lang_paymentterms["30"] = "Cr&eacute;dito 30 dias";
$_lang_paymentterms["45"] = "Cr&eacute;dito 45 dias";
$_lang_paymentterms["60"] = "Cr&eacute;dito 60 dias";
$_lang_paymentterms["90"] = "Cr&eacute;dito 90 dias";
$_lang_paymentterms["00"] = "Otros";


$_lang_hastaxes[0]  = "Es exportaci&oacute;n, no incluye impuestos";
$_lang_hastaxes[1]  = "Incluye impuestos";

$_lang_aoperationtype["R"] = "Renta";
$_lang_aoperationtype["S"] = "Venta";

$_lang_register_label_update = "Modificar";

$_lang_register_label_delete = "Eliminar";
$_lang_view_label_close = "Cerrar";

$_lang_view_label_confirmdelete = "Eliminar";
$_lang_view_label_confirmdelete_message = "¿Esta seguro de eliminar esta cotizaci&oacute;n?";
$_lang_view_label_deletedok = "La cotizaci&oacute;n fue eliminada";


$_lang_view_label_generateinvoice = "Generar Factura";


$_lang_quote_title = "Cotizaci&oacute;n";
$_lang_quote_intro = "La siguiente cotización comprende lo siguiente: ";
$_lang_label_paymentterms = "Términos de pagos";
$_lang_label_shippingcompany = "Empresa de envio";

$_lang_view_label_downloadquote = "Descargar cotizaci&oacute;n";
$_lang_view_label_download = "Descargar";

$_lang_deliveryterms =  "T&eacute;rminos de entrega";
$_lang_deliveryterms_other =  "Otros t&eacute;rminos de entrega";
$_lang_deliveryterms_option["pickupinoffice"] = "Recojo en oficina";
$_lang_deliveryterms_option["sendingbycurier"] = "Envio por curier";
$_lang_deliveryterms_option["shippingnotincluded"] = "Envio no incluido"; 
$_lang_deliveryterms_option["other"] = "Otros"; 


$_lang_currency_code = "Moneda"; 
$_lang_currency_exchange = "Tipo de cambio"; 


//------Email Labels------------------------------
$_lang_view_label_sendemail = "Enviar Correo";
$_lang_view_label_email_sent = "Correo enviado";
$_lang_modal_sendemail_title = "Envio de correo";

$_lang_modal_sendemail_subject = "Asunto";
$_lang_modal_sendemail_to = "Para";
$_lang_modal_sendemail_toinfo = "(Separado por comas , )";
$_lang_modal_sendemail_attach = "Adjunto";
$_lang_modal_sendemail_description = "Descripci&oacute;n";

$_lang_view_label_email_sentsuccess = "Correo enviado satisfactoriamente";
$_lang_view_label_email_senterror   = "Problemas enviando el correo, por favor intente nuevamente o m&aacute;s tarde";


$_lang_msg_selectaccount = "Por favor seleccione la cuenta cliente y su contacto primero";