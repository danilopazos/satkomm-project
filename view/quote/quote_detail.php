<?php 
$_aProductRowId = array();
$_aServiceRowId = array();
?>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_operationdetails; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
               
                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="operationtype">
                        <?php echo $_lang_register_operationtype; ?>
                        </label>
                        <div class="col-sm-4"> 
                            <select name="operationtype" id="operationtype" class="form-control">
                            <?php
                            $total = count($this->aOperationType);
                            if ($total > 0) {
                                foreach ($this->aOperationType as $operationtype) {
                                    $selected = '';
                                    if ($operationtype  == $this->quote->operationtype) {
                                        $selected = ' selected = "true" ';
                                    }
                                     echo '<option ' . $selected . ' value="' . $operationtype . '">' ;
                                     if($operationtype == "S"){
                                         echo $_lang_register_operationsale;
                                     }
                                     if($operationtype == "R"){
                                         echo $_lang_register_operationrent;
                                     }
                                    echo '</option>';
                                }
                            }
                            ?>
                            </select>
                        </div>
                        
                        <label class="col-sm-2 control-label" for="subtotal">
                        <?php echo $_lang_operation_subtotal; ?>
                        </label>
                        <div class="col-sm-4">
                            <input    value="<?php echo floatval($this->quote->subtotal); ?>"    type="text"  id="subttotal"  name="subtotal" class="form-control">
                        </div> 
                        
                    </div>
  
                    <div class="form-group">
        
                        <label class="col-sm-2 control-label" for="discount">
                        <?php echo $_lang_operation_discount; ?>
                        </label>
                        <div class="col-sm-4">
                            <input    value="<?php echo floatval($this->quote->discount); ?>"    type="text"  id="discount"  name="discount" class="form-control">
                        </div> 
         
                        <label class="col-sm-2 control-label" for="shippingcost">
                        <?php echo $_lang_operation_shippingcost; ?>
                        </label>
                        <div class="col-sm-4">
                            <input    value="<?php echo floatval($this->quote->shippingcost); ?>"    type="text"  id="shippingcost"  name="shippingcost" class="form-control">
                        </div> 

                    </div> 
                     
                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="hastaxes">
                        <?php echo $_lang_register_label_hastaxes; ?>
                        </label>
                        <div class="col-sm-4"> 
                            <select name="hastaxes" id="hastaxes" class="form-control">
                            <?php
                            $total = count($this->aHasTaxes);
                            if ($total > 0) {
                                foreach ($this->aHasTaxes as $hastaxes) {
                                    $selected = '';
                                    if ($hastaxes == $this->quote->hastaxes) {
                                        $selected = ' selected = "true" ';
                                    }
                                    echo '<option ' . $selected . ' value="' . $hastaxes . '">' . $_lang_hastaxes[$hastaxes] . '</option>';
                                }
                            }
                            ?>
                            </select>
                        </div> 


                        <label class="col-sm-2 control-label" for="taxespercent">
                        <?php echo $_lang_register_label_taxespercent; ?>
                        </label>
                        <div class="col-sm-4">
                            <input    value="<?php echo floatval($this->quote->taxespercent); ?>"    type="text"  id="taxespercent"  name="taxespercent" class="form-control">
                        </div> 

                    </div>
   
                    <div class="form-group">

                        
                        <div class="col-sm-2 control-label">

                          <?php echo $_lang_currency_exchange; ?>

                        </div>
                        <div class="col-sm-2">
                            
                            <input type="hidden" name="currency_o" id="currency_o" value="<?php echo $this->quote->currency_code; ?>" />
                            <input type="hidden" name="currency_oex" id="currency_oex" value="<?php echo $this->quote->currency_exchange; ?>" />
                            
                            <select name="currency_code" id="currency_code" class="form-control">
                                <?php 
                                foreach($this->aCurrency as $currency){
                                    $selected  = '  ';
                                    if($this->quote->currency_code == $currency->code){
                                        $selected  = ' selected = "true" ';
                                    }
                                    
                                    echo '<option '.$selected.' value="'.$currency->code.'-_-'.$currency->xrate.'">'.$_lang_currency_code.':  '.$currency->code.'</option>';
                                }
                                ?>
                            </select>
                            
                        </div>
                        
                        <div class="col-sm-2">
                            <?php 
                            $_display = ' style="display:none;" ';
                            if($this->quote->currency_code !="USD"){
                                    $_display = '';                        
                            }
                            ?>
                            <input  <?php echo $_display; ?> type="text" placeholder="<?php echo $_lang_currency_exchange ;  ?>" class="form-control" id="currency_exchange" name="currency_exchange" value="<?php echo $this->quote->currency_exchange; ?>" />
                            
                        </div>                        
                        
                        
                        <label class="col-sm-2 control-label" for="total">
                        <?php echo $_lang_operation_total; ?>
                        </label>
                        <div class="col-sm-4">
                            <input    value="<?php echo floatval($this->quote->total); ?>"    type="text"  id="total"  name="total" class="form-control">
                        </div> 

                    </div>           
                    
                    
                    <div class="form-group">
                        <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                        </button>
                        </div>                        
                    </div>
                    
                </div>
                
                
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>    
     
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_productdetails; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
 
                    <?php 
                    $displayNone = ' style="display: none;"   ';
                    $subTotalColspan = 2;
                    if($this->quote->operationtype =="R"){
                        $displayNone = '   ';
                        $subTotalColspan = 3;
                    }
                    ?>
                    
                    
                    <table id="tableproducts" class="table table-full-width table-responsive table-striped">
                        <thead>
                            <th><?php echo $_lang_product_name; ?></th>
                            <th><?php echo $_lang_product_price; ?></th>
                            <th><?php echo $_lang_product_qty; ?></th>
                            <th <?php echo $displayNone; ?>  class="productMonths"><?php echo $_lang_product_months; ?></th>
                            <th><?php echo $_lang_product_subtotal; ?></th>
                            <th align="center"><a id="btnAddProduct" href="#" class="btn btn-xs btn-primary"><i class="fa fa-plus-circle">&nbsp;<?php echo $_lang_product_addproductbtn; ?></i></a></th>
                        </thead>
                        <tbody>
                            <?php 
                            if($this->aProduct){
                                $totalProduct = count($this->aProduct);
                                if($totalProduct > 0){
                                    $_operationtype = $this->quote->operationtype;
                                    foreach($this->aProduct as $item){
                                        $_rowid = date('YmdHis').''.rand(10000,99999).''.rand(10000,99999); 
                                        array_push($_aProductRowId, $_rowid);
                                        $_tr =  '<tr id="trProductId_'.$_rowid.'">';
                                        $_tr .= '<td valign="top" width="50%">';
                                        $_tr .= '<input type="input" value="'.$item->name.'" name="productname_' .$_rowid. '" id="productname_' .$_rowid . '" class="  ui-autocomplete-input form-control "  />';
                                        $_tr .= '<textarea class="form-control" name="productdesc_' .$_rowid .'"  id="productdesc_' .$_rowid . '" >'.$item->description.'</textarea>';
                                        $_tr .= '</td>';
                                        $_tr .= '<td style=" vertical-align:top" width="10%"><input type="text" value="'.$item->price.'" name="productprice_' .$_rowid .'" id="productprice_' .$_rowid . '" class="form-control txtProductPrice" /></td>';
                                        $_tr .= '<td style=" vertical-align:top"  width="10%"><input type="text" value="'.$item->qty.'" name="productqty_'.$_rowid .'" id="productqty_' .$_rowid . '" class="form-control txtProductQty" /></td>';
                                        $_tr .= '<td class="productMonths" id="tdProductMonths_' .$_rowid . '" style=" display:none; vertical-align:top"  width="10%"><input type="text" value="'.$item->months.'" name="productmonths_' .$_rowid . '" id="productmonths_' .$_rowid . '" class="form-control txtProductMonths" /></td>';
                                        $_tr .= '<td style=" vertical-align:top"  width="10%" align="center"><span id="productsubtotal_' .$_rowid . '" class="LblProductSubtotal">'.$item->subtotal.'</span></td>';
                                        $_tr .= '<td style=" vertical-align:top"  width="10%">';
                                        $_tr .= '<a href="#" id="btnProductDelete_' .$_rowid .  '" class="btn btn-xs btn-red btnProductDelete"><i class="fa fa-trash-o"></i></a>';
                                        $_tr .= '<input type="hidden" value="'.$item->idcrypt.'" name="productid_'  .$_rowid .  '"   id="productid_'  .$_rowid .  '"    />';
                                        $_tr .= '<input type="hidden"  name="productrowid[]"   id="productrow_'  .$_rowid .  '"  value="' .$_rowid . '"  />';

                                        $_tr .= '<input type="hidden"  name="operationtype_'  .$_rowid .  '"  id="operationtype_'  .$_rowid .  '" value="'.$_operationtype.'"    />';
                                        $_tr .= '</td>';
                                        $_tr .= '</tr>';

                                        echo $_tr;
                                    }

                                }
                            }
                            ?>
                            
                        </tbody>
                        <tfoot><tr><td id="tdProductSubtotalColspan" colspan="<?php echo $subTotalColspan; ?>" ></td><th><?php echo $_lang_product_subtotal; ?></th><td align="center" id="tdProductSubtotal">0</td><td></td></tr></tfoot>
                    </table>
                    
  
                </div>
                
                
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>    

    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_servicesdetails; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
 
                   
                    <table id="tableservices" class="table table-full-width table-responsive table-striped">
                        <thead>
                            <th><?php echo $_lang_service_name; ?></th>
                            <th><?php echo $_lang_service_price; ?></th>
                            <th><?php echo $_lang_service_activation; ?></th>
                            <th><?php echo $_lang_service_qty; ?></th>
                             <th><?php echo $_lang_product_subtotal; ?></th>
                            <th align="center"><a id="btnAddService" href="#" class="btn btn-xs btn-primary"><i class="fa fa-plus-circle">&nbsp;<?php echo $_lang_product_addservicebtn; ?></i></a></th>
                        </thead>
                        <tbody>
                            
                            <?php 
                            $totalService = count($this->aService);
                            if($this->aService){
                                if($totalService > 0){
                                foreach($this->aService as $item){
                                    $_rowid = date('YmdHis').''.rand(10000,99999).''.rand(10000,99999); 
                                    array_push($_aServiceRowId, $_rowid);
                                    
                                    $_tr  = '';
                                    $_tr =  '<tr id="trServiceId_'.$_rowid.'">';
                                    $_tr .= '<td style=" vertical-align:top"   width="50%">';
                                    $_tr .= '<input type="input" value="'.$item->name.'"  name="servicename_' .$_rowid. '" id="servicename_' .$_rowid. '" class="  ui-autocomplete-input form-control "  />';
                                    $_tr .= '<textarea class="form-control servicedesc" disabled="true" name="servicedesc_'.$_rowid.  '" id="servicedesc_' .$_rowid.  '" >'.$item->description.'</textarea>';
                                    $_tr .= '</td>';
                                    $_tr .= '<td style=" vertical-align:top"  ><input type="text" value="'.$item->price.'"  name="serviceprice_' .$_rowid.  '" id="serviceprice_' .$_rowid. '" class="form-control txtServicePrice" /></td>';
                                    $_tr .= '<td style=" vertical-align:top"  ><input type="text" value="'.$item->activationprice.'" name="serviceactivation_' .$_rowid. '" id="serviceactivation_' .$_rowid.  '" class="form-control txtServiceActivation" /></td>';
                                    $_tr .= '<td style=" vertical-align:top"  ><input type="text" value="'.$item->qty.'" name="serviceqty_' .$_rowid.  '" id="serviceqty_' .$_rowid.  '" class="form-control txtServiceQty" /></td>';
                                    $_tr .= '<td style=" vertical-align:top"  align="center"><span id="servicesubtotal_' .$_rowid. '" class="LblServiceSubtotal">'.$item->subtotal.'</span></td>';
                                    $_tr .= '<td style=" vertical-align:top"  >';
                                    $_tr .= '<a href="#" id="btnServiceDelete_'.$_rowid.  '" class="btn btn-xs btn-red btnServiceDelete"><i class="fa fa-trash-o"></i></a>';
                                    $_tr .= '<input type="hidden" name="serviceid_' .$_rowid.  '" id="serviceid_' .$_rowid.  '"   value="'.$item->idcrypt.'"  />';
                                    $_tr .= '<input type="hidden"  name="servicerowid[]"   id="servicerow_' .$_rowid.  '"  value="'.$_rowid. '"  />';
                                    $_tr .= '</td>';
                                    $_tr .= '</tr>';
                                    
                                    echo $_tr;
                                }
                                
                            }
                            }
                            ?>
                            
                        </tbody>
                        <tfoot><tr><td colspan="3" ></td><th><?php echo $_lang_product_subtotal; ?></th><td align="center" id="tdServiceSubtotal">0</td><td></td></tr></tfoot>
                    </table>
                    
  
                </div>
                
                
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>   
     

    <!--
    <div class="row">
        <div class="col-sm-12">
             <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_accesoriesdetails; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  
 
                   
                    <table id="tableaccessory" class="table table-full-width table-responsive table-striped">
                        <thead>
                            <th><?php echo $_lang_accessory_name; ?></th>
                            <th><?php echo $_lang_accessory_price; ?></th>
                            <th><?php echo $_lang_accessory_qty; ?></th>
                            <th><?php echo $_lang_accessory_months; ?></th>
                            <th><?php echo $_lang_accessory_subtotal; ?></th>
                            <th align="center"><a id="btnAddAccessory" href="#" class="btn btn-xs btn-primary"><i class="fa fa-plus-circle">&nbsp;<?php echo $_lang_accessory_addaccessorybtn; ?></i></a></th>
                        </thead>
                        <tbody></tbody>
                        <tfoot><tr><td colspan="3" ></td><th><?php echo $_lang_accessory_subtotal; ?></th><td align="center" id="tdAccessorySubtotal">0</td><td></td></tr></tfoot>
                    </table>
                    
  
                </div>
                
                
            </div>
         </div>
    </div>    
    -->
    
<?php 
$_totalProduct = count($_aProductRowId);
$_totalService = count($_aServiceRowId);

$_jsProductArray = 'var aProductDefault = [];  ';
if($_totalProduct > 0 ){
    foreach($_aProductRowId as $productRowId){
        $_jsProductArray .="  aProductDefault.push('".$productRowId."');  ";
    }
}

$_jsServiceArray = 'var aServiceDefault = [];  ';
if($_totalService > 0 ){
    foreach($_aServiceRowId as $serviceRowId){
        $_jsServiceArray .="  aServiceDefault.push('".$serviceRowId."');  ";
    }
}

echo '<script type="text/javascript">';
echo " var _operationtype = 'S' ; ";
if($this->quote->operationtype == 'R'){
    echo "   _operationtype = 'R' ; ";  
}
echo $_jsProductArray; 
echo $_jsServiceArray;
 
echo '</script>';