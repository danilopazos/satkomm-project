<!-- register update -->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-user  box-user-register" id="form-user"  >

                <div class="modal-body">

                    <div class="errorHandlerUser alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                   
                    <div id="messageUsernameProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_contactprocessok; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_view_block_contactblock; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="username">
                                            <?php echo $_lang_view_label_username; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userform_username" name="username" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="profile">
                                            <?php echo $_lang_view_label_profile; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <select name="profile" id="userform_profile" class="form-control">
                                                <?php
                                                foreach ($this->aProfile as $profile) {
                                                    if ($profile->id != "SKIU-ROOT-MG") {
                                                        echo '<option value="' . $profile->id . '">' . $profile->name . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div> 
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="firstname">
                                            <?php echo $_lang_view_label_firstname; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userform_firstname" name="firstname" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="lastname">
                                            <?php echo $_lang_view_label_lastname; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userform_lastname" name="lastname" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label" for="phone">
                                            <?php echo $_lang_view_label_phone; ?>
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text"  id="userform_phone" placeholder="<?php echo $_lang_register_label_phone_placeholder; ?>" name="phone" class="form-control">
                                        </div> 


                                        <label class="col-sm-2 control-label" for="phoneext">
                                            <?php echo $_lang_view_label_phoneext; ?>
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text"  id="userform_phoneext" placeholder="<?php echo $_lang_register_label_phoneext_placeholder; ?>"  name="phoneext" class="form-control">
                                        </div> 

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="mobile">
                                            <?php echo $_lang_view_label_mobile; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userform_mobile" name="mobile" class="form-control">
                                        </div>
                                    </div>

                                    <input type="hidden" name="id" id="userform_id"   />
                                    <input type="hidden" name="accountid" id="accountid" value="<?php echo $this->id; ?>"  />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_view_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- update user -->
<div class="modal fade" id="user-modalupdate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-userupdate  box-user-register" id="form-userupdate"  >

                <div class="modal-body">

                    <div class="errorHandlerUserUpdate alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div id="errorUsernameUpdateRepeated" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_usernamerepeated; ?>
                    </div>
                    <div id="errorUsernameUpdateProcessProblems" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
                    </div>
                    <div id="messageUsernameUpdateProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_userprocessok; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_view_block_userblock; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="profile">
                                            <?php echo $_lang_view_label_profile; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <select name="profile" id="userformupdate_profile" class="form-control">
                                                <?php
                                                foreach ($this->aProfile as $profile) {
                                                    if ($profile->id != "SKIU-ROOT-MG") {
                                                        echo '<option value="' . $profile->id . '">' . $profile->name . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div> 
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="firstname">
                                            <?php echo $_lang_view_label_firstname; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userformupdate_firstname" name="firstname" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="lastname">
                                            <?php echo $_lang_view_label_lastname; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userformupdate_lastname" name="lastname" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label" for="phone">
                                            <?php echo $_lang_view_label_phone; ?>
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text"  id="userformupdate_phone" placeholder="<?php echo $_lang_register_label_phone_placeholder; ?>" name="phone" class="form-control">
                                        </div> 


                                        <label class="col-sm-2 control-label" for="phoneext">
                                            <?php echo $_lang_view_label_phoneext; ?>
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text"  id="userformupdate_phoneext" placeholder="<?php echo $_lang_register_label_phoneext_placeholder; ?>"  name="phoneext" class="form-control">
                                        </div> 

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="mobile">
                                            <?php echo $_lang_view_label_mobile; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userformupdate_mobile" name="mobile" class="form-control">
                                        </div>
                                    </div>

                                    <input type="hidden" name="id" id="userformupdate_id"   />
                                    <input type="hidden" name="accountid" id="accountid" value="<?php echo $this->id; ?>"  />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_view_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div id="panel_contacts" class="tab-pane">

    <div class="page-header">  
           <!--<h1>Accounts <small>list</small></h1>-->
        <a class="btn btn-xs btn-primary  contact-addmodal   " data-toggle="modal"    href="#contact-modal">
            <i class="fa fa-plus-circle"></i> <?php echo $_lang_view_link_addcontact; ?>
        </a>
    </div>


    <table class="table table-striped table-bordered table-hover" id="table_users">
        <thead>
            <tr>
                <th><?php echo $_lang_view_table_email; ?></th> 
                <th><?php echo $_lang_view_table_firstname; ?></th>
                <th class="hidden-xs  "><?php echo $_lang_view_table_lastname; ?></th>
                <th class="hidden-xs  "><?php echo $_lang_view_table_title; ?></th>
                <th class="hidden-xs  "><?php echo $_lang_view_table_department; ?></th>
                <th class="hidden-xs  "><?php echo $_lang_view_table_mobile; ?></th>
                <th class="hidden-xs  "><?php echo $_lang_view_table_phone; ?></th>
                <th> </th>
            </tr>
        </thead>
        <tbody>
            <tr>

                <td>jorge.tellez@buenaventura.com</td>
                <td>Jorge</td> 
                <td class="hidden-xs">Tellez</td>
                <td class="hidden-xs">Gerente General</td> 
                <td class="hidden-xs">Gerencia</td>
                <td class="hidden-xs">+51 997481999</td> 
                <td>+51 017481999</td>
                <td> <a href="#"    class="btn btn-xs btn-default  user-viewmodal  "  ><i class="fa fa-pencil"></i></a> </td>
            </tr>
            <tr>

                <td>juan.sotomayor@buenaventura.com</td>
                <td>Juan</td> 
                <td class="hidden-xs">Sotomayor</td>
                <td class="hidden-xs">Contabilidad</td> 
                <td class="hidden-xs">Finanzas</td>
                <td class="hidden-xs">+51 997483499</td> 
                <td>+51 017482399</td>
                <td> <a href="#"    class="btn btn-xs btn-default  user-viewmodal  "  ><i class="fa fa-pencil"></i></a> </td>
            </tr>
            <tr>

                <td>rafael.perez@buenaventura.com</td>
                <td>Rafael</td> 
                <td class="hidden-xs">Perez</td>
                <td class="hidden-xs">Gerente de Operaciones</td> 
                <td class="hidden-xs">Producci&oacute;n</td>
                <td class="hidden-xs">+51 997483449</td> 
                <td>+51 017482399</td>
                <td> <a href="#"    class="btn btn-xs btn-default  user-viewmodal  "  ><i class="fa fa-pencil"></i></a> </td>
            </tr>      
            
        </tbody>
    </table>
</div>