<div id="panel_overview" class="tab-pane in active">
    <div class="row">
        <div class="col-md-8">
            <div class="user-left">
 
                <table class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th colspan="3"><?php echo $_lang_register_block_accountinfo; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td width="25%"><?php echo $_lang_register_label_code; ?></td>
                            <td>
                                <?php echo $this->account->code; ?>
                            </td> 
                        </tr>
                        
                        <tr>
                            <td><?php echo $_lang_register_label_taxid; ?></td>
                            <td>
                               <?php 
                               if($this->account->taxtypeid != "" &&    $this->account->taxid != ""  ){
                                   echo $this->account->taxtypeid.'&nbsp;' ;
                               }
                               if($this->account->taxid){
                                   echo $this->account->taxid;
                               }else{
                                   echo '---';
                               }
                               
                               ?>
                            </td> 
                        </tr>
                    
                        <tr>
                            <td><?php echo $_lang_register_label_name; ?></td>
                            <td>
                               <?php 
                               
                                if($this->account->name){
                                    echo $this->account->name;
                                }else{
                                    echo '---';
                                }
                               
                               ?>
                            </td> 
                        </tr>

                        <tr>
                            <td><?php echo "Moneda de la cuenta"; ?></td>
                            <td>
                               <?php 
                               
                                if($this->account->name){
                                    echo $this->account->defaultcurrency." - ".$this->account->descriptioncurrency;
                                }
                               
                               ?>
                            </td> 
                        </tr>
                        
                        <?php 
                        if(strlen($this->account->email)>0){ ?>
                            <tr>
                                <td><?php echo $_lang_register_label_email; ?></td>
                                <td>
                                    <?php echo $this->account->email; ?>
                                </td> 
                            </tr>
                            <?php 
                        } 
                        
                        if(strlen($this->account->subdomain)>0){ ?>
                            <tr>
                                <td><?php echo $_lang_register_label_subdomain; ?></td>
                                <td>
                                    
                                    <?php echo $this->account->subdomain ; ?>
                                    
                                </td> 
                            </tr>
                        <?php 
                        } 

                        if(strlen($this->account->website)>0){ ?>
                            <tr>
                                <td><?php echo $_lang_register_label_website; ?></td>
                                <td>
                                    
                                    <?php echo '<a href="'.$this->account->website.'" target="_blank">'.$this->account->website.'</a>'; ?>
                                    
                                </td> 
                            </tr>
                            <?php 
                        }

                        if(strlen($this->account->phone)>0){ ?>
                            <tr>
                                <td><?php echo $_lang_register_label_phone; ?></td>
                                <td>
                                    <?php echo $this->account->phone.'&nbsp; '.$_lang_register_label_phoneext.': '.$this->account->phoneext; ?>
                                </td> 
                            </tr>
                            <?php 
                        } ?>
                        
                        <tr>
                            <td><?php echo $_lang_label_status; ?></td>
                            <td>
                                <?php 
                                if($this->account->status  == 1){
                                     echo '<span class="label label-sm label-info">'.$_lang_status_active.'</span>';
                                }else{
                                      echo '<span class="label label-sm label-danger">'.$_lang_status_inactive.'</span>';
                                }
                                
                                ?>
                            </td> 
                        </tr>
                        
                        
                        
                    </tbody>
                </table>
                
                <table class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th colspan="3"><?php echo $_lang_register_block_billaddress; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td><?php echo $_lang_register_label_country; ?></td>
                            <td><?php echo $this->account->billaddress->country; ?></td> 
                        </tr>
                       
                        <tr>
                            <td><?php echo $_lang_register_label_state; ?></td>
                            <td><?php echo $this->account->billaddress->state; ?></td> 
                        </tr>
                        
                        <tr>
                            <td><?php echo $_lang_register_label_city; ?></td>
                            <td><?php echo $this->account->billaddress->city; ?></td> 
                        </tr>
                        
                        <tr>
                            <td><?php echo $_lang_register_label_street; ?></td>
                            <td><?php echo $this->account->billaddress->street; ?></td> 
                        </tr>
                        
                        <tr>
                            <td><?php echo $_lang_register_label_zipcode; ?></td>
                            <td><?php echo $this->account->billaddress->zipcode; ?></td> 
                        </tr>
                        
                        <?php if($this->account->billaddress->pobox != ""){ ?>
                        <tr>
                            <td><?php echo $_lang_register_label_pobox; ?></td>
                            <td><?php echo $this->account->billaddress->pobox; ?></td> 
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
                
            
                 <table class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th colspan="3"><?php echo $_lang_register_block_shipaddress; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td><?php echo $_lang_register_label_country; ?></td>
                            <td><?php echo $this->account->shipaddress->country; ?></td> 
                        </tr>
                       
                        <tr>
                            <td><?php echo $_lang_register_label_state; ?></td>
                            <td><?php echo $this->account->shipaddress->state; ?></td> 
                        </tr>
                        
                        <tr>
                            <td><?php echo $_lang_register_label_city; ?></td>
                            <td><?php echo $this->account->shipaddress->city; ?></td> 
                        </tr>
                        
                        <tr>
                            <td><?php echo $_lang_register_label_street; ?></td>
                            <td><?php echo $this->account->shipaddress->street; ?></td> 
                        </tr>
                        
                        <tr>
                            <td><?php echo $_lang_register_label_zipcode; ?></td>
                            <td><?php echo $this->account->shipaddress->zipcode; ?></td> 
                        </tr>
                        
                        <?php if($this->account->shipaddress->pobox != ""){ ?>
                        <tr>
                            <td><?php echo $_lang_register_label_pobox; ?></td>
                            <td><?php echo $this->account->shipaddress->pobox; ?></td> 
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
                
                
            </div>
        </div>
        <div class="col-sm-7 col-md-8">
           
            <?php  //require_once 'view_dashboard.php';   ?>
            
            <?php  //require_once 'view_recentactivities.php';   ?>
            
        </div>
    </div>
</div>