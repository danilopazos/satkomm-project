 <div class="row">
                <div class="col-sm-3">
                    <button class="btn btn-icon btn-block">
                        <i class="clip-clip"></i>
                        Projects <span class="badge badge-info"> 4 </span>
                    </button>
                </div>
                <div class="col-sm-3">
                    <button class="btn btn-icon btn-block pulsate">
                        <i class="clip-bubble-2"></i>
                        Messages <span class="badge badge-info"> 23 </span>
                    </button>
                </div>
                <div class="col-sm-3">
                    <button class="btn btn-icon btn-block">
                        <i class="clip-calendar"></i>
                        Calendar <span class="badge badge-info"> 5 </span>
                    </button>
                </div>
                <div class="col-sm-3">
                    <button class="btn btn-icon btn-block">
                        <i class="clip-list-3"></i>
                        Notifications <span class="badge badge-info"> 9 </span>
                    </button>
                </div>
            </div>
            