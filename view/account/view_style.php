

<div id="panel_style" class="tab-pane">






    <div class="row">
        <div class="col-sm-6">
            <!-- start: FILE UPLOAD PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_updatestyle; ?>
                    <div class="panel-tools">
               
                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>
                      
                    </div>
                </div>
                <div class="panel-body">


                    <form class="form-horizontal" method="post" action="/account/requestupdatestyle"  >
                        
                        <div class="form-group">
                            <div class="col-sm-12">
                                
                            <div class="panel-body  form-horizontal">  


                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="code">
                                        <?php echo $_lang_register_label_stylecolor; ?>
                                    </label>
                                    <div class="col-sm-6">
                                         <?php 
                                        
                                         ?>
                                        <input type="hidden" name="accountid" id="accountid" value="<?php echo $this->account->id; ?>" />
                                        <select class="form-control" name="stylecolor" id="stylecolor">
                                            <option value=""><?php echo $_lang_register_updatestyle_pleaseselect; ?></option>
                                            <?php 
                                            foreach($this->aStyles as $record){
                                                $selected = '';
                                                if(  trim($this->account->stylecolor) == trim($record["value"]) ){
                                                    $selected = ' selected = "true"  ';
                                                }else{
                                                    $selected = '';
                                                }
                                                echo '<option '.$selected.' value="'.$record["value"].'">'.$record["label"].'</option>';
                                                
                                            }
                                            ?>
                                        </select>
                                    </div>
                                  
                                </div>

                            </div>
                                
                                
                                
                            </div>
                             
                        </div>


                        <div class="row">
                            <div class="col-sm-12">


                                <div class="panel panel-default">
                                    <div class="panel-footer">

                                        <button type="submit" class="btn btn-primary ">
                                            <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_updatestyle; ?> 
                                        </button>

                                    </div>
                                </div>


                            </div>
                        </div>

                    </form>



                </div>



            </div>
            <!-- end: FILE UPLOAD PANEL -->
        </div>
    </div>


</div>