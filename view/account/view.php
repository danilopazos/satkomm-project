
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>

        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            if ($this->session_accountid != $this->id) {
                echo '<a class="btn btn-xs btn-default" href="/account/index">';
                echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li class="active">
                    <a data-toggle="tab" href="#panel_overview">
                        <?php echo $_lang_register_label_panel_overview; ?>
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#panel_users">
                        <?php echo $_lang_register_label_panel_users; ?>
                    </a>
                </li>

                <!--
                <li>
                    <a data-toggle="tab" href="#panel_creditcards">
                <?php echo $_lang_register_label_panel_creditcards; ?>
                    </a>
                </li>
                -->

                <?php
                $showRegistedTab = true;
                if ($this->session_profileid != 'SKIU-ROOT-MG' && $this->session_accountid == $this->id) {
                    $showRegistedTab = false;
                }
                $showLogoTab = false;
                if ($this->session_profileid == 'SKIU-ROOT-MG' || $this->session_profileid == 'SKIU-AC-MG') {
                    $showLogoTab = true;
                }
                ?>


                <?php if ($showRegistedTab) { ?>
                    <li>
                        <a  href="/account/register/id/<?php echo $this->id; ?>">
                            <?php echo $_lang_register_label_panel_editaccount; ?>
                        </a>
                    </li>
                <?php } ?>


                <?php if ($showLogoTab) { ?>
                    <li>
                        <a data-toggle="tab" href="#panel_logo">
                            <?php echo $_lang_register_label_panel_logo; ?>
                        </a>
                    </li>      
                    

                    
                <?php } 
                if( $this->id == $this->session_accountid  ){
                ?>    
                                        
                    <li>
                        <a data-toggle="tab" href="#panel_style">
                            <?php echo $_lang_register_label_panel_stylecolor; ?>
                        </a>
                    </li> 
                    
                <?php    
                }
                
                ?>

                

                <!--
                <li>
                    <a data-toggle="tab" href="#panel_contacts">
                        <?php echo $_lang_register_label_panel_contacts; ?>
                    </a>
                </li> 
                -->
            </ul>


            <div class="tab-content">
                <!-- Panel Overview -->
                <?php require 'view_overview.php'; ?>

                <!-- Panel Users -->
                <?php require 'view_users.php'; ?>

                <!-- Panel Creditcards -->
                <?php
                if ($showLogoTab) {
                    require 'view_logo.php';
                }
                ?>

                <?php 
                if( $this->id == $this->session_accountid  ){
                    require 'view_style.php'; 
                }
                ?>


                <?php //require 'view_contacts.php'; ?>


            </div>

        </div>
    </div>
</div>
<!-- end: PAGE CONTENT-->
