<script type="text/javascript">
    var _accountid = <?php echo $this->id; ?>;
</script>


<div id="panel_logo" class="tab-pane">






    <div class="row">
        <div class="col-sm-12">
            <!-- start: FILE UPLOAD PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_updatelogo; ?>
                    <div class="panel-tools">
               
                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>
                      
                    </div>
                </div>
                <div class="panel-body">


                    <form class="form-horizontal" action="/account/requestupdatemylogo" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>
                                    <img src="/media/mylogo"  height="50em"  />
                                </label>
                                <input id="input-simple" type="file" class="file" data-show-preview="false"  name="mediaimage_file" id="fileToUpload">
                                <p><br />
                                    <?php echo $_lang_register_logo_info; ?>
                                </p>
                            </div>
                             
                        </div>


                        <div class="row">
                            <div class="col-sm-12">


                                <div class="panel panel-default">
                                    <div class="panel-footer">

                                        <button type="submit" class="btn btn-primary ">
                                            <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_updatelogo; ?> 
                                        </button>

                                    </div>
                                </div>


                            </div>
                        </div>



                    </form>



                </div>



            </div>
            <!-- end: FILE UPLOAD PANEL -->
        </div>
    </div>


</div>