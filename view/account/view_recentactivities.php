 
            
            
            <div class="panel panel-white">
                <div class="panel-heading">
                    <i class="clip-menu"></i> Recent Activities
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                        </a>
                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-refresh" href="#">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body panel-scroll" style="height:300px">
                    <ul class="activities">
                        <li>
                            <a class="activity" href="javascript:void(0)">
                                <i class="clip-upload-2 circle-icon circle-green"></i>
                                <span class="desc">You uploaded a new release.</span>
                                <div class="time">
                                    <i class="fa fa-time bigger-110"></i> 2 hours ago
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="activity" href="javascript:void(0)">
                                <img alt="image" src="assets/images/avatar-2.jpg">
                                <span class="desc">Nicole Bell sent you a message.</span>
                                <div class="time">
                                    <i class="fa fa-time bigger-110"></i> 3 hours ago
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="activity" href="javascript:void(0)">
                                <i class="clip-data circle-icon circle-bricky"></i>
                                <span class="desc">DataBase Migration.</span>
                                <div class="time">
                                    <i class="fa fa-time bigger-110"></i> 5 hours ago
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="activity" href="javascript:void(0)">
                                <i class="clip-clock circle-icon circle-teal"></i>
                                <span class="desc">You added a new event to the calendar.</span>
                                <div class="time">
                                    <i class="fa fa-time bigger-110"></i> 8 hours ago
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="activity" href="javascript:void(0)">
                                <i class="clip-images-2 circle-icon circle-green"></i>
                                <span class="desc">Kenneth Ross uploaded new images.</span>
                                <div class="time">
                                    <i class="fa fa-time bigger-110"></i> 9 hours ago
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="activity" href="javascript:void(0)">
                                <i class="clip-image circle-icon circle-green"></i>
                                <span class="desc">Peter Clark uploaded a new image.</span>
                                <div class="time">
                                    <i class="fa fa-time bigger-110"></i> 12 hours ago
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div> 