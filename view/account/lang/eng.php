<?php
//--- View Index
$_lang_index_label_module = 'Accounts';
$_lang_index_label_action = 'List'; 
$_lang_index_link_addaccount = 'Add Account'; 
$_lang_register_label_panel_logo = "Logo";
        
$_lang_index_table_title = 'Accounts'; 
$_lang_index_table_th_name = 'Name'; 
$_lang_index_table_th_code = 'Code'; 
$_lang_index_table_th_taxid = 'Tax ID'; 
$_lang_index_table_th_country = 'Country'; 
$_lang_index_table_th_city = 'City'; 


$_lang_profile_SKIUCUAR = "Account Representative";
$_lang_profile_SKIUCURL = "Legal Representative";
$_lang_profile_SKIUCUAL = "Sub-Representative";



//--- View Register
$_lang_register_block_accountinfo = "Account info";
$_lang_register_goback = "Go back";
$_lang_register_save = "Save record";
$_lang_register_saveandnew = "Save and new";

$_lang_register_label_formerror  ="You have some form errors. Please check below.";
$_lang_register_label_coderepeated  ="Other account have the same code, please change it.";
$_lang_register_label_processproblems  ="The requested operation present problems, please try later";
$_lang_register_label_processok  ="Success, the account was registered";

$_lang_register_label_code = "Code";
$_lang_register_label_code_help ="Introduce a unique code";
$_lang_register_label_requiredfield_help = "This field is required";
$_lang_register_label_taxtype = "Tax Record";
$_lang_register_label_taxtype_placeholder = "Local Tax Record";
$_lang_register_label_taxid = "Tax ID";
$_lang_register_label_taxid_placeholder = "Local Tax ID number";
$_lang_register_label_name = "Name";
$_lang_register_label_email = "Email";
$_lang_register_label_website = "Website";
$_lang_register_label_phone = "Phone";
$_lang_register_label_phone_placeholder = "Example: +1 877 954 6985";
$_lang_register_label_phoneext = "Phone Ext";
$_lang_register_label_phoneext_placeholder = "Phone extension";

$_lang_register_block_billaddress = "Billing Address";
$_lang_register_block_shipaddress = "Shipping Address";
$_lang_register_label_country = "Country";
$_lang_register_label_state = "State";
$_lang_register_label_city = "City";
$_lang_register_label_street = "Street";
$_lang_register_label_zipcode  = "Zip Code";
$_lang_register_label_pobox = "P.O. Box";

//-- Detail View
$_lang_register_label_panel_overview = "Overview";
$_lang_register_label_panel_users = "Users";
$_lang_register_label_panel_creditcards = "Credit Cards";
$_lang_register_label_panel_editaccount = "Edit Account";
$_lang_register_label_panel_stylecolor = "Style";

$_lang_view_link_addauser = "Add User";

$_lang_view_label_username = "Email";
$_lang_view_label_profile = "Profile";
$_lang_view_label_password = "Password";
$_lang_view_label_password_confirm = "Password Confirmation";
$_lang_view_label_firstname = "Firstname";
$_lang_view_label_lastname = "Lastname";
$_lang_view_label_phone = "Phone";
$_lang_view_label_phoneext = "Phone Ext";
$_lang_view_label_mobile = "Mobile";
$_lang_view_label_save = "Save";
$_lang_view_label_close = "Close";
$_lang_view_block_userblock = "User Info";
$_lang_view_block_usermodal = "User";
$_lang_view_block_contactblock = "Contact Info";


$_lang_view_table_status = "Status";
$_lang_view_table_username = "Username";
$_lang_view_table_profile = "Profile";
$_lang_view_table_firstname = "Firstname";
$_lang_view_table_lastname = "Lastname";
$_lang_view_table_mobile = "Mobile";
$_lang_view_table_phone = "Phone";



$_lang_view_label_usernamerepeated = "This email was used for other user, please use other";
$_lang_view_label_userprocessok = "User registered";
$_lang_view_label_userpasswordchanged = "Password Changed";
$_lang_view_label_usernamechanged = "Email Changed";
$_lang_view_label_statuschanged = "Status Changed";
    
$_lang_register_block_changepassword = "Change Password";
$_lang_register_block_changeemail = "Change Email";
$_lang_register_block_changestatus = "Change Status";

$_lang_register_updatelogo = "Update Logo";
$_lang_register_block_updatelogo = "Update account logo";

$_lang_register_logo_info = "Image features. filetype: png / jpg, sizes: 135px x 50px, weight max: 100Kb";

$_lang_register_label_subdomain_repeated = "The subdomain introduce is used for other account, please introduce other subdomain";
$_lang_register_label_subdomain = "Subdomain";

$_lang_register_label_stylecolor = "Style";
$_lang_register_updatestyle = "Update Style";
$_lang_register_block_updatestyle = "Update Account Style";
$_lang_register_updatestyle_pleaseselect = "Please select one";

$_lang_register_label_asignedto = "Asigned to";
$_lang_register_label_asignedto_select = "Please select an user";


$_lang_register_label_panel_contacts = "Contacts";
$_lang_view_link_addcontact = "Add Contact"; 
$_lang_view_table_title = "Title";
$_lang_view_table_department = "Department";
$_lang_view_label_contactprocessok = "Contact registered";

$_lang_view_label_email = "Email";
$_lang_view_label_title = "Title";
$_lang_view_label_salutation = "Salutation";
$_lang_view_label_department = "Department";
$_lang_view_label_contactpriority = "Contact priority";
$_lang_view_label_phoneec = "Country Code";
$_lang_view_label_mobileec = "Country Code";
$_lang_view_label_ownerid = "Assigned to";
$_lang_view_label_leadsource = "Lead source";
$_lang_view_label_dateofbirth = "Date of Birth";
$_lang_view_label_others = "Others";
$_lang_view_label_description = "Description";


$_lang_view_label_position = "Position";

 
$_lang_register_block_accountcredit = "Account credit";
$_lang_register_label_creditlimit = "Credit limit";
$_lang_register_label_invoicedue = "Invoice due (in days)"; 

$_lang_register_block_accountconfig = "Account config";
$_lang_register_label_conf_quotevaliddays = "Quotes valid (days)";
$_lang_register_label_conf_invoicedue = "Invoice due (days)";
$_lang_register_label_conf_taxespercent = "Taxes percent";

$_lang_register_label_invoiceprefix = "Invoice prefix";
$_lang_register_label_quoteprefix = "Quote prefix";

$_lang_register_label_activatecustomplans = "Custom plans";
$_lang_register_label_activatecustomplans_yes = "Activated";
$_lang_register_label_activatecustomplans_no = "Deactivated";