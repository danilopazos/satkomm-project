<?php
//--- View Index
$_lang_index_label_module = 'Cuentas';
$_lang_index_label_action = 'Lista'; 
$_lang_index_link_addaccount = 'Agregar cuenta'; 
$_lang_register_label_panel_logo = "Logo";
        
$_lang_index_table_title = 'Cuentas'; 
$_lang_index_table_th_name = 'Nombre'; 
$_lang_index_table_th_code = 'C&oacute;digo'; 
$_lang_index_table_th_taxid = 'C&oacute;digo de impuestos'; 
$_lang_index_table_th_country = 'Pais'; 
$_lang_index_table_th_city = 'Ciudad'; 


$_lang_profile_SKIUCUAR = "Representante de la Cuenta";
$_lang_profile_SKIUCURL = "Representante Legal";
$_lang_profile_SKIUCUAL = "Representante Alterno";


//--- View Register
$_lang_register_block_accountinfo = "Informaci&oacute;n de la Cuenta";
$_lang_register_goback = "Volver";
$_lang_register_save = "Guardar Registo";
$_lang_register_saveandnew = "Guardar y Nuevo";

$_lang_register_label_formerror  ="Usted presenta errores en el formuario. Por favor verifique.";
$_lang_register_label_coderepeated  ="Otra cuenta tiene el mismo c&oacute;digo, por favor rev&iacute;selo.";
$_lang_register_label_processproblems  ="La operaci&oacute;n requerida presenta problemas, por favor intente nuevamente";
$_lang_register_label_processok  ="Exito!, la cuenta fue registrada";

$_lang_register_label_code = "C&oacute;digo";
$_lang_register_label_code_help ="Introduzca un nuevo c&oacute;digo";
$_lang_register_label_requiredfield_help = "Este campo es requerido";
$_lang_register_label_taxtype = "Registro Tributario";
$_lang_register_label_taxtype_placeholder = "Registro Tributario local";
$_lang_register_label_taxid = "ID del Impuesto";
$_lang_register_label_taxid_placeholder = "N&uacute;mero de Impuesto Local";
$_lang_register_label_name = "Nombre";
$_lang_register_label_email = "Correo";
$_lang_register_label_website = "Sitio web";
$_lang_register_label_phone = "Tel&eacute;fono";
$_lang_register_label_phone_placeholder = "Ejemplo: +1 877 954 6985";
$_lang_register_label_phoneext = "Anexo";
$_lang_register_label_phoneext_placeholder = "Extensi&oacute;n telef&oacute;nica";

$_lang_register_block_billaddress = "Direcci&oacute;n de Facturaci&oacute;n";
$_lang_register_block_shipaddress = "Direcci&oacute;n de Env&iacute;o";
$_lang_register_label_country = "Pais";
$_lang_register_label_state = "Estado/Region";
$_lang_register_label_city = "Ciudad";
$_lang_register_label_street = "Calle";
$_lang_register_label_zipcode  = "Direcci&oacute;n Postal";
$_lang_register_label_pobox = "P.O. Box";

//-- Detail View
$_lang_register_label_panel_overview = "Resumen";
$_lang_register_label_panel_users = "Usuarios";
$_lang_register_label_panel_creditcards = "Tarjetas de c&eacute;dito";
$_lang_register_label_panel_editaccount = "Editar Cuenta";
$_lang_register_label_panel_stylecolor = "Estilo";

$_lang_view_link_addauser = "A&ntilde;adir Usuario";

$_lang_view_label_username = "Correo";
$_lang_view_label_profile = "Perfil";
$_lang_view_label_password = "Contrase&ntilde;a";
$_lang_view_label_password_confirm = "Confirmaci&oacute;n de Contrase&ntilde;a";
$_lang_view_label_firstname = "Nombres";
$_lang_view_label_lastname = "Apellidos";
$_lang_view_label_phone = "Tel&eacute;fono";
$_lang_view_label_phoneext = "Anexo Telef&oacute;nico";
$_lang_view_label_mobile = "M&oacute;vil";
$_lang_view_label_save = "Guardar";
$_lang_view_label_close = "Cerrar";
$_lang_view_block_userblock = "Informaci&oacute;n del Usuario";
$_lang_view_block_usermodal = "Usuario";
$_lang_view_block_contactblock = "Informaci&oacute;n del Contacto";


$_lang_view_table_status = "Estado";
$_lang_view_table_username = "Nombre de Usuario";
$_lang_view_table_profile = "Perfil";
$_lang_view_table_firstname = "Nombres";
$_lang_view_table_lastname = "Apellidos";
$_lang_view_table_mobile = "M&oacute;vil";
$_lang_view_table_phone = "Tel&eacute;fono";



$_lang_view_label_usernamerepeated = "Este correo electr&oacute;nico ha sido usado por otro usuario, por favor ingrese otro";
$_lang_view_label_userprocessok = "Usuario registrado";
$_lang_view_label_userpasswordchanged = "Contrase&ntilde;a actualizado";
$_lang_view_label_usernamechanged = "Correo actualizado";
$_lang_view_label_statuschanged = "Estado cambiado";
    
$_lang_register_block_changepassword = "Cambiar Contrase&ntilde;a";
$_lang_register_block_changeemail = "Cambiar Correo";
$_lang_register_block_changestatus = "Cambiar estado";

$_lang_register_updatelogo = "Actualizar Logo";
$_lang_register_block_updatelogo = "Actualizar el logo de la cuenta";

$_lang_register_logo_info = "Caracter&iacute;sticas del archivo. Extensi&oacute;n: png / jpg, tama&ntilde;o: 135px x 50px, peso m&aacute;ximo: 100Kb";

$_lang_register_label_subdomain_repeated = "El subdominio ingresado pertenece a otra cuenta, por favor ingrese otro subdominio";
$_lang_register_label_subdomain = "Sub-dominio";

$_lang_register_label_stylecolor = "Estilo";
$_lang_register_updatestyle = "Actualizar Estilo";
$_lang_register_block_updatestyle = "Actualizar estilo de la cuenta";
$_lang_register_updatestyle_pleaseselect = "Por favor escoja";

$_lang_register_label_asignedto = "Asignado a";
$_lang_register_label_asignedto_select = "Por favor seleccione un usuario";

$_lang_register_label_panel_contacts = "Contactos";
$_lang_view_link_addcontact = "A&ntilde;adir contacto"; 
$_lang_view_table_title = "T&iacute;tulo";
$_lang_view_table_department = "Divisi&oacute;n";
$_lang_view_label_contactprocessok = "Contacto registrado";

$_lang_view_label_email = "Correo Electr&oacute;nico";
$_lang_view_label_title = "T&iacute;tulo";
$_lang_view_label_salutation = "Saludo";
$_lang_view_label_department = "Divisi&oacute;n";
$_lang_view_label_contactpriority = "Prioridad del contacto";
$_lang_view_label_phoneec = "C&oacute;digo de Pa&iacute;s";
$_lang_view_label_mobileec = "C&oacute;digo de Pa&iacute;s";
$_lang_view_label_ownerid = "Asigando a";
$_lang_view_label_leadsource = "Fuente";
$_lang_view_label_dateofbirth = "Fecha de nacimiento";
$_lang_view_label_others = "Otros";
$_lang_view_label_description = "Descripci&oacute;n";

$_lang_view_label_position = "Cargo";

$_lang_register_block_accountcredit = "Cr&eacute;dito de la cuenta";
$_lang_register_label_creditlimit = "L&iacute;mite de cr&eacute;dito en USD";
$_lang_register_label_invoicedue = "Vencimiento de facturas (dias)"; 

$_lang_register_block_accountconfig = "Configuraci&oacute;n de la cuenta";
$_lang_register_label_conf_quotevaliddays = "Validez de cotizaciones (dias)";
$_lang_register_label_conf_invoicedue = "Vencimiento de facturas (dias)";
$_lang_register_label_conf_taxespercent = "% del impuesto";


$_lang_register_label_invoiceprefix = "Factura prefijo";
$_lang_register_label_quoteprefix = "Cotizaci&oacute;n prefijo";


$_lang_register_label_activatecustomplans = "Planes personalizados";
$_lang_register_label_activatecustomplans_yes = "Activo";
$_lang_register_label_activatecustomplans_no = "Desactivo";