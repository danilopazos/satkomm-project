<script type="text/javascript">
    var _accountid = <?php echo $this->id; ?>;
</script>


<!-- register update -->
<div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-user  box-user-register" id="form-user"  >

                <div class="modal-body">

                    <div class="errorHandlerUser alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div id="errorUsernameRepeated" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_usernamerepeated; ?>
                    </div>
                    <div id="errorUsernameProcessProblems" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
                    </div>
                    <div id="messageUsernameProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_userprocessok; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_view_block_userblock; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="username">
                                            <?php echo $_lang_view_label_username; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userform_username" name="username" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="profile">
                                            <?php echo $_lang_view_label_profile; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <select name="profile" id="userform_profile" class="form-control">
                                                <?php
                                             
                                                
                                                foreach ($this->aProfile as $profile) {
                                                    if ($profile->id != "SKIU-ROOT-MG") {
                                                        
                                                        $profileValue = $profile->name;
                                                        if($profile->id =='SKIUCUAR'){ $profileValue = $_lang_profile_SKIUCUAR;  } 
                                                        if($profile->id =='SKIUCURL'){ $profileValue = $_lang_profile_SKIUCURL;  } 
                                                        if($profile->id =='SKIUCUAL'){ $profileValue = $_lang_profile_SKIUCUAL;  } 
                                                        
                                                        
                                                        echo '<option value="' . $profile->id . '">' . $profileValue . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div> 
                                    </div>

                                    
                                    <?php if(! $this->iscustomer){ ?>
                                    
                                    
                                     
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="username">
<?php echo $_lang_view_label_password; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="password"  id="userform_password" name="password" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="userform_password_confirm">
<?php echo $_lang_view_label_password_confirm; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="password"  id="userform_password_confirm" name="password_confirm" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>
                                    <?php } ?>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="firstname">
<?php echo $_lang_view_label_firstname; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userform_firstname" name="firstname" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="lastname">
<?php echo $_lang_view_label_lastname; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userform_lastname" name="lastname" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label" for="phone">
<?php echo $_lang_view_label_phone; ?>
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text"  id="userform_phone" placeholder="<?php echo $_lang_register_label_phone_placeholder; ?>" name="phone" class="form-control">
                                        </div> 


                                        <label class="col-sm-2 control-label" for="phoneext">
<?php echo $_lang_view_label_phoneext; ?>
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text"  id="userform_phoneext" placeholder="<?php echo $_lang_register_label_phoneext_placeholder; ?>"  name="phoneext" class="form-control">
                                        </div> 

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="mobile">
<?php echo $_lang_view_label_mobile; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userform_mobile" name="mobile" class="form-control">
                                        </div>
                                    </div>

                                    <input type="hidden" name="id" id="userform_id"   />
                                    <input type="hidden" name="accountid" id="accountid" value="<?php echo $this->id; ?>"  />

                                    <?php 
                                    if($this->iscustomer){
                                    ?>
                                        
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="position">
                                            <?php echo $_lang_view_label_position; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="position" name="position" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>  
                                      
                                    
                                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="user_country">
                            <?php echo $_lang_register_label_country; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value=""     type="text"  id="user_country" name="user_country" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="user_state">
                            <?php echo $_lang_register_label_state; ?>
                        </label>
                        <div class="col-sm-5">
                            <input   value=""    type="text"  id="user_state" name="user_state" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="user_city">
                            <?php echo $_lang_register_label_city; ?>
                        </label>
                        <div class="col-sm-5">
                            <input     value=""      type="text"  id="user_city" name="user_city" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="user_street">
                            <?php echo $_lang_register_label_street; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value=""       type="text"  id="user_street" name="user_street" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="user_zipcode">
                            <?php echo $_lang_register_label_zipcode; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value=""    type="text"  id="user_zipcode" name="user_zipcode" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="user_pobox">
                            <?php echo $_lang_register_label_pobox; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value=""    type="text"  id="user_pobox" name="user_pobox" class="form-control">
                        </div> 
                    </div>

                                    
                                    <input type="hidden" value="1" name="iscustomer" id="iscustomer" />
                                    
                                    
                                    <?php    
                                    }
                                    ?>
                                    
                                    
                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
<?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_view_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- update user -->
<div class="modal fade" id="user-modalupdate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-userupdate  box-user-register" id="form-userupdate"  >

                <div class="modal-body">

                    <div class="errorHandlerUserUpdate alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div id="errorUsernameUpdateRepeated" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_usernamerepeated; ?>
                    </div>
                    <div id="errorUsernameUpdateProcessProblems" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
                    </div>
                    <div id="messageUsernameUpdateProcessOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_userprocessok; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_view_block_userblock; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="profile">
                                            <?php echo $_lang_view_label_profile; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <select name="profile" id="userformupdate_profile" class="form-control">
                                                <?php
                                                foreach ($this->aProfile as $profile) {
                                                    if ($profile->id != "SKIU-ROOT-MG") {
                                                        
                                                        $profileValue = $profile->name;
                                                        if($profile->id =='SKIUCUAR'){ $profileValue = $_lang_profile_SKIUCUAR;  } 
                                                        if($profile->id =='SKIUCURL'){ $profileValue = $_lang_profile_SKIUCURL;  } 
                                                        if($profile->id =='SKIUCUAL'){ $profileValue = $_lang_profile_SKIUCUAL;  } 
                                                        
                                                        
                                                        echo '<option value="' . $profile->id . '">' . $profileValue . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div> 
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="firstname">
                                            <?php echo $_lang_view_label_firstname; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userformupdate_firstname" name="firstname" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="lastname">
                                        <?php echo $_lang_view_label_lastname; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userformupdate_lastname" name="lastname" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label" for="phone">
                                        <?php echo $_lang_view_label_phone; ?>
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text"  id="userformupdate_phone" placeholder="<?php echo $_lang_register_label_phone_placeholder; ?>" name="phone" class="form-control">
                                        </div> 


                                        <label class="col-sm-2 control-label" for="phoneext">
                                        <?php echo $_lang_view_label_phoneext; ?>
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text"  id="userformupdate_phoneext" placeholder="<?php echo $_lang_register_label_phoneext_placeholder; ?>"  name="phoneext" class="form-control">
                                        </div> 

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="mobile">
                                        <?php echo $_lang_view_label_mobile; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userformupdate_mobile" name="mobile" class="form-control">
                                        </div>
                                    </div>

                                    <?php 
                                    if($this->iscustomer){
                                    ?>
                                        
                                   

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="position">
                                            <?php echo $_lang_view_label_position; ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text"  id="userformupdate_position" name="position" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>  
                                      
                                    

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="user_country">
                                            <?php echo $_lang_register_label_country; ?>
                                        </label>
                                        <div class="col-sm-5">
                                            <input    value=""     type="text"  id="userformupdate_user_country" name="user_country" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="user_state">
                                            <?php echo $_lang_register_label_state; ?>
                                        </label>
                                        <div class="col-sm-5">
                                            <input   value=""    type="text"  id="userformupdate_user_state" name="user_state" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="user_city">
                                            <?php echo $_lang_register_label_city; ?>
                                        </label>
                                        <div class="col-sm-5">
                                            <input     value=""      type="text"  id="userformupdate_user_city" name="user_city" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="user_street">
                                            <?php echo $_lang_register_label_street; ?>
                                        </label>
                                        <div class="col-sm-5">
                                            <input    value=""       type="text"  id="userformupdate_user_street" name="user_street" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="user_zipcode">
                                            <?php echo $_lang_register_label_zipcode; ?>
                                        </label>
                                        <div class="col-sm-5">
                                            <input    value=""    type="text"  id="userformupdate_user_zipcode" name="user_zipcode" class="form-control">
                                        </div>
                                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="user_pobox">
                                            <?php echo $_lang_register_label_pobox; ?>
                                        </label>
                                        <div class="col-sm-5">
                                            <input    value=""    type="text"  id="userformupdate_user_pobox" name="user_pobox" class="form-control">
                                        </div> 
                                    </div> 
                                        <input type="hidden" value="1" name="iscustomer" id="iscustomer" />
                                    <?php    
                                    }
                                    ?>
                                    
                                    
                                    <input type="hidden" name="id" id="userformupdate_id"   />
                                    <input type="hidden" name="accountid" id="accountid" value="<?php echo $this->id; ?>"  />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
<?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_view_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- change password -->
<div class="modal fade" id="user-changepassword" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-changepassword  box-user-register" id="form-changepassword"  >

                <div class="modal-body">

                    <div class="errorHandlerChangePassword alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div id="messagePasswordChangedOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_userpasswordchanged; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_changepassword; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="password">
<?php echo $_lang_view_label_password; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="password"  id="form_changepassword_password" name="password" class="form-control">
                                        </div> 
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="userform_password_confirm">
<?php echo $_lang_view_label_password_confirm; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="password"  id="form_changepassword_password_confirm" name="password_confirm" class="form-control">
                                        </div> 
                                    </div>



                                    <input type="hidden" name="id" id="form_changepassword_id"   />
                                    <input type="hidden" name="accountid" id="accountid" value="<?php echo $this->id; ?>"  />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
<?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_view_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- change username -->
<div class="modal fade" id="user-changeusername" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-changeusername  box-user-register" id="form-changeusername"  >

                <div class="modal-body">

                    <div class="errorHandlerChangeUsername alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>

                    <div id="errorChangeUsernameRepeated" class="alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_usernamerepeated; ?>
                    </div>

                    <div id="messageUsernameChangedOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_usernamechanged; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_changeemail; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="username">
<?php echo $_lang_view_label_username; ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text"  id="form_changeemail_username" name="username" class="form-control">
                                        </div>  
                                    </div>


                                    <input type="hidden" name="id" id="form_changeemail_id"   />
                                    <input type="hidden" name="accountid" id="accountid" value="<?php echo $this->id; ?>"  />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
<?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_view_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- change status -->
<div class="modal fade" id="user-changestatus" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-changestatus  box-user-register" id="form-changestatus"  >

                <div class="modal-body">

                    <div class="errorHandlerChangeStatus alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                    </div>
                    <div id="messageStatusChangedOk" class="alert alert-success no-display">
                        <i class="fa fa-remove-sign"></i> <?php echo $_lang_view_label_statuschanged; ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_changestatus; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="username">
                                                <?php echo $_lang_label_status; ?>
                                        </label>
                                        <div class="col-sm-10">

                                            <select name="status" id="form_change_status" class="form-control">
                                                <option value="0"><?php echo $_lang_status_inactive; ?></option>
                                                <option value="1"><?php echo $_lang_status_active; ?></option> 
                                            </select>

                                        </div>

                                    </div>

                                    <input type="hidden" name="id" id="form_changestatus_id"   />
                                    <input type="hidden" name="accountid" id="accountid" value="<?php echo $this->id; ?>"  />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
<?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo $_lang_view_label_save; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

 
<div id="panel_users" class="tab-pane">
    
    <div class="page-header">
           <!--<h1>Accounts <small>list</small></h1>-->
        <a class="btn btn-xs btn-primary  user-addmodal   " data-toggle="modal"    href="#user-modal">
            <i class="fa fa-plus-circle"></i> <?php echo $_lang_view_link_addauser; ?>
        </a>
    </div>
    
    
    <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%"   id="table_users">
        <thead>
            <tr>
                <th><?php echo $_lang_view_table_status; ?></th> 
                <th><?php echo $_lang_view_table_username; ?></th> 
                <th><?php echo $_lang_view_table_profile; ?></th>
                <th class="hidden-xs"><?php echo $_lang_view_table_firstname; ?></th>
                <th class="hidden-xs  "><?php echo $_lang_view_table_lastname; ?></th>
                <th class="hidden-xs  "><?php echo $_lang_view_table_mobile; ?></th>
                <th class="hidden-xs  "><?php echo $_lang_view_table_phone; ?></th>
                <th width="20%">- </th>
            </tr>
        </thead>
        <tbody>
            <tr>

                <td>-</td>
                <td>-</td> 
                <td class="hidden-xs">-</td>
                <td class="hidden-xs">-</td> 
                <td class="hidden-xs">-</td>
                <td class="hidden-xs">-</td> 
                <td  width="20%">-</td> 
            </tr>

        </tbody>
    </table>
</div>