                
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/account/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            if ($this->session_accountid != $this->id) {
                echo '<a class="btn btn-xs btn-default" href="/account/index">';
                echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
                echo '</a>';
            }
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<form role="form" class="form-register   box-register" id="form"  >

    <div class="errorHandler alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
    </div>
    <div id="errorCodeRepeated" class="alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_coderepeated; ?>
    </div>
    <div id="errorProcessProblems" class="alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
    </div>


    <div id="errorSubdomainRepeated" class="alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
    </div>

    <div id="messageProcessOk" class="alert alert-success no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
    </div>

    <div class="row">


        <div class="col-sm-6">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_accountinfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  


                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="code">
                            <?php echo $_lang_register_label_code; ?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text"  id="code" name="code" class="form-control" value="<?php echo $this->account->code; ?>">
                        </div>
                        <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_code_help; ?> </span>
                    </div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="taxtypeid">
                            <?php echo $_lang_register_label_taxtype; ?>
                        </label>
                        <div class="col-sm-2">
                            <!--
                            <input    value="<?php echo $this->account->taxtypeid; ?>" type="text"  id="taxtypeid" placeholder="<?php echo $_lang_register_label_taxtype_placeholder; ?>" name="taxtypeid" class="form-control">
                            -->
                            <select name="taxtypeid" id="taxtypeid" class="form-control">
                                <?php
                                foreach ($this->aCountryTaxType as $ct) {
                                    $selected = '';
                                    if ($ct->taxtype == $this->account->taxtypeid && $ct->code == $this->account->countrycode) {
                                        $selected = ' selected = "true" ';
                                    }
                                    echo '<option ' . $selected . ' value="' . $ct->code . '_' . $ct->taxtype . '">' . $ct->name . ' - ' . $ct->taxtype . '  </option>';
                                }
                                ?>
                            </select>
                        </div> 


                        <label class="col-sm-2 control-label" for="taxid">
                            <?php echo $_lang_register_label_taxid; ?>
                        </label>
                        <div class="col-sm-3">
                            <input value="<?php echo $this->account->taxid; ?>"  type="text"  id="taxid" placeholder="<?php echo $_lang_register_label_taxid_placeholder; ?>"  name="taxid" class="form-control">
                        </div> 

                    </div>




                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">
                            <?php echo $_lang_register_label_name; ?>
                        </label>
                        <div class="col-sm-7">
                            <input   value="<?php echo $this->account->name; ?>"    type="text"  id="name" name="name" class="form-control">
                        </div>
                        <span class="help-inline col-sm-2"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>   </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="email">
                            <?php echo $_lang_register_label_email; ?>
                        </label>
                        <div class="col-sm-7">
                            <input     value="<?php echo $this->account->email; ?>"  type="text"  id="email" name="email" class="form-control">
                        </div>
                        <span class="help-inline col-sm-2"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="website">
                            <?php echo $_lang_register_label_website; ?>
                        </label>
                        <div class="col-sm-7">
                            <input    value="<?php echo $this->account->website; ?>"  type="text"  id="website" name="website" class="form-control">
                        </div>
                    </div>


                    <div class="form-group">

                        <label class="col-sm-2 control-label" for="phone">
                            <?php echo $_lang_register_label_phone; ?>
                        </label>
                        <div class="col-sm-3">
                            <input    value="<?php echo $this->account->phone; ?>"  type="text"  id="phone" placeholder="<?php echo $_lang_register_label_phone_placeholder; ?>" name="phone" class="form-control">
                        </div> 


                        <label class="col-sm-2 control-label" for="phoneext">
                            <?php echo $_lang_register_label_phoneext; ?>
                        </label>
                        <div class="col-sm-2">
                            <input    value="<?php echo $this->account->phoneext; ?>"    type="text"  id="phoneext" placeholder="<?php echo $_lang_register_label_phoneext_placeholder; ?>"  name="phoneext" class="form-control">
                        </div> 

                    </div>



                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="website">
                            <?php echo $_lang_register_label_subdomain; ?>
                        </label>
                        <div class="col-sm-7">
                            <input    value="<?php echo $this->account->subdomain; ?>"  type="text"  id="subdomain" name="subdomain" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="code">
                            <?php echo "Moneda de la cuenta"; ?>
                        </label>
                        <div class="col-sm-7">
                            <?php if($this->id){ ?>
                            <input type="text"  id="invoiceduedays" name="invoiceduedays" class="form-control" value="<?php echo $this->account->defaultcurrency; ?>" disabled>
                            <?php }else{ ?>
                                <input type="text"  id="invoiceduedays" name="invoiceduedays" class="form-control" value="USD" disabled>
                            <?php } ?>
                        </div>
                        <span class="help-inline col-sm-2"> <i class="fa fa-info-circle"></i> <?php echo "No afecta la facturación"; ?> </span>
                    </div> 

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="asignedto">
                            <?php echo $_lang_register_label_asignedto; ?>
                        </label>
                        <div class="col-sm-7">
                            <select name="ownerid" id="ownerid" class="form-control">
                                <option value=""><?php echo $_lang_register_label_asignedto_select; ?></option>
                                <?php
                                $total = count($this->aUsers);
                                if ($total > 0) {
                                    foreach ($this->aUsers as $user) {
                                        $selected = '';
                                        if ($user->id == $this->account->ownerid) {
                                            $selected = ' selected = "true" ';
                                        }
                                        echo '<option ' . $selected . ' value="' . $user->id . '">' . $user->firstname . ' ' . $user->lastname . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>



                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>


        <div class="col-sm-6">
            <!-- start: TEXT FIELDS PANEL -->
            
            <?php
      
            if($this->account->parentaccountid == $this->session_accountid || intval($this->account->id) ==0  ){
                ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">


                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_accountcredit; ?>
                                <div class="panel-tools">

                                    <a class="btn btn-xs btn-link panel-expand" href="#">
                                        <i class="fa fa-resize-full"></i>
                                    </a>

                                </div>
                            </div>


                            <div class="panel-body  form-horizontal">  


                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="code">
                                        <?php echo $_lang_register_label_creditlimit; ?>
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text"  id="creditlimit" name="creditlimit" class="form-control" value="<?php echo $this->account->creditlimit; ?>">
                                    </div>
                                    <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="code">
                                        <?php echo $_lang_register_label_invoicedue; ?>
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text"  id="invoiceduedays" name="invoiceduedays" class="form-control" value="<?php echo $this->account->invoiceduedays; ?>">
                                    </div>
                                    <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                </div> 

                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="code">
                                        <?php echo "Moneda de la facturación"; ?>
                                    </label>
                                    <div class="col-sm-4">
                                    <select class="form-control" name="currencyrelation" id="currencyrelation">
                                        <?php
                                        foreach($this->listCurrecies as $item){
                                            $selected="";
                                            if($this->currencyRelation==$item->code) $selected="selected='selected'";
                                            ?>
                                            <option value="<?php echo $item->code; ?>" <?php echo $selected; ?>><?php echo $item->code; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    </div>
                                    <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo "Recibos y facturas se emitiran en esta moneda"; ?> </span>
                                </div> 
                                    
                                
                            </div>

                        </div>
                    </div>
                </div>
                <?php
            }
            
            ?>


            
            
            <?php
      
            if($this->session_accountid === 10001){
                ?>
           
                <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">


                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_accountconfig; ?>
                            <div class="panel-tools">

                                <a class="btn btn-xs btn-link panel-expand" href="#">
                                    <i class="fa fa-resize-full"></i>
                                </a>

                            </div>
                        </div>


                        <div class="panel-body  form-horizontal">  


                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="code">
                                    <?php echo $_lang_register_label_conf_quotevaliddays; ?>
                                </label>
                                <div class="col-sm-4">
                                    <input type="text"  id="confquotevaliddays" name="confquotevaliddays" class="form-control" value="<?php echo $this->account->confquotevaliddays; ?>">
                                </div>
                                <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="code">
                                    <?php echo $_lang_register_label_conf_invoicedue; ?>
                                </label>
                                <div class="col-sm-4">
                                    <input type="text"  id="confinvoicedue" name="confinvoicedue" class="form-control" value="<?php echo $this->account->confinvoicedue; ?>">
                                </div>
                                <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                            </div> 

                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="code">
                                    <?php echo $_lang_register_label_conf_taxespercent; ?>
                                </label>
                                <div class="col-sm-4">
                                    <input type="text"  id="conftaxespercent" name="conftaxespercent" class="form-control" value="<?php echo $this->account->conftaxespercent; ?>">
                                </div>
                                <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                            </div> 
                            
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="code">
                                        <?php echo $_lang_register_label_invoiceprefix; ?>
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text"  id="invoiceprefix" name="invoiceprefix" class="form-control" value="<?php echo $this->account->invoiceprefix; ?>">
                                    </div>
                                    <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                </div> 

                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="code">
                                        <?php echo $_lang_register_label_quoteprefix; ?>
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text"  id="quoteprefix" name="quoteprefix" class="form-control" value="<?php echo $this->account->quoteprefix; ?>">
                                    </div>
                                    <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                </div>   
                            
                            
                            
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="customplansactive">
                                        <?php echo $_lang_register_label_activatecustomplans; ?>
                                    </label>
                                    <div class="col-sm-4">
                                        
                                        <select name ="customplansactive" id="customplansactive" class="form-control">
                                            <?php
                                            
                                            if(intval($this->account->customplansactive) == 1 ){
                                                echo '<option value="0">'.$_lang_register_label_activatecustomplans_no.'</option>';
                                                echo '<option value="1" selected ="true">'.$_lang_register_label_activatecustomplans_yes.'</option>';
                                            }else{
                                                echo '<option value="0" selected ="true">'.$_lang_register_label_activatecustomplans_no.'</option>';
                                                echo '<option value="1">'.$_lang_register_label_activatecustomplans_yes.'</option>';                                                
                                            }
                                            ?>
                                        </select>
                                        
                                    </div>
                                    <span class="help-inline col-sm-4"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                                </div>                              
                            
                            
                        </div>

                    </div>
                </div>
            </div>
            
            <?php
            }
            ?>
        </div>


    </div>
    <!-- end: TEXT FIELDS PANEL -->


    <div class="row">

        <div class="col-sm-6">
            <!-- start: TEXT AREA PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_billaddress; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>
                <div class="panel-body  form-horizontal" >

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_country">
                            <?php echo $_lang_register_label_country; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value="<?php echo $this->account->billaddress->country; ?>"     type="text"  id="bill_country" name="bill_country" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_state">
                            <?php echo $_lang_register_label_state; ?>
                        </label>
                        <div class="col-sm-5">
                            <input   value="<?php echo $this->account->billaddress->state; ?>"    type="text"  id="bill_state" name="bill_state" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_city">
                            <?php echo $_lang_register_label_city; ?>
                        </label>
                        <div class="col-sm-5">
                            <input     value="<?php echo $this->account->billaddress->city; ?>"      type="text"  id="bill_city" name="bill_city" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_street">
                            <?php echo $_lang_register_label_street; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value="<?php echo $this->account->billaddress->street; ?>"       type="text"  id="bill_street" name="bill_street" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_zipcode">
                            <?php echo $_lang_register_label_zipcode; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value="<?php echo $this->account->billaddress->zipcode; ?>"    type="text"  id="bill_zipcode" name="bill_zipcode" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="bill_pobox">
                            <?php echo $_lang_register_label_pobox; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value="<?php echo $this->account->billaddress->pobox; ?>"    type="text"  id="bill_pobox" name="bill_pobox" class="form-control">
                        </div> 
                    </div>


                </div>
            </div>
            <!-- end: TEXT AREA PANEL -->
        </div>

        <div class="col-sm-6">
            <!-- start: TEXT AREA PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_shipaddress; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>
                <div class="panel-body  form-horizontal" >

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_country">
                            <?php echo $_lang_register_label_country; ?>
                        </label>
                        <div class="col-sm-5">
                            <input     value="<?php echo $this->account->shipaddress->country; ?>"    type="text"  id="ship_country" name="ship_country" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i><?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_state">
                            <?php echo $_lang_register_label_state; ?>
                        </label>
                        <div class="col-sm-5">
                            <input     value="<?php echo $this->account->shipaddress->state; ?>"  type="text"  id="ship_state" name="ship_state" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_city">
                            <?php echo $_lang_register_label_city; ?>
                        </label>
                        <div class="col-sm-5">
                            <input   value="<?php echo $this->account->shipaddress->city; ?>"     type="text"  id="ship_city" name="ship_city" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_street">
                            <?php echo $_lang_register_label_street; ?>
                        </label>
                        <div class="col-sm-5">
                            <input    value="<?php echo $this->account->shipaddress->street; ?>"     type="text"  id="ship_street" name="ship_street" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?>  </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_zipcode">
                            <?php echo $_lang_register_label_zipcode; ?>
                        </label>
                        <div class="col-sm-5">
                            <input     value="<?php echo $this->account->shipaddress->zipcode; ?>"  type="text"  id="ship_zipcode" name="ship_zipcode" class="form-control">
                        </div>
                        <span class="help-inline col-sm-5"> <i class="fa fa-info-circle"></i> <?php echo $_lang_register_label_requiredfield_help; ?> </span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ship_pobox">
                            <?php echo $_lang_register_label_pobox; ?>
                        </label>
                        <div class="col-sm-5">
                            <input     value="<?php echo $this->account->shipaddress->pobox; ?>"  type="text"  id="ship_pobox" name="ship_pobox" class="form-control">
                        </div>
                    </div>


                </div>
            </div>
            <!-- end: TEXT AREA PANEL -->
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">


            <div class="panel panel-default">
                <div class="panel-footer">

                    <button type="submit" class="btn btn-primary ">
                        <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                    </button>

                </div>
            </div>


        </div>
    </div>

    <?php
    if ($this->id != "") {
        echo '<input type="hidden" name="id" id="accountid" value="' . $this->id . '" />';
    }
    ?>

</form>
<!-- end: PAGE CONTENT-->
