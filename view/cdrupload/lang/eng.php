<?php
$_lang_view_account = "Account";
$_lang_view_serial = "Simcard #";
$_lang_view_voicen = "Voice #";
$_lang_view_datan = "Data #";
$_lang_view_daterange = "Date";
$_lang_btn_generatereport = "Generate Report";


//-- Catalog Index
$_lang_index_label_module = "Cdr";
$_lang_index_label_action = "Report";

$_lang_index_label_title = "Cdr Report";

$_lang_view_label_startdate = "Start Date";
$_lang_view_label_starttime = "Start Time";
$_lang_view_label_mobilenumber = "Mobile number";
$_lang_view_label_callednumber = "Called number";
$_lang_view_label_countrydialedname = "Country Dialed Name";
$_lang_view_label_ratingcalltype = "Rating call type";
$_lang_view_label_end_date = "End Date";
$_lang_view_label_end_time = "End Time";
$_lang_view_label_units = "Units";
$_lang_view_label_totalcharge = "Total Charge";

$_lang_view_datetime ="Date";
$_lang_view_source ="Origen";
$_lang_view_destination ="Destination";
$_lang_view_type ="Type";
$_lang_view_plan ="Plan";
$_lang_view_duration ="Duration";
$_lang_view_additionalcharges ="Additional airtime";
$_lang_view_total ="Total";


$_lang_view_cdrinfo = "You have to consider a  latency between the cdrs recorded in the system and real-time cdrs, this latency may differ between satellite networks, i.e.  ";
$_lang_view_cdrinfo .= " Inmarsat CDRs with a latency of 2 hours, Iridium CDRs with a latency of 48 hours. This info is only referencial ";
