<?php
$_lang_view_account = "Cuenta";
$_lang_view_serial = "Simcard #";
$_lang_view_voicen = "Voz #";
$_lang_view_datan = "Datos #";
$_lang_view_daterange = "Fecha";
$_lang_btn_generatereport = "Generar Reporte";


//-- Catalog Index
$_lang_index_label_module = "Cdr";
$_lang_index_label_action = "Reporte";

$_lang_index_label_title = "Reporte de CDRs";

$_lang_view_label_startdate = "Fecha de Inicio";
$_lang_view_label_starttime = "Hora de Inicio";
$_lang_view_label_mobilenumber = "N&uacute;mero m&oacute;vil";
$_lang_view_label_callednumber = "N&uacute;mero marcado";
$_lang_view_label_countrydialedname = "Pa&iacute;s marcado";
$_lang_view_label_ratingcalltype = "Cargos por el tipo de llamada";
$_lang_view_label_end_date = "Fecha de Fin";
$_lang_view_label_end_time = "Hora de Fin";
$_lang_view_label_units = "Unidades";
$_lang_view_label_totalcharge = "Cargos totales";


$_lang_view_datetime ="Fecha";
$_lang_view_source ="Origen";
$_lang_view_destination ="Destino";
$_lang_view_plan ="Plan";
$_lang_view_type ="Tipo";
$_lang_view_duration ="Duraci&oacute;n";
$_lang_view_additionalcharges ="Consumo adicional";
$_lang_view_total ="Total";

$_lang_view_cdrinfo = "Usted deber&aacute; considerar una latencia entre los CDRs registrados en el sistema y los CDRs procesados en tiempo real, ";
$_lang_view_cdrinfo .= "esta latencia puede diferir entre redes satelitales, tomando como ejemplo m&aacute;s no &uacute;nico:  ";
$_lang_view_cdrinfo .= " los CDRs de Inmarsat presentan una latencia de 2 horas, Los CDRs de Iridium presentan una latencia de 48 horas. Esta informaci&oacute;n es &uacute;nicamente referencial ";