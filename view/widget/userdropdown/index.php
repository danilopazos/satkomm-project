<li class="dropdown current-user">
    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
        <!--<img src="assets/images/avatar-1-small.jpg" class="circle-img" alt="">-->
        <span class="username"><?php echo $this->session_firstname.' '.$this->session_lastname; ?></span>
        <i class="clip-chevron-down"></i>
    </a>
    <ul class="dropdown-menu">
        <li>
            <a href="/myprofile/index">
                <i class="clip-user-2"></i> &nbsp;<?php echo $lang_userdropdown_myprofile; ?>
            </a>
        </li>
        
        <li>
            <a href="/account/myaccount">
                <i class="fa fa-building-o"></i> &nbsp;<?php echo $lang_userdropdown_myaccount; ?>
            </a>
        </li>
        
        <!--
        <li>
            <a href="pages_calendar.html">
                <i class="clip-calendar"></i> &nbsp;My Calendar
            </a>
        <li>
            <a href="pages_messages.html">
                <i class="clip-bubble-4"></i> &nbsp;My Messages (3)
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="utility_lock_screen.html">
                <i class="clip-locked"></i> &nbsp;Lock Screen
            </a>
        </li>
        -->
        <li>
            <a href="/auth/signout">
                <i class="clip-exit"></i> &nbsp;<?php echo $lang_userdropdown_logout; ?>
            </a>
        </li>
    </ul>
</li>