<?php
$_lang_index_module_dashboard= "Dashboard";
$_lang_index_module_accounts= "Accounts";
$_lang_index_module_customerorders= "Customer Orders";
$_lang_index_module_customerinvoices= "Customer Invoices";
$_lang_index_module_customerorders_airtime= "Customer Orders - Airtime";
$_lang_index_module_taxesconfiguration= "Taxes Configuration";
$_lang_index_module_feesconfiguration= "Fees Configuration";
$_lang_index_module_catalog= "Catalog";
$_lang_index_module_mycart= "My Cart";
$_lang_index_module_myorders= "My Orders";
$_lang_index_module_inventorydevices= "Inventory Devices";
$_lang_index_module_inventorysimcards= "Inventory Simcards";
$_lang_index_module_cdrs= "CDRs";
$_lang_index_module_cdrs_2= "CDRs (2)";
$_lang_index_module_mycart_airtime= "My Cart - Airtime";
$_lang_index_module_myorders_airtime= "My Orders - Airtime";
$_lang_index_module_airtime_alerts= "Airtime Alerts.";
$_lang_index_module_traffictype= "Traffic type";
$_lang_index_module_trafficunit= "Units (Traffic)";
$_lang_index_module_airtimeplan = "Plan (Traffic)";
$_lang_index_module_reports = "Reports";
$_lang_index_module_quotes = "Quotes";
$_lang_index_module_productspricebook = "My products";
$_lang_index_module_myairtimeplans = "My airtime plans";
$_lang_index_module_myproviderorders = "My Orders";
$_lang_index_module_myinventory = "My Inventory";
$_lang_index_module_commercial = "Marketing";
$_lang_index_module_consumptionalerts= "CDRs & Alerts";

$_lang_index_module_invoicepayment= "Payments";
$_lang_index_module_mystatement= "My account statement";


$_lang_index_module_invoices= "Invoices";
$_lang_index_module_consumption= "Consumption";

$_lang_index_module_currency= "Currency";