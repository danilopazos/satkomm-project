<?php
$_lang_index_module_dashboard= "Dashboard";
$_lang_index_module_accounts= "Cuentas";
$_lang_index_module_customerorders= "OC - Equipos";
$_lang_index_module_customerinvoices= "Facturas de Clientes";
$_lang_index_module_customerorders_airtime= "OC - Tiempo de aire";
$_lang_index_module_taxesconfiguration= "Impuestos";
$_lang_index_module_feesconfiguration= "Cargos";
$_lang_index_module_catalog= "Cat&aacute;logo";
$_lang_index_module_mycart= "Mi Carrito";
$_lang_index_module_myorders= "Mis &Oacute;rdenes ";
$_lang_index_module_inventorydevices= "Equipos(Inventario)";
$_lang_index_module_inventorysimcards= "Simcards(Inventario)";
$_lang_index_module_cdrs= "CDRs";
$_lang_index_module_cdrs_2= "CDRs (2)";
$_lang_index_module_mycart_airtime= "Mi Carrito - Tiempo de Aire";
$_lang_index_module_myorders_airtime= "Mis &Oacute;rdenes - Tiempo de Aire";
$_lang_index_module_airtime_alerts= "Alertas de consumo";
$_lang_index_module_traffictype= "Tipo de tr&aacute;fico";
$_lang_index_module_trafficunit= "Unidades (Tr&aacute;fico)";
$_lang_index_module_airtimeplan = "Plan (Tr&aacute;fico)";
$_lang_index_module_reports = "Reportes";
$_lang_index_module_quotes = "Cotizaciones";
$_lang_index_module_productspricebook = "Mis productos";
$_lang_index_module_myairtimeplans = "Mis planes";
$_lang_index_module_myproviderorders = "Mis &oacute;rdenes";
$_lang_index_module_myinventory = "Mi Inventario";
$_lang_index_module_commercial = "Comercial";
$_lang_index_module_consumptionalerts= "Consumo y alertas";

$_lang_index_module_invoicepayment= "Pagos";
$_lang_index_module_mystatement= "Mi estado de cuenta";


$_lang_index_module_invoices= "Facturaci&oacute;n";
$_lang_index_module_consumption= "Consumo";

$_lang_index_module_currency= "Tipo de cambio";