<!-- start: MAIN NAVIGATION MENU -->
<ul class="main-navigation-menu">

    <?php
    if ($this->session_profileid == "SKIU-ROOT-MG") {
        ?>   
        <li>
            <!--active open-->
            <a href="/">
                <i class="clip-home-3"></i>
                <span class="title"> <?php echo $_lang_index_module_dashboard; ?> </span><span class="selected"></span>
            </a>
        </li> 

        <li>
            <!--active open-->
            <a href="/account/index">
                <i class="fa fa-building-o"></i>
                <span class="title"> <?php echo $_lang_index_module_accounts; ?> </span><span class="selected"></span>
            </a>
        </li>




        <li> 
            <a href="/saleorder/index">
                <i class="fa fa-codepen"></i>
                <span class="title"> <?php echo $_lang_index_module_customerorders; ?> </span><span class="selected"></span>
            </a>
        </li>


        <li> 
            <a href="/invoice/index">
                <i class="fa fa-list-alt"></i>
                <span class="title"> <?php echo $_lang_index_module_customerinvoices; ?> </span><span class="selected"></span>
            </a>
        </li>




        <li> 
            <a href="/airtimeoperation/customerorders">
                <i class="fa fa-plane"></i>
                <span class="title"> <?php echo $_lang_index_module_customerorders_airtime; ?> </span><span class="selected"></span>
            </a>
        </li>    

        <li> 
            <a href="/customerconsumption/index">
                <i class="fa fa-file-pdf-o"></i>
                <span class="title">  <?php echo $_lang_index_module_consumption; ?> </span><span class="selected"></span>
            </a>
        </li>  


                <li> 
                    <a href="/cdr/index">
                        <i class="fa fa-keyboard-o"></i>
                        <span class="title"> <?php echo $_lang_index_module_cdrs; ?></span><span class="selected"></span>
                    </a>
                </li>  
        
        <!--
                <li> 
                    <a href="/cdr/index">
                        <i class="fa fa-keyboard-o"></i>
                        <span class="title"> <?php echo $_lang_index_module_cdrs; ?></span><span class="selected"></span>
                    </a>
                </li>  
        -->
        <li> 
            <a href="/system/taxes">
                <i class="fa fa-cog"></i>
                <span class="title">  <?php echo $_lang_index_module_taxesconfiguration; ?></span><span class="selected"></span>
            </a>
        </li>    
        <li> 
            <a href="/system/fees">
                <i class="fa fa-cog"></i>
                <span class="title"> <?php echo $_lang_index_module_feesconfiguration; ?></span><span class="selected"></span>
            </a>
        </li>     
        <li> 
            <a href="/traffictype/index">
                <i class="fa fa-plane"></i>
                <span class="title"> <?php echo $_lang_index_module_traffictype; ?></span><span class="selected"></span>
            </a>
        </li>             
        <li> 
            <a href="/trafficunit/index">
                <i class="fa fa-plane"></i>
                <span class="title"> <?php echo $_lang_index_module_trafficunit; ?></span><span class="selected"></span>
            </a>
        </li>    
        <li> 
            <a href="/airtimeplan/index">
                <i class="fa fa-plane"></i>
                <span class="title"> <?php echo $_lang_index_module_airtimeplan; ?></span><span class="selected"></span>
            </a>
        </li>    

        <li> 
            <a href="/invoicepayment/index">
                <i class="fa fa-money"></i>
                <span class="title">  <?php echo $_lang_index_module_invoicepayment; ?> </span><span class="selected"></span>
            </a>
        </li>      
        
        
        <li>

            <a href="/myproducts/index">
                <i class="fa fa fa-list-alt"></i>
                <span class="title"> <?php echo $_lang_index_module_productspricebook; ?>  </span><span class="selected"></span>
            </a>
        </li> 

        <?php
    } else {
        ?> 
        <li>
            <!--active open-->
            <a href="/">
                <i class="clip-home-3"></i>
                <span class="title"> <?php echo $_lang_index_module_dashboard; ?> </span><span class="selected"></span>
            </a>
        </li> 


        <li>
            <a href="javascript:void(0)">
                <i class="fa fa-shopping-cart"></i>
                <span class="title"> <?php echo $_lang_index_module_myproviderorders; ?> </span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li> 
                    <a href="/catalog/index">
                        <i class="fa fa-list-alt"></i>
                        <span class="title"> <?php echo $_lang_index_module_catalog; ?> </span><span class="selected"></span>
                    </a>
                </li>
                <li> 
                    <a href="/cart/mycart">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="title"> <?php echo $_lang_index_module_mycart; ?>  </span><span class="selected"></span>
                    </a>
                </li>
                <li> 
                    <a href="/saleorder/myorders">
                        <i class="fa fa-codepen"></i>
                        <span class="title">  <?php echo $_lang_index_module_myorders; ?> </span><span class="selected"></span>
                    </a>
                </li>
                <li> 
                    <a href="/airtimeoperation/mycart">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="title"><?php echo $_lang_index_module_mycart_airtime; ?> </span><span class="selected"></span>
                    </a>
                </li>     

                <li> 
                    <a href="/airtimeoperation/myorders">
                        <i class="fa fa-plane"></i>
                        <span class="title"> <?php echo $_lang_index_module_myorders_airtime; ?>  </span><span class="selected"></span>
                    </a>
                </li>  
            </ul>
        </li>

        <li>
            <a href="javascript:void(0)">
                <i class="fa fa-dropbox"></i>
                <span class="title"> <?php echo $_lang_index_module_myinventory; ?> </span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li> 
                    <a href="/inventory/mydevices">
                        <i class="fa fa-dropbox"></i>
                        <span class="title">  <?php echo $_lang_index_module_inventorydevices; ?> </span><span class="selected"></span>
                    </a>
                </li>  
                <li> 
                    <a href="/inventory/mysimcards">
                        <i class="fa fa-keyboard-o"></i>
                        <span class="title">  <?php echo $_lang_index_module_inventorysimcards; ?> </span><span class="selected"></span>
                    </a>
                </li>  
            </ul>
        </li>    


        <li>
            <a href="javascript:void(0)">
                <i class="fa fa-money"></i>
                <span class="title"> <?php echo $_lang_index_module_commercial; ?> </span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li>

                    <a href="/myproducts/index">
                        <i class="fa fa fa-list-alt"></i>
                        <span class="title"> <?php echo $_lang_index_module_productspricebook; ?>  </span><span class="selected"></span>
                    </a>
                </li> 
                <li>

                    <a href="/myairtimeplan/index">
                        <i class="fa fa fa-list-alt"></i>
                        <span class="title"> <?php echo $_lang_index_module_myairtimeplans; ?>  </span><span class="selected"></span>
                    </a>
                </li> 
                <li>

                    <a href="/account/index">
                        <i class="fa fa-building-o"></i>
                        <span class="title"> <?php echo $_lang_index_module_accounts; ?>  </span><span class="selected"></span>
                    </a>
                </li>


                <li>
                    <a href="/quote/index">
                        <i class="fa fa-file-archive-o"></i>
                        <span class="title"> <?php echo $_lang_index_module_quotes; ?>  </span><span class="selected"></span>
                    </a>
                </li>

                <li>
                    <a href="/currency/index">
                        <i class="fa fa-money"></i>
                        <span class="title"> <?php echo $_lang_index_module_currency; ?>  </span><span class="selected"></span>
                    </a>
                </li>

            </ul>
        </li>   



        <li>
            <a href="javascript:void(0)">
                <i class="clip-screen"></i>
                <span class="title"> <?php echo $_lang_index_module_consumptionalerts; ?> </span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">


                <li> 
                    <a href="/cdr/index">
                        <i class="fa fa-keyboard-o"></i>
                        <span class="title"> <?php echo $_lang_index_module_cdrs; ?></span><span class="selected"></span>
                    </a>
                </li>
                
                <li> 
                    <a href="/cdr/temporaryfilter">
                        <i class="fa fa-keyboard-o"></i>
                        <span class="title"> <?php echo $_lang_index_module_cdrs_2; ?></span><span class="selected"></span>
                    </a>
                </li>   

                <li> 
                    <a href="/allowancealert/index">
                        <i class="fa fa-warning"></i>
                        <span class="title">  <?php echo $_lang_index_module_airtime_alerts; ?> </span><span class="selected"></span>
                    </a>
                </li>  

                <li>
                    <a href="/invoicetc/index">
                        <i class="fa fa-file-archive-o"></i>
                        <span class="title"> <?php echo $_lang_index_module_invoices; ?>  </span><span class="selected"></span>
                    </a>
                </li>

                <li> 
                    <a href="/customerconsumption/index">
                        <i class="fa fa-file-pdf-o"></i>
                        <span class="title">  <?php echo $_lang_index_module_consumption; ?> </span><span class="selected"></span>
                    </a>
                </li>                 

                <li> 
                    <a href="/invoicepayment/index">
                        <i class="fa fa-money"></i>
                        <span class="title">  <?php echo $_lang_index_module_invoicepayment; ?> </span><span class="selected"></span>
                    </a>
                </li>                  

                <li> 
                    <a href="/invoicepayment/mystatement">
                        <i class="fa fa-money"></i>
                        <span class="title">  <?php echo $_lang_index_module_mystatement; ?> </span><span class="selected"></span>
                    </a>
                </li>                    

            </ul>
        </li>    

        <li> 
            <a href="/reports/index">
                <i class="fa fa-list-alt"></i>
                <span class="title">  <?php echo $_lang_index_module_reports; ?> </span><span class="selected"></span>
            </a>
        </li> 

        <?php
    }
    ?>

</ul>
<!-- end: MAIN NAVIGATION MENU -->