<?php

//-- Catalog Index
$_lang_index_label_module = "Reportes";
$_lang_index_label_action = "Lista";
 $_lang_index_label_reportslist = "Lista de Reportes";
 
 $_lang_index_label_services_consolidated = "Consolidado de servicios";
$_lang_index_label_products_consolidated = "Consolidado de productos";
 
$_lang_index_label_salesman = "Ejecutivo ";
$_lang_index_label_period = "Periodo ";
$_lang_index_label_network = "Redes ";
$_lang_index_label_operation = "Operaci&oacute;n ";
$_lang_index_label_airtimeplans = "Planes ";
$_lang_index_label_nrorecords = "Nro registros ";

$_lang_all = "Todos";


$_lang_month[0] = "Enero";
$_lang_month[1] = "Febrero";
$_lang_month[2] = "Marzo";
$_lang_month[3] = "Abril";
$_lang_month[4] = "Mayo";
$_lang_month[5] = "Junio";
$_lang_month[6] = "Julio";
$_lang_month[7] = "Agosto";
$_lang_month[8] = "Septiembre";
$_lang_month[9] = "Octubre";
$_lang_month[10] = "Noviembre";
$_lang_month[11] = "Diciembre";


$_lang_operation_activate = "Activaci&oacute;n";
$_lang_operation_suspend = "Suspensi&oacute;n";
$_lang_operation_deactivate = "Desactivaci&oacute;n";

$_lang_downloadxls = "Descargar reporte en XLS";
 
$_lang_generation_reportname = "Reporte: ";
$_lang_generation_user = "Generado por: ";
$_lang_generation_datetime = "Fecha y hora:  ";