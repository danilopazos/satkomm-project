<?php

//-- Catalog Index
$_lang_index_label_module = "Reports";
$_lang_index_label_action = "List";
$_lang_index_label_reportslist = "Reports List";


$_lang_index_label_services_consolidated = "Services consolidated";
$_lang_index_label_products_consolidated = "Products consolidated";



$_lang_index_label_salesman = "Assigned to ";
$_lang_index_label_period = "Period ";
$_lang_index_label_network = "Network ";
$_lang_index_label_operation = "Operation ";
$_lang_index_label_airtimeplans = "Airtime plans ";
$_lang_index_label_nrorecords = "Nro Records ";

$_lang_all = "All";


$_lang_month[0] = "January";
$_lang_month[1] = "February";
$_lang_month[2] = "March";
$_lang_month[3] = "April";
$_lang_month[4] = "May";
$_lang_month[5] = "June";
$_lang_month[6] = "July";
$_lang_month[7] = "August";
$_lang_month[8] = "September";
$_lang_month[9] = "October";
$_lang_month[10] = "November";
$_lang_month[11] = "December";

$_lang_operation_activate = "Activation";
$_lang_operation_suspend = "Suspension";
$_lang_operation_deactivate = "Deactivation";

$_lang_downloadxls = "Download report in XLS";

 

$_lang_generation_reportname = "Report: ";
$_lang_generation_user = "Generate by: ";
$_lang_generation_datetime = "Date and time:  ";