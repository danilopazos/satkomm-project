
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-building-o"></i>
                <a href="/reports/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_products_consolidated; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_label_products_consolidated; ?>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">

                <form name="formdefault" id="formdefault" action="/reports/servicesconsolidated_xls" method="POST" target="_blank"  >

                    <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">

                        <tbody>
                            <tr>
                                <td align="right"><?php echo $_lang_index_label_salesman; ?></td>
                                <td>
                                    <select id="salesman"  name="user[]" class="form-control" multiple="true"> 
                                        <?php
                                        foreach ($this->aUsers as $user) {
                                            echo '<option value="' . $user->id . '">' . $user->firstname . ' ' . $user->lastname . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td> 

                            </tr>

                            <tr>
                                <td align="right"><?php echo $_lang_index_label_period; ?></td>
                                <td>
                                    <select id="period" name="period[]" class="form-control" multiple="true"> 
                                        <?php
                                        foreach ($this->aPeriod as $period) {
                                            $_year = intval(date("Y"));
                                            $_month = intval(date("m"));

                                            $selected = '';
                                            if ($period["year"] == $_year && $period["month"] == $_month) {
                                                $selected = ' selected = "true" ';
                                            }

                                            echo '<option ' . $selected . ' value="' . $period["value"] . '">' . $period["year"] . ' - ' . $_lang_month[intval($period["month"]) - 1] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td align="right"><?php echo $_lang_index_label_network; ?></td>
                                <td>
                                    <select id="networkcode"  name="networkcode[]" class="form-control" multiple="true"> 
                                        <?php
                                        foreach ($this->aNetwork as $network) {
                                            echo '<option value="' . $network->code . '">' . $network->description . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td> 

                            </tr>

                            <tr>
                                <td align="right"><?php echo $_lang_index_label_operation; ?></td>
                                <td>
                                    <select id="operation"  name="operation[]" class="form-control" multiple="true"> 
                                        <?php
                                        echo '<option value="activate">Venta</option>';
                                        echo '<option value="suspend">Arrendamiento</option>';
                                        ?>
                                    </select>
                                </td> 

                            </tr>


                            <tr>
                                <td align="right">Products</td>
                                <td>
                                    <select id="plan"  name="plan[]" class="form-control" multiple="true"> 
                                        <?php
                                        foreach ($this->aPlan as $plan) {
                                            echo '<option value="' . $plan->idcrypt . '">' . $plan->name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td> 

                            </tr>

 
                            <tr>
                                <td align="right"> </td>
                                <td>
                                    <a href="#" id="downloadxls" class="btn btn-green"><?php echo $_lang_downloadxls; ?></a>
                                </td> 

                            </tr>


                        </tbody>
                    </table>

                </form>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->

    </div>
</div>
<!-- end: PAGE CONTENT-->
