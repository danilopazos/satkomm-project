 
        <form role="form" class="form-register   box-register" id="form"  >
            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
            </div>
            <div id="errorCodeRepeated" class="alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_coderepeated; ?>
            </div>
            <div id="errorProcessProblems" class="alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
            </div>

            <div id="errorSkuAndVersion" class="alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_skuandversionproblems; ?>
            </div>            
            
            <div id="messageProcessOk" class="alert alert-success no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
            </div>            

            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_airtimeplaninfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="customername">
                            <?php echo $_lang_view_label_customerassigned; ?>
                        </label>
                        
                        <div class="col-sm-6">
                            <?php 
                            if(intval($this->entity->customeraccountid) > 0){
                                echo '<input type="text" disabled="true"  class="form-control" value="'.$this->entity->customername.'" />';
                            }else{
                                echo '<input type="text" disabled="true"    class="form-control" value="'.$_lang_view_label_customerassigned_all.'" />';
                            }
                            ?>
                        </div>
                         
                    </div>                    
                    

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="sku">
                            <?php echo $_lang_register_label_sku; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="sku" name="sku" class="form-control" value="<?php echo $this->entity->sku; ?>" />
                        </div>
                    </div>
                    
                    

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="version">
                            <?php echo $_lang_register_label_version; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="version" name="version" class="form-control" value="<?php echo $this->entity->version; ?>" />
                        </div>
                    </div>                    
                    
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="aviable">
                            <?php echo $_lang_register_label_aviable; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="aviable" id="aviable">
                                
                                <?php 
                                if($this->entity->aviable == 1){
                                    
                                    echo '<option value="0"  >No aviable</option>';
                                    echo '<option value="1" selected ="true" >Aviable</option>';
                                    
                                }else{
                                    
                                    echo '<option value="0" selected="true" >No aviable</option>';
                                    echo '<option value="1" >Aviable</option>';
                                    
                                }
                                ?>
                                

                            </select>
                        </div>
                    </div>                       
                    
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="networkcode">
                            <?php echo $_lang_register_label_networkcode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="networkcode" id="networkcode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php
                                foreach ($this->aNetwork as $network) {
                                    $selected = '';
                                    if ($this->entity->networkcode == $network->code) {
                                        $selected = ' selected = "true" ';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option ' . $selected . ' value="' . $network->code . '">' . $network->description . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="networkcode">
                            <?php echo $_lang_label_ttgroup; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="ttgroup" id="ttgroup" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php 
                                foreach($this->aTrafficTypeGroup as $ttgroup){
                                    
                                    $selected = '';
                                    if ($this->entity->ttgroup == $ttgroup) {
                                        $selected = ' selected = "true" ';
                                    } else {
                                        $selected = '';
                                    }                                    
                                    
                                    echo '<option '.$selected.' value="'.$ttgroup.'">'.$ttgroup.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>                    
                    
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="servicetypecode">
                            <?php echo $_lang_register_label_servicetypecode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="servicetypecode" id="servicetypecode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php
                                foreach ($this->aServiceType as $serviceType) {
                                    $selected = '';
                                    if ($this->entity->servicetypecode == $serviceType->code) {
                                        $selected = ' selected = "true" ';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option ' . $selected . ' value="' . $serviceType->code . '">' . $serviceType->code . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>                    

                    <div class="form-group" id="div_paymentperiodcode">
                        <label class="col-sm-6 control-label" for="paymentperiodcode">
                            <?php echo $_lang_register_label_paymentperiodcode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="paymentperiodcode" id="paymentperiodcode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php
                                foreach ($this->aPeriod as $period) {
                                    $selected = '';
                                    if ($this->entity->paymentperiodcode == $period->code) {
                                        $selected = ' selected = "true" ';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option ' . $selected . ' value="' . $period->code . '">' . periodToOption($period->code, $period->timecode, $period->qty) . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="contractperiodcode" id="lbl_contractperiodcode">
                            <?php echo $_lang_register_label_contractperiodcode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="contractperiodcode" id="contractperiodcode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php
                                foreach ($this->aPeriod as $period) {
                                    $selected = '';
                                    if ($this->entity->contractperiodcode == $period->code) {
                                        $selected = ' selected = "true" ';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option ' . $selected . '  value="' . $period->code . '">' . periodToOption($period->code, $period->timecode, $period->qty) . '</option>';
                                }
                                ?>
                            </select>                        
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="subscriptionprice">
                            <?php echo $_lang_register_label_subscriptionprice; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="subscriptionprice" name="subscriptionprice" class="form-control" value="<?php echo $this->entity->subscriptionprice; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="fixedpayment">
                            <?php echo $_lang_register_label_fixedpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="fixedpayment" name="fixedpayment" class="form-control" value="<?php echo $this->entity->fixedpayment; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="activationpayment">
                            <?php echo $_lang_register_label_activationpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="activationpayment" name="activationpayment" class="form-control" value="<?php echo $this->entity->activationpayment; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="suspensionpayment">
                            <?php echo $_lang_register_label_suspensionpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="suspensionpayment" name="suspensionpayment" class="form-control" value="<?php echo $this->entity->suspensionpayment; ?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="deactivationpayment">
                            <?php echo $_lang_register_label_deactivationpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="deactivationpayment" name="deactivationpayment" class="form-control" value="<?php echo $this->entity->deactivationpayment; ?>">
                        </div>
                    </div>                       
                    
                    <div class="form-group"  id="div_extensionpayment">
                        <label class="col-sm-6 control-label" for="extensionpayment">
                            <?php echo $_lang_register_label_extensionpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="extensionpayment" name="extensionpayment" class="form-control" value="<?php echo $this->entity->extensionpayment; ?>">
                        </div>
                    </div>                        
                    
                    
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->

            <div class="row">
                <div class="col-sm-12">


                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <input type="hidden"  id="idcrypt" name="idcrypt" value="<?php echo $this->entity->idcrypt; ?>">
                            <button type="submit" class="btn btn-primary ">
                                <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                            </button>

                        </div>
                    </div>


                </div>
            </div>            


        </form>
 




<!-- end: PAGE CONTENT-->
