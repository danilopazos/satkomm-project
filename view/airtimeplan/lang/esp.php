<?php
//--- View Index
$_lang_index_label_module = 'Planes';
$_lang_index_label_action = 'Lista'; 
$_lang_index_label_action_register = "Registrar";

$_lang_index_link_addairtimeplan = "Registrar plan de tiempo de aire";

$_lang_index_table_th_sku = "SKU";
$_lang_index_table_th_contractperiodcode = "Periodo de contrato";
$_lang_index_table_th_networkcode = "Red";
$_lang_index_table_th_servicetypecode= "Tipo";
$_lang_index_table_th_name = "Nombre";
$_lang_index_table_th_price = "Precio";

$_lang_index_table_th_subsprice = "Precio Subscripci&oacute;n";


//--- View Register
$_lang_register_block_accountinfo = "Informaci&oacute;n del plan";

$_lang_register_goback = "Volver";
$_lang_register_save = "Guardar nuevo";
$_lang_register_saveandnew = "Guardar y nuevo";

$_lang_register_label_formerror  ="Presentar algunos errores en el formulario, por favor revise";
$_lang_register_label_coderepeated  ="Otro plan presenta el mismo SKU, por favor revise.";
$_lang_register_label_processproblems  ="La presente operaci&oacute;n presenta problemas, por favor intente m&aacute;s tarde";
$_lang_register_label_processok  ="Exito, el plan fue registrado";

$_lang_register_block_airtimeplaninfo = "Informaci&oacute;n del plan";
$_lang_register_block_airtimeplandesc = "Descripci&oacute;n del plan";
$_lang_register_label_panel_productassoc =  "Equipos";


$_lang_register_label_sku = "Sku";
$_lang_register_label_networkcode = "Red";
$_lang_register_label_paymentperiodcode = "Periodo de pago";
$_lang_register_label_contractperiodcode = "Periodo de contrato";
$_lang_register_label_validityperiodcode = "Periodo de validez";
$_lang_register_label_servicetypecode = "Tipo de servicio";
$_lang_register_label_subscriptionprice = "Precio de subscripci&oacute;n USD";
$_lang_register_label_fixedpayment = "Cargos fijos USD";
$_lang_register_label_activationpayment = "Cargos por activaci&oacute;n USD";
$_lang_register_label_name = "Nombre";
$_lang_register_label_intro = "Intro";
$_lang_register_label_description = "Descripci&oacute;n";
$_lang_register_label_langcode = "Idioma";
$_lang_register_label_createdby = "Creado por";
$_lang_register_label_modifiedby = "Modificado por";

$_lang_register_label_select = "Seleccione";
$_lang_register_label_selectempty = "---";

$_lang_register_label_panel_esp = "Espa&ntilde;ol";
$_lang_register_label_panel_eng = "Ingl&eacute;s";


$_lang_register_label_panel_info = "Informaci&oacute;n del plan";
$_lang_register_label_panel_trafficsetup = "Configuraci&oacute;n del tr&aacute;fico";
$_lang_register_label_panel_trafficallowance = "Tr&aacute;fico incluido";

$_lang_trafficincrement_traffictype = "Tr&aacute;fico";
$_lang_trafficincrement_minimunduration = "Duraci&oacute;n m&iacute;nima";
$_lang_trafficincrement_increment = "Incremento";
$_lang_trafficincrement_add = "A&ntilde;adir incremento";

$_lang_register_block_trafficincrement = "Tr&aacute;fico (Incrementos)";
$_lang_register_block_trafficelement = "Tr&aacute;fico (Elementos)";
$_lang_register_block_trafficallowance = "Tr&aacute;fico (Incluido)";

$_lang_trafficelement_ttcode_source = "Origen";
$_lang_trafficelement_ttcode_dest = "Destino";
$_lang_trafficelement_tunitcode = "Unidades";
$_lang_trafficelement_chargecode = "Cargos";
$_lang_trafficelement_chargeammount = "Monto";
  
$_lang_trafficallowance_element = "Configuraci&oacute;n de tr&aacute;fico";
$_lang_trafficallowance_qty = "Qty";
 
$_lang_device_select = 'Seleccione el equipo';
$_lang_device_addproduct = 'A&ntilde;adir producto';

$_lang_lbl_deleteassocproduct = "Eliminar";


$_lang_lbl_sku = "Sku";
$_lang_lbl_productname = "Producto";


$_lang_register_block_productassoc_delete= "Eliminar asociaci&oacute;n";
$_lang_register_block_productassoc_msg = "Cuando se elimine la asociaci&oacute;n del producto, este plan no podr&aacute; ser asociado con el producto";
$_lang_view_label_close = "Cerrar";
$_lang_view_label_save = "Guardar";
$_lang_view_label_delete = "Eliminar";

$_lang_register_label_suspensionpayment = "Cargos por suspensi&oacute;n USD";
$_lang_register_label_deactivationpayment = "Cargos por desactivaci&oacute;n USD";

$_lang_register_label_panel_alert = "Alerta Tiempo de Aire";
$_lang_productalert_airtimeat50 = "Consumo al  50%";
$_lang_productalert_airtimeat75 = "Consumo al  75%";
$_lang_productalert_airtimeat90 = "Consumo al 90%";
$_lang_productalert_actionselect = "Seleccione la opci&ouacute;n a ejecutar";

$_lang_productalert_action_1_notifycustomer = "Notidicar al Cliente";
$_lang_productalert_action_2_notifycustomer = "Notificar al Cliente y al administrador del airtime";
$_lang_productalert_action_3_notifycustomer = "Suspender Simcard";

$_lang_productalert_setaction = "Aplicar acci&oacute;n";

$_lang_productalert_ActionAlertOk = "Configuraci&oacute;n de alerta guardada";

$_lang_trafficallowance_name = "Nombre";


$_lang_register_label_panel_simcard = "Simcard";


$_lang_index_table_simcards = "Simcards";
$_lang_index_table_th_sku = "Sku";
$_lang_index_table_th_serial = "Serial";
$_lang_index_table_th_productname = "Descripci&oacute;n";


$_lang_index_table_th_voicenumber = "Voz #";
$_lang_index_table_th_datanumber = "Datos #";
$_lang_index_table_th_status = "Status";
$_lang_index_table_th_customername = "Cliente";
$_lang_index_table_th_operationdate = "Operaci&oacute;n";


$_lang_view_label_customer = "Asignar al cliente";
$_lang_view_label_customer_all = "Todos los clientes";
$_lang_view_label_customerassigned = "Cliente asignado";
$_lang_view_label_customerassigned_all = "Todos los clientes";


$_lang_msg_selectaccount = "El plan ha crear solo ser&aacute; asignado al cliente seleccionado, posteriormente no podr&aacute; cambiar de cliente";
$_lang_view_label_accept = "Aceptar";

$_lang_register_label_version ="Version";
$_lang_register_label_aviable ="Disponible";
$_lang_register_label_noaviable ="No disponible";

$_lang_index_table_th_totalsimcards = "Total Sims";

$_lang_register_label_skuandversionproblems = "Sku y versi&oacute;n repetido";

$_lang_label_ttgroup = "Grupo de tipo de tr&aacute;fico";
//        tugarte@prochile.gob.cl

$_lang_register_label_extensionpayment = "Precio de extensi&oacute;n USD";


$_lang_recalculatetrafficsetup = "Recalcular tr&aacute;fico en planes hijos";

$_lang_recalculatetrafficsetup_recalculate = "Recalcular";
$_lang_recalculatetrafficsetup_cancel = "Cancelar";
$_lang_recalculatetrafficsetup_notify = "Notificar a los distribuidores la configuraci&oacute;n actualizada";
$_lang_recalculatetrafficsetup_text = "El recalcular el tr&aacute;fico actulizar&aacute; la configuraci&oacute;n de incrementos, tr&aacute;fico incluido y tr&aacute;fico adicional de planes hijos.";
$_lang_recalculatetrafficsetup_messageok = "El tr&aacute;fico fue recalculado";
$_lang_recalculatetrafficsetup_messageerror = "Tenemos problemas recalculando el tr&aacute;fico, por favor intente m&aacute;s tarde";