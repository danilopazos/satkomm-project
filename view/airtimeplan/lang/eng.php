<?php
//--- View Index
$_lang_index_label_module = 'Airtime Plans';
$_lang_index_label_action = 'List'; 
$_lang_index_label_action_register = "Register";

$_lang_index_link_addairtimeplan = "Register airtime plan";

$_lang_index_table_th_sku = "SKU";
$_lang_index_table_th_contractperiodcode = "Contract Period";
$_lang_index_table_th_networkcode = "Network";
$_lang_index_table_th_servicetypecode= "Type";
$_lang_index_table_th_name = "Name";
$_lang_index_table_th_price = "Price";

$_lang_index_table_th_subsprice = "Subscription Price";

//--- View Register
$_lang_register_block_accountinfo = "Airtime Plan info";
$_lang_register_goback = "Go back";
$_lang_register_save = "Save record";
$_lang_register_saveandnew = "Save and new";

$_lang_register_label_formerror  ="You have some form errors. Please check below.";
$_lang_register_label_coderepeated  ="Other Plan have the same SKU, please change it.";
$_lang_register_label_processproblems  ="The requested operation present problems, please try later";
$_lang_register_label_processok  ="Success, the artime was registered";

$_lang_register_block_airtimeplaninfo = "Airtime plan info";
$_lang_register_block_airtimeplandesc = "Airtime plan description";

$_lang_register_label_sku = "Sku";
$_lang_register_label_networkcode = "Network";
$_lang_register_label_paymentperiodcode = "Payment period";
$_lang_register_label_contractperiodcode = "Contract period";
$_lang_register_label_validityperiodcode = "Validity period";
$_lang_register_label_servicetypecode = "Service type";
$_lang_register_label_subscriptionprice = "Subscription price USD";
$_lang_register_label_fixedpayment = "Fixed payment USD";
$_lang_register_label_activationpayment = "Activation price USD";
$_lang_register_label_name = "Name";
$_lang_register_label_intro = "Intro";
$_lang_register_label_description = "Description";
$_lang_register_label_langcode = "Language";
$_lang_register_label_createdby = "Created by";
$_lang_register_label_modifiedby = "Modified by";

$_lang_register_label_select = "Select";
$_lang_register_label_selectempty = "---";

$_lang_register_label_panel_esp = "Spanish";
$_lang_register_label_panel_eng = "English";

$_lang_register_label_panel_info = "Plan info";
$_lang_register_label_panel_trafficsetup = "Traffic Setup";
$_lang_register_label_panel_trafficallowance =  "Traffic Allowance";
$_lang_register_label_panel_productassoc =  "Devices";


$_lang_trafficincrement_traffictype = "Traffic Type";
$_lang_trafficincrement_minimunduration = "Minimun duration";
$_lang_trafficincrement_increment = "Increment";
$_lang_trafficincrement_add = "Add Increment";

$_lang_register_block_trafficincrement = "Traffic Increment";
$_lang_register_block_trafficelement = "Traffic Elements";
$_lang_register_block_trafficallowance = "Traffic Allowance";


$_lang_trafficelement_ttcode_source = "Traffic Source";
$_lang_trafficelement_ttcode_dest = "Traffic Dest";
$_lang_trafficelement_tunitcode = "Unit";
$_lang_trafficelement_chargecode = "Charge";
$_lang_trafficelement_chargeammount = "Ammount";


$_lang_trafficallowance_element = "Traffic Configuration";
$_lang_trafficallowance_qty = "Qty";
 
$_lang_device_select = 'Select device';
$_lang_device_addproduct = 'Add Product';

$_lang_lbl_deleteassocproduct = "Delete";


$_lang_lbl_sku = "Sku";
$_lang_lbl_productname = "Product";

$_lang_register_block_productassoc_delete= "Delete product";
$_lang_register_block_productassoc_msg = "When a association is delete, this product won't be related with the actual plan";
$_lang_view_label_close = "Close";
$_lang_view_label_save = "Save";
$_lang_view_label_delete = "Delete";

$_lang_register_label_suspensionpayment = "Suspension charges USD";
$_lang_register_label_deactivationpayment = "Deactivation charges USD";


$_lang_register_label_panel_alert = "Airtime Alert";
$_lang_productalert_airtimeat50 = "Airtime at 50%";
$_lang_productalert_airtimeat75 = "Airtime at 75%";
$_lang_productalert_airtimeat90 = "Airtime at 90%";
$_lang_productalert_actionselect = "Select Action to execute";


$_lang_productalert_action_1_notifycustomer = "Notify Customer";
$_lang_productalert_action_2_notifycustomer = "Notify Customer and Airtime Admin";
$_lang_productalert_action_3_notifycustomer = "Suspend Simcard";

$_lang_productalert_setaction = "Apply Action";

$_lang_productalert_ActionAlertOk = "Alert configuration saved";

$_lang_trafficallowance_name = "Name";

$_lang_view_label_customer = "Asign to customer";
$_lang_view_label_customer_all = "All customers";
$_lang_view_label_customerassigned = "Customer assigned";
$_lang_view_label_customerassigned_all = "All customer";

$_lang_msg_selectaccount = "The plan created will be asigned only to the  selected customer, later will not be able to change the client";
$_lang_view_label_accept = "Accept";

$_lang_register_label_version ="Version";
$_lang_register_label_aviable ="Available";
$_lang_register_label_noaviable ="Not available";

$_lang_index_table_th_totalsimcards = "Total Sims";

$_lang_register_label_skuandversionproblems = "Sku and version repeated";

$_lang_label_ttgroup = "Traffic Type group";

$_lang_register_label_extensionpayment = "Extensi&oacute;n price USD";

$_lang_recalculatetrafficsetup = "Recalculate traffic in child plans";
$_lang_recalculatetrafficsetup_recalculate = "Recalculate";
$_lang_recalculatetrafficsetup_cancel = "Cancel";
$_lang_recalculatetrafficsetup_notify = "Notify to dealers the configuration updated";
$_lang_recalculatetrafficsetup_text = "The traffic recalculation updates the increments configuration, allowance traffic and traffic setup on child plans.";
$_lang_recalculatetrafficsetup_messageok = "The traffic was recalculated";
$_lang_recalculatetrafficsetup_messageerror = "We have some problems recalculating the traffic, please try later";