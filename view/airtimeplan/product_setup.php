<script>
    var _productsTable = null;
</script>

<div class="row">
    <div class="col-sm-12">
 
        <div class="panel panel-default">
 
            <div class="panel-body  form-horizontal">  
 
                <form role="form" action="#" method="post" id="form-productassoc"  >    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="productid">
                            <?php echo $_lang_device_select; ?>
                        </label>
                        <div class="col-sm-8">
                            <select name="productid" id="assoc_productid" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php
                                foreach ($this->aCatalog as $product) {

                                    echo '<option   value="' . $product->idcrypt . '">' . $product->name . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-2"> 
                            <input type="hidden" name="operation" value="<?php echo $this->entity->servicetypecode; ?>" />
                            <input type="hidden" name="planid" id="productassocplanid" value="<?php echo $this->entity->idcrypt; ?>" />
                            <input type="button" id="btn_productassoc_add" class="btn   btn-primary" value="<?php echo $_lang_device_addproduct; ?>" />
                        </div>

                    </div>
                </form>


            </div>
        </div>
        
    </div>
</div>

<div class="row">
    <div class="col-sm-12">

        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width" id="productTable">
                <thead>
                    <tr>
                        <th width="10%"><?php echo $_lang_index_table_th_sku; ?></th>
                        <th width="80%"><?php echo $_lang_index_table_th_name; ?></th>
                        <th width="10%"></th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="10%"> - </td>
                        <td  width="80%"> - </td>
                        <td width="10%"> - </td>
                    </tr>
                </tbody>
            </table>        
        </div>
        <?php
        ?>  
    </div>
</div>

<div class="modal fade" id="product-delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">


            <form role="form" class="form-product-delete" id="form-product-delete"  >

                <div class="modal-body">
  
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: TEXT FIELDS PANEL -->
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_productassoc_delete; ?>
                                    <div class="panel-tools">

                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>

                                    </div>
                                </div>

                                <div class="panel-body  form-horizontal">  

                                    <div  class="alert alert-warning  ">
                                        <i class="fa fa-remove-sign"></i><?php echo $_lang_register_block_productassoc_msg; ?>
                                    </div>
                                    
                                    <input type="hidden" name="productid" id="productassoc_productid" value="0"  />

                                </div>

                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $_lang_view_label_close; ?>
                    </button>
                    <button type="button" class="btn btn-primary" id="btn_productassoc_delete">
                        <?php echo $_lang_view_label_delete; ?>
                    </button>
                </div>

            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>