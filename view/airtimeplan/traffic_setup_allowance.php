 
<form role="form" class="form-register   box-register" id="form-trafficallowance"  >

    <div id="errorSetupAllowanceProblems" class="alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
    </div>

    <div id="messageSetupAllowanceOk" class="alert alert-success no-display">
        <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
    </div>            

    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">


        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_trafficallowance; ?>
            <div class="panel-tools">

                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>

            </div>
        </div>


        <div class="panel-body  form-horizontal">  

            <table class="table table-bordered table-responsive" id="table_allowance">
                <thead>
                <th width="20%"><?php echo $_lang_trafficallowance_name; ?></th>
                <th width="60%"><?php echo $_lang_trafficallowance_element; ?></th>
                <th width="10%"><?php echo $_lang_trafficallowance_qty; ?></th>
                <th width="10%"><a href="#" id="btn_addtrafficallowance" class="btn btn-xs btn-primary"><i class="fa fa-plus-circle"></i></a></th>
                </thead>
                <tbody>
                    <?php
                    $totalTrafficAllowance =  $this->trafficallowance_total ;
                    $_tr = '';
                    for ($i = 0; $i < $totalTrafficAllowance; $i++) {
                        
                        $entity = $this->aTrafficAllowance[$i];
                        $aSetupSelected = $entity->aTrafficSetup;
                        
                         
                        $controls = '<input type="hidden" name="ts_deleted_' . $i . '" id="ts_deleted_' . $i . '" value="0" />';
                        $controls .= '<a href="#" onclick="fnc_deleteallowance(\'' . $i . '\')" class="btn btn-xs btn-bricky"><i class="fa fa-trash"></i></a>';

                        $aTrafficSetup = '<select multiple="multiple" name="trafficsetupid_' . $i . '[]"  id="trafficsetupid_' . $i . '"  class="form-control">';
                        $total = count($this->aTrafficElement);
                        for ($x = 0; $x < $total; $x++) {

                            $e = $this->aTrafficElement[$x];
                            
                            $label_trafficsetup = $e->ttcode_source_description;
                            /*
                            if($e->ttcode_dest_description != ""){
                                $label_trafficsetup .=" - ".$e->ttcode_dest_description;
                            }
                             */
                            $label_trafficsetup .= '( '.$e->tunitcode.' )';
                            
                            
                            $selected = '';
                            foreach($aSetupSelected as $trafficselected){
                                if($e->ttcode_source == $trafficselected->ttcode_source &&
                                   $e->ttcode_dest == $trafficselected->ttcode_dest     &&
                                   $e->tunitcode == $trafficselected->tunitcode          
                                        ){ 
                                    $selected =  ' selected = "true" '; 
                                }
                            }
                            
                            
                            $aTrafficSetup .= '<option '.$selected.' value="' . $e->id . '">' . $label_trafficsetup . '</option>';
                        }
                        $aTrafficSetup .= '</select>';
 
                        $text_name = '<input value = "'.$entity->tname.'"  type="text" name="ts_allowancename_' . $i . '" class="form-control"   > ';
                        
                        
                        $text_qty = '<input value="'.$entity->qty.'" type="text" name="qty_' . $i . '"   id="qty_' . $i . '" class="form-control inc_unitqty" />';

                        $_tr .= '<tr id="ts_tr_' . $i . '">';
                        
                        $_tr .= '<td>' . $text_name . '</td>';
                        $_tr .= '<td>' . $aTrafficSetup . '</td>';
                        $_tr .= '<td>' . $text_qty . '</td>';

                        $_tr .= '<td>' . $controls . '</td>';
                        $_tr .= '</tr>';
                    }

                    echo $_tr;
                    ?>
                </tbody>
            </table>
            <input type="hidden"  id="tsallowance_planidcrypt" name="planidcrypt" value="<?php echo $this->entity->idcrypt; ?>">
            <input type="hidden"  id="tsallowance_total" name="tsallowance_total" value="<?php echo $totalTrafficAllowance; ?>">            

            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-footer">

                            <button type="button" class="btn btn-primary " id="btn_register_trafficallowance">
                                <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                            </button>

                        </div>
                    </div>
                </div>
            </div>               
            
            
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->

</form>