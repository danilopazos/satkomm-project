<div class="tabbable">
    <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#panel_eng">
                <?php echo $_lang_register_label_panel_eng; ?>
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#panel_esp">
                <?php echo $_lang_register_label_panel_esp; ?>
            </a>
        </li>
    </ul>


    <div class="tab-content">

        <div id="panel_eng" class="tab-pane in active">

            <form role="form" class="form-eng   box-eng" id="formeng"  >
                <div class="errorHandlerEng  alert alert-danger no-display">
                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                </div>

                <div id="errorProcessEngProblems" class="alert alert-danger no-display">
                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
                </div>

                <div id="messageProcessEngOk" class="alert alert-success no-display">
                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
                </div>   

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_airtimeplaninfo; ?>
                        <div class="panel-tools">
                            <a class="btn btn-xs btn-link panel-expand" href="#">
                                <i class="fa fa-resize-full"></i>
                            </a>
                        </div>
                    </div>


                    <div class="panel-body  form-horizontal">  

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">
                                <?php echo $_lang_register_label_name; ?>
                            </label>
                            <div class="col-sm-9">
                                <input type="text"   name="name" class="form-control" value="<?php echo $this->entity->name; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="intro">
                                <?php echo $_lang_register_label_intro; ?>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="intro" class="form-control"><?php echo $this->entity->intro; ?></textarea>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="description">
                                <?php echo $_lang_register_label_description; ?>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control"><?php echo $this->entity->description; ?></textarea>
                            </div>
                        </div>                        

                    </div>


                </div>    
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-footer">
                                <input type="hidden"  name="langcode" value="ENG">
                                <input type="hidden"  name="idcrypt" value="<?php echo $this->entity->idcrypt; ?>">
                                <button type="submit" class="btn btn-primary ">
                                    <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                                </button>

                            </div>
                        </div>
                    </div>
                </div>     


            </form>

        </div>

        <div id="panel_esp" class="tab-pane">



            <form role="form" class="form-esp   box-esp" id="formesp"  >
                
                <div class="errorHandlerEsp  alert alert-danger no-display">
                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
                </div>

                <div id="errorProcessEspProblems" class="alert alert-danger no-display">
                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
                </div>

                <div id="messageProcessEspOk" class="alert alert-success no-display">
                    <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
                </div>   

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_airtimeplaninfo; ?>
                        <div class="panel-tools">
                            <a class="btn btn-xs btn-link panel-expand" href="#">
                                <i class="fa fa-resize-full"></i>
                            </a>
                        </div>
                    </div>


                    <div class="panel-body  form-horizontal">  

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">
                                <?php echo $_lang_register_label_name; ?>
                            </label>
                            <div class="col-sm-9">
                                <input type="text"   name="name" class="form-control" value="<?php echo $this->entity_esp->name; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="intro">
                                <?php echo $_lang_register_label_intro; ?>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="intro" class="form-control"><?php echo $this->entity_esp->intro; ?></textarea>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="description">
                                <?php echo $_lang_register_label_description; ?>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control"><?php echo $this->entity_esp->description; ?></textarea>
                            </div>
                        </div>                        

                    </div>


                </div>    
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-footer">
                                <input type="hidden"  name="langcode" value="ESP">
                                <input type="hidden"  name="idcrypt" value="<?php echo $this->entity->idcrypt; ?>">
                                <button type="submit" class="btn btn-primary ">
                                    <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                                </button>

                            </div>
                        </div>
                    </div>
                </div>     


            </form>            



        </div>


    </div>

</div>