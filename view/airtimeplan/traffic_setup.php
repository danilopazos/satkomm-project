<script type="text/javascript">

<?php
$totalTrafficType = count($this->aTrafficType);
$record = "";
for ($i = 0; $i < $totalTrafficType; $i++) {
    $e = $this->aTrafficType[$i];
    $record .= '{ code :"' . trim($e->code) . '", description :"' . trim($e->description) . '"}';
    if (($i + 1) < $totalTrafficType) {
        $record .= ",";
    }
}
$record;
?>
    var _traffictype = [<?php echo $record; ?>];


<?php
$totalUnit = count($this->aTrafficUnit);
$record = "";
for ($i = 0; $i < $totalUnit; $i++) {
    $e = $this->aTrafficUnit[$i];
    $record .= '{ code :"' . trim($e->code) . '", description :"' . trim($e->description) . '"}';
    if (($i + 1) < $totalUnit) {
        $record .= ",";
    }
}
?>
    var _trafficunit = [<?php echo $record; ?>];


<?php
$totalCharge = count($this->aCharge);
$record = "";
for ($i = 0; $i < $totalCharge; $i++) {
    $e = $this->aCharge[$i];
    $record .= '{ code :"' . trim($e->code) . '", description :"' . trim($e->name) . '"}';
    if (($i + 1) < $totalCharge) {
        $record .= ",";
    }
}
?>
    var _charge = [<?php echo $record; ?>];


<?php
$record = "";
if ($this->aTrafficElement) {
    $totalElement = count($this->aTrafficElement);

    for ($i = 0; $i < $totalElement; $i++) {
        $e = $this->aTrafficElement[$i];

        $label_trafficsetup = $e->ttcode_source_description;
        /*
          if($e->ttcode_dest_description != ""){
          $label_trafficsetup .=" - ".$e->ttcode_dest_description;
          }
         */

        $label_trafficsetup .= '( ' . $e->tunitcode . ' )';


        $record .= '{ id :"' . trim($e->id) . '", description :"' . $label_trafficsetup . '"}';
        if (($i + 1) < $totalElement) {
            $record .= ",";
        }
    }
}
?>
    var _trafficsetup = [<?php echo $record; ?>];

    var _totalincrementrows = <?php echo $this->trafficincrement_total; ?>;
    var _totalelementrows = <?php echo $this->trafficelement_total; ?>;
    var _totalallowancerows = <?php echo $this->trafficallowance_total; ?>;
</script>

<div class="row">


    <div class="col-sm-6">
        <div class="panel panel-default">


            <div class="panel-body  form-horizontal">
                <div class="form-group">
                    <label class="col-sm-6 control-label"  >
                        <?php echo '<strong>' . $_lang_label_ttgroup . '</strong>: ' . $this->entity->ttgroup; ?>             
                    </label>
                </div>                
            </div>


        </div>
    </div>

    <div class="col-sm-6">
        <div class="panel panel-default">


            <div class="panel-body  form-horizontal">
                <div class="form-group">
                    <label class="col-sm-6 control-label"  >
                        <?php echo '<a href="#"  class="btn btn-primary" data-toggle="modal" data-target="#modalRecalculateConfirmation">' . $_lang_recalculatetrafficsetup . '</a>'; ?>
                    </label>
                </div>                
            </div>


        </div>
    </div>
    
    
    <!-- Modal -->
    <div id="modalRecalculateConfirmation" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?php echo $_lang_recalculatetrafficsetup; ?></h4>
          </div>
          <div class="modal-body">
              
            <div class="alert alert-success hidden" id="messageRecalculateOk" >
              <?php echo $_lang_recalculatetrafficsetup_messageok; ?>
            </div>

            <div class="alert alert-danger hidden" id="messageRecalculateProblems">
              <?php echo $_lang_recalculatetrafficsetup_messageerror; ?>
            </div>
              
              
              
            <p><?php echo $_lang_recalculatetrafficsetup_text; ?></p>
            <p><input type="checkbox" id="checkbox_notifyrecalculation" checked="true"> <?php echo $_lang_recalculatetrafficsetup_notify; ?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnRecalculateChildPlans" ><?php echo $_lang_recalculatetrafficsetup_recalculate; ?></button>  
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $_lang_recalculatetrafficsetup_cancel; ?></button>
          </div>
        </div>

      </div>
    </div>

    
    
    
    
    <div class="col-sm-12">
        <?php require_once 'traffic_setup_increment.php'; ?>
    </div>
    <div class="col-sm-12">
        <?php require_once 'traffic_setup_element.php'; ?>
    </div>


</div>