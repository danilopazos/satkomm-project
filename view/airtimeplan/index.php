
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-plane"></i>
                <a href="/airtimeplan/index">
                    <?php echo $_lang_index_label_module;  ?>
                </a>
            </li>
            <li class="active">
                    <?php echo $_lang_index_label_action;  ?>
            </li>
            <li class="search-box">
               <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
                <a class="btn btn-xs btn-primary" href="/airtimeplan/register">
                    <i class="fa fa-plus-circle"></i> <?php echo $_lang_index_link_addairtimeplan; ?>
                </a>
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><?php echo $_lang_index_table_title;  ?>
                <div class="panel-tools">
                     <a class="btn btn-xs btn-link panel-expand" href="#"> <i class="fa fa-resize-full"></i> </a> 
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="mainTable">
                    <thead>
                        <tr>
                            <th  width="5%"  class="hidden-xs"><?php echo $_lang_index_table_th_sku;  ?></th>
                            <th  width="5%"  class="hidden-xs"><?php echo $_lang_index_table_th_networkcode;  ?></th>
                            <th  width="5%"><?php echo $_lang_index_table_th_servicetypecode;  ?> </th>
                            <th ><?php echo $_lang_index_table_th_name;  ?></th>
                            <th ><?php echo $_lang_view_label_customerassigned;  ?></th>
                            <th  width="5%"class="hidden-xs"><?php echo $_lang_index_table_th_contractperiodcode;  ?></th>
                            <th  width="5%"class="hidden-xs"><?php echo $_lang_register_label_paymentperiodcode;  ?></th>
                            <th  width="5%"class="hidden-xs"><?php echo $_lang_index_table_th_subsprice;  ?></th>
                            <th  width="5%"class="hidden-xs"><?php echo $_lang_register_label_activationpayment;  ?></th>
                            
                            <th  width="5%"class="hidden-xs"><?php echo $_lang_index_table_th_totalsimcards;  ?></th>
                            
                            <th  width="5%"class="hidden-xs"><?php echo $_lang_register_label_version;  ?></th>
                            <th  width="5%"class="hidden-xs"><?php echo $_lang_register_label_aviable;  ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                            <td class="hidden-xs"> - </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
        
    </div>
</div>
<!-- end: PAGE CONTENT-->