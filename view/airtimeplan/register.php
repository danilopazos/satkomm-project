<script type="text/javascript">
    var _label_contractperiodcode = '<?php echo $_lang_register_label_contractperiodcode; ?>';
    var _label_validityperiodcode = '<?php echo $_lang_register_label_validityperiodcode; ?>';
</script>


<!-- register update -->
<div class="modal fade" id="alert-selectaccount" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                 <div  class="alert alert-danger ">
                    <i class="fa fa-remove-sign"></i><?php echo $_lang_msg_selectaccount; ?>
                </div> 
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?php echo $_lang_view_label_accept; ?>
                </button>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-plane"></i>
                <a href="/airtimeplan/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_register; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
                echo '<a class="btn btn-xs btn-default" href="/airtimeplan/index">';
                    echo '<i class="fa fa-arrow-circle-left"></i>'.$_lang_register_goback;
                echo '</a>';
            ?>
            
        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<form role="form" class="form-register   box-register" id="form"  >
     
    <div class="row">
        <div class="col-sm-6">
            
            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_formerror; ?>
            </div>
            <div id="errorCodeRepeated" class="alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_coderepeated; ?>
            </div>
            <div id="errorProcessProblems" class="alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processproblems; ?>
            </div>

            <div id="messageProcessOk" class="alert alert-success no-display">
                <i class="fa fa-remove-sign"></i> <?php echo $_lang_register_label_processok; ?>
            </div>            
            
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">


                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> <?php echo $_lang_register_block_airtimeplaninfo; ?>
                    <div class="panel-tools">

                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>

                    </div>
                </div>


                <div class="panel-body  form-horizontal">  

                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="customeraccountid">
                            <?php echo $_lang_view_label_customer; ?>
                        </label>
                        <div class="col-sm-6">
                            
                        <select name="customeraccountid" id="customeraccountid" class="form-control">
                            <option value="0">--- <?php echo $_lang_view_label_customer_all; ?> ---</option>
                            <?php
                            $_totalcustomer = intval($this->aCustomer);
                            if ($_totalcustomer > 0) {

                                foreach ($this->aCustomer as $customer) {

                                    echo '<option value="' . $customer->id . '">' . $customer->name . '</option>';
                                }
                            }
                            ?>
                        </select>
                            
                        </div>
                    </div>                    
                    
                    

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="sku">
                            <?php echo $_lang_register_label_sku; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="sku" name="sku" class="form-control" value="<?php echo $this->entity->sku; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="networkcode">
                            <?php echo $_lang_register_label_networkcode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="networkcode" id="networkcode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php 
                                foreach($this->aNetwork as $network){
                                    echo '<option value="'.$network->code.'">'.$network->description.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- aTrafficTypeGroup -->
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="networkcode">
                            <?php echo $_lang_label_ttgroup; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="ttgroup" id="ttgroup" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php 
                                foreach($this->aTrafficTypeGroup as $ttgroup){
                                    echo '<option value="'.$ttgroup.'">'.$ttgroup.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="servicetypecode">
                            <?php echo $_lang_register_label_servicetypecode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="servicetypecode" id="servicetypecode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php 
                                foreach($this->aServiceType as $serviceType){
                                    echo '<option value="'.$serviceType->code.'">'.$serviceType->code.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>                    
                    
                    <div class="form-group" id="div_paymentperiodcode">
                        <label class="col-sm-6 control-label" for="paymentperiodcode">
                            <?php echo $_lang_register_label_paymentperiodcode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="paymentperiodcode" id="paymentperiodcode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php 
                                foreach($this->aPeriod as $period){
                                    echo '<option value="'.$period->code.'">'.periodToOption($period->code, $period->timecode, $period->qty).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="contractperiodcode" id="lbl_contractperiodcode">
                            <?php echo $_lang_register_label_contractperiodcode; ?>
                        </label>
                        <div class="col-sm-6">
                            <select name="contractperiodcode" id="contractperiodcode" class="form-control">
                                <option value=""><?php echo $_lang_register_label_select; ?></option>
                                <?php 
                                foreach($this->aPeriod as $period){
                                    echo '<option value="'.$period->code.'">'.periodToOption($period->code, $period->timecode, $period->qty).'</option>';
                                }
                                ?>
                            </select>                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="subscriptionprice">
                            <?php echo $_lang_register_label_subscriptionprice; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="subscriptionprice" name="subscriptionprice" class="form-control" value="<?php echo $this->entity->subscriptionprice; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="fixedpayment">
                            <?php echo $_lang_register_label_fixedpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="fixedpayment" name="fixedpayment" class="form-control" value="<?php echo $this->entity->fixedpayment; ?>">
                        </div>
                    </div>
                    
 
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="activationpayment">
                            <?php echo $_lang_register_label_activationpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="activationpayment" name="activationpayment" class="form-control" value="<?php echo $this->entity->activationpayment; ?>">
                        </div>
                    </div>                    
                    
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="suspensionpayment">
                            <?php echo $_lang_register_label_suspensionpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="suspensionpayment" name="suspensionpayment" class="form-control" value="<?php echo $this->entity->suspensionpayment; ?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="deactivationpayment">
                            <?php echo $_lang_register_label_deactivationpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="deactivationpayment" name="deactivationpayment" class="form-control" value="<?php echo $this->entity->deactivationpayment; ?>">
                        </div>
                    </div>                    
                    
                    
                    <div class="form-group" id="div_extensionpayment">
                        <label class="col-sm-6 control-label" for="extensionpayment">
                            <?php echo $_lang_register_label_extensionpayment; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="extensionpayment" name="extensionpayment" class="form-control" value="<?php echo $this->entity->extensionpayment; ?>">
                        </div>
                    </div>                     
                    
                    
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" for="name">
                            <?php echo $_lang_register_label_name; ?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="name" name="name" class="form-control" value="<?php echo $this->entity->name; ?>">
                        </div>
                    </div>

                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>

     

    <div class="row">
        <div class="col-sm-6">


            <div class="panel panel-default">
                <div class="panel-footer">

                        <button type="submit" class="btn btn-primary ">
                            <i class="fa fa-save"></i>&nbsp;<?php echo $_lang_register_save; ?> 
                        </button>
                    
                </div>
            </div>


        </div>
    </div>
    
    <?php
    if($this->id != ""){
        echo '<input type="hidden" name="id" id="accountid" value="'.$this->id.'" />';
    }
    ?>

</form>
<!-- end: PAGE CONTENT-->
