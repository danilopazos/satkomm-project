<script type="text/javascript">
    var _label_contractperiodcode = '<?php echo $_lang_register_label_contractperiodcode; ?>';
    var _label_validityperiodcode = '<?php echo $_lang_register_label_validityperiodcode; ?>';
</script>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">

        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-plane"></i>
                <a href="/airtimeplan/index">
                    <?php echo $_lang_index_label_module; ?>
                </a>
            </li>
            <li class="active">
                <?php echo $_lang_index_label_action_register; ?>
            </li>
            <li class="search-box">
                <?php $this->loadwidget("sidebarsearch"); ?>
            </li>
        </ol>

        <div class="page-header">
               <!--<h1>Accounts <small>list</small></h1>-->
            <?php
            echo '<a class="btn btn-xs btn-default" href="/airtimeplan/index">';
            echo '<i class="fa fa-arrow-circle-left"></i>' . $_lang_register_goback;
            echo '</a>';
            ?>

        </div>

        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->


<div class="tabbable">
    <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#panel_info">
                <?php echo $_lang_register_label_panel_info; ?>
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#panel_trafficsetup">
                <?php echo $_lang_register_label_panel_trafficsetup; ?>
            </a>
        </li>
        
        <?php if($this->trafficelement_total > 0){ ?>
        <li>
            <a data-toggle="tab" href="#panel_trafficallowance">
                <?php echo $_lang_register_label_panel_trafficallowance; ?>
            </a>
        </li>        
        <?php } ?>     
        
        <li>
            <a data-toggle="tab" href="#panel_product">
                <?php echo $_lang_register_label_panel_productassoc; ?>
            </a>
        </li>
       
        <li>
            <a data-toggle="tab" href="#panel_simcards">
                Simcards
            </a>
        </li>
        
        
    </ul>
    <div class="tab-content">

        <div id="panel_info" class="tab-pane in active">
            <?php require_once 'update_info.php'; ?>
        </div>

        <div id="panel_trafficsetup" class="tab-pane">
            <?php require_once 'traffic_setup.php'; ?>
        </div>
        
        <?php if($this->trafficelement_total > 0){ ?>
        <div id="panel_trafficallowance" class="tab-pane">
           <?php require_once 'traffic_setup_allowance.php'; ?>
        </div>
        <?php } ?>        
        
        <div id="panel_product" class="tab-pane">
            <?php require_once 'product_setup.php'; ?>
        </div>
        
       
        <div id="panel_simcards" class="tab-pane">
             
            
            <table class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%" id="mainSimcardTable">
                <thead>
                    <tr>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_serial; ?></th>
                        
                        <th ><?php echo $_lang_index_table_th_voicenumber; ?></th>
                        <th ><?php echo $_lang_index_table_th_datanumber; ?></th>
                        <th ><?php echo $_lang_index_table_th_sku; ?></th>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_productname; ?> </th>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_status; ?> </th>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_customername; ?> </th>
                        <th  class="hidden-xs"><?php echo $_lang_index_table_th_operationdate; ?> </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td  class="hidden-xs"> - </td>
                        <td> - </td>
                        <td  class="hidden-xs"> - </td>
                        <td> - </td>
                         <td> - </td>
                        <td  class="hidden-xs"> - </td>
                        <td> - </td>
                        <td  class="hidden-xs"> - </td>
                    </tr>
                </tbody>
            </table>            
            
            
        </div>
        
        
    </div>
    
</div>      


<!-- end: PAGE CONTENT-->
