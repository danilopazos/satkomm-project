<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    //
    public function scopeGetList($query)
    {
        return $query->select('id as code', 'description')
            ->get();
    }
}
