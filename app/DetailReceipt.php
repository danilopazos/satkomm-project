<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailReceipt extends Model
{
    //
    protected $table="detailreceipts";

public function scopeNextCode($query, $receiptId)
{
    $maxCode = $query->select('id')
                ->where('receipt_id', $receiptId)
                ->orderBy('id', 'DESC')
                ->limit(1)
                ->first();
    if(isset($maxCode->id)){
        $newCode = $maxCode->id + 1;
    }else{
        $newCode = 1;
    }
    return ($newCode);
}

public function scopeGetListDetail($query, $receiptId)
{
    return $query->select('detailreceipts.order_id', 'detailreceipts.sancode', 'detailreceipts.date_start', 'detailreceipts.date_end', 'detailreceipts.typeplan', 'detailreceipts.description', 
                            'detailreceipts.daystopay', 'detailreceipts.usd_price', 'address.addressname', 'districts.name as district', 'provinces.name as province', 'departments.name as department')
                ->join('ordservices', 'detailreceipts.order_id', '=', 'ordservices.id')
                ->join('address', 'ordservices.address_id', '=', 'address.id')
                ->join('districts', 'address.district_id', '=', 'districts.id')
                ->join('provinces', 'districts.province_id', '=', 'provinces.id')
                ->join('departments', 'provinces.department_id', '=', 'departments.id')
                ->where([
                    ['detailreceipts.receipt_id', $receiptId],
                    ['detailreceipts.status', true]
                ])
                ->get();
    }

    public function scopeGetEntryAndExits($query, $orderId)
    {
        return $query->select('consumptionreceipts.period', 'typeplan', 'usd_price', 'daystopay', 'usd_cost', 'commode_cost')
            ->join('consumptionreceipts', 'detailreceipts.receipt_id', '=', 'consumptionreceipts.id')
            ->where([
                ['detailreceipts.status', true],
                ['consumptionreceipts.status', true],
                ['detailreceipts.order_id', $orderId]
            ])
            ->get();
    }

}
