<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeInvoice extends Model
{
    //
    protected $table = "typeinvoices";

    public function scopeListDocuments($query){

        return $query->select('id as code', 'description')->get();

    }
}
