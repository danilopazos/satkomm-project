<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderService extends Model
{
    //
    protected $table = "ordservices";
    protected $dates = ['date_start', 'date_due', 'date_end'];

    public function scopeGetMinPeriod($query, $companyId)
    {
        return $query->select('date_start')
            ->where([
                ['provider_id', $companyId],
                ['status', true]
            ])
            ->orderBy('created_at', 'ASC')
            ->limit(1)
            ->first();
    }

    public function scopeValidateCode($query, $orderId)
    {
        return $query->where('id', '=', $orderId)->exists();
    }

    public function scopeListOrderServices($query, $providerId)
    {
        return $query->select('ordservices.id as code', 'typeservices.id as typeservice_id', 'typeservices.description as typeservice', 'sancode',
                                'companies.name as customer', 'date_start', 'ordservices.date_end',
                                'ordstatus.id as status_id', 'ordstatus.description as status', 'ordstatus.stylealert', 'ordstatus.iconalert',
                                'address.addressname', 'districts.name as district', 'provinces.name as province', 'departments.name as department', 'ordservices.created_at')
            ->join('companies', 'ordservices.customer_id', '=', 'companies.id')
            ->join('typeservices', 'ordservices.typeservice_id', '=', 'typeservices.id')
            ->join('ordstatus', 'ordservices.ordstatus_id', '=', 'ordstatus.id')
            ->join('address', 'ordservices.address_id', '=', 'address.id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->where([
                ['ordservices.provider_id', '=', $providerId],
                ['ordservices.status', '=', true]
            ])
            ->get();
    }

    public function scopeGetInfoOrder($query, $orderId)
    {
        return $query->select('ordservices.id as code', 'ordservices.created_at', 'ordservices.signed_order_attach as attach', 'typedocuments.description as typedocument', 'companies.id as companycode',
                                'companies.taxnumber as companyid', 'companies.name as companyname', 'address.addressname', 'address.address', 'address.coordinates',
                                'districts.name as district', 'provinces.name as province', 'departments.name as department', 'countries.name as country',
                                'companies_customers.daystodue as creditdays', 'typesofbilling.description as typeofbilling', 'ordservices.anotations as ordernotes', 'ordservices.lastorderservice_id',
                                'typeservices.description as typeservice',
                                'ordservices.usdprice', 'ordservices.date_start', 'ordservices.date_due', 'ordservices.date_end', 'ordservices.contractmonths', 'ordservices.sancode', 
                                'ordservices.admin_validation', 'companies_customers.typecustomer_id', 
                                'ordstatus.description as statusText', 'ordstatus.stylealert as statusStyle', 'ordstatus.iconalert as statusIcon')
            ->join('ordstatus', 'ordservices.ordstatus_id', '=', 'ordstatus.id')
            ->join('companies', 'ordservices.customer_id', '=', 'companies.id')
            ->join('companies_customers', 'ordservices.customer_id', '=', 'companies_customers.customer_id')
            ->join('typedocuments', 'companies.typedocument_id', '=', 'typedocuments.id')
            ->join('address', 'ordservices.address_id', '=', 'address.id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('countries', 'departments.country_id', '=', 'countries.id')
            ->join('typesofbilling', 'ordservices.typebilling_id', '=', 'typesofbilling.id')
            ->join('typeservices', 'ordservices.typeservice_id', '=', 'typeservices.id')
            ->where([
                ['ordservices.status', true],
                ['ordservices.id', $orderId]
            ])
            ->firstOrFail();
    }

    public function scopeGetPstAdi_ActivePerPeriod($query, $companyId, $dateEnd, $excludeOrdersId)
    {
        return $query->select('ordservices.id as ordercode', 'ordservices.customer_id')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->join('typeplans', 'vsatplans.typeplan_id', '=', 'typeplans.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.typeservice_id', 'AC'],
                ['ordservices.date_start','<=', $dateEnd]
            ])
            ->whereIn('typeplans.id', array('PS', 'AD'))
            ->whereNotIn('ordservices.ordstatus_id', array('S1', 'S4')) // "pendiente" y "cancelado"
            ->whereNotIn('ordservices.id', $excludeOrdersId)
            ->groupBy('ordservices.id')
            ->get();
    }

    public function scopeGetPstAdi_CancelPerPeriod($query, $companyId, $dateStart, $dateEnd, $excludeOrdersId){
        return $query->select('ordservices.id as ordercode', 'ordservices.customer_id')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->join('typeplans', 'vsatplans.typeplan_id', '=', 'typeplans.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.typeservice_id', 'AC'],
                ['ordservices.ordstatus_id', 'S4'],
                ['ordservices.date_start','<=', $dateEnd],
                ['ordservices.date_end','>=', $dateStart]
            ])
            ->whereIn('typeplans.id', array('PS', 'AD'))
            ->whereNotIn('ordservices.id', $excludeOrdersId)
            ->groupBy('ordservices.id')
            ->get();
    }

    public function scopeGetTkn_PerPeriod($query, $companyId, $dateStart, $dateEnd, $excludeOrdersId)
    {
        return $query->select('ordservices.id as ordercode', 'ordservices.customer_id')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->join('typeplans', 'vsatplans.typeplan_id', '=', 'typeplans.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.typeservice_id', 'AC'],
                ['ordservices.date_start','<=', $dateEnd],
                ['ordservices.date_start','>=', $dateStart],
                ['typeplans.id', 'TK'],
                ['ordservices.ordstatus_id', 'S2']  
            ])
            ->whereNotIn('ordservices.id', $excludeOrdersId)
            ->groupBy('ordservices.id')
            ->get();
    }

    public function scopeGetInfoOrderToBill($query, $ordersId, $providerId)
    {
        return $query->select('ordservices.id as ordercode', 'companies.id as companyCode', 'companies.name as company', 'address.id as addressid','address.addressname', 'ordservices.sancode', 
                                'ordservices.date_start', 'ordservices.date_end',
                                'districts.name as district', 'provinces.name as province', 'departments.name as department',
                                'ordservices.ordstatus_id', 'ordstatus.description as statusname', 'ordstatus.stylealert', 'ordstatus.iconalert'
                                )
            ->join('companies', 'ordservices.customer_id', '=', 'companies.id')
            ->join('address', 'ordservices.address_id', '=', 'address.id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('ordstatus', 'ordservices.ordstatus_id', '=', 'ordstatus.id')
            ->where([
                ['ordservices.provider_id', $providerId]
            ])
            ->whereIn('ordservices.id', $ordersId)
            ->get();
    }
    //posible a elminarse ******
    public function scopeOrdersForInvoice($query, $companyId, $dateEnd, $excludeCompanies)
    {
        return $query->select('ordservices.id as ordercode', 'companies.id as companyCode', 'companies.name as company', 'address.id as addressid','address.addressname', 'ordservices.sancode', 'ordservices.date_start',
                                'districts.name as district', 'provinces.name as province', 'departments.name as department',
                                'ordservices.ordstatus_id', 'ordstatus.description as statusname', 'ordstatus.stylealert', 'ordstatus.iconalert', 'vsatplans.description', 'orders_plans.usd_price',
                                'currencies.id as currencyCode', 'currencies.symbol', 'currencies.iso4217', 'companies_currencies.exchange')
            ->join('companies', 'ordservices.customer_id', '=', 'companies.id')
            
            ->join('companies_customers', 'companies.id', '=', 'companies_customers.customer_id')
            ->join('currencies', 'companies_customers.currency_id', '=', 'currencies.id')
            ->join('companies_currencies', 'currencies.id', '=', 'companies_currencies.currency_id')

            ->join('address', 'ordservices.address_id', '=', 'address.id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('ordstatus', 'ordservices.ordstatus_id', '=', 'ordstatus.id')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.date_end', null],
                ['ordservices.typeservice_id', 'AC'],
                ['vsatplans.typeplan_id', 'PS'], //postpaid
                ['ordservices.date_start','<=', $dateEnd],
                ['companies_currencies.company_id', $companyId],
                ['companies_currencies.status','=', true],
            ])
            ->whereIn('ordservices.ordstatus_id', array('S2', 'S3', 'S5')) //Active & Suspended
            ->whereNotIn('ordservices.id', $excludeCompanies)
            ->get();
    }
    // *******

    public function scopeOrdersCancelsForInvoice($query, $companyId, $dateStart, $dateEnd, $excludeCompanies)
    {
        return $query->select('ordservices.id as ordercode', 'companies.id as companyCode', 'companies.name as company', 'address.id as addressid', 'address.addressname', 'ordservices.sancode', 'ordservices.date_start',
                                'districts.name as district', 'provinces.name as province', 'departments.name as department',
                                'ordservices.date_end', 'ordservices.ordstatus_id', 'ordstatus.description as statusname', 'ordstatus.stylealert', 'ordstatus.iconalert', 'vsatplans.description', 'orders_plans.usd_price',
                                'currencies.id as currencyCode', 'currencies.symbol', 'currencies.iso4217', 'companies_currencies.exchange')
            ->join('companies', 'ordservices.customer_id', '=', 'companies.id')

            ->join('companies_customers', 'companies.id', '=', 'companies_customers.customer_id')
            ->join('currencies', 'companies_customers.currency_id', '=', 'currencies.id')
            ->join('companies_currencies', 'currencies.id', '=', 'companies_currencies.currency_id')

            ->join('address', 'ordservices.address_id', '=', 'address.id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('ordstatus', 'ordservices.ordstatus_id', '=', 'ordstatus.id')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.typeservice_id', 'AC'],
                ['vsatplans.typeplan_id', 'PS'], //postpaid
                ['ordservices.ordstatus_id', 'S4'], //cancels
                ['ordservices.date_end','>=', $dateStart],
                ['ordservices.date_start','<=', $dateEnd],
                ['companies_currencies.company_id', $companyId],
                ['companies_currencies.status','=', true]
            ])
            ->whereNotIn('ordservices.id', $excludeCompanies)
            ->get();
    }

    public function scopeGetTokesForOrders($query, $companyId, $sanCode, $dateStart, $dateEnd)
    {
        return $query->select('ordservices.id as orderCode', 'vsatplans.typeplan_id', 'vsatplans.description as plan', 'orders_plans.usd_price as price', 
                                'ordservices.date_start as dateActive', 'vsatplans.usdcost')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.sancode', $sanCode],
                ['ordservices.typeservice_id', 'AC'],
                ['vsatplans.typeplan_id', 'TK'], //postpaid
                ['ordservices.ordstatus_id', 'S2'],
                ['ordservices.date_start', '>=', $dateStart],
                ['ordservices.date_start', '<=', $dateEnd],
            ])
            ->get();
    }

    public function scopeGetDatesOrder($query, $orderId)
    {
        return $query->select('companies.name as company', 'vsatplans.description as planname', 'orders_plans.usd_price', 'address.addressname', 'districts.name as district', 'provinces.name as province', 'departments.name as department', 'date_start as start', 'date_end as end')
            ->join('companies', 'ordservices.customer_id', '=', 'companies.id')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->join('address', 'ordservices.address_id', '=', 'address.id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->where('ordservices.id', $orderId)
            ->first();
    }

    public function scopeQtyCompaniesPerOrderPeriod($query, $companyId, $dateStart, $dateEnd)
    {
        return $query->select('ordservices.customer_id as code')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.typeservice_id', 'AC'],
                ['vsatplans.typeplan_id', 'PS'], //postpaid
                ['ordservices.ordstatus_id', 'S4'], //cancels
                ['ordservices.date_end','>=', $dateStart],
                ['ordservices.date_start','<=', $dateEnd],
            ])
            ->groupBy('ordservices.customer_id')
            ->get();
    }

    public function scopeQtyOrdersCancelsPerPeriod($query, $companyId, $dateStart, $dateEnd)
    {
        return $query->select('ordservices.id as sancode')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.typeservice_id', 'AC'],
                ['vsatplans.typeplan_id', 'PS'], //postpaid
                ['ordservices.ordstatus_id', 'S4'], //cancels
                ['ordservices.date_end','>=', $dateStart],
                ['ordservices.date_start','<=', $dateEnd],
            ]);
    }

    public function scopeGetQtyCompaniesOrdersActivations($query, $companyId, $dateEnd)
    {

        return $query->select('ordservices.customer_id as code')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.typeservice_id', 'AC'],
                ['vsatplans.typeplan_id', 'PS'], //postpaid
                ['ordservices.date_start','<=', $dateEnd],
            ])
            ->whereIn('ordservices.ordstatus_id', array('S2', 'S3'))
            ->groupBy('ordservices.customer_id')
            ->get();

    }

    public function scopeGetQtyActivations($query, $companyId, $dateStart, $dateEnd)
    {
        return $query->select('id')
            ->where([
                ['provider_id', $companyId],
                ['typeservice_id', 'AC'],
                ['date_start', '>=', $dateStart],
                ['date_start', '<=', $dateEnd],
                ['status', true]
            ]);
    }

    public function scopeGetQtyToDue($query, $companyId, $dateStart, $dateEnd)
    {
        return $query->select('id')
            ->where([
                ['provider_id', $companyId],
                ['typeservice_id', 'AC'],
                ['date_due', '>=', $dateStart],
                ['date_due', '<=', $dateEnd],
                ['status', true]
            ]);
    }

    public function scopeGetAllSanPerCustomerActive($query, $customerId, $providerId, $dateEnd)
    {
        return $query->select('ordservices.id as ordercode', 'vsatplans.typeplan_id', 'ordservices.sancode', 'ordservices.creditdays', 'ordservices.date_start', 'vsatplans.description', 'vsatplans.anotations',
                                'orders_plans.usd_price', 'vsatplans.usdcost')
            ->join('companies', 'ordservices.customer_id', '=', 'companies.id')
            ->join('ordstatus', 'ordservices.ordstatus_id', '=', 'ordstatus.id')
            ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['ordservices.provider_id', $providerId],
                ['ordservices.customer_id', $customerId],
                ['ordservices.status', true],
                ['ordservices.date_end', null],
                ['ordservices.typeservice_id', 'AC'],
                ['vsatplans.typeplan_id', 'PS'], //postpaid
                ['ordservices.date_start','<=', $dateEnd]
            ])
            ->whereIn('ordservices.ordstatus_id', array('S2', 'S3', 'S5')) //Active & Suspended
            ->get();
    }

    public function scopeGetAllSanPerCustomerCancels($query, $customerId, $providerId, $dateStart, $dateEnd)
    {
        return $query->select('ordservices.id as ordercode', 'vsatplans.typeplan_id', 'ordservices.sancode', 'ordservices.creditdays', 'ordservices.date_start',
                                'ordservices.date_end', 'vsatplans.description', 'vsatplans.anotations', 'orders_plans.usd_price', 'vsatplans.usdcost')
                        ->join('companies', 'ordservices.customer_id', '=', 'companies.id')
                        ->join('ordstatus', 'ordservices.ordstatus_id', '=', 'ordstatus.id')
                        ->join('orders_plans', 'ordservices.id', '=', 'orders_plans.orderservice_id')
                        ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
                        ->where([
                                ['ordservices.provider_id', $providerId],
                                ['ordservices.customer_id', $customerId],
                                ['ordservices.status', true],
                                ['ordservices.typeservice_id', 'AC'],
                                ['vsatplans.typeplan_id', 'PS'], //postpaid
                                ['ordservices.ordstatus_id', 'S4'], //cancels
                                ['ordservices.date_end','>=', $dateStart],
                                ['ordservices.date_start','<=', $dateEnd]
                            ])
                        ->get();
    }

    public function scopeGetCountPendings($query, $companyId){
        return $query->where([
                ['provider_id', $companyId],
                ['admin_validation', 'PV'],
                ['status', true]
            ])
            ->count();
    }

    public function scopeGetListPendings($query, $companyId){
        return $query->select('ordservices.id as orderCode', 'companies.name as companyName', 'users.name as userName', 
                                'users.lastname as userLastname', 'ordservices.created_at as date')
            ->join('users', 'ordservices.createdbyuser', '=', 'users.id')
            ->join('companies', 'ordservices.customer_id', '=', 'companies.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['ordservices.status', true],
                ['ordservices.admin_validation', 'PV']
            ])
            ->orderBy('ordservices.created_at', 'DESC')
            ->limit(6)
            ->get();
    }

}
