<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MyNote extends Model
{
    //
    protected $table = "mynotes";

    public function scopeNextCode($query){
        $maxCode = $query->max('id');
        return ($maxCode + 1);
    }

    public function scopeGetNotes($query, $userId)
    {
        return $query->select('id as code', 'title', 'body', 'updated_at')
            ->where([
                ['user_id', $userId],
                ['status', true]
            ])
            ->orderBy('updated_at', 'DESC')
            ->get();
    }

    public function scopeGetInfoNote($query, $userId, $noteId)
    {
        return $query->select('id as code', 'title', 'body')
            ->where([
                ['id', $noteId],
                ['user_id', $userId],
                ['status', true]
            ])
            ->firstOrFail();
    }

}
