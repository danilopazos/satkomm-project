<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelProduct extends Model
{
    //
    protected $table = "modelproducts";

    public function scopeListModels($query, $brandId)
    {
        return $query->select('id as code', 'description')
            ->where('brandproduct_id', '=', $brandId)
            ->get();
    }

    public function scopeGetBrandModel($query, $modelId)
    {
        return $query->select('brandsproducts.description as brand', 'modelproducts.description as model')
            ->join('brandsproducts', 'modelproducts.brandproduct_id', '=', 'brandsproducts.id')
            ->where('modelproducts.id', '=', $modelId)
            ->first();
    }

}
