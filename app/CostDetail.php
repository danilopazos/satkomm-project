<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostDetail extends Model
{
    //
    protected $table = 'costdetails';

    public function scopeNextCode($query){
        $maxCode = $query->max('id');
        if($maxCode == null){
            $newCode = 1;
        }else{
            $newCode = $maxCode + 1;
        }
        return $newCode;
    }

    public function scopeGetListByOrder($query, $orderId, $companyId)
    {
        return $query->select('costdetails.id as code', 'companies.name as company', 'typeinvoices.description as document', 'costdetails.numberdocument', 'typecosts.description as typeCost', 
                                    'costdetails.anotations', 'costdetails.typerecord', 'currencies.symbol', 'costdetails.amount', 'currencies.iso4217', 'companies_currencies.exchange', 
                                    'costdetails.attach', 'costdetails.created_at', 'users.name as username', 'users.lastname as userlastname')
            ->join('companies', 'costdetails.provider_id', '=', 'companies.id')
            ->join('typeinvoices', 'costdetails.typeinvoice_id', '=', 'typeinvoices.id')
            ->join('typecosts', 'costdetails.typecost_id', '=', 'typecosts.id')
            ->join('currencies', 'costdetails.currency_id', '=', 'currencies.id')
            ->join('companies_currencies', 'currencies.id', '=', 'companies_currencies.currency_id')
            ->join('users', 'costdetails.createdbyuser', '=', 'users.id')
            ->where([
                ['costdetails.order_id', $orderId],
                ['costdetails.status', true],
                ['companies_currencies.company_id', $companyId],
                ['companies_currencies.status', true]
            ])
            ->get();
    }

    public function scopeGetListTotals($query, $orderId)
    {
        return $query->select('costdetails.typerecord', 'currencies.iso4217', 'companies_currencies.exchange','costdetails.amount')
            ->join('currencies', 'costdetails.currency_id', '=', 'currencies.id')
            ->join('companies_currencies', 'currencies.id', '=', 'companies_currencies.currency_id')
            ->where([
                ['costdetails.order_id', $orderId],
                ['costdetails.status', true]
            ])
            ->get();
    }
}
