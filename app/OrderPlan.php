<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderPlan extends Model
{
    //
    protected $table = "orders_plans";
    public $timestamps = false;

    public function scopeNextCode($query)
    {
       $maxCode = $query->max('id');
       $nextCode = $maxCode + 1;
       return $nextCode;
    }

    public function scopeGetAllPlansOrders($query, $companyId)
    {
        return $query->select('ordservices.id as orderId', 'orders_plans.id as code', 'vsatplans.id as codePlan', 'vsatplans.typeplan_id', 'typeplans.description as typePlan',
                                'vsatplans.description as namePlan', 'vsatplans.anotations as notePlan', 'orders_plans.usd_price as pricePlan', 'vsatplans.commodecost','vsatplans.usdcost')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->join('typeplans', 'vsatplans.typeplan_id', '=', 'typeplans.id')
            ->join('ordservices', 'orders_plans.orderservice_id', '=', 'ordservices.id')
            ->where([
                ['ordservices.provider_id', $companyId],
                ['orders_plans.status', true]
            ])
            ->get();
    }

    public function scopeGetPlanForOrder($query, $orderId)
    {
        return $query->select('orders_plans.id as code', 'vsatplans.id as codePlan', 'vsatplans.typeplan_id', 'typeplans.description as typePlan',
                                'vsatplans.description as namePlan', 'vsatplans.anotations as notePlan', 'orders_plans.usd_price as pricePlan', 'vsatplans.commodecost','vsatplans.usdcost')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->join('typeplans', 'vsatplans.typeplan_id', '=', 'typeplans.id')
            ->where([
                ['orders_plans.orderservice_id', $orderId],
                ['orders_plans.status', true]
            ])
            ->get();
    }

    public function scopeGetPlanForInvoice($query, $ordersId)
    {
        return $query->select('orders_plans.orderservice_id as ordercode', 'orders_plans.id as code', 'vsatplans.id as codePlan', 'vsatplans.typeplan_id', 'typeplans.description as typePlan',
                                'vsatplans.description as namePlan', 'vsatplans.anotations as notePlan', 'orders_plans.usd_price as pricePlan', 'vsatplans.commodecost','vsatplans.usdcost')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->join('typeplans', 'vsatplans.typeplan_id', '=', 'typeplans.id')
            ->where([
                ['orders_plans.status', true]
            ])
            ->whereIn('orders_plans.orderservice_id', $ordersId)
            ->get();
    }

    public function scopeGetAditionalServicePerOrder($query, $orderId){
        return $query->select('vsatplans.description', 'orders_plans.usd_price')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['orders_plans.orderservice_id', $orderId],
                ['orders_plans.status', true],
                ['vsatplans.typeplan_id', 'AD']
            ])
            ->get();
    }

    public function scopeGetAditionalServiceDetail($query, $orderId){
        return $query->select('vsatplans.description', 'vsatplans.anotations', 'vsatplans.typeplan_id', 'orders_plans.usd_price', 'vsatplans.usdcost')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['orders_plans.orderservice_id', $orderId],
                ['orders_plans.status', true],
                ['vsatplans.typeplan_id', 'AD']
            ])
            ->get();
    }

    public function scopeGetQtyActives($query, $planId, $providerId)
    {
        return $query
            ->join('ordservices', 'orders_plans.orderservice_id', '=', 'ordservices.id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['orders_plans.vsatplan_id', $planId],
                ['orders_plans.status', true],
                ['ordservices.provider_id', $providerId],
                ['ordservices.date_end', null],
                ['ordservices.status', true],
                ['vsatplans.typeplan_id', 'PS']
            ])
            ->count();
    }

    public function scopeGetListActives($query, $planId, $providerId)
    {
        return $query->select('companies.name as companyName', 'ordservices.id as orderCode', 'ordservices.date_start', 'orders_plans.usd_price as usdPrice')
            ->join('ordservices', 'orders_plans.orderservice_id', '=', 'ordservices.id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->join('companies', 'ordservices.customer_id', '=', 'companies.id')
            ->where([
                ['orders_plans.vsatplan_id', $planId],
                ['orders_plans.status', true],
                ['ordservices.provider_id', $providerId],
                ['ordservices.date_end', null],
                ['ordservices.status', true],
                ['vsatplans.typeplan_id', 'PS']
            ])
            ->get();
    }

    public function scopeGetListForCreateInvoice($query, $orderId){
        return $query->select('ordservices.id as code', 'ordservices.sancode', 'vsatplans.typeplan_id', 'vsatplans.description as namePlan', 'vsatplans.anotations as notePlan', 
                                'orders_plans.usd_price', 'vsatplans.usdcost', 'vsatplans.commodecost')
            ->join('ordservices', 'orders_plans.orderservice_id', '=', 'ordservices.id')
            ->join('vsatplans', 'orders_plans.vsatplan_id', '=', 'vsatplans.id')
            ->where([
                ['ordservices.id', $orderId]
            ])
            ->get();
    }
}
