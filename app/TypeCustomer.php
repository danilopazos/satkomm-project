<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCustomer extends Model
{
    
    protected $table = 'typecustomers';

    public function scopeGetList($query, $language)
    {
        return $query->select('id as code', 'description')
            ->where('language', $language)
            ->get();
    }
}
