<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VsatRecord extends Model
{
    //
    protected $table = "vsatrecords";

    public function scopeListVsatRecords($query, $companyId){
        return $query->select('id as code', 'description')
            ->where([
                ['status', '=', true],
                ['company_id', '=', $companyId]
            ])
            ->get();
    }
}
