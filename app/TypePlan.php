<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePlan extends Model
{
    //
    protected $table = "typeplans";

    public function scopeListTypePlans($query){
        return $query->select('id as code', 'description')
            ->get();
    }
}
