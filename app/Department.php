<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $table = "departments";

    public function scopeListDepartments($query, $countryId)
    {
        return $query->select('id as code', 'name')
            ->where('country_id', '=', $countryId)
            ->orderBy('name', 'ASC')
            ->get();
    }

}
