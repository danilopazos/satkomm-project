<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VsatPrepaidPrice extends Model
{
    //
    protected $table = "vsatprepaid_prices";
    public $timestamps = false;

    public function scopeNextCode($query){

        $maxCode = $query->max('id');
        $maxCode ++;

        return $maxCode;
    }

    public function scopeGetListPrices($query, $planId, $customerId)
    {
        return $query->where([
                    ['vsatplan_id', '=', $planId],
                    ['customer_id', '=', $customerId]
                ])
            ->get();

    }

    public function scopeGetCountPerCustomer($query, $customerId)
    {
        return $query->select('vsatplan_id', DB::raw('count(vsatplan_id) as qtyPrices'))
            ->where([
                ['customer_id', $customerId]
            ])
            ->groupBy('vsatplan_id')
            ->get();
    }

    public function scopeGetPrepaidAvailables($query, $companyId, $band, $customerCode, $plan)
    {
        return $query->select('vsatplans.id as code', 'vsatplans.description', 'vsatrecords.description as band')
            ->join('vsatplans', 'vsatprepaid_prices.vsatplan_id', '=', 'vsatplans.id')
            ->join('vsatrecords', 'vsatplans.vsatrecord_id', '=', 'vsatrecords.id')
            ->where([
                ['vsatprepaid_prices.customer_id', $customerCode],
                ['vsatplans.typeplan_id', $plan],
                ['vsatplans.status', true],
                ['vsatrecords.id', $band],
                ['vsatrecords.company_id', $companyId],
                ['vsatrecords.status', true]
            ])
            ->groupBy('vsatprepaid_prices.id')
            ->get();
    }

}
