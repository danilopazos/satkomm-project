<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspensionControl extends Model
{
    protected $table = "suspensioncontrols";
    public $timestamps = false;
    protected $dates = ['start_suspension', 'end_suspension'];

    public function scopeNextCode($query, $orderId){
        $max = $query->select('id as code')
            ->where([
                    ['order_id', $orderId]
                ])
            ->orderBy('code', 'DESC')
            ->limit(1)
            ->first();
        
        if(isset($max->code)){
            $nextCode = ($max->code) +1;
        
        }else{
            $nextCode = 1;
        
        }

        return $nextCode;
    }

    public function scopeGetId($query, $orderId)
    {
        return $query->select('id as code')
            ->where([
                ['order_id', $orderId],
                ['end_suspension', null],
                ['status', true]
            ])
            ->first();
    }

    public function scopeGetInformation($query, $ordersId, $dateStart, $dateEnd)
    {
        return $query->select('start_suspension', 'end_suspension')
            ->where([
                ['order_id', $ordersId],
                ['status', true],
                ['start_suspension', '>=', $dateStart],
                ['start_suspension', '<=', $dateEnd],
                ['stop_billing', true]
            ])
            ->get();
    }

    public function scopeGetListByOrderId($query, $orderId)
    {
        return $query->select('start_suspension', 'end_suspension', 'stop_billing', 'suspensioncontrols.status', 
                                'users.name', 'users.lastname')
            ->join('users', 'suspensioncontrols.user_id', '=', 'users.id')
            ->where([
                ['suspensioncontrols.status', true],
                ['order_id', $orderId]
            ])
            ->orderBy('suspensioncontrols.id', 'DESC')
            ->get();
    }

}
