<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptHistory extends Model
{
    protected $table = 'receipthistories';

    public function scopeNextCode($query, $receiptId)
    {
        $maxCode = $query->select('id')
            ->where([
                ['receipt_id', $receiptId]
            ])
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->first();
        if(isset($maxCode->id)){
            $newCode = $maxCode->id + 1;
        }else{
            $newCode = 1;
        }
        return $newCode;
    }

    public function scopeHistoryUpdates($query, $receiptId)
    {
        return $query->select('users.name', 'users.lastname', 'users.imgprofile', 
                                'receipthistories.anotations', 'receipthistories.updates', 'receipthistories.created_at')
            ->join('users', 'receipthistories.user_id', '=', 'users.id')
            ->where([
                ['receipthistories.receipt_id', $receiptId]
            ])
            ->orderBy('receipthistories.id', 'DESC')
            ->get();
    }
}
