<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    //
    protected $table = "currencies";

    public function scopeListCurrencies($query, $listCodes)
    {
        return $query->select('id as code', 'iso4217', 'name')
            ->whereNotIn('id', $listCodes)
            ->get();
    }

    public function scopeGetCurrencyName($query, $currencyId)
    {
        $nameCurrency = $query->select('name', 'iso4217')
            ->where('id', $currencyId)
            ->first();

        return $nameCurrency->name.' '.$nameCurrency->iso4217;
    }
}
