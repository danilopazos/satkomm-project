<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $table = "countries";

    public function scopeListCountries($query)
    {
        return $query->select('id as code', 'name')
            ->get();
    }

}
