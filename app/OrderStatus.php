<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table = "ordstatus";

    public function scopeGetList($query, $notIn = [])
    {
        return $query->select('id as code', 'action_name as description')
            ->whereNotIn('id', $notIn)
            ->orderBy('action_name', 'ASC')
            ->get();
    }

    public function scopeGetStatusOrder($query, $orderId)
    {
        return $query->select('description', 'stylealert', 'iconalert')
            ->join('ordservices', 'ordstatus.id', '=', 'ordservices.ordstatus_id')
            ->where([
                ['ordservices.id', $orderId]
            ])
            ->first();
    }

}
