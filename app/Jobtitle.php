<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobtitle extends Model
{
    //
    public $timestamps = false;

    public function scopeGetList($query)
    {
        return $query->select('id as code', 'description')
            ->get();
    }

}
