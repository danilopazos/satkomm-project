<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ConsumptionReceipt extends Model
{
    //
    protected $table = "consumptionreceipts";

    public function scopeNextCode($query)
    {
        $maxCode = $query->max('id');
        return ($maxCode + 1);
    }

    public function scopeGetNumberSerial($query, $companyId)
    {
        return $query->select('serialnumber', 'correlative')
            ->where('provider_id', $companyId)
            ->orderBy('serialnumber', 'DESC')
            ->orderBy('correlative', 'DESC')
            ->limit(1)
            ->first();
    }

    public function scopeGetListCompaniesNoReceipts($query, $companyId, $period){
        return $query->select('detailreceipts.order_id')
            ->join('detailreceipts', 'consumptionreceipts.id', '=', 'detailreceipts.receipt_id')
            ->where([
                ['consumptionreceipts.provider_id', $companyId],
                ['consumptionreceipts.period', $period],
                ['consumptionreceipts.status', true]
            ])
            ->groupBy('detailreceipts.order_id')
            ->get();
    }

    public function scopeGetListReceipts($query, $companyId){
        return $query->select('consumptionreceipts.id as code', 'serialnumber', 'correlative', 'period', 'customer_id', 'companies.name as companyname',
                                'currencies.symbol', 'exchange', 'usdtotal', 'currencies.iso4217', 'consumptionreceipts.created_at')
            ->join('companies', 'consumptionreceipts.customer_id', '=', 'companies.id')
            ->join('currencies', 'consumptionreceipts.currency_id', '=', 'currencies.id')
            ->where([
                ['consumptionreceipts.provider_id', $companyId],
                ['consumptionreceipts.status', true]
            ])
            ->get();
    }

    public function scopeGetInfoInvoice($query, $receiptId, $providerId)
    {
        return $query->select('serialnumber', 'correlative', 'customer_id', 'period', 'consumptionreceipts.created_at', 'date_due',
                                'currencies.name as curname', 'currencies.iso4217 as curiso', 'currencies.symbol as cursymbol', 'exchange', 'usdtotal')
            ->join('currencies', 'consumptionreceipts.currency_id', '=', 'currencies.id')
            ->where([
                ['consumptionreceipts.id', $receiptId],
                ['consumptionreceipts.provider_id', $providerId],
                ['consumptionreceipts.status', true]
            ])
            ->firstOrFail();
    }

    public function scopeGetListMonths($query, $companyId){
        return $query->select(DB::raw("concat_ws('-', YEAR(created_at), MONTH(created_at)) as period"))
            ->where([
                ['provider_id', $companyId],
                ['status', true]
            ])
            ->orderBy('period', 'DESC')
            ->groupBy(DB::raw("concat_ws('-', YEAR(created_at), MONTH(created_at))"))
            ->get();
    }

    public function scopeGetEarningsPerPeriod($query, $companyId, $dateStart, $dateEnd){
        return $query->select('usdtotal')
            ->where([
                ['provider_id', $companyId],
                ['status', true]
            ])
            ->whereBetween('created_at', [$dateStart, $dateEnd])
            ->get();
    }

    public function scopeGetListEarningsByMonths($query, $companyId, $period){
        $dateStart = $period.'-01';
        $tmpDate = date("Y-m-d", strtotime($dateStart."+ 1 month"));
        $dateEnd = date("Y-m-d", strtotime($tmpDate."- 1 day"));
        return $query->select(DB::raw('sum(usdtotal) as total'), DB::raw("concat_ws('-', YEAR(created_at), MONTH(created_at)) as period"), DB::raw('sum(usdcost) as usdcost'))
        ->where([
            ['provider_id', $companyId],
            ['created_at', '<', $dateEnd],
            ['status', true]
        ])
        ->orderBy('period', 'DESC')
        ->groupBy(DB::raw("concat_ws('-', YEAR(created_at), MONTH(created_at))"))
        ->limit(6)
        ->get();
    }

}
