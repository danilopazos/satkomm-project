<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VsatPlan extends Model
{
    //
    protected $table = "vsatplans";

    public function scopeNextCode($query){

        $maxCode = $query->max('id');

        if ($maxCode == null){
            $newCode = "P00001";

        }else{
            $correlative = substr($maxCode, 1); //001
            $correlative ++; //2
            $cut = 6 - (strlen($correlative)); //4-1 = 3
            $newCode = (substr("P00000", 0, $cut)).$correlative;

        }

        return $newCode;
    }

    public function  scopeListPlans($query, $companyId){
        return $query->select('vsatplans.id as code', 'vsatplans.description', 'vsatplans.anotations', 'vsatrecords.description as band', 'typeplans.id as codePlan', 'typeplans.description as plan')
            ->join('vsatrecords', 'vsatplans.vsatrecord_id', '=', 'vsatrecords.id')
            ->join('typeplans', 'vsatplans.typeplan_id', '=', 'typeplans.id')
            ->where([
                ['vsatrecords.company_id', '=', $companyId],
                ['vsatrecords.status', '=', true],
                ['vsatplans.status', '=', true]
            ])
            ->get();
    }

    public function scopeGetInfoPlan($query, $codePlan, $companyId){
        return $query->select('vsatplans.id as code', 'vsatplans.*')
            ->join('vsatrecords', 'vsatplans.vsatrecord_id', '=', 'vsatrecords.id')
            ->where([
                    ['vsatplans.id', '=', $codePlan],
                    ['vsatrecords.company_id', '=', $companyId]
                ])
            ->firstOrFail();
    }

    public function scopeInfoForEditOS($query, $planId){

        return $query->select('vsatrecord_id', 'typeplan_id', 'description')
            ->where('id', $planId)
            ->first();

    }

    public function scopeListPricesPostpaidByCustomer($query, $companyId){
        return $query->select('vsatplans.id as code', 'vsatrecords.description as band', 'vsatplans.typeplan_id as typePlanId', 'typeplans.description as typePlan', 
                                'vsatplans.description as planName', 'vsatplans.anotations as planAnotation',
                                'vsatplans.usdcost', 'vsatplans.commodecost')
            ->join('vsatrecords', 'vsatplans.vsatrecord_id', '=', 'vsatrecords.id')
            ->join('typeplans', 'vsatplans.typeplan_id', '=', 'typeplans.id')
            ->where([
                ['vsatplans.status', true],
                ['vsatrecords.company_id', $companyId],
                ['vsatrecords.status', true]
            ])
            ->orderBy('typePlan', 'ASC')
            ->get();
    }

    public function scopeInfoToShowPlan($query, $planId, $companyId)
    {
        return $query->select('vsatplans.id as code', 'vsatrecords.description as bandDescription', 'typeplans.id as typeId', 'typeplans.description as typeDescription', 'vsatplans.description as planDescription', 
                                'vsatplans.anotations as planNote', 'usdcost', 'commodecost', 'download', 'typedownload', 'upload', 'typeupload', 'anytime', 'typeanytime', 'earlymorning', 'typeearlymorning',
                                'total', 'typetotal', 'cir', 'typecir')
            ->join('vsatrecords', 'vsatplans.vsatrecord_id', '=', 'vsatrecords.id')
            ->join('typeplans', 'vsatplans.typeplan_id', '=', 'typeplans.id')
            ->where([
                ['vsatplans.id', $planId],
                ['vsatrecords.company_id', $companyId]
            ])
            ->firstOrFail();
    }

}
