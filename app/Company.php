<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table="companies";

    public function scopeNewCode($query)
    {
        $maxCode = $query->max('id');
        $maxCode ++;
        return $maxCode;
    }

    public function scopeCustomerListSimple($query, $companyId){
        return $query->select('companies.id as code', 'companies.name', 'typedocuments.description as typedocument', 'companies.taxnumber')
            ->join('typedocuments', 'companies.typedocument_id', '=', 'typedocuments.id')
            ->join('companies_customers', 'companies.id', '=', 'companies_customers.customer_id')
            ->where('companies_customers.provider_id', '=', $companyId)
            ->get();
    }

    public function scopeProviderListSimpleDocs($query, $companyId){
        return $query->select('companies.id as code', 'companies.name', 'typedocuments.description as typedocument', 'companies.taxnumber')
            ->join('typedocuments', 'companies.typedocument_id', '=', 'typedocuments.id')
            ->join('companies_providers', 'companies.id', '=', 'companies_providers.provider_id')
            ->where('companies_providers.company_id', '=', $companyId)
            ->get();
    }

    public function scopeProviderListSimple($query, $companyId){
        return $query->select('companies.id as code', 'companies.name')
            ->join('companies_providers', 'companies.id', '=', 'companies_providers.provider_id')
            ->where('companies_providers.company_id', '=', $companyId)
            ->get();
    }

    public function scopeGetName($query, $companyId){

        $name = "";

        if(strlen($companyId) == 4) {
            $result = $query->select('name')
                ->where('id', $companyId)
                ->first();

            $name = $result->name;
        }

        return $name;
    }

    public function scopeCompaniesForAccount($query)
    {
        return $query->select('id as code', 'name')
            ->where('currency_id', '<>', null)
            ->get();
    }

    public function scopeGetInfoCompany($query, $companyId)
    {
        return $query->select('typedocuments.description as typedocument', 'taxnumber', 'image', 'companies.name', 'brand', 'mail', 'telephone', 'website', 'iso4217', 'currencies.name as curname')
            ->join('typedocuments','companies.typedocument_id','=','typedocuments.id')
            ->join('currencies','companies.currency_id','=','currencies.id')
            ->where('companies.id', '=', $companyId)
            ->first();
    }

    public function scopeGetInfoProviderToEdit($query, $providerId, $companyId)
    {
        return $query->select('companies.id as code', 'typedocument_id', 'taxnumber', 'name', 'brand', 'mail', 'telephone', 'website')
            ->join('companies_providers', 'companies.id', '=', 'companies_providers.provider_id')
            ->where([
                ['companies_providers.company_id', '=', $companyId],
                ['companies_providers.status', '=', true],
                ['companies.id' , '=', $providerId]
            ])->firstOrFail();
    }

    public function scopeMyCompanyForInvoice($query, $companyId)
    {
        return $query->select('typedocuments.description as typedocument', 'taxnumber', 'image', 'companies.name', 'mail', 'telephone', 'address.address', 'districts.name as distric', 'provinces.name as province', 'countries.taxamount')
            ->join('typedocuments','companies.typedocument_id','=','typedocuments.id')
            ->join('address', 'companies.id', '=', 'address.company_id')
            ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('countries', 'departments.country_id', '=', 'countries.id')
            ->where([
                    ['companies.id', '=', $companyId],
                    ['companies_address.typeaddress_id', '=', 'FCT'],
                ])
            ->first();
    }

    public function scopeGetInfoCustomer($query, $customerId, $providerId, $language){
        return $query->select('companies.id as code', 'typecustomers.id as typeId', 'typecustomers.description as typeClient', 'typedocuments.description as typedocument', 'taxnumber', 
                                'image', 'companies.name', 'brand', 'mail', 'telephone', 'website', 'companies_customers.daystodue')
            ->join('companies_customers', 'companies.id', '=', 'companies_customers.customer_id')
            ->join('typecustomers', 'companies_customers.typecustomer_id', '=', 'typecustomers.id')
            ->join('typedocuments','companies.typedocument_id','=','typedocuments.id')
            ->where([
                    ['companies.id', '=', $customerId],
                    ['companies_customers.provider_id', $providerId],
                    ['companies_customers.status', true],
                    ['typecustomers.language', $language]
                ])
            ->firstOrFail();
    }

    public function scopeGetInfoProvider($query, $providerId, $companyId){
        return $query->select('companies.id as code', 'typedocuments.description as typedocument', 'taxnumber', 'image', 'companies.name', 'brand', 'mail', 'telephone', 'website')
            ->join('companies_providers', 'companies.id', '=', 'companies_providers.provider_id')
            ->join('typedocuments','companies.typedocument_id','=','typedocuments.id')
            ->where([
                    ['companies.id', '=', $providerId],
                    ['companies_providers.company_id', $companyId],
                    ['companies_providers.status', true]
                ])
            ->firstOrFail();
    }

    public function scopeGetInfoToEditCustomer($query, $companyId, $customerId){
        return $query->select('companies.id as code', 'companies_customers.typecustomer_id', 'typedocument_id', 'taxnumber', 'name', 'brand', 
                                'mail', 'telephone', 'website', 'companies_customers.currency_id', 'companies_customers.daystodue')
            ->join('companies_customers', 'companies.id', '=', 'companies_customers.customer_id')
            ->where([
                ['provider_id', '=', $companyId],
                ['companies_customers.status', '=', true],
                ['companies.id' , '=', $customerId]
            ])->firstOrFail();
    }

}
