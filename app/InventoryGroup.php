<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryGroup extends Model
{
    //
    protected $table = "inventorygroups";
    public $timestamps = false;

    public function scopeGetNewCode($query)
    {
        $maxCode = $query->max('id');
        $correlative = substr($maxCode, 1); //001
        $correlative ++; //2
        $cut = 4 - (strlen($correlative)); //4-1 = 3
        $newCode = (substr("G000", 0, $cut)).$correlative;

        return $newCode;
    }

    public function scopeGetList($query, $companyId)
    {
        return $query->select('id as code', 'description')
            ->where('company_id', '=', $companyId)
            ->get();
    }

    public function scopeGetName($query, $groupId)
    {
        $name = $query->select('description')
            ->where('id', '=', $groupId)
            ->first();
        return $name->description;
    }
}
