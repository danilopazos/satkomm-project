<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlertCenter extends Model
{
    //
    protected $table = "alertcenter";

    public function scopeGetQtyAlerts($query, $userId)
    {
        return $query->select('alert_id')
            ->where([
                ['receiver_id', $userId],
                ['status', true]
            ])
            ->count('alert_id');
    }

    public function scopeGetAlertsMessages($query, $companyId, $language)
    {
        return $query->select('alertcenter.alert_id as code', 'messages.icon', 'messages.title', 'messages.message', 'messages.url', 
                                'alertcenter.created_at')
            ->join('messages', 'alertcenter.message_id', '=', 'messages.id')
            ->where([
                ['receiver_id', $companyId],
                ['messages.language', $language],
                ['messages.status', true],
                ['alertcenter.status', true]
            ])
            ->get();
    }

}
