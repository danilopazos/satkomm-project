<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountReceipt extends Model
{
    protected $table = 'discountsreceipts';
    public $timestamps = false;

    public function scopeNextCode($query, $receiptId)
    {
        $maxCode = $query->select('id')
                ->where('receipt_id', $receiptId)
                ->orderBy('id', 'DESC')
                ->limit(1)
                ->first();
        if(isset($maxCode->id)){
            $newCode = $maxCode->id + 1;
        }else{
            $newCode = 1;
        }
        return $newCode;
    }

    public function scopeGetDiscounts($query, $receiptId)
    {
        return $query->select('order_id', 'usd_discount')
            ->where([
                ['receipt_id', $receiptId],
                ['status', true]
            ])
            ->get();
    }
}
