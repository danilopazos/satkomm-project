<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompaniesCurrencies extends Model
{
    //
    protected $table = "companies_currencies";

    public function scopeGetMyCurrenciesList($query, $companyId)
    {
        return $query->select('currencies.id as code', 'iso4217', 'name')
            ->join('currencies', 'companies_currencies.currency_id', '=', 'currencies.id')
            ->where([
                ['companies_currencies.company_id', $companyId],
                ['companies_currencies.status', true]
            ])
            ->get();
    }

    public function scopeGetCodesList($query, $companyId)
    {
        return $query->select('currencies.id as code')
            ->join('currencies', 'companies_currencies.currency_id', '=', 'currencies.id')
            ->where([
                ['companies_currencies.company_id', $companyId],
                ['companies_currencies.status', true]
            ])
            ->get();
    }

    public function scopeNextCode($query)
    {
        $maxCode = $query->max('id');
        return ($maxCode + 1);
    }

}
