<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellCategory extends Model
{
    //
    protected $table = "sellcategories";

    public function scopeListSellCategories($query)
    {
        return $query->select('id as code', 'description')->get();
    }

}
