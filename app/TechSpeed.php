<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechSpeed extends Model
{
    //
    protected $table = "techspeeds";

    public function scopeListSpeeds($query){
        return $query->select('id as code', 'description')
            ->get();
    }

}
