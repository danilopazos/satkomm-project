<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryItem extends Model
{
    //
    protected $table = "inventoryitems";

    public function scopeNextCode($query)
    {

        $maxCode = $query->max('id');
        $correlative = substr($maxCode, 1); //001
        $correlative ++; //2
        $cut = 4 - (strlen($correlative)); //6-1 = 5
        $newCode = (substr("I001", 0, $cut)).$correlative;

        return $newCode;

    }

    public function scopeListInventories($query, $companyId)
    {

        return $query->select('inventoryitems.id as code', 'group_id', 'anotations', 'itemavailables.id as available_id', 'itemavailables.description as available', 'itemavailables.stylealert', 'itemavailables.iconalert', 'modelproducts.description as model', 'brandsproducts.description as brand', 'typesproducts.description as typeproduct')
            ->join('itemavailables', 'inventoryitems.available_id', '=', 'itemavailables.id')
            ->join('modelproducts', 'inventoryitems.model_id', '=', 'modelproducts.id')
            ->join('brandsproducts', 'modelproducts.brandproduct_id', '=', 'brandsproducts.id')
            ->join('typesproducts', 'brandsproducts.typeproduct_id', '=', 'typesproducts.id')
            ->where([
                ['company_id', '=', $companyId],
                ['sellcategory_id', '=', 'S001']
            ])
            ->get();

    }

    public function scopeInfoInventory($query, $inventoryId, $companyId)
    {

        return $query->select('inventoryitems.id as code', 'inventoryitems.group_id', 'inventoryitems.model_id as model', 'modelproducts.brandproduct_id as brand', 'brandsproducts.typeproduct_id as type', 'typesproducts.sellcategory_id as category', 'inventoryitems.anotations', 'inventoryitems.available_id', 'inventoryitems.company_id')
            ->join('modelproducts', 'inventoryitems.model_id', '=', 'modelproducts.id')
            ->join('brandsproducts', 'modelproducts.brandproduct_id', '=', 'brandsproducts.id')
            ->join('typesproducts', 'brandsproducts.typeproduct_id', '=', 'typesproducts.id')
            ->where([
                    ['inventoryitems.id', '=', $inventoryId],
                    ['inventoryitems.company_id', '=', $companyId]
                ])
            ->first();

    }

    public function scopeGetListForNewOrder($query, $companyId, $typeId)
    {
        return $query->select('inventoryitems.id as code', 'brandsproducts.description as brand', 'modelproducts.description as model', 'inventoryitems.anotations')
            ->join('modelproducts', 'inventoryitems.model_id', '=', 'modelproducts.id')
            ->join('brandsproducts', 'modelproducts.brandproduct_id', '=', 'brandsproducts.id')
            ->where([
                ['inventoryitems.company_id', '=', $companyId],
                ['inventoryitems.available_id', '=', 'S1'], //available
                ['brandsproducts.typeproduct_id', '=', $typeId]
            ])
            ->get();
    }

}
