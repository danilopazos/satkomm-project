<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAttachment extends Model
{
    //
    protected $table = "typeattachments";

    public function scopeGetListComplete($query, $language){
        return $query->select('description', 'forcustomers', 'fororders', 'important', 'validate_end')
        ->where([
            ['language', $language]
        ])
        ->orderBy('description', 'ASC')
        ->get();
    }

    public function scopeGetList($query, $language){
        return $query->select('id as code', 'description')
            ->where([
                ['language', $language],
                ['forcustomers', true]
            ])
            ->orderBy('description', 'ASC')
            ->get();
    }

    public function scopeGetListForOrders($query, $language){
        return $query->select('id as code', 'description')
            ->where([
                ['language', $language],
                ['fororders', true]
            ])
            ->orderBy('description', 'ASC')
            ->get();
    }

    public function scopeGetListImportantForCustomers($query, $language, $excludeDocs){
        return $query->select('id as code', 'description', 'validate_end')
            ->where([
                ['language', $language],
                ['important', true],
                ['forcustomers', true]
            ])
            ->whereNotIn('id', $excludeDocs)
            ->get();
    }

    public function scopeGetListImportantForOrder($query, $language){
        return $query->select('id as code', 'description', 'validate_end')
            ->where([
                ['language', $language],
                ['important', true],
                ['fororders', true]
            ])
            ->get();
    }
}
