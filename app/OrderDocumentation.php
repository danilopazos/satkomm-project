<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDocumentation extends Model
{
    protected $table = "orderdocumentations";

    public function scopeNextCode($query, $orderId){
        $maxCode = $query->select('id')
            ->where([
                ['order_id', $orderId]
            ])
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->first();
        if(isset($maxCode->id)){
            $newCode = $maxCode->id + 1;
        }else{
            $newCode = 1;
        }
        return $newCode;
    }

    public function scopeGetLisByOrder($query, $orderId, $language){
        return $query->select('typeattach_id as code', 'attach', 'anotations', 'typeattachments.description')
            ->join('typeattachments', 'orderdocumentations.typeattach_id', '=', 'typeattachments.id')
            ->where([
                ['orderdocumentations.order_id', $orderId],
                ['orderdocumentations.status', true],
                ['typeattachments.language', $language]
            ])
            ->get();
    }

}
