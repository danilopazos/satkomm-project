<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAttach extends Model
{
    //
    protected $table = "orders_attaches";

    public $timestamps = false;

    public function scopeGetItemAttach($query, $orderId)
    {
        return $query->select('inventoryitems.id as code', 'modelproducts.description as model', 'brandsproducts.description as brand', 'typesproducts.id as typeproduct', 'typesproducts.sellcategory_id as sellcategory')
            ->join('inventoryitems', 'inventoryrecord_id', '=', 'inventoryitems.id')
            ->join('modelproducts', 'model_id', 'modelproducts.id')
            ->join('brandsproducts', 'brandproduct_id', 'brandsproducts.id')
            ->join('typesproducts', 'typeproduct_id', 'typesproducts.id')
            ->where([
                ['ordservice_id', $orderId],
                ['status', true]
            ])
            ->first();
    }

    public function scopeGetInfoItem($query, $orderId)
    {
        return $query->select('inventoryitems.id as code', 'modelproducts.description as model', 'brandsproducts.description as brand',
                                'typesproducts.description as typeproduct', 'itemavailables.id as statusid', 'itemavailables.description as status',
                                'itemavailables.stylealert', 'itemavailables.iconalert', 'inventoryitems.anotations as notes')
            ->join('inventoryitems', 'inventoryrecord_id', '=', 'inventoryitems.id')
            ->join('itemavailables', 'inventoryitems.available_id', '=', 'itemavailables.id')
            ->join('modelproducts', 'model_id', 'modelproducts.id')
            ->join('brandsproducts', 'brandproduct_id', 'brandsproducts.id')
            ->join('typesproducts', 'typeproduct_id', 'typesproducts.id')
            ->where([
                ['ordservice_id', $orderId],
                ['status', true]
            ])
            ->first();
    }

    public function scopeGetCodeAttach($query, $orderId)
    {
        return $query->select('inventoryrecord_id as code')
            ->where([
                ['ordservice_id', $orderId],
                ['status', true]
            ])
            ->first();
    }

}
