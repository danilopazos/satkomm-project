<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ItemAvailable extends Model
{
    //
    protected $table = "itemavailables";

    public function scopeListStatus($query)
    {
        return $query->select('id as code', 'description')
            ->get();
    }

    public function scopeListInventoryStatys($query, $companyId)
    {
        return $query->select('itemavailables.id as code', 'itemavailables.description', DB::raw('count(inventoryitems.id) as total'))
            ->join('inventoryitems', 'itemavailables.id', '=', 'inventoryitems.available_id')
            ->where([
                ['inventoryitems.company_id', $companyId]
            ])
            ->groupBy('itemavailables.id')
            ->orderBy('itemavailables.id', 'ASC')
            ->get();
    }
}
