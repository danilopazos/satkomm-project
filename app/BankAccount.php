<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    //
    protected $table = "bankaccounts";

    public function scopeNewCode($query){
        $maxCode = $query->max('id');
        return ($maxCode + 1);
    }

    public function scopeGetList($query, $companyId){
        return $query->select('bankaccounts.id as code', 'banks.image', 'banks.description as bankName', 'currencies.name as curName', 'currencies.iso4217', 'bankaccounts.number_account', 'bankaccounts.cci_account')
            ->join('banks', 'bankaccounts.bank_id', '=', 'banks.id')
            ->join('currencies', 'bankaccounts.currency_id', '=', 'currencies.id')
            ->where([
                ['bankaccounts.company_id', $companyId],
                ['bankaccounts.status', true]
            ])
            ->get();
    }
    
}
