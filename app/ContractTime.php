<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractTime extends Model
{
    //
    protected $table = "contracttimes";

    public function scopeListTimes($query){
        return $query->select('id as code', 'months')
            ->orderBy('months', 'ASC')
            ->get();
    }

}
