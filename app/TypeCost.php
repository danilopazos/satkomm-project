<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCost extends Model
{
    protected $table = 'typecosts';

    public function scopeGetList($query, $companyId)
    {
        return $query->select('id as code', 'description', 'created_at')
            ->where([
                    ['company_id', $companyId]
                ])
            ->get();
    }

    public function scopeNextCode($query, $companyId)
    {
        $maxCode = $query->max('id');

        if ($maxCode == null){
            $newCode = "C001";

        }else{
            $correlative = substr($maxCode, 1);
            $correlative ++; 
            $cut = 4 - (strlen($correlative));
            $newCode = (substr("C000", 0, $cut)).$correlative;

        }

        return $newCode;
    }

    public function scopeGetListForCombo($query, $companyId)
    {
        return $query->select('id as code', 'description')
            ->where([
                    ['company_id', $companyId]
                ])
            ->get();
    }

}
