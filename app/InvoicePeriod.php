<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InvoicePeriod extends Model
{
    //
    protected $table = "invoiceperiods";

    public function scopeGetNextCode($query)
    {
        $maxId = $query->max('id');
        return $maxId + 1;
    }

    public function scopeGetListComplete($query, $period)
    {
        $period = substr($period, 0, 7);

        return $query->select('sancode')
            ->where([
                ['period', $period],
                ['status', true]
            ])
            ->get();
    }

    public function scopeGetListAllInvoices($query, $companyId)
    {
        return $query->select('invoiceperiods.id as code', 'invoiceperiods.period', 'invoiceperiods.sancode', 'typeinvoices.description as typedoc',
                                'invoiceperiods.principalorder_id', 'invoiceperiods.numberdocument', 'invoiceperiods.doc_dateissue', 'invoiceperiods.usdamount')
            ->join('typeinvoices', 'invoiceperiods.typeinvoice_id', '=', 'typeinvoices.id')
            ->where([
                ['invoiceperiods.company_id', $companyId],
                ['invoiceperiods.status', true]
            ])
            ->get();
    }

}
