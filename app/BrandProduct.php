<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandProduct extends Model
{
    //
    public $table = "brandsproducts";

    public function scopeListBrands($query, $productId)
    {
        return $query->select('id as code', 'description')
            ->where('typeproduct_id', '=', $productId)
            ->get();
    }

}
