<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeDocument extends Model
{
    //

    protected $table = "typedocuments";

    public function scopeListDocuments($query)
    {
        return $query->select('id as code', 'description')
            ->get();
    }

}
