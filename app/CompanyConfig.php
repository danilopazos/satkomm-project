<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyConfig extends Model
{
    //
    protected $table = "companiesconfig";

    public function scopeGetInfoDocumentsDue($query, $companyId){
        $response = $query->select('value')
            ->where([
                    ['textconfig_id', 'T01'],
                    ['company_id', $companyId]
                ])
            ->first();
            
        $defaultDue = 3;
        if(isset($response->value)){
            $defaultDue = $response->value;
        }

        return $defaultDue;

    }

    public function scopeExistInfoDocumentDue($query, $companyId){
        $response = $query->select('value')
            ->where([
                    ['textconfig_id', 'T01'],
                    ['company_id', $companyId]
                ])
            ->first();
        
        $exist = false;
        if(isset($response->value)){
            $exist = true;
        }

        return $exist;
    }

}
