<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDiscount extends Model
{
    protected $table = 'orddiscounts';

    public function scopeNextCode($query, $orderId)
    {
        $maxCode = $query->select('id')
            ->where('order_id', $orderId)
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->first();
        if(isset($maxCode->id)){
            $newCode = $maxCode->id + 1;
        }else{
            $newCode = 1;
        }
        return ($newCode);
    }

    public function scopeGetLisPerOrder($query, $orderId)
    {
        return $query->select('id as code', 'description', 'discount_amount', 'date_start', 'date_end')
            ->where([
                ['order_id', $orderId],
                ['status', true]
            ])
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function scopeGetDocument($query, $orderId, $discountId)
    {
        return $query->select('attach')
            ->where([
                ['id', $discountId],
                ['order_id', $orderId]
            ])
            ->firstOrFail();
    }

    public function scopeGetTotalDiscuount($query, $orderId, $periodStart, $periodEnd)
    {
        $listDiscounts = $query->select('discount_amount', 'date_start', 'date_end')
                            ->where([
                                ['order_id', $orderId],
                                ['status', true]
                            ])
                            ->orderBy('created_at', 'DESC')
                            ->get();
        
        $totalDiscount = 0;

        foreach($listDiscounts as $discount){
            if($discount->date_start <= $periodEnd and $discount->date_end >= $periodStart)
            {
                $totalDiscount += $discount->discount_amount;
            }
        }

        return $totalDiscount;
    }
}
