<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $table = "address";

    public function scopeNewAddress($query)
    {
        $maxCode = $query->max('id'); //A001
        $correlative = substr($maxCode, 1); //001
        $correlative ++; //2
        $cut = 4 - (strlen($correlative)); //4-1 = 3
        $newCode = (substr("A000", 0, $cut)).$correlative;

        return $newCode;
    }

    public function scopeGetAddressName($query, $addressId){

        $name = "";

        if(strlen($addressId) == 4){
            $name = $query->select('addressname')
                ->where('id', $addressId)
                ->first()->addressname;
        }

        return $name;

    }

    public function scopeGetBillAddress($query, $companyId)
    {
        return $query->select('address.addressname', 'address.address', 'countries.name as country', 'departments.name as department', 'provinces.name as province', 'districts.name as district', 'address.postal_code')
            ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('countries', 'departments.country_id', '=', 'countries.id')
            ->where([
                ['address.company_id', '=', $companyId],
                ['companies_address.typeaddress_id', '=', 'FCT']
            ])
            ->first();
    }

    public function scopeGetBillAddressToEdit($query, $companyId)
    {
        return $query->select('address.id as code', 'address.addressname', 'address.address', 'countries.id as country', 'departments.id as department', 'provinces.id as province', 'districts.id as district', 'address.postal_code')
        ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
        ->join('districts', 'address.district_id', '=', 'districts.id')
        ->join('provinces', 'districts.province_id', '=', 'provinces.id')
        ->join('departments', 'provinces.department_id', '=', 'departments.id')
        ->join('countries', 'departments.country_id', '=', 'countries.id')
        ->where([
            ['address.company_id', '=', $companyId],
            ['companies_address.typeaddress_id', '=', 'FCT']
        ])
        ->first();
    }

    public function scopeGetAllAddress($query, $companyId)
    {
        return $query->select('address.id as code', 'typeaddress.name as typeaddress', 'address.addressname', 'address.address', 'countries.name as country', 'departments.name as department', 'provinces.name as province', 'districts.name as district', 'address.postal_code', 'companies_address.status as status')
            ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
            ->join('typeaddress', 'companies_address.typeaddress_id', '=', 'typeaddress.id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('countries', 'departments.country_id', '=', 'countries.id')
            ->where([
                ['address.company_id', '=', $companyId]
            ])
            ->get();
    }

    public function scopeGetCodeCompany($query, $addressId)
    {
        return $query->select('company_id')
            ->where('id', $addressId)
            ->firstOrFail();
    }

    public function scopeGetAddressInformation($query, $addressId)
    {
        return $query->select('address.id as code', 'companies.id as companyCode', 'companies.name as company', 'address.addressname', 'address.address', 'address.coordinates', 'address.postal_code',
                                'companies_address.typeaddress_id', 'departments.country_id as country', 'provinces.department_id as department', 'districts.province_id as province', 'address.district_id as district')
            ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
            ->join('companies', 'address.company_id', '=', 'companies.id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->where([
                ['address.id', $addressId],
                ['address.status', true]
            ])
            ->firstOrFail();
    }

    public function scopeGetAddressProviderToEdit($query, $providerId)
    {
        return $query->select('address.id as code', 'address.addressname', 'address.address', 'countries.id as country', 'departments.id as department', 'provinces.id as province', 'districts.id as district', 'address.postal_code')
        ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
        ->join('districts', 'address.district_id', '=', 'districts.id')
        ->join('provinces', 'districts.province_id', '=', 'provinces.id')
        ->join('departments', 'provinces.department_id', '=', 'departments.id')
        ->join('countries', 'departments.country_id', '=', 'countries.id')
        ->where([
            ['address.company_id', '=', $providerId],
            ['companies_address.typeaddress_id', '=', 'FCT']
        ])
        ->first();
    }

}
