<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeProduct extends Model
{
    //
    protected $table = "typesproducts";

    public function scopeListProducts($query, $categoryId)
    {
        return $query->select('id as code', 'description')
            ->where('sellcategory_id', '=', $categoryId)
            ->get();
    }

}
