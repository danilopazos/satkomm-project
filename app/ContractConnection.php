<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractConnection extends Model
{
    //
    protected $table = "contractconnections";

    public function scopeListConnections($query){
        return $query->select('id as code', 'min', 'max')
            ->orderBy('id', 'ASC')
            ->get();
    }

}
