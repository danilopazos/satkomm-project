<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeInfoUser($query, $userId)
    {
        return $query->select('users.id', 'email', 'name', 'lastname', 'imgprofile', 'telephone', 'cellphone', 'jobtitles.description', 'company_id', 'usertype_id', 'default_language')
            ->join('jobtitles', 'users.jobtitle_id', '=','jobtitles.id')
            ->where('users.id', '=', $userId)
            ->first();
    }

    public function scopeListUsers($query){
        return $query->select('users.id as code', 'users.email', 'password', 'users.name', 'users.lastname', 'companies.name as company', 'jobtitles.description as jobtitle', 'usertypes.description as usertype')
            ->join('companies', 'users.company_id', '=', 'companies.id')
            ->join('jobtitles', 'users.jobtitle_id', '=', 'jobtitles.id')
            ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
            ->where([
                ['usertype_id', '!=', 'ALLS']
            ])
            ->get();
    }

    public function scopeNextCode($query)
    {
        $maxCode = $query->max('id');
        return ($maxCode + 1);
    }


}
