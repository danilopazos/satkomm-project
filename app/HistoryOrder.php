<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryOrder extends Model
{
    //
    protected $table = "historyorders";

    public function scopeNextCode($query, $orderId){
        $maxCode = $query->select('id')
            ->where([
                ['order_id', $orderId]
            ])
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->first();
        if(isset($maxCode->id)){
            $newCode = $maxCode->id + 1;
        }else{
            $newCode = 1;
        }
        return $newCode;
    }

    public function scopeGetList($query, $orderId, $language){
        return $query->select('historyorders.id as code', 'historystatus.description', 'historyorders.anotations', 'users.imgprofile', 'users.name', 'users.lastname', 
                                'historyorders.created_at', 'icon', 'colortext', 'weighttext', 'attach')
            ->join('historystatus', 'historyorders.historystatus_id', '=', 'historystatus.id')
            ->join('users', 'historyorders.user_id', '=', 'users.id')
            ->where([
                ['historystatus.language', $language],
                ['historyorders.order_id', $orderId]
            ])
            ->orderBy('historyorders.created_at', 'DESC')
            ->get();
    }

    public function scopeGetLastFive($query, $orderId, $language){
        return $query->select('historyorders.id as code', 'historystatus.description', 'historyorders.anotations', 'users.imgprofile', 'users.name', 'users.lastname', 
                                'historyorders.created_at', 'icon', 'colortext', 'weighttext', 'attach')
            ->join('historystatus', 'historyorders.historystatus_id', '=', 'historystatus.id')
            ->join('users', 'historyorders.user_id', '=', 'users.id')
            ->where([
                ['historystatus.language', $language],
                ['historyorders.order_id', $orderId]
            ])
            ->orderBy('historyorders.created_at', 'DESC')
            ->limit(5)
            ->get();
    }
    
    public function scopeGetInfoToDownload($query, $orderId, $historyId)
    {
        return $query->select('attach')
            ->where([
                ['id', $historyId],
                ['order_id', $orderId]
            ])
            ->firstOrFail();
    }
}
