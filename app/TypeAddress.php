<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAddress extends Model
{
    //
    protected $table = "typeaddress";
    public $timestamps = false;

    public function scopeGetList($query)
    {
        return $query->select('id as code', 'name')
            ->where('id', '<>', 'FCT')
            ->get();
    }

    public function scopeGetListComplete($query)
    {
        return $query->select('id as code', 'name')
            ->get();
    }

}
