<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyAdress extends Model
{
    //
    protected $table = "companies_address";
    public $timestamps = false;

    public function scopeGetAllAddress($query, $companyId){

        return $query->select('address.id as code', 'addressname', 'address', 'districts.name as district', 'provinces.name as province', 'departments.name as department', 'countries.name as country')
            ->join('typeaddress', 'companies_address.typeaddress_id', '=', 'typeaddress.id')
            ->join('address', 'companies_address.address_id', '=', 'address.id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('countries', 'departments.country_id', '=', 'countries.id')
            ->where([
                    ['address.company_id', '=', $companyId],
                    ['address.status', '=', true],
                    ['companies_address.status', '=', true]
                ])
            ->get();

    }
}
