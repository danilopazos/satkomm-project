<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProvider extends Model
{
    //
    protected $table = 'companies_providers';
    protected $primaryKey = ['company_id', 'provider_id'];

    public $incrementing = false;
    public $timestamps = false;

    public function scopeListProviders($query, $companyId)
    {
        return $query->select('provider_id', 'companies.name', 'typedocuments.description as typedocument', 'taxnumber', 'districts.name as district', 'provinces.name as province')
            ->join('companies', 'companies_providers.provider_id', '=', 'companies.id')
            ->join('typedocuments', 'companies.typedocument_id', '=', 'typedocuments.id')
            ->join('address', 'companies.id', '=', 'address.company_id')
            ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->where([
                ['companies_providers.company_id', '=', $companyId],
                ['companies_address.typeaddress_id', '=', 'FCT'],
                ['companies_address.status', '=', true]
            ])
            ->get();
    }

    public function scopeValidateCompanyIsHere($query, $companyId, $providerId)
    {
        return $query->select('company_id')
            ->where([
                ['company_id', $companyId],
                ['provider_id', $providerId],
                ['status', true]
            ])
            ->first();
    }

    public function scopeGetListProvider($query, $companyId)
    {
        return $query->select('companies.id as code', 'companies.name')
            ->join('companies', 'companies_providers.provider_id', '=', 'companies.id')
            ->where([
                ['companies_providers.company_id', $companyId],
                ['companies_providers.status', true],
            ])
            ->orderBy('name', 'asc')
            ->get();
    }

}
