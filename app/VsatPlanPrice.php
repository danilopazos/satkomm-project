<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VsatPlanPrice extends Model
{
    //
    protected $primaryKey = ['contracttime_id', 'contractconnection_id', 'vsatplan_id'];
    public $incrementing = false;
    protected $table = "vsatplans_prices";
    protected $fillable = ['contracttime_id', 'contractconnection_id', 'vsatplan_id', 'customer_id', 'usdprice'];
    public $timestamps = false;

    public function scopeGetListPrices($query, $planId, $customerId){
        return $query->where([
                    ['vsatplan_id', '=', $planId],
                    ['customer_id', '=', $customerId]
                ])
            ->get();
    }

    public function scopeGetListPriceForOrderService($query, $planId, $customerId)
    {
        return $query->select('contracttime_id', 'contractconnection_id', 'contracttimes.months', 'contractconnections.min', 'contractconnections.max', 'usdprice')
            ->join('contracttimes', 'vsatplans_prices.contracttime_id', '=', 'contracttimes.id')
            ->join('contractconnections', 'vsatplans_prices.contractconnection_id', '=', 'contractconnections.id')
            ->where([
                    ['vsatplan_id', '=', $planId],
                    ['customer_id', '=', $customerId]
                ])
            ->orderBy('months', 'DESC')
            ->get();
    }

    public function scopeGetCountPerCustomer($query, $customerId){
        return $query->select('vsatplan_id', DB::raw('count(vsatplan_id) as qtyPrices'))
            ->where([
                ['customer_id', $customerId]
            ])
            ->groupBy('vsatplan_id')
            ->get();
    }

    public function scopeValidateExist($query, $time, $connection, $plan, $customer)
    {
        return $query->select('usdprice')
            ->where([
                ['contracttime_id', $time],
                ['contractconnection_id', $connection],
                ['vsatplan_id', $plan],
                ['customer_id', $customer]
            ])
            ->count();
    }

    public function scopeGetPostpaidAvailables($query, $companyId, $band, $customerCode)
    {
        return $query->select('vsatplans_prices.vsatplan_id as code', 'vsatplans.description', 'vsatrecords.description as band')
            ->join('vsatplans', 'vsatplans_prices.vsatplan_id', '=', 'vsatplans.id')
            ->join('vsatrecords', 'vsatplans.vsatrecord_id', '=', 'vsatrecords.id')
            ->where([
                ['vsatplans_prices.customer_id', $customerCode],
                ['vsatplans.typeplan_id', 'PS'],
                ['vsatplans.status', true],
                ['vsatrecords.id', $band],
                ['vsatrecords.company_id', $companyId],
                ['vsatrecords.status', true]
            ])
            ->groupBy('vsatplans_prices.vsatplan_id')
            ->get();
    }

}
