<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextDetailInventory extends Model
{
    //
    protected $table = "textdetailinventories";
    public $timestamps = false;

    public function scopeInfoRelations($query, $inventoryId)
    {
        return $query->select('textdetailinventories.textrelation_id as code', 'namespecifications.title', 'description')
            ->join('textrelations', 'textrelation_id', '=', 'textrelations.id')
            ->join('namespecifications', 'textrelations.namespecification_id', '=', 'namespecifications.id')
            ->where([
                ['inventoryitem_id', '=', $inventoryId],
                ['status', '=', true]
            ])
            ->get();
    }
}
