<?php

namespace App\Http\Controllers\Me;

use App\MyNote;
use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MeController extends Controller
{
    public function myNotes()
    {
        $encryptNotes = MyNote::GetNotes(Auth()->user()->id);
        $arrayNotes = [];

        foreach ($encryptNotes as $note){
            $unitNote = [
                'code' => $note->code,
                'title' => substr(decrypt($note->title) , 0, 42),
                'body' => substr(decrypt($note->body), 0, 122),
                'day' => $note->updated_at->format('d'),
                'month' => $note->updated_at->format('m'),
                'year' => $note->updated_at->format('Y - H:i')
            ];
            array_push($arrayNotes, $unitNote);
        }

        return view('me.notes', [
            'arrayNotes' => $arrayNotes
        ]);
    }

    public function newNote()
    {
        return view('me.newnote');
    }

    public function saveNewNote(Request $request)
    {
        if(strlen($request->hiddenCode) > 0){
            $codeNote = $request->hiddenCode;
            $updateNote = MyNote::find($request->hiddenCode);
                $updateNote->title = encrypt(trim($request->textTitle));
                $updateNote->body = encrypt(trim($request->textBody));
                $updateNote->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $is_saved = $updateNote->save();

        }else{
            $codeNote = MyNote::NextCode();
            $newNote = new MyNote();
                $newNote->id = $codeNote;
                $newNote->user_id = Auth()->user()->id;
                $newNote->title = encrypt(trim($request->textTitle));
                $newNote->body = encrypt(trim($request->textBody));
                $newNote->status = true;
                $newNote->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newNote->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $is_saved = $newNote->save();

        }

        if($is_saved){
            $code = $codeNote;
        }else{
            $code = "not_save";
        }

        echo $code;
    }

    public function viewNote($id)
    {
        $encrypNote = MyNote::GetInfoNote(Auth()->user()->id, $id);
        $note = [
            'code' => $encrypNote->code,
            'title' => decrypt($encrypNote->title),
            'body' => decrypt($encrypNote->body)
        ];

        return view('me.viewnote', [
            'note' => $note
        ]);
    }

    public function deleteNote(Request $request)
    {
        DB::table('mynotes')
            ->where([
                ['id', '=', $request->hiddenCode],
                ['user_id', '=', Auth()->user()->id]
            ])
            ->update([
                'status' => false
            ]);
        echo true;

    }

}
