<?php

namespace App\Http\Controllers;

use App\InvoicePeriod;
use App\ItemAvailable;
use App\OrderService;
use App\ConsumptionReceipt;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $companyId = Auth::user()->company_id;
        //Ejm: en julio, facturamos lo de junio
        if(isset($request->period)){
            $periodNow = $request->period;
        }else{
            $periodNow = Carbon::now('America/Lima')->format("Y-m");
        }

        $periodNowStart = $periodNow."-01";
        $tmp = date("Y-m-d", strtotime($periodNowStart."+ 1 month"));
        $periodNowEnd = date("Y-m-d", strtotime($tmp."- 1 day"));

        $periodLast = date("Y-m", strtotime($periodNowStart."- 1 month"));
        $periodLastStart = $periodLast."-01";
        $periodLastEnd = date("Y-m-d", strtotime($periodNowStart."- 1 day"));

        $postpaidOrders = OrderService::GetQtyCompaniesOrdersActivations($companyId, $periodLastEnd);
        $postpaidCancels = OrderService::QtyCompaniesPerOrderPeriod($companyId, $periodLastStart, $periodLastEnd);

        $qtyCompanies = $postpaidOrders->count();

        foreach($postpaidCancels as $cancel){
            $validate = true;
            foreach($postpaidOrders as $active){
                if($active->code == $cancel->code){
                    $validate = false;
                } 
            }
            if($validate){
                $qtyCompanies ++;
            }
        }
        
        $services = ConsumptionReceipt::GetListCompaniesNoReceipts($companyId, $periodLast)->count();
        
        $activations = OrderService::GetQtyActivations($companyId, $periodNowStart, $periodNowEnd)->count('id');
        $toDue = OrderService::GetQtyToDue($companyId, $periodNowStart, $periodNowEnd)->count('id');
       

        if($qtyCompanies == 0){
            $percent = 0;
        }else{
            $percent = ($services / $qtyCompanies) * 100;
        }

        $months = ConsumptionReceipt::GetListMonths($companyId);
        $earnings = ConsumptionReceipt::GetEarningsPerPeriod($companyId, $periodNowStart, $periodNowEnd);

        $dataSummary = [
            'services' => $services,
            'fullServices' => $qtyCompanies, //ok
            'percentServices' => $percent,
            'earnings' => $earnings->sum('usdtotal'),
            'activations' => $activations, //ok
            'toDue' => $toDue //ok
        ];

        return view('dashboard.home', [
            'period' => $periodNow,
            'dataSummary' => $dataSummary,
            'months' => $months
        ]);
    }
}
