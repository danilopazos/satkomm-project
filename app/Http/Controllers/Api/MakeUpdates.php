<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\HistoryOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MakeUpdates extends Controller
{
    public function updateDisableAlert(Request $request)
    {
        DB::table('alertcenter')
            ->where([
                ['alert_id', '=', $request->id],
                ['receiver_id', '=', Auth()->user()->id],
                ['status', '=', true]
            ])
            ->update([
                'status' => false,
                'updated_at' => Carbon::now('America/Lima')->format("Y-m-d H:i:s")
            ]);
    }

    public function updateApproveOrderService(Request $request){
        $is_upated = DB::table('ordservices')
            ->where([
                ['id', '=', $request->orderId],
                ['provider_id', '=', Auth()->user()->company_id],
                ['status', '=', true]
            ])
            ->update([
                'admin_validation' => 'AP'
            ]);

        if($is_upated){
            if(isset($request->notes)){
                $historyStatus = 'AWP'; //approved with problems
                $anotations = $request->notes;

            }else{
                $historyStatus = 'AOK'; //without problems
                $anotations = null;

            }

            $newCode = HistoryOrder::NextCode($request->orderId);
            $newHistory = new HistoryOrder();
                $newHistory->id = $newCode;
                $newHistory->order_id = $request->orderId;
                $newHistory->historystatus_id = $historyStatus;
                $newHistory->anotations = $anotations;
                $newHistory->user_id = Auth()->user()->id;
                $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newHistory->save();
        }
        echo $is_upated;
    }

    public function updateDisapproveOrderService(Request $request)
    {
        $is_upated = DB::table('ordservices')
            ->where([
                ['id', '=', $request->orderId],
                ['provider_id', '=', Auth()->user()->company_id],
                ['status', '=', true]
            ])
            ->update([
                'admin_validation' => 'DS'
            ]);

        if($is_upated){
            $newCode = HistoryOrder::NextCode($request->orderId);
            $newHistory = new HistoryOrder();
                $newHistory->id = $newCode;
                $newHistory->order_id = $request->orderId;
                $newHistory->historystatus_id = 'DSP';
                $newHistory->anotations = null;
                $newHistory->user_id = Auth()->user()->id;
                $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newHistory->save();
        }
        echo $is_upated;
    }
}
