<?php

namespace App\Http\Controllers\Api;

use App\AlertCenter;
use App\BrandProduct;
use App\Company;
use App\CompanyAdress;
use App\InventoryItem;
use App\InvoicePeriod;
use App\ItemAvailable;
use App\ModelProduct;
use App\TypeInvoice;
use App\TypeProduct;
use App\User;
use App\Department;
use App\District;
use App\Province;
use App\Currency;
use App\CompaniesCurrencies;
use App\Bank;
use App\ConsumptionReceipt;
use App\OrderService;

use DateTime;

use App\VsatPlan;
use App\VsatPlanPrice;
use App\VsatPrepaidPrice;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class GetList extends Controller
{

    public function getDepartments(Request $request)
    {
        return Department::ListDepartments($request->code);
    }

    public function getProvinces(Request $request)
    {
        return Province::ListProvinces($request->code);
    }

    public function getDistricts(Request $request)
    {
        return District::ListDistricts($request->code);
    }

    public function getTypeProducts(Request $request)
    {
        return TypeProduct::ListProducts($request->code);
    }

    public function getListBrands(Request $request)
    {
        return BrandProduct::ListBrands($request->code);
    }

    public function getListModels(Request $request)
    {
        return ModelProduct::ListModels($request->code);
    }

    public function getListCustomers()
    {
        return Company::CustomerListSimple(Auth()->user()->company_id);
    }

    public function getAddressCustomers(Request $request)
    {
        return CompanyAdress::GetAllAddress($request->code);
    }

    public function getListPlans(Request $request)
    {
        if($request->plan == 'PS'){
            return VsatPlanPrice::GetPostpaidAvailables(Auth()->user()->company_id, $request->band, $request->customerCode);   
        }else{
            return VsatPrepaidPrice::GetPrepaidAvailables(Auth()->user()->company_id, $request->band, $request->customerCode, $request->plan);
        }
    }
    public function getListInventoryForNewOrder(Request $request)
    {
        return InventoryItem::GetListForNewOrder(Auth()->user()->company_id, $request->type);
    }

    public function getListPlanPrices(Request $request)
    {
        if($request->plan == "PS"){
            //postpaid
            return VsatPlanPrice::GetListPriceForOrderService($request->code, $request->customer);

        }else{
            //token or aditionals
            return VsatPrepaidPrice::GetListPrices($request->code, $request->customer);

        }
    }

    public function getListTypeInvoice()
    {
        return TypeInvoice::ListDocuments();
    }

    public function getListEarningsMonths()
    {
        return InvoicePeriod::GetListMonths(Auth()->user()->company_id);
    }

    public function getListEarningsByMonths(Request $request)
    {
        return ConsumptionReceipt::GetListEarningsByMonths(Auth()->user()->company_id, $request->period);
    }

    public function getListInventory()
    {
        return ItemAvailable::ListInventoryStatys(Auth()->user()->company_id);
    }

    public function getQtyAlertMessages()
    {
        return AlertCenter::GetQtyAlerts(Auth()->user()->id);
    }

    public function getAlertMessages()
    {
        return AlertCenter::GetAlertsMessages(Auth()->user()->id, Auth()->user()->default_language);
    }

    public function getListCurrencies()
    {
        $codesList = CompaniesCurrencies::GetCodesList(Auth()->user()->company_id);
        return Currency::ListCurrencies($codesList);
    }

    public function getMyListCurrencies()
    {
        return CompaniesCurrencies::GetMyCurrenciesList(Auth()->user()->company_id);
    }

    public function getListBanks()
    {
        return Bank::GetList();
    }

    public function getTotalPendings()
    {
        return OrderService::GetCountPendings(Auth()->user()->company_id);
    }

    public function getListPendings(){
        return OrderService::GetListPendings(Auth()->user()->company_id);
    }

    public function getDateDueOrder($orderid){
        $info = OrderService::select('date_due')
                        ->where([
                                ['id', $orderid]
                            ])
                        ->first();

        $date_end = new DateTime($info->date_due->format('Y-m-d'));
        
        $today = new DateTime(Carbon::now('America/Lima')->format('Y-m-d'));
        //dd($today);
        $interbal = date_diff($today, $date_end);
        $daysInterval = (int)$interbal->format('%R%a');

        echo $daysInterval;
    }
}