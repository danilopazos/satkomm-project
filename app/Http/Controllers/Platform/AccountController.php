<?php

namespace App\Http\Controllers\Platform;

use App;
use App\Company;
use App\Jobtitle;
use App\User;

use App\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AccountController extends Controller
{
    public function __construct()
    {
        if($this->middleware('auth')){
            $this->middleware(function ($request, $next){
                //Only Super Administrator
                if(Auth::user()->usertype_id == "ALLS") {
                    App::setLocale(Auth::user()->default_language);
                    return $next($request);

                }else{
                    return redirect('/error/nopermission');

                }
            });
        }
    }

    public function showUsers()
    {
        return view('platform.usersaccounts', [
            'listUsers' => User::ListUsers()
        ]);
    }

    public function newUserAccount()
    {

        $listCompanies = Company::CompaniesForAccount();
        $listJobtitles = Jobtitle::GetList();
        $listTypes = UserType::GetList();

        return view('platform.newaccount', [
            'listCompanies' => $listCompanies,
            'listJobtitles' => $listJobtitles,
            'listTypes' => $listTypes
        ]);
    }

    public function createAccount(Request $request)
    {
        $codeUser = User::NextCode();
        $password = Hash::make($request->textPassword);

        $user = new User();
            $user->id = $codeUser;
            $user->name = $request->textName;
            $user->lastname = $request->textLastName;
            $user->imgprofile = "/profileimage/default.png";
            $user->email = $request->textEmail;
            $user->telephone = $request->textTelepohne;
            $user->cellphone = $request->textCellphone;
            $user->company_id = $request->selectCompany;
            $user->jobtitle_id = $request->selectJobtitle;
            $user->usertype_id = $request->selectUserType;
            $user->default_language = $request->selectLanguage;
            $user->email_verified_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $user->password = $password;
        $user->save();

        return redirect('/admin/showusers')->with('status', true);
    }

    public function ResetPassword(User $user)
    {
        if($user->usertype_id != 'ALLS'){
            $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
            $is_saved = $user->save();

            echo $is_saved;
        }else{
            return false;
        }
    }

    public function blockAccount(User $user)
    {
        if($user->usertype_id != 'ALLS'){
            $user->password = '';
            $is_saved = $user->save();

            echo $is_saved;
        }else{
            return false;
        }
    }
}
