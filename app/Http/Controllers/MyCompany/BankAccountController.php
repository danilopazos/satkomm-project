<?php

namespace App\Http\Controllers\MyCompany;

use App\BankAccount;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankAccountController extends Controller
{
    
    public function listBanks()
    {
        $listBanks = BankAccount::GetList(Auth()->user()->company_id);

        return view('mycompany.bankaccounts', [
            'listBanks' => $listBanks
        ]);
    }

    public function saveAccount(Request $request)
    {
        $newCode = BankAccount::NewCode();
        $newBank = new BankAccount();
            $newBank->id = $newCode;
            $newBank->company_id = Auth()->user()->company_id;
            $newBank->bank_id = $request->codeBank;
            $newBank->description = null;
            $newBank->currency_id = $request->codeCurrency;
            $newBank->number_account = $request->numberAccount;
            $newBank->cci_account = $request->cciAccount;
            $newBank->status = true;
            $newBank->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newBank->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $is_saved = $newBank->save();

        echo $is_saved;
    }
}
