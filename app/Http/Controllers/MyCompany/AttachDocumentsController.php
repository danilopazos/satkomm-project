<?php

namespace App\Http\Controllers\MyCompany;

use App\CompanyCustomer;
use App\TypeAttachment;
use App\CompanyContract;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\File;
use Spatie\Dropbox\Client;
use Illuminate\Support\Facades\Storage;

class AttachDocumentsController extends Controller
{
    public function showPage($id)
    {
        $listType = TypeAttachment::GetList(Auth()->user()->default_language);
        $isCustomer = CompanyCustomer::ValidateCompanyIsHere(Auth()->user()->company_id, $id);

        if(isset($isCustomer->customer_id)){
            return view('mycompany.showattach', [
                'companyCode' => $id,
                'listType' => $listType
            ]);

        }else{
            abort(404);

        }
    }

    public function upload(Request $request)
    {
        $tmpName = $request->file('fileAttach')->getClientOriginalName();
        $tmpName = str_replace(' ', '-', $tmpName);
        $tmpName = str_replace('_', '-', $tmpName);
        
        $filename = $request->customerCode.'_'.Carbon::now('America/Lima')->format("YmdHis").'_'.$tmpName;

        $newCode = CompanyContract::NextCode($request->customerCode);
        $newContract = new CompanyContract();
            $newContract->id = $newCode;
            $newContract->customer_id = $request->customerCode;
            $newContract->provider_id = Auth()->user()->company_id;
            $newContract->typeattach_id = $request->typeAttach;
            $newContract->document_attach = $filename;
            $newContract->anotations = $request->textAnotations;
            $newContract->date_expedition = $request->dateExpedition;
            $newContract->date_expiration = $request->dateExpiration;
            $newContract->status = true;
            $newContract->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newContract->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $is_saved = $newContract->save();

        if($is_saved){
            $infoStorage = Storage::disk('dropbox')->putFileAs(
                '/CustomerContracts', 
                $request->file('fileAttach'), 
                $filename
            );

            /*
            Cambiar el https://www.dropbox.com por https://dl.dropboxusercontent.com
            La ruta viene en el elemento : 'url' del array

            $response = Storage::disk('dropbox')->getDriver()->getAdapter()->getClient()->createSharedLinkWithSettings(
                $infoStorage, 
                ["requested_visibility" => "public"]
            );

            dd($response);
            */
            
        }

        return back()->with('status', true);
    }

    public function download($filename){
        return Storage::disk('dropbox')->download('/CustomerContracts/'.$filename);

    }

}
