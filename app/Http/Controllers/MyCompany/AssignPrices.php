<?php

namespace App\Http\Controllers\MyCompany;

use App\VsatPlan;
use App\VsatPlanPrice;
use App\VsatPrepaidPrice;
use App\ContractConnection;
use App\ContractTime;
use App\Company;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AssignPrices extends Controller
{
    public function showAssignPrices($companyId)
    {
        $listPlans = VsatPlan::ListPricesPostpaidByCustomer(Auth()->user()->company_id);
        $postpaidPrices = VsatPlanPrice::GetCountPerCustomer($companyId);
        $prepaidPrices = VsatPrepaidPrice::GetCountPerCustomer($companyId);
        $companyName = Company::GetName($companyId);

        return view('mycompany.assignprices', [
            'customerId' => $companyId,
            'listPlans' => $listPlans,
            'postpaidPrices' => $postpaidPrices,
            'prepaidPrices' => $prepaidPrices,
            'companyName' => $companyName
        ]);
    }

    public function getListCompleteWithPrices(Request $request)
    {
        $arrayPrices = [];
        
        if($request->plan == "PS"){
            $postpaidPrices = VsatPlanPrice::GetListPrices($request->code, $request->customer);
            $allTimes = ContractTime::ListTimes();
            $allConnections = ContractConnection::ListConnections();

            foreach($allTimes as $time){
                foreach($allConnections as $connection){
                    $sellPrice = '';
                    foreach($postpaidPrices as $postpaid){
                        if($postpaid->contracttime_id == $time->code and $postpaid->contractconnection_id == $connection->code){
                            $sellPrice = $postpaid->usdprice;

                        }
                    }

                    array_push($arrayPrices, array(
                                                'connection_id' => $connection->code,
                                                'minText' => $connection->min,
                                                'maxText' => $connection->max,
                                                'time_id' => $time->code,
                                                'monthsText' => $time->months,
                                                'usdprice' => $sellPrice
                                            ));

                }
            }

        }else{
            $prepaidPrices = VsatPrepaidPrice::GetListPrices($request->code, $request->customer);
            foreach($prepaidPrices as $prepaid){
                array_push($arrayPrices, array(
                                            'id' => $prepaid->id,
                                            'anotationText' => $prepaid->anotation,
                                            'usdprice' => $prepaid->usd_price
                                        ));
            }

        }

        return ($arrayPrices);
    }

    public function savePrices(Request $request)
    {
        if($request->typePlan == "PS"){
            $allTimes = ContractTime::ListTimes();
            $allConnections = ContractConnection::ListConnections();

            foreach($allTimes as $time){
                foreach($allConnections as $connection){

                    $attachPrice = 'price_'.$request->codePlan.'_'.$time->code.'_'.$connection->code;

                    $exist = VsatPlanPrice::ValidateExist($time->code, $connection->code, $request->codePlan, $request->customerId);

                    if($exist > 0){
                        //update
                        DB::table('vsatplans_prices')
                            ->where([
                                ['contracttime_id', $time->code],
                                ['contractconnection_id', $connection->code],
                                ['vsatplan_id', $request->codePlan],
                                ['customer_id', $request->customerId]
                            ])
                            ->update([
                                'usdprice' => $request->$attachPrice
                            ]);
                    }else{
                        //save
                        $vsatPlanPrice = new VsatPlanPrice();
                            $vsatPlanPrice->contracttime_id = $time->code;
                            $vsatPlanPrice->contractconnection_id = $connection->code;
                            $vsatPlanPrice->vsatplan_id = $request->codePlan;
                            $vsatPlanPrice->customer_id = $request->customerId;
                            $vsatPlanPrice->usdprice = $request->$attachPrice;
                        $vsatPlanPrice->save();

                    }
                }
            }
            
        }else{
            $prepaidPrices = VsatPrepaidPrice::GetListPrices($request->codePlan, $request->customerId);

            foreach($prepaidPrices as $prepaid){
                $attachPrice = 'price_'.$request->codePlan.'_'.$prepaid->id;

                DB::table('vsatprepaid_prices')
                    ->where([
                        ['id', $prepaid->id]
                    ])
                    ->update([
                        'usd_price' => $request->$attachPrice
                    ]);
            }

            for($i = 1; $i <= $request->numberAds; $i++){
                $newCode = VsatPrepaidPrice::NextCode();

                $attachNote = 'anotation_new_'.$i;
                $attachPrice = 'price_new_'.$i;

                $vsatPrepaidPrices = new VsatPrepaidPrice();
                    $vsatPrepaidPrices->id = $newCode;
                    $vsatPrepaidPrices->customer_id = $request->customerId;
                    $vsatPrepaidPrices->vsatplan_id = $request->codePlan;
                    $vsatPrepaidPrices->anotation = $request->$attachNote;
                    $vsatPrepaidPrices->usd_price = $request->$attachPrice;
                $vsatPrepaidPrices->save();
            }
        }

    }

}
