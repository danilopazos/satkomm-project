<?php

namespace App\Http\Controllers\MyCompany;

use App;
use App\User;
use App\TypeAttachment;
use App\CompanyConfig;
use App\TypeCost;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EditMyCompanyController extends Controller
{
    public function __construct()
    {
        if($this->middleware('auth')){
            $this->middleware(function ($request, $next){
                //Only Super Administrator and Administrators
                if(Auth::user()->usertype_id == "ALLS" or Auth::user()->usertype_id == "ADSY") {
                    App::setLocale(Auth::user()->default_language);
                    return $next($request);

                }else{
                    return redirect('/error/nopermission');

                }
            });
        }
    }

    public function showTimeForDocumentDue()
    {
        $listAttach = TypeAttachment::GetListComplete(Auth::user()->default_language);
        $timeToDue = CompanyConfig::GetInfoDocumentsDue(Auth::user()->company_id);

        return view('mycompany.editdocumentsdue', [
            'listAttach' => $listAttach,
            'timeToDue' => $timeToDue
        ]);
    }

    public function updateMonthsDue(Request $request)
    {
        $existRecord = CompanyConfig::ExistInfoDocumentDue(Auth::user()->company_id);

        if($existRecord){
            //update
            $is_saved = DB::table('companiesconfig')
                ->where([
                    ['company_id', '=', Auth::user()->company_id],
                    ['textconfig_id', '=', 'T01']
                ])
                ->update([
                    'value' => $request->monthsDue
                ]);

        }else{
            //create
            $newConfig = new CompanyConfig();
                $newConfig->company_id = Auth::user()->company_id;
                $newConfig->textconfig_id = 'T01';
                $newConfig->value = $request->monthsDue;
                $newConfig->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newConfig->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $is_saved = $newConfig->save();
        }

        echo $is_saved;
    }

    public function costsRecord()
    {
        $costsList = TypeCost::GetList(Auth::user()->company_id);

        return view('mycompany.costsrecord', [
            'costsList' => $costsList
        ]);
    }

    public function saveCost(Request $request)
    {
        $newCode = TypeCost::NextCode(Auth::user()->company_id);

        $newCost = new TypeCost();
            $newCost->id = $newCode;
            $newCost->company_id = Auth::user()->company_id;
            $newCost->description = $request->description;
            $newCost->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newCost->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $is_saved = $newCost->save();
        
        echo $is_saved;
    }
}