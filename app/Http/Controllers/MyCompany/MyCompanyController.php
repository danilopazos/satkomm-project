<?php

namespace App\Http\Controllers\MyCompany;

use App\Address;
use App\CompaniesCurrencies;
use App\CompanyAdress;
use App\CompanyCustomer;
use App\CompanyProvider;
use App\Country;
use App\Department;
use App\District;
use App\Province;
use App\TypeAddress;
use App\User;
use App\Company;
use App\TypeDocument;
use App\Currency;
use App\CompanyContract;
use App\TypeAttachment;
use App\TypeCustomer;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\File;
use Spatie\Dropbox\Client;
use Illuminate\Support\Facades\Storage;

class MyCompanyController extends Controller
{
    protected function _saveAddress($companyId, $addressName, $textAddress, $districtId, $postalCode, $typeAddress, $coordinates = null)
    {
        $newCodeAddress = Address::NewAddress();
        $address = new Address();
            $address->id = $newCodeAddress;
            $address->company_id = $companyId;
            $address->addressname = $addressName;
            $address->address = $textAddress;
            $address->coordinates = $coordinates;
            $address->district_id = $districtId;
            $address->postal_code = $postalCode;
            $address->status = true;
        $address->save();

        $companyAddress = new CompanyAdress();
            $companyAddress->address_id = $newCodeAddress;
            $companyAddress->typeaddress_id = $typeAddress;
            $companyAddress->status = true;
        $companyAddress->save();
    }

    protected function _saveCompany($companyId, $documentId, $taxNumber, $name, $brand, $mail, $phone, $web)
    {
        $company = new Company();
            $company->id = $companyId;
            $company->typedocument_id = $documentId;
            $company->taxnumber = $taxNumber;
            $company->image = null;
            $company->name = $name;
            $company->brand = $brand;
            $company->mail = $mail;
            $company->telephone = $phone;
            $company->website = $web;
            $company->currency_id = null; //It'll be register when assign user
        $company->save();
    }

    public function myProfile()
    {
        return view('mycompany.mycompany', [
            'company'=>Company::GetInfoCompany(Auth::user()->company_id), 
            'address'=>Address::GetBillAddress(Auth::user()->company_id)
        ]);
    }

    public function myCustomers()
    {
        return view('mycompany.mycustomers', [
                'customers' => CompanyCustomer::ListCustomers(Auth::user()->company_id, Auth::user()->default_language)
            ]);
    }

    public function myProviders()
    {
        return view('mycompany.myproviders', [
            'providers' => CompanyProvider::ListProviders(Auth::user()->company_id)
        ]);
    }

    public function newCustomer()
    {
        return view('mycompany.newcustomer', [
            'documents' => TypeDocument::ListDocuments(),
            'currencies' => CompaniesCurrencies::GetMyCurrenciesList(Auth::user()->company_id),
            'countries' => Country::ListCountries(),
            'listTypeRecord' => TypeCustomer::GetList(Auth::user()->default_language)
        ]);
    }

    public function saveNewCustomer(Request $request)
    {
        //save companies
        $newCodeCompany = Company::NewCode();
            $this->_saveCompany($newCodeCompany, $request->selectTypeDocument, $request->textNumberDocument, $request->textNameCompany, 
                                $request->textBrandCompany, $request->textMailCompany, $request->textPhoneCompany, $request->textWebsiteCompany);
        
        //save companies_customers
        $companyCustomer = new CompanyCustomer();
            $companyCustomer->provider_id = Auth::user()->company_id;
            $companyCustomer->customer_id = $newCodeCompany;
            $companyCustomer->typecustomer_id = $request->selectTypeRecord;
            $companyCustomer->currency_id = $request->selectCurrency;
            $companyCustomer->daystodue = $request->textCreditDays;
            $companyCustomer->status = true;
        $companyCustomer->save();

        $this->_saveAddress($newCodeCompany, $request->textFastAddress, $request->textAddress, $request->selectDistrict, $request->textPostalCode, 'FCT');
        
        echo true;
    }

    public function editProvider($id)
    {
        $address=Address::GetAddressProviderToEdit($id);

        return view('mycompany.editprovider', [
            'documents' => TypeDocument::ListDocuments(),
            'countries' => Country::ListCountries(),
            'departments' => Department::ListDepartments($address->country),
            'provinces' => Province::ListProvinces($address->department),
            'districts' => District::ListDistricts($address->province),
            'customer' => Company::GetInfoProviderToEdit($id, Auth::user()->company_id),
            'address' => $address
        ]);
    }

    public function editCustomer($id)
    {
        $address = Address::GetBillAddressToEdit($id);
        
        return view('mycompany.editcustomer', [
            'documents' => TypeDocument::ListDocuments(),
            'currencies' => CompaniesCurrencies::GetMyCurrenciesList(Auth::user()->company_id),
            'countries' => Country::ListCountries(),
            'departments' => Department::ListDepartments($address->country),
            'provinces' => Province::ListProvinces($address->department),
            'districts' => District::ListDistricts($address->province),
            'customer' => Company::GetInfoToEditCustomer(Auth::user()->company_id, $id),
            'address' => $address,
            'listTypeRecord' => TypeCustomer::GetList(Auth::user()->default_language)
        ]);
    }

    public function updateProvider(Request $request){

        //update companies
        $company = Company::find($request->hiddenCodeCustomer);
            $company->typedocument_id = $request->selectTypeDocument;
            $company->taxnumber = $request->textNumberDocument;
            //$company->image = null;
            $company->name = $request->textNameCompany;
            $company->brand = $request->textBrandCompany;
            $company->mail = $request->textMailCompany;
            $company->telephone = $request->textPhoneCompany;
            $company->website = $request->textWebsiteCompany;
            //$company->currency_id = null; //It'll be register when assign user
        $company->save();

        //update address
        $address = Address::find($request->hiddenCodeAddress);
            $address->addressname = $request->textFastAddress;
            $address->address = $request->textAddress;
            $address->district_id = $request->selectDistrict;
            $address->postal_code = $request->textPostalCode;
            $address->status = true;
        $address->save();

        return redirect('/editprovider/'.$request->hiddenCodeCustomer)->with('status', true);
    }

    public function updateCustomer(Request $request){

        //update companies
        $company = Company::find($request->hiddenCodeCustomer);
            $company->typedocument_id = $request->selectTypeDocument;
            $company->taxnumber = $request->textNumberDocument;
            //$company->image = null;
            $company->name = $request->textNameCompany;
            $company->brand = $request->textBrandCompany;
            $company->mail = $request->textMailCompany;
            $company->telephone = $request->textPhoneCompany;
            $company->website = $request->textWebsiteCompany;
            //$company->currency_id = null; //It'll be register when assign user
        $company->save();

        //update companies_customers
        DB::table('companies_customers')
            ->where([
                ['provider_id', '=', Auth::user()->company_id],
                ['customer_id', '=', $request->hiddenCodeCustomer]
            ])
            ->update([
                'typecustomer_id' => $request->selectTypeRecord,
                'currency_id' => $request->selectCurrency,
                'daystodue' => $request->textCreditDays,
                'status' => true
            ]);

        //update address
        $address = Address::find($request->hiddenCodeAddress);
            $address->addressname = $request->textFastAddress;
            $address->address = $request->textAddress;
            $address->district_id = $request->selectDistrict;
            $address->postal_code = $request->textPostalCode;
            $address->status = true;
        $address->save();

        return redirect('/editcustomer/'.$request->hiddenCodeCustomer)->with('status', true);
    }

    public function newProvider()
    {
        return view('mycompany.newprovider', [
            'documents' => TypeDocument::ListDocuments(),
            'countries' => Country::ListCountries()
        ]);
    }

    public function providersList()
    {
        return view ('mycompany.providertocustomer', [
            'providers' => Company::ProviderListSimple(Auth::user()->company_id)
        ]);
    }

    public function customersList()
    {
        return view ('mycompany.customertoprovider', [
                'customers' => Company::CustomerListSimple(Auth::user()->company_id)
            ]);
    }

    public function providerLikeCustomer($id)
    {
        //save companies_customers
        $companyCustomer = new CompanyCustomer();
            $companyCustomer->provider_id = Auth::user()->company_id;
            $companyCustomer->customer_id = $id;
            $companyCustomer->currency_id = 'C01'; //USD by default
            $companyCustomer->status = true;
        $companyCustomer->save();

        return redirect('/mycustomers')->with('status', true);
    }

    public  function customerLikeProvider($id)
    {
        $companyProvider = new CompanyProvider();
            $companyProvider->company_id = Auth::user()->company_id;
            $companyProvider->provider_id = $id;
            $companyProvider->status = true;
        $companyProvider->save();

        return redirect('/myproviders')->with('status', true);
    }

    public function saveNewProvider(Request $request)
    {
        //save companies
        $newCodeCompany = Company::NewCode();
            $this->_saveCompany($newCodeCompany, $request->selectTypeDocument, $request->textNumberDocument, $request->textNameCompany, 
                                $request->textBrandCompany, $request->textMailCompany, $request->textPhoneCompany, $request->textWebsiteCompany);

        //save companies_providers
        $companyProvider = new CompanyProvider();
            $companyProvider->company_id = Auth::user()->company_id;
            $companyProvider->provider_id = $newCodeCompany;
            $companyProvider->status = true;
        $companyProvider->save();

        $this->_saveAddress($newCodeCompany, $request->textFastAddress, $request->textAddress, $request->selectDistrict, $request->textPostalCode, 'FCT');

        return redirect('/myproviders')->with('status', true);
    }

    public function newAddress(Request $request)
    {
        $companySelected = $request->code;

        $customers = Company::CustomerListSimple(Auth::user()->company_id);
        $providers = Company::ProviderListSimpleDocs(Auth::user()->company_id);

        $allCompanies = [];
        $company = [];

        foreach ($customers as $customer){

            $company['code'] = $customer->code;
            $company['name'] = $customer->name;
            $company['typedoc'] = $customer->typedocument;
            $company['document'] = $customer->taxnumber;

            array_push($allCompanies, $company);

        }

        $qty = count($allCompanies);

        foreach ($providers as $provider) {

            $exist = false;
            for ($i = 0; $i < $qty; $i++) {

                if ($allCompanies[$i]['code'] == $provider->code) $exist = true;

            }

            if(!$exist){
                $company['code'] = $provider->code;
                $company['name'] = $provider->name;
                $company['typedoc'] = $provider->typedocument;
                $company['document'] = $provider->taxnumber;

                array_push($allCompanies, $company);
            }

        }

        $countries = Country::ListCountries();
        $typeAddress = TypeAddress::GetList();

        return view('mycompany.newaddress', [
            'countries' => $countries,
            'typeAddress' => $typeAddress,
            'allCompanies' => $allCompanies,
            'companySelected' => $companySelected
        ]);

    }

    public function saveNewAddress(Request $request)
    {
        $this->_saveAddress($request->selectCompany, $request->textFastAddress, $request->textAddress, $request->selectDistrict, $request->textPostalCode, $request->selectTypeAddres, $request->textCoordinates);
    }

    public function reviewCompany($id)
    {
        $is_customer = false;
        $getReference = CompanyCustomer::ValidateCompanyIsHere(Auth::user()->company_id, $id);
            if(isset($getReference->customer_id)){
                $is_customer = true;
                $company = Company::GetInfoCustomer($id, Auth::user()->company_id, Auth::user()->default_language);
                if($company->typeId == 'DL'){
                    $excludeDocs = [];
                }else{
                    $excludeDocs = ['A02'];
                }
                $docsImportantsForCustomer = TypeAttachment::GetListImportantForCustomers(Auth::user()->default_language, $excludeDocs);
                $listDocumentsForCustomer = CompanyContract::GetListByCustomer(Auth::user()->company_id, $id, Auth::user()->default_language);
                $contracts = CompanyContract::ListContracts($id, Auth::user()->company_id, Auth::user()->default_language);
            
            }else{
                $company = Company::GetInfoProvider($id, Auth::user()->company_id, Auth::user()->default_language);
                $docsImportantsForCustomer = [];
                $listDocumentsForCustomer = [];
                $contracts = [];
            }

        $listAddress = Address::GetAllAddress($id);

        return view('mycompany.infocompany', [
            'company' => $company,
            'contracts' => $contracts,
            'listAddress' => $listAddress,
            'is_customer' => $is_customer,
            'docsImportantsForCustomer' => $docsImportantsForCustomer,
            'listDocumentsForCustomer' => $listDocumentsForCustomer
        ]);
    }

    public function showEditAddress($id)
    {
        $validate = false;
        $codeCompany = Address::GetCodeCompany($id);

        $isProvider = CompanyProvider::ValidateCompanyIsHere(Auth::user()->company_id, $codeCompany->company_id);
        if(isset($isProvider->company_id)){
            $validate = true;
        }else{
            $isCustomer = CompanyCustomer::ValidateCompanyIsHere(Auth::user()->company_id, $codeCompany->company_id);
            if(isset($isCustomer->customer_id)){
                $validate = true;
            }
        }

        if($validate){
            $addressInfo = Address::GetAddressInformation($id);

            return view('mycompany.editaddress', [
                'addressInfo' => $addressInfo,
                'countries' => Country::ListCountries(),
                'departments' => Department::ListDepartments($addressInfo->country),
                'provinces' => Province::ListProvinces($addressInfo->department),
                'districts' => District::ListDistricts($addressInfo->province),
                'typeAddress' => TypeAddress::GetListComplete()
            ]);

        }else{
            abort(404);        
        }
    }

    public function updateAddressCompany(Request $request)
    {
        //update companies_customers
        DB::table('companies_address')
            ->where([
                ['address_id', '=', $request->hiddenCodeAddress]
            ])
            ->update([
                'typeaddress_id' => $request->selectTypeAddres
            ]);

        //update adrress
        $address = Address::find($request->hiddenCodeAddress);
            $address->addressname = $request->textFastAddress;
            $address->address = $request->textAddress;
            $address->district_id = $request->selectDistrict;
            $address->postal_code = $request->textPostalCode;
            $address->coordinates = $request->textCoordinates;
        $is_saved = $address->save();

        echo $is_saved;
    }

}