<?php

namespace App\Http\Controllers\MyCompany;

use App\User;
use App\CompaniesCurrencies;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class TypeExchangeController extends Controller
{

    public function showTypeExchange()
    {

        $currencies = CompaniesCurrencies::select('currencies.name', 'currencies.iso4217', 'currencies.symbol', 'companies_currencies.exchange', 'countries.name as country')
                ->join('currencies', 'companies_currencies.currency_id', '=', 'currencies.id')
                ->join('countries', 'currencies.country_id', '=', 'countries.id')
                ->where([
                    ['companies_currencies.company_id', '=', Auth::user()->company_id],
                    ['companies_currencies.status', '=', true]
                ])
                ->get();

        return view('mycompany.typeexchange', [
            'currencies'=>$currencies
        ]);
    }

    public function saveCurrency(Request $request)
    {
        $newCode = CompaniesCurrencies::NextCode();

        $newCurrency = new CompaniesCurrencies();
            $newCurrency->id = $newCode;
            $newCurrency->company_id = Auth::user()->company_id;
            $newCurrency->currency_id = $request->codeCurrency;
            $newCurrency->exchange = $request->amountExchange;
            $newCurrency->status = true;
            $newCurrency->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newCurrency->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $is_saved = $newCurrency->save();

        echo $is_saved;

    }

}
