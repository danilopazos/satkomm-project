<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use App\Language;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class MyProfileController extends Controller
{

    public function myProfile()
    {
        $langs = Language::GetList();

        return view('dashboard.myprofile', [
                'user'=>User::InfoUser(Auth::user()->id),
                'langs' => $langs
            ]); 
    }

    public function changePassword()
    {
        return view('dashboard.changepassword');
    }

    public function saveNewPassword(Request $request)
    {

        $hashedPassword = Auth::user()->password;
        $nowPassword = $request->userPassword;

        if (Hash::check($nowPassword, $hashedPassword)) {
            DB::table('users')
                ->where([
                    ['id', '=', Auth::user()->id]
                ])
                ->update([
                    'password' => Hash::make($request->userNewPass)
                ]);
            echo "updated";

        }else{
            echo "notPass";

        }
    }

    public function updateProfile(Request $request)
    {
        $myAccount = User::find(Auth()->user()->id);
            $myAccount->name = $request->userName;
            $myAccount->lastname = $request->userLastname;
            $myAccount->cellphone = $request->userCellphone;
            $myAccount->telephone = $request->userTelephone;
            $myAccount->default_language = $request->selectLanguage;
        $is_saved = $myAccount->save();

        echo $is_saved;
    }

}
