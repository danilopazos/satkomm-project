<?php

namespace App\Http\Controllers\Vsat;

use App\VsatPlan;
use App\OrderPlan;
use App\VsatRecord;
use App\TypePlan;
use App\TechUnit;
use App\TechSpeed;
use App\ContractTime;
use App\ContractConnection;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlansController extends Controller
{
    public function listPlans()
    {
        $plans = VsatPlan::ListPlans(Auth::user()->company_id);
        $arrayPlans = [];

        foreach($plans as $plan){
            $qtyActives = OrderPlan::GetQtyActives($plan->code, Auth::user()->company_id);

            array_push($arrayPlans, array(
                'code' => $plan->code,
                'description' => $plan->description.' - '.$plan->anotations,
                'codePlan' => $plan->codePlan,
                'plan' => $plan->plan,
                'band' => $plan->band,
                'qtyActives' => $qtyActives
            ));
        }

        return view('vsat.listplanvsat', [
            'arrayPlans' => $arrayPlans
        ]);
    }

    public function newPlan()
    {
        return view('vsat.newvsatplan', [
            'vsatRecords' => VsatRecord::ListVsatRecords(Auth::user()->company_id),
            'typePlans' => TypePlan::ListTypePlans(),
            'techUnits' => TechUnit::ListUnits(),
            'techSpeeds' => TechSpeed::ListSpeeds(),
            'conTimes' => ContractTime::ListTimes(),
            'conConnections' => ContractConnection::ListConnections()
        ]);
    }

    public function savePlan(Request $request){

        $nextVsatPlanCode = VsatPlan::NextCode();
        $vsatPlan = new VsatPlan();
            $vsatPlan->id = $nextVsatPlanCode;
            $vsatPlan->vsatrecord_id = $request->selectBand;
            $vsatPlan->description = $request->textPlanName;
            $vsatPlan->anotations = $request->textAnotationPlan;
            $vsatPlan->typeplan_id = $request->selectTypePlan;
            $vsatPlan->download = $request->textDownload;
            $vsatPlan->typedownload = $request->selectDownload;
            $vsatPlan->upload = $request->textUpload;
            $vsatPlan->typeupload = $request->selectUpload;
            $vsatPlan->anytime = $request->textAnytime;
            $vsatPlan->typeanytime = $request->selectAnytime;
            $vsatPlan->earlymorning = $request->textEarly;
            $vsatPlan->typeearlymorning = $request->selectEarly;
            $vsatPlan->total = $request->textTotal;
            $vsatPlan->typetotal = $request->selectTotal;
            $vsatPlan->cir = $request->textCir;
            $vsatPlan->typecir = $request->selectCir;
            $vsatPlan->usdcost = $request->planCost;
            $vsatPlan->commodecost = $request->commodeCost;
            $vsatPlan->status = true;
        $is_saved = $vsatPlan->save();

        echo $is_saved;

    }

    public function editPlan($id)
    {
        return view('vsat.editvsatplan', [
            'infoPlan' => VsatPlan::GetInfoPlan($id, Auth::user()->company_id),
            'vsatRecords' => VsatRecord::ListVsatRecords(Auth::user()->company_id),
            'typePlans' => TypePlan::ListTypePlans(),
            'techUnits' => TechUnit::ListUnits(),
            'techSpeeds' => TechSpeed::ListSpeeds(),
            'conTimes' => ContractTime::ListTimes(),
            'conConnections' => ContractConnection::ListConnections()
        ]);
    }

    public function updatePlan(Request $request)
    {
        $textDownload = null;
        $selectDownload = null;
        $textUpload = null;
        $selectUpload = null;
        $textAnytime = null;
        $selectAnytime = null;
        $textEarly = null;
        $selectEarly = null;
        $textTotal = null;
        $selectTotal = null;
        $textCir = null;
        $selectCir = null;

        if(isset($request->textDownload)) $textDownload = $request->textDownload;
        if(isset($request->selectDownload)) $selectDownload = $request->selectDownload;
        if(isset($request->textUpload)) $textUpload = $request->textUpload;
        if(isset($request->selectUpload)) $selectUpload = $request->selectUpload;
        if(isset($request->textAnytime)) $textAnytime = $request->textAnytime;
        if(isset($request->selectAnytime)) $selectAnytime = $request->selectAnytime;
        if(isset($request->textEarly)) $textEarly = $request->textEarly;
        if(isset($request->selectEarly)) $selectEarly = $request->selectEarly;
        if(isset($request->textTotal)) $textTotal = $request->textTotal;
        if(isset($request->selectTotal)) $selectTotal = $request->selectTotal;
        if(isset($request->textCir)) $textCir = $request->textCir;
        if(isset($request->selectCir)) $selectCir = $request->selectCir;
        
        $vsatPlan = VsatPlan::find($request->codeVsatPlan);
            $vsatPlan->description = $request->textPlanName;
            $vsatPlan->anotations = $request->textAnotationPlan;
            $vsatPlan->download = $textDownload;
            $vsatPlan->typedownload = $selectDownload;
            $vsatPlan->upload = $textUpload;
            $vsatPlan->typeupload = $selectUpload;
            $vsatPlan->anytime = $textAnytime;
            $vsatPlan->typeanytime = $selectAnytime;
            $vsatPlan->earlymorning = $textEarly;
            $vsatPlan->typeearlymorning = $selectEarly;
            $vsatPlan->total = $textTotal;
            $vsatPlan->typetotal = $selectTotal;
            $vsatPlan->cir = $textCir;
            $vsatPlan->typecir = $selectCir;
            $vsatPlan->usdcost = $request->planCost;
            $vsatPlan->commodecost = $request->commodeCost;
            $vsatPlan->status = true;
        $is_saved = $vsatPlan->save();

        echo $is_saved;
    }

    public function showPlan($id)
    {
        $infoPlan = VsatPlan::InfoToShowPlan($id, Auth::user()->company_id);

        if($infoPlan->typeId == 'PS'){
            $ordersAssoc = OrderPlan::GetListActives($id, Auth::user()->company_id);
        }else{
            $ordersAssoc = [];
        }

        return view('vsat.showplan', [
            'planId' => $id,
            'infoPlan' => $infoPlan,
            'ordersAssoc' => $ordersAssoc,
            'listSeppeds' => TechSpeed::ListSpeeds(),
            'listUnits' => TechUnit::ListUnits()
        ]);
    }
}
