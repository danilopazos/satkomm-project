<?php

namespace App\Http\Controllers\Vsat;

use App\HistoryOrder;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HistoryOrderController extends Controller
{
    public function showHistory($orderid)
    {
        return view('vsat.historyorder', [
            'orderId' => $orderid,
            'listHistory' => HistoryOrder::GetList($orderid, Auth::user()->default_language)
        ]);
    }
}
