<?php

namespace App\Http\Controllers\Vsat;

use App\SellCategory;
use App\InventoryGroup;
use App\InventoryItem;
use App\ItemAvailable;
use App\TextRelation;
use App\ModelProduct;
use App\TextDetailInventory;
use App\TypeProduct;
use App\BrandProduct;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class InventoryController extends Controller
{
    public function showList()
    {
        return view('vsat.listinventoryvsat', [
            'listInventories' => InventoryItem::ListInventories(Auth::user()->company_id),
            'listGroups' => InventoryGroup::GetList(Auth::user()->company_id)
        ]);
    }

    public function showFormNew()
    {
        return view('vsat.newinventoryvsat', [
            'sellCategories' => SellCategory::ListSellCategories(),
            'listgroups' => InventoryGroup::GetList(Auth::user()->company_id)
        ]);
    }

    public function showInsertItems(Request $request)
    {
        if ($request->selectEntryGroup == "EGE") {
            //select
            $codeGroup = $request->selectGroupSelection;
            $nameGroup = InventoryGroup::GetName($codeGroup);

        }else if ($request->selectEntryGroup == "ENC") {
            //create
            $codeGroup = InventoryGroup::GetNewCode();
            $nameGroup = $request->textCreateGroup;
            $inventoryGroup = new InventoryGroup();
                $inventoryGroup->id = $codeGroup;
                $inventoryGroup->description = $nameGroup;
                $inventoryGroup->company_id = Auth::user()->company_id;
            $inventoryGroup->save();

        }else{
            //no asign
            $codeGroup = "";
            $nameGroup = "";

        }

        return view('vsat.productsinventoryvsat',[
            'code_group' => $codeGroup,
            'info' => $request,
            'listStatus' => ItemAvailable::ListStatus(),
            'campusTexts' => TextRelation::GetListCampus($request->selectBrandProduct),
            'brand' => ModelProduct::GetBrandModel($request->selectModelProduct),
            'codeBrand' => $request->selectBrandProduct,
            'codeModel' => $request->selectModelProduct,
            'nameGroup' => $nameGroup
        ]);
    }

    public function saveInventoryVsat(Request $request)
    {
        $qtyItems = $request->qtyElements;

        for($i=1; $i<=$qtyItems; $i++){

            $codeInventory = InventoryItem::NextCode();
            $inventoryItem = new InventoryItem();
                $inventoryItem->id = $codeInventory;
                $inventoryItem->group_id = $request->codeGroup;
                $inventoryItem->model_id = $request->codeModel;
                $attachNote = "textNotes_".$i;
                $inventoryItem->anotations = $request->$attachNote;
                $attachSelect = "selectStatus_".$i;
                $inventoryItem->available_id = $request->$attachSelect;
                $inventoryItem->company_id = Auth::user()->company_id;
            $inventoryItem->save();

            $campusTexts = TextRelation::GetListCampus($request->codeBrand);
            foreach ( $campusTexts as $campusText ){

                $attachAditional = "textAditional_".$i."_".$campusText->code;
                $textDetail = new TextDetailInventory();
                    $textDetail->textrelation_id = $campusText->code;
                    $textDetail->inventoryitem_id = $codeInventory;
                    $textDetail->description = $request->$attachAditional;
                    $textDetail->status = true;
                $textDetail->save();
            }
        }

        return redirect('/listinventorysvsat')->with('status', true);
    }

    public function showEdit($id)
    {
        $infoInventory = InventoryItem::InfoInventory($id, Auth::user()->company_id);
        
        return view('vsat.editinventoryvsat', [
            'infoInventory' => $infoInventory,
            'sellCategories' => SellCategory::ListSellCategories(),
            'listTypeProducts' => TypeProduct::ListProducts($infoInventory->category),
            'listBrands' => BrandProduct::ListBrands($infoInventory->type),
            'listModels' => ModelProduct::ListModels($infoInventory->brand),
            'listGroups' => InventoryGroup::GetList(Auth::user()->company_id),
            'listAvailables' => ItemAvailable::ListStatus(),
            'campusList' => TextDetailInventory::InfoRelations($id)
        ]);
    }

    public function updateInventory(Request $request)
    {
        $inventoryItem = InventoryItem::find($request->codeInventory);
            $inventoryItem->group_id = $request->selectGroupSelection;
            $inventoryItem->model_id = $request->selectModelProduct;
            $inventoryItem->anotations = $request->textAnotation;
            $inventoryItem->available_id = $request->selectStatus;
        $inventoryItem->save();

        $campusTexts = TextRelation::GetListCampus($request->selectBrandProduct);
        foreach ( $campusTexts as $campusText ){

            $attachAditional='aditional_'.$campusText->code;

            DB::table('textdetailinventories')
                ->where([
                    ['textrelation_id', '=', $campusText->code],
                    ['inventoryitem_id', '=', $request->codeInventory],
                    ['status', '=', true]
                ])
                ->update([
                    'description' => $request->$attachAditional
                ]);
        }
    }

}
