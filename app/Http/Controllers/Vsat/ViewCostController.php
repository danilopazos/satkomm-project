<?php

namespace App\Http\Controllers\Vsat;

use App\CostDetail;
use App\CompanyProvider;
use App\TypeCost;
use App\TypeInvoice;
use App\CompaniesCurrencies;
use App\CostTypeRecord;
use App\HistoryOrder;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\File;
use Spatie\Dropbox\Client;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class ViewCostController extends Controller
{
    public function showCosts($orderid)
    {
        $costsList = CostDetail::GetListByOrder($orderid, Auth::user()->company_id);

        return view('vsat.viewcosts', [
            'orderid' => $orderid,
            'costsList' => $costsList

        ]);
    }

    public function showAssignCost($orderid)
    {
        $listProvider = CompanyProvider::GetListProvider(Auth::user()->company_id);
        $listTypeCost = TypeCost::GetListForCombo(Auth::user()->company_id);
        $listInvoice = TypeInvoice::ListDocuments();
        $currencies = CompaniesCurrencies::GetMyCurrenciesList(Auth::user()->company_id);
        $listRecord = CostTypeRecord::GetList(Auth::user()->default_language);

        return view('vsat.assigncosts', [
            'orderid' => $orderid,
            'listProvider' => $listProvider,
            'listTypeCost' => $listTypeCost,
            'listInvoice' => $listInvoice,
            'currencies' => $currencies,
            'listRecord' => $listRecord
        ]);
    }

    public function saveAssignCost(Request $request)
    {
        $extensionFile = '.'.$request->file('fileAttach')->getClientOriginalExtension();
        $filename = $request->orderCode.'_'.Carbon::now('America/Lima')->format("YmdHis").$extensionFile;
        $hisNewCode = HistoryOrder::NextCode($request->orderCode);
        
        if($request->selectTypeRecord == 'EN'){
            $companyId = Auth::user()->company_id;
            $newHistory = new HistoryOrder();
                $newHistory->id = $hisNewCode;
                $newHistory->order_id = $request->orderCode;
                $newHistory->historystatus_id = 'REN';
                $newHistory->anotations = '/CostDocuments/'.$filename;
                $newHistory->user_id = Auth::user()->id;
                $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newHistory->save();
            
        }else{
            $companyId = $request->selectProvider;
            $newHistory = new HistoryOrder();
                $newHistory->id = $hisNewCode;
                $newHistory->order_id = $request->orderCode;
                $newHistory->historystatus_id = 'REX';
                $newHistory->anotations = '/CostDocuments/'.$filename;
                $newHistory->user_id = Auth::user()->id;
                $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newHistory->save();
        }

        $newCode = CostDetail::NextCode();
        $newCost = new CostDetail();
            $newCost->id = $newCode;
            $newCost->order_id = $request->orderCode;
            $newCost->typerecord = $request->selectTypeRecord;
            $newCost->provider_id = $companyId;
            $newCost->typecost_id = $request->selectCost;
            $newCost->anotations = $request->textAnotation;
            $newCost->typeinvoice_id = $request->selectDocument;
            $newCost->numberdocument = $request->numberDocument;
            $newCost->currency_id = $request->selectCurrency;
            $newCost->amount = $request->costAmount;
            $newCost->attach = $filename;
            $newCost->createdbyuser = Auth::user()->id;
            $newCost->status = true;
            $newCost->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newCost->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $is_saved = $newCost->save();

        if($is_saved){
            Storage::disk('dropbox')->putFileAs(
                '/CostDocuments', 
                $request->file('fileAttach'), 
                $filename
            );
        }

        return redirect('/vsat/viewcosts/'.$request->orderCode)->with('status', true);
    }

    public function downloadCostDocument($ordercode, $filename)
    {
        $hisNewCode = HistoryOrder::NextCode($ordercode);
        $newHistory = new HistoryOrder();
            $newHistory->id = $hisNewCode;
            $newHistory->order_id = $ordercode;
            $newHistory->historystatus_id = 'DWO';
            $newHistory->anotations = '/CostDocuments/'.$filename;
            $newHistory->user_id = Auth::user()->id;
            $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $newHistory->save();

        return Storage::disk('dropbox')->download('/CostDocuments/'.$filename);
    }
}
