<?php

namespace App\Http\Controllers\Vsat;

use App;
use App\OrderStatus;
use App\HistoryOrder;
use App\OrderAttach;
use App\OrderService;
use App\InventoryItem;
use App\SuspensionControl;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\File;
use Spatie\Dropbox\Client;
use Illuminate\Support\Facades\Storage;

class OrderStatusController extends Controller
{
    public function __construct()
    {
        if($this->middleware('auth')){
            $this->middleware(function ($request, $next){
                //Only Super Administrator
                if(Auth::user()->usertype_id == "ALLS" or Auth::user()->usertype_id == "ADSY") {
                    App::setLocale(Auth::user()->default_language);
                    return $next($request);

                }else{
                    return redirect('/error/nopermission');

                }
            });
        }
    }

    protected function _saveHistoryOrder($orderCode, $status, $anotations, $filename = null)
    {
        $newCode = HistoryOrder::NextCode($orderCode);
        $newHistory = new HistoryOrder();
            $newHistory->id = $newCode;
            $newHistory->order_id = $orderCode;
            $newHistory->historystatus_id = $status;
            $newHistory->anotations = $anotations;
            $newHistory->attach = $filename;
            $newHistory->user_id = Auth::user()->id;
            $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $is_saved = $newHistory->save();
        
        return $is_saved;
    }

    protected function _updateOrderService($orderCode, $dateStart, $dateDue, $orderStatus)
    {
        $orderRecord = OrderService::findOrFail($orderCode);
            if($dateStart != null) $orderRecord->date_start = $dateStart;
            if($dateDue != null) $orderRecord->date_due = $dateDue;
            $orderRecord->ordstatus_id = $orderStatus;
            $orderRecord->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $orderRecord->save();
    }

    protected function _saveSuspension($orderId, $start_suspension, $stop_billing)
    {
        $newCode = SuspensionControl::NextCode($orderId);
        $suspensionRecord = new SuspensionControl();
            $suspensionRecord->id = $newCode;
            $suspensionRecord->order_id = $orderId;
            $suspensionRecord->start_suspension = $start_suspension;
            $suspensionRecord->end_suspension = null;
            $suspensionRecord->stop_billing = $stop_billing;
            $suspensionRecord->status = true;
            $suspensionRecord->user_id = Auth::user()->id;
        $suspensionRecord->save();
    }

    protected function _updateSuspension($orderId, $end_suspension){
        $suspensionId = SuspensionControl::GetId($orderId);
        if(isset($suspensionId->code)){
            $suspensionRecord = SuspensionControl::find($suspensionId->code);
                $suspensionRecord->end_suspension = $end_suspension;
            $suspensionRecord->save();
        }
    }

    public function showForm(OrderService $order)
    {
        if($order->ordstatus_id == 'S1'){ //pendiente
            $excludeList = ['S1', 'S3', 'S4', 'S5'];

        }else if($order->ordstatus_id == 'S4'){ //cancelado
            $excludeList = ['S1', 'S2', 'S3', 'S4', 'S5'];

        }else if($order->ordstatus_id == 'S5'){ //cancelado
            $excludeList = ['S1', 'S3', 'S5'];

        }else{
            $excludeList = ['S1', $order->ordstatus_id];

        }

        return view('vsat.newstatusorder', [
            'user' => Auth::user(),
            'orderCode' => $order->id,
            'listStatus' => OrderStatus::GetList($excludeList),
            'statusOrder' => OrderStatus::GetStatusOrder($order->id)
        ]);
    }

    public function saveStatus(Request $request)
    {
        $dateSelected = $request->dateCreate;

        $is_saved = false;
        $historyStatus = null;
        $date_start = null;
        $date_due = null;

        $attachDocument = false;
        $filename = null;

        if(strlen($request->inputFile) > 0){
            $attachDocument = true;
            $extensionFile = '.'.$request->file('inputFile')->getClientOriginalExtension();
            $filename = $request->textOrdenCode.'_'.Carbon::now('America/Lima')->format("YmdHis").$extensionFile;
    
        }
        
        $ordService = OrderService::findOrFail($request->textOrdenCode);

        if($request->selectStatus == "S2"){ //activar
        
            if($ordService->ordstatus_id == 'S1') //si esta pendiente
            {
                $date_start = $dateSelected . ' ' . Carbon::now('America/Lima')->format("H:i:s");
                $contractMonths = $ordService->contractmonths;
                $date_due = date("Y-m-d", strtotime($date_start."+ $contractMonths month"));
                $historyStatus = 'ACT';

            }else if($ordService->ordstatus_id == 'S3'){ //si esta con corte de servicio
                $this->_updateSuspension($request->textOrdenCode, $dateSelected);
                $historyStatus = 'RAC';

            }else if($ordService->ordstatus_id == 'S5'){ //si la facturacion esta suspendida
                $this->_updateSuspension($request->textOrdenCode, $dateSelected);
                $historyStatus = 'RAF';

            }

        }else if($request->selectStatus == "S3"){ //cortar servicio y seguir facturando
         
            if($ordService->ordstatus_id == 'S2'){ //si esta activa
                $this->_updateSuspension($request->textOrdenCode, $dateSelected);
                $this->_saveSuspension($request->textOrdenCode, $dateSelected, false);
                $historyStatus = 'SSF';

            }

        }else if($request->selectStatus == "S5"){ //suspender facturación

            if($ordService->ordstatus_id == 'S2' or $ordService->ordstatus_id == 'S3'){ //solo si tiene servicio activo
                $this->_updateSuspension($request->textOrdenCode, $dateSelected);
                $this->_saveSuspension($request->textOrdenCode, $dateSelected, true);
                $historyStatus = 'SSB';

            }

        }else if($request->selectStatus == "S4"){ //cancelar

            if($ordService->ordstatus_id != 'S4' and $ordService->ordstatus_id != 'S1'){ //solo si esta activo o suspendido de facturacion

                if($request->problems == 'true'){
                    $historyStatus = 'CNP';
                }else{
                    $historyStatus = 'CAN';
                }

                $codeInventory = OrderAttach::GetCodeAttach($request->textOrdenCode);

                if(isset($codeInventory->code)) {
                    $inventoryRecord = InventoryItem::findOrFail($codeInventory->code);
                        $inventoryRecord->anotations = 'La orden de servicio : ' . $request->textOrdenCode . " fue cancelada.";
                        $inventoryRecord->available_id = 'S5';
                    $inventoryRecord->save();
                }
                $this->_updateSuspension($request->textOrdenCode, $dateSelected);

            }
        }

        if($historyStatus != null){
            $is_saved = $this->_saveHistoryOrder($request->textOrdenCode, $historyStatus, $request->textAnotations, $filename);
        }

        if($is_saved){
            $this->_updateOrderService($request->textOrdenCode, $date_start, $date_due, $request->selectStatus);

            if($attachDocument){
                Storage::disk('dropbox')->putFileAs(
                    '/AttachedStatusOrder', 
                    $request->file('inputFile'), 
                    $filename
                );
            }
            return redirect('/vieworderservicevsat/'.$request->textOrdenCode)->with('statusUpdate', 'updated');

        }else{
            return redirect('/newstatusorderservice/'.$request->textOrdenCode)->with('statusUpdate', 'no');
        }
    }

    public function downloadAttach($orderId, $historyId)
    {
        $infoDownload = HistoryOrder::GetInfoToDownload($orderId, $historyId);
        $filename = $infoDownload->attach;

        $anotations = '/AttachedStatusOrder/'.$filename;
        $this->_saveHistoryOrder($orderId, 'DWL', $anotations);

        return Storage::disk('dropbox')->download('/AttachedStatusOrder/'.$filename);
    }
}
