<?php

namespace App\Http\Controllers\Vsat;

use App;
use App\ConsumptionReceipt;
use App\CompaniesCurrencies;
use App\Company;
use App\ReceiptHistory;
use App\Currency;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class EditReceiptController extends Controller
{

    public function __construct(){
        if($this->middleware('auth')){
            $this->middleware(function ($request, $next){
                //Only Super Administrator and Administrators
                if(Auth::user()->usertype_id == "ALLS" or Auth::user()->usertype_id == "ADSY") {
                    App::setLocale(Auth::user()->default_language);
                    return $next($request);

                }else{
                    return redirect('/error/nopermission');

                }
            });
        }
    }

    public function index(ConsumptionReceipt $receipt)
    {
        return view('vsat.editreceipt', [
            'receipt' => $receipt,
            'currencies' => CompaniesCurrencies::GetMyCurrenciesList(Auth::user()->company_id),
            'companyName' => Company::GetName($receipt->customer_id),
            'historyUpdates' => ReceiptHistory::HistoryUpdates($receipt->id)
        ]);
    }

    public function store(Request $request)
    {
        $textUpdates = '';

        $consumptionRecord = ConsumptionReceipt::findOrFail($request->receiptCode);
            if($consumptionRecord->serialnumber != $request->serialNumber){
                $textUpdates .= 'Serie : '.$consumptionRecord->serialnumber." --> ".$request->serialNumber.'<br />';
                $consumptionRecord->serialnumber = $request->serialNumber;
            }

            if($consumptionRecord->correlative != $request->correlativeNumber){
                $textUpdates .= 'Correlativo : '.$consumptionRecord->correlative." --> ".$request->correlativeNumber.'<br />';
                $consumptionRecord->correlative = $request->correlativeNumber;
            }

            if($consumptionRecord->created_at->format('Y-m-d') != $request->dateCreate){
                $textUpdates .= 'Fecha de emisión : '.$consumptionRecord->created_at->format('Y-m-d')." --> ".$request->dateCreate.'<br />';
                $consumptionRecord->created_at = $request->dateCreate.' '.Carbon::now('America/Lima')->format('H:i:s');
            }

            if($consumptionRecord->date_due != $request->dateDue){
                $textUpdates .= 'Fecha de vencimiento : '.$consumptionRecord->date_due." --> ".$request->dateDue.'<br />';
                $consumptionRecord->date_due = $request->dateDue;
            }

            if($consumptionRecord->currency_id != $request->currency){
                $lastCur = Currency::GetCurrencyName($consumptionRecord->currency_id);
                $newCur = Currency::GetCurrencyName($request->currency);
                $textUpdates .= 'Moneda : '.$lastCur." --> ".$newCur.'<br />';
                $consumptionRecord->currency_id = $request->currency;
            }

            if($consumptionRecord->exchange != $request->exchange){
                $textUpdates .= 'Tipo de cambio : '.number_format($consumptionRecord->exchange, 3)." --> ".number_format($request->exchange, 3).'<br />';
                $consumptionRecord->exchange = $request->exchange;
            }

            $consumptionRecord->anotations = $request->anotations;
        $is_saved = $consumptionRecord->save();

        if($is_saved){
            $historyRecord = new ReceiptHistory();
                $historyRecord->id = ReceiptHistory::NextCode($request->receiptCode);
                $historyRecord->receipt_id = $request->receiptCode;
                $historyRecord->user_id = Auth::user()->id;
                $historyRecord->anotations = $request->anotations;
                $historyRecord->updates = $textUpdates;
                $historyRecord->created_at = Carbon::now('America/Lima')->format('Y-m-d H:i:s');
                $historyRecord->updated_at = Carbon::now('America/Lima')->format('Y-m-d H:i:s');
            $historyRecord->save();
        }

        echo $is_saved;
    }
}
