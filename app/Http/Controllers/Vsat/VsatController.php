<?php

namespace App\Http\Controllers\Vsat;

use App\Address;
use App\Company;
use App\OrderAttach;
use App\OrderPlan;
use App\OrderService;
use App\SellCategory;
use App\TextDetailInventory;
use App\TypePlan;
use App\TypeProduct;
use App\TypeService;
use App\TypesOfBilling;
use App\TypeAttachment;
use App\CompanyContract;
use App\HistoryOrder;
use App\OrderDocumentation;
use App\CompanyConfig;
use App\VsatRecord;
use App\CostDetail;
use App\InventoryItem;
use App\SuspensionControl;
use App\DetailReceipt;
use App\OrderDiscount;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VsatController extends Controller
{
    protected function _saveHistoryOrder($orderCode, $historyStatus, $anotations)
    {
        $newCode = HistoryOrder::NextCode($orderCode);
        $newHistory = new HistoryOrder();
            $newHistory->id = $newCode;
            $newHistory->order_id = $orderCode;
            $newHistory->historystatus_id = $historyStatus;
            $newHistory->anotations = $anotations;
            $newHistory->user_id = Auth()->user()->id;
            $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $newHistory->save();
    }

    protected function _updateOrderStatus($orderId, $statusId, $date_end = null)
    {
        $orderRecord = OrderService::find($orderId);
            if($date_due != null) $orderRecord->date_end = $date_end;
            $orderRecord->ordstatus_id = $statusId;
            $orderRecord->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $statusId->save();
    }

    public function listServicesVsat()
    {
        return view('vsat.listservicesvsat', [
            'listOrders' => OrderService::ListOrderServices(Auth()->user()->company_id),
            'listPlans' => OrderPlan::GetAllPlansOrders(Auth()->user()->company_id)
        ]);
    }

    public function newOrdenServicevsat()
    {
        return view('vsat.newordenservicevsat', [
            'typesBilling' => TypesOfBilling::ListTypesOfBilling(),
            'vsatRecords' => VsatRecord::ListVsatRecords(Auth()->user()->company_id),
            'typePlans' => TypePlan::ListTypePlans(),
            'typeServices' => TypeService::ListTypeServices(),
            'sellCategories' => SellCategory::ListSellCategories()
        ]);
    }

    public function saveNewOrderService(Request $request)
    {
        $lastOrderCode = null;
        $dateCreate = $request->dateCreate." ".Carbon::now('America/Lima')->format("H:i:s");

        if(isset($request->textLastOrder)){

            $lastOrderCode = $request->textLastOrder;
            $dateEnd = date("Y-m-d", strtotime($request->dateActivation."- 1 day"));
            $codeInventory = OrderAttach::GetCodeAttach($lastOrderCode);
            $itemCode = $codeInventory->code;

            $this->_updateOrderStatus($lastOrderCode, 'S4', $dateEnd);
            $this->_saveHistoryOrder($lastOrderCode, 'MGT', null);

        }else{
            $itemCode = $request->hiddenCodeItem;
        }

        //save multiple plan
        $totalAmountOrder = 0;
        $qtyPlan = $request->qtyPlansTable;
        for ($i = 1; $i <= $qtyPlan; $i++) {

            $attachCodePlan = "codePlanTable_".$i;
            if(isset($request->$attachCodePlan)) {
                $nextCode = OrderPlan::NextCode();

                $ordPlan = new OrderPlan();
                    $ordPlan->id = $nextCode;
                    $ordPlan->orderservice_id = $request->textOrdenCode;
                    $ordPlan->vsatplan_id = $request->$attachCodePlan;
                        $attachPricePlan = "codePriceTable_" . $i;
                    $ordPlan->usd_price = $request->$attachPricePlan;
                    $ordPlan->status = true;
                $ordPlan->save();

                $totalAmountOrder = $totalAmountOrder + $request->$attachPricePlan;
            }

        }

        //order service
        $ordService = new OrderService();
            $ordService->id = $request->textOrdenCode;
            $ordService->provider_id = Auth()->user()->company_id;
            $ordService->customer_id = $request->hiddenCodeCustomer;
            $ordService->address_id = $request->hiddenCodeAddress;
            $ordService->typeservice_id = $request->selectTypeActive;
            $ordService->usdprice = $totalAmountOrder; //Total multiple
            $ordService->anotations = $request->textareaComents;
            $ordService->typebilling_id = $request->selectTypeBilling;
            $ordService->sancode = $request->textSanCode;
            $ordService->date_start = null;
            $ordService->date_due = null;
            $ordService->contractmonths = $request->textTimeContract;
            $ordService->date_end = null; //end service
            $ordService->lastorderservice_id = $lastOrderCode;
            $ordService->ordstatus_id = 'S1';
            $ordService->createdbyuser = Auth()->user()->id;
            $ordService->status = true;
            $ordService->created_at = $dateCreate;
        $ordService->save();

        $this->_saveHistoryOrder($request->textOrdenCode, 'CRE', $request->textareaComents);

        //update item status
        if( strlen($itemCode) == 4 ) {

            //order attach items
            $ordAttach = new OrderAttach();
                $ordAttach->ordservice_id = $request->textOrdenCode;
                $ordAttach->inventoryrecord_id = $itemCode;
                $ordAttach->status = true;
            $ordAttach->save();

            $inventoryRecord = InventoryItem::find($itemCode);
                $inventoryRecord->anotations = 'Adjunto a Orden de Servicio : '.$request->textOrdenCode;
                $inventoryRecord->available_id = 'S2';
            $inventoryRecord->save();
        }

    }

    public function showEditOrderService(OrderService $order)
    {
        $companyName = Company::GetName($order->customer_id);
        $companyAddress = Address::GetAddressName($order->address_id);

        $itemAttach = OrderAttach::GetItemAttach($order->id);
        if(isset($itemAttach->code)){
            $itemCode = $itemAttach->code;
            $itemName = $itemAttach->code." - ".  $itemAttach->brand." ".$itemAttach->model;
            $itemTypeCode = $itemAttach->typeproduct;
            $itemSellCode = $itemAttach->sellcategory;
        }else{
            $itemCode = "";
            $itemName = "";
            $itemTypeCode = "";
            $itemSellCode = "";
        }

        $dateStart = "";
        $dateDue = "";
        if(strlen($order->date_start) > 0){
            $dateStart =  $order->date_start->format("Y-m-d");
        }
        if(strlen($order->date_due) > 0){
            $dateDue = $order->date_due->format("Y-m-d");
        }

        $infoOrder = (object) [
            'code' => $order->id,
            'create_date' => $order->created_at->format("Y-m-d"),
            'customer_code' => $order->customer_id,
            'customer_name' => $companyName,
            'address_code' => $order->address_id,
            'address_name' => $companyAddress,
            'typebilling_id' => $order->typebilling_id,
            'lastOrderCode' => $order->lastorderservice_id,
            'typeservice_id' => $order->typeservice_id,
            'price_plan' => $order->usdprice,
            'price_name' => "$ ".$order->usdprice,
            'date_start' => $dateStart,
            'sancode' => $order->sancode,
            'contracttime' => $order->contractmonths,
            'date_due' => $dateDue,
            'sellcategodory_id' => $itemSellCode,
            'typeproduct_id' => $itemTypeCode,
            'product_id' => $itemCode,
            'product_name' => $itemName,
            'notes' => $order->anotations

        ];

        return view('vsat.editordenservicevsat', [
            'infoOrder' => $infoOrder,
            'typesBilling' => TypesOfBilling::ListTypesOfBilling(),
            'vsatRecords' => VsatRecord::ListVsatRecords(Auth()->user()->company_id),
            'typePlans' => TypePlan::ListTypePlans(),
            'typeServices' => TypeService::ListTypeServices(),
            'sellCategories' => SellCategory::ListSellCategories(),
            'typeProducts' => TypeProduct::ListProducts($itemSellCode),
            'listPlans' => $listPlans = OrderPlan::GetPlanForOrder($order->id)
        ]);

    }

    public function UpdateOrderService(Request $request)
    {
        //delete relation
        $lasRelation = OrderPlan::where([
            ['orderservice_id', $request->textOrdenCode]
        ]);
        $lasRelation->delete();

        //update plans
        $qty = $request->qtyPlansTable;
        $total = 0;
        for($i = 1; $i <= $qty; $i++){

            $attachCodePlan = "codePlanTable_".$i;

            if(isset($request->$attachCodePlan)) {
                $nextCode = OrderPlan::NextCode();

                $ordPlan = new OrderPlan();
                    $ordPlan->id = $nextCode;
                    $ordPlan->orderservice_id = $request->textOrdenCode;
                    $ordPlan->vsatplan_id = $request->$attachCodePlan;
                    $attachPricePlan = "codePriceTable_" . $i;
                    $ordPlan->usd_price = $request->$attachPricePlan;
                    $ordPlan->status = true;
                $ordPlan->save();

                $attachPrice = "codePriceTable_" . $i;
                $total = $total + $request->$attachPrice;
            }
        }

        //order service
        $ordService = OrderService::find($request->textOrdenCode);
            $ordService->customer_id = $request->hiddenCodeCustomer;
            $ordService->address_id = $request->hiddenCodeAddress;
            $ordService->usdprice = $total;
            $ordService->anotations = $request->textareaComents;
            $ordService->typebilling_id = $request->selectTypeBilling;
            $ordService->sancode = $request->textSanCode;
            $ordService->date_start = $request->dateActivation;
            $ordService->date_due = $request->dateDue;
            $ordService->contractmonths = $request->textTimeContract;
            $ordService->created_at = $request->dateCreate;
        $ordService->save();

        $this->_saveHistoryOrder($request->textOrdenCode, 'UPT', null);

        //update las item
        if ( $request->hiddenLastCodeItem != $request->hiddenCodeItem ){
            if( strlen($request->hiddenLastCodeItem) == 4 ) {
                //update inventory
                $inventoryRecord = InventoryItem::find($itemCode);
                    $inventoryRecord->anotations = '';
                    $inventoryRecord->available_id = 'S1';
                $inventoryRecord->save();

                //delete relation
                $lasRelation = OrderAttach::where([
                    ['ordservice_id', $request->textOrdenCode],
                    ['inventoryrecord_id', $request->hiddenLastCodeItem]
                ]);
                $lasRelation->delete();

            }

            //order attach items
            $ordAttach = new OrderAttach();
                $ordAttach->ordservice_id = $request->textOrdenCode;
                $ordAttach->inventoryrecord_id = $request->hiddenCodeItem;
                $ordAttach->status = true;
            $ordAttach->save();

            //update item status
            if( strlen($request->hiddenCodeItem) == 4 ) {
                $inventoryRecord = InventoryItem::find($itemCode);
                    $inventoryRecord->anotations = 'Adjunto a Orden de Servicio : '.$request->textOrdenCode;
                    $inventoryRecord->available_id = 'S2';
                $inventoryRecord->save();
            }

        }

    }

    public function ViewOrderService($id)
    {
        $getOrder = OrderService::GetInfoOrder($id);
        $itemAttach = OrderAttach::GetInfoItem($id);
        $listPlans = OrderPlan::GetPlanForOrder($id);
        $operatingCosts = CostDetail::GetListTotals($id);
        $invoiceAmounts = DetailReceipt::GetEntryAndExits($id);

        if($getOrder->typecustomer_id == 'DL'){
            $excludeDocs = [];
        }else{
            $excludeDocs = ['A02'];
        }

        $entryAmount = 0;
        $exitAmount = 0;
        foreach($operatingCosts as $operating){
            if($operating->typerecord == 'EN'){
                if($operating->iso4217 == 'USD'){
                    $entryAmount += $operating->amount;
                }else{
                    $entryAmount += $operating->amount / $operating->exchange;
                }
            }else{
                if($operating->iso4217 == 'USD'){
                    $exitAmount += $operating->amount;
                }else{
                    $exitAmount += $operating->amount / $operating->exchange;
                }
            }
        }

        $itemCode = "";
        $itemStatusId = "";
        $itemStatus = "";
        $itemDescription = "";
        $itemAnotations = "";
        $itemStyleAlert = "";
        $itemIconAlert = "";

        if(isset($itemAttach->code)){
            $itemCode = $itemAttach->code;
            $itemStatusId = $itemAttach->statusid;
            $itemStatus = $itemAttach->status;
            $itemStyleAlert = $itemAttach->stylealert;
            $itemIconAlert =$itemAttach->iconalert;
            $itemDescription = $itemAttach->typeproduct." - ".$itemAttach->brand." - ".$itemAttach->model;
            $itemAnotations = $itemAttach->notes;
        }

        $textDetail = TextDetailInventory::InfoRelations($itemCode);

        $infoOrder = (object)[
            'code' => $getOrder->code,
            'create' => $getOrder->created_at,
            'attach' => $getOrder->attach,
            'companyDoc' => $getOrder->typedocument." - ".$getOrder->companyid,
            'companyCode' => $getOrder->companycode,
            'companyName' => $getOrder->companyname,
            'addressInfo' => "(".$getOrder->addressname.") ".$getOrder->address." - ".$getOrder->district.", ".$getOrder->province.", ".$getOrder->department.", ".$getOrder->country,
            'coordinates' => $getOrder->coordinates,
            'credit' => $getOrder->creditdays,
            'typeOfBilling' => $getOrder->typeofbilling,
            'orderAnotations' => $getOrder->ordernotes,
            'lastOrderCorde' => $getOrder->lastorderservice_id,
            'typeService' => $getOrder->typeservice,
            'adminValidation' => $getOrder->admin_validation,

            'pricePlan' => $getOrder->usdprice,
            'dateActive' => $getOrder->date_start,
            'dateDue' => $getOrder->date_due,
            'dateEnd' => $getOrder->date_end,
            'contractMonths' => $getOrder->contractmonths,
            'sanCode' => $getOrder->sancode,
            'itemCode' => $itemCode,
            'itemStatusId' => $itemStatusId,
            'itemStatus' => $itemStatus,
            'itemStyleAlert' => $itemStyleAlert,
            'itemIconAlert' => $itemIconAlert,
            'itemDescription' => $itemDescription,
            'itemAnotations' => $itemAnotations,
            'itemTextDetail' => $textDetail,

            'statusText' => $getOrder->statusText,
            'statusStyle' => $getOrder->statusStyle,
            'statusIcon' => $getOrder->statusIcon
        ];

        $invoiceEntry = 0;
        $invoiceExit = 0;
        
        foreach($invoiceAmounts as $invoice){
            if($invoice->typeplan == 'PS'){
                $periodStart = $invoice->period.'-01';
                $tmpEnd = date("Y-m-d", strtotime($periodStart."+ 1 month"));
                $maxDay = date("d", strtotime($tmpEnd."- 1 day"));

                $invoiceEntry += round(($invoice->usd_price / $maxDay) * $invoice->daystopay, 2);
                $invoiceExit += round(($invoice->usd_cost / $maxDay) * $invoice->daystopay, 2);

            }else{
                $invoiceEntry += $invoice->usd_price;
                $invoiceExit += $invoice->usd_cost;

            }

            if($invoice->commode_cost != null){
                $invoiceExit += $invoice->commode_cost;
            }
        }

        return view('vsat.vieworderservice', [
            'infoOrder' => $infoOrder,
            'listPlans' => $listPlans,
            'listDocumentsImportants' => TypeAttachment::GetListImportantForCustomers(Auth()->user()->default_language, $excludeDocs),
            'listDocumentsAttached' => CompanyContract::GetListByCustomer(Auth()->user()->company_id, $getOrder->companycode, Auth()->user()->default_language),
            'docsImportantsForOrder' => TypeAttachment::GetListImportantForOrder(Auth()->user()->default_language),
            'listDocumentsForOrder' => OrderDocumentation::GetLisByOrder($id, Auth()->user()->default_language),
            'timeToDue' => CompanyConfig::GetInfoDocumentsDue(Auth()->user()->company_id),
            'entryAmount' => $entryAmount,
            'exitAmount' => $exitAmount,
            'listSuspension' => SuspensionControl::GetListByOrderId($id),
            'invoiceEntry' => $invoiceEntry,
            'invoiceExit' => $invoiceExit,
            'listDiscounts' => OrderDiscount::GetLisPerOrder($id)
        ]);
    }

    public function makeUpgrade($id)
    {
        return view('vsat.newordenservicevsat', [
            'typesBilling' => TypesOfBilling::ListTypesOfBilling(),
            'vsatRecords' => VsatRecord::ListVsatRecords(Auth()->user()->company_id),
            'typePlans' => TypePlan::ListTypePlans(),
            'typeServices' => TypeService::ListTypeServices(),
            'sellCategories' => SellCategory::ListSellCategories(),
            'lastOrder' => $id
        ]);
    }

}
