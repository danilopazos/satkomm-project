<?php

namespace App\Http\Controllers\Vsat;

use App\ConsumptionReceipt;
use App\DetailReceipt;
use App\OrderService;
use App\OrderPlan;
use App\SuspensionControl;
use App\CompanyCustomer;
use App\OrderDiscount;
use App\DiscountReceipt;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    static function _saveDetailReceipt($receipt_id, $order_id, $sancode, $date_start, $date_end, $typeplan, $description, $usd_price, $usd_cost)
    {
        $newCode = DetailReceipt::NextCode($receipt_id);
        $newDetail = new DetailReceipt();
            $newDetail->id = $newCode;
            $newDetail->receipt_id = $receipt_id;
            $newDetail->order_id = $order_id;
            $newDetail->sancode = $sancode;
            $newDetail->date_start = $date_start;
            $newDetail->date_end = $date_end;
            $newDetail->typeplan = $typeplan;
            $newDetail->description = $description;
            $newDetail->usd_price = $usd_price;
            $newDetail->usd_cost = $usd_cost;
            $newDetail->status = true;
            $newDetail->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newDetail->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $newDetail->save();
    }

    protected function _getOrdersConsumptionPerPeriod($periodStart)
    {
        $tmpDate = date("Y-m-d", strtotime($periodStart."+ 1 month"));
        $periodEnd = date("Y-m-d", strtotime($tmpDate."- 1 day"));
        $maxDay = date("d", strtotime($periodEnd));

        $excludeOrdersId = ConsumptionReceipt::GetListCompaniesNoReceipts(Auth::user()->company_id, substr($periodStart, 0, 7));

        //Postpago y Adicionales
        $activeOrders_PsAd = OrderService::GetPstAdi_ActivePerPeriod(Auth::user()->company_id, $periodEnd, $excludeOrdersId)->toArray();
        $activeOrders_Tk = OrderService::GetTkn_PerPeriod(Auth::user()->company_id, $periodStart, $periodEnd, $excludeOrdersId)->toArray();
        $cancelOrders_PsAd = OrderService::GetPstAdi_CancelPerPeriod(Auth::user()->company_id, $periodStart, $periodEnd, $excludeOrdersId)->toArray();
        
        $ordersToBill = array_merge($activeOrders_PsAd, $activeOrders_Tk, $cancelOrders_PsAd);
        
        $onlyOrdersCodes = [];
        foreach($ordersToBill as $eachOrder){
            array_push($onlyOrdersCodes, $eachOrder['ordercode']);
        }

        $allInfoPlans = OrderPlan::GetPlanForInvoice($onlyOrdersCodes);
        $allOrderInfo = OrderService::GetInfoOrderToBill($onlyOrdersCodes, Auth::user()->company_id);

        $customerCodes = [];
        foreach($ordersToBill as $order){
            $found = false;
            foreach($customerCodes as $customer){
                if($customer == $order['customer_id']){
                    $found = true;
                }
            }

            if(!$found){
                array_push($customerCodes, $order['customer_id']);
            }
        }

        $customersInfo = CompanyCustomer::GetCustomerForReceipt($customerCodes, Auth::user()->company_id);

        //Validando consumos
        $consumptionList = [];

        foreach($customersInfo as $customer){
            
            $ordersList = [];

            foreach($ordersToBill as $order){
                if($customer->customerId == $order['customer_id']){

                    $listPlans = [];

                    foreach($allInfoPlans as $plan){
                        if($plan->ordercode == $order['ordercode']){
                            array_push($listPlans, array(
                                'planCode' => $plan->codePlan,
                                'typePlan' => $plan->typeplan_id,
                                'descriptionTypePlan' => $plan->typePlan,
                                'namePlan' => $plan->namePlan,
                                'notePlan' => $plan->notePlan,
                                'pricePlan' => $plan->pricePlan,
                                'usdCost' => $plan->usdcost,
                                'commodeCost' => $plan->commodecost
                            ));
                        }
                    }

                    foreach($allOrderInfo as $eachOrderInfo){
                        if($eachOrderInfo->ordercode == $order['ordercode']){
                            $orderInfo = $eachOrderInfo;
                        }
                    }
                    if($orderInfo->date_start < $periodStart){
                        $orderTmpStart = $periodStart;
                    }else{
                        $orderTmpStart = $orderInfo->date_start->format('Y-m-d');
                    }

                    if($orderInfo->date_end == null or $orderInfo->date_end > $periodEnd){
                        $orderTmpEnd = $periodEnd;
                    }else{
                        $orderTmpEnd = $orderInfo->date_end->format('Y-m-d');
                    }

                    //calcular prorrateo
                    $daysToPay = (date("d", strtotime($orderTmpEnd)) - date("d", strtotime($orderTmpStart)) + 1);

                    //facturacion suspendida
                    $daysSuspended = 0;
                    $suspensionInfo = SuspensionControl::GetInformation($order['ordercode'], $periodStart, $periodEnd);
                    
                    foreach($suspensionInfo as $info){
                        if($info->start_suspension<$periodEnd and $info->end_suspension == null){
                            
                            if($info->start_suspension >= $periodStart){
                                $suspDayStart = date("d", strtotime($info->start_suspension));
                                $daysSuspended += $maxDay - $suspDayStart;

                            }else{
                                $daysSuspended += $maxDay - 1;
                            }
                        }else if($info->start_suspension < $periodStart){
                            
                            if($info->end_suspension >= $periodStart and $info->end_suspension <= $periodEnd){
                                $suspDayEnd = date("d", strtotime($info->end_suspension));
                                $daysSuspended += $suspDayEnd - 1;

                            }else if($info->end_suspension > $periodEnd){
                                $daysSuspended += $maxDay;
                            }
                        }else if($info->start_suspension >= $periodStart){
                            
                            if($info->end_suspension <= $periodEnd){
                                $suspDayStart = date("d", strtotime($info->start_suspension));
                                $suspDayEnd = date("d", strtotime($info->end_suspension));
                                $daysSuspended += $suspDayEnd - $suspDayStart;
                                
                            }else if($info->end_suspension > $periodEnd){
                                $suspDayStart = date("d", strtotime($info->start_suspension));
                                $daysSuspended += $maxDay - $suspDayStart;
                            }
                        }
                    }

                    $daysToPay = $daysToPay - $daysSuspended;

                    $orderDateEnd = null;
                    if($orderInfo->date_end != null){
                        $orderDateEnd = $orderInfo->date_end->format('Y-m-d');
                    }

                    $totalDiscount = OrderDiscount::GetTotalDiscuount($orderInfo->ordercode, $periodStart, $periodEnd);

                    array_push($ordersList, array(
                        'orderCode' => $orderInfo->ordercode,
                        'companyCode' => $orderInfo->companyCode,
                        'sanCode' => $orderInfo->sancode,
                        'addressId' => $orderInfo->addressid,
                        'addressName' => $orderInfo->addressname,
                        'addressDescription' =>$orderInfo->district.", ".$orderInfo->province.", ".$orderInfo->department,
                        'status' => $orderInfo->ordstatus_id,
                        'statusName' => $orderInfo->statusname,
                        'statusStyle' => $orderInfo->stylealert,
                        'statusIcon' => $orderInfo->iconalert,
                        'dateActive' => $orderInfo->date_start->format('Y-m-d'),
                        'dateEnd' => $orderDateEnd,
                        'daysToPay' => $daysToPay,
                        'maxDay' => $maxDay,
                        'listPlans' => $listPlans,
                        'totalDiscount' => $totalDiscount
                    )); 
                }
            }

            array_push($consumptionList, array(
                'companyCode' => $customer->customerId, 
                'companyName' => $customer->name,
                'currencyId' =>$customer->curId,
                'currencySymbol' =>$customer->cursymbol,
                'currencyIso' =>$customer->curiso,
                'currencyExchange' =>$customer->exchange,
                'ordersList' => $ordersList,
                'daysToDue' => $customer->daystodue
            ));
        }

        return $consumptionList;
    }

    public function showList()
    {
        $arrayReceipts = [];

        $listReceipts = ConsumptionReceipt::GetListReceipts(Auth::user()->company_id);
        
        foreach ($listReceipts as $receipt){
            $listSan = [];
            $arrayDetails = [];

            $listDetails = DetailReceipt::GetListDetail($receipt->code);

            $dateStart = $receipt->period."-01";
            $tmpDate = date("Y-m-d", strtotime($dateStart."+ 1 month"));
            $dateEnd = date("Y-m-d", strtotime($tmpDate."- 1 day"));

            foreach ($listDetails as $detail){

                $maxDay = substr($dateEnd, 8, 2);
                
                if($detail->typeplan == 'PS'){
                    $payDays = $detail->daystopay;
                    $proration = ($detail->usd_price * $receipt->exchange / $maxDay) * $payDays;
                    $textProration = $receipt->symbol." ".number_format($proration, 2)." ".$receipt->iso4217;
                    
                }else{
                    $payDays = $maxDay;
                    $textProration = '';
                }

                array_push($arrayDetails, array(
                    'orderId' => $detail->order_id,
                    'sanCode' => $detail->sancode,
                    'dateStart' => $detail->date_start,
                    'dateEnd' => $detail->date_end,
                    'typePlan' => $detail->typeplan,
                    'planName' => $detail->description,
                    'totalAmount' => $receipt->symbol." ".number_format($detail->usd_price * $receipt->exchange, 2)." ".$receipt->iso4217,
                    'payDays' => $payDays,
                    'proration' => $textProration
                ));
                
                $qtySan = count($listSan);
                if($qtySan == 0){
                    array_push($listSan, array(
                        'sanCode' => $detail->sancode,
                        'addressName' => $detail->addressname,
                        'location' => $detail->district.', '.$detail->province.', '.$detail->department
                    ));

                }else{
                    $validate = true;
                    foreach ($listSan as $san){
                        if($san['sanCode'] == $detail->sancode){
                            $validate = false;
                        }
                    }
                    if($validate){
                        array_push($listSan, array(
                            'sanCode' => $detail->sancode,
                            'addressName' => $detail->addressname,
                            'location' => $detail->district.', '.$detail->province.', '.$detail->department
                        ));
                    }
                }

            }
            
            array_push($arrayReceipts, array( 'receiptCode' => $receipt->code,
                                                'receiptNumber' => $receipt->serialnumber."-".$receipt->correlative.substr($receipt->created_at, 5, 2).substr($receipt->created_at, 2, 2),
                                                'customerName' => $receipt->companyname,
                                                'listSan' => $listSan,
                                                'ordersAttach' => $arrayDetails,
                                                'month' => substr($receipt->period, 5, 2),
                                                'year' => substr($receipt->period, 0, 4),
                                                'total' => $receipt->symbol." ".number_format($receipt->usdtotal * $receipt->exchange, 2)." ".$receipt->iso4217
                                             ));
        }

        return view('vsat.listinvoices', [
            'arrayReceipts' => $arrayReceipts,
        ]);

    }

    public function showFormSearch()
    {
        $infoDate = OrderService::GetMinPeriod(Auth::user()->company_id);

        if(isset($infoDate->date_start)){
            $minDate = $infoDate->date_start;
        }else{
            $minDate = date("Y-m-d");
        }

        $minYear = date("Y", strtotime($minDate));
        $minMonth = date("m", strtotime($minDate));

        $nowYear = date("Y");
        $nowMonth = date("m");

        $arrayPeriods = [];

        for($year = $minYear; $year <= $nowYear; $year++){
            if($year == $minYear){
                $maxMonth = 12;
                if($year == $nowYear){
                    $maxMonth = $nowMonth - 1;
                }
                for($mes = $minMonth; $mes <= $maxMonth; $mes++){
                    if(strlen($mes)==1){
                        $mes="0".$mes;
                    }
                    $date = $year."-".$mes."-01";
                    array_push($arrayPeriods, $date);
                }
            }
            elseif($year < $nowYear){
                for($mes = 1; $mes <= 12; $mes++){
                    if(strlen($mes)==1){
                        $mes="0".$mes;
                    }
                    $date = $year."-".$mes."-01";
                    array_push($arrayPeriods, $date);
                }
            }else{
                for($mes = 1; $mes < $nowMonth; $mes++){
                    if(strlen($mes)==1){
                        $mes="0".$mes;
                    }
                    $date = $year."-".$mes."-01";
                    array_push($arrayPeriods, $date);
                }
            }
        }

        return view("vsat.viewordersconsumptions", [
            'arrayPeriods' => array_reverse($arrayPeriods)
        ]);

    }

    public function showResultSearch(Request $request)
    {
        return view('vsat.viewordersconsumptions', [
            'consumptionList' => $this->_getOrdersConsumptionPerPeriod($request->selectPeriod),
            'period' => $request->selectPeriod
        ]);
    }

    public function createReceipts(Request $request)
    {   
        $dateStart = $request->hiddenPeriod;
        $_tmpDate = date("Y-m-d", strtotime($dateStart."+ 1 month"));
        $dateEnd = date("Y-m-d", strtotime($_tmpDate."- 1 day"));

        $consumptionList = $this->_getOrdersConsumptionPerPeriod($dateStart);

        $incoiceNumber = ConsumptionReceipt::GetNumberSerial(Auth::user()->company_id);
        if(isset($incoiceNumber->serialnumber)){
            $serial_number = $incoiceNumber->serialnumber;
            $correlative_number = $incoiceNumber->correlative + 1;
        
        }else{
            $serial_number = 'R001';
            $correlative_number = 10301;
        }

        $receiptCode = ConsumptionReceipt::NextCode();
        $consumptionRecord = [];
        $detailRecord = [];
        $discountRecord = [];

        foreach($consumptionList as $consumption){
            $totalPrice = 0;
            $totalCost = 0;
            $totalCommode = 0;
            $detailCode = 1;

            foreach($consumption['ordersList'] as $order){
                foreach($order['listPlans'] as $plan){

                    if($plan['typePlan'] == 'PS'){ //solo postpago prorratea
                        $daysToPay = $order['daysToPay'];
                        $totalPrice += round(($plan['pricePlan'] / $order['maxDay']) * $daysToPay, 2);
                        $totalCost += round(($plan['usdCost'] / $order['maxDay']) * $daysToPay, 2);
                        
                    }else{
                        $daysToPay = $order['maxDay'];
                        $totalPrice += $plan['pricePlan'];
                        $totalCost += $plan['usdCost'];
    
                    }

                    $totalCommode += $plan['commodeCost'];

                    array_push($detailRecord, [
                        'id' => $detailCode,
                        'receipt_id' => $receiptCode,
                        'order_id' => $order['orderCode'],
                        'sancode' => $order['sanCode'],
                        'date_start' => $dateStart,
                        'date_end' => $dateEnd,
                        'typeplan' => $plan['typePlan'],
                        'description' => $plan['namePlan'] .' '. $plan['notePlan'],
                        'daystopay' => $daysToPay,
                        'usd_price' => $plan['pricePlan'],
                        'usd_cost' => $plan['usdCost'],
                        'commode_cost' => $plan['commodeCost'],
                        'status' => true,
                        'created_at' => Carbon::now('America/Lima')->format("Y-m-d H:i:s"),
                        'updated_at' => Carbon::now('America/Lima')->format("Y-m-d H:i:s")
                    ]);
                    $detailCode ++;
                }

                if($order['totalDiscount'] > 0){
                    $totalPrice -= $order['totalDiscount'];

                    array_push($discountRecord, [
                        'id' => 1,
                        'receipt_id' => $receiptCode,
                        'order_id' => $order['orderCode'],
                        'usd_discount' => $order['totalDiscount'],
                        'status' => true
                    ]);
                }

            }

            $daysToDue = $consumption['daysToDue'];
            $invoiceDateDue = date("Y-m-d", strtotime($_tmpDate."+ $daysToDue days"));;

            array_push($consumptionRecord, [
                'id' => $receiptCode,
                'serialnumber' => $serial_number,
                'correlative' => $correlative_number,
                'provider_id' => Auth::user()->company_id,
                'customer_id' => $consumption['companyCode'],
                'period' => substr($request->hiddenPeriod, 0, 7),
                'date_due' => $invoiceDateDue,
                'currency_id' => $consumption['currencyId'],
                'exchange' => $consumption['currencyExchange'],
                'usdtotal' => $totalPrice,
                'usdcost' => $totalCost + $totalCommode,
                'anotations' => null,
                'status' => true,
                'created_at' => $_tmpDate,
                'updated_at' => Carbon::now('America/Lima')->format("Y-m-d H:i:s")
            ]);

            $correlative_number ++;
            $receiptCode ++;
        }

        DB::table('consumptionreceipts')->insert($consumptionRecord);
        DB::table('detailreceipts')->insert($detailRecord);
        DB::table('discountsreceipts')->insert($discountRecord);

        echo "ok";

    }

    /*
    use App\InvoicePeriod; -> NOT IMPORTED - insert route on WEB.PHP

    public function newInvoice(Request $request){

        $code = InvoicePeriod::GetNextCode();

        $invoice = new InvoicePeriod();
            $invoice->id = $code;
            $invoice->company_id = Auth::user()->company_id;
            $invoice->sancode = $request->sanCode;
            $invoice->principalorder_id = $request->orderid;
            $invoice->period = $request->period;
            $invoice->usdamount = $request->totalToPay;
            $invoice->typeinvoice_id = $request->typeDoc;
            $invoice->numberdocument = $request->numberDoc;
            $invoice->doc_dateissue = $request->dateDoc;
            $invoice->anotations = $request->notes;
            $invoice->status = true;
            $invoice->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $invoice->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $is_saved = $invoice->save();

        echo $is_saved;

    }
    */

}
