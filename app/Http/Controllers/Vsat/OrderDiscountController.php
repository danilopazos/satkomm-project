<?php

namespace App\Http\Controllers\Vsat;

use App\OrderService;
use App\OrderDiscount;
use App\HistoryOrder;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\File;
use Spatie\Dropbox\Client;
use Illuminate\Support\Facades\Storage;

class OrderDiscountController extends Controller
{
    public function index(OrderService $order)
    {
        return view('vsat.showdiscount', [
            'order' => $order
        ]);
    }

    public function store(Request $request)
    {
        $orderCode = $request->textOrderCode;
        $extensionFile = '.'.$request->file('fileAttach')->getClientOriginalExtension();
        $filename = $orderCode.'_'.Carbon::now('America/Lima')->format("YmdHis").$extensionFile;

        $newCode = OrderDiscount::NextCode($orderCode);
        $newOrder = new OrderDiscount();
            $newOrder->id = $newCode;
            $newOrder->order_id = $orderCode;
            $newOrder->description = $request->textAnotations;
            $newOrder->discount_amount = $request->textDiscount;
            $newOrder->attach = $filename;
            $newOrder->date_start = $request->dateStart;
            $newOrder->date_end = $request->dateEnd;
            $newOrder->user_id = Auth::user()->id;
            $newOrder->status = true;
            $newOrder->created_at = Carbon::now('America/Lima')->format('Y-m-d H:i:s');
            $newOrder->updated_at = Carbon::now('America/Lima')->format('Y-m-d H:i:s');
        $is_saved = $newOrder->save();

        if($is_saved){
            $hisNewCode = HistoryOrder::NextCode($orderCode);
            $newHistory = new HistoryOrder();
                $newHistory->id = $hisNewCode;
                $newHistory->order_id = $orderCode;
                $newHistory->historystatus_id = 'UPD';
                $newHistory->anotations = '/OrderServiceDiscounts/'.$filename;
                $newHistory->user_id = Auth::user()->id;
                $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newHistory->save();

            Storage::disk('dropbox')->putFileAs(
                '/OrderServiceDiscounts', 
                $request->file('fileAttach'), 
                $filename
            );
        }

        return redirect()->route('vsat.order.ViewOrderService', [$orderCode])->with('status', true);
    }

    public function download($orderId, $discountId)
    {
        $record = OrderDiscount::GetDocument($orderId, $discountId);

        $hisNewCode = HistoryOrder::NextCode($orderId);
        $newHistory = new HistoryOrder();
            $newHistory->id = $hisNewCode;
            $newHistory->order_id = $orderId;
            $newHistory->historystatus_id = 'DWD';
            $newHistory->anotations = '/OrderServiceDiscounts/'.$record->attach;
            $newHistory->user_id = Auth::user()->id;
            $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $newHistory->save();

        return Storage::disk('dropbox')->download('/OrderServiceDiscounts/'.$record->attach);
    }
}
