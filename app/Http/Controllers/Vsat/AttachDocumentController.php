<?php

namespace App\Http\Controllers\Vsat;

use App\TypeAttachment;
use App\OrderDocumentation;
use App\HistoryOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use App\File;
use Spatie\Dropbox\Client;
use Illuminate\Support\Facades\Storage;

class AttachDocumentController extends Controller
{

    public function showAttach($orderId)
    {
        return view('vsat.showattach', [
            'listType' => TypeAttachment::GetListForOrders(Auth::user()->default_language),
            'oderCode' => $orderId
        ]);
    }

    public function uploadDocument(Request $request)
    {
        $extensionFile = '.'.$request->file('fileAttach')->getClientOriginalExtension();
        $filename = $request->orderCode.'_'.Carbon::now('America/Lima')->format("YmdHis").$extensionFile;

        $newCode = OrderDocumentation::NextCode($request->orderCode);
        $newDocument = new OrderDocumentation();
            $newDocument->id = $newCode;
            $newDocument->order_id = $request->orderCode;
            $newDocument->typeattach_id = $request->typeAttach;
            $newDocument->attach = $filename;
            $newDocument->anotations = $request->textAnotations;
            $newDocument->status = true;
            $newDocument->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newDocument->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $is_updated = $newDocument->save();

        if($is_updated){
            $hisNewCode = HistoryOrder::NextCode($request->orderCode);
            $newHistory = new HistoryOrder();
                $newHistory->id = $hisNewCode;
                $newHistory->order_id = $request->orderCode;
                $newHistory->historystatus_id = 'UPL';
                $newHistory->anotations = 'AttachedToServiceOrder/'.$filename;
                $newHistory->user_id = Auth::user()->id;
                $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
                $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newHistory->save();
            
            Storage::disk('dropbox')->putFileAs(
                '/AttachedToServiceOrder', 
                $request->file('fileAttach'), 
                $filename
            );
        }

        /*
        delete
        Storage::disk('dropbox')->getDriver()->getAdapter()->getClient()->delete($file->name);
        */

        return back()->with('status', true);
    }

    public function downloadDocument($orderCode, $filename)
    {
        $hisNewCode = HistoryOrder::NextCode($orderCode);
        $newHistory = new HistoryOrder();
            $newHistory->id = $hisNewCode;
            $newHistory->order_id = $orderCode;
            $newHistory->historystatus_id = 'DWL';
            $newHistory->anotations = '/AttachedToServiceOrder/'.$filename;
            $newHistory->user_id = Auth::user()->id;
            $newHistory->created_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
            $newHistory->updated_at = Carbon::now('America/Lima')->format("Y-m-d H:i:s");
        $newHistory->save();

        return Storage::disk('dropbox')->download('/AttachedToServiceOrder/'.$filename);
    }
}
