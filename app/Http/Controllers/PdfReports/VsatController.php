<?php

namespace App\Http\Controllers\PdfReports;

use App\User;
use App\CompanyCustomer;
use App\Company;
use App\ConsumptionReceipt;
use App\DetailReceipt;
use App\BankAccount;
use App\DiscountReceipt;

use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VsatController extends Controller
{

    public function vsatConsumption($id)
    {

        $lines = 0;
        $total_invoice = 0;

        $listDiscounts = DiscountReceipt::GetDiscounts($id);
        $listBanks = BankAccount::GetList(Auth::user()->company_id);

        $myCompany = Company::MyCompanyForInvoice(Auth::user()->company_id);
        
        $detailInvoice = ConsumptionReceipt::GetInfoInvoice($id, Auth::user()->company_id);

        $myCustomer = CompanyCustomer::GetCustomerForInvoice($detailInvoice->customer_id, Auth::user()->company_id);

        $listDetails = DetailReceipt::GetListDetail($id);

        $dateStart = $detailInvoice->period."-01";
        $tmpDate = date("Y-m-d", strtotime($dateStart."+ 1 month"));
        $dateEnd = date("Y-m-d", strtotime($tmpDate."- 1 day"));
        $maxDay = substr($dateEnd, 8, 2);

        $arrayDetails = [];
        $listSan = [];

        foreach ($listDetails as $detail){

            if($detail->typeplan == 'PS'){
                $lines = $lines + 3; //print 3 lines on PDF

                if($detail->daystopay != $maxDay){
                    $payDays = $detail->daystopay;
                    $proration = number_format(($detail->usd_price * $detailInvoice->exchange / $maxDay) * $payDays, 2);
                    $textProration = $detailInvoice->cursymbol." ".number_format($proration, 2)." ".$detailInvoice->curiso;
                }else{
                    $textProration = '';
                    $payDays = '';
                }

            }else{
                $lines = $lines + 1; //print 1 line on PDF
                $payDays = '';
                $textProration = '';

            }

            if(strlen($textProration) > 0){
                $ammountToSum = $proration;
            }else{
                $ammountToSum = number_format($detail->usd_price * $detailInvoice->exchange, 2);
            }

            $total_invoice += $ammountToSum;

            //not show TK with $0 cost
            if($detail->typeplan == 'TK' and $ammountToSum == 0){
                
            }else{
                $arrayDetail = [
                    'orderId' => $detail->order_id,
                    'sanCode' => $detail->sancode,
                    'dateStart' => $detail->date_start,
                    'dateEnd' => $detail->date_end,
                    'typePlan' => $detail->typeplan,
                    'planName' => $detail->description.' : '.$detailInvoice->cursymbol." ".number_format($detail->usd_price * $detailInvoice->exchange, 2)." ".$detailInvoice->curiso,
                    'totalAmount' => $detailInvoice->cursymbol." ".number_format($detail->usd_price * $detailInvoice->exchange, 2)." ".$detailInvoice->curiso,
                    'ammountToSum' => $ammountToSum,
                    'payDays' => $payDays,
                    'proration' => $textProration
                ];
                array_push($arrayDetails, $arrayDetail);
            }
            
            $qtySan = count($listSan);
            if($qtySan == 0){
                array_push($listSan, array('sanCode' => $detail->sancode,
                                            'addressName' => $detail->addressname,
                                            'location' => $detail->district.', '.$detail->province.', '.$detail->department));

            }else{
                $validate = true;
                foreach ($listSan as $san){
                    if($san['sanCode'] == $detail->sancode){
                        $validate = false;
                    }
                }
                if($validate){
                    array_push($listSan, array('sanCode' => $detail->sancode,
                                                'addressName' => $detail->addressname,
                                                'location' => $detail->district.', '.$detail->province.', '.$detail->department));
                }
            }

        }

        $arrayBanks = [];

        foreach($listBanks as $bank){
            $curName = $bank->curName;
            $curName = str_replace('á', 'Á', $curName);
            $curName = str_replace('é', 'É', $curName);
            $curName = str_replace('í', 'Í', $curName);
            $curName = str_replace('ó', 'Ó', $curName);
            $curName = str_replace('ú', 'Ú', $curName);

            array_push($arrayBanks, array(
                'image' => $bank->image,
                'currency' => $curName,
                'number' => $bank->number_account,
                'cci' => $bank->cci_account
            ));
        }

        $arrayDiscounts = [];
        foreach($listDiscounts as $discount){
            array_push($arrayDiscounts, [
                'order_id' => $discount->order_id,
                'usd_discount' => round($discount->usd_discount * $detailInvoice->exchange, 2)
            ]);
        }

        $data = [
            'company_logo' => ".".$myCompany->image,
            'company_name' => $myCompany->name,
            'company_typedocument' => $myCompany->typedocument." ",
            'company_taxnumber' => $myCompany->taxnumber,
            'company_address' => $myCompany->address.", ".$myCompany->distric.", ".$myCompany->province,
            'company_number' => $myCompany->telephone,
            'company_email' => $myCompany->mail,
            'company_taxamount' => $myCompany->taxamount,
            'customer_typedocument' => $myCustomer->typedoc,
            'customer_taxnumber' => $myCustomer->taxnumber,
            'customer_name' => $myCustomer->name,
            'customer_address' => $myCustomer->address,
            'customer_location' => $myCustomer->district.", ".$myCustomer->province.", ".$myCustomer->department.", ".$myCustomer->country,
            'customer_currencyName' => $detailInvoice->curname,
            'customer_currencySymbol' => $detailInvoice->cursymbol,
            'customer_currencyIso' => $detailInvoice->curiso,
            'customer_currencyExchange' => $detailInvoice->exchange,
            'invoice_number' => $detailInvoice->serialnumber."-".$detailInvoice->correlative.substr($detailInvoice->created_at, 5 ,2).substr($detailInvoice->created_at, 2 ,2),
            'invoice_create' => date("Y-m-d", strtotime($detailInvoice->created_at)),
            'invoice_due' => $detailInvoice->date_due,
            'invoice_monthperiod' => substr($detailInvoice->period, 6, 2),
            'invoice_yearperiod' => substr($detailInvoice->period, 0 ,4),
            'invoice_total' => $total_invoice,
            'qr_code' => $detailInvoice->correlative.substr($detailInvoice->period, 5 ,2).substr($detailInvoice->period, 2 ,2).date("dmY", strtotime($detailInvoice->created_at)),
            'document_lines' => $lines,
            'listSan' => $listSan,
            'arrayDetails' => $arrayDetails,
            'arrayBanks' => $arrayBanks,
            'arrayDiscounts' => $arrayDiscounts
        ];

        $specialName = str_replace(' ', '_', $myCustomer->name);
        $specialName = str_replace('.', '', $specialName);
        $specialPeriod = str_replace('-', '_', $detailInvoice->period);

        $filename = $specialName."_".$specialPeriod.'.pdf';

        $pdf = PDF::loadView('pdfReports.myPDF', $data);
        $pdf->setPaper('a4');


        return $pdf->stream($filename);
        //return $pdf->download('itsolutionstuff.pdf');
    }
}
