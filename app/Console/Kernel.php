<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*
           $schedule->command('backup:clean')->daily()->at('01:00'); //Limpieza de copias de seguridad
           1 : 8pm & 18 : 1pm
          
           HACIENDO QUE FUNCIONE
           $ crontab -e
           * * * * * cd /var/www/html/astelproject && php artisan schedule:run >> /dev/null 2>&1
        */

        $schedule->command('backup:run')->twiceDaily(1, 18);

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
