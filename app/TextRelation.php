<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextRelation extends Model
{
    //
    protected $table = "textrelations";

    public function scopeGetListCampus($query, $brandId)
    {
        return $query->select('textrelations.id as code', 'namespecifications.title')
            ->join('namespecifications', 'textrelations.namespecification_id', '=', 'namespecifications.id')
            ->where([
                ['textrelations.brandproduct_id', '=', $brandId]
            ])
            ->get();
    }

}
