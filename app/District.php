<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    //
    public function scopeListDistricts($query, $countryId)
    {
        return $query->select('id as code', 'name')
            ->where('province_id', '=', $countryId)
            ->orderBy('name', 'ASC')
            ->get();
    }

}
