<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyCustomer extends Model
{
    //
    protected $table = "companies_customers";
    protected $primaryKey = ['provider_id', 'customer_id'];
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ['currency_id', 'status'];
    protected $guarded = [];

    public function scopeListCustomers($query, $providerId, $language)
    {
        return $query->select('customer_id', 'typecustomers.description as typeClient', 'companies.name', 'typedocuments.description as typedocument', 
                                'taxnumber', 'currencies.name as currency', 'districts.name as district', 'provinces.name as province')
            ->join('typecustomers', 'companies_customers.typecustomer_id', '=', 'typecustomers.id')
            ->join('companies', 'companies_customers.customer_id', '=', 'companies.id')
            ->join('typedocuments', 'companies.typedocument_id', '=', 'typedocuments.id')
            ->join('currencies', 'companies_customers.currency_id', '=', 'currencies.id')
            ->join('address', 'companies.id', '=', 'address.company_id')
            ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->where([
                ['companies_customers.provider_id', '=', $providerId],
                ['companies_address.typeaddress_id', '=', 'FCT'],
                ['companies_address.status', '=', true],
                ['typecustomers.language', $language]
            ])
            ->get();
    }

    public function scopeValidateCompanyIsHere($query, $companyId, $providerId)
    {
        return $query->select('customer_id')
            ->where([
                ['provider_id', $companyId],
                ['customer_id', $providerId],
                ['status', true]
            ])
            ->first();
    }

    public function scopeGetCustomerForInvoice($query, $customerId, $providerId)
    {
        return $query->select('typedocuments.description as typedoc', 'companies.taxnumber', 'companies.name', 'address.address',
                                'districts.name as district', 'provinces.name as province', 'departments.name as department', 'countries.name as country',
                                'currencies.id as curId', 'currencies.name as curname', 'currencies.symbol as cursymbol', 'currencies.iso4217 as curiso', 'companies_currencies.exchange as exchange',
                                'companies_customers.daystodue')
            ->join('currencies', 'companies_customers.currency_id', '=', 'currencies.id')
            ->join('companies_currencies', 'currencies.id', '=', 'companies_currencies.currency_id')
            ->join('companies', 'companies_customers.customer_id', '=', 'companies.id')
            ->join('typedocuments', 'companies.typedocument_id', '=', 'typedocuments.id')
            ->join('address', 'companies.id', '=', 'address.company_id')
            ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('countries', 'departments.country_id', '=', 'countries.id')
            ->where([
                    ['companies_customers.provider_id', $providerId],
                    ['companies.id', $customerId],
                    ['companies_address.typeaddress_id', 'FCT'],
                    ['companies_currencies.status', true]
                ])
            ->first();
    }

    public function scopeGetCustomerForReceipt($query, $customersId, $providerId)
    {
        return $query->select('companies.id as customerId', 'typedocuments.description as typedoc', 'companies.taxnumber', 'companies.name', 'address.address',
                                'districts.name as district', 'provinces.name as province', 'departments.name as department', 'countries.name as country',
                                'currencies.id as curId', 'currencies.name as curname', 'currencies.symbol as cursymbol', 'currencies.iso4217 as curiso', 'companies_currencies.exchange as exchange',
                                'companies_customers.daystodue')
            ->join('currencies', 'companies_customers.currency_id', '=', 'currencies.id')
            ->join('companies_currencies', 'currencies.id', '=', 'companies_currencies.currency_id')
            ->join('companies', 'companies_customers.customer_id', '=', 'companies.id')
            ->join('typedocuments', 'companies.typedocument_id', '=', 'typedocuments.id')
            ->join('address', 'companies.id', '=', 'address.company_id')
            ->join('companies_address', 'address.id', '=', 'companies_address.address_id')
            ->join('districts', 'address.district_id', '=', 'districts.id')
            ->join('provinces', 'districts.province_id', '=', 'provinces.id')
            ->join('departments', 'provinces.department_id', '=', 'departments.id')
            ->join('countries', 'departments.country_id', '=', 'countries.id')
            ->where([
                    ['companies_customers.provider_id', $providerId],
                    ['companies_address.typeaddress_id', 'FCT'],
                    ['companies_currencies.status', true]
                ])
            ->whereIn('companies.id', $customersId)
            ->get();
    }

    public function scopeGetInfoToCreateInvoice($query, $customerId, $providerId)
    {
        return $query->select('currencies.id as curId', 'companies_currencies.exchange as curEx', 'daystodue')
            ->join('currencies', 'companies_customers.currency_id', '=', 'currencies.id')
            ->join('companies_currencies', 'currencies.id', '=', 'companies_currencies.currency_id')
            ->join('companies', 'companies_customers.customer_id', '=', 'companies.id')
            ->where([
                    ['companies_customers.provider_id', $providerId],
                    ['companies.id', $customerId],
                    ['companies_currencies.status', true]
                ])
            ->first();
    }

}
