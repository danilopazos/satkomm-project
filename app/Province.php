<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    //

    public function scopeListProvinces($query, $departmentId)
    {
        return $query->select('id as code', 'name')
            ->where('department_id', '=', $departmentId)
            ->orderBy('name', 'ASC')
            ->get();
    }

}
