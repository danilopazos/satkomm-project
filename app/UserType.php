<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    //
    protected $table = "usertypes";
    public $timestamps = false;

    public function scopeGetList($query)
    {
        return $query->select('id as code', 'description')
            ->where('id', '<>', 'ALLS')
            ->get();
    }
}
