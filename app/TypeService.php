<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeService extends Model
{
    //
    protected $table = "typeservices";
    public $timestamps = false;

    public function scopeListTypeServices($query)
    {
        return $query->select('id as code', 'description')->get();
    }
}
