<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    //
    public $timestapms = false;

    public function scopeGetList($query){
        return $query->select('id as code', 'description')
            ->get();
    }

}
