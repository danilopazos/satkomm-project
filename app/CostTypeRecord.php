<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostTypeRecord extends Model
{
    protected $table = 'costtyperecords';

    public function scopeGetList($query, $language)
    {
        return $query->select('id as code', 'description')
            ->where([
                ['language', $language]
            ])
            ->get();
    }

}
