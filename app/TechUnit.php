<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechUnit extends Model
{
    //
    protected $table = "techunits";

    public function scopeListUnits($query){
        return $query->select('id as code', 'description')
            ->get();
    }

}
