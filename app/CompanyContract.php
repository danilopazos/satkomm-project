<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyContract extends Model
{
    //
    protected $table = 'companiescontracts';

    public function scopeNextCode($query, $customerId){
        $maxCode = $query->select('id')
            ->where([
                ['customer_id', $customerId]
            ])
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->first();
        if(isset($maxCode->id)){
            $newCode = $maxCode->id + 1;
        }else{
            $newCode = 1;
        }
        return $newCode;
    }

    public function scopeListContracts($query, $customerId, $providerId, $language){
        return $query->select('typeattachments.description', 'companiescontracts.document_attach')
            ->join('typeattachments', 'companiescontracts.typeattach_id', '=', 'typeattachments.id')
            ->where([
                ['typeattachments.language', $language],
                ['companiescontracts.customer_id', $customerId],
                ['companiescontracts.provider_id', $providerId],
                ['companiescontracts.status', true]
            ])
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function scopeGetListByCustomer($query, $providerId, $customerId, $language){
        return $query->select('companiescontracts.typeattach_id', 'companiescontracts.date_expedition', 'companiescontracts.document_attach', 'typeattachments.description', 'companiescontracts.anotations')
            ->join('typeattachments', 'companiescontracts.typeattach_id', '=', 'typeattachments.id')
            ->where([
                ['companiescontracts.provider_id', $providerId],
                ['companiescontracts.customer_id', $customerId],
                ['companiescontracts.status', true],
                ['typeattachments.language', $language]
            ])
            ->get();
    }
}
