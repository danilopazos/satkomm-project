<?php

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

//Error
Route::get('/error/nopermission', 'Error\ErrorController@noPermission')->name('error.permission');

//Platform
Route::get('/admin/showusers', 'Platform\AccountController@showUsers')->name('showUsers');
Route::get('/admin/newaccount', 'Platform\AccountController@newUserAccount')->name('newUserAccount');
Route::post('/admin/createaccount', 'Platform\AccountController@createAccount')->name('createAccount');
Route::get('/admin/resetpassword/{user}', 'Platform\AccountController@ResetPassword')->name('resetPassword');
Route::get('/admin/blockaccount/{user}', 'Platform\AccountController@blockAccount')->name('blockAccount');

//Personal
Route::get('/me/notes', 'Me\MeController@myNotes')->name('me.notes.myNotes');
Route::get('/me/newnote', 'Me\MeController@newNote')->name('me.notes.newNote');
Route::post('/me/savenewnote', 'Me\MeController@saveNewNote')->name('me.notes.saveNewNote');
Route::get('/me/viewnote/{id}', 'Me\MeController@viewNote')->name('me.notes.viewNote');
Route::post('/me/deletenote', 'Me\MeController@deleteNote')->name('me.notes.deleteNote');

//My profile
Route::get('/myprofile', 'Dashboard\MyProfileController@myProfile')->name('myProfile');
Route::get('/changepassword', 'Dashboard\MyProfileController@changePassword')->name('changePassword');
Route::post('/savenewpassword', 'Dashboard\MyProfileController@saveNewPassword')->name('saveNewPassword');
Route::post('/myprofile/update', 'Dashboard\MyProfileController@updateProfile')->name('updateProfile');

//My company
Route::get('/mycompany', 'MyCompany\MyCompanyController@myProfile')->name('mycompany.profile.information');
Route::get('/mycompany/timefordocumentsdue', 'MyCompany\EditMyCompanyController@showTimeForDocumentDue')->name('mycompany.profile.showTimeForDocumentDue');
Route::get('/mycompany/costsrecord', 'MyCompany\EditMyCompanyController@costsRecord')->name('mycompany.profile.costsRecord');
Route::get('/bank/listbanks', 'MyCompany\BankAccountController@listBanks')->name('mycompany.banks.listBanks');
Route::get('/bank/save', 'MyCompany\BankAccountController@saveAccount')->name('mycompany.banks.saveAccount');
Route::get('/typeexchange', 'MyCompany\TypeExchangeController@showTypeExchange')->name('mycompany.exchange.showTypeExchange');
Route::get('/mycompany/savecurrency', 'MyCompany\TypeExchangeController@saveCurrency')->name('mycompany.saveCurrency');
Route::get('/mycustomers', 'MyCompany\MyCompanyController@myCustomers')->name('mycompany.mycustomer.myCustomers');
Route::get('/myproviders', 'MyCompany\MyCompanyController@myProviders')->name('mycompany.myprovider.myProviders');
Route::get('/newcustomer', 'MyCompany\MyCompanyController@newCustomer')->name('mycompany.mycustomer.newCustomer');
Route::get('/newprovider', 'MyCompany\MyCompanyController@newProvider')->name('mycompany.myprovider.newProvider');
Route::get('/editcustomer/{id}', 'MyCompany\MyCompanyController@editCustomer')->name('mycompany.mycustomer.editCustomer');
Route::get('/editprovider/{id}', 'MyCompany\MyCompanyController@editProvider')->name('mycompany.myprovider.editProvider');
Route::post('/savenewcustomer', 'MyCompany\MyCompanyController@saveNewCustomer')->name('mycompany.saveNewCustomer');
Route::post('/savenewprovider', 'MyCompany\MyCompanyController@saveNewProvider')->name('mycompany.saveNewProvider');
Route::post('/updatecustomer', 'MyCompany\MyCompanyController@updateCustomer')->name('mycompany.updateCustomer');
Route::post('/updateprovider', 'MyCompany\MyCompanyController@updateProvider')->name('mycompany.updateProvider');
Route::get('/customerslist', 'MyCompany\MyCompanyController@customersList')->name('mycompany.customersList');
Route::get('/providerslist', 'MyCompany\MyCompanyController@providersList')->name('mycompany.providersList');
Route::get('/likeprovider/{id}', 'MyCompany\MyCompanyController@customerLikeProvider')->name('mycompany.customerLikeProvider');
Route::get('/likecustomer/{id}', 'MyCompany\MyCompanyController@providerLikeCustomer')->name('mycompany.providerLikeCustomer');
Route::get('/newaddress', 'MyCompany\MyCompanyController@newAddress')->name('mycompany.newAddress');
Route::post('/savenewaddress', 'MyCompany\MyCompanyController@saveNewAddress')->name('mycompany.saveNewAddress');
Route::get('/reviewcompany/{id}', 'MyCompany\MyCompanyController@reviewCompany')->name('mycompany.reviewCompany');
Route::get('/editaddress/{id}', 'MyCompany\MyCompanyController@showEditAddress')->name('mycompany.showEditAddress');
Route::post('/updateaddresscompany', 'MyCompany\MyCompanyController@updateAddressCompany')->name('mycompany.updateAddressCompany');
Route::get('/mycompany/attachdocuments/{id}', 'MyCompany\AttachDocumentsController@showPage')->name('mycompany.mycustomer.showPage');
Route::post('/mycompany/attachcontracts', 'MyCompany\AttachDocumentsController@upload')->name('mycompany.upload');
Route::get('/mycompany/downloadcontract/{filename}', 'MyCompany\AttachDocumentsController@download')->name('mycompany.download');
Route::post('/mycompany/updatemonthsdue', 'MyCompany\EditMyCompanyController@updateMonthsDue')->name('mycompany.updateMonthsDue');
Route::get('/mycompany/savecost', 'MyCompany\EditMyCompanyController@saveCost')->name('mycompany.saveCost');
Route::get('/mycompany/assignprices/{companyId}', 'MyCompany\AssignPrices@showAssignPrices')->name('mycompany.mycustomer.showAssignPrices');
Route::get('/mycompany/savelistprices', 'MyCompany\AssignPrices@savePrices')->name('mycompany.savePrices');

//Vsat
Route::get('/listplansvsat', 'Vsat\PlansController@listPlans')->name('vsat.plan.listPlans');
Route::get('/showplan/{id}', 'Vsat\PlansController@showPlan')->name('vsat.plan.showPlan');
Route::get('/newvsatplan', 'Vsat\PlansController@newPlan')->name('vsat.plan.newPlan');
Route::post('/mycompany/savenewvsatplan', 'Vsat\PlansController@savePlan')->name('vsat.savePlan');
Route::get('/editvsatplan/{id}', 'Vsat\PlansController@editPlan')->name('vsat.plan.editPlan');
Route::post('/mycompany/updatevsatplan', 'Vsat\PlansController@updatePlan')->name('vsat.updatePlan');
Route::get('/listservicesvsat', 'Vsat\VsatController@listServicesVsat')->name('vsat.order.listServicesVsat');
Route::get('/listinventorysvsat', 'Vsat\InventoryController@showList')->name('vsat.inventory.showList');
Route::get('/newinventoryvsat', 'Vsat\InventoryController@showFormNew')->name('vsat.inventory.showFormNew');
Route::post('/startinsertitemsvsat', 'Vsat\InventoryController@showInsertItems')->name('vsat.showInsertItems');
Route::post('/saveinventoryvsat', 'Vsat\InventoryController@saveInventoryVsat')->name('vsat.saveInventoryVsat');
Route::get('/editinventoryvsat/{id}', 'Vsat\InventoryController@showEdit')->name('vsat.inventory.showEdit');
Route::post('/updateinventoryvsat', 'Vsat\InventoryController@updateInventory')->name('vsat.updateInventory');
Route::get('/newordenservicevsat', 'Vsat\VsatController@newOrdenServicevsat')->name('vsat.order.newOrdenServicevsat');
Route::post('/saveneworderservicevsat', 'Vsat\VsatController@saveNewOrderService')->name('vsat.saveNewOrderService');
Route::get('/editorderservicevsat/{order}', 'Vsat\VsatController@showEditOrderService')->name('vsat.order.showEditOrderService');
Route::post('/updateorderservicevsat', 'Vsat\VsatController@UpdateOrderService')->name('vsat.UpdateOrderService');
Route::get('/vieworderservicevsat/{id}', 'Vsat\VsatController@ViewOrderService')->name('vsat.order.ViewOrderService');
Route::get('/newstatusorderservice/{order}', 'Vsat\OrderStatusController@showForm')->name('vsat.order.newStatus');
Route::post('/savenewstatusfororder', 'Vsat\OrderStatusController@saveStatus')->name('vsat.saveStatus');
Route::get('/download/attachstatus/{orderId}/{historyId}', 'Vsat\OrderStatusController@downloadAttach')->name('vsat.order.downloadAttach');
Route::get('/makeupgradeorderservice/{id}', 'Vsat\VsatController@makeUpgrade')->name('vsat.order.makeUpgrade');
Route::get('/listinvoicesvsat', 'Vsat\InvoiceController@showList')->name('vsat.invoices.showList');
Route::get('/viewordersconsumptions', 'Vsat\InvoiceController@showFormSearch')->name('vsat.invoices.showFormSearch');
Route::post('/viewordersconsumptions', 'Vsat\InvoiceController@showResultSearch')->name('vsat.invoices.showResultSearch');
Route::post('/createconsumptionsreceipts', 'Vsat\InvoiceController@createReceipts')->name('vsat.createReceipts');
Route::get('/orderservice/attachdocuments/{orderid}', 'Vsat\AttachDocumentController@showAttach')->name('vsat.order.showAttach');
Route::post('/orderservice/uploaddocument', 'Vsat\AttachDocumentController@uploadDocument')->name('vsat.uploadDocument');
Route::get('/orderservice/downloaddocument/{orderCode}/{filename}', 'Vsat\AttachDocumentController@downloadDocument')->name('vsat.downloadDocument');
Route::get('/orderservice/history/{orderid}', 'Vsat\HistoryOrderController@showHistory')->name('vsat.order.showHistory');
Route::get('/vsat/viewcosts/{orderid}', 'Vsat\ViewCostController@showCosts')->name('vsat.order.showCosts');
Route::get('/vsat/showassigncost/{orderid}', 'Vsat\ViewCostController@showAssignCost')->name('vsat.order.showAssignCost');
Route::post('/vsat/saveassigncost', 'Vsat\ViewCostController@saveAssignCost')->name('vsat.saveAssignCost');
Route::get('/vsat/downloadcostdocument/{ordercode}/{filename}', 'Vsat\ViewCostController@downloadCostDocument')->name('vsat.downloadCostDocument');
Route::get('/vsat/adddiscount/{order}', 'Vsat\OrderDiscountController@index')->name('vsat.order.addDiscount');
Route::post('/vsat/savediscount', 'Vsat\OrderDiscountController@store')->name('vsat.order.attachDiscount');
Route::get('/vsat/downloaddiscount/{orderId}/{discountId}', 'Vsat\OrderDiscountController@download')->name('vsat.order.downloadDiscount');
Route::get('/vsat/editreceipt/{receipt}', 'Vsat\EditReceiptController@index')->name('vsat.invoices.editReceipt');
Route::post('/vsat/updatereceipt', 'Vsat\EditReceiptController@store')->name('vsat.updateReceipt');

//PDF's
Route::get('/pdfReport/vsatconsumption/{id}', 'PdfReports\VsatController@vsatConsumption')->name('vsat.vsatConsumption');

//Api
Route::get('/api/getdepartments', 'Api\GetList@getDepartments')->name('getDepartments');
Route::get('/api/getprovinces', 'Api\GetList@getProvinces')->name('getProvinces');
Route::get('/api/getdistricts', 'Api\GetList@getDistricts')->name('getDistricts');
Route::get('/api/gettypeproducts', 'Api\GetList@getTypeProducts')->name('getTypeProducts');
Route::get('/api/getlistbrands', 'Api\GetList@getListBrands')->name('getListBrands');
Route::get('/api/getlistmodels', 'Api\GetList@getListModels')->name('getListModels');
Route::get('/api/getcustomers', 'Api\GetList@getListCustomers')->name('getListCustomers');
Route::get('/api/getaddresscustomers', 'Api\GetList@getAddressCustomers')->name('getAddressCustomers');
Route::get('/api/getlistplans', 'Api\GetList@getListPlans')->name('getListPlans');
Route::get('/api/getlistinventoryforneworder', 'Api\GetList@getListInventoryForNewOrder')->name('getListInventoryForNewOrder');
Route::get('/api/getlistplanprices', 'Api\GetList@getListPlanPrices')->name('getListPlanPrices');
Route::get('/api/getlistcompletewithprices', 'MyCompany\AssignPrices@getListCompleteWithPrices')->name('getListCompleteWithPrices');
Route::get('/api/validateorderservicecode', 'Api\GetValidate@validateCodeOrderServiceExist')->name('validateCodeOrderServiceExist');
Route::get('/api/getlisttypeinvoice', 'Api\GetList@getListTypeInvoice')->name('getListTypeInvoice');
Route::get('/api/getlistmonthearnings', 'Api\GetList@getListEarningsMonths')->name('getListEarningsMonths');
Route::get('/api/getlistearningsbymonths', 'Api\GetList@getListEarningsByMonths')->name('getListEarningsByMonths');
Route::get('/api/getlistinventory', 'Api\GetList@getListInventory')->name('getListInventory');
Route::get('/api/getqtyalertmessages', 'Api\GetList@getQtyAlertMessages')->name('getQtyAlertMessages');
Route::get('/api/getalertmessages', 'Api\GetList@getAlertMessages')->name('getAlertMessages');
Route::get('/api/disabledalertid', 'Api\MakeUpdates@updateDisableAlert')->name('updateDisableAlert');
Route::get('/api/getlistcurrencies', 'Api\GetList@getListCurrencies')->name('getListCurrencies');
Route::get('/api/getlistbanks', 'Api\GetList@getListBanks')->name('getListBanks');
Route::get('/api/getmycurrencies', 'Api\GetList@getMyListCurrencies')->name('getMyListCurrencies');
Route::get('/api/getcountpendings', 'Api\GetList@getTotalPendings')->name('getTotalPendings');
Route::get('/api/getlistpendings', 'Api\GetList@getListPendings')->name('getListPendings');
Route::get('/api/updateDisapproveOrder', 'Api\MakeUpdates@updateDisapproveOrderService')->name('updateDisapproveOrderService');
Route::get('/api/updateApproveOrder', 'Api\MakeUpdates@updateApproveOrderService')->name('updateApproveOrderService');
Route::get('/api/getdatedueorder/{orderid}', 'Api\GetList@getDateDueOrder')->name('getDateDueOrder');