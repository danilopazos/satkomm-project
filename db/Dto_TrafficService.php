<?php
class Dto_TrafficService extends Dto_Db{
    
   public function getAll(){
       $query = "select * from ait_trafficservice where deleted = 0";
       $rows = $this->selectRows($query);
       if(!$rows){ return false; }
       $total = count($rows);
       $aRecord = array();
       foreach($rows as $row){
           $e = new TrafficService();
           $e->code = $row["code"];
           $e->description = $row["description"];
           array_push($aRecord, $e);
       }
       return $aRecord;
   }
    
}