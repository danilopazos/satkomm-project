<?php

class Dto_ChargeType extends Dto_Db {

    public function getAll() {
        $query = "select * from chargetype";

        $rows = $this->selectRows($query);
        $aRecords = array();
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        foreach ($rows as $row) {
            $chargeType = new ChargeType();
            $chargeType->code = $row["chargecode"];
            $chargeType->name = $row["chargename"];
            array_push($aRecords, $chargeType);
        }

        return $aRecords;
    }

}
