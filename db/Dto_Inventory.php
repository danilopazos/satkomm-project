<?php

class Dto_Inventory extends Dto_Db {

    public function getImsiBySimCard($_simcard){

        $query  = "select I.imsi from inventoryitem I where I.iserial='$_simcard' ";
        $row = $this->selectOneRow($query);
        if(strlen($row['imsi'])==0){
            return false;
        }
        return $row['imsi'];

    }

    public function getInventoryHistoryById($_inventoryitemid) {

        $query = "select h.*  ";
        $query .= ", u.firstname, u.lastname  ";
        $query .= ", invoice.invoiceno, invoice.idcrypt as invoiceidcrypt, s.iserial, s.voicenumber, s.operationrequested  ";
        $query .= "from   ";
        $query .= "inventoryitem_history h  ";
        $query .= "left join ait_usersystem u on u.id = h.actionuserid ";
        $query .= "left join invoicetocustomer invoice on invoice.id = h.customerinvoiceid ";
        $query .= "left join inventoryitem s on s.id = h.atsimcardid ";
        $query .= "where h.inventoryitemid = $_inventoryitemid order by h.statusdate desc ";
        
        //echo $query; die();
        
        $_rows = $this->selectRows($query);
        $total = count($_rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();

        foreach ($_rows as $_row) {
            $e = new InventoryItemHistory();
            $e->id = $_row["id"];
            $e->inventoryitemid = $_row["inventoryitemid"];
            $e->inventorystatus = $_row["inventorystatus"];
            $e->statusdate = $_row["statusdate"];
            $e->statusdescription = stripslashes($_row["statusdescription"]);
            $e->customerinvoiceid = $_row["customerinvoiceid"];
            $e->atsimcardid = $_row["atsimcardid"];
            $e->atstatus = $_row["atstatus"];
            $e->actiondatetime = $_row["actiondatetime"];
            $e->actionuserid = $_row["actionuserid"];
            $e->actionuser_firstname = $_row["firstname"];
            $e->actionuser_lastname = $_row["lastname"];
            $e->customerinvoiceno = $_row["invoiceno"];
            $e->customerinvoiceidcrypt = $_row["invoiceidcrypt"];
            $e->simcardserial = $_row["iserial"];
            $e->simcardvoicenumber = $_row["voicenumber"];
            $e->simcardoperationrequested = $_row["operationrequested"];
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

//function

    public function updateInventoryStatus(
    $_inventoryitemid, $_inventorystatus, $_statusdate, $_statusdescription, $_customerinvoiceid, $_atsimcardid, $_atstatus, $_actionuserid
    ) {

        $_statusdescription = addslashes($_statusdescription);

        $query = "update inventoryitem set  ";
        $query .= " inventorystatus = $_inventorystatus , statusdate = '$_statusdate' , customerinvoiceid = $_customerinvoiceid ,  ";
        $query .= " atsimcardid = $_atsimcardid , atstatus = $_atstatus ,   ";
        $query .= " actiondatetime = CURRENT_TIMESTAMP() , actionuserid = '$_actionuserid' , statusdescription = '$_statusdescription'   ";
        $query .= " WHERE id = $_inventoryitemid  ";

        //echo $query; die();

        $this->updateInventoryStatusHistory($_inventoryitemid, $_inventorystatus, $_statusdate, $_statusdescription, $_customerinvoiceid, $_atsimcardid, $_atstatus, $_actionuserid);

        return $this->executeUpdate($query);
    }

    public function updateInventoryStatusHistory(
    $_inventoryitemid, $_inventorystatus, $_statusdate, $_statusdescription, $_customerinvoiceid, $_atsimcardid, $_atstatus, $_actionuserid
    ) {

        $_statusdescription = addslashes($_statusdescription);

        $query = " insert into inventoryitem_history ( ";
        $query .= "  inventoryitemid, inventorystatus, statusdate,  customerinvoiceid, atsimcardid, atstatus, actiondatetime, actionuserid , statusdescription )  values  ";
        $query .= " ( $_inventoryitemid, $_inventorystatus, '$_statusdate' ,  $_customerinvoiceid, $_atsimcardid , $_atstatus, CURRENT_TIMESTAMP(), '$_actionuserid' , '$_statusdescription' ) ";

        return $this->executeUpdate($query);
    }

    public function getItemByid($_id) {
        $query = " select i.* , m.idcrypt as mediaid from inventoryitem i ";
        $query .= " right join product p on p.idcrypt = i.productid  ";
        $query .= " left join media m on m.entity_id = p.id and m.type='IMAGE' and m.ismain = 1 and entity_table = 'PRODUCT' ";
        $query .= " where i.id =  $_id ";
        $row = $this->selectOneRow($query);
        $entity = new InventoryItem();
        if ($row) {
            $entity->id = $row["id"];
            $entity->sku = $row["sku"];
            $entity->productname = $row["productname"];
            $entity->datereceived = $row["datereceived"];
            $entity->productid = $row["productid"];
            $entity->iserial = $row["iserial"];
            $entity->documentcode = $row["documentcode"];
            $entity->accountid = $row["accountid"];
            $entity->issimcard = intval($row["issimcard"]);
            $entity->itemindex = intval($row["itemindex"]);
            $entity->inventorystatus = intval($row["inventorystatus"]);
            $entity->itemid = $row["itemid"];
            $entity->productprice = $row["productprice"];
            $entity->mediaid = $row["mediaid"];
            $entity->voicenumber = $row["voicenumber"];
            $entity->datanumber = $row["datanumber"];
            
            $entity->puk1 = $row["puk1"];
            $entity->puk2 = $row["puk2"];
            $entity->pin = $row["pin"];
            
            $entity->gawosc = intval($row["gawosc"]);
            $entity->operationregistered = $row["operationregistered"];
            $entity->operationrequested = $row["operationrequested"];
        }
        return $entity;
    }

    public function transferInventoryFromInvoice($_code, $_deliverydate) {

        $dto_invoice = new Dto_Invoice();
        $invoice = $dto_invoice->getByCode($_code);
        $dispatchItems = $dto_invoice->getDetailDispatchedByCode($_code);
        $total = count($dispatchItems);
        if ($total == 0) {
            return false;
        }
        $querydeleted = "delete from inventoryitem where documentcode = '$_code' ";
        $this->executeUpdate($querydeleted);

        $query = "insert into inventoryitem (accountid,  productid,   documentcode, itemindex, itemid, iserial, icode2, sku, productname, productprice, issimcard, datereceived, gawosc ) values  ";
        for ($i = 0; $i < $total; $i++) {
            $item = $dispatchItems[$i];
            $item->accountid = $invoice->accountid;
            $query .= " (   '" . $item->accountid . "' , '" . $item->productid . "' ,     '" . $item->documentcode . "' ,  " . $item->itemindex . "  , '" . $item->itemid . "' , '" . $item->iserial . "' , '" . $item->icode2 . "'  , '" . $item->sku . "'    , '" . $item->productname . "' ,   " . $item->productprice . "  , " . $item->issimcard . " , '$_deliverydate' ,  " . $item->gawosc . "   )      ";
            if (($i + 1) < $total) {
                $query .= " , ";
            }
        }

        $this->executeUpdate($query);
    }

    //---------INVENTORY DEVICES--------------------------
    public function getMyInventory($_status, $_accountid, $issimcard, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10, $_inventorystatus = 0) {

        $query = " select  i.* , m.idcrypt as mediaid ,  pldesc.name as planname   from inventoryitem i ";
        $query .= " right join product p on p.idcrypt = i.productid  ";
        $query .= " left join plan pl on pl.idcrypt = i.planid    ";
        $query .= " left join plandesc pldesc on pldesc.plan_id = pl.id  and pldesc.lang_code = 'ENG'  ";
        $query .= " left join media m on m.entity_id = p.id and m.type='IMAGE' and m.ismain = 1 and entity_table = 'PRODUCT' ";

        $query .= "where i.accountid = $_accountid and i.issimcard = $issimcard ";

        if ($_inventorystatus > 2) {
            $query .= " and i.inventorystatus > $_inventorystatus ";
        } else {
            $query .= " and i.inventorystatus = $_inventorystatus ";
        }

        if ($_search != "") {
            $query .= " and (";
            $query .= "  i.documentcode like '%$_search%' OR  ";
            $query .= "  i.productname like '%$_search%' OR  ";
            $query .= "  i.iserial like '%$_search%' OR   ";
            $query .= "  i.sku like '%$_search%' OR   ";
            $query .= "  i.voicenumber like '%$_search%' OR   ";
            $query .= "  i.datanumber like '%$_search%'  OR  ";
            $query .= "  pldesc.name like '%$_search%'    ";
            $query .= "  )  ";
        }

        if ($_status != "") {
            if ($_status == "activate") {
                $query .= " AND i.operationregistered = 'activate' ";
            } else if ($_status == "suspend") {
                $query .= " AND i.operationregistered = 'suspend' ";
            } else if ($_status == "deactivate") {
                $query .= " AND i.operationregistered = 'deactivate' ";
            }

            $query .= " OR ( i.operationregistered = 'none' ) ";
        }

        $query .= "ORDER BY $_sortCol $_sortDir";
        $query .= " LIMIT $_limit1 , $_limit2";

        //echo $query; die();
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            $entity = new InventoryItem();
            $entity->id = $row["id"];
            $entity->issimcard = $row["issimcard"];
            $entity->documentcode = $row["documentcode"];
            $entity->itemindex = $row["itemindex"];
            $entity->itemid = $row["itemid"];
            $entity->iserial = $row["iserial"];
            $entity->icode2 = $row["icode2"];
            $entity->sku = $row["sku"];
            $entity->issimcard = $row["issimcard"];
            $entity->productname = $row["productname"];
            $entity->productprice = $row["productprice"];
            $entity->datereceived = $row["datereceived"];
            $entity->issimcard = intval($row["issimcard"]);
            $entity->mediaid = trim($row["mediaid"]);

            $entity->voicenumber = trim($row["voicenumber"]);
            $entity->datanumber = trim($row["datanumber"]);
            $entity->puk1 = trim($row["puk1"]);
            $entity->puk2 = trim($row["puk2"]);
            $entity->pin = trim($row["pin"]);
            $entity->planid = trim($row["planid"]);
            $entity->planname = trim($row["planname"]);

            $entity->planid = trim($row["planid"]);
            $entity->planname = trim($row["planname"]);


            /*
              public $inventorystatus;
              public $statusdate;
              public $customerinvoiceid;
              public $atsimcardid;
              public $atstatus;
              public $actiondatetime;
              public $actionuserid;
             */
            $entity->inventorystatus = intval($row["inventorystatus"]);
            $entity->statusdate = trim($row["statusdate"]);
            $entity->customerinvoiceid = intval($row["customerinvoiceid"]);
            $entity->atsimcardid = intval($row["atsimcardid"]);
            $entity->atstatus = intval($row["atstatus"]);
            $entity->actiondatetime = $this->getDateByTimeZone($row["actiondatetime"]);
            $entity->actionuserid = trim($row["actionuserid"]);

            array_push($aRecords, $entity);
        }
        return $aRecords;
    }

    public function getMyInventoryTotal($_status, $_accountid, $issimcard, $_search, $_inventorystatus = 0) {

        $query = " select  count(*) as total   from inventoryitem i ";
        $query .= " right join product p on p.idcrypt = i.productid  ";
        $query .= " left join plan pl on pl.idcrypt = i.planid    ";
        $query .= " left join plandesc pldesc on pldesc.plan_id = pl.id  and pldesc.lang_code = 'ENG'  ";
        $query .= " left join media m on m.entity_id = p.id and m.type='IMAGE' and m.ismain = 1 and entity_table = 'PRODUCT' ";

        $query .= "where i.accountid = $_accountid and i.issimcard = $issimcard ";

        if ($_inventorystatus > 2) {
            $query .= " and i.inventorystatus > $_inventorystatus ";
        } else {
            $query .= " and i.inventorystatus = $_inventorystatus ";
        }

        if ($_search != "") {
            $query .= " and (";
            $query .= "  i.documentcode like '%$_search%' OR  ";
            $query .= "  i.productname like '%$_search%' OR  ";
            $query .= "  i.iserial like '%$_search%'  OR  ";
            $query .= "  i.sku like '%$_search%' OR   ";
            $query .= "  i.voicenumber like '%$_search%'  OR  ";
            $query .= "  i.datanumber like '%$_search%'  OR  ";
            $query .= "  pldesc.name like '%$_search%'     ";
            $query .= "  )  ";
        }

        if ($_status != "") {
            if ($_status == "activate") {
                $query .= " AND i.operationregistered = 'activate' ";
            } else if ($_status == "suspend") {
                $query .= " AND i.operationregistered = 'suspend' ";
            } else if ($_status == "deactivate") {
                $query .= " AND i.operationregistered = 'deactivate' ";
            }

            $query .= " OR ( i.operationregistered = 'none' ) ";
        }



        $total = intval($this->selectOneColumn("total", $query));

        return $total;
    }

    //---------INVENTORY SIMCARD--------------------------
    public function getMyInventorySimcard($_status, $_accountid, $issimcard, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10, $_inventorystatus = 0) {
        $issimcard = intval($issimcard);
        $query = " select  i.* , m.idcrypt as mediaid ,  pldesc.name as planname ,  inv.invoicedate , inv.dispatchdate  ";
        $query .= " from inventoryitem i ";
        $query .= " right join invoice inv on inv.code = i.documentcode ";
        $query .= " left join product p on p.idcrypt = i.productid  ";
        $query .= " left join plan pl on pl.idcrypt = i.planid    ";
        $query .= " left join plandesc pldesc on pldesc.plan_id = pl.id  and pldesc.lang_code = 'ENG'  ";
        $query .= " left join media m on m.entity_id = p.id and m.type='IMAGE' and m.ismain = 1 and entity_table = 'PRODUCT' ";

        $query .= "where i.accountid = $_accountid and i.issimcard = $issimcard ";

        if ($_inventorystatus < 2 && $_inventorystatus > 0) {
            // $query .=" and i.inventorystatus = $_inventorystatus ";
        }


        if ($_search != "") {
            $query .= " and (";
            $query .= "  i.documentcode like '%$_search%' OR  ";
            $query .= "  i.productname like '%$_search%' OR  ";
            $query .= "  i.iserial like '%$_search%' OR   ";
            $query .= "  i.sku like '%$_search%' OR   ";
            $query .= "  i.voicenumber like '%$_search%' OR   ";
            $query .= "  i.datanumber like '%$_search%'  OR  ";
            $query .= "  pldesc.name like '%$_search%'    ";
            $query .= "  )  ";
        }

        if ($_status != "") {
            if ($_status == "activate") {
                $query .= " AND i.operationregistered = 'activate' OR ( i.operationregistered = 'none' and i.operationrequested = 'airtime_order' ) ";
            } else if ($_status == "suspend") {
                $query .= " AND i.operationregistered = 'suspend' ";
            } else if ($_status == "deactivate") {
                $query .= " AND i.operationregistered = 'deactivate' ";
            } else if ($_status == "oninventory") {
                $query .= " AND i.inventorystatus = 1 ";
            } else if ($_status == "notinventory") {
                $query .= " AND i.inventorystatus = 0 ";
            }
        }

        $query .= "ORDER BY $_sortCol $_sortDir";
        $query .= " LIMIT $_limit1 , $_limit2";



        //echo $query; die();
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            $entity = new InventoryItem();
            $entity->id = $row["id"];
            $entity->issimcard = $row["issimcard"];
            $entity->documentcode = $row["documentcode"];
            $entity->itemindex = $row["itemindex"];
            $entity->itemid = $row["itemid"];
            $entity->iserial = $row["iserial"];
            $entity->icode2 = $row["icode2"];
            $entity->sku = $row["sku"];
            $entity->issimcard = $row["issimcard"];
            $entity->productname = $row["productname"];
            $entity->productprice = $row["productprice"];
            $entity->datereceived = $row["datereceived"];
            $entity->issimcard = intval($row["issimcard"]);
            $entity->mediaid = trim($row["mediaid"]);

            $entity->invoicedate = $row["invoicedate"];
            $entity->dispatchdate = $row["dispatchdate"];
            $entity->datereceived = $row["datereceived"];

            $entity->voicenumber = trim($row["voicenumber"]);
            $entity->datanumber = trim($row["datanumber"]);
            $entity->puk1 = trim($row["puk1"]);
            $entity->puk2 = trim($row["puk2"]);
            $entity->pin = trim($row["pin"]);
            $entity->planid = trim($row["planid"]);
            $entity->planname = trim($row["planname"]);

            $entity->planid = trim($row["planid"]);
            $entity->planname = trim($row["planname"]);
            $entity->operationrequested = trim($row["operationrequested"]);

            $entity->inventorystatus = intval($row["inventorystatus"]);
            $entity->statusdate = trim($row["statusdate"]);
            $entity->customerinvoiceid = intval($row["customerinvoiceid"]);
            $entity->atsimcardid = intval($row["atsimcardid"]);
            $entity->atstatus = intval($row["atstatus"]);
            $entity->actiondatetime = $this->getDateByTimeZone($row["actiondatetime"]);
            $entity->actionuserid = trim($row["actionuserid"]);

            array_push($aRecords, $entity);
        }
        return $aRecords;
    }

    public function getMyInventorySimcardTotal($_status, $_accountid, $issimcard, $_search, $_inventorystatus = 0) {
        $issimcard = intval($issimcard);
        $query = " select  count(*) as total   from inventoryitem i ";
        $query .= " left join product p on p.idcrypt = i.productid  ";
        $query .= " left join plan pl on pl.idcrypt = i.planid    ";
        $query .= " left join plandesc pldesc on pldesc.plan_id = pl.id  and pldesc.lang_code = 'ENG'  ";
        $query .= " left join media m on m.entity_id = p.id and m.type='IMAGE' and m.ismain = 1 and entity_table = 'PRODUCT' ";

        $query .= "where i.accountid = $_accountid and i.issimcard = $issimcard ";

        if ($_inventorystatus < 2 && $_inventorystatus > 0) {
            //$query .=" and i.inventorystatus = $_inventorystatus ";
        }

        if ($_search != "") {
            $query .= " and (";
            $query .= "  i.documentcode like '%$_search%' OR  ";
            $query .= "  i.productname like '%$_search%' OR  ";
            $query .= "  i.iserial like '%$_search%'  OR  ";
            $query .= "  i.sku like '%$_search%' OR   ";
            $query .= "  i.voicenumber like '%$_search%'  OR  ";
            $query .= "  i.datanumber like '%$_search%'  OR  ";
            $query .= "  pldesc.name like '%$_search%'     ";
            $query .= "  )  ";
        }

        if ($_status != "") {
            if ($_status == "activate") {
                $query .= " AND i.operationregistered = 'activate' OR ( i.operationregistered = 'none' and i.operationrequested = 'airtime_order' ) ";
            } else if ($_status == "suspend") {
                $query .= " AND i.operationregistered = 'suspend' ";
            } else if ($_status == "deactivate") {
                $query .= " AND i.operationregistered = 'deactivate' ";
            } else if ($_status == "oninventory") {
                $query .= " AND i.inventorystatus = 1 ";
            } else if ($_status == "notinventory") {
                $query .= " AND i.inventorystatus = 0 ";
            }
        }



        $total = intval($this->selectOneColumn("total", $query));

        return $total;
    }

    public function getSimInformationActivated(){
        $query = "select ao.id , ao.productid, ao.planid, ao.planidcryptforcustomer, ao.inventoryitemid, ao.operation , ao.iserial, ao.voicenumber, ao.datanumber, ao.productname, ao.servicetype_code, pla.price, pla.fixedpayment, ao.planname as providerplanname, ao.plannameforcustomer as customerplanname, acc.name as customername, ao.processeddatetime from airtimeoperation ao left join ait_account acc on acc.id = ao.accountassignedid left join plan pla on pla.idcrypt = ao.planidcryptforcustomer where ao.operation = 'activate' and ao.accountid = 10003 and ao.requeststatus = 'executed'";
        $_rows = $this->selectRows($query);
        return $_rows;
    }

    public function getSimcardsActivated($_accountid, $_operation, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10 ,  $_requeststatus = 'executed' ) {

        $query = "select ao.id , ao.productid, ao.planid, ao.planidcryptforcustomer, ";
        $query .= " ao.inventoryitemid, ao.operation , ao.iserial, ao.voicenumber, ao.datanumber, ";
        $query .= "ao.productname, ao.servicetype_code,  ";
        $query .= "ao.planname as providerplanname, ao.plannameforcustomer as customerplanname , acc.name as customername ";
        $query .= ", ao.processeddatetime ";
        $query .= "from airtimeoperation ao ";
        // $query .= "left join invoicetc_dispatchservice itc on itc.simcarditemid = ao.inventoryitemid ";
        $query .= "left join ait_account acc on acc.id = ao.accountassignedid ";
        $query .= "where ao.operation = '$_operation' and ao.requeststatus = '$_requeststatus'  ";
        $query .= "and accountid = $_accountid ";
        
        
        if($_requeststatus == "executed"){
            $query .= " and ao.processeddatetime = ( select max(processeddatetime) from airtimeoperation where inventoryitemid = ao.inventoryitemid )  ";
        }
        
        
        if (trim($_search) != "") {
            $query .= " and (";
            $query .= "    ao.iserial like '%$_search%'  ";
            $query .= " or ao.voicenumber like '%$_search%'  ";
            $query .= " or ao.datanumber like '%$_search%'  ";
            $query .= " or ao.productname like '%$_search%'  ";
            $query .= " or ao.planname like '%$_search%'  ";
            $query .= " or ao.plannameforcustomer like '%$_search%'  ";
            $query .= " or acc.name like '%$_search%'  ";
            $query .= " ) ";
        }

        $query .= "ORDER BY $_sortCol $_sortDir";
        $query .= " LIMIT $_limit1 , $_limit2";

        //echo $query; die();
        
        
        $_rows = $this->selectRows($query);
        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {

            $e = new SimcardActivated();
            $e->id = $_row["id"];
            $e->productid = $_row["productid"];
            $e->planid = $_row["planid"];
            $e->planidcryptforcustomer = $_row["planidcryptforcustomer"];
            $e->inventoryitemid = $_row["inventoryitemid"];
            $e->operation = $_row["operation"];
            $e->iserial = $_row["iserial"];
            $e->voicenumber = $_row["voicenumber"];
            $e->datanumber = $_row["datanumber"];
            $e->productname = $_row["productname"];
            $e->servicetype_code = $_row["servicetype_code"];
            $e->providerplanname = $_row["providerplanname"];
            $e->customerplanname = $_row["customerplanname"];
            $e->customername = $_row["customername"];
            $e->processeddatetime = $_row["processeddatetime"];
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getSimcardsActivatedTotal($_accountid, $_operation, $_search ,$_requeststatus = 'executed') {

        $query = "select count(*) as total ";
        $query .= "from airtimeoperation ao ";
        $query .= "left join invoicetc_dispatchservice itc on itc.simcarditemid = ao.inventoryitemid ";
        $query .= "left join ait_account acc on acc.id = ao.accountassignedid ";
        $query .= "where ao.operation = '$_operation' and ao.requeststatus = '$_requeststatus'  ";
        $query .= "and accountid = $_accountid ";
        
        if($_requeststatus == "executed"){
            $query .= " and ao.processeddatetime = ( select max(processeddatetime) from airtimeoperation where inventoryitemid = ao.inventoryitemid )  ";
        }
        
        if (trim($_search) != "") {
            $query .= " and (";
            $query .= "    ao.iserial like '%$_search%'  ";
            $query .= " or ao.voicenumber like '%$_search%'  ";
            $query .= " or ao.datanumber like '%$_search%'  ";
            $query .= " or ao.productname like '%$_search%'  ";
            $query .= " or ao.planname like '%$_search%'  ";
            $query .= " or ao.plannameforcustomer like '%$_search%'  ";
            $query .= " or acc.name like '%$_search%'  ";
            $query .= " ) ";
        }

        //echo $query; die();
        
        return intval($this->selectOneColumn("total", $query));
    }

    //----------------------------------------------------

    public function getMyInventoryActivatedByItemId($_inventoryitemid, $_accountid) {

        $query = "select  i.* , m.idcrypt as mediaid ,  pldesc.name as planname , pl.servicetype_code , pl.idcrypt as planidcrypt ";
        $query .= " from inventoryitem i  ";
        $query .= "right join product p on p.idcrypt = i.productid  ";
        $query .= "left join airtimeoperation ao on ao.inventoryitemid = i.id and ao.operation = 'activate' ";
        $query .= "left join plan pl on pl.idcrypt = ao.planidcryptforcustomer     ";
        $query .= "left join plandesc pldesc on pldesc.plan_id = pl.id  and pldesc.lang_code = 'ENG'   ";
        $query .= "left join media m on m.entity_id = p.id and m.type='IMAGE' and m.ismain = 1 and entity_table = 'PRODUCT'  ";
        $query .= "where i.accountid = $_accountid and i.issimcard = 1 ";
        $query .= " and i.operationregistered = 'activate'  ";
        $query .= " and ao.processeddatetime = ( select max(processeddatetime)  ";
        $query .= " from  airtimeoperation where inventoryitemid = i.id and operationregistered = 'activate'  ) ";
        $query .= " and i.id = $_inventoryitemid  limit 1";



        $row = $this->selectOneRow($query);
        if (!$row) {
            return false;
        }

        $entity = new InventoryItem();
        $entity->id = $row["id"];
        $entity->issimcard = $row["issimcard"];
        $entity->documentcode = $row["documentcode"];
        $entity->itemindex = $row["itemindex"];
        $entity->itemid = $row["itemid"];
        $entity->iserial = $row["iserial"];
        $entity->icode2 = $row["icode2"];
        $entity->sku = $row["sku"];
        $entity->issimcard = $row["issimcard"];
        $entity->productid = $row["productid"];
        $entity->productname = $row["productname"];
        $entity->productprice = $row["productprice"];
        $entity->datereceived = $row["datereceived"];
        $entity->issimcard = intval($row["issimcard"]);
        $entity->mediaid = trim($row["mediaid"]);

        $entity->voicenumber = trim($row["voicenumber"]);
        $entity->datanumber = trim($row["datanumber"]);
        $entity->puk1 = trim($row["puk1"]);
        $entity->puk2 = trim($row["puk2"]);
        $entity->pin = trim($row["pin"]);
        $entity->planid = trim($row["planidcrypt"]);
        $entity->planname = trim($row["planname"]);
        $entity->servicetype_code = trim($row["servicetype_code"]);


        $entity->inventorystatus = intval($row["inventorystatus"]);
        $entity->statusdate = trim($row["statusdate"]);
        $entity->customerinvoiceid = intval($row["customerinvoiceid"]);
        $entity->atsimcardid = intval($row["atsimcardid"]);
        $entity->atstatus = intval($row["atstatus"]);
        $entity->actiondatetime = $this->getDateByTimeZone($row["actiondatetime"]);
        $entity->actionuserid = trim($row["actionuserid"]);


        $entity->operationrequested = trim($row["operationrequested"]);
        $entity->operationregistered = trim($row["operationregistered"]);


        return $entity;
    }

    public function getMyInventoryActivated($_accountid, $issimcard, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10) {

        /*
          $query = " select  i.* , m.idcrypt as mediaid ,  pldesc.name as planname   from inventoryitem i ";
          $query .= " right join product p on p.idcrypt = i.productid  ";
          $query .= " left join plan pl on pl.idcrypt = i.planid    ";
          $query .= " left join plandesc pldesc on pldesc.plan_id = pl.id  and pldesc.lang_code = 'ENG'  ";
          $query .= " left join media m on m.entity_id = p.id and m.type='IMAGE' and m.ismain = 1 and entity_table = 'PRODUCT' ";

          $query .="where i.accountid = $_accountid and i.issimcard = $issimcard and i.operationregistered = 'activate' ";
         */

        $query = "select  i.* , m.idcrypt as mediaid ,  pldesc.name as planname , pl.servicetype_code ";
        $query .= " from inventoryitem i  ";
        $query .= "right join product p on p.idcrypt = i.productid  ";
        $query .= "left join airtimeoperation ao on ao.inventoryitemid = i.id and ao.operation = 'activate' ";
        $query .= "left join plan pl on pl.idcrypt = ao.planidcryptforcustomer     ";
        $query .= "left join plandesc pldesc on pldesc.plan_id = pl.id  and pldesc.lang_code = 'ENG'   ";
        $query .= "left join media m on m.entity_id = p.id and m.type='IMAGE' and m.ismain = 1 and entity_table = 'PRODUCT'  ";
        $query .= "where i.accountid = $_accountid and i.issimcard = $issimcard ";
        $query .= "and i.operationregistered = 'activate'  ";
        $query .= "and ao.processeddatetime = ( select max(processeddatetime)  ";
        $query .= "from  airtimeoperation where inventoryitemid = i.id and operationregistered = 'activate'  ) ";

        if ($_search != "") {
            $query .= " and (";
            $query .= "  i.documentcode like '%$_search%' OR  ";
            $query .= "  i.productname like '%$_search%' OR  ";
            $query .= "  i.iserial like '%$_search%' OR   ";
            $query .= "  i.sku like '%$_search%' OR   ";
            $query .= "  i.voicenumber like '%$_search%' OR   ";
            $query .= "  i.datanumber like '%$_search%'  OR  ";
            $query .= "  pldesc.name like '%$_search%'    ";
            $query .= "  )  ";
        }

        // echo $query; die();
        $query .= "ORDER BY $_sortCol $_sortDir";
        $query .= " LIMIT $_limit1 , $_limit2";


        //echo $query; die();

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            $entity = new InventoryItem();
            $entity->id = $row["id"];
            $entity->issimcard = $row["issimcard"];
            $entity->documentcode = $row["documentcode"];
            $entity->itemindex = $row["itemindex"];
            $entity->itemid = $row["itemid"];
            $entity->iserial = $row["iserial"];
            $entity->icode2 = $row["icode2"];
            $entity->sku = $row["sku"];
            $entity->issimcard = $row["issimcard"];
            $entity->productname = $row["productname"];
            $entity->productprice = $row["productprice"];
            $entity->datereceived = $row["datereceived"];
            $entity->issimcard = intval($row["issimcard"]);
            $entity->mediaid = trim($row["mediaid"]);

            $entity->voicenumber = trim($row["voicenumber"]);
            $entity->datanumber = trim($row["datanumber"]);
            $entity->puk1 = trim($row["puk1"]);
            $entity->puk2 = trim($row["puk2"]);
            $entity->pin = trim($row["pin"]);
            $entity->planid = trim($row["planid"]);
            $entity->planname = trim($row["planname"]);
            $entity->servicetype_code = trim($row["servicetype_code"]);

            $entity->operationrequested = trim($row["operationrequested"]);
            $entity->operationregistered = trim($row["operationregistered"]);

            array_push($aRecords, $entity);
        }
        return $aRecords;
    }

    public function getMyInventoryActivatedTotal($_accountid, $issimcard, $_search) {

        $query = " select  count(*) as total   from inventoryitem i ";
        $query .= " right join product p on p.idcrypt = i.productid  ";
        $query .= " left join plan pl on pl.idcrypt = i.planid    ";
        $query .= " left join plandesc pldesc on pldesc.plan_id = pl.id  and pldesc.lang_code = 'ENG'  ";
        $query .= " left join media m on m.entity_id = p.id and m.type='IMAGE' and m.ismain = 1 and entity_table = 'PRODUCT' and i.operationregistered = 'activate'  ";

        $query .= "where i.accountid = $_accountid and i.issimcard = $issimcard  and i.operationregistered = 'activate' ";
        if ($_search != "") {
            $query .= " and (";
            $query .= "  i.documentcode like '%$_search%' OR  ";
            $query .= "  i.productname like '%$_search%' OR  ";
            $query .= "  i.iserial like '%$_search%'  OR  ";
            $query .= "  i.sku like '%$_search%' OR   ";
            $query .= "  i.voicenumber like '%$_search%'  OR  ";
            $query .= "  i.datanumber like '%$_search%'  OR  ";
            $query .= "  pldesc.name like '%$_search%'     ";
            $query .= "  )  ";
        }

        $total = intval($this->selectOneColumn("total", $query));

        return $total;
    }

    public function setInventoryOperationRequested($_id, $_operationrequested) {
        $query = "update inventoryitem set operationrequested = '$_operationrequested' where id = $_id ";
        return $this->executeUpdate($query);
    }

    public function setInventoryOperationRequestedByBatch($_arecords, $_operationrequested) {

        $total = count($_arecords);
        if ($total == 0) {
            return false;
        }
        $query = "update inventoryitem set operationrequested = '$_operationrequested' where  ";
        for ($i = 0; $i < $total; $i++) {
            $item = $_arecords[$i];
            $query .= " id = " . $item["inventoryitemid"] . "  ";
            if (($i + 1) < $total) {
                $query .= " OR  ";
            }
        }
        return $this->executeUpdate($query);
    }
    
    
    //----------------------
    
    public function getInventoryDevicesOnCustomer($_accountid, $_status, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10) {

        $query = "SELECT invdet.deviceitemid, item.iserial, item.productid,  item.productname, item.sku,  ";
        $query .= "inv.accountid , inv.customerid, acc.name as customername,  item.documentcode as providerinvoice, inv.invoiceno customerinvoice ,";
        $query .= "inv.idcrypt as customerinvoiceid ,   inv.invoicedate, inv.invoiceduedate  ";
        $query .= " , invdet.operationdatetime ";
        $query .= "FROM satstore.invoicetc_dispatchdevice invdet ";
        $query .= "right join invoicetocustomer inv on inv.idcrypt = invdet.invoicetcid  ";
        $query .= "right join inventoryitem item on item.id = invdet.deviceitemid  ";
        $query .= "join ait_account acc on acc.id  = inv.customerid ";
        $query .= "where item.accountid = $_accountid and inv.deleted = 0 ";

        if ($_search != "") {
            $query .= " and (";
            $query .= "  item.iserial like '%$_search%' OR  ";
            $query .= "  item.productname like '%$_search%' OR  ";
            $query .= "  item.sku like '%$_search%' OR   ";
            $query .= "  acc.name like '%$_search%' OR   ";
            $query .= "  item.documentcode like '%$_search%' OR   ";
            $query .= "  inv.invoiceno like '%$_search%'     ";
            $query .= "  )  ";
        }
 
        $query .= "ORDER BY $_sortCol $_sortDir";
        $query .= " LIMIT $_limit1 , $_limit2";
        
        //echo $query; die();
        
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            $e = new DeviceOnSale();
            
            $e->inventoryitemid = $row["deviceitemid"];
            $e->iserial= $row["iserial"];
            $e->productid = $row["productid"];
            $e->productname = $row["productname"];
            $e->sku = $row["sku"];
            $e->accountid = $row["accountid"];
            $e->customerid = $row["customerid"];
            $e->customername = $row["customername"];
            $e->providerinvoice = $row["providerinvoice"];
            $e->customerinvoice = $row["customerinvoice"];
            $e->customerinvoiceid = $row["v"];
            $e->invoicedate = $row["invoicedate"];
            $e->invoiceduedate = $row["invoiceduedate"];
            $e->operationdatetime = $row["operationdatetime"];

            array_push($aRecords, $e);
        }
        return $aRecords;
    }
    
    public function getInventoryDevicesOnCustomerTotal($_accountid, $_status, $_search  ) {

        $query = "SELECT count(*) as total ";
        $query .= "FROM satstore.invoicetc_dispatchdevice invdet ";
        $query .= "right join invoicetocustomer inv on inv.idcrypt = invdet.invoicetcid  ";
        $query .= "right join inventoryitem item on item.id = invdet.deviceitemid  ";
        $query .= "join ait_account acc on acc.id  = inv.customerid ";
        $query .= "where item.accountid = $_accountid and inv.deleted = 0 ";

        if ($_search != "") {
            $query .= " and (";
            $query .= "  item.iserial like '%$_search%' OR  ";
            $query .= "  item.productname like '%$_search%' OR  ";
            $query .= "  item.sku like '%$_search%' OR   ";
            $query .= "  acc.name like '%$_search%' OR   ";
            $query .= "  item.documentcode like '%$_search%' OR   ";
            $query .= "  inv.invoiceno like '%$_search%'     ";
            $query .= "  )  ";
        }
  
      
        return intval( $this->selectOneColumn("total", $query) );
    }

}
