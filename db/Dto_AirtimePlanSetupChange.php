<?php

class Dto_AirtimePlanSetupChange extends Dto_Db {
    
    public function register($_airtimeSetupChange){
      
        $_planidcrypt = $_airtimeSetupChange->planidcrypt;
        $_accountidchanger = $_airtimeSetupChange->accountidchanger;
        $_useridchanger = $_airtimeSetupChange->useridchanger;
        $_tonotify = $_airtimeSetupChange->tonotify;
        
        $_trafficIncrement = serialize($_airtimeSetupChange->trafficIncrement);
        $_trafficSetup = serialize($_airtimeSetupChange->trafficSetup);
        $_trafficAllowance = serialize($_airtimeSetupChange->trafficAllowance);
        $_trafficAllowanceDetail = serialize($_airtimeSetupChange->trafficAllowanceDetail);
        
        
        $query  = "insert into apsetupchange (planidcrypt, accountidchanger, useridchanger, tonotify , ";
        $query .= " datetimechange, trafficincrement, trafficsetup, trafficallowance, trafficallowancedetail ) ";
        $query .= " values ";
        $query .= " ('$_planidcrypt' , $_accountidchanger , $_useridchanger , $_tonotify , ";
        $query .= " CURRENT_TIMESTAMP(), '$_trafficIncrement' , '$_trafficSetup' , '$_trafficAllowance' , '$_trafficAllowanceDetail' )  ";
        
        return  $this->executeUpdate( $query  );
        
    }//function
    

}