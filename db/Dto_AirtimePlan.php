<?php

class Dto_AirtimePlan extends Dto_Db {
    
    public function recalculateChildPlans($_planidcrypt , $_accountid, $_userid, $_tonotify=0){
        
        $_ok = true;
         
        $_dtoPlan = new Dto_AirtimePlan();
        $_planId = $_dtoPlan->getIdByIdCrypt($_planidcrypt);
        
        $_fatherTrafficIncrement = $this->getTrafficIncrementByPlanId($_planId); 
        $_fatherTrafficSetup = $this->getTrafficSetupeByPlanId($_planId);  
        $_fatherTrafficAllowance = $this->getTrafficAllowanceByPlanId($_planId);  
       
        $_childplansIds = $this->getIdsPlanChildsByIdcrypt($_planidcrypt);
        
        
        if(!$_childplansIds){return true;}
        
        foreach($_childplansIds as $_childplan){
            
            $_childidcrypt= trim($_childplan->idcrypt);
            $_childid     = intval($_childplan->id);
            
            $aIncrement = array();
            $aSetup     = array();
            $aAllowance = array();
            $aAllowanceDetail = array();
            
            $_childTrafficIncrement = $this->getTrafficIncrementByPlanId($_childid);
            $_childTrafficSetup = $this->getTrafficSetupeByPlanId($_childid);
            $_childTrafficAllowance = $this->getTrafficAllowanceByPlanId($_childid);
            
            //we need to compare the increment
            
            //Traffic Increment
            $_fTotalIncrement = count($_fatherTrafficIncrement);
            if( $_fTotalIncrement > 0 ){
                 
                foreach( $_fatherTrafficIncrement as $fIncrement ){
                    $_fIncrementExists = false;
                    
                  
                    
                    $_totalIncrement = count($_childTrafficIncrement);
                    if($_totalIncrement > 0){
                        
                        for( $i = 0; $i < $_totalIncrement && !$_fIncrementExists; $i++ ){
                            $_eIncrement = $_childTrafficIncrement[$i];
                            if(
                                    $fIncrement->traffictypecode == $_eIncrement->traffictypecode &&
                                    $fIncrement->min_unitcode == $_eIncrement->min_unitcode &&
                                    $fIncrement->min_unitqty == $_eIncrement->min_unitqty &&
                                    $fIncrement->inc_unitcode == $_eIncrement->inc_unitcode &&
                                    $fIncrement->inc_unitqty == $_eIncrement->inc_unitqty
                                    
                                    ){
                                 $_fIncrementExists = true;
                            }
                        }
                    }
                    
                    if(!$_fIncrementExists){
                        
                        $tempIncrement = $fIncrement;
                        $tempIncrement->planid = $_childid;
                        $tempIncrement->planidcrypt = $_childidcrypt;
                                
                        array_push($aIncrement, $tempIncrement);
                        
                    }
                    
                    
                }//foreach
                
            }//if
            
             
            //Traffic Setup
            $_fTotalSetup = count($_fatherTrafficSetup);
            if( $_fTotalSetup > 0 ){
                
                foreach( $_fatherTrafficSetup as $fSetup ){
                    $_fSetupExists = false;
                    $_totalSetup = count($_childTrafficSetup);
                    if($_totalSetup > 0 ){
                        
                         for( $i = 0; $i < $_totalSetup && !$_fSetupExists; $i++ ){
                             
                            $_eSetup = $_childTrafficSetup[$i];
                            /*
                            if(
                                    $fSetup->ttcode_source == $_eSetup->ttcode_source &&
                                    $fSetup->ttcode_dest == $_eSetup->ttcode_dest &&
                                    $fSetup->tunitcode == $_eSetup->tunitcode &&
                                    $fSetup->chargecode == $_eSetup->chargecode 
                                    
                                    ){
                                 $_fSetupExists = true;
                            }                             
                            */ 
                            if(  $fSetup->ttcode_source == $_eSetup->ttcode_source ){
                                
                                $_fSetupExists = true;
                                 
                                 
                                if( $fSetup->tunitcode != $_eSetup->tunitcode || 
                                    $fSetup->chargecode != $_eSetup->chargecode  ){
                                    
                                    //$_fSetupExists = true;
                                    //we need to update the traffic type code
                                    $_queryUpdateTrafficSetup  = "update ait_trafficsetup set "; 
                                    $_queryUpdateTrafficSetup .= "  tunitcode = '".$fSetup->tunitcode."' , chargecode = '".$fSetup->chargecode."'    "; 
                                    $_queryUpdateTrafficSetup .= "where id = ".$_eSetup->id."  "; 
                                    
                                   // echo $_queryUpdateTrafficSetup; die();
                                    
                                    $this->executeUpdate( $_queryUpdateTrafficSetup );                                    
                                    
                                }                                  
                                 
                                 
                            }      //if                       
                            
                            
                         }//for
                        
                        
                    }//if
                    
                    if(!$_fSetupExists){
                        
                        $tempSetup = $fSetup;
                        $tempSetup->planid = $_childid;
                        $tempSetup->planidcrypt = $_childidcrypt;
                        $tempSetup->chargeammount = $fSetup->chargeammount + ( $fSetup->chargeammount * 0.2  ) ;
                        
                        array_push($aSetup, $tempSetup);
                    }                    
                    
                    
                }//foreach                
                
            }//if
           
            
            //Traffic Allowance
            $_fTotalAllowance = count($_fatherTrafficAllowance);
            if( $_fTotalAllowance > 0 ){
                
                foreach( $_fatherTrafficAllowance as $fAllowance ){
                    $_fAllowanceExists = false;
                    
                    $_totalAllowance = count($_childTrafficAllowance);
                    if($_totalAllowance > 0 ){
                        
                        for( $i = 0; $i < $_totalAllowance && !$_fAllowanceExists; $i++ ){
                              
                            $_eAllowance = $_childTrafficSetup[$i];
                            if(  $fAllowance->seedcode == $_eAllowance->seedcode  ){
                                 //$_fSetupExists = true;
                                $_fAllowanceExists = true;
                                
                                $_totalAllowanceDetail = count($fAllowance->aTrafficAllowanceDetail);
                                if($_totalAllowanceDetail > 0){
                                    
                                    $aFAllowanceDetail = $fAllowance->aTrafficAllowanceDetail;
                                    
                                    for($j=0; $j<$_totalAllowanceDetail; $j++){
                                        
                                        $_fAllowanceDetailElement = $aFAllowanceDetail[j];
                                        
                                        $_fAllowanceDetailExists = false;
                                        $_totalAllowanceDetailChild = count($_eAllowance->aTrafficAllowanceDetail);
                                        $_aChildAllowanceDetailChild = $_eAllowance->aTrafficAllowanceDetail;
                                        
                                        if($_totalAllowanceDetailChild > 0){
                                            for( $x=0; $x<$_totalAllowanceDetailChild; $x++  ){
                                                
                                                $_eChildAllowanceDetailElement = $_aChildAllowanceDetailChild[$x];
                                                
                                                if(
                                                       $_eChildAllowanceDetailElement->ttcode_source  ==  $_fAllowanceDetailElement->ttcode_source &&
                                                       $_eChildAllowanceDetailElement->tunitcode  ==  $_fAllowanceDetailElement->tunitcode
                                                        
                                                        ){
                                                    
                                                    $_fAllowanceDetailExists = true;
                                                    
                                                }
                                                
                                            }//for
                                            
                                        }//if
                                        
                                        ///-----------------------
                                        if(!$_fAllowanceDetailExists){
                                            
                                            $tempAllowanceDetailElement                     = $_fAllowanceDetailElement;
                                            $tempAllowanceDetailElement->trafficallowanceid = $_eAllowance->id;
                                            $tempAllowanceDetailElement->planidcrypt        =  $_childidcrypt;
                                            
                                            array_push($aAllowanceDetail, $tempAllowanceDetailElement);
                                            
                                        }
                                        
                                        ///-----------------------
                                    }//for
                                    
                                }//if
                                ///-----------------------
                                if(!$_fAllowanceDetailExists){

                                    $tempAllowance  = $_eAllowance;
                                    $tempAllowance->planid        =  $_childid;

                                    array_push($aAllowance, $tempAllowance);

                                }

                                ///-----------------------
                                 
                            }//if
                            
                                   
                        }//for
                        
                    }//if
                    
                    
                }//foreach
                
            
            }//if
            
            
            
            
            ///----------------------------------------------------------------
            $_registerNotification = false;
            //we need to register the Increment Remaining
            if(count($aIncrement) >0 ){
                $dto_trafficIncrement = new Dto_TrafficIncrement();
                $_ok = $dto_trafficIncrement->registerBatch($aIncrement);
                $_registerNotification = true;
            }
            
            //we need to register the Setup Remaining
            if(count($aSetup)>0){
                $dto_trafficSetup = new Dto_TrafficSetup();
                $_ok = $dto_trafficSetup->registerBatch($aSetup);
                $_registerNotification = true;
            }
            
            $dto_trafficAllowance = new Dto_TrafficAllowance();
            //we need to register the allowance detail remaining
            if(count($aAllowanceDetail)){
                $_ok = $dto_trafficAllowance->registerBatchDetail( $aAllowanceDetail );
                $_registerNotification = true;
            }
            
            //we need to register the allowance group if is empty
            if(count($aAllowance) > 0){
                $_ok = $dto_trafficAllowance->registerBatch( $aAllowance );
                $_registerNotification = true;
            }
            
            //RegisterNotificacion
            if($_registerNotification){
                $apsc = new AirtimePlanSetupChange();
                
                $apsc->planidcrypt            = $_childidcrypt;
                $apsc->accountidchanger       = $_accountid;
                $apsc->useridchanger          = $_userid;
                $apsc->trafficIncrement       = $aIncrement;
                $apsc->trafficSetup           = $aSetup;
                $apsc->trafficAllowanceDetail = $aAllowanceDetail;
                $apsc->trafficAllowance       = $aAllowance;
                $apsc->tonotify               = $_tonotify;
                
                $dto_apsc = new Dto_AirtimePlanSetupChange();
                $dto_apsc->register($apsc); 
                
            }//if
            
            
        }//foreach
        
        
        return $_ok;
        
    }//function
    
    public function getIdsPlanChildsByIdcrypt($_idcrypt){
        $query ="select id, idcrypt from plan where parentplan = '$_idcrypt' ";        
        $_result = $this->selectRows($query);
        
        if(!$_result){ return false;}
        
        $_total = count($_result);
        if($_total == 0){
            return false;
        }
        
        $aPlans = array();
        foreach($_result as $_row){
            $plan = new AirtimePlan();
            
            $plan->idcrypt  = trim($_row["idcrypt"]);
            $plan->id       = intval($_row["id"]);
            
            array_push($aPlans, $plan);
        }
        
        return $aPlans;
    }//function
    
    
    public function getPlanDescTemporary($_codePlan, $_language = 'ESP'){
        $query  = "select D.name, P.price from plandesc D, plan P ";
        $query .= "where P.id=D.plan_id and P.id=$_codePlan and D.lang_code='$_language' ";
    
        $row = $this->selectOneRow($query);
    
        return $row;
    }
    
    public function getListAll(  $_accountid = 0, $_langcode = "ENG") {

        $query = "select p.* , pd.name, pd.intro, pd.description from plan p ";
        $query .= "right join plandesc pd on pd.plan_id = p.id  and lang_code = '$_langcode'  ";
        $query .= "where p.deleted = 0 ";
        $query .= " AND p.accountid = $_accountid ";
        
        $query .= "ORDER BY pd.name asc"; 
        
      
        
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            array_push($aRecords, $this->getRow($row));
        }
        return $aRecords;
    }

    
    
    
    public function deleteAirtimePlan($_idcrypt){
        
        $query ="update plan set deleted = 1 where idcrypt = '$_idcrypt' ";
        $this->executeUpdate($query);
        
    }
    
    
    
    public function getSimcardsByPlan($_providerid , $_planid, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10) {
        
        
        $query = "SELECT ";
        $query .="i.id, i.iserial, i.voicenumber, i.datanumber, i.sku, i.productname , i.operationregistered, ao.datetoexecute , acc.name as customername ";
        $query .="FROM  inventoryitem i  ";
        $query .="right join airtimeoperation ao on ao.iserial = i.iserial ";
        
        
        if($_providerid == 10001){
            $query .="left join ait_account acc on acc.id = ao.accountid ";
        }else{
            $query .="left join ait_account acc on acc.id = ao.accountassignedid ";
        }
        
        $query .="where ( i.operationregistered = 'activate' or i.operationregistered = 'suspend' or i.operationregistered = 'deactivate' )  and ";
        $query .="ao.datetoexecute = (  select max(datetoexecute) from airtimeoperation where iserial = i.iserial ) ";
        $query .="and (ao.planid = '$_planid'   or  ao.planidcryptforcustomer = '$_planid' ) ";
        
         if ($_search != "") {
            $query .= " and (";
            $query .= " i.sku like '%$_search%' OR  ";
            $query .= " i.iserial like '%$_search%' OR  ";
            $query .= " acc.name like '%$_search%' OR  ";   
            $query .= " i.operationregistered like '%$_search%' OR  "; 
            $query .= " i.datanumber like '%$_search%' OR  ";
            $query .= " i.voicenumber like '%$_search%' OR  ";
            $query .= " i.productname like '%$_search%'    ";
            $query .= "  )  ";
        }
      
        $query .= "ORDER BY $_sortCol $_sortDir";
        
        $query .= " LIMIT $_limit1 , $_limit2";


        $_rows = $this->selectRows($query);
        $total = count($_rows);
        $aRecord = array();

        if ($total == 0) {
            return false;
        }
        foreach ($_rows as $_row) {

            $e = new Simcard();
            $e->inventoryitemid = $_row["id"];
            $e->iserial = $_row["iserial"];
            $e->customername = $_row["customername"];
            $e->voicenumber = $_row["voicenumber"];
            $e->datanumber = $_row["datanumber"];
            $e->sku = $_row["sku"];
            $e->productname = $_row["productname"];
            $e->operationregistered = $_row["operationregistered"];
            $e->datetoexecute = $_row["datetoexecute"];
            array_push($aRecord, $e);
        }

        return $aRecord;
    }

    public function getSimcardsByPlanTotal( $_planid , $_search  ) {
      
        
        $query = "SELECT ";
        $query .=" count(*) as total ";
        $query .="FROM  inventoryitem i  ";
        $query .="right join airtimeoperation ao on ao.iserial = i.iserial ";
        $query .="left join ait_account acc on acc.id = ao.accountassignedid ";
        $query .="where ( i.operationregistered = 'activate' or i.operationregistered = 'suspend' or i.operationregistered = 'deactivate' )  and ";
        $query .="ao.datetoexecute = (  select max(datetoexecute) from airtimeoperation where iserial = i.iserial ) ";
        $query .="and (ao.planid = '$_planid'   or  ao.planidcryptforcustomer = '$_planid' ) ";
        
         if ($_search != "") {
            $query .= " and (";
            $query .= " i.sku like '%$_search%' OR  ";
            $query .= " i.iserial like '%$_search%' OR  ";
            $query .= " acc.name like '%$_search%' OR  ";
            $query .= " i.operationregistered like '%$_search%' OR  "; 
            $query .= " i.datanumber like '%$_search%' OR  ";
            $query .= " i.voicenumber like '%$_search%' OR  ";
            $query .= " i.productname like '%$_search%'    ";
            $query .= "  )  ";
        }
        return intval($this->selectOneColumn("total", $query));
    }
 
    public function updatePlanInfo($e) {
        $_id = $e->id;

        $_sku = $e->sku;
        
        
        $_version = $e->version;
        $_aviable = $e->aviable;
        
        $_price = doubleval($e->subscriptionprice);
        $_fixedpayment = doubleval($e->fixedpayment);
        $_activationpayment = doubleval($e->activationpayment);
        $_suspensionpayment = doubleval($e->suspensionpayment);
        $_deactivationpayment = doubleval($e->deactivationpayment);

        $_name = $e->name;
        $_description = $e->description;


        $query = "UPDATE plan SET ";
        $query .= " sku = '$_sku' , price  = $_price , fixedpayment =$_fixedpayment , ";
        
        $query .= " aviable = $_aviable , vers = '$_version' ,  ";
        
        $query .= " activationpayment = $_activationpayment , suspensionpayment = $_suspensionpayment , ";
        $query .= " deactivationpayment = $_deactivationpayment ";
        $query .= " where id = $_id ";

        $this->executeUpdate($query);

        $query = "UPDATE plandesc set name = '$_name' , intro = '$_description', description = '$_description' where plan_id = $_id  ";

        $this->executeUpdate($query);
    }

    public function updateAllowance($e, $aRecord) {
        $_id = $e->id;

        $total = count($aRecord);
        if ($total > 0) {

            foreach ($aRecord as $record) {
                $seedcode = trim($record->seedcode);
                $qty = intval($record->qty);
                $tname = trim($record->tname);
                $query = "update ait_trafficallowance set qty = $qty , tname = '$tname'    where planid  = $_id and seedcode = '$seedcode' ";
                //echo $query; die();
                $this->executeUpdate($query);
            }//foreach
        }//if
    }

    public function updateAdditional($e, $aRecord) {
        $_id = $e->id;
        //we need to update the qty
        // select planid, ttcode_source, chargeammount from ait_trafficsetup where  planid = 6 and deleted = 0;
        $total = count($aRecord);
        if ($total > 0) {

            foreach ($aRecord as $record) {
                $chargeammount = doubleval($record->chargeammount);
                $ttcode_source = trim($record->ttcode_source);
                $query = "update ait_trafficsetup set chargeammount = $chargeammount where planid  = $_id and ttcode_source = '$ttcode_source' ";

                $this->executeUpdate($query);
            }//foreach
        }//if
    }

    public function getTotalSku($_sku) {

        $query = "select count(*) as total from plan where sku like '$_sku%' ";
        return $this->selectOneColumn("total", $query);
    }

    public function getTrafficSetupeByPlanId($_planid) {

        $query = "select td.id, td.ttcode_source , td.tunitcode , tt.description , td.chargecode, td.chargeammount ";
        $query .= "from ait_trafficsetup td  ";
        $query .= "left join ait_traffictype tt on tt.code =  td.ttcode_source ";
        $query .= "where td.planid = $_planid and td.deleted = 0 order by td.ttcode_source asc";
     
        //echo $query; die();
        
        
        $rows = $this->selectRows($query);
        $aRecord = array();

        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        foreach ($rows as $row) {
            $e = new TrafficSetup();
            $e->id = $row["id"];
            $e->ttcode_source = $row["ttcode_source"];
            $e->tunitcode = $row["tunitcode"];
            $e->description = $row["description"];
            $e->chargecode = $row["chargecode"];
            $e->chargeammount = doubleval($row["chargeammount"]);

            array_push($aRecord, $e);
        }

        return $aRecord;
    }

    public function getTrafficAllowanceByPlanId($_planid) {
        $query = "SELECT id, tname, qty , seedcode  FROM ait_trafficallowance where planid = $_planid and deleted = 0 ";
      
        $rows = $this->selectRows($query);
        $aRecord = array();

        $total = count($rows);
        if ($total == 0) {
            return false;
        }
  
        foreach ($rows as $row) {
            $e = new TrafficAllowance();
            $e->id = $row["id"];
            $e->tname = $row["tname"];
            $e->seedcode = trim($row["seedcode"]);
            $e->qty = intval($row["qty"]);
            $e->aTrafficAllowanceDetail = $this->getTrafficAllowanceDetailById($e->id);
            array_push($aRecord, $e);
        }



        return $aRecord;
    }

    public function getTrafficAllowanceDetailById($_taid) {

        $query = "select td.ttcode_source , td.tunitcode , tt.description, td.trafficallowanceid, td.planidcrypt ";
        $query .= "from ait_trafficallowance_detail td  ";
        $query .= "left join ait_traffictype tt on tt.code =  td.ttcode_source ";
        $query .= "where td.trafficallowanceid = $_taid ";
       
        $rows = $this->selectRows($query);
        $aRecord = array();

        $total = count($rows);
        if ($total == 0) {
            return false;
        }
         
        foreach ($rows as $row) {  
            $e = new TrafficAllowanceDetail();   
            $e->trafficallowanceid = intval($row["trafficallowanceid"]);
            $e->planidcrypt = trim($row["planidcrypt"]);
            $e->ttcode_source = $row["ttcode_source"];
            $e->tunitcode = $row["tunitcode"];
            $e->description = trim($row["description"]);
            array_push($aRecord, $e);
        }
       
        return $aRecord;
    }

    public function getTrafficIncrementByPlanId($_planid) {

        $query = "select ti.traffictypecode, ti.min_unitcode, ti.min_unitqty, ";
        $query .= "ti.inc_unitcode, ti.inc_unitqty, ts.description from  ";
        $query .= "ait_trafficincrement ti ";
        $query .= "left join ait_traffictype ts on ts.code = ti.traffictypecode ";
        $query .= "where ti.deleted = 0 and ti.planid = $_planid ";
       
        $rows = $this->selectRows($query);
        $aRecord = array();

        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        foreach ($rows as $row) {

            $e = new TrafficIncrement();
            $e->traffictypecode = $row["traffictypecode"];
            $e->min_unitcode = $row["min_unitcode"];
            $e->min_unitqty = $row["min_unitqty"];

            $e->inc_unitcode = $row["inc_unitcode"];
            $e->inc_unitqty = $row["inc_unitqty"];

            $e->description = trim($row["description"]);
            array_push($aRecord, $e);
        }

        return $aRecord;
    }

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
 

    public function generateSkuByParentPlan($_sku) {

        $_total = $this->getTotalBySku($_sku);
        if ($_total) {
            $_sku = $_sku . '' . $_total;
        }

        return $_sku;
    }

    public function getTotalBySku($_sku) {
        $query = "select count(*) as total from plan where sku = '$_sku' ";
        $_total = intval($this->selectOneColumn("total", $query));
        return $_total;
    }

    public function setUpList($_accountid, $_parentaccountid) {
 
        $query  = " select p.id, p.idcrypt, p.accountid, p.customeraccountid,  son.idcrypt as sonplan ";
        $query .= " from plan p  ";
        $query .= " left join plan son on son.parentplan = p.idcrypt and son.accountid = $_accountid  ";
        $query .= " where p.aviable = 1 and p.accountid = $_parentaccountid and (p.customeraccountid = 0 or  p.customeraccountid = $_accountid)  ";
        
         
        $_rows = $this->selectRows($query);

        if ($_rows) {
            foreach ($_rows as $_row) {
                if ($_row["sonplan"] == NULL) {
                    //we need to setup this plan
                    $this->createSonPlan($_accountid, $_parentaccountid, $_row["idcrypt"]);
                }
            }
        }
    }
    
    
    public function getBySkuAndVersionIsUnique($_idcrypt, $_accountid,  $_sku, $_version){
        
        $query = "select count(*) as total from plan where vers = '$_version' and accountid = $_accountid   and sku = '$_sku' and (idcrypt != '$_idcrypt')  ";
        $_total =  intval($this->selectOneColumn("total", $query));
        
        if($_total > 0){
            return false;
        }else{
            return true;
        }
        
    }
    
    
    public function createSonPlan($_accountid, $_parentaccountid, $_parentplan) {

        $_e = $this->getByIdCrypt($_parentplan);

        $_e->price = doubleval($_e->price) * 1.2;
        $_e->subscriptionprice = doubleval($_e->subscriptionprice) * 1.2;
        $_e->accountid = $_accountid;
        $_e->parentplan = $_parentplan;
        //$_e->sku = $this->generateSkuByParentPlan($_e->sku);
        
        //$_e->version = $_accountid.'.'.$_e->version;
        
        //$_e->version = $_accountid.'.'.$_e->version;
        
        
        $_e->aviable = 0;
      
        
        $_idcrypt = $this->register($_e);
        $_newplan = $this->getByIdCrypt($_idcrypt);


        $dto_trafficsetup = new Dto_TrafficSetup();
        $aTrafficSetup = $dto_trafficsetup->getByPlanId($_e->id);

        $aRecord = array();
        if ($aTrafficSetup) {
            $total = count($aTrafficSetup);
            if ($total > 0) {
                foreach ($aTrafficSetup as $record) {
                    $record->planid = $_newplan->id;
                    $record->planidcrypt = $_newplan->idcrypt;
                    $record->chargeammount = $record->chargeammount * 1.2;
                    array_push($aRecord, $record);
                }
                $dto_trafficsetup->registerBatch($aRecord);
            }
        }



        $dto_trafficAllowance = new Dto_TrafficAllowance();
        $aTrafficAllowance = $dto_trafficAllowance->getByPlanId($_e->id);

        $aRecordAllowance = array();
        if ($aTrafficAllowance) {
            $total = count($aTrafficAllowance);
            if ($total > 0) {
                foreach ($aTrafficAllowance as $record) {

                    //$record->idcrypt = $_newplan->idcrypt;
                    $record->planid = $_newplan->id;

                    $aTrafficAllowanceDetail = $record->aTrafficSetup;
                    if ($aTrafficAllowanceDetail) {
                        $totalsetup = count($aTrafficAllowanceDetail);
                        if ($totalsetup > 0) {
                            $aRecordSetup = array();
                            foreach ($aTrafficAllowanceDetail as $detail) {
                                $detail->planidcrypt = $_newplan->idcrypt;
                                array_push($aRecordSetup, $detail);
                            }

                            // echo var_dump($aRecordSetup); die();

                            $record->aTrafficSetupId = $aRecordSetup;
                        }//if
                    }//if
                    array_push($aRecordAllowance, $record);
                }//if
            }//if
        }//if
        $dto_trafficAllowance->registerBatch($aRecordAllowance, 1);


        $dto_trafficIncrement = new Dto_TrafficIncrement();
        $aTrafficIncrement = $dto_trafficIncrement->getByPlanId($_e->id);
        $aRecordIncrement = array();
        if ($aTrafficIncrement) {
            $total = count($aTrafficIncrement);
            if ($total > 0) {
                foreach ($aTrafficIncrement as $record) {
                    $record->planid = $_newplan->id;
                    $record->planidcrypt = $_newplan->idcrypt;
                    array_push($aRecordIncrement, $record);
                }
                $dto_trafficIncrement->registerBatch($aRecordIncrement);
            }
        }


        //   }

        return $_newplan->idcrypt;
    }

    public function getIdByIdCrypt($_idcrypt) {
        $query = "select id from plan where idcrypt = '$_idcrypt' ";
        return $this->selectOneColumn("id", $query);
    }

    public function getProductAssoc($_planid) {

        $query = "select pd.name , p.idcrypt, p.sku ";
        $query .= "from productdesc pd  ";
        $query .= "right join product p on p.id = pd.product_id ";
        $query .= "right join productplan pp on pp.product_id = p.id and pp.plan_id = $_planid ";
        $query .= "where pp.plan_id = $_planid and pp.deleted = 0 and pd.lang_code = 'ENG' ";
         
        
        $rows = $this->selectRows($query);

        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($rows as $row) {
            $e = new Product();
            $e->idcrypt = $row["idcrypt"];
            $e->name = $row["name"];
            $e->sku = $row["sku"];
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function assocProduct($_planid, $_productid, $_operation, $_userid) {

        $querySelect = "select product_id from productplan where product_id=  $_productid  and plan_id =  $_planid   and operation= '$_operation' and deleted = 0  ";
        $product_id = intval($this->selectOneColumn("product_id", $querySelect));
        if ($product_id > 0) {
            return true;
        }


        $query = "insert into productplan(product_id, plan_id, currency_id, operation, status, datecreated, createdby, deleted) ";
        $query .= " values( $_productid, $_planid, 1, '$_operation', 1, CURRENT_TIMESTAMP(), 1, 0  ) ";

        return $this->executeUpdate($query);
    }

    public function deleteassocProduct($_planid, $_productid, $_userid) {

        $query = " update productplan set deleted = 1 , datedeleted = CURRENT_TIMESTAMP() where product_id = $_productid and plan_id = $_planid ";
        return $this->executeUpdate($query);
    }

    public function getByIdCrypt($_idcrypt, $_langcode = "ENG") {
        $query = "select p.* , a.name as customername ,  pd.name, pd.intro, pd.description from plan p ";
        $query .= "right join plandesc pd on pd.plan_id = p.id  and lang_code = '$_langcode'  ";
        $query .= "left join ait_account  a on a.id = p.customeraccountid  ";
        $query .= "where p.idcrypt = '$_idcrypt' ";
         
        $row = $this->selectOneRow($query);
        if (!$row) {
            return false;
        }
        return $this->getRow($row);
    }

    public function getBySku($_sku, $_langcode = "ENG") {
        $query = "select p.* , pd.name, pd.intro, pd.description from plan p ";
        $query .= "right join plandesc pd on pd.plan_id = p.id and lang_code = '$_langcode' ";
        $query .= "where p.sku = '$_sku' ";
        
        
        $row = $this->selectOneRow($query);
        if (!$row) {
            return false;
        }
        return $this->getRow($row);
    }

    public function getListOnlyPostPaid($_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10, $_langcode = "ENG", $_provideraccountid = 0 , $_accountid = 10001) {

        $query = "select p.* , pd.name, pd.intro, pd.description from plan p ";
        $query .= "right join plandesc pd on pd.plan_id = p.id  and lang_code = '$_langcode'  ";
        $query .= "where p.deleted = 0 and p.servicetype_code = 'POSTPAID'  ";

        if ($_search != "") {
            $query .= " and (";
            $query .= " p.sku like '%$_search%' OR  ";
            $query .= " p.network_code like '%$_search%' OR  ";
            $query .= " pd.name  like '%$_search%'  ";
            $query .= "  )  ";
        }

        if ($_provideraccountid > 0) {
            $query .= " AND p.accountid = $_provideraccountid ";
        } else {
            $query .= " AND p.accountid = 10001 ";
        }
        $query .=" AND   ( p.customeraccountid = $_accountid or p.customeraccountid = 0  ) AND p.aviable = 1 ";
        $query .= "ORDER BY $_sortCol $_sortDir";
        $query .= " LIMIT $_limit1 , $_limit2";

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            array_push($aRecords, $this->getRow($row));
        }
        return $aRecords;
    }    
    
    public function updateCustomerAccountid($_idcrypt, $_customeraccountid){
        $query = " update plan set customeraccountid = $_customeraccountid where idcrypt = '$_idcrypt' ";
        return $this->executeUpdate($query);
    }
    
    
    public function getList($_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10, $_langcode = "ENG", $_accountid = 0) {

        $query  =" select ";
        
        //-----TOTAL SIMCARDS ASSOC ---------------
        $query .=" ( ";
        $query .=" select DISTINCT count(ao.iserial) from airtimeoperation ao ";
        $query .=" where ";
        if ($_accountid > 0) {
            $query .=" ao.planidcryptforcustomer = p.idcrypt ";
        }else{
            $query .=" ao.planid = p.idcrypt ";
        }
        $query .=" ) as totalsimcards, ";       
        //-----END TOTAL SIMCARDS ASSOC ---------------
        
        
        $query .="p.* , pd.name, a.name as customername,  pd.intro, pd.description ";
        $query .=" from plan p ";
        $query .=" right join plandesc pd on pd.plan_id = p.id  and lang_code = '$_langcode'  ";
        $query .=" left  join ait_account a on a.id = p.customeraccountid  ";
        

        if ($_accountid > 0) {
            $query .= " AND p.accountid = $_accountid ";
        } else {
            $query .= " AND p.accountid = 10001 ";
        }
        
        
        $query .=" where p.deleted = 0 ";

        if ($_search != "") {
            $query .= " and (";
            $query .= " p.sku like '%$_search%' OR  ";
            $query .= " p.network_code like '%$_search%' OR  ";
            $query .= " pd.name  like '%$_search%' OR  ";
            $query .= " a.name  like '%$_search%'  ";
            $query .= "  )  ";
        }

        if ($_accountid > 0) {
            $query .= " AND p.accountid = $_accountid ";
        } else {
            $query .= " AND p.accountid = 10001 ";
        }
        
       
        $query .= "ORDER BY $_sortCol $_sortDir";
        $query .= " LIMIT $_limit1 , $_limit2";
        
    
        
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            array_push($aRecords, $this->getRow($row));
        }
        return $aRecords;
    }

    public function getListTotal($_search, $_langcode = "ENG", $_accountid = 0) {
        $query = "select count(p.id) as total  from plan p ";
        $query .= "right join plandesc pd on pd.plan_id = p.id  and lang_code = '$_langcode'  ";
        $query .= "where p.deleted = 0  ";
        if ($_search != "") {
            $query .= " and (";
            $query .= " p.sku like '%$_search%' OR  ";
            $query .= " p.network_code like '%$_search%' OR  ";
            $query .= " pd.name  like '%$_search%' OR  ";
            $query .= " a.name  like '%$_search%'  ";
            $query .= "  )  ";
        }

        if ($_accountid > 0) {
            $query .= " AND p.accountid = $_accountid ";
        } else {
            $query .= " AND p.accountid = 10001 ";
        }
        
        
        return intval($this->selectOneColumn("total", $query));
    }

    public function getRow($_row) {
        
        //echo var_dump($_row); die();
        
        
        $e = new AirtimePlan();
        $e->id = $_row["id"];
        $e->idcrypt = $_row["idcrypt"];
        $e->sku = $_row["sku"];
        $e->networkcode = $_row["network_code"];
        $e->paymentperiodcode = $_row["payment_period_code"];
        $e->contractperiodcode = $_row["contract_period_code"];
        $e->servicetypecode = $_row["servicetype_code"];
        $e->accountid = intval($_row["accountid"]);
        $e->customeraccountid = intval($_row["customeraccountid"]);
        $e->customername = trim($_row["customername"]);
        $e->parentplan = $_row["parentplan"];

        $e->subscriptionprice = doubleval($_row["price"]);
        $e->fixedpayment = doubleval($_row["fixedpayment"]);
        $e->activationpayment = doubleval($_row["activationpayment"]);
        $e->suspensionpayment = doubleval($_row["suspensionpayment"]);
        $e->deactivationpayment = doubleval($_row["deactivationpayment"]);
        $e->extensionpayment = doubleval($_row["extensionpayment"]);

        $e->name = trim($_row["name"]);
        $e->intro = trim($_row["intro"]);
        $e->description = trim($_row["description"]);

        $e->isgroup = intval($_row["isgroup"]);
        $e->grouptype = intval($_row["grouptype"]);
        
        $e->ttgroup = trim($_row["ttgroup"]);
        
        $e->version = trim($_row["vers"]);
        $e->aviable = intval($_row["aviable"]);

        $e->totalsimcards  = intval($_row["totalsimcards"]);
        
        
        $e->modifiedby = $_row["modifiedby"];
        $e->createdby = $_row["createdby"];
        $e->datecreated = $_row["datecreated"];
        $e->datemodified = $_row["datemodified"];
        return $e;
    }

    public function deleted($_e) {
        $_idcrypt = $_e->idcrypt;
        $_deletedby = $_e->deletedby;
        $query = "update plan set deleted = 1 , deletedby = $_deletedby , datedeleted = CURRENT_TIMESTAMP()  where idcrypt = '$_idcrypt' ";
        return $this->executeUpdate($query);
    }

    public function updateDesc($_e) {
        $_plan_id = $_e->id;
        $_lang_code = $_e->langcode;
        $_name = addslashes($_e->name);
        $_intro = addslashes($_e->intro);
        $_description = addslashes($_e->description);

        $query = "update plandesc set name= '$_name' , intro = '$_intro' , description = '$_description' where plan_id = $_plan_id and lang_code = '$_lang_code' ";
        return $this->executeUpdate($query);
    }

    public function update($_e) {
        
        $_idcrypt = $_e->idcrypt;
        $_sku = $_e->sku;
        $_network_code = $_e->networkcode;
        $_ttgroup = $_e->ttgroup;
        
        $_version  = trim($_e->version);
        $_aviable = intval($_e->aviable);
        
        $_payment_period_code = $_e->paymentperiodcode;
        $_contract_period_code = $_e->contractperiodcode;
        $_servicetypecode = $_e->servicetypecode;
        $_price = doubleval($_e->subscriptionprice);
        $_fixedpayment = doubleval($_e->fixedpayment);
        $_activationpayment = doubleval($_e->activationpayment);
        $_suspensionpayment = doubleval($_e->suspensionpayment);
        $_deactivationpayment = doubleval($_e->deactivationpayment);
        $_extensionpayment = doubleval($_e->extensionpayment);
        
        $_modifiedby = $_e->modifiedby;
        $_isgroup = intval($_e->isgroup);
        $_grouptype = intval($_e->grouptype);

        $query = "update plan set ";
        $query .= "  isgroup = $_isgroup,  grouptype =  $_grouptype ,    sku = '$_sku' , network_code = '$_network_code' , payment_period_code = '$_payment_period_code', ";
        $query .= "contract_period_code = '$_contract_period_code' , servicetype_code = '$_servicetypecode' ,  ";
        $query .= "price = $_price, fixedpayment = $_fixedpayment , activationpayment = $_activationpayment ,    ";
        $query .= " extensionpayment = $_extensionpayment ,   ";
        $query .= "deactivationpayment = $_deactivationpayment, suspensionpayment = $_suspensionpayment , ttgroup = '$_ttgroup'  ,  ";
        
        $query .= " vers = '$_version' , aviable = $_aviable ,    ";
        
        $query .= "modifiedby = '$_deactivationpayment', datemodified = CURRENT_TIMESTAMP() ";
        $query .= "where idcrypt = '$_idcrypt' ";
        
        
        //echo $query; die();

        return $this->executeUpdate($query);
    }

    public function getIdBySku($_sku) {
        $query = "select id from plan where sku = '$_sku' and deleted = 0";
        return intval($this->selectOneColumn("id", $query));
    }

    public function getIdCryptBySku($_sku) {
        $query = "select idcrypt from plan where sku = '$_sku' and deleted = 0";
        return trim($this->selectOneColumn("idcrypt", $query));
    }

    public function register($_e) {
        $_idcrypt = md5(date("YmdHis") . "" . rand(100000, 999999));
        $_accountid = intval($_e->accountid);
        $_customeraccountid = intval($_e->customeraccountid);
        $_parentplan = trim($_e->parentplan);
        $_sku = $_e->sku;
        
        $_aviable = intval($_e->aviable);
        $_version = trim($_e->version);
        $_ttgroup = $_e->ttgroup;
        
        $_network_code = $_e->networkcode;
        $_payment_period_code = $_e->paymentperiodcode;
        $_contract_period_code = $_e->contractperiodcode;
        $_servicetypecode = $_e->servicetypecode;
        $_price = doubleval($_e->subscriptionprice);
        $_fixedpayment = doubleval($_e->fixedpayment);
        $_activationpayment = doubleval($_e->activationpayment);
        $_suspensionpayment = doubleval($_e->suspensionpayment);
        $_deactivationpayment = doubleval($_e->deactivationpayment);
        $_extensionpayment = doubleval($_e->extensionpayment);
        
        $_currency_code = 'USD';
        $_createdby = $_e->createdby;
        
        if($_accountid == $_customeraccountid){
            $_customeraccountid = 0;
        }
        
        
        $_name = addslashes($_e->name);
        $_intro = addslashes($_e->intro);
        $_description = addslashes($_e->description);

        $ok = false;
        $id = 0;
        $query = "insert into plan (idcrypt, sku,  accountid, customeraccountid,  parentplan,  network_code, payment_period_code, contract_period_code, servicetype_code, price, fixedpayment, ";
        $query .= " activationpayment, currency_code, datecreated, status, createdby, suspensionpayment, deactivationpayment , aviable, vers , ";
        $query .=" extensionpayment,  ttgroup ) values ( ";
        $query .= " '$_idcrypt', '$_sku' , $_accountid, $_customeraccountid ,  '$_parentplan' , '$_network_code' , '$_payment_period_code' , '$_contract_period_code' , '$_servicetypecode' , $_price, $_fixedpayment ,   ";
        $query .= " $_activationpayment, '$_currency_code' , CURRENT_TIMESTAMP(), 1, '$_createdby' , $_suspensionpayment , $_deactivationpayment , $_aviable, '$_version' , ";
        $query .=  "  $_extensionpayment , '$_ttgroup' ) ";
            
        //echo $query; die();
        
        $_result = $this->executeUpdate($query);
        if ($_result) {
            $id = intval($this->selectOneColumn("id", "select id from plan where idcrypt = '$_idcrypt' "));
        }
        if ($id > 0) { 
            $query = "insert into plandesc(plan_id, lang_code, name, intro, description ) values ";
            $query .= " ( $id, 'ENG', '$_name' , '$_intro', '$_description'  )  ,  ";
            $query .= " ( $id, 'ESP', '$_name' , '$_intro', '$_description'  )    ";
            $_resultlang = $this->executeUpdate($query);
            if ($_resultlang && $_result) {
                $ok = true;
            }
        }
        
        if (!$ok) {
            $_idcrypt = "";
            $this->executeUpdate("delete from plan where idcrypt = '$_idcrypt' ");
        }
        
        return $_idcrypt;
    }

}
