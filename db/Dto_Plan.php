<?php

class Dto_Plan extends Dto_Db {

    
    public function getIdByIdCrypt($_idcrypt){
        $query = "select id from plan where idcrypt = '$_idcrypt' ";
        $this->selectOneColumn("id", $query);
    }
     
    public function getPlansByProduct($_productid, $_langcode, $_accountid=10001, $_provideraccountid = 10001) {
        
        $query = "SELECT pl.idcrypt, pdesc.name , pl.price, pl.payment_period_code, pl.contract_period_code  FROM  productplan pp  ";
        $query .="RIGHT JOIN product p on p.id = pp.product_id  ";
        $query .="RIGHT JOIN plan pl on pl.id = pp.plan_id ";
        $query .="RIGHT JOIN plandesc pdesc on pdesc.plan_id = pp.plan_id and pdesc.lang_code = '" . $_langcode . "' ";
        $query .="WHERE p.deleted = 0 and pl.deleted  = 0 and p.idcrypt = '$_productid' and pp.deleted = 0 ";
        //$query .=" and ( pl.customeraccountid = 0 or pl.customeraccountid = $_accountid ) ";
        $query .=" and ( pl.customeraccountid = 0 or pl.customeraccountid = $_accountid  ) and pl.accountid = $_provideraccountid ";
        
        
        /*
        $query = "SELECT p.idcrypt, pd.name, p.price, p.payment_period_code, p.contract_period_code ";
        $query .="FROM plan p  ";
        $query .="RIGHT JOIN plandesc pd ON pd.plan_id = p.id   ";
        $query .="RIGHT JOIN plan pf on pf.idcrypt = p.parentplan   ";
        $query .="AND pd.lang_code =  '$_langcode'   ";
        $query .="WHERE p.accountid =$_accountid ";
            
	    $query .="AND pf.id  ";
            $query .="IN (  ";
            $query .="SELECT pp.plan_id  ";
            $query .="FROM productplan pp  ";
            $query .="RIGHT JOIN product pr ON pr.id = pp.product_id  ";
            $query .="WHERE  ";
          	  $query .="pr.idcrypt =  '$_productid'  ";
            $query .="AND pp.deleted =0  ";
            $query .=")  ";
        */
        $rows = $this->selectRows($query);

        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($rows as $row) {
            $entity = new Plan();
            $entity->idcrypt = $row["idcrypt"];
            $entity->name = $row["name"];
            $entity->price = $row["price"];
            $entity->payment_period_code = $row["payment_period_code"];
            $entity->contract_period_code = $row["contract_period_code"];
            array_push($aRecord, $entity);
        }
        return $aRecord;
    }
    
    public function getPlanForCustomer($_accountid, $_parentplan, $_langcode ){
        
        $query = "SELECT p.idcrypt, pd.name, p.price, p.payment_period_code, p.contract_period_code ";
        $query .= "FROM plan p  ";
        $query .= "RIGHT JOIN plandesc pd ON pd.plan_id = p.id  ";
        $query .= "RIGHT JOIN plan pf on pf.idcrypt = p.parentplan  ";
        $query .= "AND pd.lang_code =  '$_langcode' ";
        $query .= "WHERE p.accountid =$_accountid and p.parentplan = '$_parentplan' and p.deleted = 0 and pf.deleted = 0 ";
        
        $rows = $this->selectRows($query);

        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($rows as $row) {
            $entity = new Plan();
            $entity->idcrypt = $row["idcrypt"];
            $entity->name = $row["name"];
            $entity->networkcode = $row["network_code"];
            $entity->price = $row["price"];
            $entity->servicetype_code = $row["servicetype_code"];

            $entity->activationpayment = doubleval($row["activationpayment"]);
            $entity->suspensionpayment = doubleval($row["suspensionpayment"]);
            $entity->deactivationpayment = doubleval($row["deactivationpayment"]);

            $entity->fixedpayment = $row["fixedpayment"];
            $entity->payment_period_code = $row["payment_period_code"];
            $entity->contract_period_code = $row["contract_period_code"];

            $entity->intro = $row["intro"];
            $entity->description = $row["description"];

            $entity->aTrafficType = $this->getTrafficTypeByPlanId($entity->idcrypt);
            array_push($aRecord, $entity);
        }
        return $aRecord;
         
    }
    
    
    public function getPlanById($_planid, $_langcode) {

        $query = "SELECT pl.idcrypt, pdesc.name , pl.price,  ";
        $query .="pl.payment_period_code, pl.contract_period_code , pl.currency_code, pl.fixedpayment,  ";
        $query .="pl.activationpayment, pl.network_code, pl.servicetype_code, pdesc.intro, pdesc.description ";
        $query .="FROM plan pl  ";
        $query .="RIGHT JOIN plandesc pdesc on pdesc.plan_id = pl.id and pdesc.lang_code = '$_langcode' ";
        $query .="WHERE   pl.deleted  = 0 and pl.idcrypt = '$_planid'   ";


        $row = $this->selectOneRow($query);

        if (!$row) {
            return false;
        }
        $entity = new Plan();
        $entity->idcrypt = $row["idcrypt"];
        $entity->name = $row["name"];
        $entity->networkcode = $row["network_code"];
        $entity->price = $row["price"];
        $entity->servicetype_code = $row["servicetype_code"];
        
        $entity->activationpayment = doubleval($row["activationpayment"]);
        $entity->suspensionpayment = doubleval($row["suspensionpayment"]);
        $entity->deactivationpayment = doubleval($row["deactivationpayment"]);
        
        $entity->fixedpayment = $row["fixedpayment"];
        $entity->payment_period_code = $row["payment_period_code"];
        $entity->contract_period_code = $row["contract_period_code"];
        
        $entity->intro = $row["intro"];
        $entity->description = $row["description"];

        $entity->aTrafficType = $this->getTrafficTypeByPlanId($entity->idcrypt);
        return $entity;
    }

    public function getTrafficTypeByPlanId($_planid) {

        $query = "SELECT pl.idcrypt as planid, tt.traffictype_code, tt.qtyxperiod, tt.aditionalprice, tt.roundedcode, tt.roundedqty ";
        $query .="FROM traffictypeonplan tt  ";
        $query .="RIGHT JOIN plan pl on pl.id = tt.plan_id  ";
        $query .="WHERE pl.idcrypt = '$_planid' ";

        $rows = $this->selectRows($query);

        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($rows as $row) {
            $entity = new PlanTrafficType();  
            $entity->planid = $row["idcrypt"];
            $entity->traffictype_code = $row["traffictype_code"];
            $entity->qtyxperiod = $row["qtyxperiod"];
            $entity->additionalprice = $row["aditionalprice"];
            $entity->roundedcode = $row["roundedcode"];
            $entity->roundedqty = $row["roundedqty"];
            array_push($aRecord, $entity);
        }
        
        
      
        
        return $aRecord;
    }

}
