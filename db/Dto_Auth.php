<?php
class Dto_Auth extends Dto_Db{
    
    public function authForgot($_username){
        $query = "select id, profileid, accountid, status, username, firstname, lastname from ait_usersystem where username = '$_username' and deleted = 0  ";
    
        $user = new User();
        $row = $this->selectOneRow($query);
        if($row){
            $user->id = trim($row["id"]);
            $user->profileid = trim($row ["profile"]);
            $user->accountid = trim($row ["accountid"]);
            $user->status = intval($row ["status"]);
            $user->deleted = intval($row ["deleted"]);
          
            $user->username = trim($row["username"]);
            $user->firstname = trim($row["firstname"]);
            $user->lastname = trim($row["lastname"]);
        }
        
        $token = "";
        if($user->id != ""){
            $token = md5(date('ymdhis').'-'.$user->username.'-'.  rand(100000, 999999));
            $query  = "insert into ait_forgot (  token, userid,  username, datecreated, dateactivated, iprequested, status )";
            $query .=" values( '$token' ,  '".$user->id."'   ,  '".$user->username."' ,  CURRENT_TIMESTAMP()  , NULL , '".  get_ip()."' , 1  )  ";
            
            $this->executeUpdate($query);
            
        }
        
        return $token;        
    }
    
    public function changePasswordByForgot($_token, $_password){
        $query = "select userid,  username from ait_forgot where token = '$_token' ";
        $row = $this->selectOneRow($query);
        $username = "";
        $userid = "";
        if($row){
            $username = trim($row["username"]);
            $userid = trim($row["userid"]);
        }
        
        $ok = false;
        if($username != ""){
            //we proceed to update the password
            $query = "update ait_usersystem set pass = md5('$_password') where username = '$username'  ";
            $ok = $this->executeUpdate($query);
            $this->updateAudit($userid, "auth", "changepasswordbyforgot", "password changed");
        }
        return $ok;
    }
    
    public function validateForgot($_token){
        $query = "select id from ait_forgot where token = '$_token' and status = 1 ";
        $forgotid = intval($this->selectOneColumn("id", $query));
        
         if($forgotid > 0){
             $query = "update forgot set status = 2 , dateactivated = CURRENT_TIMESTAMP() where id = $forgotid";
             //echo $query; die();
             $this->executeUpdate($query);
         }
         return $forgotid;
    }
    
    
    
    public function getById($id){
        $user = new User();
        $query = "select * from ait_usersystem where id = '$id' ";
        $row = $this->selectOneRow($query);
        if($row){
            $user->id = trim($row["id"]);
            $user->profileid = trim($row ["profile"]);
            $user->accountid = trim($row ["accountid"]);
            $user->status = intval($row ["status"]);
            $user->deleted = intval($row ["deleted"]);
          
            $user->username = trim($row["username"]);
            $user->firstname = trim($row["firstname"]);
            $user->lastname = trim($row["lastname"]);
            $user->phone = trim($row["phone"]);
            $user->phoneext = trim($row["phoneext"]);
            $user->mobile = trim($row["mobile"]);
            
        }
        return $user;
    }
    
    public function authIndex($_username, $_password){
        
        $user = new User();
        $user->id = '';
        
        $query = "select id, status from ait_usersystem where username = '$_username' and pass = md5('$_password')  limit 1  ";
         
        $row = $this->selectOneRow($query);
        
        if($row){
            if(trim($row ["id"]) != ""){
                $user->id = trim($row ["id"]);
                $user->status = intval($row ["status"]);
            }
        }
      
        return $user;
        
    }
 
    
}
