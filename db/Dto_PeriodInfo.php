<?php

class Dto_PeriodInfo extends Dto_Db {

   public function getAll(){
       $query = "select * from periodinfo";
       $rows = $this->selectRows($query);
       if(!$rows){return false;}
       $total = count($rows);
       if($total == 0){return false;}
       $aRecord = array();
       foreach($rows as $row){
           $e = new PeriodInfo();
           $e->code = trim($row["code"]);
           $e->timecode = trim($row["timecode"]);
           $e->qty = trim($row["qty"]);
           array_push($aRecord, $e);
       }
       return $aRecord;
   }

}
