<?php
class Dto_TrafficUnitEq extends Dto_Db{

   public function getByCodeA($_codea){
       $query = "select te.* , ub.id as codebid from ait_trafficuniteq te ";
       $query .= "right join ait_trafficunit ub on ub.code = te.codeb ";
       $query .="where te.deleted = 0 and te.codea = '$_codea' ";
       $rows = $this->selectRows($query);
       if(!$rows){return false;}
       if(count($rows) == 0){ return false; }
       $aRecord = array();
       foreach($rows as $row){
           array_push($aRecord, $this->getRow($row));
       }
       return $aRecord;
   }
   
   public function getRow($_row){
       $e = new TrafficUnitEq();
       $e->id = trim($_row["id"]);
       $e->codea = trim($_row["codea"]);
       $e->codeb = trim($_row["codeb"]);
       $e->codebid = trim($_row["codebid"]);
       $e->qtya = doubleval($_row["qtya"]);
       return $e;
   }
   
   public function register($_e){
       $_codea = $_e->codea;
       $_codeb = $_e->codeb;
       $_qtya = doubleval($_e->qtya);
       $_id = $_e->id;
       $queryDelete = "update ait_trafficuniteq set deleted = 1 where codea = '$_codea'  and codeb = '$_codeb' ";
       $this->executeUpdate($queryDelete);
       
       $query = "insert into ait_trafficuniteq (id, codea, codeb, qtya) values ( '$_id','$_codea','$_codeb', $_qtya ) ";
       return $this->executeUpdate($query);
   }
   
   public function registerBatch($_records, $_codea){
       $queryDelete = "update ait_trafficuniteq set deleted = 1 where codea = '$_codea' ";
       $this->executeUpdate($queryDelete);
       
       $total = count($_records);
       if($total == 0 ){return false;}
       
       $query = "insert into ait_trafficuniteq (id, codea, codeb, qtya) values ";
       for($i=0; $i<$total; $i++){
         $_e = $_records[$i];  
         $_codeb = $_e->codeb;
         $_qtya = doubleval($_e->qtya);
         $_id = $this->generateId();
         $query .=" (  '$_id','$_codea','$_codeb', $_qtya   )  ";
         if( ($i+1) < $total  ){ $query .=" , ";  }
       }
       
       return $this->executeUpdate($query);
   }
   
   public function generateId(){
       return date('YmdHis')."".rand(100000,999999)."".rand(100000,999999);
   }

}