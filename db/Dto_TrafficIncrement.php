<?php

class Dto_TrafficIncrement extends Dto_Db {

    public function registerBatch($_aRecord) {
        $query = "insert into ait_trafficincrement(planid, planidcrypt, traffictypecode, min_unitcode, ";
        $query .= " min_unitqty, inc_unitcode, inc_unitqty, dateregistered, createdby ) values  ";
        $total = count($_aRecord);
        if ($total == 0) {
            return false;
        }
        for ($i = 0; $i < $total; $i++) {
            $e = $_aRecord[$i];
            $query .= " ( " . $e->planid . " , '" . $e->planidcrypt . "' , '" . $e->traffictypecode . "' ,  '" . $e->min_unitcode . "' ,  ";
            $query .= "  " . $e->min_unitqty . " , '" . $e->inc_unitcode . "' , " . $e->inc_unitqty . " , CURRENT_TIMESTAMP(),  '" . $e->createdby . "' ) ";
            if (($i + 1) < $total) {
                $query .= " , ";
            }
        }
     
        return $this->executeUpdate($query);
    }

    public function deleteBatch($_planidcrypt) {
        $query = "update ait_trafficincrement set deleted = 1 where planidcrypt='$_planidcrypt'";
     
        return $this->executeUpdate($query);
    }

    public function getByPlanId($_planid) {
        $query = "select * from ait_trafficincrement where planid = $_planid and deleted = 0  order by id asc";
       
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
          
            array_push($aRecord, $this->getEntityByRow($row));
        }
        return $aRecord;
    }

    public function getEntityByRow($_row) {
        $e = new TrafficIncrement();
      
        $e->id = intval($_row["id"]);
        $e->planid = intval($_row["planid"]);
        $e->planidcrypt = trim($_row["planidcrypt"]);

        $e->traffictypecode = trim($_row["traffictypecode"]);
        $e->min_unitcode = trim($_row["min_unitcode"]);
        $e->min_unitqty = doubleval($_row["min_unitqty"]);
        $e->inc_unitcode = trim($_row["inc_unitcode"]);
        $e->inc_unitqty = doubleval($_row["inc_unitqty"]);

        $e->dateregistered = trim($_row["dateregistered"]);
        $e->createdby = trim($_row["createdby"]);
        
     
        
        return $e;
    }

}
