<?php

class Dto_TrafficAllowance extends Dto_Db {
    
    public function getTrafficAllowanceByInventoryItemId( $_inventoryitemid){
       
        $_query   = "select ta.idcrypt, ta.planid, ta.tname, ta.qty ,  ";
        $_query  .= "( select tunitcode from ait_trafficallowance_detail where trafficallowanceid = ta.id group by tunitcode ) as unitcode   ";
        $_query  .= "from ait_trafficallowance ta   ";
        $_query  .= "right join plan p on p.id = ta.planid   ";
        $_query  .= "right join airtimeoperation ao on ao.planidcryptforcustomer = p.idcrypt and ao.requeststatus = 'executed' and ao.operation = 'activate'  ";
        $_query  .= "  and ao.processeddatetime = (select max(processeddatetime)  from airtimeoperation   ";
        $_query  .= "  where inventoryitemid  = $_inventoryitemid and requeststatus = 'executed' and operation = 'activate' )  ";
        $_query  .= " where ao.inventoryitemid = $_inventoryitemid and ta.deleted = 0 and p.deleted = 0    ";
       
        //echo $_query; die();
        $rows = $this->selectRows($_query);
        $aRecord = array();
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        
        foreach ($rows as $row) {
            $e = new TrafficAllowance();
            $e->idcrypt = trim($row["idcrypt"]);
            $e->seedcode = trim($row["seedcode"]);
            $e->planid = trim($row["planid"]);
            $e->tname = trim($row["tname"]);
            $e->qty = trim($row["qty"]);
            $e->unitcode = trim($row["unitcode"]);
            array_push($aRecord, $e);
        }
        
      
        
        return $aRecord;
        
        
    }//function
    
    
    public function registerBatch($_aRecord, $_robot = 0) {

        $total = count($_aRecord);
        if ($total == 0) {
            return false;
        }

        foreach ($_aRecord as $e) {
            
            
            
            $idcrypt = md5(date("YmdHis") . "" . rand(100000, 999999));
            
            $_seedcode = trim($e->seedcode);
            
            if($_seedcode ==""){  $_seedcode = $idcrypt; }
            
            
            $planid = intval($e->planid);
            $tname = trim($e->tname);
            $qty = doubleval($e->qty);
            $createdby = trim($e->createdby);
            $query = "insert into ait_trafficallowance (idcrypt, seedcode, planid, tname,  qty, createdby, datecreated) values('$idcrypt', '$_seedcode'  , $planid , '$tname'  , $qty , '$createdby' , CURRENT_TIMESTAMP() )  ";
            
            //echo $query; die();
            
            $result = $this->executeUpdate($query);
            if ($result) {

                $querySelectId = "select id from ait_trafficallowance where idcrypt = '$idcrypt' ";
                $trafficallowanceid = intval($this->selectOneColumn("id", $querySelectId));

                //echo "Llegue aqui"; die();

                $this->registerDetail($trafficallowanceid, $e->aTrafficSetupId, $_robot);
            }//if
        }//foreach

        return $result;
    }

    public function registerDetail($_id, $_aIds, $_robot = 0) {
        $aRecord = null;
        if ($_robot == 1) {
            $aRecord = $_aIds;
        } else {
            $aRecord = $this->getTrafficSetupByIds($_aIds);
        }
        if (!$aRecord) {
            return false;
        }
        $total = count($aRecord);
        $query = "insert into ait_trafficallowance_detail (trafficallowanceid, planidcrypt, ttcode_source, ttcode_dest, tunitcode ) values  ";
        for ($i = 0; $i < $total; $i++) {
            $e = $aRecord[$i];
            $query .="($_id, '" . $e->planidcrypt . "' , '" . $e->ttcode_source . "'  , '" . $e->ttcode_dest . "'  , '" . $e->tunitcode . "' ) ";
            if (($i + 1) < $total) {
                $query .=" , ";
            }
        }
        return $this->executeUpdate($query);
    }
    
    public function registerBatchDetail( $aRecord ) {
        
      
        $total = count($aRecord);
        $query = "insert into ait_trafficallowance_detail (trafficallowanceid, planidcrypt, ttcode_source, ttcode_dest, tunitcode ) values  ";
        for ($i = 0; $i < $total; $i++) {
            $e = $aRecord[$i];
            $query .="($e->trafficallowanceid, '" . $e->planidcrypt . "' , '" . $e->ttcode_source . "'  , '" . $e->ttcode_dest . "'  , '" . $e->tunitcode . "' ) ";
            if (($i + 1) < $total) {
                $query .=" , ";
            }
        }
        return $this->executeUpdate($query);
    }    
    

    public function getTrafficSetupByIds($_aIds) {
        $query = "select * from ait_trafficsetup where deleted = 0 AND (  ";
        $total = count($_aIds);
        if ($total == 0) {
            return false;
        }
        for ($i = 0; $i < $total; $i++) {
            $query .=" id = " . $_aIds[$i] . "  ";
            if (($i + 1) < $total) {
                $query .=" OR ";
            }
        }
        $query .=" ) ";

        $rows = $this->selectRows($query);
        $aRecord = array();
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        foreach ($rows as $row) {
            $e = new TrafficSetup();
            $e->ttcode_source = trim($row["ttcode_source"]);
            $e->ttcode_dest = trim($row["ttcode_dest"]);
            $e->tunitcode = trim($row["tunitcode"]);
            $e->planidcrypt = trim($row["planidcrypt"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function deleteBatch($_planid) {
        $query = "update ait_trafficallowance set deleted = 1 where planid = $_planid ";
        return $this->executeUpdate($query);
    }

    public function getByPlanId($_planid) {
        $query = "select * from ait_trafficallowance where deleted = 0 and planid = $_planid  ";

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            array_push($aRecord, $this->getEntityByRow($row));
        }

        return $aRecord;
    }

    public function getTrafficSetupId($_trafficallowanceid) {

        $query = "select * from ait_trafficallowance_detail where trafficallowanceid = $_trafficallowanceid ";
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            $e = new TrafficSetup();
            $e->planidcrypt = trim($row["planidcrypt"]);
            $e->ttcode_source = trim($row["ttcode_source"]);
            $e->ttcode_dest = trim($row["ttcode_dest"]);
            $e->tunitcode = trim($row["tunitcode"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

    public function getEntityByRow($_row) {
        $e = new TrafficAllowance();
        $e->id = intval($_row["id"]);
        $e->planid = intval($_row["planid"]);
        $e->seedcode = trim($_row["seedcode"]);
        $e->tname = trim($_row["tname"]);
        $e->qty = intval($_row["qty"]);
        $e->createdby = $_row["createdby"];
        $e->aTrafficSetup = $this->getTrafficSetupId($e->id);

        return $e;
    }

}
