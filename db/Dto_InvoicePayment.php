<?php
class Dto_InvoicePayment extends Dto_Db {
    
    public function savePayment($_provideraccountid, $_customeraccountid,
            $_tableinvoice, $_invoiceid, $_paymentammount, $_paymentdate,  $_paymentref, $_user ){
     
        
        $query ="insert into invoicepayment ( tableinvoice, provideraccountid, customeraccountid, ";
        $query .=" tableinvoiceid, paymentammount, paymentdate,  paymentref, registereduser, registereddatetime )  ";
        $query .=" values  ";
        $query .=" ( '$_tableinvoice' , $_provideraccountid, $_customeraccountid, $_invoiceid,  ";
        $query .="  $_paymentammount, '$_paymentdate', '$_paymentref', '$_user', CURRENT_TIMESTAMP() ) ";
         
        return $this->executeUpdate($query);
        
    }
    
    
    public function getMyInvoices($_accountid){
        
        $query ="( ";
        $query .=" select 10001 as provideraccountid, i.accountid as customeraccountid,  ";
        $query .=" i.code as invoiceno,  CAST( i.invoicedate as DATE) as invoicedate , CAST( i.invoiceduedate as DATE)  as invoiceduedate , ";
        $query .=" i.x_ammount as invoiceammount, ip.paymentammount , ip.paymentref, ip.paymentdate , paymentfilename ";
        $query .=" from invoice i  ";
        $query .=" left join invoicepayment ip on ip.tableinvoiceid = i.id and ip.tableinvoice = 'invoice' ";
        $query .=" where i.accountid =$_accountid order by i.invoiceduedate desc ";
        $query .=" )  ";
        $query .=" UNION ";
        $query .=" ( ";
        $query .=" select i.accountid as provideraccountid, i.customerid as customeraccountid, ";
        $query .=" i.invoiceno, i.invoicedate, i.invoiceduedate , i.totalusd as invoiceammount , ip.paymentammount , ip.paymentref, ip.paymentdate , paymentfilename ";
        $query .=" from invoicetocustomer i  ";
        $query .=" left join invoicepayment ip on ip.tableinvoiceid = i.id and ip.tableinvoice = 'invoicetocustomer' ";
        $query .=" where i.customerid = $_accountid  and i.deleted = 0 order by i.invoiceduedate desc ";
        $query .=" ) ";        
         
        $_rows = $this->selectRows($query);
        if(count($_rows)==0){return false;}
        
        $aRecord = array();
        
        foreach($_rows as $_row){
            array_push($aRecord, $this->parseRowToEntity($_row));
        }
        
        return $aRecord;
    }
    
    
    public function getProviderInvoicesFromCustomersSummary($_accountid){
       
        $query  ="select a.id as customeraccountid, a.name as customername , a.creditlimit , ";
        $query .="(  select sum(x_ammount) from invoice where accountid = a.id and deleted = 0  ) as invoiceammount, ";
        $query .="(  select sum(paymentammount) from invoicepayment where customeraccountid = a.id  ) as paymentammount ";
        $query .="from ait_account a ";
        $query .="where a.parentaccountid = $_accountid ";
        $query .="and a.deleted = 0 ";
        
        
        
        $_rows = $this->selectRows($query);
        if(count($_rows)==0){return false;}
        
        $aRecord = array();
        
        foreach($_rows as $_row){
            array_push($aRecord, $this->parseRowToEntity($_row));
        }
        
        return $aRecord;   
        
    }    
    
    
    public function getInvoicesFromCustomersSummary($_accountid){
       
        $query  ="select a.id as customeraccountid, a.name as customername , a.creditlimit , ";
        $query .="(  select sum(totalusd) from invoicetocustomer where customerid = a.id and deleted = 0  ) as invoiceammount, ";
        $query .="(  select sum(paymentammount) from invoicepayment where customeraccountid = a.id  ) as paymentammount ";
        $query .="from ait_account a ";
        $query .="where a.parentaccountid = $_accountid ";
        $query .="and a.deleted = 0 ";
 

        
        $_rows = $this->selectRows($query);
        if(count($_rows)==0){return false;}
        
        $aRecord = array();
        
        foreach($_rows as $_row){
            array_push($aRecord, $this->parseRowToEntity($_row));
        }
        
        return $aRecord;   
        
    }
    
    
    public function getProviderInvoicesFromCustomers($_accountid, $_customerid){
       
        
        $query ="(  ";
        $query .="SELECT 'invoice' as type, i.id as invoiceid, i.idcrypt, i.code as invoiceno, i.invoicedate, i.invoiceduedate, i.x_ammount as invoiceammount ";
        $query .="from invoice i where i.provideraccountid = $_accountid and i.accountid = $_customerid   order by i.id desc , i.invoiceduedate desc ";
        $query .=" ) ";
        $query .=" UNION  ";
        $query .=" (  ";
        $query .="SELECT 'payment' as type, p.id as paymentid,  '' , '', p.paymentdate as invoicedate, '', p.paymentammount from ";
        $query .="invoicepayment p where p.provideraccountid = $_accountid and p.customeraccountid = $_customerid  ";
        $query .="and p.deleted = 0 order by p.id desc , p.paymentdate desc ";
        $query .=" ) ";
        $query .=" order by invoicedate  desc  ";
        
        //echo $query; die();
        
        $_rows = $this->selectRows($query);
        if(count($_rows)==0){return false;}
        
        $aRecord = array();
        
        foreach($_rows as $_row){
            array_push($aRecord, $this->parseRowToEntity($_row));
        }
        
        return $aRecord;        
        
    }    
    
    
    
    public function getInvoicesFromCustomers($_accountid, $_customerid){
       
        
        $query ="(  ";
        $query .="SELECT 'invoice' as type, i.id as invoiceid, i.idcrypt, i.invoiceno, i.invoicedate, i.invoiceduedate, i.totalusd as invoiceammount ";
        $query .="from invoicetocustomer i where i.accountid = $_accountid and i.customerid = $_customerid and i.deleted = 0 order by i.id desc , i.invoiceduedate desc ";
        $query .=" ) ";
        $query .=" UNION  ";
        $query .=" (  ";
        $query .="SELECT 'payment' as type, p.id as paymentid,  '' , '', p.paymentdate as invoicedate, '', p.paymentammount from ";
        $query .="invoicepayment p where p.provideraccountid = $_accountid and p.customeraccountid = $_customerid  ";
        $query .="and p.deleted = 0 order by p.id desc , p.paymentdate desc ";
        $query .=" ) ";
        $query .=" order by invoicedate  desc  ";
 
        
        $_rows = $this->selectRows($query);
        if(count($_rows)==0){return false;}
        
        $aRecord = array();
        
        foreach($_rows as $_row){
            array_push($aRecord, $this->parseRowToEntity($_row));
        }
        
        return $aRecord;        
        
    }
    
    public function parseRowToEntity($row){
        $e = new InvoicePayment();
        $e->type = $row["type"]; 
        $e->idcrypt = trim($row["idcrypt"]);
        $e->invoiceid = $row["invoiceid"];
        $e->provideraccountid = $row["provideraccountid"];
        $e->customeraccountid = $row["customeraccountid"];
        $e->invoiceno = $row["invoiceno"];
        $e->invoicedate = $row["invoicedate"];
        $e->invoiceduedate = $row["invoiceduedate"];
        $e->invoiceammount = $row["invoiceammount"];
        $e->paymentammount = $row["paymentammount"];
        $e->paymentref = $row["paymentref"];
        $e->paymentdate = $row["paymentdate"];
        $e->paymentfilename = $row["paymentfilename"];
        $e->customername = $row["customername"];
        $e->creditlimit = $row["creditlimit"];
        return $e;
    }
     
}