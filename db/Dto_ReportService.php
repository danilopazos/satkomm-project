<?php

class Dto_ReportService extends Dto_Db {

    public function getConsolidatedByPlan($aUsers, $aPeriod, $aNetwork, $aOperation, $aPlans) {

        $query = "SELECT ";
        $query .= "planid, planname, operation, count(*) as qty , YEAR(processeddatetime) as year ,  MONTH(processeddatetime) as  month ";
        $query .= "FROM `airtimeoperation`  ";
        $query .= "where requeststatus = 'executed' ";
        
        /*
        if(count($aNetwork)>0){
            $query .=" AND ( ";
            for($i=0; $i<count($aNetwork); $i++){
                $code = $aNetwork[$i];
                $query .=" networkcode = '".$code."'   ";
                if(($i+1)<count($aNetwork)){
                    $query .=" OR ";
                }
            }
            $query .=" ) ";
        }
        */
        
        $query .= "group by planname, operation , YEAR(processeddatetime), MONTH(processeddatetime) ";
        $query .= "order by YEAR(processeddatetime) desc, MONTH(processeddatetime) desc, count(*)  desc ";
        
        return $this->selectRows($query);
        
    }

    public function getConsolidatedByNetwork($aUsers, $aPeriod, $aNetwork, $aOperation, $aPlans) {

        $query = "SELECT ";
        $query .= "networkcode, operation, count(*) as qty , YEAR(processeddatetime) as year ,  MONTH(processeddatetime) as  month ";
        $query .= "FROM `airtimeoperation`  ";
        $query .= "where requeststatus = 'executed' ";
        $query .= "group by networkcode, operation , YEAR(processeddatetime), MONTH(processeddatetime) ";
        $query .= "order by YEAR(processeddatetime) desc, MONTH(processeddatetime) desc, count(*)  desc ";
        
        return $this->selectRows($query);
        
    }
    
    public function getConsolidatedByUser($aUsers, $aPeriod, $aNetwork, $aOperation, $aPlans) {

        $query = "SELECT CONCAT( u.firstname,  ' ',  u.lastname ) AS username, operation,  ";
        $query .= " COUNT( * ) AS qty, YEAR( processeddatetime )  AS YEAR, MONTH( processeddatetime ) AS MONTH   ";
        $query .= "FROM  `airtimeoperation`  ";
        $query .= "LEFT JOIN ait_usersystem u ON u.id = requestedbyuserid  ";
        $query .= "WHERE requeststatus =  'executed'  ";
        $query .= "GROUP BY u.username, operation, YEAR( processeddatetime ) , MONTH( processeddatetime )   ";
        $query .= "ORDER BY YEAR( processeddatetime ) DESC , MONTH( processeddatetime ) DESC , COUNT( * ) DESC   ";
        $query .= "LIMIT 0 , 30 ";
        
        //echo $query; die();
        
        return $this->selectRows($query);
        
    }
    
}
