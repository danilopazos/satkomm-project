<?php
class Dto_Quote extends Dto_Document {
    
    public function setQuoteApproved($_quoteidcrypt, $_status, $_invoiceidcrypt){
        $query = "update quote set status = '$_status' , invoiceidcrypt='$_invoiceidcrypt' where idcrypt = '$_quoteidcrypt' ";
        return $this->executeUpdate($query);
    }
    
    public function unsetQuoteApproved(  $_status,   $_invoiceidcrypt){
        $query = "update quote set status = '$_status' , invoiceidcrypt = '' where invoiceidcrypt = '$_invoiceidcrypt' ";
        return $this->executeUpdate($query);
    }
    
    public function getIdcryptByInvoiceId($_invoiceidcrypt){
        $query = "select idcrypt from quote where invoiceidcrypt = '$_invoiceidcrypt' ";
       
        return $this->selectOneColumn("idcrypt", $query);
    }
    
    public function getListBySubject($_accountid, $_search, $_orderby, $_orderdir, $limit1, $limit2){
        $query = " select q.idcrypt, q.quoteno, q.subject from quote q where q.accountid = $_accountid and q.deleted = 0 ";
        if ($_search != "") {
            $query .=" and (      q.subject  like '%$_search%' OR q.quoteno like '%$_search%'   )   ";
        }
        $query .=" order by $_orderby  $_orderdir  ";

        $query .="  limit $limit1, $limit2  ";
    
        $aQuote = array();
        $rows = $this->selectRows($query);
        if ($rows) {

            if (count($rows) > 0) {
                foreach ($rows as $row) {

                 
                    $entity = new Quote();

                    $entity->idcrypt = $row["idcrypt"];
                    $entity->quoteno = $row["quoteno"];
                    $entity->subject = $row["subject"];
                 
                    array_push($aQuote, $entity);
                }
            }
        }

        return $aQuote;
    }
    
    public function delete($_idcrypt, $_userid){
        $query = "update quote set deleted = 1 , deletedby = '$_userid' , deleteddate = CURRENT_TIMESTAMP() where idcrypt =  '$_idcrypt'  ";
        
        return $this->executeUpdate($query);
    }
    
    public function getList($_accountid, $_search, $_orderby, $_orderdir, $limit1, $limit2) {

        $query  =" select q.* , ";
        $query .=" ac.name as customer_name, concat(con.firstname,' ', con.lastname) as contact_names,   ";
        $query .=" concat(asi.firstname,' ', asi.lastname) as assigned_names  ";
        $query .=" from quote q ";
        $query .=" left join ait_account ac on ac.id = q.customerid ";
        $query .=" left join ait_usersystem con on con.id = q.contactid ";
        $query .=" left join ait_usersystem asi on asi.id = q.assignedto ";        
        $query .=" WHERE q.accountid = ".$_accountid." and q.deleted = 0  ";
        if($_search != ""){
            $query .="  AND  (  ";
            $query .="  q.subject like '%$_search' "; 
            $query .=" ) ";
        }
        
        $query .=" order by $_orderby $_orderdir limit $limit1 , $limit2  ";
      
        
        $_rows = $this->selectRows($query);
        $_total = count($_rows);
        if($_total == 0 ){return false;}
        
        $aRecord = array();
        foreach($_rows as $_row){
            $_e = $this->getByRow( $_row) ;
            array_push($aRecord, $_e );
        }
        return $aRecord;
    }
    
    public function getListTotal($_accountid, $_search ) {

        $query  =" select count(*) as total ";
        $query .=" from quote q ";
        $query .=" left join ait_account ac on ac.id = q.customerid ";
        $query .=" left join ait_usersystem con on con.id = q.contactid ";
        $query .=" left join ait_usersystem asi on asi.id = q.assignedto ";        
        $query .=" WHERE q.accountid = ".$_accountid."  and q.deleted = 0  ";
        
        if($_search != ""){
            $query .="  AND  (  ";
            $query .="  q.subject like '%$_search' "; 
            $query .=" ) ";
        }
        
       return intval($this->selectOneColumn("total", $query));
    }
    
    public function getByIdCrypt($_idcrypt){
        $query  =" select q.* , ";
        $query .=" ac.name as customer_name, concat(con.firstname,' ', con.lastname) as contact_names,   ";
        $query .=" concat(asi.firstname,' ', asi.lastname) as assigned_names  ";
        $query .=" from quote q ";
        $query .=" left join ait_account ac on ac.id = q.customerid ";
        $query .=" left join ait_usersystem con on con.id = q.contactid ";
        $query .=" left join ait_usersystem asi on asi.id = q.assignedto ";
        $query .=" where q.idcrypt = '$_idcrypt' ";
        //echo $query; die();
        $_row = $this->selectOneRow($query);
        return $this->getByRow($_row);
    }
    
    public function getByRow($_row){
        $_e = new Quote();
        $_e->idcrypt = trim($_row["idcrypt"]);
        $_e->quoteno = trim($_row["quoteno"]);
        $_e->accountid = intval($_row["accountid"]);
        $_e->subject = stripslashes($_row["subject"]);
        $_e->status = stripslashes($_row["status"]);
        $_e->quotedate = stripslashes($_row["quotedate"]);
        $_e->quoteuntil = stripslashes($_row["quoteuntil"]);
        $_e->assignedto = stripslashes($_row["assignedto"]);
        $_e->customerid = intval($_row["customerid"]);
        $_e->contactid = stripslashes($_row["contactid"]);
        $_e->contacttelephone = stripslashes($_row["contacttelephone"]);
        $_e->contactemail = stripslashes($_row["contactemail"]);
        $_e->contactmobile = stripslashes($_row["contactmobile"]);
        $_e->contactsalutation = stripslashes($_row["contactsalutation"]);
        $_e->paymentterms = stripslashes($_row["paymentterms"]);
        $_e->paymenttermsother = stripslashes($_row["paymenttermsother"]);
        
        $_e->deliveryterms = stripslashes($_row["deliveryterms"]);
        $_e->deliverytermsother = stripslashes($_row["deliverytermsother"]);
        
        $_e->carriercompany = stripslashes($_row["carriercompany"]);
        $_e->carriercompanyother = stripslashes($_row["carriercompanyother"]);
        $_e->operationtype = stripslashes($_row["operationtype"]);
        $_e->obs = stripslashes($_row["obs"]);
        $_e->shippingenabled = stripslashes($_row["shippingenabled"]);
        $_e->discount = doubleval($_row["discount"]);
        $_e->hastaxes = intval($_row["hastaxes"]);
        $_e->subtotal = doubleval($_row["subtotal"]);
        $_e->shippingcost = doubleval($_row["shippingcost"]);
        $_e->taxespercent = doubleval($_row["taxespercent"]);
        $_e->total = doubleval($_row["total"]);
        
        $_e->bill_country = stripslashes($_row["bill_country"]);
        $_e->bill_state   = stripslashes($_row["bill_state"]);
        $_e->bill_city    = stripslashes($_row["bill_city"]);
        $_e->bill_street  = stripslashes($_row["bill_street"]);
        $_e->bill_zipcode = stripslashes($_row["bill_zipcode"]);
        $_e->bill_pobox   = stripslashes($_row["bill_pobox"]);
        
        $_e->ship_country = stripslashes($_row["ship_country"]);
        $_e->ship_state   = stripslashes($_row["ship_state"]);
        $_e->ship_city    = stripslashes($_row["ship_city"]);
        $_e->ship_street  = stripslashes($_row["ship_street"]);
        $_e->ship_zipcode = stripslashes($_row["ship_zipcode"]);
        $_e->ship_pobox   = stripslashes($_row["ship_pobox"]);
        
        $_e->contact_names =stripslashes($_row["contact_names"]);
        $_e->assigned_names =stripslashes($_row["assigned_names"]);
        $_e->customer_name =stripslashes($_row["customer_name"]);
         
        $_e->currency_code =stripslashes($_row["currency_code"]);
        $_e->currency_exchange = doubleval($_row["currency_exchange"]);
        $_e->totalusd = doubleval($_row["totalusd"]);
        
        return $_e;
    }
     
    public function register($_e){
        
        $_accountid = $_e->accountid;
        $_subject = addslashes($_e->subject);
        $_status = addslashes( $_e->status);
        $_quotedate = addslashes($_e->quotedate);
        $_quoteuntil = addslashes($_e->quoteuntil);
        $_assignedto = addslashes($_e->assignedto);
        $_customerid = intval($_e->customerid);
        $_contactid = addslashes($_e->contactid);
        $_contacttelephone = addslashes($_e->contacttelephone);
        $_contactmobile = addslashes($_e->contactmobile);
        $_contactemail = addslashes($_e->contactemail);
        $_contactsalutation = addslashes($_e->contactsalutation);
        $_paymentterms = addslashes($_e->paymentterms);
        $_paymenttermsother = addslashes($_e->paymenttermsother);
        
        $_deliveryterms = addslashes($_e->deliveryterms);
        $_deliverytermsother = addslashes($_e->deliverytermsother);
        
        $_carriercompany = addslashes($_e->carriercompany);
        $_carriercompanyother = addslashes($_e->carriercompanyother);
        $_operationtype = addslashes($_e->operationtype);
        $_obs = addslashes($_e->obs);
        $_shippingenabled = addslashes($_e->shippingenabled);
        $_discount = doubleval($_e->discount);
        $_hastaxes = doubleval($_e->hastaxes);
        $_subtotal = doubleval($_e->subtotal);
        $_shippingcost = doubleval($_e->shippingcost);
        $_taxespercent = doubleval($_e->taxespercent);
        $_total = doubleval($_e->total);
        
        $_currency_exchange = doubleval($_e->currency_exchange);        
        $_currency_code = trim($_e->currency_code);  
        
        $_bill_country  = addslashes($_e->bill_country); 
        $_bill_state    = addslashes($_e->bill_state);
        $_bill_city     = addslashes($_e->bill_city);
        $_bill_street   = addslashes($_e->bill_street);
        $_bill_zipcode  = addslashes($_e->bill_zipcode);
        $_bill_pobox    = addslashes($_e->bill_pobox);
        
        $_ship_country  = addslashes($_e->ship_country);
        $_ship_state    = addslashes($_e->ship_state);
        $_ship_city     = addslashes($_e->ship_city);
        $_ship_street   = addslashes($_e->ship_street);
        $_ship_zipcode  = addslashes($_e->ship_zipcode);
        $_ship_pobox    = addslashes($_e->ship_pobox);
        
        $_totalusd = (1/$_currency_exchange)*$_total;
        
        
        $_createdby = trim($_e->createdby);
        
        
        $_prefix = $this->selectOneColumn("quoteprefix", "select quoteprefix from ait_account where id = $_accountid ");
        
        $_quoteno = $this->generateQuoteNo($_prefix ,  $_e->accountid );
        
        $query = "insert into quote ( ";
        $query .=" idcrypt, accountid, quoteno, subject, status, quotedate, quoteuntil, assignedto,   ";
        $query .=" customerid, contactid, contacttelephone, contactmobile, contactemail, contactsalutation,    ";
        $query .=" paymentterms, paymenttermsother, carriercompany, carriercompanyother,     ";
        $query .=" operationtype, shippingenabled, obs,      ";
        $query .=" discount, hastaxes, subtotal, shippingcost, taxespercent, total,     ";
        $query .=" bill_country, bill_state, bill_city, bill_street, bill_zipcode, bill_pobox,      ";
        $query .=" ship_country, ship_state, ship_city, ship_street, ship_zipcode, ship_pobox,      ";
        $query .=" createdby, createddate, deliveryterms, deliverytermsother, currency_code, currency_exchange , totalusd )  values  (   ";
        
        $query .=" '$_e->idcrypt' , ".$_e->accountid.", '$_quoteno', '$_subject',  '$_status' , '$_quotedate' , '$_quoteuntil' , '$_assignedto' ,  ";
        $query .=" $_customerid , '$_contactid' , '$_contacttelephone' , '$_contactmobile' , '$_contactemail' , '$_contactsalutation' , ";
        $query .=" '$_paymentterms' , '$_paymenttermsother' , '$_carriercompany' , '$_carriercompanyother' ,  ";
        $query .=" '$_operationtype' , '$_shippingenabled' , '$_obs' ,   ";
        $query .=" $_discount , $_hastaxes , $_subtotal , $_shippingcost , $_taxespercent , $_total ,  ";
        $query .=" '$_bill_country' , '$_bill_state' , '$_bill_city' , '$_bill_street' , '$_bill_zipcode' , '$_bill_pobox' ,  ";
        $query .=" '$_ship_country' , '$_ship_state' , '$_ship_city' , '$_ship_street' , '$_ship_zipcode' , '$_ship_pobox' ,  ";
        $query .="  '$_createdby' , CURRENT_TIMESTAMP() , '$_deliveryterms' , '$_deliverytermsother' , '$_currency_code', $_currency_exchange , $_totalusd ) ";
        
      //  echo $query; die();   
        
        $_result = $this->executeUpdate($query);
        
        $this->setStatus($_e->idcrypt, "quote", $_status, $_createdby);
        
        return $_result;
        
    }
    
    public function update($_e){
        
        $_idcrypt = $_e->idcrypt;
        $_accountid = $_e->accountid;
        $_subject = addslashes($_e->subject);
        $_status = addslashes( $_e->status);
        $_quotedate = addslashes($_e->quotedate);
        $_quoteuntil = addslashes($_e->quoteuntil);
        $_assignedto = addslashes($_e->assignedto);
        $_customerid = intval($_e->customerid);
        $_contactid = addslashes($_e->contactid);
        $_contacttelephone = addslashes($_e->contacttelephone);
        $_contactmobile = addslashes($_e->contactmobile);
        $_contactemail = addslashes($_e->contactemail);
        $_contactsalutation = addslashes($_e->contactsalutation);
        $_paymentterms = addslashes($_e->paymentterms);
        $_paymenttermsother = addslashes($_e->paymenttermsother);
        $_carriercompany = addslashes($_e->carriercompany);
        $_carriercompanyother = addslashes($_e->carriercompanyother);
        $_operationtype = addslashes($_e->operationtype);
        $_obs = addslashes($_e->obs);
        $_shippingenabled = addslashes($_e->shippingenabled);
        $_discount = doubleval($_e->discount);
        $_hastaxes = doubleval($_e->hastaxes);
        $_subtotal = doubleval($_e->subtotal);
        $_shippingcost = doubleval($_e->shippingcost);
        $_taxespercent = doubleval($_e->taxespercent);
        $_total = doubleval($_e->total);
        
        $_deliveryterms = addslashes($_e->deliveryterms);
        $_deliverytermsother = addslashes($_e->deliverytermsother);
        
        
        $_bill_country  = addslashes($_e->bill_country);
        $_bill_state    = addslashes($_e->bill_state);
        $_bill_city     = addslashes($_e->bill_city);
        $_bill_street   = addslashes($_e->bill_street);
        $_bill_zipcode  = addslashes($_e->bill_zipcode);
        $_bill_pobox    = addslashes($_e->bill_pobox);
        
        $_ship_country  = addslashes($_e->ship_country);
        $_ship_state    = addslashes($_e->ship_state);
        $_ship_city     = addslashes($_e->ship_city);
        $_ship_street   = addslashes($_e->ship_street);
        $_ship_zipcode  = addslashes($_e->ship_zipcode);
        $_ship_pobox    = addslashes($_e->ship_pobox);
        
        $_currency_exchange = doubleval($_e->currency_exchange);        
        $_currency_code = trim($_e->currency_code);               
        $_totalusd = (1/$_currency_exchange)*$_total;
 
        $_modifiedby = trim($_e->modifiedby);
        
        $query  = " update quote set ";
        $query .= " accountid = $_accountid ,   subject = '$_subject' , quotedate = '$_quotedate' , quoteuntil = '$_quoteuntil', assignedto = '$_assignedto' ,  ";
        $query .= " customerid = $_customerid , contactid = '$_contactid', contacttelephone = '$_contacttelephone' , contactmobile = '$_contactmobile' , contactemail = '$_contactemail' , contactsalutation = '$_contactsalutation' ,  ";
        $query .= " paymentterms = '$_paymentterms' , paymenttermsother = '$_paymenttermsother' , carriercompany = '$_carriercompany' , carriercompanyother = '$_carriercompanyother' , ";
        $query .= " operationtype = '$_operationtype' , shippingenabled = '$_shippingenabled' , obs = '$_obs' , status = '$_status' , ";
        $query .= " discount = $_discount , hastaxes = $_hastaxes , subtotal = $_subtotal , shippingcost = $_shippingcost , taxespercent = $_taxespercent , total = $_total,  ";
        $query .= " bill_country = '$_bill_country' , bill_state = '$_bill_state' , bill_city = '$_bill_city', bill_street = '$_bill_street' , bill_zipcode = '$_bill_zipcode' , bill_pobox = '$_bill_pobox' ,  ";
        $query .= " ship_country = '$_ship_country' , ship_state = '$_ship_state' , ship_city = '$_ship_city', ship_street = '$_ship_street' , ship_zipcode = '$_ship_zipcode' , ship_pobox = '$_ship_pobox' ,  ";
        $query .= " modifiedby = '$_modifiedby' , modifieddate = CURRENT_TIMESTAMP() , deliveryterms = '$_deliveryterms' , deliverytermsother = '$_deliverytermsother' ,  ";
        $query .= " currency_code= '$_currency_code', currency_exchange = $_currency_exchange , totalusd = $_totalusd ";
        $query .= " where idcrypt = '$_idcrypt' ";
         
        $_result = $this->executeUpdate($query);
        
        
        $this->setStatus($_e->idcrypt, "quote", $_status, $_modifiedby);
        
        return $_result;
        
    }
     
    public function generateQuoteNo($prefix, $_accountid){
        $query = "select count(*) as total from quote where accountid = $_accountid ";
        $_total = $this->selectOneColumn("total", $query);
        
        $_quoteno = ($_total+1);
        while(strlen($_quoteno) < 6){ $_quoteno = '0'.$_quoteno;  }
        return $prefix.''.$_quoteno;
    }
    

}
