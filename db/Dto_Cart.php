<?php

class Dto_Cart extends Dto_Db {
    
    public function deleteCart($_accountid){
        $query = "DELETE FROM portalcart where accountid='$_accountid'   ";
        return $this->executeUpdate($query);
    }
    
    public function getCartBySessionId($_accountid) {

        $aRecord = array();
        
        $query = "SELECT  ";
        $query .= "m.idcrypt as mediaid,  ";
        $query .= "pc.id, pc.itemid, pc.qty, pc.productid, pc.sessionid,   ";
        $query .= "pc.operation, pc.buyprice, pc.subtotal,  ";
        $query .= "pd.name , p.sku  ";
        $query .= "FROM  portalcart pc  ";
        $query .= "right join product p on p.idcrypt = pc.productid  ";
        $query .= "right join productdesc pd on pd.product_id = p.id  ";
        $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1  ";
        $query .= "where pd.lang_code = 'ENG' and p.deleted = 0 and pc.accountid = '$_accountid'  ";
         
        $rows = $this->selectRows($query);
       
        if($rows){
            $total = count($rows);
            if($total > 0 ){
                foreach ($rows as $row){
                    
                    $cart = new Cart();
                    $cart->mediaid = $row["mediaid"];
                    $cart->id = $row["id"];
                    $cart->itemid = $row["itemid"];
                    $cart->qty = intval($row["qty"]);
                    $cart->productid = $row["productid"];
                    $cart->sessionid = $row["sessionid"];
                    $cart->operation = $row["operation"];
                    $cart->buyprice = $row["buyprice"];
                    $cart->subtotal = $row["subtotal"];
                    $cart->name = $row["name"];
                    $cart->sku = $row["sku"];
                    
                    array_push($aRecord, $cart);
                }
            }
        }
        
        return $aRecord;
        
    }

    public function additem($_cart) {

        $_itemid = time() . '' . rand(100000, 999999);
        $_qty = $_cart->qty;
        $_productid = $_cart->productid;
        $_datecreated = date('Y-m-d H:i:s');
        $_sessionid = session_id();
        $_userip = get_ip();
        $_operation = $_cart->operation;
        $_buyprice = $_cart->buyprice;
        $_subtotal = $_cart->subtotal;
        $_accountid = $_cart->accountid;

        $query = "insert into portalcart (itemid, accountid   ,qty, productid, datecreated, sessionid, userip, operation , buyprice, subtotal ) ";
        $query .=" values(  '$_itemid' , $_accountid ,  $_qty , '$_productid' , '$_datecreated' , '$_sessionid' , '$_userip' ,  '$_operation' , $_buyprice , $_subtotal   )  ";

        return $this->executeUpdate($query);
    }

    public function updateitem($_cart) {
        $_qty = $_cart->qty;
        $_productid = $_cart->productid;
        $_itemid = $_cart->itemid;
        $_buyprice = $_cart->buyprice;
        $_subtotal = $_cart->subtotal;

        $query = "update portalcart set   qty='$_qty' , buyprice = $_buyprice , subtotal = $_subtotal    where itemid='$_itemid'  and productid = '$_productid'  ";
        return $this->executeUpdate($query);
    }

    public function deleteitem($_cart) {
        $_productid = $_cart->productid;
        $_itemid = $_cart->itemid;
        $query = "delete from portalcart   where itemid='$_itemid'  and productid = '$_productid'  ";
        return $this->executeUpdate($query);
    }

    public function deletebyitemid($_itemid) {
     
        $query = "delete from portalcart   where itemid='$_itemid'    ";
        return $this->executeUpdate($query);
    } 
    
    
}
