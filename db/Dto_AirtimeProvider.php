<?php
class Dto_AirtimeProvider extends Dto_Db {
    
    public function getAll( ){
        $query = "select * from airtimeprovider";
        $rows =   $this->selectRows($query);
        if(!$rows){
            return false;
        }
        $total = count($rows);
        if($total == 0){ return false; }
        
        $aRecords = array();
          
        foreach($rows as $row){
            $e = new AirtimeProvider();
            $e->code = $row["code"];
            $e->description =  $row["description"];
            array_push($aRecords, $e);
        }
        return $aRecords;
    }
    
     
}
