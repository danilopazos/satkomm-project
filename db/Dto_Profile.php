<?php

class Dto_Profile extends Dto_Db {
    
    public $aProfile = array();
    
    function __construct() {
        //SKIU-ROOT-MG
        $e1 = new Profile();
        $e1->id = "SKIU-AC-MG";
        $e1->name = "Account Manager";
        array_push($this->aProfile , $e1);
        
        $e2 = new Profile();
        $e2->id = "SKIU-AC-CS";
        $e2->name = "Account Viewer";
        array_push($this->aProfile , $e2);
        
        $e2 = new Profile();
        $e2->id = "SKIU-AC-SALES-EXEC";
        $e2->name = "Sales Executive";
        array_push($this->aProfile , $e2);
        
        
        $e3 = new Profile();
        $e3->id = "SKIU-ROOT-MG";
        $e3->name = "ROOT";
        array_push($this->aProfile , $e3);
        
    }
    
    public function getProfiles(){
        return $this->aProfile;
    }
    
    public function getProfileClient(){
        $aRecord =  array();
        
        $e1 = new Profile();
        $e1->id = "SKIUCUAR";
        $e1->name = "Account Responsible";
        array_push($aRecord , $e1);
        
        $e1 = new Profile();
        $e1->id = "SKIUCURL";
        $e1->name = "Legal Representative";
        array_push($aRecord , $e1);    
        
        $e1 = new Profile();
        $e1->id = "SKIUCUAL";
        $e1->name = "Sub-Representative";
        array_push($aRecord , $e1);   
        
        
        return $aRecord;
    }
    
    public function getProfileClientById($_id){
        $entity = new Profile();
        $clientProfiles = $this->getProfileClient();
        $total = count($clientProfiles);
        if($total > 0){
            foreach($clientProfiles as $profile){
                if($profile->id == $_id){
                    $entity = $profile;
                }
            }
        }
        return $entity;
    }
    
    
    
    
    public function getProfileById($_id){
        $entity = new Profile();
        $total = count($this->aProfile);
        if($total > 0){
            foreach($this->aProfile as $profile){
                if($profile->id == $_id){
                    $entity = $profile;
                }
            }
        }
        return $entity;
    }
    
}