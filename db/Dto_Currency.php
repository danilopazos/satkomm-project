<?php

class Dto_Currency extends Dto_Db {

    public function getListActive($_accountid) {

        $query = "SELECT c.* , e.xrate, e.registereddatetime , u.username  ";
        $query .= "FROM  currency  c ";
        $query .= "left join currencyexchange e on e.accountid = $_accountid and e.currency_code = c.code and e.isactive = 1 ";
        $query .= " left join ait_usersystem u on u.id = e.registereduser where e.isactive = 1 ";

        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }

        $total = count($_rows);
        if ($total == 0) {
            return false;
        }

        $aRecord = array();

        $e = new Currency();
        $e->id = 1;
        $e->code = "USD";
        $e->description = "Dólares Americanos";
        $e->symbol = "$";
        $e->country = "UNITED STATES";
        $e->xrate = 1;
        $e->username = "ROOT";
        $e->registereddatetime = date('Y-m-d H:i:s');

        array_push($aRecord, $e);


        foreach ($_rows as $_row) {

            $e = new Currency();
            $e->id = trim($_row["id"]);
            $e->code = trim($_row["code"]);
            $e->description = trim($_row["description"]);
            $e->symbol = trim($_row["symbol"]);
            $e->country = trim($_row["country"]);
            $e->xrate = doubleval($_row["xrate"]);
            $e->username = trim($_row["username"]);
            //$e->registereddatetime = getDateByTimeZone($this->$_row["registereddatetime"]);
            $e->registereddatetime = $this->$_row["registereddatetime"];
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

//function    

    public function getList($_accountid) {

        $query = "select  ";
        $query .= "C.id, C.country, C.code, C.symbol, E.xrate, U.firstname,  ";
        $query .= "U.lastname, E.registereddatetime ";
        $query .= "from currency C, currencyexchange E, ait_usersystem U ";
        $query .= "where  ";
        $query .= "E.registereduser=U.id and C.code=E.currency_code  ";
        $query .= "and E.accountid='$_accountid' and E.isactive=true  ";

        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }

        $total = count($_rows);
        if ($total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {
            $e = new Currency();
            $e->id = trim($_row["id"]);
            $e->code = trim($_row["code"]);
            $e->symbol = trim($_row["symbol"]);
            $e->country = trim($_row["country"]);
            $e->xrate = number_format($_row['xrate'], 2);
            $e->username = trim($_row["firstname"]." ".$_row["lastname"]);
            //$e->registereddatetime = $this->getDateByTimeZone($_row["registereddatetime"]);
            $e->registereddatetime = $_row["registereddatetime"];
            
            array_push($aRecord, $e);
        }
        return $aRecord;
    }

//function

    public function getListHistory($_accountid, $_currency_code) {

        $query = "SELECT e.*  , u.username  ";
        $query .= "from currencyexchange e  ";
        $query .= "left join ait_usersystem u on u.id = e.registereduser ";
        $query .= "where e.accountid = $_accountid and e.currency_code = '$_currency_code' ";
        $query .= "order by e.registereddatetime desc  limit 10  ";

        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }

        $total = count($_rows);
        if ($total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {

            $e = new CurrencyExchange();
            $e->id = trim($_row["id"]);
            $e->currency_code = trim($_row["currency_code"]);
            $e->username = trim($_row["username"]);
            $e->xrate = doubleval($_row["xrate"]);
            $e->registereddatetime = $this->getDateByTimeZone($_row["registereddatetime"]);

            array_push($aRecord, $e);
        }
        return $aRecord;
    }

//function    

    public function addExchange($_accountid, $_currency_code, $_xrate, $_userid) {

        $_registereddatetime = date('Y-m-d H:i:s');

        $queryinactive = "update currencyexchange set isactive = 0 where accountid = $_accountid and currency_code = '$_currency_code' and isactive = 1 ";
        $_result = $this->executeUpdate($queryinactive);

        if ($_result) {
            $query = "insert into currencyexchange (accountid, currency_code, xrate, registereduser, registereddatetime ) values  ";
            $query .= " ($_accountid, '$_currency_code' , $_xrate, '$_userid' , '$_registereddatetime' )  ";
            $_result = $this->executeUpdate($query);
        }

        return $_result;
    }

}

//clas