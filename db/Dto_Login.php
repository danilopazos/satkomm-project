<?php
class Dto_Login extends Dto_Db {
 
    public function registerip( $_e ){
        $query  = "insert into usersystembyip (id, userid, ip ) values (";
        $query .= " '".$_e->id."' , '".$_e->userid."' , '".$_e->ip."' )  ";
        return $this->executeUpdate($query);
    }
    
    public function registerlog($_e){
        
        //we need to check if the up was registered befor
        /*
        if(!($this->getIfIpExistsByUserid($_e->userid, $_e->userip))){
            $usersystemByIp= new UsersystemByIp();
            $usersystemByIp->id = $this->generateId();
            $usersystemByIp->userid = $_e->userid;
            $usersystemByIp->ip  = $_e->userip;
            $this->registerip($usersystemByIp);
        }
        */
        $query = "insert into usersystemloginlog (id, accessdate, userid, userip) values (";
        $query .=" '".$_e->id."' , CURRENT_TIMESTAMP(), '".$_e->userid."' , '".$_e->userip."' ) ";
        return $this->executeUpdate($query);
    }
    
    public function registertokencode($_e){
        
        $query1= "update usersystemlogintoken set status = -1 where status = 0 and userid ='".$_e->userid."'";
        $result= $this->executeUpdate($query1);
        
        if($result){
            $query = "insert into usersystemlogintoken(id, userid, iptovalidate, daterequested, ";
            $query .=" validationcode, validationtype, validationdest ) values ( ";
            $query .=" '".$_e->id."' ,  '".$_e->userid."' ,  '".$_e->iptovalidate."' ,  CURRENT_TIMESTAMP(),  ";
            $query .=" md5('".$_e->validationcode."') , '".$_e->validationtype."' , '".$_e->validationdest."' )  ";
            return $this->executeUpdate($query);
        }
        return false;
    }
    
    public function validateTokenCode($_e){
        $query = "select id from usersystemlogintoken where userid =  '".$_e->userid."' and iptovalidate = '".$_e->iptovalidate."' and status = 0   ";        
        $query .=" and validationcode = md5('".$_e->validationcode."') limit 1 ";
        $id = $this->selectOneColumn("id", $query);
        return $id;    
    }
    
    public function setTokenValidated($_id){
        $query = "update usersystemlogintoken set status=1 , datevalidated = CURRENT_TIMESTAMP() where id = '$_id' ";
        return $this->executeUpdate($query);
    }
    
    public function getIfIpExistsByUserid($_userid, $_ip){
        $query = "select id from usersystembyip where ip = '$_ip'   and userid = '$_userid' ";
        
        
        $id = $this->selectOneColumn("id", $query);
        if($id !=""){
            return true;
        }
        return false;
    }
    
    public function generateId(){
        return md5(date('ymdHis').''.rand(10000,99999).''.rand(10000,99999));
    }
    
}