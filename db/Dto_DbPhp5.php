<?php

class Dto_Db {

    public $link;

    public function connect($typeAction = "") {
        $config = new ConfigReader();
        if ($typeAction == "update") {
            $config = new ConfigUpdate();
        }

        $this->link = mysql_connect($config->main_dbhost . ":" . $config->main_dbport, $config->main_dbuser, $config->main_dbpass);
         
        mysql_select_db($config->main_dbname, $this->link);
    }

    public function close() {
        mysql_close($this->link);
    }

    public function updateAudit($userid, $module, $action, $description) {
        $query = "insert into ait_auditlog (userid, module, action, dateaudit, ip, description ) values('$userid', '$module' , '$action' ,  CURRENT_TIMESTAMP() , '" . get_ip() . "' ,   '$description' )  ";
        $this->executeUpdate($query);
    }

    public function selectRows($query) {
        $this->connect();

        $data = mysql_query($query, $this->link);

        $aList = array();

        if (!$data) {
            return false;
        } else {

            if (mysql_numrows($data) > 0) {

                while ($item = mysql_fetch_array($data)) {

                    array_push($aList, $item);
                }//while
            } else {
                return false;
            }//else
        }//else

        $this->close();

        return $aList;
    }

    public function selectOneRow($query) {

        $this->connect();
        $data = mysql_query($query, $this->link);
        $aList = array();

        if (!$data) {
            return false;
        } else {

            if (mysql_numrows($data) > 0) {

                $aList = mysql_fetch_array($data);
            } else {
                return false;
            }//else
        }//else

        $this->close();

        return $aList;
    }

    public function selectOneColumn($column, $query) {
        $this->connect();
        $data = mysql_query($query, $this->link);

        $value = null;

        if (!$data) {
            return false;
        } else {

            if (mysql_numrows($data) > 0) {

                $item = mysql_fetch_array($data);

                $value = $item[$column];
            } else {
                return false;
            }//else
        }//else

        $this->close();

        return $value;
    }

    public function executeUpdate($query) {

        $this->connect("update");

        $ok = false;
        if (mysql_query($query, $this->link)) {
            $ok = true;
        } else {
            $ok = false;
        }

        $this->close();

        return $ok;
    }
    
    public function getDateByTimeZone($_date){
        //America/New_York	
        if( $_SESSION["session_timezonename"] == ""){
            $_SESSION["session_timezonename"] = "America/New_York";
        }
        if($_date !=""){
            $UTC = new DateTimeZone("UTC");
            $newTZ = new DateTimeZone($_SESSION["session_timezonename"]);
            $date = new DateTime( $_date , $UTC );
            $date->setTimezone( $newTZ );
            return $date->format('Y-m-d H:i:s'); 
        }else{
            return "";
        }
    }

}
