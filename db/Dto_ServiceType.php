<?php
class Dto_ServiceType extends Dto_Db {

    public function getAll(){
        $aRecord = array();
        $e1 = new ServiceType();
        $e1->code = 'PREPAID';
        array_push($aRecord, $e1);
        
        $e2 = new ServiceType();
        $e2->code = 'POSTPAID';
        array_push($aRecord, $e2);
        return $aRecord;
    }

}