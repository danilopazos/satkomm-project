<?php

class Dto_Dashboard extends Dto_Db {

    public function getCustomerswithmoreactivity($_accountid) {
        
        $query = "";
        
        if($_accountid == 10001){


            $query = "select a.id as customeraccountid, a.name as customername , a.creditlimit , ";
            $query .= "(  select sum(x_ammount) from invoice where accountid = a.id   ) as invoiceammount, ";
            $query .= "(  select sum(paymentammount) from invoicepayment where customeraccountid = a.id  ) as paymentammount ";
            $query .= "from ait_account a ";
            $query .= "where a.parentaccountid = $_accountid ";
            $query .= "and a.deleted = 0 order by invoiceammount desc  limit 5";
           //echo $query; die();
        }else{


            $query = "select a.id as customeraccountid, a.name as customername , a.creditlimit , ";
            $query .= "(  select sum(totalusd) from invoicetocustomer where customerid = a.id and deleted = 0  ) as invoiceammount, ";
            $query .= "(  select sum(paymentammount) from invoicepayment where customeraccountid = a.id  ) as paymentammount ";
            $query .= "from ait_account a ";
            $query .= "where a.parentaccountid = $_accountid ";
            $query .= "and a.deleted = 0 order by invoiceammount desc  limit 5";

            
        }
        

        $_rows = $this->selectRows($query);
        if (count($_rows) == 0) {
            return false;
        }

        $aRecord = array();

        foreach ($_rows as $row) {

            $e = new InvoicePayment();
            $e->type = $row["type"];
            $e->invoiceid = $row["invoiceid"];
            $e->provideraccountid = $row["provideraccountid"];
            $e->customeraccountid = $row["customeraccountid"];
            $e->invoiceno = $row["invoiceno"];
            $e->invoicedate = $row["invoicedate"];
            $e->invoiceduedate = $row["invoiceduedate"];
            $e->invoiceammount = $row["invoiceammount"];
            $e->paymentammount = $row["paymentammount"];
            $e->paymentref = $row["paymentref"];
            $e->paymentdate = $row["paymentdate"];
            $e->paymentfilename = $row["paymentfilename"];
            $e->customername = $row["customername"];
            $e->creditlimit = $row["creditlimit"];
            
            array_push($aRecord, $e);
        }

        return $aRecord;
    }

    public function getServicesMoreActivated($_accountid, $_datestart = '', $_dateend = '') {
 
        $query = "select ";
        $query .= "d.name as planname , count(*) as total   ";
        $query .= "from documentservice d ";
        $query .= "right join invoicetocustomer ic on ic.idcrypt = d.docidcrypt and d.doctype = 'invoicetc' ";
        $query .= "where ic.invoicedate between '$_datestart' and '$_dateend' and ic.accountid = $_accountid and ic.deleted = 0 and d.name is not null  ";
        $query .= "group by d.name ";
 
        return $this->selectRows($query);
    }

    public function getServicesMoreActivatedMatrix( $_datestart = '', $_dateend = '') {
        /*
        $query = "select ";
        $query .= "d.name as planname , count(*) as total   ";
        $query .= "from documentservice d ";
        $query .= "right join invoicetocustomer ic on ic.idcrypt = d.docidcrypt and d.doctype = 'invoicetc' ";
        $query .= "where ic.invoicedate between '$_datestart' and '$_dateend' and ic.accountid = $_accountid and ic.deleted = 0 and d.name is not null  ";
        $query .= "group by d.name ";
        */
        $_dateend = date('Y-m-d');
        $query  = "select planname, count(*) as total from airtimeoperation where operation = 'activate' ";
        $query .= "and requesteddatetime between    '$_datestart' and '$_dateend'  ";
        $query .= "group by planname ";
        
       //echo $query; die();
        
        return $this->selectRows($query);
    }    
    
    
    
    public function getActivationsPerDistributor($_accountid, $_datestart = '', $_dateend = '') {

        $_dateend = date('Y-m-d');
        //$_datestart = date('Y-m-d', strtotime('-30 days'));


        $query = " SELECT ac.name, count(aop.id) as total FROM  airtimeoperation aop  ";
        $query .= "RIGHT join airtimeorder ao on ao.code = aop.airtimeordercode ";
        $query .= "RIGHT join ait_account ac on ac.id = ao.accountcustomerid ";
        $query .= "where requeststatus = 'executed' ";
        if ($_accountid != '10001') {
            $query .= " ao.accountcustomerid = $_accountid ";
        }
        $query .= "and aop.datetoexecute between '$_datestart' and '$_dateend' ";
        $query .= "group by ac.name ";


        return $this->selectRows($query);
    }

    public function getDevicesBestSellMatrix(  $_datestart = '', $_dateend = '') {
 
        $_dateend = date('Y-m-d');
        
        
        /*
        $query = "select  ";
        $query .= "d.productname as productname , count(*) as total   ";
        $query .= "from invoicedetail d  ";
        $query .= "right join invoice ic on ic.idcrypt = d.docidcrypt and d.doctype = 'invoicetc' ";
        $query .= "where ic.invoicedate between '$_datestart' and '$_dateend'    and d.name is not null  ";
        $query .= "group by d.name ";
        */
        
        $query = "select d.productname as productname , count(*) as total ";
        $query .= "from invoicedetail d  ";
        $query .= "right join invoice ic  ";
        $query .= "on ic.code = d.code   where ic.invoicedate between '$_datestart' and '$_dateend' ";
        $query .= "and d.productname is not null group by d.productname ";
        
      
        
        return $this->selectRows($query);
    }    
    
    
    public function getDevicesBestSell($_accountid, $_datestart = '', $_dateend = '') {

       

        $query = "select  ";
        $query .= "d.name as productname , count(*) as total   ";
        $query .= "from documentproduct d  ";
        $query .= "right join invoicetocustomer ic on ic.idcrypt = d.docidcrypt and d.doctype = 'invoicetc' ";
        $query .= "where ic.invoicedate between '$_datestart' and '$_dateend' and ic.accountid = $_accountid and ic.deleted = 0  and d.name is not null  ";
        $query .= "group by d.name ";



        return $this->selectRows($query);
    }

    public function getDistributorPerSales($_accountid, $_datestart = '', $_dateend = '') {

        $_dateend = date('Y-m-d');
       
        $query = "select ac.name, sum(i.x_ammount)  as total ";
        $query .= "FROM invoice i ";
        $query .= "RIGHT JOIN ait_account ac on ac.id = i.accountid ";
        $query .= "where i.accountid > 0 ";
        if ($_accountid != '10001') {
            $query .= " and i.accountid = $_accountid ";
        }
        $query .= "and i.invoicedate between '$_datestart' and '$_dateend' ";
        $query .= "group by ac . name ";


        return $this->selectRows($query);
    }

}
