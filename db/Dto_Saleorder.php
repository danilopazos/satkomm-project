<?php

class Dto_Saleorder extends Dto_Db {

    public function generateOrderCode() {
        /*
        $query = "select count(*) as total from saleorder ";
        $total = intval($this->selectOneColumn("total", $query)) + 1;

        $code = '' . $total;

        while (strlen($code) < 6) {
            $code = "0" . $code;
        }

        $code = 'O-' ."1". $code;
        return $code;
         */
        
        $yearcode = date('Y');
        $y2 = date('y');        
        
        $query = "select count(*) as total from saleorder where year(orderdate) = '$yearcode'    ";
        $index = '' . intval($this->selectOneColumn("total", $query)) + 1;
 
        while (strlen($index) < 7) {
            $index = "0" . $index;
        }

        return "OD-".$y2."". $index;        
        
        
    }

    public function placeOrder($_saleorder, $_saleorderdetail, $_billaddress, $_shipaddress) {
        
        $_ordercomments = addslashes($_saleorder->ordercomments);
        $_bill_country = addslashes($_billaddress->country);
        $_bill_state = addslashes($_billaddress->state);
        $_bill_city = addslashes($_billaddress->city);
        $_bill_street = addslashes($_billaddress->street);
        $_bill_zipcode = addslashes($_billaddress->zipcode);
        $_bill_pobox = addslashes($_billaddress->pobox);
        $_ship_country = addslashes($_shipaddress->country);
        $_ship_state = addslashes($_shipaddress->state);
        $_ship_city = addslashes($_shipaddress->city);
        $_ship_street = addslashes($_shipaddress->street);
        $_ship_zipcode = addslashes($_shipaddress->zipcode);
        $_ship_pobox = addslashes($_shipaddress->pobox);

        $_dispatchnow = intval($_saleorder->dispatchnow);

        $idcrypt = md5(date('YmdHis').''. rand(1000, 999).''. rand(1000, 999));

        
        $query = "insert into saleorder (  ";
        $query .="  idcrypt, code,   accountid  ,  orderdate, orderpaymentrand, userid, userip, sessionid, username,   ";
        $query .="  payment_method , totalitems, subtotal, taxes, shipping_cost, x_ammount, ordercomments,  ";
        $query .="  billing_country, billing_state, billing_city, billing_address, billing_zipcode, billing_pobox, ";
        $query .="  shipping_country, shipping_state, shipping_city, shipping_address, shipping_zipcode, shipping_pobox , dispatchnow ";
        $query .=" ) values  ";
        $query .=" ( '$idcrypt', '" . $_saleorder->code . "' ,  " . $_saleorder->accountid . "      ,  CURRENT_TIMESTAMP()    , '" . $_saleorder->orderpaymentrand . "' , '" . $_saleorder->userid . "' ,  '" . $_saleorder->userip . "'    ,  '" . $_saleorder->sessionid . "' , '" . $_saleorder->username . "' ,  ";
        $query .="   '" . $_saleorder->payment_method . "' ,   " . $_saleorder->totalitems . " ,   " . $_saleorder->subtotal . " ,   " . $_saleorder->taxes . "     ,    " . $_saleorder->shipping_cost . "     ,     " . $_saleorder->x_ammount . " ,  '" . $_ordercomments . "'    ,  ";
        $query .="   '" . $_bill_country . "' , '" . $_bill_state . "' , '" . $_bill_city . "' , '" . $_bill_street . "' , '" . $_bill_zipcode . "' , '" . $_bill_pobox . "' ,  ";
        $query .="   '" . $_ship_country . "' , '" . $_ship_state . "' , '" . $_ship_city . "' , '" . $_ship_street . "' , '" . $_ship_zipcode . "' , '" . $_ship_pobox . "'  , $_dispatchnow   )     ";

        $result = $this->executeUpdate($query);
        if ($result) {
            $result = $this->placeOrderDetail($_saleorder->code, $_saleorderdetail);
        }
        return $result;
    }

    public function placeOrderDetail($_code, $_saleorderdetail) {
        $result = false;
        $query = "insert into saleorderdetail (code, itemid,   sku, qty, productid, productprice, productname, operation, typeofproduct, subtotal )";
        $total = count($_saleorderdetail);
        if ($total > 0) {
            $query .=" values     ";
            for ($index = 0; $index < $total; $index ++) {
                $item = $_saleorderdetail[$index];
                $_itemid = $item->itemid;
                $_sku = $item->sku;
                $_qty = $item->qty;
                $_productid = $item->productid;
                $_productprice = $item->productprice;
                $_productname = $item->productname;
                $_operation = $item->operation;
                $_typeofproduct = $item->typeofproduct;
                $_subtotal = $item->subtotal;
                $query .="  ( '$_code' , '$_itemid' , '$_sku' , $_qty , '$_productid' , $_productprice , '$_productname' , '$_operation' , '$_typeofproduct'  , $_subtotal )   ";
                if (($index + 1 ) < $total) {
                    $query .=" ,  ";
                }
            }
            $result = $this->executeUpdate($query);
        }
        return $result;
    }

    public function cancelOrder($_code, $_userid) {
        $query = "update saleorder set orderstatus = 0 , canceldatetime = CURRENT_TIMESTAMP() , canceluserid = '$_userid' where code = '$_code' ";

        return $this->executeUpdate($query);
    }
    
    public function getCodeByIdcrypt($_idcrypt){
        $query ="select code from saleorder where idcrypt = '$_idcrypt' ";        
        return $this->selectOneColumn("code", $query);
    }
    
    
    public function getByCode($_code) {
        $query = " SELECT a.name as accountname,  u.username as u_username, u.firstname as u_firstname , u.lastname as u_lastname,     so.*  "
                . " , i.code as invoice_code, i.dispatchdate, i.deliverydate  "
                . " FROM saleorder so "
                . " RIGHT JOIN ait_account  a on a.id = so.accountid "
                . " RIGHT JOIN ait_usersystem  u on u.id = so.userid "
                . " LEFT JOIN invoice  i on i.ordercode = so.code "
                . " WHERE so.code = '$_code' LIMIT   1  ";

        $row = $this->selectOneRow($query);
        $saleOrder = new SaleOrder();
        if ($row) {
            $saleOrder->id = $row["id"];
            
            $saleOrder->idcrypt = $row["idcrypt"];
            
            $saleOrder->code = $row["code"];
            $saleOrder->accountname = $row["accountname"];
            $saleOrder->accountid = $row["accountid"];
            $saleOrder->username = $row["u_username"];
            $saleOrder->firstname = $row["u_firstname"];
            $saleOrder->lastname = $row["u_lastname"];

            $saleOrder->ordercomments = $row["ordercomments"];


            $saleOrder->orderdate = $this->getDateByTimeZone($row["orderdate"]);

            $saleOrder->invoice_code = $row["invoice_code"];
            $saleOrder->invoice_dispatchdate = $this->getDateByTimeZone($row["dispatchdate"]);
            $saleOrder->invoice_deliverydate = $this->getDateByTimeZone($row["deliverydate"]);

            $saleOrder->userid = $row["userid"];
            $saleOrder->sessionid = $row["sessionid"];
            $saleOrder->totalitems = intval($row["totalitems"]);
            $saleOrder->subtotal = doubleval($row["subtotal"]);
            $saleOrder->taxes = doubleval($row["taxes"]);
            $saleOrder->shipping_cost = doubleval($row["shipping_cost"]);
            $saleOrder->x_ammount = doubleval($row["x_ammount"]);

            $saleOrder->billing_country = $row["billing_country"];
            $saleOrder->billing_state = $row["billing_state"];
            $saleOrder->billing_city = $row["billing_city"];
            $saleOrder->billing_address = $row["billing_address"];
            $saleOrder->billing_zipcode = $row["billing_zipcode"];
            $saleOrder->billing_pobox = $row["billing_pobox"];

            $saleOrder->shipping_country = $row["shipping_country"];
            $saleOrder->shipping_state = $row["shipping_state"];
            $saleOrder->shipping_city = $row["shipping_city"];
            $saleOrder->shipping_address = $row["shipping_address"];
            $saleOrder->shipping_zipcode = $row["shipping_zipcode"];
            $saleOrder->shipping_pobox = $row["shipping_pobox"];
            $saleOrder->dispatchnow = intval($row["dispatchnow"]);


            $saleOrder->orderstatus = trim($row["orderstatus"]);
            $saleOrder->canceldatetime = $this->getDateByTimeZone($row["canceldatetime"]);
        }

        return $saleOrder;
    }

    public function getDetailByCode($_code) {
        $aRecord = array();
        $query = "SELECT sd.* , m.idcrypt as mediaid    "
                . " FROM saleorderdetail sd  "
                . " RIGHT JOIN product p on p.idcrypt = sd.productid "
                . " LEFT JOIN media m on m.entity_id = p.id  and m.type = 'IMAGE' and m.ismain = 1  "
                . " WHERE sd.code = '$_code'   ";

        $rows = $this->selectRows($query);
        if ($rows) {
            $total = count($rows);
            if ($total == 0) {
                return false;
            }

            foreach ($rows as $row) {
                $entity = new SaleOrderDetail();
                $entity->id = $row["id"];
                $entity->code = $row["code"];
                $entity->sku = $row["sku"];
                $entity->qty = intval($row["qty"]);
                $entity->productprice = doubleval($row["productprice"]);
                $entity->productname = $row["productname"];
                $entity->productid = $row["productid"];
                $entity->operation = $row["operation"];
                $entity->itemid = $row["itemid"];
                $entity->accountid = $row["accountid"];
                $entity->subtotal = doubleval($row["subtotal"]);
                $entity->mediaid = $row["mediaid"];
                array_push($aRecord, $entity);
            }
        }
        return $aRecord;
    }

    public function getCodeByChildOrder($_parentaccountid, $_code) {
        $query = "SELECT so.id ";
        $query .= "FROM saleorder  so   ";
        $query .= "RIGHT JOIN ait_account a ON a.id = so.accountid  and a.parentaccountid = $_parentaccountid ";
        $query .= "WHERE a.id = so.accountid  and a.parentaccountid = $_parentaccountid  and so.code = '$_code' ";

        $id = intval($this->selectOneColumn("id", $query));
        return $id;
    }

    //-- My Sales Order
    public function getMySaleOrders($_orderstate, $_accountid, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10) {

        $query = "SELECT so.code, sp.code as shippingprovidercode, sp.name as shippingprovider_name, i.shippingtrackingid , so.orderdate, so.orderstatus, so.userid, so.totalitems , so.x_ammount , u.firstname, u.lastname , ";
        $query .="  i.code as invoice_code, i.deliverydate, i.dispatchdate   ";
        $query .="FROM saleorder  so ";
        $query .="RIGHT JOIN ait_usersystem u ON u.id = so.userid ";

        $query .= " LEFT JOIN invoice i on i.ordercode = so.code ";
        $query .= " LEFT JOIN shippingprovider  sp on sp.code = i.shippingprovidercode ";
        $query .="where so.accountid = $_accountid ";

        if ($_search != "") {
            $query .=" and (";
            $query .=" i.shippingtrackingid like '%$_search%' OR  ";
            $query .=" a.name like '%$_search%' OR  ";
            $query .=" so.code like '%$_search%' OR  ";
            $query .=" so.orderdate like '%$_search%' OR  ";
            $query .=" u.firstname like '%$_search%' OR  ";
            $query .=" i.code like '%$_search%' OR  ";
            $query .=" i.deliverydate  like '%$_search%' OR  ";
            $query .=" i.dispatchdate  like  '%$_search%' OR  ";
            $query .=" so.lastname  like '%$_search%'  ";
            $query .="  )  ";
        }


        if ($_orderstate != "") {
            if ($_orderstate == "created") {
                $query .=" and i.deliverydate is null and i.dispatchdate is null and so.canceldatetime is null and i.invoicedate is null   ";
            }
            if ($_orderstate == "dispatched") {
                $query .=" and i.deliverydate is null and i.dispatchdate is not null and so.canceldatetime is null  ";
            }
            if ($_orderstate == "delivered") {
                $query .=" and i.deliverydate is not null and i.dispatchdate is not null and so.canceldatetime is null  ";
            }
            if ($_orderstate == "cancelled") {
                $query .=" and i.deliverydate is  null and i.dispatchdate is  null and so.canceldatetime is not null  ";
            }
            if ($_orderstate == "readytodispatch") {
                $query .=" and i.deliverydate is  null and i.dispatchdate is  null and so.canceldatetime is  null and i.invoicedate is not null  ";
            }
        }


        $query .="ORDER BY $_sortCol $_sortDir";
        $query .=" LIMIT $_limit1 , $_limit2";

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            $entity = new SaleOrder();
            $entity->code = $row["code"];
            $entity->orderdate = $row["orderdate"];
            $entity->userid = $row["userid"];
            $entity->totalitems = intval($row["totalitems"]);
            $entity->x_ammount = doubleval($row["x_ammount"]);
            $entity->firstname = $row["firstname"];
            $entity->lastname = $row["lastname"];
            $entity->dispatchnow = intval($row["dispatchnow"]);

            $entity->invoice_code = $row["invoice_code"];
            $entity->invoice_deliverydate = $this->getDateByTimeZone($row["deliverydate"]);
            $entity->invoice_dispatchdate = $this->getDateByTimeZone($row["dispatchdate"]);

            $entity->shippingtrackingid = $row["shippingtrackingid"];
            $entity->shippingprovidercode = $row["shippingprovidercode"];
            $entity->shippingprovider_name = $row["shippingprovider_name"];

            $entity->orderstatus = $row["orderstatus"];


            array_push($aRecords, $entity);
        }
        return $aRecords;
    }

    public function getMySaleOrdersTotal($_orderstate, $_accountid, $_search) {

        $query = "SELECT count(so.id) as total ";
        $query .="FROM saleorder  so ";
        $query .="RIGHT JOIN ait_usersystem u ON u.id = so.userid ";
        $query .= " LEFT JOIN invoice i on i.ordercode = so.code ";
        $query .= " LEFT JOIN shippingprovider  sp on sp.code = i.shippingprovidercode ";
        $query .="where so.accountid = $_accountid ";


        if ($_search != "") {
            $query .=" and (";
            $query .=" i.shippingtrackingid like '%$_search%' OR  ";
            $query .=" a.name like '%$_search%' OR  ";
            $query .=" so.code like '%$_search%' OR  ";
            $query .=" so.orderdate like '%$_search%' OR  ";
            $query .=" u.firstname like '%$_search%' OR  ";
            $query .=" i.code like '%$_search%' OR  ";
            $query .=" i.deliverydate  like '%$_search%' OR  ";
            $query .=" i.dispatchdate  like  '%$_search%' OR  ";
            $query .=" so.lastname  like '%$_search%'  ";
            $query .="  )  ";
        }

        if ($_orderstate != "") {
            if ($_orderstate == "created") {
                $query .=" and i.deliverydate is null and i.dispatchdate is null and so.canceldatetime is null and i.invoicedate is null   ";
            }
            if ($_orderstate == "dispatched") {
                $query .=" and i.deliverydate is null and i.dispatchdate is not null and so.canceldatetime is null  ";
            }
            if ($_orderstate == "delivered") {
                $query .=" and i.deliverydate is not null and i.dispatchdate is not null and so.canceldatetime is null  ";
            }
            if ($_orderstate == "cancelled") {
                $query .=" and i.deliverydate is  null and i.dispatchdate is  null and so.canceldatetime is not null  ";
            }
            if ($_orderstate == "readytodispatch") {
                $query .=" and i.deliverydate is  null and i.dispatchdate is  null and so.canceldatetime is  null and i.invoicedate is not null  ";
            }
        }

        $total = intval($this->selectOneColumn("total", $query));

        return $total;
    }

    //-- My Customer Orders
    public function getMyCustomerOrders($_parentaccountid, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10) {

        $query = " SELECT so.code, a.name as accountname,  so.orderdate, so.userid, so.orderstatus,   ";
        $query .= " so.totalitems , so.x_ammount , u.firstname, u.lastname ,  i.code as invoice_code, i.deliverydate, i.dispatchdate   ";
        $query .= " FROM saleorder  so    ";
        $query .= " RIGHT JOIN ait_usersystem u ON u.id = so.userid ";
        $query .= " RIGHT JOIN ait_account a ON a.id = so.accountid  and a.parentaccountid = $_parentaccountid  ";
        $query .= " LEFT JOIN invoice i on i.ordercode = so.code ";
        $query .= " WHERE a.id = so.accountid  and a.parentaccountid = $_parentaccountid         ";


        if ($_search != "") {
            $query .=" and (";
            $query .=" a.name like '%$_search%' OR  ";
            $query .=" so.code like '%$_search%' OR  ";
            $query .=" so.orderdate like '%$_search%' OR  ";
            $query .=" u.firstname like '%$_search%' OR  ";
            $query .=" i.code like '%$_search%' OR  ";
            $query .=" i.deliverydate  like '%$_search%' OR  ";
            $query .=" i.dispatchdate  like  '%$_search%' OR  ";
            $query .=" so.lastname  like '%$_search%'  ";
            $query .="  )  ";
        }
        $query .="ORDER BY $_sortCol $_sortDir";
        $query .=" LIMIT $_limit1 , $_limit2";



        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            $entity = new SaleOrder();
            $entity->code = $row["code"];
            $entity->accountname = $row["accountname"];
            $entity->orderdate = $row["orderdate"];
            $entity->userid = $row["userid"];
            $entity->totalitems = intval($row["totalitems"]);
            $entity->x_ammount = doubleval($row["x_ammount"]);
            $entity->firstname = $row["firstname"];
            $entity->lastname = $row["lastname"];
            $entity->dispatchnow = intval($row["dispatchnow"]);

            $entity->invoice_code = $row["invoice_code"];
            $entity->invoice_deliverydate = $this->getDateByTimeZone($row["deliverydate"]);
            $entity->invoice_dispatchdate = $this->getDateByTimeZone($row["dispatchdate"]);
            $entity->orderstatus = $row["orderstatus"];

            array_push($aRecords, $entity);
        }
        return $aRecords;
    }

    public function getMyCustomerOrdersTotal($_parentaccountid, $_search) {

        $query = " SELECT count(so.id) as total ";
        $query .= " FROM saleorder  so  ";
        $query .= " RIGHT JOIN ait_usersystem u ON u.id = so.userid ";
        $query .= " RIGHT JOIN ait_account a ON a.id = so.accountid ";
        $query .= " where a.parentaccountid = $_parentaccountid ";
        if ($_search != "") {
            $query .=" and (";
            $query .=" a.name like '%$_search%' OR  ";
            $query .=" so.code like '%$_search%' OR  ";
            $query .=" so.orderdate like '%$_search%' OR  ";
            $query .=" u.firstname like '%$_search%' OR  ";
            $query .=" i.code like '%$_search%' OR  ";
            $query .=" i.deliverydate  like '%$_search%' OR  ";
            $query .=" i.dispatchdate  like  '%$_search%' OR  ";
            $query .=" so.lastname  like '%$_search%'  ";
            $query .="  )  ";
        }

        $total = intval($this->selectOneColumn("total", $query));

        return $total;
    }

}
