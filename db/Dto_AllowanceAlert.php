<?php
class Dto_AllowanceAlert extends Dto_Db {
    
    public function generateid(){
        return md5(date('Ymd').''.rand(10000,99999).''.rand(111,999));
    }
    
    
    public function deleteById( $idcrypt ){
        $query = " update allowancealert set deleted = 1 where id = '$idcrypt'   "; 
        return $this->executeUpdate($query);
    }
    
    public function getPlanIdcryptByTallowance($_taidcrypt){
        
        $query  = "SELECT ";
        $query .= "p.idcrypt as planidcrypt ";
        $query .= "FROM ait_trafficallowance ta  ";
        $query .= "right join plan p on p.id = ta.planid ";
        $query .= "where ta.idcrypt = '$_taidcrypt' ";           
        
        
        return $this->selectOneColumn("planidcrypt", $query);
    }
    
    
    public function register($_e){
        
        $_emails = addslashes($_e->emails);
        $_subject = addslashes($_e->subject);
        $_mobilenumbers = addslashes($_e->mobilenumbers);
        
        $query  =  "insert into allowancealert ( id, customeraccountid, registeruserid, registerdatetime ";
        $query .= ", inventoryitemid, source, iserial,  emails, subject, mobilenumbers ";
        
        $query .=" , planidcrypt , taidcrypt  ";
        $query .=" , taexceed01 , taexceed02 , taexceed03  ";
        $query .=" , acexceed01 , acexceed02 , acexceed03   ";
        
        $query  .=" ) ";
        
        $query .= "values ( '".$_e->id."' , ".$_e->customeraccountid." , '".$_e->registeruserid."' , CURRENT_TIMESTAMP()    ";
        $query .= " , ".$_e->inventoryitemid." , '".$_e->source."' , '".$_e->iserial."'   ";
        $query .=" ,  '".$_emails."' , '".$_subject."' , '".$_mobilenumbers."'  ";
        
        $query .=" ,  '".$_e->planidcrypt."' , '".$_e->taidcrypt."'  ";
        $query .=" ,  ".$_e->taexceed01." ,  ".$_e->taexceed02." ,  ".$_e->taexceed03."     ";
        $query .=" ,  ".$_e->acexceed01." ,  ".$_e->acexceed02." ,  ".$_e->acexceed03."     ";
        
        $query .=") ";
     
       // echo $query; die();
        
        
        return $this->executeUpdate($query);
    }
    
    
    public function update($_e){
        
        $_emails = addslashes($_e->emails);
        $_subject = addslashes($_e->subject);
        $_mobilenumbers = addslashes($_e->mobilenumbers);
       
        $_id = $_e->id;
        
        $query = "";
        $query .="update allowancealert set ";
      
        $query .="emails = '".$_emails."' ,  ";
        $query .="subject = '".$_subject."' ,  ";
        $query .="mobilenumbers = '".$_mobilenumbers."' ,  ";
        
        $query .="taidcrypt = '".$_e->taidcrypt."' ,  ";
        
        $query .="taexceed01 = ".$_e->taexceed01." ,  ";
        $query .="taexceed02 = ".$_e->taexceed02." ,  ";
        $query .="taexceed03 = ".$_e->taexceed03." ,  ";
        
        $query .="acexceed01 = ".$_e->acexceed01." ,  ";
        $query .="acexceed02 = ".$_e->acexceed02." ,  ";
        $query .="acexceed03 = ".$_e->acexceed03."    ";        
        $query .="where id = '".$_id."'  ";     
        
       // echo $query; die();
        
        return $this->executeUpdate($query);
    }    
    
    
    
    public function delete($_e){
        $query = "update allowancealert set deleted = 1 , deleteduserid = '".$_e->deleteduserid."' , deleteddatetime = CURRENT_TIMESTAMP() where id = '".$_e->id."' ";
        return $this->executeUpdate($query);
    }
    
    public function getById($_id){
        
        /*
        $query = "select a.* from ";
        $query .=" allowancealert a  ";
        $query .=" where a.id = '$_id' ";
        */
        
        $query = " select a.* , ta.tname as trafficallowance_name ";
        $query .=" from allowancealert a  ";
        $query .=" left join ait_trafficallowance ta on ta.idcrypt = a.taidcrypt ";
        $query .=" where a.id = '$_id' " ;        
        
        //echo $query; die();
        
        $row = $this->selectOneRow($query);
        return  $this->getEntityByRow($row);
    }
    
    public function getEntityByRow($row){
        
        $entity = new AllowanceAlert();
        if(!$row){return false;}
        
        $entity->id = trim($row["id"]);
        $entity->customeraccountid = intval($row["customeraccountid"]);
        $entity->accountname = trim($row["accountname"]);
        $entity->registeruserid = trim($row["registeruserid"]);
        $entity->registerdatetime = trim($row["registerdatetime"]);
        $entity->deleted = intval($row["deleted"]);
        $entity->deleteduserid = trim($row["deleteduserid"]);
        $entity->deleteddatetime = trim($row["deleteddatetime"]);
        $entity->inventoryitemid = trim($row["inventoryitemid"]);
        $entity->iserial = trim($row["iserial"]);
        $entity->source = trim($row["source"]);
      
        $query .=" , planidcrypt , taidcrypt  ";
        $query .=" , taexceed01 , taexceed02 , taexceed03  ";
        $query .=" , acexceed01 , acexceed02 , acexceed03   ";        
        
        
        $entity->trafficallowance_name = trim($row["trafficallowance_name"]);
        $entity->plan_name             = trim($row["plan_name"]);        
        
        $entity->planidcrypt = trim($row["planidcrypt"]);
        $entity->taidcrypt = trim($row["taidcrypt"]);
        
        $entity->taexceed01 = doubleval($row["taexceed01"]);
        $entity->taexceed02 = doubleval($row["taexceed02"]);
        $entity->taexceed03 = doubleval($row["taexceed03"]);
        
        $entity->acexceed01 = doubleval($row["acexceed01"]);
        $entity->acexceed02 = doubleval($row["acexceed02"]);
        $entity->acexceed03 = doubleval($row["acexceed03"]);        
        
        
        $entity->emails = trim($row["emails"]);
        $entity->subject = trim($row["subject"]);
        $entity->mobilenumbers = trim($row["mobilenumbers"]);
        
        return $entity;
        
    }
    
    public function getListByAccountid($_accountid, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10){
       
        $query   = "  select alert.id, alert.registerdatetime,  a.name as accountname, alert.emails, alert.mobilenumbers,    ";
        $query  .= "  pd.name as plan_name,  ";
        $query  .= "  ta.tname as trafficallowance_name ,   alert.iserial, alert.subject , alert.inventoryitemid   ";
        $query  .= "  from   allowancealert alert   ";
        $query  .= "  left join ait_account a on a.id = alert.customeraccountid   ";
        $query  .= "  left join ait_trafficallowance ta on ta.idcrypt = alert.taidcrypt   ";
        $query  .= "  left join plan p on p.idcrypt = alert.planidcrypt  ";
        $query  .= "  left join plandesc pd on pd.plan_id = p.id  and pd.lang_code = 'ENG' ";
        $query  .= "  WHERE alert.customeraccountid = $_accountid  and alert.deleted = 0 and alert.deleted = 0  ";
        
        if($_search !=""){
            $query .=" AND (  ";
            $query .=" alert.iserial like '%$_search%'  ";
            $query .=" a.name like '%$_search%'  "; 
            $query .=" )  ";
        }
        
        $query .=" order by $_sortCol  $_sortDir ";
        $query .=" limit $_limit1, $_limit2 ";    
          
        $rows = $this->selectRows($query);
        if(!$rows){return false;}
        $total = count($rows);
        if($total ==0){return false;}
        $aRecord = array();
        foreach($rows as $row){
            array_push($aRecord, $this->getEntityByRow($row));
        }
        return $aRecord;
    }
    
    public function getListByAccountidTotal($_accountid, $_search){
        
        /*
        $query  = "select count(*) as total from ";
        $query .=" allowancealert a  ";
        $query .=" where a.customeraccountid = $_accountid ";
        if($_search !=""){
            $query .=" AND (  ";
            $query .=" a.source like '%$_search%'  ";
            $query .=" OR a.iserial like '%$_search%'  ";
            $query .=" OR a.emails like '%$_search%'  ";
            $query .=" OR a.subject like '%$_search%'  ";
            $query .=" OR a.mobilenumbers like '%$_search%'  ";
            $query .=" )  ";
        }
         * 
         */
        
        $query  = " select count(*) as total ";
        $query  .= " from  ";
        $query  .= " allowancealert alert "; 
        $query  .= " left join ait_account a on a.id = alert.customeraccountid ";
        $query  .= " WHERE alert.customeraccountid = $_accountid  and alert.deleted = 0   "; 
        if($_search !=""){
            $query .=" AND (  ";
            $query .=" alert.iserial like '%$_search%'  ";
            $query .=" a.name like '%$_search%'  "; 
            $query .=" )  ";
        }
        
        
        return intval( $this->selectOneColumn("total", $query)  );
    }
    
}
