<?php

class Dto_InvoiceToCustomer extends Dto_Document {
    
    public function setInvoicetcDispatched( $id ){
        $query = "update invoicetocustomer set dispatched = 1 where id = $id";
        $this->executeUpdate($query);
    }
    
    
    public function getDispatchedServices($_invoiceid){
        
        $query = "select d.* , i.sku, i.productname, i.iserial , pd.name  ";
        $query .=" from invoicetc_dispatchservice d ";
        $query .=" left join inventoryitem i on i.id = d.simcarditemid "; 
        $query .=" where d.invoicetcid = $_invoiceid ";
        $_rows = $this->selectRows($query);
        
        $_total = count($_rows);
        if($_total == 0){ return false;}
        
        $aRecord = array();
        
        foreach($_rows as $row){
            
            $e = new InvoicetcDispatchService();
            $e->invoicetcid = $_invoiceid;
            $e->simcarditemid = $row["simcarditemid"];
            $e->planforaccount = $row["plaforaccount"];
            $e->planforprovider = $row["planforprovider"];
            $e->activationdate = $row["activationdate"];
            $e->activationtime = $row["activationtime"];
            $e->deviceitemid = $row["deviceitemid"];
            $e->description= $row["description"];
            
            $e->productname = $row["productname"];
            $e->sku = $row["sku"];
            $e->simcardserial = $row["iserial"];
            
            
            array_push($aRecord , $e);
        }
        
        return $aRecord;
        
    }
    
    public function getDispatchedDevices($_invoiceid){
        
        
        $query = "select d.* , i.sku, i.productname, i.iserial from invoicetc_dispatchdevice d left join inventoryitem i on i.id = d.simcarditemid";
        $query .=" where d.invoicetcid = $_invoiceid ";
        $_rows = $this->selectRows($query);
        
        $_total = count($_rows);
        if($_total == 0){ return false;}
        
        $aRecord = array();
        
        foreach($_rows as $row){
            
            $e = new InvoicetcDispatchDevice();
            $e->invoicetcid = $_invoiceid; 
            $e->deviceitemid = $row["deviceitemid"];
            $e->description= $row["description"];
            
            $e->productname = $row["productname"];
            $e->sku = $row["sku"];
            $e->deviceserial= $row["iserial"];
            
            
            array_push($aRecord , $e);
        }
        
        return $aRecord;
        
    }
    
    public function updateInventoryItem($aItem) {

        /*
          $item = new InventoryItem();
          $item->id = intval($service["simcarditemid"]);
          $item->customerinvoiceid = $tempInvoice->id;
          $item->inventorystatus = 2;
          $item->operationdate = $service["activationdate"];
          $item->operationtime = $service["activationtime"];
          $item->statusdescription = $service["operationdesc"];
         */
        $_total = count($aItem);
        if ($_total > 0) {
            foreach ($aItem as $item) {

                $_invoiceid = intval($item->customerinvoiceid);
                $_inventoryitemid = intval($item->id);

                $_inventorystatus = intval($item->inventorystatus);
                $_description = addslashes($item->statusdescription);

                $_atsimcardid = intval($item->atsimcardid);
                $_atstatus = intval($item->atstatus);
                $_statusdate = date('Y-m-d');
                $_actionuserid = $item->actionuserid;

                $query = "update inventoryitem set inventorystatus = $_inventorystatus, statusdate = '$_statusdate' , statusdescription = '$_description' , ";
                $query .= " customerinvoiceid = $_invoiceid , actionuserid = '$_actionuserid' , actiondatetime = CURRENT_TIMESTAMP()  ";
                if ($_atsimcardid > 0) {
                    $query .= " , atsimcardid = $_atsimcardid , atstatus = $_atstatus ";
                }
                $query .= "where id = $_inventoryitemid ";

                $this->executeUpdate($query);

                $dto_inventory = new Dto_Inventory();
                $dto_inventory->updateInventoryStatusHistory($_inventoryitemid, $_inventorystatus, $_statusdate, $_description, $_invoiceid, $_atsimcardid, $_atstatus, $_actionuserid);
            }//foreach
        }
    }

//function

    public function insertInvoiceDispatchServices($provideraccountid, $customeraccountid, $aItem) {
        /*
          create table invoicetc_dispatchservice(
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `invoicetcid` varchar(200) DEFAULT NULL,
          `simcarditemid` int(11) NOT NULL,
          `simcardserial` varchar(200) DEFAULT NULL,
          `planforaccount` varchar(200) DEFAULT NULL,
          `planforprovider` varchar(200) DEFAULT NULL,
          activationdate date default null,
          activationtime time default null,
          description TEXT default null,
          `deviceitemid` int(11) DEFAULT 0,
          `deviceserial` varchar(200) DEFAULT NULL,
          userid varchar(200) default null,
          operationdatetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
          primary key(id)
          );

         *          */

        $_total = count($aItem);
        if ($_total > 0) {

            $query = "insert into invoicetc_dispatchservice (invoicetcid, simcarditemid, planforaccount, planforprovider, activationdate,  activationtime, description,  deviceitemid,  userid,  operationdatetime ) values ";

            for ($i = 0; $i < $_total; $i++) {
                $item = $aItem[$i];

                $invoicetcid = $item->invoicetcid;
                $simcarditemid = $item->simcarditemid;


                $planforaccount = $item->planforaccount;
                $planforprovider = $item->planforprovider;
                $description = addslashes($item->description);
                $deviceitemid = $item->deviceitemid;
                $userid = $item->userid;

                $activationdate = $item->activationdate;
                $activationtime = $item->activationtime;



                $query .= " ( '$invoicetcid' , $simcarditemid , '$planforaccount', '$planforprovider', '$activationdate',  '$activationtime',  '$description' , $deviceitemid, '$userid', CURRENT_TIMESTAMP()  )";
                if (($i + 1) < $_total) {
                    $query .= " ,  ";
                }
                
                
                //we need to create the airtime operation order
                //----------------------------------------------
                $id =  date('YmdHis') . '' . rand(10000, 99999) . '' . rand(10000, 99999);
                $dto_inventory = new Dto_Inventory();
                $dto_plan = new Dto_Plan();

                $inventoryitem = $dto_inventory->getItemByid($item->simcarditemid);   
                $plan = $dto_plan->getPlanById($planforprovider, "ENG");  
                $plancustomer = $dto_plan->getPlanById($planforaccount, "ENG");  

                $queryAO = "insert into airtimeoperation (id, inventoryitemid, planid, accountid, accountassignedid, iserial, productname, productid, planname, ";
                $queryAO .=" subscription_period, contract_period, activation_price, fixed_price, subscription_price,  datetoexecute, timetoexecute,  subtotal, taxes, paymentmethod, paymentmethod_price, totalpayment,  ";
                $queryAO .=" operation,  operationstatus, requestedbyuserid, requesteddatetime, networkcode, servicetype_code, requeststatus, suspension_price , planidcryptforcustomer , plannameforcustomer ) values ( ";
                
                $_subtotal = doubleval($plan->activationpayment) + doubleval($plan->fixedpayment ) + doubleval( $plan->price );
                
                $queryAO .=" '$id',   '" . $item->simcarditemid . "'   , '" .$planforprovider . "' , " .$provideraccountid. "  ,  " . $customeraccountid . "  ,  ";
                $queryAO .=" '" . $inventoryitem->iserial . "' , '" . $inventoryitem->productname . "' , '" . $inventoryitem->productid . "' , '" . $plan->name . "' ,  ";
                $queryAO .=" '" . $plan->payment_period_code . "' , '" . $plan->contract_period_code . "' , " . $plan->activationpayment . " , " . $plan->fixedpayment . " , " . $plan->price . "  ,  ";
                $queryAO .=" '" . $activationdate . "' ,  '" . $activationtime . "' , $_subtotal , 0 , NULL ,  0 , 0 ,   ";
                $queryAO .=" 'activate' ,  0 , '" . $userid. "' , '" . date('Y-m-d h:i:s') . "' , '" . $plan->networkcode . "'  , '" . $plan->servicetype_code . "' , 'requested' , $plan->suspensionpayment , ";
                $queryAO .=" '" .$planforaccount . "' , '" . $plancustomer->name . "'  ";
                $queryAO .=' ) ';

                $this->executeUpdate($queryAO);
                //----------------------------------------------
               
            }
        
            return $this->executeUpdate($query);
        }
        
        return false;
    }

//function

    public function insertInvoiceDispatchDevices($aItem) {
        /*
          create table invoicetc_dispatchdevice(
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `invoicetcid` varchar(200) DEFAULT NULL,
          `deviceitemid` int(11) DEFAULT 0,
          `deviceserial` varchar(200) DEFAULT NULL,
          description TEXT default null,
          userid varchar(200) default null,
          operationdatetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
          primary key(id)
          );
         *          */


        $_total = count($aItem);
        if ($_total > 0) {

            $query = "insert into invoicetc_dispatchdevice (invoicetcid,   description,  deviceitemid,  userid,  operationdatetime ) values ";

            for ($i = 0; $i < $_total; $i++) {
                $item = $aItem[$i];

                $invoicetcid = $item->invoicetcid;

                $description = addslashes($item->description);
                $deviceitemid = $item->deviceitemid;
                $userid = $item->userid;


                $query .= " ( '$invoicetcid' ,  '$description' , $deviceitemid, '$userid', CURRENT_TIMESTAMP()  )";
                if (($i + 1) < $_total) {
                    $query .= " ,  ";
                }
            }

            return $this->executeUpdate($query);
        }
        return false;
    }

//function

    public function getSimcardAssocToService($_accountid, $_customeraccountid , $_invoiceid, $_planidcrypt, $_parentplanidcrypt) {

        $query = "select   p.idcrypt as productid, it.id as inventoryitemid, it.iserial  , pdesc.name , 'noactive' as servicestatus , 'noexpiry' as expirydate  ";
        $query .= "from inventoryitem  it  ";
        $query .= "right join product p on p.idcrypt = it.productid ";
        $query .= "right join productplan pp on pp.product_id = p.id ";
        $query .= "right join productdesc pdesc on pdesc.product_id = p.id  and pdesc.lang_code = 'ENG' ";
        $query .= "where pp.deleted = 0 and p.issimcard = 1 and it.inventorystatus = 1  and it.accountid = $_accountid ";
        $query .= "and pp.plan_id in  ";
        $query .= "(   ";
        $query .= "select id from plan  ";
        $query .= "where  ";
        $query .= "idcrypt = '$_planidcrypt'  ";
        $query .= "or  ";
        $query .= "idcrypt = '$_parentplanidcrypt'  ";
        $query .= " ) ";
 

        $query .= " UNION ";

        $query .= " ( select it.productid, it.id as inventoryitemid,  it.iserial , it.productname  , it.operationregistered, it.expirydate ";
        $query .= " from inventoryitem it";
        $query .= " right join airtimeoperation ao on ao.inventoryitemid = it.id";
        $query .= " where ao.requeststatus = 'executed' and ao.operation = 'activate'";
        $query .= " and ao.accountid = $_accountid and ao.accountassignedid = $_customeraccountid ";
        $query .= " and it.operationregistered = 'activate' and it.operationrequested = 'executed' )";
        
      
        
        $_rows = $this->selectRows($query);
        $_total = count($_rows);


        $aRecord = array();
        if ($_total > 0) {
            foreach ($_rows as $_row) {
                $e = new InventoryItemSimple();
                $e->id = $_row["inventoryitemid"];
                $e->iserial = $_row["iserial"];
                $e->name = $_row["name"];
                $e->servicestatus = $_row["servicestatus"];
                $e->expirydate    = $_row["expirydate"];
                array_push($aRecord, $e);
            }
        }
        return $aRecord;
    }

    public function getDevicesAssocToService($_accountid, $_invoiceid, $_planidcrypt, $_parentplanidcrypt) {

        $query = "select   p.idcrypt as productid, it.id as inventoryitemid, it.iserial  , pdesc.name  ";
        $query .= "from inventoryitem  it  ";
        $query .= "right join product p on p.idcrypt = it.productid ";
        $query .= "right join productplan pp on pp.product_id = p.id ";
        $query .= "right join productdesc pdesc on pdesc.product_id = p.id  and pdesc.lang_code = 'ENG' ";
        $query .= "where pp.deleted = 0 and p.issimcard = 0 and it.inventorystatus = 1  and it.accountid = $_accountid ";
        $query .= "and pp.plan_id in  ";
        $query .= "(   ";
        $query .= "select id from plan  ";
        $query .= "where  ";
        $query .= "idcrypt = '$_planidcrypt'  ";
        $query .= "or  ";
        $query .= "idcrypt = '$_parentplanidcrypt'  ";
        $query .= " ) ";
        $query .= " and p.idcrypt in ( select idcrypt from documentproduct where doctype = 'invoicetc' and docidcrypt = '$_invoiceid')  ";

        $_rows = $this->selectRows($query);
        $_total = count($_rows);


        $aRecord = array();
        if ($_total > 0) {
            foreach ($_rows as $_row) {
                $e = new InventoryItemSimple();
                $e->id = $_row["inventoryitemid"];
                $e->iserial = $_row["iserial"];
                $e->name = $_row["name"];
                $e->productidcrypt = trim($_row["productid"]);
                array_push($aRecord, $e);
            }
        }
        return $aRecord;
    }

    public function getDevicesByProductId($_accountid, $_invoiceid, $_productidcrypt) {

        $query = "select   p.idcrypt as productid, it.id as inventoryitemid, it.iserial  , pdesc.name  ";
        $query .= "from inventoryitem  it  ";
        $query .= "right join product p on p.idcrypt = it.productid ";
        $query .= "right join productdesc pdesc on pdesc.product_id = p.id  and pdesc.lang_code = 'ENG' ";
        $query .= "where  p.issimcard = 0 and it.inventorystatus = 1  and it.accountid = $_accountid ";
        $query .= "and p.idcrypt  = '$_productidcrypt'  ";


        $_rows = $this->selectRows($query);
        $_total = count($_rows);


        $aRecord = array();
        if ($_total > 0) {
            foreach ($_rows as $_row) {
                $e = new InventoryItemSimple();
                $e->id = $_row["inventoryitemid"];
                $e->iserial = $_row["iserial"];
                $e->name = $_row["name"];
                $e->productidcrypt = trim($_row["productid"]);
                array_push($aRecord, $e);
            }
        }
        return $aRecord;
    }

    public function delete($_idcrypt, $_userid) {
        $query = "update invoicetocustomer set deleted = 1 , deletedby = '$_userid' , deleteddate = CURRENT_TIMESTAMP() where idcrypt =  '$_idcrypt'  ";

        return $this->executeUpdate($query);
    }

    public function getList($_accountid, $_search, $_orderby, $_orderdir, $limit1, $limit2) {

        $query = " select q.* , ";
        $query .= " ac.name as customer_name, concat(con.firstname,' ', con.lastname) as contact_names,   ";
        $query .= " concat(asi.firstname,' ', asi.lastname) as assigned_names  ";
        $query .= " from invoicetocustomer q ";
        $query .= " left join ait_account ac on ac.id = q.customerid ";
        $query .= " left join ait_usersystem con on con.id = q.contactid ";
        $query .= " left join ait_usersystem asi on asi.id = q.assignedto ";
        $query .= " WHERE q.accountid = " . $_accountid . " and q.deleted = 0  ";
        if ($_search != "") {
            $query .= "  AND  (  ";
            $query .= "  q.subject like '%$_search' ";
            $query .= " ) ";
        }

        $query .= " order by $_orderby $_orderdir limit $limit1 , $limit2  ";


        $_rows = $this->selectRows($query);
        if (!$_rows) {
            return false;
        }
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        foreach ($_rows as $_row) {
            $_e = $this->getByRow($_row);
            array_push($aRecord, $_e);
        }
        return $aRecord;
    }

    public function getListTotal($_accountid, $_search) {

        $query = " select count(*) as total ";
        $query .= " from invoicetocustomer q ";
        $query .= " left join ait_account ac on ac.id = q.customerid ";
        $query .= " left join ait_usersystem con on con.id = q.contactid ";
        $query .= " left join ait_usersystem asi on asi.id = q.assignedto ";
        $query .= " WHERE q.accountid = " . $_accountid . "  and q.deleted = 0  ";

        if ($_search != "") {
            $query .= "  AND  (  ";
            $query .= "  q.subject like '%$_search' ";
            $query .= " ) ";
        }

        return intval($this->selectOneColumn("total", $query));
    }

    public function getByIdCrypt($_idcrypt) {
        $query = " select q.* , ";
        $query .= " ac.name as customer_name, concat(con.firstname,' ', con.lastname) as contact_names,   ";
        $query .= " concat(asi.firstname,' ', asi.lastname) as assigned_names , k.quoteno, k.subject as quote_subject  ";
        $query .= " from invoicetocustomer q ";
        $query .= " left join quote k on k.idcrypt = q.quoteidcrypt ";
        $query .= " left join ait_account ac on ac.id = q.customerid ";
        $query .= " left join ait_usersystem con on con.id = q.contactid ";
        $query .= " left join ait_usersystem asi on asi.id = q.assignedto ";
        $query .= " where q.idcrypt = '$_idcrypt' ";

        $_row = $this->selectOneRow($query);
        return $this->getByRow($_row);
    }

    public function getByRow($_row) {
        $_e = new InvoiceToCustomer();


        $_e->id = intval($_row["id"]);
        $_e->dispatched = intval($_row["dispatched"]);
        $_e->idcrypt = trim($_row["idcrypt"]);
        $_e->quoteidcrypt = trim($_row["quoteidcrypt"]);

        $_e->quotedesc = stripslashes($_row["quoteno"]) . " - " . stripslashes($_row["quote_subject"]);
        $_e->invoiceno = trim($_row["invoiceno"]);
        $_e->accountid = intval($_row["accountid"]);
        $_e->subject = stripslashes($_row["subject"]);
        $_e->status = stripslashes($_row["status"]);
        $_e->invoicedate = stripslashes($_row["invoicedate"]);
        $_e->invoiceduedate = stripslashes($_row["invoiceduedate"]);
        $_e->assignedto = stripslashes($_row["assignedto"]);
        $_e->customerid = intval($_row["customerid"]);
        $_e->contactid = stripslashes($_row["contactid"]);
        $_e->contacttelephone = stripslashes($_row["contacttelephone"]);
        $_e->contactemail = stripslashes($_row["contactemail"]);
        $_e->contactmobile = stripslashes($_row["contactmobile"]);
        $_e->contactsalutation = stripslashes($_row["contactsalutation"]);
        $_e->paymentterms = stripslashes($_row["paymentterms"]);
        $_e->paymenttermsother = stripslashes($_row["paymenttermsother"]);
        $_e->carriercompany = stripslashes($_row["carriercompany"]);
        $_e->carriercompanyother = stripslashes($_row["carriercompanyother"]);
        $_e->carriertracking = stripslashes($_row["carriertracking"]);
        $_e->operationtype = stripslashes($_row["operationtype"]);
        $_e->obs = stripslashes($_row["obs"]);
        $_e->shippingenabled = stripslashes($_row["shippingenabled"]);
        $_e->discount = doubleval($_row["discount"]);
        $_e->hastaxes = intval($_row["hastaxes"]);
        $_e->subtotal = doubleval($_row["subtotal"]);
        $_e->shippingcost = doubleval($_row["shippingcost"]);
        $_e->taxespercent = doubleval($_row["taxespercent"]);
        $_e->total = doubleval($_row["total"]);

        $_e->deliveryterms = stripslashes($_row["deliveryterms"]);
        $_e->deliverytermsother = stripslashes($_row["deliverytermsother"]);
        
        
        $_e->purchaseorderno = stripslashes($_row["purchaseorderno"]);

        $_e->bill_country = stripslashes($_row["bill_country"]);
        $_e->bill_state = stripslashes($_row["bill_state"]);
        $_e->bill_city = stripslashes($_row["bill_city"]);
        $_e->bill_street = stripslashes($_row["bill_street"]);
        $_e->bill_zipcode = stripslashes($_row["bill_zipcode"]);
        $_e->bill_pobox = stripslashes($_row["bill_pobox"]);

        $_e->ship_country = stripslashes($_row["ship_country"]);
        $_e->ship_state = stripslashes($_row["ship_state"]);
        $_e->ship_city = stripslashes($_row["ship_city"]);
        $_e->ship_street = stripslashes($_row["ship_street"]);
        $_e->ship_zipcode = stripslashes($_row["ship_zipcode"]);
        $_e->ship_pobox = stripslashes($_row["ship_pobox"]);

        $_e->contact_names = stripslashes($_row["contact_names"]);
        $_e->assigned_names = stripslashes($_row["assigned_names"]);
        $_e->customer_name = stripslashes($_row["customer_name"]);

        $_e->currency_code =stripslashes($_row["currency_code"]);
        $_e->currency_exchange = doubleval($_row["currency_exchange"]);
        $_e->totalusd = doubleval($_row["totalusd"]);

        return $_e;
    }

    public function register($_e) {

        $_accountid = $_e->accountid;

        $_quoteidcrypt = $_e->quoteidcrypt;
        $_subject = addslashes($_e->subject);
        $_status = addslashes($_e->status);
        $_invoicedate = addslashes($_e->invoicedate);
        $_invoiceduedate = addslashes($_e->invoiceduedate);
        $_assignedto = addslashes($_e->assignedto);
        $_customerid = intval($_e->customerid);
        $_contactid = addslashes($_e->contactid);
        $_contacttelephone = addslashes($_e->contacttelephone);
        $_contactmobile = addslashes($_e->contactmobile);
        $_contactemail = addslashes($_e->contactemail);
        $_contactsalutation = addslashes($_e->contactsalutation);
        $_paymentterms = addslashes($_e->paymentterms);
        $_paymenttermsother = addslashes($_e->paymenttermsother);
        $_carriercompany = addslashes($_e->carriercompany);
        $_carriercompanyother = addslashes($_e->carriercompanyother);
        $_carriertracking = addslashes($_e->carriertracking);
        $_operationtype = addslashes($_e->operationtype);
        $_obs = addslashes($_e->obs);
        $_shippingenabled = addslashes($_e->shippingenabled);
        $_discount = doubleval($_e->discount);
        $_hastaxes = doubleval($_e->hastaxes);
        $_subtotal = doubleval($_e->subtotal);
        $_shippingcost = doubleval($_e->shippingcost);
        $_taxespercent = doubleval($_e->taxespercent);
        $_total = doubleval($_e->total);

        $_deliveryterms = addslashes($_e->deliveryterms);
        $_deliverytermsother = addslashes($_e->deliverytermsother);
        
        
        $_purchaseorderno = addslashes($_e->purchaseorderno);

        $_bill_country = addslashes($_e->bill_country);
        $_bill_state = addslashes($_e->bill_state);
        $_bill_city = addslashes($_e->bill_city);
        $_bill_street = addslashes($_e->bill_street);
        $_bill_zipcode = addslashes($_e->bill_zipcode);
        $_bill_pobox = addslashes($_e->bill_pobox);

        $_ship_country = addslashes($_e->ship_country);
        $_ship_state = addslashes($_e->ship_state);
        $_ship_city = addslashes($_e->ship_city);
        $_ship_street = addslashes($_e->ship_street);
        $_ship_zipcode = addslashes($_e->ship_zipcode);
        $_ship_pobox = addslashes($_e->ship_pobox);

        $_createdby = trim($_e->createdby);

        $_currency_exchange = doubleval($_e->currency_exchange);        
        $_currency_code = trim($_e->currency_code);          
        $_totalusd = (1/$_currency_exchange)*$_total;
        
        
        $_prefix = $this->selectOneColumn("invoiceprefix", "select invoiceprefix from ait_account where id = $_accountid ");
        $_invoiceno = $this->generateInvoiceNo($_prefix, $_e->accountid);

        $query = "insert into invoicetocustomer ( ";
        $query .= " idcrypt, quoteidcrypt,  accountid, invoiceno, subject, status, invoicedate, invoiceduedate, assignedto, carriertracking, purchaseorderno,     ";
        $query .= " customerid, contactid, contacttelephone, contactmobile, contactemail, contactsalutation,    ";
        $query .= " paymentterms, paymenttermsother, carriercompany, carriercompanyother,     ";
        $query .= " operationtype, shippingenabled, obs,      ";
        $query .= " discount, hastaxes, subtotal, shippingcost, taxespercent, total,     ";
        $query .= " bill_country, bill_state, bill_city, bill_street, bill_zipcode, bill_pobox,      ";
        $query .= " ship_country, ship_state, ship_city, ship_street, ship_zipcode, ship_pobox,      ";
        $query .= " createdby, createddate , deliveryterms, deliverytermsother , currency_code, currency_exchange , totalusd )  values  (   ";

        $query .= " '$_e->idcrypt' ,  '$_quoteidcrypt'   ,  " . $_e->accountid . ", '$_invoiceno', '$_subject',  '$_status' , '$_invoicedate' , '$_invoiceduedate' , '$_assignedto' , '$_carriertracking', '$_purchaseorderno' ,  ";
        $query .= " $_customerid , '$_contactid' , '$_contacttelephone' , '$_contactmobile' , '$_contactemail' , '$_contactsalutation' , ";
        $query .= " '$_paymentterms' , '$_paymenttermsother' , '$_carriercompany' , '$_carriercompanyother' ,  ";
        $query .= " '$_operationtype' , '$_shippingenabled' , '$_obs' ,   ";
        $query .= " $_discount , $_hastaxes , $_subtotal , $_shippingcost , $_taxespercent , $_total ,  ";
        $query .= " '$_bill_country' , '$_bill_state' , '$_bill_city' , '$_bill_street' , '$_bill_zipcode' , '$_bill_pobox' ,  ";
        $query .= " '$_ship_country' , '$_ship_state' , '$_ship_city' , '$_ship_street' , '$_ship_zipcode' , '$_ship_pobox' ,  ";
        $query .= "  '$_createdby' , CURRENT_TIMESTAMP() , '$_deliveryterms' , '$_deliverytermsother' , '$_currency_code', $_currency_exchange , $_totalusd  ) ";

        $_result = $this->executeUpdate($query);

        return $_result;
    }

    public function update($_e) {

        $_idcrypt = $_e->idcrypt;
        $_quoteidcrypt = $_e->quoteidcrypt;

        $_accountid = $_e->accountid;
        $_subject = addslashes($_e->subject);
        $_status = addslashes($_e->status);
        $_invoicedate = addslashes($_e->invoicedate);
        $_invoiceduedate = addslashes($_e->invoiceduedate);
        $_assignedto = addslashes($_e->assignedto);
        $_customerid = intval($_e->customerid);
        $_contactid = addslashes($_e->contactid);
        $_contacttelephone = addslashes($_e->contacttelephone);
        $_contactmobile = addslashes($_e->contactmobile);
        $_contactemail = addslashes($_e->contactemail);
        $_contactsalutation = addslashes($_e->contactsalutation);
        $_paymentterms = addslashes($_e->paymentterms);
        $_paymenttermsother = addslashes($_e->paymenttermsother);
        $_carriercompany = addslashes($_e->carriercompany);
        $_carriercompanyother = addslashes($_e->carriercompanyother);
        $_carriertracking = addslashes($_e->carriertracking);
        $_operationtype = addslashes($_e->operationtype);
        $_obs = addslashes($_e->obs);
        $_shippingenabled = addslashes($_e->shippingenabled);
        $_discount = doubleval($_e->discount);
        $_hastaxes = doubleval($_e->hastaxes);
        $_subtotal = doubleval($_e->subtotal);
        $_shippingcost = doubleval($_e->shippingcost);
        $_taxespercent = doubleval($_e->taxespercent);
        $_total = doubleval($_e->total);

        $_deliveryterms = addslashes($_e->deliveryterms);
        $_deliverytermsother = addslashes($_e->deliverytermsother);
        
        
        $_purchaseorderno = addslashes($_e->purchaseorderno);

        $_bill_country = addslashes($_e->bill_country);
        $_bill_state = addslashes($_e->bill_state);
        $_bill_city = addslashes($_e->bill_city);
        $_bill_street = addslashes($_e->bill_street);
        $_bill_zipcode = addslashes($_e->bill_zipcode);
        $_bill_pobox = addslashes($_e->bill_pobox);

        $_ship_country = addslashes($_e->ship_country);
        $_ship_state = addslashes($_e->ship_state);
        $_ship_city = addslashes($_e->ship_city);
        $_ship_street = addslashes($_e->ship_street);
        $_ship_zipcode = addslashes($_e->ship_zipcode);
        $_ship_pobox = addslashes($_e->ship_pobox);

        $_modifiedby = trim($_e->modifiedby);

        $_currency_exchange = doubleval($_e->currency_exchange);        
        $_currency_code = trim($_e->currency_code);               
        $_totalusd = (1/$_currency_exchange)*$_total;        
        
        $query = " update invoicetocustomer set quoteidcrypt = '$_quoteidcrypt' , purchaseorderno = '$_purchaseorderno' ,  ";
        $query .= " accountid = $_accountid ,   subject = '$_subject' , invoicedate = '$_invoicedate' , invoiceduedate = '$_invoiceduedate', assignedto = '$_assignedto' ,  ";
        $query .= " customerid = $_customerid , contactid = '$_contactid', contacttelephone = '$_contacttelephone' , contactmobile = '$_contactmobile' , contactemail = '$_contactemail' , contactsalutation = '$_contactsalutation' ,  ";
        $query .= " paymentterms = '$_paymentterms' , paymenttermsother = '$_paymenttermsother' , carriercompany = '$_carriercompany' , carriercompanyother = '$_carriercompanyother' , carriertracking = '$_carriertracking'  ,   ";
        $query .= " operationtype = '$_operationtype' , shippingenabled = '$_shippingenabled' , obs = '$_obs' , status = '$_status' , ";
        $query .= " discount = $_discount , hastaxes = $_hastaxes , subtotal = $_subtotal , shippingcost = $_shippingcost , taxespercent = $_taxespercent , total = $_total,  ";
        $query .= " bill_country = '$_bill_country' , bill_state = '$_bill_state' , bill_city = '$_bill_city', bill_street = '$_bill_street' , bill_zipcode = '$_bill_zipcode' , bill_pobox = '$_bill_pobox' ,  ";
        $query .= " ship_country = '$_ship_country' , ship_state = '$_ship_state' , ship_city = '$_ship_city', ship_street = '$_ship_street' , ship_zipcode = '$_ship_zipcode' , ship_pobox = '$_ship_pobox' ,  ";
        $query .= " modifiedby = '$_modifiedby' , modifieddate = CURRENT_TIMESTAMP() , deliveryterms = '$_deliveryterms' , deliverytermsother = '$_deliverytermsother' ,  ";
        $query .= " currency_code= '$_currency_code', currency_exchange = $_currency_exchange , totalusd = $_totalusd ";
        $query .= " where idcrypt = '$_idcrypt' ";

        $_result = $this->executeUpdate($query);

        return $_result;
    }

    public function generateInvoiceNo($prefix, $_accountid) {
        
        $query = "select count(*) as total from invoicetocustomer where accountid = $_accountid ";
        $_total = $this->selectOneColumn("total", $query);

        $_invoiceno = ($_total + 1);
        while (strlen($_invoiceno) < 6) {
            $_invoiceno = '0' . $_invoiceno;
        }
        return $prefix.''.$_invoiceno;
    }

    public function changeStatusOfOtherDocuments($_e, $_userid) {
        //$dto_quote = new Dto_Quote();
        //$dto_quote->updateStatus($_quoteidcrypt, $_status);
        $dto_quote = new Dto_Quote();
        $_quoteidcrypt = $dto_quote->getIdcryptByInvoiceId($_e->idcrypt);

        if ($_quoteidcrypt != $_e->quoteidcrypt || $_e->quoteidcrypt == "") {
            $_laststatus = $dto_quote->getPreLastStatus($_quoteidcrypt, "quote");
            $dto_quote->unsetQuoteApproved($_laststatus, $_e->idcrypt);
            $dto_quote->setStatus($_quoteidcrypt, "quote", $_laststatus, $_userid);
        }

        if ($_e->quoteidcrypt != "") {
            //we need to update the quote status
            $dto_quote = new Dto_Quote();
            $dto_quote->setQuoteApproved($_e->quoteidcrypt, "A", $_e->idcrypt);
            $dto_quote->setStatus($_e->quoteidcrypt, "quote", "A", $_userid);
        }
    }

}
