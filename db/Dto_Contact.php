<?php
class Dto_Account extends Dto_Db {
    
    public function updateStyleColor($_accountid, $_stylecolor){
        $query = "update ait_account set stylecolor = '$_stylecolor' where id = $_accountid";
        return $this->executeUpdate($query);
    }
    
    
    public function getLogoBlobByAccountid($_accountid){
        $query  = "SELECT  mb.original_blob  ";
        $query .= "FROM media  m  ";
        $query .= "RIGHT JOIN mediablob mb on mb.media_id = m.id ";
        $query .= "where m.entity_table = 'ACCOUNT' AND m.entity_id = $_accountid AND m.ismain = 1 and m.type = 'IMAGE' ";
        
        $row = $this->selectOneRow($query);
        return $row["original_blob"]; 
    }
    
    public function getAccountBySubdomain($_subdomain){
        $query = "select id from ait_account where subdomain = '$_subdomain' and deleted = 0  ";
        return $this->selectOneColumn("id", $query);
    }
    
    
    public function getLogoIdByAccountid($_accountid){
        $query  = "SELECT  m.idcrypt  ";
        $query .= "FROM media  m  ";
        $query .= "where m.entity_table = 'ACCOUNT' AND m.entity_id = $_accountid AND m.ismain = 1 and m.type = 'IMAGE' ";
       
        $row = $this->selectOneRow($query);
        return trim($row["idcrypt"]); 
    }    
    
    public function setLogoByAccountid($accountid, $original_blob, $fileext, $filename){
        
        $mediaCryptId = $this->getLogoIdByAccountid($accountid);
        $ok = false;
        if($mediaCryptId == ""){
            $mediaid = md5(date('Ymdhis').''.rand(10000,99999));
            $queryInsert = "insert into media (idcrypt, entity_table, entity_id, ismain, type) values  ";
            $queryInsert .="( '$mediaid' , 'ACCOUNT' , $accountid, 1, 'IMAGE' ) ";
            
            $resultInsert = $this->executeUpdate($queryInsert);
            
            if($resultInsert){
                
                $id = intval( $this->selectOneColumn("id", "select id from media where idcrypt =  '$mediaid'  ") );
                
                $query = "insert into mediablob (media_id, original_blob, fileext, filename) values( $id , '$original_blob' , '$fileext' , '$filename')  ";
             
                $ok = $this->executeUpdate($query);
            }
        }else{
            $id = intval( $this->selectOneColumn("id", "select id from media where idcrypt =  '$mediaCryptId'  ") );
            $query = "update mediablob set original_blob = '$original_blob' , fileext = '$fileext' , filename = '$filename' where media_id =  $id  ";
            $ok = $this->executeUpdate($query);
        }
        return $ok;
    }
    
    
    public function setcreditterms($_userid , $_accountid, $_aCreditTerms){
        $total = count($_aCreditTerms);
        if($total == 0) {return false;}
        $query = " update account_creditterms set status = 0 , updateddate = CURRENT_TIMESTAMP() , updatedbyuser = $_userid  ";
        $query .=" where accountid = $_accountid and status = 1  ";
        $result = $this->executeUpdate($query);

        $insert_result = false;
        if($result){
            $query2 = "insert into account_creditterms ( accountid, credittermsid, registerbyuser, registerdate, status ) values  ";        
            for( $i=0 ; $i<$total; $i++  ){
                $credittermsid = $_aCreditTerms[$i];
                $query2 .=" ( $_accountid,   $credittermsid , $_userid, CURRENT_TIMESTAMP(), 1  )  ";
                if( ($i+1) < $total){
                    $query2 .=" , ";
                }
            }
            $insert_result = $this->executeUpdate($query2);
        }
        return $insert_result;
    }
     
    public function getCreditTerms(){
        $query = "select * from creditterms";        
        $rows = $this->selectRows($query);
        if(!$rows){return false;}
        $total = count($rows);
        if($total == 0){ return false; }
        $aRecord = array();
        foreach($rows as $row){
            $entity = new CreditTerms();
            $entity->id = $row["id"];
            $entity->days = $row["days"];
            array_push($aRecord, $entity);
        }
        return $aRecord;
    }
    
    public function getCreditTermsByAccount($_accountid){
        $query  = "select acct.id, acct.days from accountcreditterms acct ";
        $query .= "right join creditterms ct on ct.id = acct.credittermsid  ";
        $query .= " where acct.accountid = $_accountid and acct.status = 1 order by acct.days asc ";
    }
    
    public function create($_account) {
        $_id = $_account->id;
        $_code = addslashes($_account->code);
        $_name = addslashes($_account->name);
        $_email = addslashes($_account->email);
        $_phone = addslashes($_account->phone);
        $_website = addslashes($_account->website);
        $_phoneext = addslashes($_account->phoneext);
        $_billaddressid = $_account->billaddressid;
        $_shipaddressid = $_account->shipaddressid;
        $_parentaccountid = $_account->parentaccountid;
        $_ownerid = $_account->ownerid;
        $_createdbyuser = $_account->createdbyuser;
        
        $_subdomain = $_account->subdomain;

        $_taxid = addslashes($_account->taxid);
        $_taxtypeid = addslashes($_account->taxtypeid);
        
        $query = "insert into ait_account ( id, taxid, taxtypeid,  parentaccountid, code, name,  email, website,  phone, phoneext, billaddressid, shipaddressid , subdomain, ownerid, createddatetime, createdbyuser ) values ";
        $query .=" ( $_id,  '$_taxid' , '$_taxtypeid',       $_parentaccountid,   '$_code' , '$_name' , '$_email' ,  '$_website' ,       '$_phone' , '$_phoneext' , $_billaddressid , $_shipaddressid , '$_subdomain' , '$_ownerid' , CURRENT_TIMESTAMP() , '$_createdbyuser' )   ";

        $ok = $this->executeUpdate($query);
        if($ok){
            $ok = $this->createTaxes($_id);
        }
        
        return $ok;
    }

    public function createTaxes($_accountid){
        $query = "insert into taxes (accountid, saletaxes, communicationtaxes) values ($_accountid, 6, 3.4) ";
        return $this->executeUpdate($query);
    }
    
    
    public function update($_account) {
        $_id = $_account->id;
        $_code = addslashes($_account->code);
        $_name = addslashes($_account->name);
        $_email = addslashes($_account->email);
        $_phone = addslashes($_account->phone);
        $_website = addslashes($_account->website);
        $_phoneext = addslashes($_account->phoneext);
        $_billaddressid = intval($_account->billaddressid);
        $_shipaddressid = intval($_account->shipaddressid);
        $_subdomain = addslashes($_account->subdomain);
        $_ownerid = $_account->ownerid;
        $_modifiedbyuser = $_account->modifiedbyuser;

        $_taxid = addslashes($_account->taxid);
        $_taxtypeid = addslashes($_account->taxtypeid);

        $query = "update   ait_account  set code = '$_code' , name = '$_name' ,  subdomain = '$_subdomain'  ,email = '$_email' , phone = '$_phone' , phoneext = '$_phoneext'  , billaddressid = $_billaddressid   ";
        $query .= " , taxid = '$_taxid' , taxtypeid = '$_taxtypeid' , shipaddressid = $_shipaddressid ,   website = '$_website' , modifiedbyuser = '$_modifiedbyuser' , ownerid = '$_ownerid', modifieddatetime = CURRENT_TIMESTAMP()  where id = $_id ";
        //echo $query; die();
        $ok = $this->executeUpdate($query);
        return $ok;
    }
    
    public function delete($_id){
        $query = "update ait_account set deleted = 1 where id = $_id";
        return $this->executeUpdate($query);
    }
    
    public function updateStatus($_id, $_status){
        $query = "update ait_account set status = $_status where id = $_id ";
        return $this->executeUpdate($query);
    }

    public function getById($_id) {

        $query = "select a.*    ";
        $query .=" , ba.country as bill_country, ba.state as bill_state, ba.city as bill_city , ba.street as bill_street , ba.zipcode as bill_zipcode , ba.pobox as bill_pobox   ";
        $query .=" , sa.country as ship_country, sa.state as ship_state, sa.city as ship_city , sa.street as ship_street , sa.zipcode as ship_zipcode , sa.pobox as ship_pobox   ";
        $query .="  from ait_account a   ";
        $query .="  left join ait_address ba on ba.id = a.billaddressid ";
        $query .="  left join ait_address sa on sa.id = a.shipaddressid ";
        $query .="  where a.id = $_id and a.deleted = 0 ";
         
        
        $shipaddress = new Address();
        $billaddress = new Address();
        $account = new Account();

        $row = $this->selectOneRow($query);
        if ($row) {

            $account->id = $row["id"];
            $account->code = $row["code"];
            $account->name = $row["name"];
            $account->email = $row["email"];
            $account->phone = $row["phone"];
            $account->phoneext = $row["phoneext"];
            $account->parentaccountid = $row["parentaccountid"];
            $account->billaddressid = $row["billaddressid"];
            $account->shipaddressid = $row["shipaddressid"];

            $account->website = $row["website"];
            $account->taxtypeid = $row["taxtypeid"];
            $account->taxid = $row["taxid"];
            $account->subdomain = trim($row["subdomain"]);
            $account->stylecolor = trim($row["stylecolor"]);
            $account->status = intval($row["status"]);
            
            $account->ownerid = trim($row["ownerid"]);
            
            $billaddress->country = $row["bill_country"];
            $billaddress->state = $row["bill_state"];
            $billaddress->city = $row["bill_city"];
            $billaddress->street = $row["bill_street"];
            $billaddress->zipcode = $row["bill_zipcode"];
            $billaddress->pobox = $row["bill_pobox"];

            $shipaddress->country = $row["ship_country"];
            $shipaddress->state = $row["ship_state"];
            $shipaddress->city = $row["ship_city"];
            $shipaddress->street = $row["ship_street"];
            $shipaddress->zipcode = $row["ship_zipcode"];
            $shipaddress->pobox = $row["ship_pobox"];
            

            $account->billaddress = $billaddress;
            $account->shipaddress = $shipaddress;
        }

        return $account;
    }

    public function getList($_parentaccountid ,$_search, $_orderby, $_orderdir, $limit1, $limit2) {

        $query = "select a.*    ";
        $query .=" , ba.country as bill_country, ba.state as bill_state, ba.city as bill_city , ba.street as bill_street , ba.zipcode as bill_zipcode , ba.pobox as bill_pobox   ";
        $query .=" , sa.country as ship_country, sa.state as ship_state, sa.city as ship_city , sa.street as ship_street , sa.zipcode as ship_zipcode , sa.pobox as ship_pobox   ";
        $query .="  from ait_account a   ";
        $query .="  left join ait_address ba on ba.id = a.billaddressid ";
        $query .="  left join ait_address sa on sa.id = a.shipaddressid ";
        $query .="  where   a.deleted = 0 and parentaccountid = $_parentaccountid ";

        if ($_search != "") {
            $query .=" and (  ";
            $query .="    code  like '%$_search%'  ";
            $query .="  or name  like '%$_search%'  ";
            $query .="  or email  like '%$_search%'  ";
            $query .="  or website  like '%$_search%'  ";
            $query .="  or phone  like '%$_search%'  ";
            $query .="  or taxid  like '%$_search%'  ";

            $query .="  or ba.country  like '%$_search%'  ";
            $query .="  or ba.state  like '%$_search%'  ";
            $query .="  or ba.city  like '%$_search%'  ";
            $query .="  or ba.street  like '%$_search%'  ";
            $query .="  or ba.zipcode  like '%$_search%'  ";
            $query .="  or ba.pobox  like '%$_search%'  ";

            $query .="  or sa.country  like '%$_search%'  ";
            $query .="  or sa.state  like '%$_search%'  ";
            $query .="  or sa.city  like '%$_search%'  ";
            $query .="  or sa.street  like '%$_search%'  ";
            $query .="  or sa.zipcode  like '%$_search%'  ";
            $query .="  or sa.pobox  like '%$_search%'  ";

            $query .=" ) ";
        }
        
        $query .=" order by $_orderby  $_orderdir  ";
        
        $query .="  limit $limit1, $limit2  ";
        
        
        $aAccount = array();
        $rows = $this->selectRows($query);
        if ($rows) {

            if (count($rows) > 0) {
                foreach ($rows as $row) {
                    
                    $shipaddress = new Address();
                    $billaddress = new Address();
                    $account = new Account();

                    $account->id = $row["id"];
                    $account->code = $row["code"];
                    $account->name = $row["name"];
                    $account->email = $row["email"];
                    $account->phone = $row["phone"];
                    $account->phoneext = $row["phoneext"];
                    $account->parentaccountid = $row["parentaccountid"];
                    $account->taxid = $row["taxid"];
                    $account->taxtypeid = $row["taxtypeid"];
                    $account->billaddressid = $row["billaddressid"];
                    $account->shipaddress = $row["shipaddressid"];
                    $account->subdomain = trim($row["subdomain"]);
                    $account->stylecolor = trim($row["stylecolor"]);
                    
                    $billaddress->country = $row["bill_country"];
                    $billaddress->state = $row["bill_state"];
                    $billaddress->city = $row["bill_city"];
                    $billaddress->street = $row["bill_street"];
                    $billaddress->zipcode = $row["bill_zipcode"];
                    $billaddress->pobox = $row["bill_pobox"];

                    $shipaddress->country = $row["ship_country"];
                    $shipaddress->state = $row["ship_state"];
                    $shipaddress->city = $row["ship_city"];
                    $shipaddress->street = $row["ship_street"];
                    $shipaddress->zipcode = $row["ship_zipcode"];
                    $shipaddress->pobox = $row["ship_pobox"];

                    $account->billaddress = $billaddress;
                    $account->shipaddress = $shipaddress;
                    
                    
                    array_push($aAccount, $account);
                    
                }
            }
        }
        
        return $aAccount;
        
    }

    public function getListTotal($_parentaccountid , $_search) {

        $query = "select  count(*) as total   ";
        $query .="  from ait_account a   ";
        $query .="  left join ait_address ba on ba.id = a.billaddressid ";
        $query .="  left join ait_address sa on sa.id = a.shipaddressid ";
        $query .="  where  a.deleted = 0 and parentaccountid = $_parentaccountid   ";

        if ($_search != "") {
            $query .=" and (  ";
            $query .="  code  like '%$_search%'  ";
            $query .="  or name  like '%$_search%'  ";
            $query .="  or email  like '%$_search%'  ";
            $query .="  or website  like '%$_search%'  ";
            $query .="  or phone  like '%$_search%'  ";
            $query .="  or taxid  like '%$_search%'  ";

            $query .="  or ba.country  like '%$_search%'  ";
            $query .="  or ba.state  like '%$_search%'  ";
            $query .="  or ba.city  like '%$_search%'  ";
            $query .="  or ba.street  like '%$_search%'  ";
            $query .="  or ba.zipcode  like '%$_search%'  ";
            $query .="  or ba.pobox  like '%$_search%'  ";

            $query .="  or sa.country  like '%$_search%'  ";
            $query .="  or sa.state  like '%$_search%'  ";
            $query .="  or sa.city  like '%$_search%'  ";
            $query .="  or sa.street  like '%$_search%'  ";
            $query .="  or sa.zipcode  like '%$_search%'  ";
            $query .="  or sa.pobox  like '%$_search%'  ";

            $query .=" ) ";
        }
        
        return intval($this->selectOneColumn("total", $query));
    }

    public function getIdByCode($_code) {

        $query = "select id from ait_account where code = '$_code' ";
        $id = $this->selectOneColumn("id", $query);
        return intval($id);
    }

    public function generateNewId() {
        $query = "select count(*) as total from ait_account ";
        $total = intval($this->selectOneColumn("total", $query));

        $newId = 10000 + ( $total + 1);
        return $newId;
    }

    public function generateNewCode($_parentaccountid) {
        $query = "select count(*) as total from ait_account where parentaccountid = $_parentaccountid ";
        $total = intval($this->selectOneColumn("total", $query));

        $newId = 10000 + ( $total + 1);
        return $newId;
    }
    
    public function getMyCustomersAndMe($_myAccountId){
        $query = "SELECT id , name from ait_account where parentaccountid = $_myAccountId or id = $_myAccountId ";
        $rows = $this->selectRows($query);
       if(!$rows){return false; }
        $total = count($rows);
       if($total == 0){ return false;  }
        $aRecord = array();
       foreach($rows as $row){
           $account = new Account();
           $account->id = $row["id"];
           $account->name = $row["name"];
           array_push($aRecord, $account) ;
       }
       return $aRecord;
    }
}
