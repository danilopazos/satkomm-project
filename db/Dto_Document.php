<?php

class Dto_Document extends Dto_Db {
    
    
    public function setStatus($_idcrypt, $_doctype, $_status, $_userid){
        
        $_templaststatus = $this->getLastStatus($_idcrypt, $_doctype);
        
        if($_templaststatus != $_status){
        
            $query = "insert into documentstatus (docidcrypt,  doctype, datechanged, status, userid) ";
            $query .=" values( '$_idcrypt' , '$_doctype' , CURRENT_TIMESTAMP(), '$_status', '$_userid' ) ";

            return $this->executeUpdate($query);
        }
        return true;
    }
    
    public function getLastStatus($_idcrypt, $_doctype){
        $query = "select status from documentstatus where docidcrypt = '$_idcrypt' and doctype = '$_doctype' order by datechanged desc limit 1 ";
        return   $this->selectOneColumn("status", $query); 
    }
    
    public function getPreLastStatus($_idcrypt, $_doctype){
        $query = "select status from documentstatus where docidcrypt = '$_idcrypt' and doctype = '$_doctype' order by datechanged desc limit 2 ";
        $_status = $this->getLastStatus($_idcrypt, $_doctype);
      
        $_rows = $this->selectRows($query);
        if($_rows){
            $total = count($_rows);
            if($total > 0){
                for($i=0; $i<$total; $i++){
                    $_row  = $_rows[$i];
                    $_status = $_row["status"];
                }
            }
        }
        return $_status;
    }
    
    
    public function getProducts($_accountid, $_customeraccountid , $_operation = 'S', $_langcode = 'ENG', $_search = "") {

        $query = "SELECT pl.*, p.sku , p.issimcard, pd.name, pd.intro ";
        $query .= "FROM productlistprice pl ";
        $query .= "RIGHT JOIN product p ON p.idcrypt = pl.mainidcrypt ";
        $query .= "RIGHT JOIN productdesc pd ON pd.product_id = p.id ";
        $query .= "AND pd.lang_code =  '" . strtoupper($_langcode) . "' ";
        $query .= "WHERE p.deleted =0 ";
        $query .= "AND pl.accountid =$_accountid   ";
        
        $query .=" AND pl.customeraccountid = ( select max(customeraccountid) from productlistprice where mainidcrypt = p.idcrypt and accountid = $_accountid  
 and (customeraccountid = 0 or customeraccountid = $_customeraccountid )    )  ";

        if ($_search != "") {
            $query .= " AND pd.name like '%$_search%' ";
        }
        if ($_operation == 'S') {
            $query .= " AND pl.buyisenabled = 1 ";
        }
        if ($_operation == 'R') {
            $query .= " AND pl.rentpermonthisenabled = 1 ";
        }
       
        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }

        $_total = count($_rows);

        $aRecord = array();
        if ($_total > 0) {
            foreach ($_rows as $_row) {
                $_e = new ItemAutocomplete();
                $_e->id = trim($_row["mainidcrypt"]);
                $_e->sku = trim($_row["sku"]);
                $_e->name = trim($_row["name"]);
                $_e->description = trim($_row["intro"]);
                $_e->issimcard = intval($_row["issimcard"]);

                if ($_operation == 'S') {
                    $_e->price = doubleval($_row["buy"]);
                }
                if ($_operation == 'R') {
                    $_e->price = doubleval($_row["rentpermonth"]);
                }
                array_push($aRecord, $_e);
            }
        }
        return $aRecord;
    }

    public function getServices($_accountid, $_customeraccountid ,  $_operation = 'S', $_langcode = 'ENG', $_search = "", $_productlist = "") {

        $_aProductId = array();
        $_aProductIdTemp = split(",", $_productlist);
        $_total = count($_aProductIdTemp);
        if ($_total > 0) {
            foreach ($_aProductIdTemp as $_idcrypt) {
                if ($_idcrypt != "") {
                    array_push($_aProductId, $_idcrypt);
                }
            }
        }

        $query = "SELECT p . * , pd.name, pd.description  ";
        $query .= "FROM plan p ";
        $query .= "RIGHT JOIN plandesc pd ON pd.plan_id = p.id ";
        $query .= "RIGHT JOIN plan pf on pf.idcrypt = p.parentplan ";
        $query .= "AND pd.lang_code =  '" . strtoupper($_langcode) . "' ";
        $query .= "WHERE p.accountid =$_accountid AND p.deleted = 0 ";
        
        $query .=" AND ( p.customeraccountid = 0 or p.customeraccountid = $_customeraccountid  ) ";
        
        if ($_search != "") {
            $query .= "AND pd.name LIKE  '%" . $_search . "%' ";
        }

        $_totalProductId = count($_aProductId);
        if ($_totalProductId > 0) {
            $query .= "AND pf.id ";
            $query .= "IN ( ";
            $query .= "SELECT pp.plan_id ";
            $query .= "FROM productplan pp ";
            $query .= "RIGHT JOIN product pr ON pr.id = pp.product_id ";
            $query .= "WHERE ";
            if ($_totalProductId > 0) {
                for ($i = 0; $i < $_totalProductId; $i++) {
                    $query .= "    pr.idcrypt =  '" . $_aProductId[$i] . "' ";
                    if (($i + 1) < $_totalProductId) {
                        $query .= " OR ";
                    }
                }
            }


            $query .= "AND pp.deleted =0  ";
            $query .= ") ";
        }
        
        $query .=" and p.aviable = 1  ";
         

        $_rows = $this->selectRows($query);

        if (!$_rows) {
            return false;
        }

        $_total = count($_rows);

        $aRecord = array();
        if ($_total > 0) {
            foreach ($_rows as $_row) {
                $_e = new ItemAutocomplete();
                $_e->id = trim($_row["idcrypt"]);
                $_e->sku = trim($_row["sku"]);
                $_e->name = trim($_row["name"]);
                $_e->description = trim($_row["description"]);
                $_e->accountid = intval($_row["accountid"]);
                $_e->customeraccountid = intval($_row["customeraccountid"]);
                $_e->issimcard = 0;
                $_e->price = doubleval($_row["price"]);
                $_e->activationprice = doubleval($_row["activationpayment"]);

                array_push($aRecord, $_e);
            }
        }
        return $aRecord;
    }

    public function getProductsByDocumentId($_docidcrypt, $_doctype) {
        $query = "select * from documentproduct where docidcrypt = '$_docidcrypt' and doctype = '$_doctype' ";
        
        $_rows = $this->selectRows($query);
        $_total = count($_rows);
        
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        if (!$_rows) {
            return false;
        }
        $total = count($_rows);
        if ($total == 0) {
            return false;
        }
        foreach ($_rows as $_row) {
            $_e = new DocumentProduct();
            $_e->idcrypt = $_row["idcrypt"];
            $_e->name = stripslashes($_row["name"]);
            $_e->description = stripslashes($_row["description"]);
            $_e->operationtype = stripslashes($_row["operationtype"]);
            $_e->qty = intval($_row["qty"]);
            $_e->price = doubleval($_row["price"]);
            $_e->months = intval($_row["months"]);
            $_e->subtotal = doubleval($_row["subtotal"]);
            $_e->itemindex = intval($_row["itemindex"]);
            array_push($aRecord, $_e);
        }
        
        return $aRecord;
    }

    public function getServicesByDocumentId($_docidcrypt, $_doctype) {
                
        $query = " select ds.* , p.parentplan ";
        $query .= "from documentservice ds  ";
        $query .= "right join plan p on p.idcrypt = ds.idcrypt ";
        $query .= "where ds.docidcrypt = '$_docidcrypt' and ds.doctype = '$_doctype'";
                
        $_rows = $this->selectRows($query);
        $_total = count($_rows);
        if ($_total == 0) {
            return false;
        }

        $aRecord = array();
        if (!$_rows) {
            return false;
        }
        $total = count($_rows);
        if ($total == 0) {
            return false;
        }
        foreach ($_rows as $_row) {
            $_e = new DocumentService();
            $_e->parentplan = $_row["parentplan"];
            $_e->idcrypt = $_row["idcrypt"];
            $_e->name = stripslashes($_row["name"]);
            $_e->description = stripslashes($_row["description"]);
            $_e->qty = intval($_row["qty"]);
            $_e->price = doubleval($_row["price"]);
            $_e->activationprice = doubleval($_row["activationprice"]);
            $_e->subtotal = doubleval($_row["subtotal"]);
            $_e->itemindex = intval($_row["itemindex"]);
            array_push($aRecord, $_e);
        }
        return $aRecord;
    }

    public function updateProducts($_docidcrypt, $_aRecords) {

        $this->executeUpdate(" delete from documentproduct where docidcrypt = '$_docidcrypt'  ");

        $total = count($_aRecords);
        if ($total == 0) {
            return false;
        }
        


        $query = "insert into documentproduct( itemindex,  docidcrypt, doctype,  idcrypt, name, description, qty, months, price, subtotal , operationtype ) values ";
        for ($i = 0; $i < $total; $i++) {
            
            $_e = $_aRecords[$i];
            $_name = addslashes($_e->name );
            $_description = addslashes($_e->description );            
            
            $query .= " (  " . $_e->itemindex . "   , '$_docidcrypt'  , '" . $_e->doctype . "' ,  '" . $_e->idcrypt . "', '" . $_name . "' , '" .$_description. "' , " . $_e->qty . " , " . $_e->months . " , " . $_e->price . " , " . $_e->subtotal . " , '" . $_e->operationtype . "'    )  ";
            if (($i + 1) < $total) {
                $query .= " ,  ";
            }
        }
      
        
        return $this->executeUpdate($query);
    }

    public function updateServices($_docidcrypt, $_aRecords) {

        $this->executeUpdate(" delete from documentservice where docidcrypt = '$_docidcrypt'  ");

        $total = count($_aRecords);
        if ($total == 0) {
            return false;
        }

    
        
        $query = "insert into documentservice( itemindex,  docidcrypt, doctype,  idcrypt, name, description, qty, activationprice, price, subtotal  ) values ";
        for ($i = 0; $i < $total; $i++) {
            
            $_e = $_aRecords[$i];
            $_name = addslashes($_e->name );
            $_description = addslashes($_e->description );                
            
            $query .= " (  " . $_e->itemindex . "   , '$_docidcrypt' , '" . $_e->doctype . "' ,     '" . $_e->idcrypt . "', '" . $_name. "' , '" .$_description. "' , " . $_e->qty . " , " . $_e->activationprice . " , " . $_e->price . " , " . $_e->subtotal . "      )  ";
            if (($i + 1) < $total) {
                $query .= " ,  ";
            }
        }

        return $this->executeUpdate($query);
    }

}
