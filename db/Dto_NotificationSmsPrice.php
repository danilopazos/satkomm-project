<?php

class Dto_NotificationSmsPrice extends Dto_Db {

    public function getAll() {

        $query = "SELECT * FROM  notification_smsprice ";

        $rows = $this->selectRows($query);
        $aRecords = array();
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        foreach ($rows as $row) {
            $entity = new NotificationSmsPrice();
            $entity->iso = $row["iso"];
            $entity->country = $row["country"];
            $entity->description = $row["description"];
            $entity->price_sms = doubleval($row["price_sms"]);
            array_push($aRecords, $entity);
        }

        return $aRecords;
    }

}
