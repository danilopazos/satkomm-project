<?php

class Dto_ShippingProvider extends Dto_Db {
    
  public function getAll(){
      $query = "select * from shippingprovider";
      $rows = $this->selectRows($query);
      if(!$rows){return false;}
      if(count($rows) == 0){return false;}
      $aRecord = array();
      foreach($rows as $row){
          $e = new ShippingProvider();
          $e->code = trim($row["code"]);
          $e->name = trim($row["name"]);
          array_push($aRecord, $e);
      }
      return $aRecord;
  }
    
    
}//class