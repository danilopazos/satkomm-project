<?php

class Dto_Media extends Dto_Db {

    public function getIdCryptByProductId($productIdCrypt){
        $query  = "select m.idcrypt from  ";
        $query .= "product p  ";
        $query .= "right join media m on m.entity_id = p.id and m.entity_table = 'PRODUCT' ";
        $query .= "where p.idcrypt = '$productIdCrypt' and m.ismain = 1 ";
        
       
        
        return $this->selectOneColumn("idcrypt", $query);
    }    
    
    public function getByIdCrypt($idcrypt) {

        $query = "select * from media where idcrypt = '$idcrypt' ";
 
        $item = $this->selectOneRow($query);
       
        $entity = new Media();
        
        if ($item) {
            
            $entity->id = intval($item["id"]);
            $entity->idcrypt = trim($item["idcrypt"]);
            $entity->entity_table = trim($item["entity_table"]);

            $entity->entity_id = intval($item["entity_id"]);
            $entity->url = trim($item["url"]);
            $entity->type = trim($item["type"]);
           
        }//if
      
        return $entity;
    }    
    
    
    function getMetadataFromMediaBlob($id) {

        $query = "select fileext, filename from mediablob where media_id = $id ";

        $data = $this->selectOneRow($query);

        $media = new Media();
        $media->filename = trim($data["filename"]);
        $media->fileext = trim($data["fileext"]);
        return $media;
    }

//function

    public function getOriginalBlobById($id) {

        $query = "select original_blob from mediablob where media_id = $id";

        $data = $this->selectOneRow($query);

        return $data["original_blob"];
    }

//function

    public function getCommonBlobById($id) {

        $query = "select common_blob from mediablob where media_id = $id";

        $data = $this->selectOneRow($query);

        return $data["common_blob"];
    }

//function    

    public function getThumbBlobById($id) {

        $query = "select thumb_blob from mediablob where media_id = $id";

        $data = $this->selectOneRow($query);

        return $data["thumb_blob"];
    }

//function     
}

//class