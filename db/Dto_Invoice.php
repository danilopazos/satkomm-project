<?php

class Dto_Invoice extends Dto_Db {

    public function getInvoiceByOrderCode($_ordercode, $_invoicetype) {
        //$query = "SELECT idcrypt FROM invoice where invoicetype = '$_invoicetype' and ordercode = '$_ordercode' ";
        //return trim($this->selectOneColumn("idcrypt", $query));
    
        $query = " SELECT sp.name as shippingprovider_name , a.name as accountname,  u.username as u_username, u.firstname as u_firstname , u.lastname as u_lastname,     so.*  "
                . " FROM invoice so "
                . " RIGHT JOIN ait_account  a on a.id = so.accountid "
                . " RIGHT JOIN ait_usersystem  u on u.id = so.userid "
                . " LEFT JOIN shippingprovider  sp on sp.code = so.shippingprovidercode "
                . " WHERE so.ordercode = '$_ordercode' and  so.invoicetype = '$_invoicetype'  LIMIT   1  ";
        
      
        
        $row = $this->selectOneRow($query);
        $entity = new Invoice();
        if ($row) {

            $entity = $this->parseRowToEntity($row);
        }else{
            return false;
            
        }
        return $entity;        
        
        
    }

    public function generateInvoiceByAirtime($_ordercode, $_userid) {

        $_order = $this->selectOneRow("SELECT * FROM airtimeorder where code = '$_ordercode' LIMIT 1 ");
        $_aDetail = $this->selectRows("SELECT * FROM airtimeoperation WHERE airtimeordercode = '$_ordercode' ");

        $_accountcustomerid = intval($_order["accountcustomerid"]);
        $_accountproviderid = intval($_order["accountproviderid"]);

        $query = "select a.* from ait_address a where a.id = (select billaddressid from ait_account where id = $_accountcustomerid) ";
        $_address = $this->selectOneRow($query);

        $_ordercode = trim($_order["code"]);
        $_orderdate = trim($_order["orderdate"]);
        //$_userid = trim($_order["userid"]);
        $_totalitems = intval($_order["totalitems"]);
        $_subtotal = doubleval($_order["subtotal"]);
        $_taxes = doubleval($_order["taxes"]);
        $_payment_method = trim($_order["payment_method"]);
        $_convenience_fee = doubleval($_order["convenience_fee"]);
        $_totalpayment = doubleval($_order["totalpayment"]);
        $_comments = addslashes(trim($_order["ordercomments"]));

        $_country = trim($_address["country"]);
        $_state = trim($_address["state"]);
        $_city = trim($_address["city"]);
        $_street = trim($_address["street"]);
        $_zipcode = trim($_address["zipcode"]);
        $_pobox = trim($_address["pobox"]);

        $_invoicecode = $this->generateInvoiceCode("SERVICES");
        $_dategenerated = date('Y-m-d H:i:s');
        $_invoicedate = date('Y-m-d');

        $_idcrypt = md5(date('YmdHis') . '' . rand(1, 99999) . '' . rand(1, 99999));

        $query = " insert into invoice ( idcrypt,  ";
        $query .= " provideraccountid, accountid, code, dategenerated, status,  ";
        $query .= " invoicedate, invoiceduedate, ordercode, invoicetype, userid, payment_method,  ";
        $query .= " billing_country, billing_zipcode, billing_address, billing_state, billing_city, billing_pobox, ";
        $query .= " totalitems, subtotal, taxes, shipping_cost, x_ammount, payment_fee, invoicecomments ";
        $query .= ") values ( '$_idcrypt', ";
        $query .= " $_accountproviderid, $_accountcustomerid, '$_invoicecode','$_dategenerated'   , 1 , ";
        $query .= " '$_invoicedate', '$_invoicedate', '$_ordercode' , 'SERVICES',  '$_userid'  , '$_payment_method', ";
        $query .= " '$_country', '$_zipcode', '$_street', '$_state', '$_city', '$_pobox', ";
        $query .= " $_totalitems, $_subtotal, $_taxes, 0, $_totalpayment, $_convenience_fee, '$_comments'  ";
        $query .= ") ";
    
        
        $_result = $this->executeUpdate($query);
        if ($_result) {
           return $this->getByIdcrypt( $_idcrypt );
        }

        return false;
    }
  
    public function setPaymentDate($_code, $_paymentdate, $_obs, $_userid) {
        $_obs = addslashes($_obs);
        $_paymentdate = addslashes($_paymentdate);
        $query = "update invoice set paymentobs = '$_obs', paymentdate = '$_paymentdate' , paymentuserid = '$_userid' , paymentdatetime = CURRENT_TIMESTAMP() where code = '$_code'  ";
        return $this->executeUpdate($query);
    }

    public function insertItemDispatchByBatch($_records) {
        $total = count($_records);
        if ($total == 0) {
            return false;
        }

        $query = "insert into itemdispatch (documentcode, itemindex, itemid, iserial, icode2)  values   ";
        for ($i = 0; $i < $total; $i++) {

            $entity = $_records[$i];
            $_documentcode = addslashes($entity->documentcode);
            $_index = intval($entity->index);
            $_itemid = addslashes($entity->itemid);
            $_serial = addslashes($entity->serial);
            $_code2 = addslashes($entity->code2);
            $query .= " (  '$_documentcode' , $_index, '$_itemid' , '$_serial' , '$_code2'    ) ";
            if (($i + 1) < $total) {
                $query .= " , ";
            }
        }

        return $this->executeUpdate($query);
    }

    public function deleteItemDispatchedByBatch($_documentcode, $_itemid) {
        $query = "delete from itemdispatch where documentcode = '$_documentcode' and itemid = '$_itemid'   ";

        return $this->executeUpdate($query);
    }

    public function getItemsDispatched($_documentcode, $_itemid) {
        $query = "select *  from itemdispatch where documentcode = '$_documentcode' and itemid = '$_itemid'   ";

        $rows = $this->selectRows($query);
        $aRecord = array();
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        foreach ($rows as $row) {
            $entity = new ItemDispatch();
            $entity->documentcode = $row["documentcode"];
            $entity->index = $row["itemindex"];
            $entity->itemid = $row["itemid"];
            $entity->serial = $row["iserial"];
            $entity->code2 = $row["icode2"];
            array_push($aRecord, $entity);
        }
        return $aRecord;
    }

    public function generateInvoiceCode($invoicetype = "GOODS") {
        $yearcode = date('Y');
        $y2 = date('y');

        $query = "select count(*) as total from invoice where year(invoicedate) = '$yearcode' and invoicetype = '$invoicetype' ";
        
                
        $total = intval($this->selectOneColumn("total", $query)) + 1;

        $code = '' . $total;

        while (strlen($code) < 7) {
            $code = "0" . $code;
        }

        $_it = "D";
        if ($invoicetype == "SERVICES") {
            $_it = "S";
        }


        $code = 'I' . $_it . '-' . $y2 . '' . $code;
        return $code;
    }

    public function generateInvoice($_invoice, $_invoicedetail) {

        $_result = false;

        $_invoicecomments = addslashes($_invoice->invoicecomments);
        $_bill_country = addslashes($_invoice->billing_country);
        $_bill_state = addslashes($_invoice->billing_state);
        $_bill_city = addslashes($_invoice->billing_city);
        $_bill_street = addslashes($_invoice->billing_address);
        $_bill_zipcode = addslashes($_invoice->billing_zipcode);
        $_bill_pobox = addslashes($_invoice->billing_pobox);
        $_ship_country = addslashes($_invoice->shipping_country);
        $_ship_state = addslashes($_invoice->shipping_state);
        $_ship_city = addslashes($_invoice->shipping_city);
        $_ship_street = addslashes($_invoice->shipping_address);
        $_ship_zipcode = addslashes($_invoice->shipping_zipcode);
        $_ship_pobox = addslashes($_invoice->shipping_pobox);
        
        $_idcrypt = md5( date('YmdHis').''.rand(1,9999).''.rand(1,9999) );
        
        $query = "insert into invoice ( idcrypt, ";
        $query .= "  code,   accountid  ,  invoicedate, invoicepaymentrand, userid, userip, sessionid, username,   ";
        $query .= "  payment_method , totalitems, subtotal, taxes, shipping_cost, x_ammount, invoicecomments,  ";
        $query .= "  billing_country, billing_state, billing_city, billing_address, billing_zipcode, billing_pobox, ";
        $query .= "  shipping_country, shipping_state, shipping_city, shipping_address, shipping_zipcode, shipping_pobox , ";
        $query .= "  ordercode, invoiceduedate, payment_fee , dategenerated  ";
        $query .= " ) values  ";
        $query .= " ( '$_idcrypt', '" . $_invoice->code . "' ,  " . $_invoice->accountid . "      , '" . $_invoice->invoicedate . "' ,    NULL  , '" . $_invoice->userid . "' ,  '" . $_invoice->userip . "'    ,  '" . $_invoice->sessionid . "' , '" . $_invoice->username . "' ,  ";
        $query .= "   '" . $_invoice->payment_method . "' ,   " . $_invoice->totalitems . " ,   " . $_invoice->subtotal . " ,   " . $_invoice->taxes . "     ,    " . $_invoice->shipping_cost . "     ,     " . $_invoice->x_ammount . " ,  '" . $_invoicecomments . "'    ,  ";
        $query .= "   '" . $_bill_country . "' , '" . $_bill_state . "' , '" . $_bill_city . "' , '" . $_bill_street . "' , '" . $_bill_zipcode . "' , '" . $_bill_pobox . "' ,  ";
        $query .= "   '" . $_ship_country . "' , '" . $_ship_state . "' , '" . $_ship_city . "' , '" . $_ship_street . "' , '" . $_ship_zipcode . "' , '" . $_ship_pobox . "'    ,      ";
        $query .= "   '" . $_invoice->ordercode . "' ,    '" . $_invoice->invoiceduedate . "'   ,    " . $_invoice->payment_fee . "  , CURRENT_TIMESTAMP()    ";
        $query .= "      )     ";


        //echo $query; die();

        $_result = $this->executeUpdate($query);

        if (!$_result) {
            return false;
        }

        $_result = $this->generateInvoiceDetail($_invoice->code, $_invoicedetail);

        return $_result;
    }

    public function generateInvoiceDetail($_code, $_invoicedetail) {
        $result = false;
        $query = "insert into invoicedetail (code, itemid,   sku, qty, productid, productprice, productname, operation, typeofproduct, subtotal )";
        $total = count($_invoicedetail);
        if ($total > 0) {
            $query .= " values     ";
            for ($index = 0; $index < $total; $index ++) {
                $item = $_invoicedetail[$index];
                $_itemid = $item->itemid;
                $_sku = $item->sku;
                $_qty = $item->qty;
                $_productid = $item->productid;
                $_productprice = $item->productprice;
                $_productname = $item->productname;
                $_operation = $item->operation;
                $_typeofproduct = $item->typeofproduct;
                $_subtotal = $item->subtotal;
                $query .= "  (   '$_code' , '$_itemid' , '$_sku' , $_qty , '$_productid' , $_productprice , '$_productname' , '$_operation' , '$_typeofproduct'  , $_subtotal )   ";
                if (($index + 1 ) < $total) {
                    $query .= " ,  ";
                }
            }
            $result = $this->executeUpdate($query);
        }
        return $result;
    }

    public function getCodeByChildInvoice($_parentaccountid, $_code) {
        $query = "SELECT so.id ";
        $query .= "FROM invoice  so   ";
        $query .= "RIGHT JOIN ait_account a ON a.id = so.accountid  and a.parentaccountid = $_parentaccountid ";
        $query .= "WHERE a.id = so.accountid  and a.parentaccountid = $_parentaccountid  and so.code = '$_code' ";

        $id = intval($this->selectOneColumn("id", $query));
        return $id;
    }

    public function getByIdcrypt($_idcrypt) {
        $query = " SELECT sp.name as shippingprovider_name , a.name as accountname,  u.username as u_username, u.firstname as u_firstname , u.lastname as u_lastname,     so.*  "
                . " FROM invoice so "
                . " RIGHT JOIN ait_account  a on a.id = so.accountid "
                . " RIGHT JOIN ait_usersystem  u on u.id = so.userid "
                . " LEFT JOIN shippingprovider  sp on sp.code = so.shippingprovidercode "
                . " WHERE so.idcrypt = '$_idcrypt' LIMIT   1  ";
        $row = $this->selectOneRow($query);
        $entity = new Invoice();
        if ($row) {

            $entity = $this->parseRowToEntity($row);
        }
        return $entity;
    }

    public function getByCode($_code) {
        $query = " SELECT sp.name as shippingprovider_name , a.name as accountname,  u.username as u_username, u.firstname as u_firstname , u.lastname as u_lastname,     so.*  "
                . " FROM invoice so "
                . " RIGHT JOIN ait_account  a on a.id = so.accountid "
                . " RIGHT JOIN ait_usersystem  u on u.id = so.userid "
                . " LEFT JOIN shippingprovider  sp on sp.code = so.shippingprovidercode "
                . " WHERE so.code = '$_code' LIMIT   1  ";
         
        
        $row = $this->selectOneRow($query);
        $entity = new Invoice();
        if ($row) {
            $entity = $this->parseRowToEntity($row);
        }
        return $entity;
    }

    public function parseRowToEntity($row) {
        $entity = new Invoice();
        $entity->id = $row["id"];
        $entity->idcrypt = $row["idcrypt"];
        $entity->invoicetype = trim($row["invoicetype"]);
        $entity->provideraccountid = $row["provideraccountid"];
        $entity->code = $row["code"];
        $entity->ordercode = $row["ordercode"];
        $entity->accountname = $row["accountname"];
        $entity->accountid = $row["accountid"];
        $entity->username = $row["u_username"];
        $entity->firstname = $row["u_firstname"];
        $entity->lastname = $row["u_lastname"];

        $entity->invoicedate = $row["invoicedate"];
        $entity->invoiceduedate = $row["invoiceduedate"];
        $entity->userid = $row["userid"];
        $entity->sessionid = $row["sessionid"];
        $entity->totalitems = intval($row["totalitems"]);
        $entity->subtotal = doubleval($row["subtotal"]);
        $entity->taxes = doubleval($row["taxes"]);
        $entity->shipping_cost = doubleval($row["shipping_cost"]);
        $entity->payment_method = trim($row["payment_method"]);
        $entity->payment_fee = doubleval($row["payment_fee"]);
        $entity->paymentobs = stripslashes($row["paymentobs"]);
        $entity->x_ammount = doubleval($row["x_ammount"]);
        $entity->invoicecomments = trim($row["invoicecomments"]);
        $entity->status = intval($row["status"]);

        $entity->billing_country = $row["billing_country"];
        $entity->billing_state = $row["billing_state"];
        $entity->billing_city = $row["billing_city"];
        $entity->billing_address = $row["billing_address"];
        $entity->billing_zipcode = $row["billing_zipcode"];
        $entity->billing_pobox = $row["billing_pobox"];

        $entity->shipping_country = $row["shipping_country"];
        $entity->shipping_state = $row["shipping_state"];
        $entity->shipping_city = $row["shipping_city"];
        $entity->shipping_address = $row["shipping_address"];
        $entity->shipping_zipcode = $row["shipping_zipcode"];
        $entity->shipping_pobox = $row["shipping_pobox"];

        $entity->dispatchuserid = $row["dispatchuserid"];
        $entity->dispatchdate = $row["dispatchdate"];
        $entity->dispacthdatetime = $row["dispatchdatetime"];

        $entity->paymentuserid = $row["paymentuserid"];
        $entity->paymentdate = $row["paymentdate"];
        $entity->paymentdatetime = $row["paymentdatetime"];

        $entity->deliveryuserid = $row["deliveryuserid"];
        $entity->deliverydate = $row["deliverydate"];
        $entity->deliverydatetime = $row["deliverydatetime"];

        $entity->shippingtrackingid = $row["shippingtrackingid"];
        $entity->shippingprovidercode = $row["shippingprovidercode"];
        $entity->shippingprovider_name = $row["shippingprovider_name"];
        return $entity;
    }

    public function getDetailByCode($_code) {
        $aRecord = array();
        $query = "SELECT sd.* , m.idcrypt as mediaid , (  select count(*) from itemdispatch where documentcode = '$_code' and itemid = sd.itemid  ) as totaldispatched    "
                . " FROM invoicedetail sd  "
                . " RIGHT JOIN product p on p.idcrypt = sd.productid "
                . " LEFT JOIN media m on m.entity_id = p.id  and m.type = 'IMAGE' and m.ismain = 1  "
                . " WHERE sd.code = '$_code'   ";

        //echo $query; die();

        $rows = $this->selectRows($query);
        if ($rows) {
            $total = count($rows);
            if ($total == 0) {
                return false;
            }

            foreach ($rows as $row) {
                $entity = new InvoiceDetail();
                $entity->id = $row["id"];
                $entity->code = $row["code"];
                $entity->sku = $row["sku"];
                $entity->qty = intval($row["qty"]);
                $entity->productprice = doubleval($row["productprice"]);
                $entity->productname = $row["productname"];
                $entity->productid = $row["productid"];
                $entity->operation = $row["operation"];
                $entity->itemid = $row["itemid"];
                $entity->subtotal = doubleval($row["subtotal"]);
                $entity->mediaid = $row["mediaid"];
                $entity->totaldispatched = intval($row["totaldispatched"]);
                array_push($aRecord, $entity);
            }
        }
        return $aRecord;
    }

    public function getDetailDispatchedByCode($_code) {
        $aRecord = array();
        $query = "select id.* , d.sku, d.productname, d.productprice , p.issimcard  , p.gawosc   , d.productid   from itemdispatch id ";
        $query .= "RIGHT JOIN invoicedetail d on d.itemid = id.itemid  ";
        $query .= "RIGHT JOIN product p on p.idcrypt = d.productid  ";
        $query .= "where id.documentcode = '$_code' and d.code = id.documentcode ";
        $query .= "order by id.itemid , id.itemindex asc ";

        $rows = $this->selectRows($query);
        if ($rows) {
            $total = count($rows);
            if ($total == 0) {
                return false;
            }

            foreach ($rows as $row) {
                $entity = new InventoryItem();
                $entity->id = $row["id"];
                $entity->documentcode = $row["documentcode"];
                $entity->itemindex = $row["itemindex"];
                $entity->itemid = $row["itemid"];
                $entity->iserial = $row["iserial"];
                $entity->icode2 = $row["icode2"];
                $entity->productid = $row["productid"];
                $entity->sku = $row["sku"];
                $entity->issimcard = $row["issimcard"];
                $entity->productname = $row["productname"];
                $entity->productprice = $row["productprice"];
                $entity->issimcard = intval($row["issimcard"]);
                $entity->issimcard = intval($row["issimcard"]);
                $entity->gawosc = intval($row["gawosc"]);
                array_push($aRecord, $entity);
            }
        }

        return $aRecord;
    }

    public function setDispatchDate($_code, $_userid, $_date) {
        $query = "update invoice set dispatchuserid = $_userid,  dispatchdate = '$_date' , dispatchdatetime = CURRENT_TIMESTAMP()   where code = '$_code'";
        return $this->executeUpdate($query);
    }

    public function setDeliveryDate($_code, $_userid, $_date) {
        $query = "update invoice set  deliveryuserid = $_userid   , deliverydate = '$_date' , deliverydatetime = CURRENT_TIMESTAMP()   where code = '$_code' ";
        return $this->executeUpdate($query);
    }

    public function setShippingTrackingInfo($_code, $_shippingprovidercode, $_shippingtrackingid) {
        $query = "update invoice set shippingprovidercode = '$_shippingprovidercode' , shippingtrackingid = '$_shippingtrackingid' where code = '$_code' ";
        return $this->executeUpdate($query);
    }

    public function getMyCustomerInvoices($_parentaccountid, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10) {

        $query = " SELECT i.code, a.name as accountname,  i.invoicedate, i.userid,  i.dispatchdate, i.deliverydate,  ";
        $query .= " i.totalitems , i.x_ammount ,   i.payment_method  , i.payment_fee  , u.firstname, u.lastname   ";
        $query .= " FROM invoice  i  ";
        $query .= " RIGHT JOIN ait_usersystem u ON u.id = i.userid ";
        $query .= " RIGHT JOIN ait_account a ON a.id = i.accountid  and a.parentaccountid = $_parentaccountid  ";
        $query .= " WHERE a.id = i.accountid  and a.parentaccountid = $_parentaccountid         ";

        if ($_search != "") {
            $query .= " and (";
            $query .= " a.name like '%$_search%' OR  ";
            $query .= " i.code like '%$_search%' OR  ";
            $query .= " i.invoicedate like '%$_search%' OR  ";
            $query .= " i.dispatchdate like '%$_search%' OR  ";
            $query .= " i.deliverydate like '%$_search%' OR  ";
            $query .= " i.invoiceduedate like '%$_search%'  ";
            $query .= "  )  ";
        }
        $query .= "ORDER BY $_sortCol $_sortDir";
        $query .= " LIMIT $_limit1 , $_limit2";

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        $aRecords = array();
        foreach ($rows as $row) {
            $entity = new Invoice();
            $entity->code = $row["code"];
            $entity->accountname = $row["accountname"];
            $entity->invoicedate = $this->getDateByTimeZone($row["invoicedate"]);

            $entity->dispatchdate = $this->getDateByTimeZone($row["dispatchdate"]);
            $entity->deliverydate = $this->getDateByTimeZone($row["deliverydate"]);

            $entity->userid = $row["userid"];
            $entity->payment_method = trim($row["payment_method"]);
            $entity->payment_fee = doubleval($row["payment_fee"]);
            $entity->totalitems = intval($row["totalitems"]);
            $entity->x_ammount = doubleval($row["x_ammount"]);
            $entity->firstname = $row["firstname"];
            $entity->lastname = $row["lastname"];
            array_push($aRecords, $entity);
        }
        return $aRecords;
    }

    public function getMyCustomerInvoicesTotal($_parentaccountid, $_search) {

        $query = " SELECT count(i.id) as total ";
        $query .= " FROM invoice  i  ";
        $query .= " RIGHT JOIN ait_usersystem u ON u.id = i.userid ";
        $query .= " RIGHT JOIN ait_account a ON a.id = i.accountid ";
        $query .= " where a.parentaccountid = $_parentaccountid ";
        if ($_search != "") {
            $query .= " and (";
            $query .= " so.code like '%$_search%' OR  ";
            $query .= " so.orderdatelike '%$_search%' OR  ";
            $query .= " so.firstname '%$_search%' OR  ";
            $query .= " so.lastname '%$_search%'  ";
            $query .= "  )  ";
        }

        $total = intval($this->selectOneColumn("total", $query));

        return $total;
    }

    public function getSerialsRepeated($_arecords) {
        $aSerials = array();
        $aRegistered = $this->getSerialsByBatch($_arecords);

        $total = count($_arecords);

        if ($total == 0) {
            return false;
        }
        if (count($aRegistered) == 0) {
            return false;
        }

        foreach ($aRegistered as $registered) {
            $found = false;
            for ($i = 0; $i < $total && !$found; $i++) {
                $temp = $_arecords[$i];
                if ($registered->serial == $temp->serial && $registered->itemid != $temp->itemid) {
                    $found = true;
                    array_push($aSerials, $temp);
                }
            }
        }

        //echo var_dump($aSerials); die();

        return $aSerials;
    }

    public function getSerialsByBatch($_arecords) {
        $total = count($_arecords);
        if ($total == 0) {
            return false;
        }

        $query = "select *  from itemdispatch where   ";
        for ($i = 0; $i < $total; $i++) {
            $record = $_arecords[$i];
            $serial = $record->serial;
            $query .= " iserial ='$serial'  ";
            if (($i + 1) < $total) {
                $query .= " OR  ";
            }
        }

        $aRecord = array();
        $rows = $this->selectRows($query);
        if ($rows) {
            foreach ($rows as $row) {
                $itemDispatch = new ItemDispatch();
                $itemDispatch->documentcode = trim($row["documentcode"]);
                $itemDispatch->index = trim($row["index"]);
                $itemDispatch->itemid = trim($row["itemid"]);
                $itemDispatch->serial = trim($row["iserial"]);
                array_push($aRecord, $itemDispatch);
            }
        }

        return $aRecord;
    }

}
