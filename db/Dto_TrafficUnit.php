<?php
class Dto_TrafficUnit extends Dto_Db{

    public function register($_e){
        $query = "insert into ait_trafficunit (id,  code, description) values('".$_e->id."' ,    '".$_e->code."' , '".$_e->description."' ) ";
       
        return $this->executeUpdate($query);
    }
    
    public function update($_e){
        $query = "update ait_trafficunit set  code = '".$_e->code."' , description = '".$_e->description."'  ";
        $query .="where id = '".$_e->id."' ";
    
        return $this->executeUpdate($query);
    }
    
    public function delete($_e){
        $query = "update ait_trafficunit set deleted = 1 where id = '".$_e->id."' ";
        return $this->executeUpdate($query);
    }
    
    
    public function getById($_e){
        $query = "select * from ait_trafficunit where id = '".$_e->id."' and deleted = 0";
        $row = $this->selectOneRow($query);
        if(!$row){return false;}
        return $this->getRow($row);
    }
    
    public function getByCode($_e){
        $query = "select * from ait_trafficunit where code = '".$_e->code."' and deleted = 0 ";
    
        $row = $this->selectOneRow($query);
        if(!$row){return false;}
        return $this->getRow($row);
    }
    
    public function getByDescription($_e){
        $description = addslashes($_e->description);
        $query = "select * from ait_trafficunit where description = '$description' and deleted = 0  ";
        $row = $this->selectOneRow($query);
        if(!$row){return false;}
        return $this->getRow($row);        
    }
     
    public function getRow($row){
        $e = new TrafficUnit();
        $e->id = trim($row["id"]);
        $e->code = trim($row["code"]);
        $e->description = trim($row["description"]);
        return $e;                
    }
    
    public function getList( $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10) {
        $query = "select * from ait_trafficunit where deleted = 0 ";

        if ($_search != "") {
            $query .=" and  (  ";
            $query .=" code like '%$_search%'  ";
            $query .=" OR description like '%$_search%'  ";
            $query .=" )  ";
        }
        $query .=" order by $_sortCol  $_sortDir ";
        $query .=" limit $_limit1, $_limit2 ";
 
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            array_push($aRecord, $this->getRow($row));
        }
        return $aRecord;
    }
    public function getListTotal( $_search) {
        $query = "select count(*) as total from ait_trafficunit where deleted = 0 ";

        if ($_search != "") {
            $query .=" and  (  ";
            $query .=" code like '%$_search%'  ";
            $query .=" OR description like '%$_search%'  ";
            $query .=" )  ";
        }
         
        return intval($this->selectOneColumn("total", $query));
    }
    
    public function generateId(){
        return md5(date("Ymdhis")."".rand(10000,99999)."".rand(10000,99999));
    }
    
    public function getAllUnits(){
        $query = "select * from ait_trafficunit where deleted = 0";
        $rows = $this->selectRows($query);
        if(!$rows){return false;}
        $total = count($rows);
        if($total == 0){return false;}
        $aRecord = array();
        foreach($rows as $row){
            array_push($aRecord, $this->getRow($row));
        }
        return $aRecord;
    }
    
    
    public function getAll() {
        
        $query = "SELECT * FROM  unitmesuare ";

        $rows = $this->selectRows($query);
        $aRecords = array();
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        foreach ($rows as $row) {
            $entity = new TrafficUnit();
            $entity->code = $row["code"];
            $entity->name = $row["description"];
            array_push($aRecords, $entity);
        }

        return $aRecords;
    }

}