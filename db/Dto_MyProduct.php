<?php
class Dto_MyProduct extends Dto_Db {

    public function getIdByIdCrypt($_idcrypt) {
        $query = "select id from product where idcrypt = '$_idcrypt' ";
        return $this->selectOneColumn("id", $query);
    }

    public function getDetailById($_lang_code, $_idcrypt) {

        $query = "SELECT p.idcrypt ,  p.sku,    pd.*, p.network_code, p.height, p.width, p.weight  ";
        $query .= " , pl.buy, pl.rentpermonth, pl.buyisenabled, pl.rentpermonthisenabled, pl.id as productpriceid ";
        $query .= "FROM  productdesc pd  ";
        $query .= "right join product p on p.id = pd.product_id  ";
        $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt   ";
        $query .= "where pd.lang_code = '$_lang_code' and p.idcrypt = '$_idcrypt' limit 1  ";

        $entity = new ProductDetail();
        $row = $this->selectOneRow($query);
        if ($row) {

            $entity->idcrypt = $row["idcrypt"];
            $entity->sku = $row["sku"];
            $entity->network_code = $row["network_code"];
            $entity->height = $row["height"];
            $entity->width = $row["width"];
            $entity->weight = $row["weight"];
            $entity->name = $row["name"];
            $entity->intro = $row["intro"];
            $entity->description = $row["description"];
            $entity->package = $row["package"];
            $entity->technicalspecs = $row["technicalspecs"];
            $entity->features = $row["features"];
            $entity->docs = $row["docs"];
            $entity->tags = $row["tags"];

            if (intval($row["productpriceid"]) > 0) {
                $entity->buy = doubleval($row["buy"]);
                $entity->rentpermonth = doubleval($row["rentpermonth"]);
                $entity->buyisenabled = intval($row["buyisenabled"]);
                $entity->rentpermonthisenabled = intval($row["rentpermonthisenabled"]);
            }
        }
        return $entity;
    }

    public function getListByCatalogDiffPlan($plan_id, $_lang_code, $_search, $_sortcol, $_sortdir) {
        $aRecords = array();
        /*
          $query = "SELECT p.idcrypt ,   p.sku,     pd.name, p.network_code, p.height, p.width, p.weight  , pb.price as distprice , m.idcrypt as mediaid   ";
          $query .= "FROM  productdesc pd  ";
          $query .= "right join product p on p.id = pd.product_id ";
          $query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1   ";
          $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1  ";
          $query .= "where pd.lang_code = '$_lang_code'   ";
         */

        $query = "SELECT p.idcrypt ,   p.sku,     pd.name, p.network_code, p.height, p.width, p.weight  , ";
        $query .= "pb.price as distprice , m.idcrypt as mediaid    ";
        $query .= "FROM  productdesc pd   ";
        $query .= "right join product p on p.id = pd.product_id   ";
        $query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1     ";
        $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1   ";
        $query .= "where pd.lang_code = '$_lang_code'     ";
        $query .= "AND p.id NOT IN (SELECT product_id FROM productplan where plan_id =  $plan_id )  ";


        if ($_search != "") {
            $query .= "  and (   pd.name like '%$_search%'  )   ";
        }

        if ($_sortcol != "") {
            $query .= "  order by $_sortcol $_sortdir  ";
        }

        //$query .="  limit $_limit1 , $_limit2  " ;

        $rows = $this->selectRows($query);
        if ($rows) {

            $total = count($rows);
            if ($total > 0) {
                foreach ($rows as $row) {
                    $entity = new Product();
                    $entity->idcrypt = $row["idcrypt"];
                    $entity->sku = $row["sku"];
                    $entity->name = $row["name"];
                    $entity->network_code = $row["network_code"];
                    $entity->lang_code = $row["lang_code"];
                    $entity->distprice = $row["distprice"];
                    $entity->mediaid = trim($row["mediaid"]);
                    array_push($aRecords, $entity);
                }
            }
        }
        return $aRecords;
    }

    public function insertProductListPrice($_accountid, $_userid, $_idcrypt, $_buy, $_rentpermonth, $_buyisenabled, $_rentpermonthisenabled, $buymargin = 0, $_customeraccountid = 0) {
        $_customeraccountid = intval($_customeraccountid);
        $query = "insert into productlistprice(accountid, mainidcrypt, buy, buyisenabled,  rentpermonth, rentpermonthisenabled , rentperweek, rentperweekisenabled, modifiedby , datemodified , buymargin , customeraccountid )";
        $query .= " values ( $_accountid,  '$_idcrypt' , $_buy, $_buyisenabled , $_rentpermonth, $_rentpermonthisenabled , 0 , 0 ,'$_userid' , CURRENT_TIMESTAMP() , $buymargin , $_customeraccountid )  ";


      
        $_result = $this->executeUpdate($query);
        if ($_result) {
            $this->insertProductListPriceHistory($_accountid, $_userid, $_idcrypt, $_buy, $_rentpermonth, $_buyisenabled, $_rentpermonthisenabled, $buymargin, $_customeraccountid);
        }
        return $_result;
    }

    public function updateProductListPrice($_accountid, $_userid, $_idcrypt, $_buy, $_rentpermonth, $_buyisenabled, $_rentpermonthisenabled, $buymargin = 0, $_customeraccountid = 0) {

        $_customeraccountid = intval($_customeraccountid);

        $query = "update productlistprice ";
        $query .= " set ";
        $query .= " buy = $_buy , rentpermonth = $_rentpermonth, buyisenabled = $_buyisenabled, rentpermonthisenabled = $_rentpermonthisenabled ";
        $query .= " , buymargin = $buymargin   ";
        $query .= " , modifiedby = '$_userid' , datemodified = CURRENT_TIMESTAMP() , customeraccountid = $_customeraccountid ";
        $query .= " where mainidcrypt = '$_idcrypt' and accountid = $_accountid and customeraccountid = $_customeraccountid ";

        $_result = $this->executeUpdate($query);

        if ($_result) {
            $_result = $this->insertProductListPriceHistory($_userid, $_idcrypt, $_buy, $_rentpermonth, $_buyisenabled, $_rentpermonthisenabled, $_accountid, $_customeraccountid);
        }

        return $_result;
    }

    //es posible que el $_accountid vaya primero en la lista, estaba redefinido
    public function insertProductListPriceHistory($_userid, $_idcrypt, $_buy, $_rentpermonth, $_buyisenabled, $_rentpermonthisenabled, $buymargin = 0, $_accountid, $_customeraccountid = 0) {

        $query = "insert into productlistprice_history( accountid,   mainidcrypt, buy, buyisenabled,  rentpermonth, rentpermonthisenabled, rentperweek, rentperweekisenabled, modifiedby , datemodified , buymargin ,  customeraccountid  )";
        $query .= " values ( $_accountid,  '$_idcrypt' , $_buy, $_buyisenabled , $_rentpermonth, $_rentpermonthisenabled , 0 , 0 ,'$_userid' , CURRENT_TIMESTAMP() , $buymargin , $_customeraccountid )  ";
        $_result = $this->executeUpdate($query);

        return $_result;
    }
    
    public function generatemyproducts($_provideraccountid, $_accountid, $_userid ){
        
        $query = " select pl2.id as customerpriceid,   p.issimcard , pl.* ";
        $query .=" from productlistprice pl  ";
        $query .=" right join product p on p.idcrypt  = pl.mainidcrypt  ";
        $query .=" left join productlistprice pl2 on pl2.mainidcrypt = pl.mainidcrypt and pl2.accountid = $_accountid ";
        $query .=" where pl.accountid = $_provideraccountid  and pl.customeraccountid = 0 ";
        $query .=" order by pl.id asc         ";
        

        $rows = $this->selectRows($query);
        if ($rows) {

            $total = count($rows);
            if ($total > 0) {
                foreach ($rows as $row) {
                    if (intval($row["customerpriceid"]) == 0) {
                        
                        
                        $rentpermonth = 0;
                        $buy = doubleval($row["buy"]) * 1.2;
                        $buyisenabled = 1;
                        $rentpermonthisenabled = 1;
                        if ($buy > 0) {
                            $rentpermonth = $buy / 6;
                        }

                        if ( intval($row["issimcard"])  == 1) {
                            $rentpermonth = 0;
                            $rentpermonthisenabled = 0;
                        }

                        $buy = round($buy);
                        $rentpermonth = round($rentpermonth);

                        $this->insertProductListPrice($_accountid, $_userid, trim($row["mainidcrypt"]) , $buy, $rentpermonth, $buyisenabled, $rentpermonthisenabled, 20, 0);
                        
                        
                        
                    }
                }
            }
        }    
        
    }//function
    
    public function getListByCatalog($_accountid, $_userid, $_lang_code, $_search, $_sortcol, $_sortdir, $_limit1 = 0, $_limit2 = 10) {

        $query = "";
        $aRecords = array();

        if (intval($_accountid) == 10001) {
            
            /*
            $query = "SELECT p.idcrypt ,   p.sku,     pd.name, p.network_code, p.height, p.width, p.weight  , pb.price as distprice , m.idcrypt as mediaid, p.issimcard   ";
            $query .= " , pl.buy, pl.rentpermonth, pl.buyisenabled, pl.rentpermonthisenabled, pl.id as productpriceid, ";
            $query .= " ( (  ( pl.buy - pb.price  )   *100)/pb.price   ) as  buymargin ,  ";
            $query .= " ( select count(*) as total from productlistprice where  accountid = $_accountid  and customeraccountid > 0 and mainidcrypt = p.idcrypt ) as totalcustom   ";
            $query .= "FROM  productdesc pd  ";
            $query .= "right join product p on p.id = pd.product_id ";
            $query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1   ";
            $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1  ";
            $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt and pl.accountid = $_accountid  ";
            $query .= "where pd.lang_code = '$_lang_code' and pl.accountid = $_accountid and p.deleted = 0  and pl.customeraccountid = 0  ";
            */


            $query = "SELECT p.idcrypt ,   p.sku,     pd.name, p.network_code, p.height, p.width, p.weight  , pb.price as distprice , m.idcrypt as mediaid, p.issimcard   ";
            $query .= " , pl.buy, pl.rentpermonth, pl.buyisenabled, pl.rentpermonthisenabled, pl.id as productpriceid, ";
            $query .= " ( (  ( pl.buy - pb.price  )   *100)/pb.price   ) as  buymargin ,  ";
            $query .= " ( select count(*) as total from productlistprice where  accountid = $_accountid  and customeraccountid > 0 and mainidcrypt = p.idcrypt ) as totalcustom   ";
            $query .= "FROM  productdesc pd  ";
            $query .= "right join product p on p.id = pd.product_id ";
            $query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1   ";
            $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1  ";
            $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt and pl.accountid = $_accountid  and pl.customeraccountid = 0   ";
            $query .= "where pd.lang_code = '$_lang_code'  and p.deleted = 0  ";
            
            
        } else {

            $_provideraccountid = intval($this->selectOneColumn("parentaccountid", "select parentaccountid from ait_account where id = $_accountid "));
             
            $query = "SELECT pl.id as productpriceid , p.idcrypt ,   p.sku,     pd.name, p.network_code, p.height, p.width, p.weight  , pb.buy as distprice , m.idcrypt as mediaid, p.issimcard   ";
            $query .= " , pl.buy, pl.rentpermonth, pl.buyisenabled, pl.rentpermonthisenabled, pl.id as productpriceid, ( ( ( pl.buy - pb.buy  )   *100)/pb.buy  ) as  buymargin  ,  ";
            $query .= " ( select count(*) as total from productlistprice where  accountid = $_accountid  and customeraccountid > 0 and mainidcrypt = p.idcrypt ) as totalcustom  ";
            $query .= "FROM  productdesc pd  ";
            $query .= "right join product p on p.id = pd.product_id ";
            
           
            $query .= " right join productlistprice pb on pb.mainidcrypt = p.idcrypt and pb.accountid = $_provideraccountid  ";
            $query .= " and pb.customeraccountid   ";
            $query .= "= (  select max(customeraccountid) from productlistprice where mainidcrypt = p.idcrypt and accountid = $_provideraccountid    ";
            $query .= " and (customeraccountid = 0 or customeraccountid = $_accountid )  )  ";
            
            
            $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1  ";
            $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt and pl.accountid = $_accountid  ";
            $query .= "where pd.lang_code = '$_lang_code'  and pl.accountid = $_accountid   and p.deleted = 0   and pl.customeraccountid = 0  ";
             
           
            
        }
       
       //  echo $query; die();
        
        
        if ($_search != "") {
            $query .= "  and (   pd.name like '%$_search%'  )   ";
        }

        if ($_sortcol != "") {
            $query .= "  order by $_sortcol $_sortdir  ";
        }

        $query .= "  limit $_limit1 , $_limit2  ";
         
      
        
        $rows = $this->selectRows($query);
        if ($rows) {

            $total = count($rows);
            if ($total > 0) {
                foreach ($rows as $row) {
                    $entity = new Product();
                    $entity->idcrypt = $row["idcrypt"];
                    $entity->sku = $row["sku"];
                    $entity->name = $row["name"];
                    $entity->network_code = $row["network_code"];
                    $entity->lang_code = $row["lang_code"];
                    $entity->distprice = $row["distprice"];
                    $entity->mediaid = trim($row["mediaid"]);
                    $entity->issimcard = intval($row["issimcard"]);
                    $entity->buymargin = doubleval($row["buymargin"]);
                    $entity->totalcustom = intval($row["totalcustom"]);
                    
                    //if (intval($row["productpriceid"]) > 0) {
                        $entity->buy = doubleval($row["buy"]);
                        $entity->rentpermonth = doubleval($row["rentpermonth"]);
                        $entity->buyisenabled = intval($row["buyisenabled"]);
                        $entity->rentpermonthisenabled = intval($row["rentpermonthisenabled"]);
                    
                        
                    //} 
                    /*
                    else {
                        $rentpermonth = 0;
                        $buy = doubleval($row["distprice"]) * 1.2;
                        $buyisenabled = 1;
                        $rentpermonthisenabled = 1;
                        if ($buy > 0) {
                            $rentpermonth = $buy / 6;
                        }

                        if ($entity->issimcard == 1) {
                            $rentpermonth = 0;
                            $rentpermonthisenabled = 0;
                        }

                        $buy = round($buy);
                        $rentpermonth = round($rentpermonth);

                        $this->insertProductListPrice($_accountid, $_userid, $entity->idcrypt, $buy, $rentpermonth, $buyisenabled, $rentpermonthisenabled, 20, 0);

                        $entity->buy = $buy;
                        $entity->buyisenabled = intval($buyisenabled);
                        $entity->rentpermonth = $rentpermonth;
                        $entity->rentpermonthisenabled = intval($rentpermonthisenabled);
                    }
                    */
                    array_push($aRecords, $entity);
                }
            }
        }
 

        return $aRecords;
    }
    
    public function getListByCatalogTotal($_accountid, $_lang_code, $_search) {
        
        /*
        $query = "SELECT count(*) as total  ";
        $query .= "FROM  productdesc pd   ";
        $query .= "right join product p on p.id = pd.product_id  ";
        $query .= "where pd.lang_code = '$_lang_code' and p.deleted = 0   ";

        if ($_search != "") {
            $query .= "  and (   pd.name like '%$_search%'  )   ";
        }
        */
        
        if (intval($_accountid) == 10001) {

            $query = "SELECT  count(*) as total  ";
            $query .= "FROM  productdesc pd  ";
            $query .= "right join product p on p.id = pd.product_id ";
            $query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1   ";
            $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1  ";
            $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt and pl.accountid = $_accountid  ";
            $query .= "where pd.lang_code = '$_lang_code'  and p.deleted = 0  and pl.customeraccountid = 0  ";
        } else {

            $_provideraccountid = intval($this->selectOneColumn("parentaccountid", "select parentaccountid from ait_account where id = $_accountid "));

            $query = "SELECT  count(*) as total  ";
            $query .= "FROM  productdesc pd  ";
            $query .= "right join product p on p.id = pd.product_id ";
            $query .= "right join productlistprice pb on pb.mainidcrypt = p.idcrypt and pb.accountid = $_provideraccountid    ";
            $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1  ";
            $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt and pl.accountid = $_accountid  ";
            $query .= "where pd.lang_code = '$_lang_code'  and p.deleted = 0   and pl.customeraccountid = 0  ";
 
        }
        
        if ($_search != "") {
            $query .= "  and (   pd.name like '%$_search%'  )   ";
        }
        
        $total = intval($this->selectOneColumn("total", $query));
        
        return $total;
    }

    public function getPricesById($_accountid, $_idcrypt, $_customeraccountid=0) {

        $_customeraccountid = intval($_customeraccountid);

        if (intval($_accountid) == 10001) {

            $query = "SELECT p.idcrypt , pb.price as distprice ,   p.issimcard   ";
            $query .= " , pl.buy, pl.rentpermonth, pl.buyisenabled, pl.rentpermonthisenabled, pl.id as productpriceid, pl.buymargin  ";
            $query .= "FROM product p   ";
            $query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1   ";
            $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt   ";
            $query .= "where p.idcrypt = '$_idcrypt' and pl.customeraccountid = $_customeraccountid  ";
        } else {

            $_provideraccountid = intval($this->selectOneColumn("parentaccountid", "select parentaccountid from ait_account where id = $_accountid "));

            $query = "SELECT p.idcrypt , pb.buy as distprice ,   p.issimcard   ";
            $query .= " , pl.buy, pl.rentpermonth, pl.buyisenabled, pl.rentpermonthisenabled, pl.id as productpriceid, pl.buymargin  ";
            $query .= "FROM product p   ";
            $query .= "right join productlistprice pb on pb.mainidcrypt = p.idcrypt and pb.accountid = $_provideraccountid   ";
            $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt and pl.accountid = $_accountid    ";
            $query .= "where p.idcrypt = '$_idcrypt'  and pl.customeraccountid = $_customeraccountid  ";
        }

        

        $_row = $this->selectOneRow($query);
        $e = new Product();
        if ($_row) {
            $e->idcrypt = trim($_row["idcrypt"]);
            $e->distprice = doubleval($_row["distprice"]);
            $e->buy = doubleval($_row["buy"]);
            $e->issimcard = intval($_row["issimcard"]);
            $e->rentpermonth = doubleval($_row["rentpermonth"]);
            $e->buymargin = doubleval($_row["buymargin"]);
            $e->buyisenabled = intval($_row["buyisenabled"]);
            $e->rentpermonthisenabled = intval($_row["rentpermonthisenabled"]);
        }
        return $e;
    }

    public function getSpecialPrices($_accountid, $_idcrypt) {
       
        if (intval($_accountid) == 10001) {

            $query = "SELECT a.name as accountname , pl.customeraccountid,  p.idcrypt , pb.price as distprice ,   p.issimcard   ";
            $query .= " , pl.buy, pl.rentpermonth, pl.buyisenabled, pl.rentpermonthisenabled, pl.id as productpriceid, pl.buymargin  ";
            $query .= "FROM product p   ";
            $query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1   ";
            $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt   ";
            $query .= "left join ait_account a on a.id = pl.customeraccountid   ";
            $query .= "where p.idcrypt = '$_idcrypt' and pl.accountid = $_accountid  and  pl.customeraccountid > 0  ";
        } else {

            $_provideraccountid = intval($this->selectOneColumn("parentaccountid", "select parentaccountid from ait_account where id = $_accountid "));

            $query = "SELECT a.name as accountname ,  pl.customeraccountid,  p.idcrypt , pb.buy as distprice ,   p.issimcard   ";
            $query .= " , pl.buy, pl.rentpermonth, pl.buyisenabled, pl.rentpermonthisenabled, pl.id as productpriceid, pl.buymargin  ";
            $query .= "FROM product p   ";
            
            //$query .= "right join productlistprice pb on pb.mainidcrypt = p.idcrypt and pb.accountid = $_provideraccountid   ";
            
            $query .= "right join productlistprice pb on pb.mainidcrypt = p.idcrypt and pb.accountid = $_provideraccountid   ";
            $query .= "and pb.customeraccountid   ";
            $query .= "= (  select max(customeraccountid) from productlistprice where mainidcrypt = p.idcrypt and accountid = $_provideraccountid    ";
            $query .= " and (customeraccountid = 0 or customeraccountid = $_accountid )  ) ";
            
            
            $query .= "left join productlistprice pl on pl.mainidcrypt = p.idcrypt and pl.accountid = $_accountid    ";
            $query .= "left join ait_account a on a.id = pl.customeraccountid   ";
            $query .= "where p.idcrypt = '$_idcrypt' and pl.accountid = $_accountid   and  pl.customeraccountid > 0   ";
        }
        
         //echo $query; die();
                
        $_rows = $this->selectRows($query);
        if (!$_rows) {
            return false;
        }
      
        $aRecord = array();
        foreach ($_rows as $_row) {
            $e = new Product();
            $e->idcrypt = trim($_row["idcrypt"]);
            $e->distprice = doubleval($_row["distprice"]);
            $e->buy = doubleval($_row["buy"]);
            $e->issimcard = intval($_row["issimcard"]);
            $e->rentpermonth = doubleval($_row["rentpermonth"]);
            $e->buymargin = doubleval($_row["buymargin"]);
            $e->buyisenabled = intval($_row["buyisenabled"]);
            $e->rentpermonthisenabled = intval($_row["rentpermonthisenabled"]);
            $e->customeraccountid = intval($_row["customeraccountid"]);
            $e->accountid = $_accountid;
            $e->accountname = trim($_row["accountname"]);
            array_push($aRecord, $e);
        }
        return $aRecord;
    }
    
     
    

}