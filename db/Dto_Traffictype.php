<?php
class Dto_Traffictype extends Dto_Db{
    
    public function getTtgroupList(){
        $query ="select ttgroup as code from ait_traffictype  where deleted = 0 group by ttgroup ";
      
        $_rows = $this->selectRows($query);
        if (!$_rows) {
            return false;
        }
      
        
        $_aList = array();
        foreach($_rows as $_row){
            array_push($_aList, $_row["code"]);
        }
        
       
        
        return $_aList;        
    }
    
    
    public function register($_e){
        $query = "insert into ait_traffictype (id, networkcode, code, airtimeprovider,  description , ttgroup) values('".$_e->id."' , '".$_e->networkcode."' , '".$_e->code."' , '".$_e->airtimeprovider."' , '".addslashes( $_e->description )."' , '".addslashes( $_e->ttgroup )."' ) ";
       
        return $this->executeUpdate($query);
    }
    
    public function update($_e){
        $query = "update ait_traffictype set ttgroup = '$_e->ttgroup',  networkcode = '".$_e->networkcode."', airtimeprovider =  '".$_e->airtimeprovider."' , code = '".$_e->code."' , description = '".$_e->description."'  ";
        $query .="where id = '".$_e->id."' ";
    
        return $this->executeUpdate($query);
    }
    
    public function delete($_e){
        $query = "update ait_traffictype set deleted = 1 where id = '".$_e->id."' ";
        return $this->executeUpdate($query);
    }
    
    
    public function getById($_e){
        $query = "select * from ait_traffictype where id = '".$_e->id."' and deleted = 0";
        $row = $this->selectOneRow($query);
        if(!$row){return false;}
        return $this->getRow($row);
    }
    
    public function getByCode($_e){
        $query = "select * from ait_traffictype where code = '".$_e->code."' and deleted = 0 ";
    
        $row = $this->selectOneRow($query);
        if(!$row){return false;}
        return $this->getRow($row);
    }
    
    public function getByDescription($_e){
        $description = addslashes($_e->description);
        $query = "select * from ait_traffictype where description = '$description' and deleted = 0  ";
        $row = $this->selectOneRow($query);
        if(!$row){return false;}
        return $this->getRow($row);        
    }
    
    public function getByNetworkcode($_networkcode, $ttgroup="G"){
        $query = "select * from ait_traffictype where networkcode = '$_networkcode' and deleted = 0 and ttgroup = '$ttgroup' order by description desc ";
        $rows = $this->selectRows($query);
        if(!$rows){return false;}
        $total = count($rows);
        if($total == 0){return false;}
        $aRecords = array();
        foreach($rows as $row){
            array_push($aRecords, $this->getRow($row));
        }
        return $aRecords;
    }
    
    public function getRow($row){
        $e = new Traffictype();
        $e->id = trim($row["id"]);
        $e->code = trim($row["code"]);
        $e->ttgroup = trim($row["ttgroup"]);
        $e->description = stripslashes(trim($row["description"]));
        $e->networkcode = trim($row["networkcode"]);
        $e->airtimeprovider = trim($row["airtimeprovider"]);
        return $e;                
    }
    
    public function getList( $_ttgroup,  $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10) {
        $query = "select * from ait_traffictype where deleted = 0 ";
        
        if(trim($_ttgroup) != ""){
            $query .=" and ttgroup = '$_ttgroup' ";
        }
                
        
        if ($_search != "") {
            $query .=" and  (  ";
            $query .=" code like '%$_search%'  ";
            $query .=" OR networkcode like '%$_search%'  ";
            $query .=" OR description like '%$_search%'  ";
            $query .=" )  ";
        }
        $query .=" order by $_sortCol  $_sortDir ";
        $query .=" limit $_limit1, $_limit2 ";
 
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            array_push($aRecord, $this->getRow($row));
        }
        return $aRecord;
    }
    public function getListTotal( $_ttgroup , $_search) {
        $query = "select count(*) as total from ait_traffictype where deleted = 0  ";
        
        if(trim($_ttgroup) != ""){
            $query .=" and ttgroup = '$_ttgroup' ";
        }
        
        if ($_search != "") {
            $query .=" and  (  ";
            $query .=" code like '%$_search%'  ";
            $query .=" OR networkcode like '%$_search%'  ";
            $query .=" OR description like '%$_search%'  ";
            $query .=" )  ";
        }
 
        return intval($this->selectOneColumn("total", $query));
    }
    
    public function generateId(){
        return md5(date("Ymdhis")."".rand(10000,99999)."".rand(10000,99999));
    }
    
    public function getAll() {
        
        $query = "SELECT tt.code,  tt.airtimeprovider, ttd.description as name ";
        $query .= "FROM  traffictype tt ";
        $query .= "right join traffictypedesc ttd on ttd.traffictype_id = tt.id ";
        $query .= "where ttd.lang_code = 'ENG' and ( ttd.description is not null or ttd.description != '' ) ";
        $query .= "and tt.deleted = 0 ";

        $rows = $this->selectRows($query);
        $aRecords = array();
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }

        foreach ($rows as $row) {
            $entity = new Traffictype();
            $entity->code = $row["code"];
            $entity->name = $row["name"];
            $entity->airtimeprovider = $row["airtimeprovider"];
            array_push($aRecords, $entity);
        }

        return $aRecords;
    }

    
}