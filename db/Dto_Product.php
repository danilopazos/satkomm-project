<?php

class Dto_Product extends Dto_Db {
    
    
    public function getIdByIdCrypt($_idcrypt){
        $query = "select id from product where idcrypt = '$_idcrypt' ";
        return $this->selectOneColumn("id", $query);
    }
    
    public function getDetailById(  $_lang_code  , $_idcrypt    ) {

        $query = "SELECT p.idcrypt ,  p.sku,    pd.*, p.network_code, p.height, p.width, p.weight  ";
        $query .= "FROM  productdesc pd  ";
        $query .= "right join product p on p.id = pd.product_id  ";
        $query .= "where pd.lang_code = '$_lang_code' and p.idcrypt = '$_idcrypt' limit 1  ";
        
         
        $entity = new ProductDetail();
        $row = $this->selectOneRow($query);
        if($row){
            
            $entity->idcrypt = $row["idcrypt"];
            $entity->sku = $row["sku"];
            $entity->network_code = $row["network_code"];
            $entity->height = $row["height"];
            $entity->width = $row["width"];
            $entity->weight = $row["weight"];
            $entity->name = $row["name"];
            $entity->intro = $row["intro"];
            $entity->description = $row["description"];
            $entity->package = $row["package"];
            $entity->technicalspecs = $row["technicalspecs"];
            $entity->features = $row["features"];
            $entity->docs = $row["docs"];
            $entity->tags = $row["tags"];
            
        }
        return $entity;
    }

    public function getListByCatalogDiffPlan($plan_id, $_lang_code ,  $_search, $_sortcol, $_sortdir  ) {
        $aRecords = array();
        /*
        $query = "SELECT p.idcrypt ,   p.sku,     pd.name, p.network_code, p.height, p.width, p.weight  , pb.price as distprice , m.idcrypt as mediaid   ";
        $query .= "FROM  productdesc pd  ";
        $query .= "right join product p on p.id = pd.product_id ";
        $query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1   ";
        $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1  ";
        $query .= "where pd.lang_code = '$_lang_code'   ";
        */
        
        
        $query = "SELECT p.idcrypt ,   p.sku,     pd.name, p.network_code, p.height, p.width, p.weight  , ";
        $query .= "pb.price as distprice , m.idcrypt as mediaid    ";
        $query .= "FROM  productdesc pd   ";
        $query .= "right join product p on p.id = pd.product_id   ";
        $query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1     ";
        $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1   ";
        $query .= "where pd.lang_code = '$_lang_code'     ";
        $query .= "AND p.id NOT IN (SELECT product_id FROM productplan where plan_id =  $plan_id and deleted = 0 ) AND p.deleted = 0 ";      
        
        
        if ($_search != "") {
            $query .="  and (   pd.name like '%$_search%'  )   ";
        }
        
        if($_sortcol != ""){
            $query .="  order by $_sortcol $_sortdir  ";
        }
        
       // echo $query; die();
        
        
        //$query .="  limit $_limit1 , $_limit2  " ;
        
        $rows = $this->selectRows($query);
        if ($rows) {

            $total = count($rows);
            if ($total > 0) {
                foreach ($rows as $row) {
                    $entity = new Product();
                    $entity->idcrypt = $row["idcrypt"];
                    $entity->sku = $row["sku"];
                    $entity->name = $row["name"];
                    $entity->network_code = $row["network_code"];
                    $entity->lang_code = $row["lang_code"];
                    $entity->distprice = $row["distprice"];
                    $entity->mediaid = trim($row["mediaid"]);
                    array_push($aRecords, $entity);
                }
            }
        }
        return $aRecords;
    }

    
    public function getListByCatalog($_accountid, $_lang_code ,  $_search, $_sortcol, $_sortdir, $_limit1=0, $_limit2=10   ) {
        
        $_provideraccountid = intval($this->selectOneColumn("parentaccountid", "select parentaccountid from ait_account where id = $_accountid "));
        
        $aRecords = array();
        $query = "SELECT p.idcrypt ,   p.sku,     pd.name, p.network_code, p.height, p.width, p.weight  , pb.buy as distprice , m.idcrypt as mediaid   ";
        $query .= "FROM  productdesc pd  ";
        $query .= "right join product p on p.id = pd.product_id ";
        //$query .= "right join productbuy pb on pb.product_id = p.id and pb.status = 1   ";
        
        //$query .= "right join productlistprice pb on pb.mainidcrypt = p.idcrypt and pb.accountid = $_provideraccountid    ";
        
        //con este join calculamos el costo
        $query .= " right join productlistprice pb on pb.mainidcrypt = p.idcrypt and pb.accountid = $_provideraccountid   ";
        $query .= " and pb.customeraccountid  ";
        $query .= " = (  select max(customeraccountid) from productlistprice where mainidcrypt = p.idcrypt and accountid = $_provideraccountid  ";
        $query .= " and (customeraccountid = 0 or customeraccountid = $_accountid )  )  ";
        
        
        $query .= "left join media m on m.entity_id = p.id and m.type = 'IMAGE' and m.ismain = 1  ";
        $query .= "where pd.lang_code = '$_lang_code' and p.deleted = 0   ";
       
        if ($_search != "") {
            $query .="  and (   pd.name like '%$_search%'  )   ";
        }
        
        if($_sortcol != ""){
            $query .="  order by $_sortcol $_sortdir  ";
        }
        
        $query .="  limit $_limit1 , $_limit2  " ;
        
        $rows = $this->selectRows($query);
        if ($rows) {

            $total = count($rows);
            if ($total > 0) {
                foreach ($rows as $row) {
                    $entity = new Product();
                    $entity->idcrypt = $row["idcrypt"];
                    $entity->sku = $row["sku"];
                    $entity->name = $row["name"];
                    $entity->network_code = $row["network_code"];
                    $entity->lang_code = $row["lang_code"];
                    $entity->distprice = $row["distprice"];
                    $entity->mediaid = trim($row["mediaid"]);
                    array_push($aRecords, $entity);
                }
            }
        }
        return $aRecords;
    }

    public function getListByCatalogTotal( $_lang_code  ,   $_search  ) {

        $query = "SELECT count(*) as total  ";
        $query .= "FROM  productdesc pd   ";
        $query .= "right join product p on p.id = pd.product_id  ";
        $query .= "where pd.lang_code = '$_lang_code' and p.deleted = 0   ";

        if ($_search != "") {
            $query .="  and (   pd.name like '%$_search%'  )   ";
        }

        $total = intval($this->selectOneColumn("total", $query));

        return $total;
    }

}
