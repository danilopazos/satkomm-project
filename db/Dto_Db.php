<?php
class Dto_Db {

    public $link;

    public function connect($typeAction="") {
        $config = new ConfigReader();
        if($typeAction=="update"){
                    $config = new ConfigUpdate(); 
        }
        
        $this->link = mysqli_connect($config->main_dbhost . ":" . $config->main_dbport, $config->main_dbuser, $config->main_dbpass, $config->main_dbname);
        // mysqli_select_db($config->main_dbname, $this->link);
     
    } 
    
    public function close() {
        mysqli_close($this->link);
    } 
    
    public function getJsonContent($module, $action, $params){
        /*
        $totalParams = count($params);
       
        $config = new Config();
        $url=  $config->main_server."?module=".$module."&action=".$action."&token=".$config->server_token."&code=".$config->server_code."";        
        
        if($totalParams > 0){
            
            foreach($params   as $key => $value){
                $url .= "&".$key."=".$value;
            }
             
        }//if
       
        
        $json = file_get_contents( $url );
        
        return $json; 
         */
    }//function

    public function selectRows($query) {
        $this->connect();

        $data = mysqli_query( $this->link, $query );

        $aList = array();

        if (!$data) {
            return false;
        } else {

            if (mysqli_num_rows($data) > 0) {

                while ($item = mysqli_fetch_array($data)) {

                    array_push($aList, $item);
                }//while
            } else {
                return false;
            }//else
        }//else

        $this->close();

        return $aList;
    }

    public function selectOneRow($query) {

        $this->connect();
        $data = mysqli_query($this->link ,  $query  );
        $aList = array();

        if (!$data) {
            return false;
        } else {

            if (mysqli_num_rows($data) > 0) {

                $aList = mysqli_fetch_array($data);
            } else {
                return false;
            }//else
        }//else

        $this->close();

        return $aList;
    }

    public function selectOneColumn($column, $query) {
        $this->connect();
        $data = mysqli_query($this->link , $query );

        $value = null;

        if (!$data) {
            return false;
        } else {

            if (mysqli_num_rows($data) > 0) {

                $item = mysqli_fetch_array($data);

                $value = $item[$column];
            } else {
                return false;
            }//else
        }//else

        $this->close();

        return $value;
    }

    public function executeUpdate($query) {

        $this->connect("update");

        $ok = false;
        if (mysqli_query($this->link , $query  )) {
            $ok = true;
        } else {
            $ok = false;
        }

        $this->close();

        return $ok;
    }
 

}
