<?php

class Dto_Cdr extends Dto_Db {

    public function getEntityByRow($row) {

        $entity = new Cdr();
        if (!$row) {
            return false;
        }
        
        $entity->start_date = trim($row["start_date"]);
        $entity->start_time = trim($row["start_time"]);

        $entity->mobilenumber = trim($row["mobilenumber"]);
        $entity->callednumber =   $row["callednumber"];
        $entity->countrydialedname = trim($row["countrydialedname"]);
        $entity->ratingcalltype = trim($row["ratingcalltype"]);
        $entity->end_date = trim($row["end_date"]);
        $entity->end_time = trim($row["end_time"]);
        $entity->units = trim($row["units"]);
        $entity->totalcharge = trim($row["totalcharge"]); 

        return $entity;
    }

    public function getListByAccountid($_accountid, $_search, $_sortCol, $_sortDir, $_limit1 = 0, $_limit2 = 10) {
        $query = "select a.* from ";
        $query .=" cdr_test_i a  ";

        if ($_search != "") {
            $query .=" where   ";
            $query .="   (  ";
            $query .=" a.mobilename like '%$_search%'  ";
            $query .=" OR a.callednumber like '%$_search%'  ";
            $query .=" OR a.countrydialedname like '%$_search%'  ";
            $query .=" OR a.start_date like '%$_search%'  ";

            $query .=" )  ";
        }

        $query .=" order by $_sortCol  $_sortDir ";
        $query .=" limit $_limit1, $_limit2 ";

      //  echo $query; die();

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            array_push($aRecord, $this->getEntityByRow($row));
        }
        return $aRecord;
    }

    public function getListByAccountidTotal($_accountid, $_search) {
        $query = "select count(*) as total  FROM cdr_test_i  a ";


        if ($_search != "") {
            $query .=" where   ";
            $query .="   (  ";
            $query .=" a.mobilename like '%$_search%'  ";
            $query .=" OR a.callednumber like '%$_search%'  ";
            $query .=" OR a.countrydialedname like '%$_search%'  ";
            $query .=" OR a.start_date like '%$_search%'  ";

            $query .=" )  ";
        }
       
        return intval($this->selectOneColumn("total", $query));
    }

}
