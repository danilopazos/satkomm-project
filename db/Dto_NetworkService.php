<?php
class Dto_NetworkService extends Dto_Db{
    
    public function create( $_networkservice  ){
        $_id = $_networkservice->id;
        $_name = addslashes( $_networkservice->name);
        
        $query = "insert into ait_networkservice (id, name) values( '$_id' , '$_name' ) ";
        
        return $this->executeUpdate($query);
        
    }
    
    public function update( $_networkservice  ){
        $_id = $_networkservice->id;
        $_name = addslashes( $_networkservice->name);
        
        $query = "update ait_networkservice set name =  '$_name'  where id = '$_id' ";
        
        return $this->executeUpdate($query);
        
    }
    
    public function delete($_id){
        $query = "update ait_networkservice set deleted = 1 where id = '$_id' ";
        return $this->executeUpdate($query);
    }
    
    public function getList(){
        
        $query = "select * from ait_networkservice where deleted = 0 ";
        $rows = $this->selectRows($query);
        $aService  = array();
        if($rows){
            $total = count($rows);
            if($total > 0){
                foreach($rows as $row){
                    $networkService = new NetworkService();
                    $networkService->id = $row["id"];
                    $networkService->name = $row["name"];
                    array_push($aService, $networkService);
                }
            }
        }
        return $aService;
    }
    
    
}