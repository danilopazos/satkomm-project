<?php
class Dto_Network extends Dto_Db {
    
    public function getAll(){
      $query = "select * from network";
      $rows = $this->selectRows($query);
      if(!$rows){return false;}
      if(count($rows) == 0){ return false; }
      $aRecord = array();
      foreach ($rows as $row){
          array_push($aRecord, $this->getEntityByRow($row) );
      }
      return $aRecord;
    }
    
    public function getEntityByRow($_row){
        $e = new Network();
        $e->code = trim($_row["code"]);
        $e->description = trim($_row["description"]);
        return $e;
    }
   
}
