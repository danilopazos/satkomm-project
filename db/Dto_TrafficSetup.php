<?php

class Dto_TrafficSetup extends Dto_Db {

    public function registerBatch($_aRecord) {
        $query = "insert into ait_trafficsetup (planid, planidcrypt, ttcode_source, ";
        $query .="ttcode_dest, tunitcode, chargecode, chargeammount , dateregistered, createdby ) ";
        $query .="values  ";
        $total = count($_aRecord);
        if ($total == 0) {
            return false;
        }
        for ($i = 0; $i < $total; $i++) {
            $e = $_aRecord[$i];
            $query .=" ( " . $e->planid . " , '" . $e->planidcrypt . "' , '" . $e->ttcode_source . "' ,  ";
            $query .=" '" . $e->ttcode_dest . "' , '" . $e->tunitcode . "' , '" . $e->chargecode . "' ,  ";
            $query .=" " . $e->chargeammount . " , CURRENT_TIMESTAMP() , '" . $e->createdby . "' ) ";
            if (($i + 1) < $total) {
                $query .=" , ";
            }
        }

        return $this->executeUpdate($query);
    }

    public function deleteBatch($_planidcrypt) {
        $query = "update ait_trafficsetup set deleted = 1 where planidcrypt = '$_planidcrypt'";
        return $this->executeUpdate($query);
    }

    public function getByPlanId($_planid) {
        $query = "select ts.* , ";
        $query .=" source_ttype.description as ttcode_source_description , ";
        $query .=" dest_ttype.description as ttcode_dest_description ";
        $query .=" from ait_trafficsetup ts ";
        $query .= "left join ait_traffictype source_ttype on source_ttype.code = ts.ttcode_source ";
        $query .= "left join ait_traffictype dest_ttype on dest_ttype.code = ts.ttcode_dest ";
        $query .="where ts.planid = $_planid and ts.deleted = 0 order by ts.id asc";


        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            array_push($aRecord, $this->getEntityByRow($row));
        }
        return $aRecord;
    }

    public function getEntityByRow($_row) {
        $e = new TrafficSetup();
        $e->id = intval($_row["id"]);
        $e->planid = intval($_row["planid"]);
        $e->planidcrypt = trim($_row["planidcrypt"]);
        $e->ttcode_source = trim($_row["ttcode_source"]);
        $e->ttcode_source_description = trim($_row["ttcode_source_description"]);
        $e->ttcode_dest = trim($_row["ttcode_dest"]);
        $e->ttcode_dest_description = trim($_row["ttcode_dest_description"]);
        $e->tunitcode = trim($_row["tunitcode"]);
        $e->chargecode = trim($_row["chargecode"]);


        $e->chargeammount = doubleval($_row["chargeammount"]);

        $e->dateregistered = trim($_row["dateregistered"]);
        $e->createdby = trim($_row["createdby"]);
        return $e;
    }

}
