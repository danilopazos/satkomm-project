<?php

class Dto_User extends Dto_Db {

    public function create($_user){
        
        $_id = $_user->id;
        $_accountid = $_user->accountid;
        $_profileid = $_user->profileid;
        $_status = $_user->status;
        $_username = addslashes( $_user->username);
        $_pass = addslashes( $_user->pass);
        $_firstname = addslashes( $_user->firstname);
        $_lastname = addslashes( $_user->lastname);
        $_phone = addslashes( $_user->phone);
        $_phoneext = addslashes( $_user->phoneext);
        $_mobile = addslashes( $_user->mobile);
     
        $_position = addslashes( $_user->position);
        $_country = addslashes( $_user->country);
        $_state = addslashes( $_user->state);
        $_city = addslashes( $_user->city);
        $_street = addslashes( $_user->street);
        $_zipcode = addslashes( $_user->zipcode);
        $_pobox = addslashes( $_user->pobox);
        
        
        $query =  "insert into ait_usersystem (id, accountid, profileid, status, username, pass, firstname, ";
        $query .= "  lastname, phone, phoneext, mobile, position, country, state, city, street, zipcode, pobox ) ";
        $query .= "  values('$_id' , $_accountid, '$_profileid', $_status , '$_username' , md5('$_pass') , '$_firstname' , ";
        $query .= "  '$_lastname'  , '$_phone' , '$_phoneext' , '$_mobile', '$_position' , '$_country' , '$_state' , '$_city', '$_street' , '$_zipcode' , '$_pobox' ) ";
            

        return $this->executeUpdate($query);
        
    }
    
    public function update($_user){
        
        $_id = $_user->id;
        $_firstname = addslashes( $_user->firstname);
        $_lastname = addslashes( $_user->lastname);
        $_phone = addslashes( $_user->phone);
        $_phoneext = addslashes( $_user->phoneext);
        $_mobile = addslashes( $_user->mobile);
        $_profileid= trim( $_user->profileid);
         
        $_position = addslashes( $_user->position);
        $_country = addslashes( $_user->country);
        $_state = addslashes( $_user->state);
        $_city = addslashes( $_user->city);
        $_street = addslashes( $_user->street);
        $_zipcode = addslashes( $_user->zipcode);
        $_pobox = addslashes( $_user->pobox);
        
        
        $query  =" update ait_usersystem set   profileid = '$_profileid'   ,   firstname = '$_firstname' , ";
        $query .=" lastname = '$_lastname' , phone = '$_phone' ,  phoneext = '$_phoneext' , mobile = '$_mobile' , ";
        $query .=" position = '$_position' , country = '$_country' ,  state = '$_state' , city = '$_city' , ";
        $query .=" street = '$_street' , zipcode = '$_zipcode' ,  pobox = '$_pobox'  ";
        $query .=" where id = '$_id'  ";
        
        return $this->executeUpdate($query);
        
    }
    
    public function updateProfile($_id, $_profileid){
        $query = "update ait_usersystem set profileid = '$_profileid' where id = '$_id' ";
        return $this->executeUpdate($query);
    }
    
    public function updateStatus($_id, $_status){
        $query = "update ait_usersystem set status  = $_status where id = '$_id' ";
        return $this->executeUpdate($query);
    }
    
    public function updatePassword($_id, $_pass){
        $query = "update ait_usersystem set pass = md5('$_pass') where id = '$_id' ";
        return $this->executeUpdate($query);
    }
    
    public function updateUsername($_id, $_username){
        $query = "update ait_usersystem set username = '$_username'  where id = '$_id' ";
        return $this->executeUpdate($query);
    }
    
    public function getIdByUsername($_username){
        
        $query ="select id from ait_usersystem where username = '$_username' and deleted = 0 ";
      
        return trim($this->selectOneColumn("id", $query));
        
    }
        
    public function delete($_id){
        $query = "update ait_usersystem set deleted = 1 where id = '$_id' ";
        return $this->executeUpdate($query);
    }
    
    public function getById($_id){
        //$query = "select * from ait_usersystem where id = '$_id'   and deleted = 0 ";
        $query = "select ac.name as account_name, ad.country as account_shippingcountry ,  ap.id as parentaccountid,   u.* from ait_usersystem u  ";
        $query .=" right join ait_account ac on ac.id = u.accountid  ";
        $query .=" left join ait_account ap on ap.id = ac.parentaccountid ";
        $query .=" left join ait_address ad on ad.id = ac.shipaddressid ";
        $query .=" where u.id = '$_id'   and u.deleted = 0  ";         
 
        $row = $this->selectOneRow($query);
        $user = new User();
        if($row){
            $user->id = $_id;
            $user->accountid = $row["accountid"];
            $user->parentaccountid = $row["parentaccountid"];
            $user->profileid = $row["profileid"];
            $user->status = $row["status"];
            $user->firstname = $row["firstname"];
            $user->lastname = $row["lastname"];
            $user->phone = $row["phone"];
            $user->phoneext = $row["phoneext"];
            $user->mobile = $row["mobile"];
            $user->username = $row["username"];
            $user->lang = $row["lang"];
            $user->account_name = $row["account_name"];
            
            $user->position = $row["position"];
            $user->country = $row["country"];
            $user->state = $row["state"];
            $user->city = $row["city"];
            $user->street = $row["street"];
            $user->zipcode = $row["zipcode"];
            $user->pobox = $row["pobox"];
           
            $user->account_shippingcountry = trim($row["account_shippingcountry"]);
        }
        return $user;
    }
    
    public function getByAccountAndProfile($_accountid, $_profileid){
        $query = "select * from ait_usersystem where accountid = $_accountid and profileid =  '$_profileid' ";
        $rows = $this->selectRows($query);
        
        if(!$rows){return false;}
        $total   = count($rows);
        if($total == 0 ){return false;}
        $aRecords = array();
        foreach($rows as $row){
            $user = new User();
            $user->accountid = $row["accountid"];
            $user->parentaccountid = $row["parentaccountid"];
            $user->profileid = $row["profileid"];
            $user->status = $row["status"];
            $user->firstname = $row["firstname"];
            $user->lastname = $row["lastname"];
            $user->phone = $row["phone"];
            $user->phoneext = $row["phoneext"];
            $user->mobile = $row["mobile"];
            $user->username = $row["username"];
            array_push($aRecords, $user);
        }
        return $aRecords;
    }
    
    public function getByUsername($_username){
        //$query = "select * from ait_usersystem where id = '$_id'   and deleted = 0 ";
        $query = "select  ap.id as parentaccountid,   u.* from ait_usersystem u  ";
        $query .=" right join ait_account ac on ac.id = u.accountid  ";
        $query .=" left join ait_account ap on ap.id = ac.parentaccountid ";
        $query .=" where u.username = '$_username'   and u.deleted = 0  ";         
 
        $row = $this->selectOneRow($query);
        $user = new User();
        if($row){
            $user->id = trim($row["id"]);
            $user->accountid = $row["accountid"];
            $user->parentaccountid = $row["parentaccountid"];
            $user->profileid = $row["profileid"];
            $user->status = $row["status"];
            $user->firstname = $row["firstname"];
            $user->lastname = $row["lastname"];
            $user->phone = $row["phone"];
            $user->phoneext = $row["phoneext"];
            $user->mobile = $row["mobile"];
            $user->username = $row["username"];
            
        }
        return $user;
    }
    
    public function getByAccountid($_accountid){
        $query = "select * from ait_usersystem where accountid = $_accountid and deleted = 0 ";
         $rows = $this->selectRows($query);
        $aUser = array();
        if($rows){
            if(count($rows) > 0 ){
                
                foreach($rows as $row){
                    $user = new User();
                    $user->id  = $row["id"];
                    $user->accountid = $row["accountid"];
                    $user->profileid = $row["profileid"];
                    $user->status = $row["status"];
                    $user->firstname = $row["firstname"];
                    $user->lastname = $row["lastname"];
                    $user->phone = $row["phone"];
                    $user->phoneext = $row["phoneext"];
                    $user->mobile = $row["mobile"];
                    $user->username = $row["username"];
                    
                    array_push($aUser, $user);
                    
                }
                
            }
        }
        return $aUser;
    }
      
    public function generateId($_accountid){
        
        $rand = rand(1000, 9999);
        $newId = $_accountid.'-'.date('ymdhis').'-'.$rand;
        return $newId;
        
    }
    
    public function updatemyprofile($_user){
        $firstname = addslashes($_user->firstname);
        $_lastname = addslashes($_user->lastname);
        $_mobile = addslashes($_user->mobile);
        $_phone = addslashes($_user->phone);
        $_phoneext = addslashes($_user->phoneext);
        $_lang = addslashes($_user->lang);
        $_id = addslashes($_user->id);
        $query = "update ait_usersystem set lang='$_lang',  firstname = '$firstname' , lastname = '$_lastname' , mobile = '$_mobile'  , phone = '$_phone' , phoneext = '$_phoneext' where id = '$_id' ";
        return $this->executeUpdate($query);
    }
    
   public function checkPassword($_userid, $_password){
        $user = new User();
        $user->id = '';
        
        $query = "select id, status from ait_usersystem where id = '$_userid' and pass = md5('$_password')  limit 1  ";
        
        $row = $this->selectOneRow($query);
        
        if($row){
            if(trim($row ["id"]) != ""){
                $user->id = trim($row ["id"]);
                $user->status = intval($row ["status"]);
            }
        }
      
        return $user;
        
    }    
    
}
