<?php

class Dto_CreditCard extends Dto_Db {

    public function create($_creditCard){
        
        $_id =$_creditCard->id;
        $_accountid = $_creditCard->accountid;
        $_number = $_creditCard->number;
        $_verificationcode = $_creditCard->verificationcode;
        $_expmonth = $_creditCard->expmonth;
        $_expyear  = $_creditCard->expyear;
        $_cardholdername = $_creditCard->cardholdername;
        $_addressid = $_creditCard->addressid;
        $_type = $_creditCard->type;
        $_registeruser = $_creditCard->registeruser;
        
        $query = "insert into creditcard (id, accountid, number, verificationcode, expmonth, expyear, cardholdername, type, addressid, registeruser, registerip, registerdatetime )";
        $query .=" values( '$_id' , $_accountid , '$_number' , '$_verificationcode' , '$_expmonth' , '$_expyear' , '$_cardholdername' , '$_type'  ,   $_addressid, $_registeruser , '".  get_ip()."' , CURRENT_TIMESTAMP()   ";
        
        return $this->executeUpdate($query);
        
    }
    
    public function delete($_id){
        $query = "update creditcard set deleted = 1 where id = '$_id'";
        return $this->executeUpdate($query);
    }
    
    public function update($_creditCard){
        
        $_id =$_creditCard->id;
        $_number = $_creditCard->number;
        $_verificationcode = $_creditCard->verificationcode;
        $_expmonth = $_creditCard->expmonth;
        $_expyear  = $_creditCard->expyear;
        $_cardholdername = $_creditCard->cardholdername;
        $_addressid = $_creditCard->addressid;
        $_type = $_creditCard->type;
        $_registeruser = $_creditCard->registeruser;
        
        $query = "update creditcard set number = '$_number' , verificationcode = '$_verificationcode'  , expmonth = '$_expmonth' , expyear = '$_expyear'  , cardholdername = '$_cardholdername' ";
        $query .=" addressid = $_addressid , type = '$_type' , registeruser = $_registeruser, registerip = '".  get_ip()."' registerdatetime = CURRENT_TIMESTAMP() where id = '$_id'  ";
    
        return $this->executeUpdate($query);
        
    }
    
    public function getById($_id){
        $query = "select cc.* , a.country, a.state, a.city, a.street, a.zipcode, a.pobox from creditcard cc left join addresss a on a.id = cc.addressid where cc.id = '$_id' and deleted = 0  ";
        $row = $this->selectOneRow($query);
        
        $creditCard = new CreditCard();
        $address = new Address();
        
        if($row){
            $creditCard->id = $_id;
            $creditCard->number = $row["number"];
            $creditCard->verificationcode = $row["verificationcode"];
            $creditCard->expmonth = $row["expmonth"];
            $creditCard->expyear = $row["expyear"];
            $creditCard->cardholdername = $row["cardholdername"];
            $creditCard->addressid = $row["addressid"];
            $creditCard->type = $row["type"];
            $creditCard->registerdatetime = $row["registerdatetime"];
            $creditCard->registerip = $row["registerip"];
            $creditCard->registeruser = $row["registeruser"];
            
            $address->country = $row["country"];
            $address->state = $row["state"];
            $address->city = $row["city"];
            $address->street = $row["street"];
            $address->zipcode = $row["zipcode"];
            $address->pobox = $row["pobox"];
            
            $creditCard->address = $address;
            
        }
        
        return $creditCard;
    }
    
    public function getByAccountid($_accountid){
        $query = "select cc.* , a.country, a.state, a.city, a.street, a.zipcode, a.pobox from creditcard cc left join addresss a on a.id = cc.addressid where accountid = $_accountid and deleted = 0  ";
        $rows = $this->selectRows($query);
        $aCreditCard = array();
        if($rows){
            
            if(count($rows) > 0){
                
                foreach($rows as $row){
                    
                    $creditCard->id = $row["id"];
                    $creditCard->number = $row["number"];
                    $creditCard->verificationcode = $row["verificationcode"];
                    $creditCard->expmonth = $row["expmonth"];
                    $creditCard->expyear = $row["expyear"];
                    $creditCard->cardholdername = $row["cardholdername"];
                    $creditCard->addressid = $row["addressid"];
                    $creditCard->type = $row["type"];
                    $creditCard->registerdatetime = $row["registerdatetime"];
                    $creditCard->registerip = $row["registerip"];
                    $creditCard->registeruser = $row["registeruser"];

                    $address->country = $row["country"];
                    $address->state = $row["state"];
                    $address->city = $row["city"];
                    $address->street = $row["street"];
                    $address->zipcode = $row["zipcode"];
                    $address->pobox = $row["pobox"];

                    $creditCard->address = $address;
                    
                    array_push($aCreditCard, $row);
                    
                }
                
                
            }
            
        }
        return $aCreditCard;
    }
    
    public function generateId($_accountid){
        
        $newid = $_accountid.'-'.date('ymdhms').'-'.rand(10000, 99999);
        return $newid;
    }
    
}
