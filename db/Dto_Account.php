<?php
class Dto_Account extends Dto_Db {
    
    public function getAccountConfig($_accountid){
        $query = "select confquotevaliddays, invoiceprefix , quoteprefix,  confinvoicedue, conftaxespercent from ait_account where id = $_accountid ";
        $_row = $this->selectOneRow($query);
        if(!$_row){ return false;}
        
        $_e = new AccountConfig();
        $_e->confinvoicedue = intval($_row["confinvoicedue"]);
        $_e->confquotevaliddays = intval($_row["confquotevaliddays"]);
        $_e->conftaxespercent = doubleval($_row["conftaxespercent"]);
        
        $_e->invoiceprefix = trim($_row["invoiceprefix"]);
        $_e->quoteprefix = trim($_row["quoteprefix"]);
        
        return $_e;
    }
    
    
    public function updateStyleColor($_accountid, $_stylecolor) {
        $query = "update ait_account set stylecolor = '$_stylecolor' where id = $_accountid";
        return $this->executeUpdate($query);
    }

    public function getLogoBlobByAccountid($_accountid) {
        $query = "SELECT  mb.original_blob  ";
        $query .= "FROM media  m  ";
        $query .= "RIGHT JOIN mediablob mb on mb.media_id = m.id ";
        $query .= "where m.entity_table = 'ACCOUNT' AND m.entity_id = $_accountid AND m.ismain = 1 and m.type = 'IMAGE' ";

        $row = $this->selectOneRow($query);
        return $row["original_blob"];
    }

    public function getAccountBySubdomain($_subdomain) {
        $query = "select id from ait_account where subdomain = '$_subdomain' and deleted = 0  ";
        return $this->selectOneColumn("id", $query);
    }

    public function getLogoIdByAccountid($_accountid) {
        $query = "SELECT  m.idcrypt  ";
        $query .= "FROM media  m  ";
        $query .= "where m.entity_table = 'ACCOUNT' AND m.entity_id = $_accountid AND m.ismain = 1 and m.type = 'IMAGE' ";

        $row = $this->selectOneRow($query);
        return trim($row["idcrypt"]);
    }

    public function setLogoByAccountid($accountid, $original_blob, $fileext, $filename) {

        $mediaCryptId = $this->getLogoIdByAccountid($accountid);
        $ok = false;
        if ($mediaCryptId == "") {
            $mediaid = md5(date('Ymdhis') . '' . rand(10000, 99999));
            $queryInsert = "insert into media (idcrypt, entity_table, entity_id, ismain, type) values  ";
            $queryInsert .= "( '$mediaid' , 'ACCOUNT' , $accountid, 1, 'IMAGE' ) ";

            $resultInsert = $this->executeUpdate($queryInsert);

            if ($resultInsert) {

                $id = intval($this->selectOneColumn("id", "select id from media where idcrypt =  '$mediaid'  "));

                $query = "insert into mediablob (media_id, original_blob, fileext, filename) values( $id , '$original_blob' , '$fileext' , '$filename')  ";

                $ok = $this->executeUpdate($query);
            }
        } else {
            $id = intval($this->selectOneColumn("id", "select id from media where idcrypt =  '$mediaCryptId'  "));
            $query = "update mediablob set original_blob = '$original_blob' , fileext = '$fileext' , filename = '$filename' where media_id =  $id  ";
            $ok = $this->executeUpdate($query);
        }
        return $ok;
    }

    public function setcreditterms($_userid, $_accountid, $_aCreditTerms) {
        $total = count($_aCreditTerms);
        if ($total == 0) {
            return false;
        }
        $query = " update account_creditterms set status = 0 , updateddate = CURRENT_TIMESTAMP() , updatedbyuser = $_userid  ";
        $query .= " where accountid = $_accountid and status = 1  ";
        $result = $this->executeUpdate($query);

        $insert_result = false;
        if ($result) {
            $query2 = "insert into account_creditterms ( accountid, credittermsid, registerbyuser, registerdate, status ) values  ";
            for ($i = 0; $i < $total; $i++) {
                $credittermsid = $_aCreditTerms[$i];
                $query2 .= " ( $_accountid,   $credittermsid , $_userid, CURRENT_TIMESTAMP(), 1  )  ";
                if (($i + 1) < $total) {
                    $query2 .= " , ";
                }
            }
            $insert_result = $this->executeUpdate($query2);
        }
        return $insert_result;
    }

    public function getCreditTerms() {
        $query = "select * from creditterms";
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            $entity = new CreditTerms();
            $entity->id = $row["id"];
            $entity->days = $row["days"];
            array_push($aRecord, $entity);
        }
        return $aRecord;
    }

    public function getCreditTermsByAccount($_accountid) {
        $query = "select acct.id, acct.days from accountcreditterms acct ";
        $query .= "right join creditterms ct on ct.id = acct.credittermsid  ";
        $query .= " where acct.accountid = $_accountid and acct.status = 1 order by acct.days asc ";
    }

    public function create($_account) {
        $_id = $_account->id;
        $_code = addslashes($_account->code);
        $_countrycode = trim($_account->countrycode);
        $_name = addslashes($_account->name);
        $_email = addslashes($_account->email);
        $_phone = addslashes($_account->phone);
        $_website = addslashes($_account->website);
        $_phoneext = addslashes($_account->phoneext);
        $_billaddressid = $_account->billaddressid;
        $_shipaddressid = $_account->shipaddressid;
        $_parentaccountid = $_account->parentaccountid;
        $_ownerid = $_account->ownerid;
        $_createdbyuser = $_account->createdbyuser;

        $_subdomain = $_account->subdomain;

        $_taxid = addslashes($_account->taxid);
        $_taxtypeid = addslashes($_account->taxtypeid);
        
        $_creditlimit = $_account->creditlimit;
        $_invoiceduedays = $_account->invoiceduedays;
        $_currencyrelation = $_account->currencyrelation;
        $_confquotevaliddays = $_account->confquotevaliddays;
        $_confinvoicedue = $_account->confinvoicedue;
        $_conftaxespercent = $_account->conftaxespercent;
  
        $_invoiceprefix = addslashes($_account->invoiceprefix);
        $_quoteprefix = addslashes($_account->quoteprefix);        
        
        $_customplansactive = intval($_account->customplansactive);
        
        $query = "insert into ait_account ( id, defaultcurrency, taxid, taxtypeid,  parentaccountid, code, name, ";
        $query .=" email, website,  phone, phoneext, billaddressid, shipaddressid , subdomain, ownerid,  ";
        $query .="  creditlimit , invoiceduedays, confquotevaliddays, confinvoicedue, conftaxespercent ,  ";
        $query .=" createddatetime, createdbyuser , countrycode, invoiceprefix, quoteprefix , customplansactive) values ";
        $query .= " ( $_id, 'USD', '$_taxid' , '$_taxtypeid',       $_parentaccountid,   '$_code' , '$_name' , ";
        $query .=" '$_email' ,  '$_website' ,       '$_phone' , '$_phoneext' , $_billaddressid , $_shipaddressid , '$_subdomain' , '$_ownerid' , ";
        $query .= "   $_creditlimit, $_invoiceduedays, $_confquotevaliddays, $_confinvoicedue, $_conftaxespercent ,    ";
        $query .=" CURRENT_TIMESTAMP() , '$_createdbyuser' , '$_countrycode' , '$_invoiceprefix' , '$_quoteprefix' , $_customplansactive )   ";
        
        //echo $query; die();
        
        $ok = $this->executeUpdate($query);
        if ($ok) {
            $this->createCurrencyRelation($_parentaccountid, $_id, $_currencyrelation);
            $ok = $this->createTaxes($_id);
        }

        return $ok;
    }

    public function createCurrencyRelation($account, $customer, $currency){

        $query  = "insert into currencypercustomer (providerid, customerid, currencycode) ";
        $query .= "values ('$account', '$customer', '$currency')";

        return $this->executeUpdate($query);

    }

    public function createTaxes($_accountid) {
        $query = "insert into taxes (accountid, saletaxes, communicationtaxes) values ($_accountid, 6, 3.4) ";
        return $this->executeUpdate($query);
    }

    public function update($_account) {
        $_id = $_account->id;
        $_code = addslashes($_account->code);
        $_countrycode = trim($_account->countrycode);

        $_name = addslashes($_account->name);
        $_email = addslashes($_account->email);
        $_phone = addslashes($_account->phone);
        $_website = addslashes($_account->website);
        $_phoneext = addslashes($_account->phoneext);
        $_billaddressid = intval($_account->billaddressid);
        $_shipaddressid = intval($_account->shipaddressid);
        $_parentaccountid = $_account->parentaccountid;
        $_subdomain = addslashes($_account->subdomain);
        $_ownerid = $_account->ownerid;
        $_modifiedbyuser = $_account->modifiedbyuser;

        $_taxid = addslashes($_account->taxid);
        $_taxtypeid = addslashes($_account->taxtypeid);

        $_creditlimit = $_account->creditlimit;
        $_invoiceduedays = $_account->invoiceduedays;
        $_currencyrelation = $_account->currencyrelation;
        $_confquotevaliddays = $_account->confquotevaliddays;
        $_confinvoicedue = $_account->confinvoicedue;
        $_conftaxespercent = $_account->conftaxespercent;
  
        $_invoiceprefix = addslashes($_account->invoiceprefix);
        $_quoteprefix = addslashes($_account->quoteprefix);     
        
        $_customplansactive = intval($_account->customplansactive);

        $query = "update   ait_account  set code = '$_code' , name = '$_name' , ";
        $query .= "  creditlimit = $_creditlimit, invoiceduedays = $_invoiceduedays, confquotevaliddays = $_confquotevaliddays , ";
        $query .="  confinvoicedue = $_confinvoicedue , conftaxespercent = $_conftaxespercent,  ";
        $query .=" subdomain = '$_subdomain'  ,email = '$_email' , phone = '$_phone' , phoneext = '$_phoneext'  , billaddressid = $_billaddressid   ";
        $query .= " , taxid = '$_taxid' , taxtypeid = '$_taxtypeid' , shipaddressid = $_shipaddressid ,   website = '$_website' , ";
        $query .=" modifiedbyuser = '$_modifiedbyuser' , ownerid = '$_ownerid', modifieddatetime = CURRENT_TIMESTAMP() , ";
        $query .=" invoiceprefix = '$_invoiceprefix', quoteprefix = '$_quoteprefix' , ";
        $query .=" countrycode = '$_countrycode' , customplansactive = $_customplansactive  where id = $_id ";
        
        $this->updateCurrencyRelation($_parentaccountid, $_id, $_currencyrelation);
        $ok = $this->executeUpdate($query);
        return $ok;
    }

    public function updateCurrencyRelation($account, $customer, $currency){

        $query  = "select count(*) from currencypercustomer ";
        $query .= "where providerid='$account' and customerid='$customer'";

        $row = $this->selectOneRow($query);

        if($row[0] == 0){
            //not exist
            $query  = "insert into currencypercustomer (providerid, customerid, currencycode) ";
            $query .= "values ('$account', '$customer', '$currency')";

        }else{
            //exist
            $query  = "update currencypercustomer ";
            $query .= "set currencycode='$currency' ";
            $query .= "where providerid='$account' and customerid='$customer'";

        }

        return $this->executeUpdate($query);

    }

    public function delete($_id) {
        $query = "update ait_account set deleted = 1 where id = $_id";
        return $this->executeUpdate($query);
    }

    public function updateStatus($_id, $_status) {
        $query = "update ait_account set status = $_status where id = $_id ";
        return $this->executeUpdate($query);
    }

    public function infoToGeneratePdf($_providerid, $_customerid){
        
        $asign=false;

        $query  = "select A.id, A.logoimg, A.countrycode, A.headerinvoice, ";
        $query .= "A.formatinvoice,A.conftaxespercent, A.defaultcurrency ";
        $query .= "from ait_account A ";
        $query .= "where A.id=$_providerid ";
        $row = $this->selectOneRow($query);

        $information = new InfoToConsumptionPdf();

        if($row){
            $information->providerid = $row['id'];
            $information->logoimg = $row['logoimg'];
            $information->countrycode = $row['countrycode'];
            $information->headerinvoice = $row['headerinvoice'];
            $information->formatinvoice = $row['formatinvoice'];
            $information->taxammount = $row['conftaxespercent'];

            //validate relation
            $query  = "select R.currencycode ";
            $query .= "from currencypercustomer R ";
            $query .= "where R.providerid=$_providerid and R.customerid=$_customerid ";
            $currencyIso = $this->selectOneRow($query);
            
            if(strlen($currencyIso['currencycode'])>0){
                
                if($currencyIso['currencycode']=='USD'){
                    $asign=true; //default
                }else{
                    $query  = "select C.description, C.code, C.symbol, E.xrate ";
                    $query .= "from currency C, currencyexchange E ";
                    $query .= "where C.code=E.currency_code and E.isactive=true ";
                    $query .= "and E.accountid='$_providerid' and E.currency_code='".$currencyIso['currencycode']."'";
                    $currencies = $this->selectOneRow($query);
                    
                    $information->currencyDescription = $currencies['description'];
                    $information->currencyIso = $currencies['code'];
                    $information->currencySymbol = $currencies['symbol'];
                    $information->currencyExchange = $currencies['xrate'];
                }
            }else{
                if($row['defaultcurrency']=='USD'){
                    $asign=true; //default
                }else{
                    $query  = "select C.description, C.code, C.symbol, E.xrate ";
                    $query .= "from currency C, currencyexchange E ";
                    $query .= "where C.code=E.currency_code and E.isactive=true ";
                    $query .= "and E.accountid='$_providerid' and E.currency_code='".$row['defaultcurrency']."'";
                    $query .= "and  C.code='".$row['defaultcurrency']."'";
                    $currencies = $this->selectOneRow($query);
                    
                    $information->currencyDescription = $currencies['description'];
                    $information->currencyIso = $currencies['code'];
                    $information->currencySymbol = $currencies['symbol'];
                    $information->currencyExchange = $currencies['xrate'];
                }
            }

            if($asign){ //default currency
                $information->currencyDescription = 'Dolares';
                $information->currencyIso = 'USD';
                $information->currencySymbol = '$';
                $information->currencyExchange = 1;
            }

        }

        return $information;
    }

    public function getCurrencyRelation($_providerid, $_customerid){

        $query  = "select C.currencycode from currencypercustomer C ";
        $query .= "where C.providerid='$_providerid' and C.customerid='$_customerid'";

        $row = $this->selectOneRow($query);

        if($row){
            return $row['currencycode'];
        }else{
            return false;
        }
    
    }
 
    
    public function getById($_id) {

        $query = "select a.*    ";
        $query .= " , cu.description ";
        $query .= " , ba.country as bill_country, ba.state as bill_state, ba.city as bill_city , ba.street as bill_street , ba.zipcode as bill_zipcode , ba.pobox as bill_pobox   ";
        $query .= " , sa.country as ship_country, sa.state as ship_state, sa.city as ship_city , sa.street as ship_street , sa.zipcode as ship_zipcode , sa.pobox as ship_pobox   ";
        $query .= "  from ait_account a   ";
        $query .= "  left join ait_address ba on ba.id = a.billaddressid ";
        $query .= "  left join ait_address sa on sa.id = a.shipaddressid ";
        $query .= "  left join currency cu on cu.code = a.defaultcurrency ";
        $query .= "  where a.id = $_id and a.deleted = 0 ";

        $shipaddress = new Address();
        $billaddress = new Address();
        $account = new Account();
        
        $row = $this->selectOneRow($query);
        if ($row) {

            $account->id = $row["id"];
            $account->code = $row["code"];
            $account->name = $row["name"];
            $account->email = $row["email"];
            $account->phone = $row["phone"];
            $account->phoneext = $row["phoneext"];
            $account->parentaccountid = $row["parentaccountid"];
            $account->billaddressid = $row["billaddressid"];
            $account->shipaddressid = $row["shipaddressid"];

            $account->headerinvoice = $row["headerinvoice"];
            $account->formatinvoice = $row["formatinvoice"];
            $account->taxammount = $row["taxammount"];

            $account->countrycode = $row["countrycode"];
            $account->descriptioncurrency = $row["description"];
            $account->defaultcurrency = $row["defaultcurrency"];
            $account->website = $row["website"];
            $account->taxtypeid = $row["taxtypeid"];
            $account->taxid = $row["taxid"];
            $account->subdomain = trim($row["subdomain"]);
            $account->stylecolor = trim($row["stylecolor"]);
            $account->status = intval($row["status"]);

            $account->ownerid = trim($row["ownerid"]);

            $billaddress->country = $row["bill_country"];
            $billaddress->state = $row["bill_state"];
            $billaddress->city = $row["bill_city"];
            $billaddress->street = $row["bill_street"];
            $billaddress->zipcode = $row["bill_zipcode"];
            $billaddress->pobox = $row["bill_pobox"];

            $shipaddress->country = $row["ship_country"];
            $shipaddress->state = $row["ship_state"];
            $shipaddress->city = $row["ship_city"];
            $shipaddress->street = $row["ship_street"];
            $shipaddress->zipcode = $row["ship_zipcode"];
            $shipaddress->pobox = $row["ship_pobox"];
 

            $account->creditlimit = $row["creditlimit"];
            $account->invoiceduedays = $row["invoiceduedays"];
            $account->confquotevaliddays = $row["confquotevaliddays"];
            $account->confinvoicedue = $row["confinvoicedue"];
            $account->conftaxespercent = $row["conftaxespercent"];

            $account->invoiceprefix = $row["invoiceprefix"];
            $account->quoteprefix   = $row["quoteprefix"];
            
            
            $account->billaddress = $billaddress;
            $account->shipaddress = $shipaddress;
            
            
            $account->customplansactive = intval($row["customplansactive"]);
        }

        return $account;
    }

    public function getListByName($_parentaccountid, $_search, $_orderby, $_orderdir, $limit1, $limit2) {

        $query = "select a.*    ";
        $query .= " , ba.country as bill_country, ba.state as bill_state, ba.city as bill_city , ba.street as bill_street , ba.zipcode as bill_zipcode , ba.pobox as bill_pobox   ";
        $query .= " , sa.country as ship_country, sa.state as ship_state, sa.city as ship_city , sa.street as ship_street , sa.zipcode as ship_zipcode , sa.pobox as ship_pobox   ";
        $query .= "  from ait_account a   ";
        $query .= "  left join ait_address ba on ba.id = a.billaddressid ";
        $query .= "  left join ait_address sa on sa.id = a.shipaddressid ";
        $query .= "  where   a.deleted = 0 and parentaccountid = $_parentaccountid ";

        if ($_search != "") {
            $query .= " and (      name  like '%$_search%'  ) ";
        }

        $query .= " order by $_orderby  $_orderdir  ";

        $query .= "  limit $limit1, $limit2  ";


        $aAccount = array();
        $rows = $this->selectRows($query);
        if ($rows) {

            if (count($rows) > 0) {
                foreach ($rows as $row) {

                    $shipaddress = new Address();
                    $billaddress = new Address();
                    $account = new Account();

                    $account->id = $row["id"];
                    $account->code = $row["code"];
                    $account->name = $row["name"];
                    $account->email = $row["email"];
                    $account->phone = $row["phone"];
                    $account->phoneext = $row["phoneext"];
                    $account->parentaccountid = $row["parentaccountid"];
                    $account->taxid = $row["taxid"];
                    $account->taxtypeid = $row["taxtypeid"];
                    $account->billaddressid = $row["billaddressid"];
                    $account->shipaddress = $row["shipaddressid"];
                    $account->subdomain = trim($row["subdomain"]);
                    $account->stylecolor = trim($row["stylecolor"]);

                    $billaddress->country = $row["bill_country"];
                    $billaddress->state = $row["bill_state"];
                    $billaddress->city = $row["bill_city"];
                    $billaddress->street = $row["bill_street"];
                    $billaddress->zipcode = $row["bill_zipcode"];
                    $billaddress->pobox = $row["bill_pobox"];

                    $shipaddress->country = $row["ship_country"];
                    $shipaddress->state = $row["ship_state"];
                    $shipaddress->city = $row["ship_city"];
                    $shipaddress->street = $row["ship_street"];
                    $shipaddress->zipcode = $row["ship_zipcode"];
                    $shipaddress->pobox = $row["ship_pobox"];

                    $account->billaddress = $billaddress;
                    $account->shipaddress = $shipaddress;

                    $account->creditlimit = $row["creditlimit"];
                    $account->invoiceduedays = $row["invoiceduedays"];
                    $account->confquotevaliddays = $row["confquotevaliddays"];
                    $account->confinvoicedue = $row["confinvoicedue"];
                    $account->conftaxespercent = $row["conftaxespercent"];

                    $account->invoiceprefix = $row["invoiceprefix"];
                    $account->quoteprefix   = $row["quoteprefix"];         
                    
                    $account->customplansactive = intval($row["customplansactive"]);
                    
                    array_push($aAccount, $account);
                }
            }
        }

        return $aAccount;
    }

    public function getList($_parentaccountid, $_search, $_orderby, $_orderdir, $limit1, $limit2) {

        $query = "select a.*    ";
        $query .= " , ba.country as bill_country, ba.state as bill_state, ba.city as bill_city , ba.street as bill_street , ba.zipcode as bill_zipcode , ba.pobox as bill_pobox   ";
        $query .= " , sa.country as ship_country, sa.state as ship_state, sa.city as ship_city , sa.street as ship_street , sa.zipcode as ship_zipcode , sa.pobox as ship_pobox   ";
        $query .= "  from ait_account a   ";
        $query .= "  left join ait_address ba on ba.id = a.billaddressid ";
        $query .= "  left join ait_address sa on sa.id = a.shipaddressid ";
        $query .= "  where   a.deleted = 0 and parentaccountid = $_parentaccountid ";

        if ($_search != "") {
            $query .= " and (  ";
            $query .= "    code  like '%$_search%'  ";
            $query .= "  or name  like '%$_search%'  ";
            $query .= "  or email  like '%$_search%'  ";
            $query .= "  or website  like '%$_search%'  ";
            $query .= "  or phone  like '%$_search%'  ";
            $query .= "  or taxid  like '%$_search%'  ";

            $query .= "  or ba.country  like '%$_search%'  ";
            $query .= "  or ba.state  like '%$_search%'  ";
            $query .= "  or ba.city  like '%$_search%'  ";
            $query .= "  or ba.street  like '%$_search%'  ";
            $query .= "  or ba.zipcode  like '%$_search%'  ";
            $query .= "  or ba.pobox  like '%$_search%'  ";

            $query .= "  or sa.country  like '%$_search%'  ";
            $query .= "  or sa.state  like '%$_search%'  ";
            $query .= "  or sa.city  like '%$_search%'  ";
            $query .= "  or sa.street  like '%$_search%'  ";
            $query .= "  or sa.zipcode  like '%$_search%'  ";
            $query .= "  or sa.pobox  like '%$_search%'  ";

            $query .= " ) ";
        }

        $query .= " order by $_orderby  $_orderdir  ";

        $query .= "  limit $limit1, $limit2  ";


        $aAccount = array();
        $rows = $this->selectRows($query);
        if ($rows) {

            if (count($rows) > 0) {
                foreach ($rows as $row) {

                    $shipaddress = new Address();
                    $billaddress = new Address();
                    $account = new Account();

                    $account->id = $row["id"];
                    $account->code = $row["code"];
                    $account->name = $row["name"];
                    $account->email = $row["email"];
                    $account->phone = $row["phone"];
                    $account->phoneext = $row["phoneext"];
                    $account->parentaccountid = $row["parentaccountid"];
                    $account->taxid = $row["taxid"];
                    $account->taxtypeid = $row["taxtypeid"];
                    $account->billaddressid = $row["billaddressid"];
                    $account->shipaddress = $row["shipaddressid"];
                    $account->subdomain = trim($row["subdomain"]);
                    $account->stylecolor = trim($row["stylecolor"]);

                    $billaddress->country = $row["bill_country"];
                    $billaddress->state = $row["bill_state"];
                    $billaddress->city = $row["bill_city"];
                    $billaddress->street = $row["bill_street"];
                    $billaddress->zipcode = $row["bill_zipcode"];
                    $billaddress->pobox = $row["bill_pobox"];

                    $shipaddress->country = $row["ship_country"];
                    $shipaddress->state = $row["ship_state"];
                    $shipaddress->city = $row["ship_city"];
                    $shipaddress->street = $row["ship_street"];
                    $shipaddress->zipcode = $row["ship_zipcode"];
                    $shipaddress->pobox = $row["ship_pobox"];

                    $account->billaddress = $billaddress;
                    $account->shipaddress = $shipaddress;


                    $account->creditlimit = $row["creditlimit"];
                    $account->invoiceduedays = $row["invoiceduedays"];
                    $account->confquotevaliddays = $row["confquotevaliddays"];
                    $account->confinvoicedue = $row["confinvoicedue"];
                    $account->conftaxespercent = $row["conftaxespercent"];

                    
                    $account->invoiceprefix = $row["invoiceprefix"];
                    $account->quoteprefix   = $row["quoteprefix"];      
                    
                    
                    $account->customplansactive = intval($row["customplansactive"]);
                    
                    array_push($aAccount, $account);
                }
            }
        }

        return $aAccount;
    }

    public function getListTotal($_parentaccountid, $_search) {

        $query = "select  count(*) as total   ";
        $query .= "  from ait_account a   ";
        $query .= "  left join ait_address ba on ba.id = a.billaddressid ";
        $query .= "  left join ait_address sa on sa.id = a.shipaddressid ";
        $query .= "  where  a.deleted = 0 and parentaccountid = $_parentaccountid   ";

        if ($_search != "") {
            $query .= " and (  ";
            $query .= "  code  like '%$_search%'  ";
            $query .= "  or name  like '%$_search%'  ";
            $query .= "  or email  like '%$_search%'  ";
            $query .= "  or website  like '%$_search%'  ";
            $query .= "  or phone  like '%$_search%'  ";
            $query .= "  or taxid  like '%$_search%'  ";

            $query .= "  or ba.country  like '%$_search%'  ";
            $query .= "  or ba.state  like '%$_search%'  ";
            $query .= "  or ba.city  like '%$_search%'  ";
            $query .= "  or ba.street  like '%$_search%'  ";
            $query .= "  or ba.zipcode  like '%$_search%'  ";
            $query .= "  or ba.pobox  like '%$_search%'  ";

            $query .= "  or sa.country  like '%$_search%'  ";
            $query .= "  or sa.state  like '%$_search%'  ";
            $query .= "  or sa.city  like '%$_search%'  ";
            $query .= "  or sa.street  like '%$_search%'  ";
            $query .= "  or sa.zipcode  like '%$_search%'  ";
            $query .= "  or sa.pobox  like '%$_search%'  ";

            $query .= " ) ";
        }

        return intval($this->selectOneColumn("total", $query));
    }

    public function getIdByCode($_code) {

        $query = "select id from ait_account where code = '$_code' ";
        $id = $this->selectOneColumn("id", $query);
        return intval($id);
    }

    public function generateNewId() {
        $query = "select count(*) as total from ait_account ";
        $total = intval($this->selectOneColumn("total", $query));

        $newId = 10000 + ( $total + 1);
        return $newId;
    }

    public function generateNewCode($_parentaccountid) {
        $query = "select count(*) as total from ait_account where parentaccountid = $_parentaccountid ";
        $total = intval($this->selectOneColumn("total", $query));

        // $newId = 10000 + ( $total + 1);
        $newId = $total . '' . rand(10000, 99999);

        return $newId;
    }

    public function gettotalchidls($_parentaccountid) {

        $query = "select count(*) as total from ait_account where parentaccountid = $_parentaccountid ";
        return intval($this->selectOneColumn("total", $query));
    }

    public function getMyCustomersAndMe($_myAccountId) {
        $query = "SELECT id , name from ait_account where parentaccountid = $_myAccountId or id = $_myAccountId ";
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            $account = new Account();
            $account->id = $row["id"];
            $account->name = $row["name"];
            array_push($aRecord, $account);
        }
        return $aRecord;
    }

}
