<?php
class Dto_Address extends Dto_Db{
    
    
    
   public function create($_type, $_country, $_state, $_city, $_street, $_zipcode, $_pobox){
       $_country = addslashes($_country);
       $_state = addslashes($_state);
       $_city = addslashes($_city);
       $_street = addslashes($_street);
       $_zipcode = addslashes($_zipcode);
       $_pobox = addslashes($_pobox);
       
       $randunique = date('ymdhis').'-'.rand(1000,99999);
       
       $query = "insert into ait_address (type, country, state, city, street, zipcode, pobox, randunique) ";
       $query .="  values( '$_type' , '$_country' , '$_state' , '$_city' , '$_street' , '$_zipcode' , '$_pobox' , '$randunique'  )  ";
      
       $ok = $this->executeUpdate($query);
       
       $newAddressId = 0;
       if($ok){
           
           $newAddressId = $this->selectOneColumn("id", "select id from ait_address where randunique = '$randunique'");
           
       }
       
       return intval($newAddressId);
   
   }
   
   public function update( $id , $_country, $_state, $_city, $_street, $_zipcode, $_pobox ){
       $_country = addslashes($_country);
       $_state = addslashes($_state);
       $_city = addslashes($_city);
       $_street = addslashes($_street);
       $_zipcode = addslashes($_zipcode);
       $_pobox = addslashes($_pobox);
       
       $query = "update ait_address set country = '$_country' , state = '$_state'  , city = '$_city' , street = '$_street' , zipcode = '$_zipcode' , pobox = '$_pobox' where id = $id  ";
       //echo $query ; die();
       $ok = $this->executeUpdate($query);
       return $ok;
       
   }
   
   public function getById($id){
       $query = "select * from ait_address where id = $id ";
       $row = $this->selectOneRow($query);
       $address = new Address();
       if($row){
           $address->id = $row["id"];
           $address->type = $row["type"];
           $address->country = $row["country"];
           $address->state = $row["state"];
           $address->city = $row["city"];
           $address->street = $row["street"];
           $address->zipcode = $row["zipcode"];
           $address->pobox = $row["pobox"];
       }
       return $address;
   }
   
    
}