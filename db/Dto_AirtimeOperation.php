<?php

class Dto_AirtimeOperation extends Dto_Db {
    
    public function getByInventoryItemId($_inventoryitemid, $_operationstatus){
        $query = "SELECT * FROM  airtimeoperation WHERE inventoryitemid =$_inventoryitemid and operationstatus = $_operationstatus LIMIT 1 ";
        
        //echo $query; die();
        
        $row = $this->selectOneRow($query);
        if(!$row){return false;}
        return $this->setEntityByRow($row);
    }
    
    public function getListByInventoryItemId( $_inventoryitemid ){
        
        $query  = "SELECT ";
        $query .= "ao.id,  ";
        $query .= "ao.airtimeordercode, ao.operation, ao.datetoexecute, ";
        $query .= "ao.timetoexecute,  ao.processeddatetime , ";

        $query .= "pl1.idcrypt as pp_idcrypt,  ";
        $query .= "pl1.sku as pp_sku,  ";
        $query .= "pl1.network_code  as pp_networkcode,  ";
        $query .= "pl1.payment_period_code   as pp_payment_period_code,  ";
        $query .= "pl1.contract_period_code  as pp_contract_period_code,  ";
        $query .= "pl1.servicetype_code  as pp_servicetype_code,  ";
        $query .= "pl1.price   as pp_subscription_payment,  ";
        $query .= "pl1.fixedpayment   as pp_fixed_payment,  ";
        $query .= "pl1.activationpayment  as pp_activation_payment,  ";
        $query .= "pl1.suspensionpayment  as pp_suspension_payment,  ";
        $query .= "pl1.deactivationpayment  as pp_deactivation_payment, ";
        $query .= "ao.planname as pp_planname , ";

        $query .= "pl2.idcrypt as pc_idcrypt,  ";
        $query .= "pl2.sku as pc_sku,  ";
        $query .= "pl2.network_code  as pc_networkcode, ";
        $query .= "pl2.payment_period_code   as pc_payment_period_code,  ";
        $query .= "pl2.contract_period_code  as pc_contract_period_code, ";
        $query .= "pl2.servicetype_code  as pc_servicetype_code,  ";
        $query .= "pl2.price   as pc_subscription_payment,  ";
        $query .= "pl2.fixedpayment   as pc_fixed_payment,  ";
        $query .= "pl2.activationpayment  as pc_activation_payment,  ";
        $query .= "pl2.suspensionpayment  as pc_suspension_payment,  ";
        $query .= "pl2.deactivationpayment  as pc_deactivation_payment, ";
        $query .= "ao.plannameforcustomer as pc_planname  ";

        $query .= "FROM  ";
        $query .= "airtimeoperation ao  ";
        $query .= "left join plan pl1 on ao.planid = pl1.idcrypt and pl1.deleted = 0 ";
        $query .= "left join plan pl2 on ao.planidcryptforcustomer = pl2.idcrypt and pl2.deleted = 0 ";
        $query .= "WHERE ao.inventoryitemid =$_inventoryitemid and ao.operationstatus = 1 order by ao.requesteddatetime desc ";
        
        
        $aRecord = array();
        
        $_rows = $this->selectRows($query);
        if(!$_rows){return false;}
        $_total = count($_rows);
        if($_total == 0){return false;}
        
        foreach($_rows as $_row){
            $e = new InventoryAirtimeOperation();
            
            $e->id = trim($_row["id"]);
            $e->airtimeordercode = trim($_row["airtimeordercode"]);
            $e->operation = trim($_row["operation"]);
            $e->datetoexecute = trim($_row["datetoexecute"]);
            $e->timetoexecute = trim($_row["timetoexecute"]);
            $e->processeddatetime = trim($_row["processeddatetime"]);
            
            $e->pp_idcrypt              = trim($_row["pp_idcrypt"]);
            $e->pp_sku                  = trim($_row["pp_sku"]);
            $e->pp_networkcode          = trim($_row["pp_networkcode"]);
            $e->pp_payment_period_code  = trim($_row["pp_payment_period_code"]);
            $e->pp_contract_period_code = trim($_row["pp_contract_period_code"]);
            $e->pp_servicetype_code     = trim($_row["pp_servicetype_code"]);
            $e->pp_subscription_payment = doubleval($_row["pp_subscription_payment"]);
            $e->pp_fixed_payment        = doubleval($_row["pp_fixed_payment"]);   
            $e->pp_activation_payment   = doubleval($_row["pp_activation_payment"]);   
            $e->pp_suspension_payment   = doubleval($_row["pp_suspension_payment"]);   
            $e->pp_deactivation_payment = doubleval($_row["pp_deactivation_payment"]);   
            $e->pp_planname             = trim($_row["pp_planname"]);
            
            $e->pc_idcrypt              = trim($_row["pc_idcrypt"]);
            $e->pc_sku                  = trim($_row["pc_sku"]);
            $e->pc_networkcode          = trim($_row["pc_networkcode"]);
            $e->pc_payment_period_code  = trim($_row["pc_payment_period_code"]);
            $e->pc_contract_period_code = trim($_row["pc_contract_period_code"]);
            $e->pc_servicetype_code     = trim($_row["pc_servicetype_code"]);
            $e->pc_subscription_payment = doubleval($_row["pc_subscription_payment"]);
            $e->pc_fixed_payment        = doubleval($_row["pc_fixed_payment"]);   
            $e->pc_activation_payment   = doubleval($_row["pc_activation_payment"]);   
            $e->pc_suspension_payment   = doubleval($_row["pc_suspension_payment"]);   
            $e->pc_deactivation_payment = doubleval($_row["pc_deactivation_payment"]);   
            $e->pc_planname             = trim($_row["pc_planname"]);
            
            array_push($aRecord, $e);
            
        }//foreach
        
        return $aRecord;
    }//function
    
    
    
    public function setAirtimeOrderStatus($_id, $_status) {
        $query = "update airtimeorder set orderstatus = '$_status' where id = '$_id'  ";
        return $this->executeUpdate($query);
    }
    
    public function getOrdercodeByIserial($_iserial){
        
        $query = "select aorder.id ";
        $query .= "from airtimeoperation ao  ";
        $query .= "right join airtimeorder aorder on aorder.code = ao.airtimeordercode ";
        $query .= "where ao.iserial = '$_iserial' and ao.requeststatus = 'ordered' ";
        
        return trim( $this->selectOneColumn("id", $query)  );
    }
    
    
    public function setOrderCodeToAirtimeOperations($_code, $items) {
        $total = count($items);

        if ($total == 0) {
            return false;
        }

        $query = "update airtimeoperation set airtimeordercode = '$_code' , requeststatus = 'ordered' where  ";
        for ($i = 0; $i < $total; $i++) {
            
            $record = $items[$i];
              
            $query .="  id =  '" . trim($record["cartid"]) . "'  ";
            if (($i + 1) < $total) {
                $query .= " or  ";
            }
        }

        return $this->executeUpdate($query);
    }

    public function create($_entity, $langcode) {
        $id = $_entity->id;
        $dto_inventory = new Dto_Inventory();
        $dto_plan = new Dto_Plan();
     
        $inventoryitem = $dto_inventory->getItemByid($_entity->inventoryitemid);   
        $plan = $dto_plan->getPlanById($_entity->planid, $langcode);  
        

        $query = "insert into airtimeoperation (id, inventoryitemid, planid, accountid, accountassignedid, iserial, productname, productid, planname, ";
        $query .=" subscription_period, contract_period, activation_price, fixed_price, subscription_price,  datetoexecute, timetoexecute,  subtotal, taxes, paymentmethod, paymentmethod_price, totalpayment,  ";
        $query .=" operation,  operationstatus, requestedbyuserid, requesteddatetime, networkcode, servicetype_code, requeststatus, suspension_price , planidcryptforcustomer , plannameforcustomer , voicenumber, datanumber ) values ( ";
        
        $query .=" '$id', '" . $_entity->inventoryitemid . "' , '" . $_entity->planid . "' , " . $_entity->accountid . "  ,  " . $_entity->accountassignedid . "  ,  ";
        $query .=" '" . $inventoryitem->iserial . "' , '" . $inventoryitem->productname . "' , '" . $inventoryitem->productid . "' , '" . $plan->name . "' ,  ";
        $query .=" '" . $plan->payment_period_code . "' , '" . $plan->contract_period_code . "' , " . $plan->activationpayment . " , " . $plan->fixedpayment . " , " . $plan->price . "  ,  ";
        $query .=" '" . $_entity->datetoexecute . "' ,  '" . $_entity->timetoexecute . "' , " . $_entity->subtotal . " , " . $_entity->taxes . " , '" . $_entity->paymentmethod . "' ,  " . $_entity->paymentmethod_price . " , " . $_entity->totalpayment . " ,   ";
        $query .=" '" . $_entity->operation . "' ,  0 , '" . $_entity->requestedbyuserid . "' , '" . date('Y-m-d h:i:s') . "' , '" . $plan->networkcode . "'  , '" . $plan->servicetype_code . "' , 'requested' , ".doubleval($_entity->suspension_price)." , ";
        $query .=" '" . $_entity->planidcryptforcustomer . "' , '" . $_entity->plannameforcustomer . "' , '" . $_entity->voicenumber . "' , '" . $_entity->datanumber . "'  ";
        $query .=' ) ';
       
        return $this->executeUpdate($query);
    }
    

    
    
    public function deleteById($_id) {
        $query = "delete from airtimeoperation where id= '$_id' and requeststatus = 'requested'";

        return $this->executeUpdate($query);
    }
    
    public function getById($_id) {
        $query = "select a.* , ar.name as accountname, asg.name as accountassignedname , ";
        $query .=" ur.firstname as ur_firstname, ur.lastname as ur_lastname,  ";
        $query .=" up.firstname as up_firstname, up.lastname as up_lastname  ";
        $query .= "FROM airtimeoperation a ";
        $query .= "RIGHT JOIN ait_account ar on ar.id = a.accountid ";
        $query .= "RIGHT JOIN ait_account asg on asg.id = a.accountassignedid ";
        $query .= "RIGHT JOIN ait_usersystem ur on ur.id = a.requestedbyuserid  ";
        $query .= "LEFT  JOIN ait_usersystem up on up.id = a.processedbyuserid  ";
        $query .=" WHERE a.id = '$_id'   ";

        $row = $this->selectOneRow($query);
        if (!$row) {
            return false;
        }

        $entity = $this->setEntityByRow($row);
        $account = new Account();
        $accountassigned = new Account();
        $account->id = $row["accountid"];
        $account->name = $row["accountname"];
        $accountassigned->id = $row["accountassignedid"];
        $accountassigned->name = $row["accountassignedname"];
        $requesteduser = new User();
        $processeduser = new User();
        $requesteduser->id = $row["requestedbyuserid"];
        $requesteduser->firstname = $row["ur_firstname"];
        $requesteduser->lastname = $row["ur_lastname"];
        $processeduser->id = $row["processedbyuserid"];
        $processeduser->firstname = $row["up_firstname"];
        $processeduser->lastname = $row["up_lastname"];

        $entity->account = $account;
        $entity->accountassigned = $accountassigned;
        $entity->requestedbyuser = $requesteduser;
        $entity->processedbyuser = $processeduser;
        return $entity;
    }

    public function setEntityByRow($row) {
        $entity = new AirtimeOperation();
        $entity->id = $row["id"];

        $entity->operation = $row["operation"];
        $entity->requeststatus = $row["requeststatus"];
        $entity->operationstatus = $row["operationstatus"];
        
        $entity->servicestatus = trim($row["servicestatus"]);

        $entity->voicenumber = $row["voicenumber"];
        $entity->datanumber = $row["datanumber"];   
        
        $entity->faxnumber = $row["faxnumber"];
        $entity->imsi = $row["imsi"];
        
        $entity->planidcryptforcustomer = $row["planidcryptforcustomer"];
        $entity->plannameforcustomer = $row["plannameforcustomer"];

        $entity->inventoryitemid = $row["inventoryitemid"];
        $entity->planid = $row["planid"];
        $entity->accountid = $row["accountid"];
        $entity->accountassignedid = $row["accountassignedid"];
        $entity->iserial = $row["iserial"];
        $entity->productname = $row["productname"];
        $entity->productid = $row["productid"];
        $entity->planname = $row["planname"];
        $entity->networkcode = $row["networkcode"];
        $entity->servicetype_code = $row["servicetype_code"];
        $entity->subscription_period = $row["subscription_period"];
        $entity->contract_period = $row["contract_period"];
        $entity->subscription_price = $row["subscription_price"];
        $entity->activation_price = $row["activation_price"];
        $entity->suspension_price = $row["suspension_price"];
        $entity->deactivation_price = $row["deactivation_price"];
        $entity->fixed_price = $row["fixed_price"];
        /*
        $entity->datetoexecute = $this->getDateByTimeZone($row["datetoexecute"]);
        $entity->timetoexecute = $row["timetoexecute"];
         */
        
        $entity->datetoexecute = $this->getDateByTimeZone($row["datetoexecute"].' '.$row["timetoexecute"]);
        $entity->timetoexecute = $row["timetoexecute"];
        
        
        $entity->subtotal = $row["subtotal"];
        $entity->taxes = $row["taxes"];
        $entity->paymentmethod = $row["paymentmethod"];
        $entity->paymentmethod_price = $row["paymentmethod_price"];
        $entity->totalpayment = $row["totalpayment"];
        
        $entity->actual_voicenumber = trim($row["actual_voicenumber"]);
        $entity->actual_faxnumber = trim($row["actual_faxnumber"]);
        $entity->actual_datanumber = trim($row["actual_datanumber"]);
        $entity->actual_imsi = trim($row["actual_ismi"]);
        $entity->actual_expirydate = trim($row["actual_expirydate"]);
        
        $entity->actual_puk1 = trim($row["actual_puk1"]);
        $entity->actual_puk2 = trim($row["actual_puk2"]);
        $entity->actual_pin  = trim($row["actual_pin"]);
        
        $entity->requestedbyuserid = $row["requestedbyuserid"];
        $entity->requesteddatetime = $this->getDateByTimeZone($row["requesteddatetime"]);
        $entity->processeddatetime = $this->getDateByTimeZone($row["processeddatetime"]);
        $entity->processedbyuserid = $row["processedbyuserid"];
        return $entity;
    }

    public function getRequestOperationsByStatus($_ordercode, $_status = "") {

        $query = "select a.* , ar.name as accountname, asg.name as accountassignedname , ";
        $query .=" ur.firstname as ur_firstname, ur.lastname as ur_lastname,  ur.username as ur_username, ur.phone as ur_phone, ur.phoneext as ur_phoneext, ur.mobile as ur_mobile,     ";
        $query .=" up.firstname as up_firstname, up.lastname as up_lastname,  up.username as up_username, up.phone as up_phone, up.phoneext as up_phoneext, up.mobile as up_mobile    ";
        $query .=" , it.operationregistered as servicestatus, it.voicenumber as actual_voicenumber, it.datanumber as actual_datanumber, it.faxnumber as actual_faxnumber, it.imsi as actual_imsi, it.expirydate as actual_expirydate  ";
        $query .="  , it.puk1 as actual_puk1, it.puk2 as actual_puk2, it.pin as actual_pin  ";
        $query .= "FROM airtimeoperation a ";
        $query .= "RIGHT JOIN ait_account ar on ar.id = a.accountid ";
        $query .= "RIGHT JOIN ait_account asg on asg.id = a.accountassignedid ";
        $query .= "RIGHT JOIN ait_usersystem ur on ur.id = a.requestedbyuserid  ";
        $query .= "LEFT  JOIN ait_usersystem up on up.id = a.processedbyuserid  ";
         $query .= "LEFT  JOIN inventoryitem it on it.id = a.inventoryitemid ";
        $query .=" WHERE a.airtimeordercode = '$_ordercode'  ";

        if ($_status != "") {
            $query .=" and a.operationstatus = $_status  ";
        }
        
           
        //echo $query; die();
        
        
        
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
     
        
        $aRecord = array();
        foreach ($rows as $row) {

            $entity = $this->setEntityByRow($row);
            $account = new Account();
            $accountassigned = new Account();
            $account->id = $row["accountid"];
            $account->name = $row["accountname"];
            $accountassigned->id = $row["accountassignedid"];
            $accountassigned->name = $row["accountassignedname"];
            $requesteduser = new User();
            $processeduser = new User();

            $requesteduser->id = $row["requestedbyuserid"];  //ur.phone as ur_phone, ur.phoneext as ur_phoneext, ur.mobile as ur_mobile, 
            $requesteduser->firstname = $row["ur_firstname"];
            $requesteduser->lastname = $row["ur_lastname"];
            $requesteduser->phone = $row["ur_phone"];
            $requesteduser->phoneext = $row["ur_phoneext"];
            $requesteduser->mobile = $row["ur_mobile"];

            $processeduser->id = $row["processedbyuserid"];
            $processeduser->firstname = $row["up_firstname"];
            $processeduser->lastname = $row["up_lastname"];
            $processeduser->phone = $row["up_phone"];
            $processeduser->phoneext = $row["up_phoneext"];
            $processeduser->mobile = $row["up_mobile"];

            $entity->account = $account;
            $entity->accountassigned = $accountassigned;
            $entity->requestedbyuser = $requesteduser;
            $entity->processedbyuser = $processeduser;

            array_push($aRecord, $entity);
        }
        return $aRecord;
    }

    public function getRequestOperationsByCart($_accountid, $_status = "") {

        $query = "select a.* , ar.name as accountname, asg.name as accountassignedname , ";
        $query .=" ur.firstname as ur_firstname, ur.lastname as ur_lastname,  ur.username as ur_username, ur.phone as ur_phone, ur.phoneext as ur_phoneext, ur.mobile as ur_mobile,     ";
        $query .=" up.firstname as up_firstname, up.lastname as up_lastname,  up.username as up_username, up.phone as up_phone, up.phoneext as up_phoneext, up.mobile as up_mobile    ";
        $query .= "  , it.operationregistered as servicestatus  ";
        $query .= "FROM airtimeoperation a ";
        $query .= "RIGHT JOIN ait_account ar on ar.id = a.accountid ";
        $query .= "RIGHT JOIN ait_account asg on asg.id = a.accountassignedid ";
        $query .= "RIGHT JOIN ait_usersystem ur on ur.id = a.requestedbyuserid  ";
        $query .= "LEFT  JOIN ait_usersystem up on up.id = a.processedbyuserid  ";
        $query .= "LEFT JOIN inventoryitem  it on it.id = a.inventoryitemid  ";
        $query .=" WHERE a.accountid =  $_accountid   ";
        $query .=" and a.requeststatus = '$_status'  ";

        

        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {

            $entity = $this->setEntityByRow($row);
            $account = new Account();
            $accountassigned = new Account();
            $account->id = $row["accountid"];
            $account->name = $row["accountname"];
            $accountassigned->id = $row["accountassignedid"];
            $accountassigned->name = $row["accountassignedname"];
            $requesteduser = new User();
            $processeduser = new User();

            $requesteduser->id = $row["requestedbyuserid"];  //ur.phone as ur_phone, ur.phoneext as ur_phoneext, ur.mobile as ur_mobile, 
            $requesteduser->firstname = $row["ur_firstname"];
            $requesteduser->lastname = $row["ur_lastname"];
            $requesteduser->phone = $row["ur_phone"];
            $requesteduser->phoneext = $row["ur_phoneext"];
            $requesteduser->mobile = $row["ur_mobile"];

            $processeduser->id = $row["processedbyuserid"];
            $processeduser->firstname = $row["up_firstname"];
            $processeduser->lastname = $row["up_lastname"];
            $processeduser->phone = $row["up_phone"];
            $processeduser->phoneext = $row["up_phoneext"];
            $processeduser->mobile = $row["up_mobile"];

            $entity->account = $account;
            $entity->accountassigned = $accountassigned;
            $entity->requestedbyuser = $requesteduser;
            $entity->processedbyuser = $processeduser;

            array_push($aRecord, $entity);
        }
        return $aRecord;
    }

    public function placeOrder($_e) {

        $_ordercommens = $_e->ordercomments;
        $query = "insert into airtimeorder ( id, accountcustomerid, accountproviderid, code, orderdate, userid, userip , ";
        $query .=" sessionid, totalitems, subtotal, taxes, payment_method, ";
        $query .=" convenience_fee, totalpayment, ordercomments )   ";
        $query .=" values ( '" . $_e->id . "' ,  " . $_e->accountcustomerid . " , " . $_e->accountproviderid . " , '" . $_e->code . "' , '" . $_e->orderdate . "'  , '" . $_e->userid . "'  , '" . $_e->userip . "' ,  ";
        $query .=" '" . $_e->sessionid . "' ,  " . $_e->totalitems . " , " . $_e->subtotal . " , " . $_e->taxes . " ,  '" . $_e->payment_method . "'  ,  ";
        $query .=" " . $_e->convenience_fee . " , " . $_e->totalpayment . " , '" . $_ordercommens . "' )  ";

        return $this->executeUpdate($query);
    }

    public function getOrdersByAccountCustomer($_accountid, $_search, $_orderby, $_orderdir, $limit1, $limit2) {
        $query = "select  ao.*    ";
        $query .=" , ac.name as accountcustomername, ap.name as accountprovidername   ";
        $query .=" , ur.firstname as ur_firstname, ur.lastname as ur_lastname, ur.phone , ur.phoneext, ur.username   ";
        $query .="  from airtimeorder ao  ";
        $query .=" right join ait_account ac on ac.id = ao.accountcustomerid   ";
        $query .=" right join ait_account ap on ap.id = ao.accountproviderid   ";
        $query .=" right join ait_usersystem ur on ur.id = ao.userid   ";
        $query .="  where ao.accountcustomerid = $_accountid ";
        
        if($_search != ""){
            $query .=" AND ( ";
            $query .="    ao.code like '%".$_search."%'  ";
            $query .=" OR ao.orderdate like '%".$_search."%'  ";
            $query .=" ) ";
        }
        
        $query .=" order by $_orderby $_orderdir  ";
        $query .=" limit  $limit1 , $limit2  ";
        
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            array_push($aRecord, $this->generateAirtimeOrderByRow($row));
        }
        return $aRecord;
    }

    public function getOrdersByAccountCustomerTotal($_accountid, $_search) {
        $query = "select  count(*) as total  ";
        $query .="  from airtimeorder ao  ";
        $query .=" right join ait_account ac on ac.id = ao.accountcustomerid   ";
        $query .=" right join ait_account ap on ap.id = ao.accountproviderid   ";
        $query .=" right join ait_usersystem ur on ur.id = ao.userid   ";
        $query .="  where ao.accountcustomerid = $_accountid   ";
        return intval($this->selectOneColumn("total", $query));
    }

    public function getOrdersByAccountProvider($_accountid, $_search, $_orderby, $_orderdir, $limit1, $limit2) {
        $query = "select  ao.*    ";
        $query .=" , ac.name as accountcustomername, ap.name as accountprovidername   ";
        $query .=" , ur.firstname as ur_firstname, ur.lastname as ur_lastname, ur.phone , ur.phoneext, ur.username   ";
        $query .="  from airtimeorder ao  ";
        $query .=" right join ait_account ac on ac.id = ao.accountcustomerid   ";
        $query .=" right join ait_account ap on ap.id = ao.accountproviderid   ";
        $query .=" right join ait_usersystem ur on ur.id = ao.userid   ";
        $query .="  where ao.accountproviderid = $_accountid   ";
        
        
        if($_search != ""){
            $query .=" AND ( ";
            $query .="    ao.code like '%".$_search."%'  ";
            $query .=" OR ao.orderdate like '%".$_search."%'  ";
            $query .=" ) ";
        }
        
        $query .=" order by $_orderby $_orderdir  ";
        $query .=" limit  $limit1 , $limit2  ";
        
        
        $rows = $this->selectRows($query);
        if (!$rows) {
            return false;
        }
        $total = count($rows);
        if ($total == 0) {
            return false;
        }
        $aRecord = array();
        foreach ($rows as $row) {
            array_push($aRecord, $this->generateAirtimeOrderByRow($row));
        }
        return $aRecord;
    }

    public function getOrdersByAccountProviderTotal($_accountid, $_search) {
        $query = "select  count(*) as total  ";
        $query .="  from airtimeorder ao  ";
        $query .=" right join ait_account ac on ac.id = ao.accountcustomerid   ";
        $query .=" right join ait_account ap on ap.id = ao.accountproviderid   ";
        $query .=" right join ait_usersystem ur on ur.id = ao.userid   ";
        $query .="  where ao.accountproviderid = $_accountid   ";
        return intval($this->selectOneColumn("total", $query));
    }

    public function getOrderById($_id) {
        $query = "select  ao.*    ";
        $query .=" , ac.name as accountcustomername, ap.name as accountprovidername   ";
        $query .=" , ur.firstname as ur_firstname, ur.lastname as ur_lastname, ur.phone , ur.phoneext, ur.username   ";
        $query .="  from airtimeorder ao  ";

        $query .="  right join ait_usersystem ur on ur.id = ao.userid ";
        $query .="  right join ait_account ac on ac.id = ao.accountcustomerid ";
        $query .="  right join ait_account ap on ap.id = ao.accountproviderid ";
        $query .="  where ao.id = '$_id'  ";

        $row = $this->selectOneRow($query);
        if (!$row) {
            return false;
        }
        return $this->generateAirtimeOrderByRow($row);
    }

    public function generateAirtimeOrderByRow($row) {
        $e = new AirtimeOrder();
        $e->id = $row["id"];
        $e->code = $row["code"];
        $e->orderstatus = trim($row["orderstatus"]);
        $e->accountcustomerid = $row["accountcustomerid"];
        $e->accountproviderid = $row["accountproviderid"];
        $e->orderdate = $this->getDateByTimeZone($row["orderdate"]);
        $e->userid = $row["userid"];
        $e->userip = $row["userip"];
        $e->sessionid = $row["sessionid"];
        $e->totalitems = $row["totalitems"];
        $e->subtotal = $row["subtotal"];
        $e->taxes = $row["taxes"];
        $e->payment_method = $row["payment_method"];
        $e->convenience_fee = $row["convenience_fee"];
        $e->totalpayment = $row["totalpayment"];
        $e->ordercomments = $row["ordercomments"];

        $aprovider = new Account();
        $aprovider->name = $row["accountprovidername"];
        $acustomer = new Account();
        $acustomer->name = $row["accountcustomername"];
        $e->accountcustomer = $acustomer;
        $e->accountprovider = $aprovider;

        $user = new User();
        $user->firstname = $row["ur_firstname"];
        $user->lastname = $row["ur_lastname"];
        $user->phone = $row["phone"];
        $user->phoneext = $row["phoneext"];

        $e->requesteduser = $user;

        return $e;
    }

    public function generateId() {
        return date('YmdHis') . '' . rand(10000, 99999) . '' . rand(10000, 99999);
    }

    public function generatecode($_accountid) {
        
        $yearcode = date('Y');
        $y2 = date('y');        
        
        $query = "select count(*) as total from airtimeorder where year(orderdate) = '$yearcode'    ";
        $index = '' . intval($this->selectOneColumn("total", $query)) + 1;
 
        while (strlen($index) < 7) {
            $index = "0" . $index;
        }

        return "OS-".$y2."". $index;
    }

    public function getCodeByChildOrder($_parentaccountid, $_code) {
        $query = "SELECT so.id ";
        $query .= "FROM airtimeorder  so   ";
        $query .= "WHERE  so.accountproviderid = $_parentaccountid  and so.code = '$_code' ";

        return $this->selectOneColumn("id", $query);
    }
    
    public function executeActivation( $_id, $_proccesedbyuserid, 
            $_voicenumber = '', $_datanumber= '', $_faxnumber = '', $_imsi = '', $_expirydate = '',   $_puk1 = '', $_puk2='', $_pin = ''
            ){
        
        $query  = "update airtimeoperation set operationstatus = 1, requeststatus = 'executed' , processeddatetime = CURRENT_TIMESTAMP(),   ";
        $query .= " processedbyuserid = '$_proccesedbyuserid' ,    ";
        $query .= " voicenumber = '$_voicenumber' , datanumber = '$_datanumber' ,  ";
        $query .= " faxnumber = '$_faxnumber' , imsi = '$_imsi' , expirydate = '$_expirydate' ,   ";
        $query .= " puk1 = '$_puk1' , puk2 = '$_puk2' , pin = '$_pin'  ";
        $query .= " where id = '$_id'  ";
        
       
        return $this->executeUpdate($query);
    }
    
    public function executeSuspend( $_id, $_proccesedbyuserid  , $_iserial ){
        
        $queryOld  = "update airtimeoperation set operationstatus = 2   "; 
        $queryOld .= "where operationstatus = 1 and iserial = '$_iserial' ";
        $resultOld = $this->executeUpdate($queryOld);
        
        
        $query  = "update airtimeoperation set operationstatus = 1, requeststatus = 'executed' , processeddatetime = CURRENT_TIMESTAMP(),   ";
        $query .= "processedbyuserid = '$_proccesedbyuserid'  ";
        $query .= "where id = '$_id'  ";
        
        return $this->executeUpdate($query);
    }  
    
    public function executeDeactivation( $_id, $_proccesedbyuserid  , $_iserial ){
        
        $queryOld  = "update airtimeoperation set operationstatus = 2   "; 
        $queryOld .= "where operationstatus = 1 and iserial = '$_iserial' ";
        $resultOld = $this->executeUpdate($queryOld);
        
        
        $query  = "update airtimeoperation set operationstatus = 1, requeststatus = 'executed' , processeddatetime = CURRENT_TIMESTAMP(),   ";
        $query .= "processedbyuserid = '$_proccesedbyuserid'  ";
        $query .= "where id = '$_id'  ";
        
        return $this->executeUpdate($query);
    }     
    
    public function executeActivationToDevice($_accountid, $_iserial, $_operationregistered, 
             $_voicenumber = '', $_datanumber= '', $_faxnumber = '', $_imsi = '',  $_expirydate = '',  $_puk1 = '', $_puk2='', $_pin = ''
            ) {
        $query = " update inventoryitem set operationregistered = '$_operationregistered' , operationrequested = 'executed' , voicenumber = '$_voicenumber' ,  ";
        $query .="  datanumber = '$_datanumber' ,  ";
        $query .="  faxnumber = '$_faxnumber' , imsi = '$_imsi' , expirydate = '$_expirydate' ,   ";
        $query .="  puk1 = '$_puk1' , puk2 = '$_puk2' , pin = '$_pin' ";
        $query .="  where iserial = '$_iserial' and accountid = $_accountid ";
        
        return $this->executeUpdate($query); 
    }
    
    public function executeSuspendToDevice($_accountid, $_iserial, $_operationregistered   ) {
        $query = " update inventoryitem set operationregistered = '$_operationregistered' , operationrequested = 'executed'   ";
        $query .=" where iserial = '$_iserial' and accountid = $_accountid ";
        
        return $this->executeUpdate($query); 
    }    
    
    public function executeDeactivationToDevice($_accountid, $_iserial, $_operationregistered   ) {
        $query = " update inventoryitem set operationregistered = '$_operationregistered' , operationrequested = 'executed'   ";
        $query .=" where iserial = '$_iserial' and accountid = $_accountid ";
        return $this->executeUpdate($query); 
    }      
    
    
     
    public function getVoicenumber($_voicenumber){
        $query = "select iserial from inventoryitem where voicenumber = '$_voicenumber' ";
        $rows = $this->selectRows($query);
        if(!$rows){ return false; }
        $total = count($rows);
        $aRecords = array();
        if($total == 0){ return false; }
        foreach($rows as $row){
            $iserial = trim($row["iserial"]);
            array_push($aRecords, $iserial);
        }
        return $aRecords;
    }

    public function getDatanumber($_datanumber){
        $query = "select iserial from inventoryitem where datanumber = '$_datanumber' ";
        $rows = $this->selectRows($query);
        if(!$rows){ return false; }
        $total = count($rows);
        $aRecords = array();
        if($total == 0){ return false; }
        foreach($rows as $row){
            $iserial = trim($row["iserial"]);
            array_push($aRecords, $iserial);
        }
        return $aRecords;
    }    
    
    public function getFaxnumber($_faxnumber){
        $query = "select iserial from inventoryitem where faxnumber = '$_faxnumber' ";
        $rows = $this->selectRows($query);
        if(!$rows){ return false; }
        $total = count($rows);
        $aRecords = array();
        if($total == 0){ return false; }
        foreach($rows as $row){
            $iserial = trim($row["iserial"]);
            array_push($aRecords, $iserial);
        }
        return $aRecords;
    }       
    
    
    public function getImsi($_imsi){
        $query = "select iserial from inventoryitem where imsi = '$_imsi' ";
        $rows = $this->selectRows($query);
        if(!$rows){ return false; }
        $total = count($rows);
        $aRecords = array();
        if($total == 0){ return false; }
        foreach($rows as $row){
            $iserial = trim($row["iserial"]);
            array_push($aRecords, $iserial);
        }
        return $aRecords;
    }       
    
} 