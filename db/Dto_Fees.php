<?php
class Dto_Fees extends Dto_Db {
 
    public function setFeesByAccountId($_accountid, $_creditcard, $_wiretransfer){
        $tempaccountid = intval( $this->selectOneColumn("accountid", " select accountid from fees where accountid = $_accountid  ") );
        $query = "";
        if($tempaccountid > 0){
            $query = "update fees set creditcard = $_creditcard , wiretransfer = $_wiretransfer where accountid = $_accountid ";
        }else{
            $query = "insert into fees (accountid, creditcard, wiretransfer) values($_accountid, $_creditcard, $_wiretransfer ) ";
        }
        return $this->executeUpdate($query);
    }
    
    public function getFeesByAccountId($_accountid){
        $entity = new Fees();
        $query = "select * from fees where accountid = $_accountid";
        $row = $this->selectOneRow($query);
      
        $entity->accountid = $_accountid;
        $entity->creditcard = doubleval($row["creditcard"]);
        $entity->wiretransfer = doubleval($row["wiretransfer"]);
        
        return $entity;
    }
    
}